<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f">Copy</a>(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1">GetImagData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db">GetImagInd</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764">GetImagIndSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b">GetImagPtr</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2">GetImagPtrSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetNonZeros</b>() const  (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501">GetRealData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0">GetRealInd</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684">GetRealIndSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929">GetRealPtr</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882">GetRealPtrSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>imag_ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9">Matrix_ComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a0105ca0b54784973e1272931f9c9e420">Matrix_ComplexSparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a728b0dd58af72224fc92a6ab299ad274">Matrix_ComplexSparse</a>(int i, int j, int real_nz, int imag_nz)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a079f7006a23ebdc5e63c2337f88f9bfc">Matrix_ComplexSparse</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3df0b27785c83bf1b8809cdd263c48c5">Matrix_ComplexSparse</a>(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a10672a9a570060c9bd8f17eb54dc46fa">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383">operator=</a>(const Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>real_ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85">SetData</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;real_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;real_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;real_ind, Vector&lt; T, Storage0, Allocator0 &gt; &amp;imag_values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;imag_ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, int real_nz, pointer real_values, int *real_ptr, int *real_ind, int imag_nz, pointer imag_values, int *imag_ptr, int *imag_ind) (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a50bafb538cfd1b8e6bb18bba62451dff">SetData</a>(int i, int j, int real_nz, typename Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer real_values, int *real_ptr, int *real_ind, int imag_nz, typename Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;::pointer imag_values, int *imag_ptr, int *imag_ind)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a61f989f4b8128edac8f6b873ea88030d">Val</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75">~Matrix_ComplexSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___complex_sparse.php">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
