<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/preconditioner/IlutPreconditioning.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_ILUT_PRECONDITIONING_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="preprocessor">#include &quot;SymmetricIlutPreconditioning.cxx&quot;</span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="keyword">namespace </span>Seldon
<a name="l00025"></a>00025 {
<a name="l00026"></a>00026 
<a name="l00027"></a>00027   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00028"></a>00028   IlutPreconditioning&lt;real, cplx, Allocator&gt;::IlutPreconditioning()
<a name="l00029"></a>00029   {
<a name="l00030"></a>00030     print_level = 0;
<a name="l00031"></a>00031     symmetric_algorithm = <span class="keyword">false</span>;
<a name="l00032"></a>00032     type_ilu = ILUT;
<a name="l00033"></a>00033     fill_level = 1000000;
<a name="l00034"></a>00034     additional_fill = 1000000;
<a name="l00035"></a>00035     mbloc = 1000000;
<a name="l00036"></a>00036     alpha = 1.0;
<a name="l00037"></a>00037     droptol = 0.01;
<a name="l00038"></a>00038     permtol = 0.1;
<a name="l00039"></a>00039   }
<a name="l00040"></a>00040 
<a name="l00041"></a>00041 
<a name="l00042"></a>00042   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00043"></a>00043   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::Clear()
<a name="l00044"></a>00044   {
<a name="l00045"></a>00045     permutation_row.Clear();
<a name="l00046"></a>00046     mat_sym.Clear();
<a name="l00047"></a>00047     mat_unsym.Clear();
<a name="l00048"></a>00048     xtmp.Clear();
<a name="l00049"></a>00049   }
<a name="l00050"></a>00050 
<a name="l00051"></a>00051 
<a name="l00052"></a>00052   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keywordtype">int</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetFactorisationType()<span class="keyword"> const</span>
<a name="l00054"></a>00054 <span class="keyword">  </span>{
<a name="l00055"></a>00055     <span class="keywordflow">return</span> type_ilu;
<a name="l00056"></a>00056   }
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 
<a name="l00059"></a>00059   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00060"></a>00060   <span class="keywordtype">int</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetFillLevel()<span class="keyword"> const</span>
<a name="l00061"></a>00061 <span class="keyword">  </span>{
<a name="l00062"></a>00062     <span class="keywordflow">return</span> fill_level;
<a name="l00063"></a>00063   }
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 
<a name="l00066"></a>00066   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00067"></a>00067   <span class="keywordtype">int</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetAdditionalFillNumber()<span class="keyword"></span>
<a name="l00068"></a>00068 <span class="keyword">    const</span>
<a name="l00069"></a>00069 <span class="keyword">  </span>{
<a name="l00070"></a>00070     <span class="keywordflow">return</span> additional_fill;
<a name="l00071"></a>00071   }
<a name="l00072"></a>00072 
<a name="l00073"></a>00073 
<a name="l00074"></a>00074   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00075"></a>00075   <span class="keywordtype">int</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetPrintLevel()<span class="keyword"> const</span>
<a name="l00076"></a>00076 <span class="keyword">  </span>{
<a name="l00077"></a>00077     <span class="keywordflow">return</span> print_level;
<a name="l00078"></a>00078   }
<a name="l00079"></a>00079 
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00082"></a>00082   <span class="keywordtype">int</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetPivotBlockInteger()<span class="keyword"> const</span>
<a name="l00083"></a>00083 <span class="keyword">  </span>{
<a name="l00084"></a>00084     <span class="keywordflow">return</span> mbloc;
<a name="l00085"></a>00085   }
<a name="l00086"></a>00086 
<a name="l00087"></a>00087 
<a name="l00088"></a>00088   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00089"></a>00089   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00090"></a>00090   ::SetFactorisationType(<span class="keywordtype">int</span> type)
<a name="l00091"></a>00091   {
<a name="l00092"></a>00092     type_ilu = type;
<a name="l00093"></a>00093   }
<a name="l00094"></a>00094 
<a name="l00095"></a>00095 
<a name="l00096"></a>00096   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00097"></a>00097   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetFillLevel(<span class="keywordtype">int</span> level)
<a name="l00098"></a>00098   {
<a name="l00099"></a>00099     fill_level = level;
<a name="l00100"></a>00100   }
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00103"></a>00103   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00104"></a>00104   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00105"></a>00105   ::SetAdditionalFillNumber(<span class="keywordtype">int</span> level)
<a name="l00106"></a>00106   {
<a name="l00107"></a>00107     additional_fill = level;
<a name="l00108"></a>00108   }
<a name="l00109"></a>00109 
<a name="l00110"></a>00110 
<a name="l00111"></a>00111   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00112"></a>00112   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetPrintLevel(<span class="keywordtype">int</span> level)
<a name="l00113"></a>00113   {
<a name="l00114"></a>00114     print_level = level;
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00118"></a>00118   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00119"></a>00119   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetPivotBlockInteger(<span class="keywordtype">int</span> i)
<a name="l00120"></a>00120   {
<a name="l00121"></a>00121     mbloc = i;
<a name="l00122"></a>00122   }
<a name="l00123"></a>00123 
<a name="l00124"></a>00124 
<a name="l00125"></a>00125   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00126"></a>00126   real IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00127"></a>00127   ::GetDroppingThreshold()<span class="keyword"> const</span>
<a name="l00128"></a>00128 <span class="keyword">  </span>{
<a name="l00129"></a>00129     <span class="keywordflow">return</span> droptol;
<a name="l00130"></a>00130   }
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 
<a name="l00133"></a>00133   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00134"></a>00134   real IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00135"></a>00135   ::GetDiagonalCoefficient()<span class="keyword"> const</span>
<a name="l00136"></a>00136 <span class="keyword">  </span>{
<a name="l00137"></a>00137     <span class="keywordflow">return</span> alpha;
<a name="l00138"></a>00138   }
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00141"></a>00141   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00142"></a>00142   real IlutPreconditioning&lt;real, cplx, Allocator&gt;::GetPivotThreshold()<span class="keyword"> const</span>
<a name="l00143"></a>00143 <span class="keyword">  </span>{
<a name="l00144"></a>00144     <span class="keywordflow">return</span> permtol;
<a name="l00145"></a>00145   }
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00148"></a>00148   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00149"></a>00149   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00150"></a>00150   ::SetDroppingThreshold(real tol)
<a name="l00151"></a>00151   {
<a name="l00152"></a>00152     droptol = tol;
<a name="l00153"></a>00153   }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 
<a name="l00156"></a>00156   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00157"></a>00157   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;
<a name="l00158"></a>00158   ::SetDiagonalCoefficient(real beta)
<a name="l00159"></a>00159   {
<a name="l00160"></a>00160     alpha = beta;
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00164"></a>00164   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00165"></a>00165   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetPivotThreshold(real tol)
<a name="l00166"></a>00166   {
<a name="l00167"></a>00167     permtol = tol;
<a name="l00168"></a>00168   }
<a name="l00169"></a>00169 
<a name="l00170"></a>00170 
<a name="l00171"></a>00171   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00172"></a>00172   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetSymmetricAlgorithm()
<a name="l00173"></a>00173   {
<a name="l00174"></a>00174     symmetric_algorithm = <span class="keyword">true</span>;
<a name="l00175"></a>00175   }
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00178"></a>00178   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00179"></a>00179   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::SetUnsymmetricAlgorithm()
<a name="l00180"></a>00180   {
<a name="l00181"></a>00181     symmetric_algorithm = <span class="keyword">false</span>;
<a name="l00182"></a>00182   }
<a name="l00183"></a>00183 
<a name="l00184"></a>00184 
<a name="l00185"></a>00185   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00186"></a>00186   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00187"></a>00187   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00188"></a>00188   FactorizeMatrix(<span class="keyword">const</span> IVect&amp; perm,
<a name="l00189"></a>00189                   Matrix&lt;T0, General, Storage0, Allocator0&gt;&amp; mat,
<a name="l00190"></a>00190                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00191"></a>00191   {
<a name="l00192"></a>00192     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00193"></a>00193       {
<a name="l00194"></a>00194         cout &lt;&lt; <span class="stringliteral">&quot;Conversion to symmetric matrices not implemented.&quot;</span> &lt;&lt; endl;
<a name="l00195"></a>00195         abort();
<a name="l00196"></a>00196       }
<a name="l00197"></a>00197     <span class="keywordflow">else</span>
<a name="l00198"></a>00198       FactorizeUnsymMatrix(perm, mat, keep_matrix);
<a name="l00199"></a>00199   }
<a name="l00200"></a>00200 
<a name="l00201"></a>00201 
<a name="l00202"></a>00202   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00203"></a>00203   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l00204"></a>00204   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00205"></a>00205   FactorizeMatrix(<span class="keyword">const</span> IVect&amp; perm,
<a name="l00206"></a>00206                   Matrix&lt;T0, Symmetric, Storage0, Allocator0&gt;&amp; mat,
<a name="l00207"></a>00207                   <span class="keywordtype">bool</span> keep_matrix)
<a name="l00208"></a>00208   {
<a name="l00209"></a>00209     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00210"></a>00210       FactorizeSymMatrix(perm, mat, keep_matrix);
<a name="l00211"></a>00211     <span class="keywordflow">else</span>
<a name="l00212"></a>00212       FactorizeUnsymMatrix(perm, mat, keep_matrix);
<a name="l00213"></a>00213   }
<a name="l00214"></a>00214 
<a name="l00215"></a>00215 
<a name="l00216"></a>00216   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00217"></a>00217   <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00218"></a>00218   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00219"></a>00219   FactorizeSymMatrix(<span class="keyword">const</span> IVect&amp; perm, MatrixSparse&amp; mat, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00220"></a>00220   {
<a name="l00221"></a>00221     IVect inv_permutation;
<a name="l00222"></a>00222 
<a name="l00223"></a>00223     <span class="comment">// We convert matrix to symmetric format.</span>
<a name="l00224"></a>00224     Copy(mat, mat_sym);
<a name="l00225"></a>00225 
<a name="l00226"></a>00226     <span class="comment">// Old matrix is erased if needed.</span>
<a name="l00227"></a>00227     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00228"></a>00228       mat.Clear();
<a name="l00229"></a>00229 
<a name="l00230"></a>00230     <span class="comment">// We keep permutation array in memory, and check it.</span>
<a name="l00231"></a>00231     <span class="keywordtype">int</span> n = mat_sym.GetM();
<a name="l00232"></a>00232     <span class="keywordflow">if</span> (perm.GetM() != n)
<a name="l00233"></a>00233       {
<a name="l00234"></a>00234         cout &lt;&lt; <span class="stringliteral">&quot;Numbering array should have the same size as matrix.&quot;</span>;
<a name="l00235"></a>00235         cout &lt;&lt; endl;
<a name="l00236"></a>00236         abort();
<a name="l00237"></a>00237       }
<a name="l00238"></a>00238 
<a name="l00239"></a>00239     permutation_row.Reallocate(n);
<a name="l00240"></a>00240     inv_permutation.Reallocate(n);
<a name="l00241"></a>00241     inv_permutation.Fill(-1);
<a name="l00242"></a>00242     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00243"></a>00243       {
<a name="l00244"></a>00244         permutation_row(i) = perm(i);
<a name="l00245"></a>00245         inv_permutation(perm(i)) = i;
<a name="l00246"></a>00246       }
<a name="l00247"></a>00247 
<a name="l00248"></a>00248     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00249"></a>00249       <span class="keywordflow">if</span> (inv_permutation(i) == -1)
<a name="l00250"></a>00250         {
<a name="l00251"></a>00251           cout &lt;&lt; <span class="stringliteral">&quot;Error in the numbering array.&quot;</span> &lt;&lt; endl;
<a name="l00252"></a>00252           abort();
<a name="l00253"></a>00253         }
<a name="l00254"></a>00254 
<a name="l00255"></a>00255     <span class="comment">// Matrix is permuted.</span>
<a name="l00256"></a>00256     <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(mat_sym, perm, perm);
<a name="l00257"></a>00257 
<a name="l00258"></a>00258     <span class="comment">// Temporary vector used for solving.</span>
<a name="l00259"></a>00259     xtmp.Reallocate(n);
<a name="l00260"></a>00260 
<a name="l00261"></a>00261     <span class="comment">// Factorization is performed.</span>
<a name="l00262"></a>00262     <a class="code" href="namespace_seldon.php#aba8d356357d8ffc5f8baa3c5168dc29c" title="Incomplete factorization with pivot for unsymmetric matrix.">GetIlut</a>(*<span class="keyword">this</span>, mat_sym);
<a name="l00263"></a>00263   }
<a name="l00264"></a>00264 
<a name="l00265"></a>00265 
<a name="l00266"></a>00266   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00267"></a>00267   <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00268"></a>00268   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00269"></a>00269   FactorizeUnsymMatrix(<span class="keyword">const</span> IVect&amp; perm, MatrixSparse&amp; mat, <span class="keywordtype">bool</span> keep_matrix)
<a name="l00270"></a>00270   {
<a name="l00271"></a>00271     IVect inv_permutation;
<a name="l00272"></a>00272 
<a name="l00273"></a>00273     <span class="comment">// We convert matrix to unsymmetric format.</span>
<a name="l00274"></a>00274     Copy(mat, mat_unsym);
<a name="l00275"></a>00275 
<a name="l00276"></a>00276     <span class="comment">// Old matrix is erased if needed.</span>
<a name="l00277"></a>00277     <span class="keywordflow">if</span> (!keep_matrix)
<a name="l00278"></a>00278       mat.Clear();
<a name="l00279"></a>00279 
<a name="l00280"></a>00280     <span class="comment">// We keep permutation array in memory, and check it.</span>
<a name="l00281"></a>00281     <span class="keywordtype">int</span> n = mat_unsym.GetM();
<a name="l00282"></a>00282     <span class="keywordflow">if</span> (perm.GetM() != n)
<a name="l00283"></a>00283       {
<a name="l00284"></a>00284         cout &lt;&lt; <span class="stringliteral">&quot;Numbering array should have the same size as matrix.&quot;</span>;
<a name="l00285"></a>00285         cout &lt;&lt; endl;
<a name="l00286"></a>00286         abort();
<a name="l00287"></a>00287       }
<a name="l00288"></a>00288 
<a name="l00289"></a>00289     permutation_row.Reallocate(n);
<a name="l00290"></a>00290     permutation_col.Reallocate(n);
<a name="l00291"></a>00291     inv_permutation.Reallocate(n);
<a name="l00292"></a>00292     inv_permutation.Fill(-1);
<a name="l00293"></a>00293     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00294"></a>00294       {
<a name="l00295"></a>00295         permutation_row(i) = i;
<a name="l00296"></a>00296         permutation_col(i) = i;
<a name="l00297"></a>00297         inv_permutation(perm(i)) = i;
<a name="l00298"></a>00298       }
<a name="l00299"></a>00299 
<a name="l00300"></a>00300     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00301"></a>00301       <span class="keywordflow">if</span> (inv_permutation(i) == -1)
<a name="l00302"></a>00302         {
<a name="l00303"></a>00303           cout &lt;&lt; <span class="stringliteral">&quot;Error in the numbering array.&quot;</span> &lt;&lt; endl;
<a name="l00304"></a>00304           abort();
<a name="l00305"></a>00305         }
<a name="l00306"></a>00306 
<a name="l00307"></a>00307     IVect iperm = inv_permutation;
<a name="l00308"></a>00308 
<a name="l00309"></a>00309     <span class="comment">// Rows of matrix are permuted.</span>
<a name="l00310"></a>00310     <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(mat_unsym, perm, perm);
<a name="l00311"></a>00311 
<a name="l00312"></a>00312     <span class="comment">// Temporary vector used for solving.</span>
<a name="l00313"></a>00313     xtmp.Reallocate(n);
<a name="l00314"></a>00314 
<a name="l00315"></a>00315     <span class="comment">// Factorization is performed.</span>
<a name="l00316"></a>00316     <span class="comment">// Columns are permuted during the factorization.</span>
<a name="l00317"></a>00317     inv_permutation.Fill();
<a name="l00318"></a>00318     <a class="code" href="namespace_seldon.php#aba8d356357d8ffc5f8baa3c5168dc29c" title="Incomplete factorization with pivot for unsymmetric matrix.">GetIlut</a>(*<span class="keyword">this</span>, mat_unsym, permutation_col, inv_permutation);
<a name="l00319"></a>00319 
<a name="l00320"></a>00320     <span class="comment">// Combining permutations.</span>
<a name="l00321"></a>00321     IVect itmp = permutation_col;
<a name="l00322"></a>00322     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; n; i++)
<a name="l00323"></a>00323       permutation_col(i) = iperm(itmp(i));
<a name="l00324"></a>00324 
<a name="l00325"></a>00325     permutation_row = perm;
<a name="l00326"></a>00326   }
<a name="l00327"></a>00327 
<a name="l00328"></a>00328 
<a name="l00329"></a>00329   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00330"></a>00330   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00331"></a>00331   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00332"></a>00332   Solve(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1&amp; r, Vector1&amp; z)
<a name="l00333"></a>00333   {
<a name="l00334"></a>00334     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00335"></a>00335       {
<a name="l00336"></a>00336         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00337"></a>00337           xtmp(permutation_row(i)) = r(i);
<a name="l00338"></a>00338 
<a name="l00339"></a>00339         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(mat_sym, xtmp);
<a name="l00340"></a>00340 
<a name="l00341"></a>00341         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00342"></a>00342           z(i) = xtmp(permutation_row(i));
<a name="l00343"></a>00343       }
<a name="l00344"></a>00344     <span class="keywordflow">else</span>
<a name="l00345"></a>00345       {
<a name="l00346"></a>00346         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00347"></a>00347           xtmp(permutation_row(i)) = r(i);
<a name="l00348"></a>00348 
<a name="l00349"></a>00349         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(mat_unsym, xtmp);
<a name="l00350"></a>00350 
<a name="l00351"></a>00351         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00352"></a>00352           z(permutation_col(i)) = xtmp(i);
<a name="l00353"></a>00353       }
<a name="l00354"></a>00354   }
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 
<a name="l00357"></a>00357   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00358"></a>00358   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1&gt;
<a name="l00359"></a>00359   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::
<a name="l00360"></a>00360   TransSolve(<span class="keyword">const</span> Matrix1&amp; A, <span class="keyword">const</span> Vector1&amp; r, Vector1&amp; z)
<a name="l00361"></a>00361   {
<a name="l00362"></a>00362     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00363"></a>00363       Solve(A, r, z);
<a name="l00364"></a>00364     <span class="keywordflow">else</span>
<a name="l00365"></a>00365       {
<a name="l00366"></a>00366         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00367"></a>00367           xtmp(i) = r(permutation_col(i));
<a name="l00368"></a>00368 
<a name="l00369"></a>00369         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(SeldonTrans, mat_unsym, xtmp);
<a name="l00370"></a>00370 
<a name="l00371"></a>00371         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; r.GetM(); i++)
<a name="l00372"></a>00372           z(i) = xtmp(permutation_row(i));
<a name="l00373"></a>00373       }
<a name="l00374"></a>00374   }
<a name="l00375"></a>00375 
<a name="l00376"></a>00376 
<a name="l00377"></a>00377   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00378"></a>00378   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00379"></a>00379   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::Solve(Vector1&amp; z)
<a name="l00380"></a>00380   {
<a name="l00381"></a>00381     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00382"></a>00382       {
<a name="l00383"></a>00383         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00384"></a>00384           xtmp(permutation_row(i)) = z(i);
<a name="l00385"></a>00385 
<a name="l00386"></a>00386         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(mat_sym, xtmp);
<a name="l00387"></a>00387 
<a name="l00388"></a>00388         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00389"></a>00389           z(i) = xtmp(permutation_row(i));
<a name="l00390"></a>00390       }
<a name="l00391"></a>00391     <span class="keywordflow">else</span>
<a name="l00392"></a>00392       {
<a name="l00393"></a>00393         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00394"></a>00394           xtmp(permutation_row(i)) = z(i);
<a name="l00395"></a>00395 
<a name="l00396"></a>00396         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(mat_unsym, xtmp);
<a name="l00397"></a>00397 
<a name="l00398"></a>00398         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00399"></a>00399           z(permutation_col(i)) = xtmp(i);
<a name="l00400"></a>00400       }
<a name="l00401"></a>00401   }
<a name="l00402"></a>00402 
<a name="l00403"></a>00403 
<a name="l00404"></a>00404   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00405"></a>00405   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Vector1&gt;
<a name="l00406"></a>00406   <span class="keywordtype">void</span> IlutPreconditioning&lt;real, cplx, Allocator&gt;::TransSolve(Vector1&amp; z)
<a name="l00407"></a>00407   {
<a name="l00408"></a>00408     <span class="keywordflow">if</span> (symmetric_algorithm)
<a name="l00409"></a>00409       Solve(z);
<a name="l00410"></a>00410     <span class="keywordflow">else</span>
<a name="l00411"></a>00411       {
<a name="l00412"></a>00412         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00413"></a>00413           xtmp(i) = z(permutation_col(i));
<a name="l00414"></a>00414 
<a name="l00415"></a>00415         <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(SeldonTrans, mat_unsym, xtmp);
<a name="l00416"></a>00416 
<a name="l00417"></a>00417         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; z.GetM(); i++)
<a name="l00418"></a>00418           z(i) = xtmp(permutation_row(i));
<a name="l00419"></a>00419       }
<a name="l00420"></a>00420   }
<a name="l00421"></a>00421 
<a name="l00422"></a>00422 
<a name="l00423"></a>00423   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00424"></a>00424   <span class="keywordtype">void</span> qsplit_ilut(Vector&lt;cplx, Storage, Allocator&gt;&amp; a, IVect&amp; ind, <span class="keywordtype">int</span> first,
<a name="l00425"></a>00425                    <span class="keywordtype">int</span> n, <span class="keywordtype">int</span> ncut, <span class="keyword">const</span> real&amp; abs_ncut)
<a name="l00426"></a>00426   {
<a name="l00427"></a>00427     <span class="comment">//-----------------------------------------------------------------------</span>
<a name="l00428"></a>00428     <span class="comment">//     does a quick-sort split of a real array.</span>
<a name="l00429"></a>00429     <span class="comment">//     on input a(1:n). is a real array</span>
<a name="l00430"></a>00430     <span class="comment">//     on output a(1:n) is permuted such that its elements satisfy:</span>
<a name="l00431"></a>00431     <span class="comment">//</span>
<a name="l00432"></a>00432     <span class="comment">//     abs(a(i)) .ge. abs(a(ncut)) for i .lt. ncut and</span>
<a name="l00433"></a>00433     <span class="comment">//     abs(a(i)) .le. abs(a(ncut)) for i .gt. ncut</span>
<a name="l00434"></a>00434     <span class="comment">//</span>
<a name="l00435"></a>00435     <span class="comment">//     ind is an integer array which permuted in the same way as a</span>
<a name="l00436"></a>00436     <span class="comment">//-----------------------------------------------------------------------</span>
<a name="l00437"></a>00437     <span class="keywordtype">int</span> last = n-1;
<a name="l00438"></a>00438     <span class="keywordtype">int</span> ncut_ = ncut-1;
<a name="l00439"></a>00439     <span class="keywordtype">int</span> first_ = first;
<a name="l00440"></a>00440 
<a name="l00441"></a>00441     <span class="keywordflow">if</span> ((ncut_ &lt; first_) || (ncut_ &gt; last))
<a name="l00442"></a>00442       <span class="keywordflow">return</span>;
<a name="l00443"></a>00443 
<a name="l00444"></a>00444     cplx tmp; real abskey;
<a name="l00445"></a>00445     <span class="keywordtype">int</span> mid, itmp;
<a name="l00446"></a>00446     <span class="keywordtype">bool</span> test_loop = <span class="keyword">true</span>;
<a name="l00447"></a>00447 
<a name="l00448"></a>00448     <span class="comment">//     outer loop -- while mid .ne. ncut do</span>
<a name="l00449"></a>00449     <span class="keywordflow">while</span> (test_loop)
<a name="l00450"></a>00450       {
<a name="l00451"></a>00451         mid = first_;
<a name="l00452"></a>00452         abskey = abs(a(mid));
<a name="l00453"></a>00453         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = (first_+1); j &lt;= last; j++)
<a name="l00454"></a>00454           {
<a name="l00455"></a>00455             <span class="keywordflow">if</span> (abs(a(j)) &gt; abskey)
<a name="l00456"></a>00456               {
<a name="l00457"></a>00457                 mid++;
<a name="l00458"></a>00458                 <span class="comment">// Interchange.</span>
<a name="l00459"></a>00459                 tmp = a(mid);
<a name="l00460"></a>00460                 itmp = ind(mid);
<a name="l00461"></a>00461                 a(mid) = a(j);
<a name="l00462"></a>00462                 ind(mid) = ind(j);
<a name="l00463"></a>00463                 a(j)  = tmp;
<a name="l00464"></a>00464                 ind(j) = itmp;
<a name="l00465"></a>00465               }
<a name="l00466"></a>00466           }
<a name="l00467"></a>00467 
<a name="l00468"></a>00468         <span class="comment">// Interchange.</span>
<a name="l00469"></a>00469         tmp = a(mid);
<a name="l00470"></a>00470         a(mid) = a(first_);
<a name="l00471"></a>00471         a(first_)  = tmp;
<a name="l00472"></a>00472 
<a name="l00473"></a>00473         itmp = ind(mid);
<a name="l00474"></a>00474         ind(mid) = ind(first_);
<a name="l00475"></a>00475         ind(first_) = itmp;
<a name="l00476"></a>00476 
<a name="l00477"></a>00477         <span class="comment">// Test for while loop.</span>
<a name="l00478"></a>00478         <span class="keywordflow">if</span> (mid == ncut_)
<a name="l00479"></a>00479           <span class="keywordflow">return</span>;
<a name="l00480"></a>00480 
<a name="l00481"></a>00481         <span class="keywordflow">if</span> (mid &gt; ncut_)
<a name="l00482"></a>00482           last = mid-1;
<a name="l00483"></a>00483         <span class="keywordflow">else</span>
<a name="l00484"></a>00484           first_ = mid+1;
<a name="l00485"></a>00485       }
<a name="l00486"></a>00486   }
<a name="l00487"></a>00487 
<a name="l00488"></a>00488 
<a name="l00490"></a>00490   <span class="keyword">template</span>&lt;<span class="keyword">class</span> real, <span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00491"></a>00491   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aba8d356357d8ffc5f8baa3c5168dc29c" title="Incomplete factorization with pivot for unsymmetric matrix.">GetIlut</a>(<span class="keyword">const</span> IlutPreconditioning&lt;real, cplx, Allocator1&gt;&amp; param,
<a name="l00492"></a>00492                Matrix&lt;cplx, General, ArrayRowSparse, Allocator2&gt;&amp; A,
<a name="l00493"></a>00493                IVect&amp; iperm, IVect&amp; rperm)
<a name="l00494"></a>00494   {
<a name="l00495"></a>00495     <span class="keywordtype">int</span> size_row;
<a name="l00496"></a>00496     <span class="keywordtype">int</span> n = A.GetN();
<a name="l00497"></a>00497     <span class="keywordtype">int</span> type_factorisation = param.GetFactorisationType();
<a name="l00498"></a>00498     <span class="keywordtype">int</span> lfil = param.GetFillLevel();
<a name="l00499"></a>00499     real zero(0);
<a name="l00500"></a>00500     real droptol = param.GetDroppingThreshold();
<a name="l00501"></a>00501     real alpha = param.GetDiagonalCoefficient();
<a name="l00502"></a>00502     <span class="keywordtype">bool</span> variable_fill = <span class="keyword">false</span>;
<a name="l00503"></a>00503     <span class="keywordtype">bool</span> standard_dropping = <span class="keyword">true</span>;
<a name="l00504"></a>00504     <span class="keywordtype">int</span> additional_fill = param.GetAdditionalFillNumber();
<a name="l00505"></a>00505     <span class="keywordtype">int</span> mbloc = param.GetPivotBlockInteger();
<a name="l00506"></a>00506     real permtol = param.GetPivotThreshold();
<a name="l00507"></a>00507     <span class="keywordtype">int</span> print_level = param.GetPrintLevel();
<a name="l00508"></a>00508 
<a name="l00509"></a>00509     <span class="keywordflow">if</span> (type_factorisation == param.ILUT)
<a name="l00510"></a>00510       standard_dropping = <span class="keyword">false</span>;
<a name="l00511"></a>00511     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_factorisation == param.ILU_D)
<a name="l00512"></a>00512       standard_dropping = <span class="keyword">true</span>;
<a name="l00513"></a>00513     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_factorisation == param.ILUT_K)
<a name="l00514"></a>00514       {
<a name="l00515"></a>00515         variable_fill = <span class="keyword">true</span>;   <span class="comment">// We use a variable lfil</span>
<a name="l00516"></a>00516         standard_dropping = <span class="keyword">false</span>;
<a name="l00517"></a>00517       }
<a name="l00518"></a>00518     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_factorisation == param.ILU_0)
<a name="l00519"></a>00519       {
<a name="l00520"></a>00520         GetIlu0(A);
<a name="l00521"></a>00521         <span class="keywordflow">return</span>;
<a name="l00522"></a>00522       }
<a name="l00523"></a>00523     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_factorisation == param.MILU_0)
<a name="l00524"></a>00524       {
<a name="l00525"></a>00525         GetMilu0(A);
<a name="l00526"></a>00526         <span class="keywordflow">return</span>;
<a name="l00527"></a>00527       }
<a name="l00528"></a>00528     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (type_factorisation == param.ILU_K)
<a name="l00529"></a>00529       {
<a name="l00530"></a>00530         GetIluk(lfil, A);
<a name="l00531"></a>00531         <span class="keywordflow">return</span>;
<a name="l00532"></a>00532       }
<a name="l00533"></a>00533 
<a name="l00534"></a>00534     cplx fact, s, t;
<a name="l00535"></a>00535     real tnorm;
<a name="l00536"></a>00536     <span class="keywordtype">int</span> length_lower, length_upper, jpos, jrow, i_row, j_col;
<a name="l00537"></a>00537     <span class="keywordtype">int</span> i, j, k, index_lu, length;
<a name="l00538"></a>00538 
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     <span class="keywordflow">if</span> (lfil &lt; 0)
<a name="l00541"></a>00541       {
<a name="l00542"></a>00542         cout &lt;&lt; <span class="stringliteral">&quot;Incorrect fill level.&quot;</span> &lt;&lt; endl;
<a name="l00543"></a>00543         abort();
<a name="l00544"></a>00544       }
<a name="l00545"></a>00545 
<a name="l00546"></a>00546     cplx czero, cone;
<a name="l00547"></a>00547     SetComplexZero(czero);
<a name="l00548"></a>00548     SetComplexOne(cone);
<a name="l00549"></a>00549     <span class="keyword">typedef</span> Vector&lt;cplx, VectFull, Allocator2&gt; VectCplx;
<a name="l00550"></a>00550     VectCplx Row_Val(n);
<a name="l00551"></a>00551     IVect Index(n), Row_Ind(n), Index_Diag(n);
<a name="l00552"></a>00552     Row_Val.Fill(czero);
<a name="l00553"></a>00553     Row_Ind.Fill(-1);
<a name="l00554"></a>00554     Index_Diag.Fill(-1);
<a name="l00555"></a>00555 
<a name="l00556"></a>00556     Index.Fill(-1);
<a name="l00557"></a>00557 
<a name="l00558"></a>00558     <span class="keywordtype">bool</span> element_dropped; cplx dropsum;
<a name="l00559"></a>00559 
<a name="l00560"></a>00560     <span class="comment">// main loop</span>
<a name="l00561"></a>00561     <span class="keywordtype">int</span> new_percent = 0, old_percent = 0;
<a name="l00562"></a>00562     <span class="keywordflow">for</span> (i_row = 0; i_row &lt; n; i_row++)
<a name="l00563"></a>00563       {
<a name="l00564"></a>00564         <span class="comment">// Progress bar if print level is high enough.</span>
<a name="l00565"></a>00565         <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00566"></a>00566           {
<a name="l00567"></a>00567             new_percent = int(<span class="keywordtype">double</span>(i_row)/(n-1)*80);
<a name="l00568"></a>00568             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> percent = old_percent; percent &lt; new_percent; percent++)
<a name="l00569"></a>00569               {
<a name="l00570"></a>00570                 cout &lt;&lt; <span class="stringliteral">&quot;#&quot;</span>; cout.flush();
<a name="l00571"></a>00571               }
<a name="l00572"></a>00572 
<a name="l00573"></a>00573             old_percent = new_percent;
<a name="l00574"></a>00574           }
<a name="l00575"></a>00575 
<a name="l00576"></a>00576         size_row = A.GetRowSize(i_row);
<a name="l00577"></a>00577         tnorm = zero;
<a name="l00578"></a>00578 
<a name="l00579"></a>00579         dropsum = czero;
<a name="l00580"></a>00580         <span class="keywordflow">for</span> (k = 0 ; k &lt; size_row; k++)
<a name="l00581"></a>00581           <span class="keywordflow">if</span> (A.Value(i_row, k) != czero)
<a name="l00582"></a>00582             tnorm += abs(A.Value(i_row, k));
<a name="l00583"></a>00583 
<a name="l00584"></a>00584         <span class="keywordflow">if</span> (tnorm == zero)
<a name="l00585"></a>00585           {
<a name="l00586"></a>00586             cout &lt;&lt; <span class="stringliteral">&quot;Structurally singular matrix.&quot;</span> &lt;&lt; endl;
<a name="l00587"></a>00587             cout &lt;&lt; <span class="stringliteral">&quot;Norm of row &quot;</span> &lt;&lt; i_row &lt;&lt; <span class="stringliteral">&quot; is equal to 0.&quot;</span> &lt;&lt; endl;
<a name="l00588"></a>00588             abort();
<a name="l00589"></a>00589           }
<a name="l00590"></a>00590 
<a name="l00591"></a>00591         <span class="comment">// tnorm is the sum of absolute value of coefficients of row i_row.</span>
<a name="l00592"></a>00592         tnorm /= real(size_row);
<a name="l00593"></a>00593         <span class="keywordflow">if</span> (variable_fill)
<a name="l00594"></a>00594           lfil = size_row + additional_fill;
<a name="l00595"></a>00595 
<a name="l00596"></a>00596         <span class="comment">// Unpack L-part and U-part of row of A.</span>
<a name="l00597"></a>00597         length_upper = 1;
<a name="l00598"></a>00598         length_lower = 0;
<a name="l00599"></a>00599         Row_Ind(i_row) = i_row;
<a name="l00600"></a>00600         Row_Val(i_row) = czero;
<a name="l00601"></a>00601         Index(i_row) = i_row;
<a name="l00602"></a>00602 
<a name="l00603"></a>00603         <span class="keywordflow">for</span> (j = 0; j &lt; size_row; j++)
<a name="l00604"></a>00604           {
<a name="l00605"></a>00605             k = rperm(A.Index(i_row, j));
<a name="l00606"></a>00606 
<a name="l00607"></a>00607             t = A.Value(i_row,j);
<a name="l00608"></a>00608             <span class="keywordflow">if</span> (k &lt; i_row)
<a name="l00609"></a>00609               {
<a name="l00610"></a>00610                 Row_Ind(length_lower) = k;
<a name="l00611"></a>00611                 Row_Val(length_lower) = t;
<a name="l00612"></a>00612                 Index(k) = length_lower;
<a name="l00613"></a>00613                 ++length_lower;
<a name="l00614"></a>00614               }
<a name="l00615"></a>00615             <span class="keywordflow">else</span> <span class="keywordflow">if</span> (k == i_row)
<a name="l00616"></a>00616               {
<a name="l00617"></a>00617                 Row_Val(i_row) = t;
<a name="l00618"></a>00618               }
<a name="l00619"></a>00619             <span class="keywordflow">else</span>
<a name="l00620"></a>00620               {
<a name="l00621"></a>00621                 jpos = i_row + length_upper;
<a name="l00622"></a>00622                 Row_Ind(jpos) = k;
<a name="l00623"></a>00623                 Row_Val(jpos) = t;
<a name="l00624"></a>00624                 Index(k) = jpos;
<a name="l00625"></a>00625                 length_upper++;
<a name="l00626"></a>00626               }
<a name="l00627"></a>00627           }
<a name="l00628"></a>00628 
<a name="l00629"></a>00629         j_col = 0;
<a name="l00630"></a>00630         length = 0;
<a name="l00631"></a>00631 
<a name="l00632"></a>00632         <span class="comment">// Eliminates previous rows.</span>
<a name="l00633"></a>00633         <span class="keywordflow">while</span> (j_col &lt; length_lower)
<a name="l00634"></a>00634           {
<a name="l00635"></a>00635             <span class="comment">// In order to do the elimination in the correct order, we must</span>
<a name="l00636"></a>00636             <span class="comment">// select the smallest column index.</span>
<a name="l00637"></a>00637             jrow = Row_Ind(j_col);
<a name="l00638"></a>00638             k = j_col;
<a name="l00639"></a>00639 
<a name="l00640"></a>00640             <span class="comment">// Determine smallest column index.</span>
<a name="l00641"></a>00641             <span class="keywordflow">for</span> (j = j_col + 1; j &lt; length_lower; j++)
<a name="l00642"></a>00642               {
<a name="l00643"></a>00643                 <span class="keywordflow">if</span> (Row_Ind(j) &lt; jrow)
<a name="l00644"></a>00644                   {
<a name="l00645"></a>00645                     jrow = Row_Ind(j);
<a name="l00646"></a>00646                     k = j;
<a name="l00647"></a>00647                   }
<a name="l00648"></a>00648               }
<a name="l00649"></a>00649 
<a name="l00650"></a>00650             <span class="keywordflow">if</span> (k != j_col)
<a name="l00651"></a>00651               {
<a name="l00652"></a>00652                 <span class="comment">// Exchanging column coefficients.</span>
<a name="l00653"></a>00653                 j = Row_Ind(j_col);
<a name="l00654"></a>00654                 Row_Ind(j_col) = Row_Ind(k);
<a name="l00655"></a>00655                 Row_Ind(k) = j;
<a name="l00656"></a>00656 
<a name="l00657"></a>00657                 Index(jrow) = j_col;
<a name="l00658"></a>00658                 Index(j) = k;
<a name="l00659"></a>00659 
<a name="l00660"></a>00660                 s = Row_Val(j_col);
<a name="l00661"></a>00661                 Row_Val(j_col) = Row_Val(k);
<a name="l00662"></a>00662                 Row_Val(k) = s;
<a name="l00663"></a>00663               }
<a name="l00664"></a>00664 
<a name="l00665"></a>00665             <span class="comment">// Zero out element in row.</span>
<a name="l00666"></a>00666             Index(jrow) = -1;
<a name="l00667"></a>00667 
<a name="l00668"></a>00668             element_dropped = <span class="keyword">false</span>;
<a name="l00669"></a>00669             <span class="keywordflow">if</span> (standard_dropping)
<a name="l00670"></a>00670               <span class="keywordflow">if</span> (abs(Row_Val(j_col)) &lt;= droptol*tnorm)
<a name="l00671"></a>00671                 {
<a name="l00672"></a>00672                   dropsum += Row_Val(j_col);
<a name="l00673"></a>00673                   element_dropped = <span class="keyword">true</span>;
<a name="l00674"></a>00674                 }
<a name="l00675"></a>00675 
<a name="l00676"></a>00676             <span class="comment">// Gets the multiplier for row to be eliminated (jrow).</span>
<a name="l00677"></a>00677             <span class="keywordflow">if</span> (!element_dropped)
<a name="l00678"></a>00678               {
<a name="l00679"></a>00679                 <span class="comment">// first_index_upper points now on the diagonal coefficient.</span>
<a name="l00680"></a>00680                 fact = Row_Val(j_col) * A.Value(jrow, Index_Diag(jrow));
<a name="l00681"></a>00681 
<a name="l00682"></a>00682                 <span class="keywordflow">if</span> (!standard_dropping)
<a name="l00683"></a>00683                   {
<a name="l00684"></a>00684                     <span class="keywordflow">if</span> (abs(fact) &lt;= droptol)
<a name="l00685"></a>00685                       element_dropped = <span class="keyword">true</span>;
<a name="l00686"></a>00686                   }
<a name="l00687"></a>00687               }
<a name="l00688"></a>00688 
<a name="l00689"></a>00689             <span class="keywordflow">if</span> (!element_dropped)
<a name="l00690"></a>00690               {
<a name="l00691"></a>00691                 <span class="comment">// Combines current row and row jrow.</span>
<a name="l00692"></a>00692                 <span class="keywordflow">for</span> (k = (Index_Diag(jrow)+1); k &lt; A.GetRowSize(jrow); k++)
<a name="l00693"></a>00693                   {
<a name="l00694"></a>00694                     s = fact * A.Value(jrow,k);
<a name="l00695"></a>00695                     j = rperm(A.Index(jrow,k));
<a name="l00696"></a>00696 
<a name="l00697"></a>00697                     jpos = Index(j);
<a name="l00698"></a>00698 
<a name="l00699"></a>00699                     <span class="keywordflow">if</span> (j &gt;= i_row)
<a name="l00700"></a>00700                       {
<a name="l00701"></a>00701 
<a name="l00702"></a>00702                         <span class="comment">// Dealing with upper part.</span>
<a name="l00703"></a>00703                         <span class="keywordflow">if</span> (jpos == -1)
<a name="l00704"></a>00704                           {
<a name="l00705"></a>00705                             <span class="comment">// This is a fill-in element.</span>
<a name="l00706"></a>00706                             i = i_row + length_upper;
<a name="l00707"></a>00707                             Row_Ind(i) = j;
<a name="l00708"></a>00708                             Index(j) = i;
<a name="l00709"></a>00709                             Row_Val(i) = -s;
<a name="l00710"></a>00710                             ++length_upper;
<a name="l00711"></a>00711                           }
<a name="l00712"></a>00712                         <span class="keywordflow">else</span>
<a name="l00713"></a>00713                           {
<a name="l00714"></a>00714                             <span class="comment">// This is not a fill-in element.</span>
<a name="l00715"></a>00715                             Row_Val(jpos) -= s;
<a name="l00716"></a>00716                           }
<a name="l00717"></a>00717                       }
<a name="l00718"></a>00718                     <span class="keywordflow">else</span>
<a name="l00719"></a>00719                       {
<a name="l00720"></a>00720                         <span class="comment">// Dealing  with lower part.</span>
<a name="l00721"></a>00721                         <span class="keywordflow">if</span> (jpos == -1)
<a name="l00722"></a>00722                           {
<a name="l00723"></a>00723                             <span class="comment">// this is a fill-in element</span>
<a name="l00724"></a>00724                             Row_Ind(length_lower) = j;
<a name="l00725"></a>00725                             Index(j) = length_lower;
<a name="l00726"></a>00726                             Row_Val(length_lower) = -s;
<a name="l00727"></a>00727                             ++length_lower;
<a name="l00728"></a>00728                           }
<a name="l00729"></a>00729                         <span class="keywordflow">else</span>
<a name="l00730"></a>00730                           {
<a name="l00731"></a>00731                             <span class="comment">// This is not a fill-in element.</span>
<a name="l00732"></a>00732                             Row_Val(jpos) -= s;
<a name="l00733"></a>00733                           }
<a name="l00734"></a>00734                       }
<a name="l00735"></a>00735                   }
<a name="l00736"></a>00736 
<a name="l00737"></a>00737                 <span class="comment">// Stores this pivot element from left to right -- no danger</span>
<a name="l00738"></a>00738                 <span class="comment">// of overlap with the working elements in L (pivots).</span>
<a name="l00739"></a>00739                 Row_Val(length) = fact;
<a name="l00740"></a>00740                 Row_Ind(length) = jrow;
<a name="l00741"></a>00741                 ++length;
<a name="l00742"></a>00742               }
<a name="l00743"></a>00743 
<a name="l00744"></a>00744             j_col++;
<a name="l00745"></a>00745           }
<a name="l00746"></a>00746 
<a name="l00747"></a>00747         <span class="comment">// Resets double-pointer to zero (U-part).</span>
<a name="l00748"></a>00748         <span class="keywordflow">for</span> (k = 0; k &lt; length_upper; k++)
<a name="l00749"></a>00749           Index(Row_Ind(i_row+k )) = -1;
<a name="l00750"></a>00750 
<a name="l00751"></a>00751         <span class="comment">// Updates L-matrix.</span>
<a name="l00752"></a>00752         <span class="keywordflow">if</span> (!standard_dropping)
<a name="l00753"></a>00753           {
<a name="l00754"></a>00754             length_lower = length;
<a name="l00755"></a>00755             length = min(length_lower,lfil);
<a name="l00756"></a>00756 
<a name="l00757"></a>00757             <span class="comment">// Sorts by quick-split.</span>
<a name="l00758"></a>00758             qsplit_ilut(Row_Val, Row_Ind, 0, length_lower, length,tnorm);
<a name="l00759"></a>00759           }
<a name="l00760"></a>00760 
<a name="l00761"></a>00761         size_row = length;
<a name="l00762"></a>00762         A.ReallocateRow(i_row,size_row);
<a name="l00763"></a>00763 
<a name="l00764"></a>00764         <span class="comment">// store L-part</span>
<a name="l00765"></a>00765         index_lu = 0;
<a name="l00766"></a>00766         <span class="keywordflow">for</span> (k = 0 ; k &lt; length ; k++)
<a name="l00767"></a>00767           {
<a name="l00768"></a>00768             A.Value(i_row,index_lu) = Row_Val(k);
<a name="l00769"></a>00769             A.Index(i_row,index_lu) = iperm(Row_Ind(k));
<a name="l00770"></a>00770             ++index_lu;
<a name="l00771"></a>00771           }
<a name="l00772"></a>00772 
<a name="l00773"></a>00773         <span class="comment">// Saves pointer to beginning of row i_row of U.</span>
<a name="l00774"></a>00774         Index_Diag(i_row) = index_lu;
<a name="l00775"></a>00775 
<a name="l00776"></a>00776         <span class="comment">// Updates. U-matrix -- first apply dropping strategy.</span>
<a name="l00777"></a>00777         length = 0;
<a name="l00778"></a>00778         <span class="keywordflow">for</span> (k = 1; k &lt;= (length_upper-1); k++)
<a name="l00779"></a>00779           {
<a name="l00780"></a>00780             <span class="keywordflow">if</span> (abs(Row_Val(i_row+k)) &gt; droptol * tnorm)
<a name="l00781"></a>00781               {
<a name="l00782"></a>00782                 ++length;
<a name="l00783"></a>00783                 Row_Val(i_row+length) = Row_Val(i_row+k);
<a name="l00784"></a>00784                 Row_Ind(i_row+length) = Row_Ind(i_row+k);
<a name="l00785"></a>00785               }
<a name="l00786"></a>00786             <span class="keywordflow">else</span>
<a name="l00787"></a>00787               dropsum += Row_Val(i_row+k);
<a name="l00788"></a>00788           }
<a name="l00789"></a>00789 
<a name="l00790"></a>00790         <span class="keywordflow">if</span> (!standard_dropping)
<a name="l00791"></a>00791           {
<a name="l00792"></a>00792             length_upper = length + 1;
<a name="l00793"></a>00793             length = min(length_upper,lfil);
<a name="l00794"></a>00794 
<a name="l00795"></a>00795             qsplit_ilut(Row_Val, Row_Ind, i_row+1,
<a name="l00796"></a>00796                         i_row+length_upper, i_row+length+1, tnorm);
<a name="l00797"></a>00797           }
<a name="l00798"></a>00798         <span class="keywordflow">else</span>
<a name="l00799"></a>00799           length++;
<a name="l00800"></a>00800 
<a name="l00801"></a>00801         <span class="comment">// Determines next pivot.</span>
<a name="l00802"></a>00802         <span class="keywordtype">int</span> imax = i_row;
<a name="l00803"></a>00803         real xmax = abs(Row_Val(imax));
<a name="l00804"></a>00804         real xmax0 = xmax;
<a name="l00805"></a>00805         <span class="keywordtype">int</span> icut = i_row + mbloc - 1 - i_row%mbloc;
<a name="l00806"></a>00806         <span class="keywordflow">for</span> ( k = i_row + 1; k &lt;= i_row + length - 1; k++)
<a name="l00807"></a>00807           {
<a name="l00808"></a>00808             tnorm = abs(Row_Val(k));
<a name="l00809"></a>00809             <span class="keywordflow">if</span> ((tnorm &gt; xmax) &amp;&amp; (tnorm*permtol &gt; xmax0)
<a name="l00810"></a>00810                 &amp;&amp; (Row_Ind(k)&lt;= icut))
<a name="l00811"></a>00811               {
<a name="l00812"></a>00812                 imax = k;
<a name="l00813"></a>00813                 xmax = tnorm;
<a name="l00814"></a>00814               }
<a name="l00815"></a>00815           }
<a name="l00816"></a>00816 
<a name="l00817"></a>00817         <span class="comment">// Exchanges Row_Val.</span>
<a name="l00818"></a>00818         s = Row_Val(i_row);
<a name="l00819"></a>00819         Row_Val(i_row) = Row_Val(imax);
<a name="l00820"></a>00820         Row_Val(imax) = s;
<a name="l00821"></a>00821 
<a name="l00822"></a>00822         <span class="comment">// Updates iperm and reverses iperm.</span>
<a name="l00823"></a>00823         j = Row_Ind(imax);
<a name="l00824"></a>00824         i = iperm(i_row);
<a name="l00825"></a>00825         iperm(i_row) = iperm(j);
<a name="l00826"></a>00826         iperm(j) = i;
<a name="l00827"></a>00827 
<a name="l00828"></a>00828         <span class="comment">// Reverses iperm.</span>
<a name="l00829"></a>00829         rperm(iperm(i_row)) = i_row;
<a name="l00830"></a>00830         rperm(iperm(j)) = j;
<a name="l00831"></a>00831 
<a name="l00832"></a>00832         <span class="comment">// Copies U-part in original coordinates.</span>
<a name="l00833"></a>00833         <span class="keywordtype">int</span> index_diag = index_lu;
<a name="l00834"></a>00834         A.ResizeRow(i_row, size_row+length);
<a name="l00835"></a>00835 
<a name="l00836"></a>00836         <span class="keywordflow">for</span> (k = i_row ; k &lt;= i_row + length - 1; k++)
<a name="l00837"></a>00837           {
<a name="l00838"></a>00838             A.Index(i_row,index_lu) = iperm(Row_Ind(k));
<a name="l00839"></a>00839             A.Value(i_row,index_lu) = Row_Val(k);
<a name="l00840"></a>00840             ++index_lu;
<a name="l00841"></a>00841           }
<a name="l00842"></a>00842 
<a name="l00843"></a>00843         <span class="comment">// Stores inverse of diagonal element of u.</span>
<a name="l00844"></a>00844         <span class="keywordflow">if</span> (standard_dropping)
<a name="l00845"></a>00845           Row_Val(i_row) += alpha*dropsum;
<a name="l00846"></a>00846 
<a name="l00847"></a>00847         <span class="keywordflow">if</span> (Row_Val(i_row) == czero)
<a name="l00848"></a>00848           Row_Val(i_row) = (droptol + 1e-4) * tnorm;
<a name="l00849"></a>00849 
<a name="l00850"></a>00850         A.Value(i_row, index_diag) = cone / Row_Val(i_row);
<a name="l00851"></a>00851 
<a name="l00852"></a>00852       } <span class="comment">// end main loop</span>
<a name="l00853"></a>00853 
<a name="l00854"></a>00854     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00855"></a>00855       cout &lt;&lt; endl;
<a name="l00856"></a>00856 
<a name="l00857"></a>00857     <span class="keywordflow">if</span> (print_level &gt; 0)
<a name="l00858"></a>00858       cout &lt;&lt; <span class="stringliteral">&quot;The matrix takes &quot;</span> &lt;&lt;
<a name="l00859"></a>00859         int((A.GetDataSize()*(<span class="keyword">sizeof</span>(cplx)+4))/(1024*1024)) &lt;&lt; <span class="stringliteral">&quot; MB&quot;</span> &lt;&lt; endl;
<a name="l00860"></a>00860 
<a name="l00861"></a>00861     <span class="keywordflow">for</span> (i = 0; i &lt; n; i++ )
<a name="l00862"></a>00862       <span class="keywordflow">for</span> (j = 0; j &lt; A.GetRowSize(i); j++)
<a name="l00863"></a>00863         A.Index(i,j) = rperm(A.Index(i,j));
<a name="l00864"></a>00864   }
<a name="l00865"></a>00865 
<a name="l00866"></a>00866 
<a name="l00867"></a>00867   <span class="keyword">template</span>&lt;<span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l00868"></a>00868   <span class="keywordtype">void</span> GetIluk(<span class="keywordtype">int</span> lfil, Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;&amp; A)
<a name="l00869"></a>00869   {
<a name="l00870"></a>00870     <span class="keywordtype">int</span> n = A.GetM();
<a name="l00871"></a>00871     Vector&lt;cplx, VectFull, CallocAlloc&lt;cplx&gt; &gt; w;
<a name="l00872"></a>00872     w.Reallocate(n+1);
<a name="l00873"></a>00873     IVect jw(3*n), Index_Diag(n);
<a name="l00874"></a>00874     Vector&lt;IVect, VectFull, NewAlloc&lt;IVect&gt; &gt; levs(n);
<a name="l00875"></a>00875 
<a name="l00876"></a>00876     cplx czero, cone;
<a name="l00877"></a>00877     SetComplexZero(czero);
<a name="l00878"></a>00878     SetComplexOne(cone);
<a name="l00879"></a>00879 
<a name="l00880"></a>00880     <span class="comment">// Local variables</span>
<a name="l00881"></a>00881     cplx fact, s, t;
<a name="l00882"></a>00882     <span class="keywordtype">int</span> length_lower,length_upper, jpos, jrow, i_row, j_col;
<a name="l00883"></a>00883     <span class="keywordtype">int</span> i, j, k, index_lu, length;
<a name="l00884"></a>00884     <span class="keywordtype">bool</span> element_dropped;
<a name="l00885"></a>00885 
<a name="l00886"></a>00886     <span class="keywordtype">int</span> n2 = 2*n, jlev, k_, size_upper;
<a name="l00887"></a>00887     jw.Fill(-1);
<a name="l00888"></a>00888 
<a name="l00889"></a>00889     <span class="comment">// Main loop.</span>
<a name="l00890"></a>00890     <span class="keywordflow">for</span> (i_row = 0; i_row &lt; n; i_row++)
<a name="l00891"></a>00891       {
<a name="l00892"></a>00892         <span class="keywordtype">int</span> size_row = A.GetRowSize(i_row);
<a name="l00893"></a>00893 
<a name="l00894"></a>00894         <span class="comment">// Unpacks L-part and U-part of row of A in arrays w, jw.</span>
<a name="l00895"></a>00895         length_upper = 1;
<a name="l00896"></a>00896         length_lower = 0;
<a name="l00897"></a>00897         jw(i_row) = i_row;
<a name="l00898"></a>00898         w(i_row) = 0.0;
<a name="l00899"></a>00899         jw(n + i_row) = i_row;
<a name="l00900"></a>00900 
<a name="l00901"></a>00901         <span class="keywordflow">for</span> (j = 0; j &lt; size_row; j++)
<a name="l00902"></a>00902           {
<a name="l00903"></a>00903             k = A.Index(i_row,j);
<a name="l00904"></a>00904             t = A.Value(i_row,j);
<a name="l00905"></a>00905             <span class="keywordflow">if</span> (k &lt; i_row)
<a name="l00906"></a>00906               {
<a name="l00907"></a>00907                 jw(length_lower) = k;
<a name="l00908"></a>00908                 w(length_lower) = t;
<a name="l00909"></a>00909                 jw(n + k) = length_lower;
<a name="l00910"></a>00910                 jw(n2+length_lower) = -1;
<a name="l00911"></a>00911                 ++length_lower;
<a name="l00912"></a>00912               }
<a name="l00913"></a>00913             <span class="keywordflow">else</span> <span class="keywordflow">if</span> (k == i_row)
<a name="l00914"></a>00914               {
<a name="l00915"></a>00915                 w(i_row) = t;
<a name="l00916"></a>00916                 jw(n2+length_lower) = -1;
<a name="l00917"></a>00917               }
<a name="l00918"></a>00918             <span class="keywordflow">else</span>
<a name="l00919"></a>00919               {
<a name="l00920"></a>00920                 jpos = i_row + length_upper;
<a name="l00921"></a>00921                 jw(jpos) = k;
<a name="l00922"></a>00922                 w(jpos) = t;
<a name="l00923"></a>00923                 jw(n + k) = jpos;
<a name="l00924"></a>00924                 length_upper++;
<a name="l00925"></a>00925               }
<a name="l00926"></a>00926           }
<a name="l00927"></a>00927 
<a name="l00928"></a>00928         j_col = 0;
<a name="l00929"></a>00929         length = 0;
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 
<a name="l00932"></a>00932         <span class="comment">// Eliminates previous rows.</span>
<a name="l00933"></a>00933         <span class="keywordflow">while</span> (j_col &lt;length_lower)
<a name="l00934"></a>00934           {
<a name="l00935"></a>00935             <span class="comment">// In order to do the elimination in the correct order, we must</span>
<a name="l00936"></a>00936             <span class="comment">// select the smallest column index among jw(k); k = j_col + 1,</span>
<a name="l00937"></a>00937             <span class="comment">// ..., length_lower.</span>
<a name="l00938"></a>00938             jrow = jw(j_col);
<a name="l00939"></a>00939             k = j_col;
<a name="l00940"></a>00940 
<a name="l00941"></a>00941             <span class="comment">// Determines smallest column index.</span>
<a name="l00942"></a>00942             <span class="keywordflow">for</span> (j = (j_col+1) ; j &lt; length_lower; j++)
<a name="l00943"></a>00943               {
<a name="l00944"></a>00944                 <span class="keywordflow">if</span> (jw(j) &lt; jrow)
<a name="l00945"></a>00945                   {
<a name="l00946"></a>00946                     jrow = jw(j);
<a name="l00947"></a>00947                     k = j;
<a name="l00948"></a>00948                   }
<a name="l00949"></a>00949               }
<a name="l00950"></a>00950 
<a name="l00951"></a>00951             <span class="keywordflow">if</span> (k != j_col)
<a name="l00952"></a>00952               {
<a name="l00953"></a>00953                 <span class="comment">// Exchanges in jw.</span>
<a name="l00954"></a>00954                 j = jw(j_col);
<a name="l00955"></a>00955                 jw(j_col) = jw(k);
<a name="l00956"></a>00956                 jw(k) = j;
<a name="l00957"></a>00957 
<a name="l00958"></a>00958                 <span class="comment">// Exchanges in jw(n+  (pointers/ nonzero indicator).</span>
<a name="l00959"></a>00959                 jw(n+jrow) = j_col;
<a name="l00960"></a>00960                 jw(n+j) = k;
<a name="l00961"></a>00961 
<a name="l00962"></a>00962                 <span class="comment">// Exchanges in jw(n2+  (levels).</span>
<a name="l00963"></a>00963                 j = jw(n2+j_col);
<a name="l00964"></a>00964                 jw(n2+j_col)  = jw(n2+k);
<a name="l00965"></a>00965                 jw(n2+k) = j;
<a name="l00966"></a>00966 
<a name="l00967"></a>00967                 <span class="comment">// Exchanges in w.</span>
<a name="l00968"></a>00968                 s = w(j_col);
<a name="l00969"></a>00969                 w(j_col) = w(k);
<a name="l00970"></a>00970                 w(k) = s;
<a name="l00971"></a>00971               }
<a name="l00972"></a>00972 
<a name="l00973"></a>00973             <span class="comment">// Zero out element in row by setting jw(n+jrow) to zero.</span>
<a name="l00974"></a>00974             jw(n + jrow) = -1;
<a name="l00975"></a>00975 
<a name="l00976"></a>00976             element_dropped = <span class="keyword">false</span>;
<a name="l00977"></a>00977 
<a name="l00978"></a>00978             <span class="comment">// Gets the multiplier for row to be eliminated (jrow).</span>
<a name="l00979"></a>00979             fact = w(j_col) * A.Value(jrow,Index_Diag(jrow));
<a name="l00980"></a>00980 
<a name="l00981"></a>00981             jlev = jw(n2+j_col) + 1;
<a name="l00982"></a>00982             <span class="keywordflow">if</span> (jlev &gt; lfil)
<a name="l00983"></a>00983               element_dropped = <span class="keyword">true</span>;
<a name="l00984"></a>00984 
<a name="l00985"></a>00985             <span class="keywordflow">if</span> (!element_dropped)
<a name="l00986"></a>00986               {
<a name="l00987"></a>00987                 <span class="comment">// Combines current row and row jrow.</span>
<a name="l00988"></a>00988                 k_ = 0;
<a name="l00989"></a>00989                 <span class="keywordflow">for</span> (k = (Index_Diag(jrow)+1); k &lt; A.GetRowSize(jrow) ; k++)
<a name="l00990"></a>00990                   {
<a name="l00991"></a>00991                     s = fact * A.Value(jrow,k);
<a name="l00992"></a>00992                     j = A.Index(jrow,k);
<a name="l00993"></a>00993 
<a name="l00994"></a>00994                     jpos = jw(n + j);
<a name="l00995"></a>00995                     <span class="keywordflow">if</span> (j &gt;= i_row)
<a name="l00996"></a>00996                       {
<a name="l00997"></a>00997                         <span class="comment">// Dealing with upper part.</span>
<a name="l00998"></a>00998                         <span class="keywordflow">if</span> (jpos == -1)
<a name="l00999"></a>00999                           {
<a name="l01000"></a>01000                             <span class="comment">// This is a fill-in element.</span>
<a name="l01001"></a>01001                             i = i_row + length_upper;
<a name="l01002"></a>01002                             jw(i) = j;
<a name="l01003"></a>01003                             jw(n + j) = i;
<a name="l01004"></a>01004                             w(i) = -s;
<a name="l01005"></a>01005 
<a name="l01006"></a>01006                             jw(n2+i) = jlev + levs(jrow)(k_) + 1;
<a name="l01007"></a>01007                             ++length_upper;
<a name="l01008"></a>01008                           }
<a name="l01009"></a>01009                         <span class="keywordflow">else</span>
<a name="l01010"></a>01010                           {
<a name="l01011"></a>01011                             <span class="comment">// This is not a fill-in element.</span>
<a name="l01012"></a>01012                             w(jpos) -= s;
<a name="l01013"></a>01013                             jw(n2+jpos) = min(jw(n2+jpos),
<a name="l01014"></a>01014                                               jlev + levs(jrow)(k_)+1);
<a name="l01015"></a>01015                           }
<a name="l01016"></a>01016                       }
<a name="l01017"></a>01017                     <span class="keywordflow">else</span>
<a name="l01018"></a>01018                       {
<a name="l01019"></a>01019                         <span class="comment">// Dealing  with lower part.</span>
<a name="l01020"></a>01020                         <span class="keywordflow">if</span> (jpos == -1)
<a name="l01021"></a>01021                           {
<a name="l01022"></a>01022                             <span class="comment">// This is a fill-in element.</span>
<a name="l01023"></a>01023                             jw(length_lower) = j;
<a name="l01024"></a>01024                             jw(n + j) = length_lower;
<a name="l01025"></a>01025                             w(length_lower) = -s;
<a name="l01026"></a>01026                             jw(n2+length_lower) = jlev + levs(jrow)(k_) + 1;
<a name="l01027"></a>01027                             ++length_lower;
<a name="l01028"></a>01028                           }
<a name="l01029"></a>01029                         <span class="keywordflow">else</span>
<a name="l01030"></a>01030                           {
<a name="l01031"></a>01031                             <span class="comment">// This is not a fill-in element.</span>
<a name="l01032"></a>01032                             w(jpos) -= s;
<a name="l01033"></a>01033                             jw(n2+jpos) = min(jw(n2 + jpos),
<a name="l01034"></a>01034                                               jlev + levs(jrow)(k_) + 1);
<a name="l01035"></a>01035                           }
<a name="l01036"></a>01036                       }
<a name="l01037"></a>01037 
<a name="l01038"></a>01038                     k_++;
<a name="l01039"></a>01039                   }
<a name="l01040"></a>01040 
<a name="l01041"></a>01041               }
<a name="l01042"></a>01042 
<a name="l01043"></a>01043             <span class="comment">// Stores this pivot element from left to right -- no danger of</span>
<a name="l01044"></a>01044             <span class="comment">// overlap with the working elements in L (pivots).</span>
<a name="l01045"></a>01045             w(j_col) = fact;
<a name="l01046"></a>01046             jw(j_col) = jrow;
<a name="l01047"></a>01047 
<a name="l01048"></a>01048             j_col++;
<a name="l01049"></a>01049           }
<a name="l01050"></a>01050 
<a name="l01051"></a>01051         <span class="comment">// Resets double-pointer to zero (U-part).</span>
<a name="l01052"></a>01052         <span class="keywordflow">for</span> (k = 0; k &lt; length_upper; k++)
<a name="l01053"></a>01053           jw(n + jw(i_row + k )) = -1;
<a name="l01054"></a>01054 
<a name="l01055"></a>01055         <span class="comment">// Updates L-matrix.</span>
<a name="l01056"></a>01056         size_row = 1; <span class="comment">// we have the diagonal value.</span>
<a name="l01057"></a>01057         <span class="comment">// Size of L-matrix.</span>
<a name="l01058"></a>01058         <span class="keywordflow">for</span> (k = 0; k &lt; length_lower; k++)
<a name="l01059"></a>01059           <span class="keywordflow">if</span> (jw(n2+k) &lt; lfil)
<a name="l01060"></a>01060             size_row++;
<a name="l01061"></a>01061 
<a name="l01062"></a>01062         <span class="comment">// Size of U-matrix.</span>
<a name="l01063"></a>01063         size_upper = 0;
<a name="l01064"></a>01064         <span class="keywordflow">for</span> (k = (i_row+1) ; k &lt;= (i_row+length_upper-1) ; k++)
<a name="l01065"></a>01065           <span class="keywordflow">if</span> (jw(n2+k) &lt; lfil)
<a name="l01066"></a>01066             size_upper++;
<a name="l01067"></a>01067 
<a name="l01068"></a>01068         size_row += size_upper;
<a name="l01069"></a>01069         A.ReallocateRow(i_row,size_row);
<a name="l01070"></a>01070         levs(i_row).Reallocate(size_upper);
<a name="l01071"></a>01071 
<a name="l01072"></a>01072         index_lu = 0;
<a name="l01073"></a>01073         <span class="keywordflow">for</span> (k = 0; k &lt; length_lower; k++)
<a name="l01074"></a>01074           {
<a name="l01075"></a>01075             <span class="keywordflow">if</span> (jw(n2+k) &lt; lfil)
<a name="l01076"></a>01076               {
<a name="l01077"></a>01077                 A.Value(i_row,index_lu) = w(k);
<a name="l01078"></a>01078                 A.Index(i_row,index_lu) = jw(k);
<a name="l01079"></a>01079                 ++index_lu;
<a name="l01080"></a>01080               }
<a name="l01081"></a>01081           }
<a name="l01082"></a>01082 
<a name="l01083"></a>01083         <span class="comment">// Saves pointer to beginning of row i_row of U.</span>
<a name="l01084"></a>01084         Index_Diag(i_row) = index_lu;
<a name="l01085"></a>01085         A.Value(i_row,index_lu) = cone / w(i_row);
<a name="l01086"></a>01086         A.Index(i_row,index_lu++) = i_row;
<a name="l01087"></a>01087 
<a name="l01088"></a>01088         <span class="keywordflow">for</span> (k = (i_row+1) ; k &lt;= (i_row+length_upper-1) ; k++)
<a name="l01089"></a>01089           {
<a name="l01090"></a>01090             <span class="keywordflow">if</span> (jw(n2+k) &lt; lfil)
<a name="l01091"></a>01091               {
<a name="l01092"></a>01092                 A.Index(i_row,index_lu) = jw(k);
<a name="l01093"></a>01093                 A.Value(i_row,index_lu) = w(k);
<a name="l01094"></a>01094                 levs(i_row)(index_lu-Index_Diag(i_row)-1) = jw(n2+k);
<a name="l01095"></a>01095                 ++index_lu;
<a name="l01096"></a>01096               }
<a name="l01097"></a>01097           }
<a name="l01098"></a>01098       }
<a name="l01099"></a>01099   }
<a name="l01100"></a>01100 
<a name="l01101"></a>01101 
<a name="l01102"></a>01102   <span class="keyword">template</span>&lt;<span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l01103"></a>01103   <span class="keywordtype">void</span> GetIlu0(Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;&amp; A)
<a name="l01104"></a>01104   {
<a name="l01105"></a>01105     <span class="keywordtype">int</span> j_col, jrow, jw, n = A.GetM();
<a name="l01106"></a>01106     IVect Index(n), ju(n);
<a name="l01107"></a>01107 
<a name="l01108"></a>01108     cplx czero, cone;
<a name="l01109"></a>01109     SetComplexZero(czero);
<a name="l01110"></a>01110     SetComplexOne(cone);
<a name="l01111"></a>01111 
<a name="l01112"></a>01112     <span class="comment">// Initializes work vector to zero&#39;s.</span>
<a name="l01113"></a>01113     Index.Fill(-1); ju.Fill(-1);
<a name="l01114"></a>01114     cplx tl;
<a name="l01115"></a>01115 
<a name="l01116"></a>01116     <span class="comment">// Main loop.</span>
<a name="l01117"></a>01117     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_row = 0 ; i_row &lt; n ; i_row++)
<a name="l01118"></a>01118       {
<a name="l01119"></a>01119         <span class="comment">// Generating row number i_row of L and U.</span>
<a name="l01120"></a>01120         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0 ; j &lt; A.GetRowSize(i_row) ; j++ )
<a name="l01121"></a>01121           {
<a name="l01122"></a>01122             j_col = A.Index(i_row, j);
<a name="l01123"></a>01123             <span class="keywordflow">if</span> (j_col == i_row)
<a name="l01124"></a>01124               ju(i_row) = j;
<a name="l01125"></a>01125 
<a name="l01126"></a>01126             Index(j_col) = j;
<a name="l01127"></a>01127           }
<a name="l01128"></a>01128 
<a name="l01129"></a>01129         <span class="keywordtype">int</span> jm = ju(i_row)-1;
<a name="l01130"></a>01130 
<a name="l01131"></a>01131         <span class="comment">// Exit if diagonal element is reached.</span>
<a name="l01132"></a>01132         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt;= jm; j++)
<a name="l01133"></a>01133           {
<a name="l01134"></a>01134             jrow = A.Index(i_row, j);
<a name="l01135"></a>01135             tl = A.Value(i_row, j)*A.Value(jrow, ju(jrow));
<a name="l01136"></a>01136             A.Value(i_row, j) = tl;
<a name="l01137"></a>01137 
<a name="l01138"></a>01138             <span class="comment">// Performs linear combination.</span>
<a name="l01139"></a>01139             <span class="keywordflow">for</span> ( j_col = (ju(jrow)+1); j_col &lt; A.GetRowSize(jrow); j_col++)
<a name="l01140"></a>01140               {
<a name="l01141"></a>01141                 jw = Index(A.Index(jrow,j_col));
<a name="l01142"></a>01142                 <span class="keywordflow">if</span> (jw != -1)
<a name="l01143"></a>01143                   A.Value(i_row, jw) -= tl*A.Value(jrow, j_col);
<a name="l01144"></a>01144               }
<a name="l01145"></a>01145           }
<a name="l01146"></a>01146 
<a name="l01147"></a>01147 
<a name="l01148"></a>01148         <span class="comment">// Inverts and stores diagonal element.</span>
<a name="l01149"></a>01149         <span class="keywordflow">if</span> (A.Value(i_row, ju(i_row)) == czero)
<a name="l01150"></a>01150           {
<a name="l01151"></a>01151             cout &lt;&lt; <span class="stringliteral">&quot;Factorization fails because we found a null coefficient&quot;</span>
<a name="l01152"></a>01152                  &lt;&lt; <span class="stringliteral">&quot; on diagonal &quot;</span> &lt;&lt; i_row &lt;&lt; endl;
<a name="l01153"></a>01153             abort();
<a name="l01154"></a>01154           }
<a name="l01155"></a>01155 
<a name="l01156"></a>01156         A.Value(i_row,ju(i_row)) = cone / A.Value(i_row,ju(i_row));
<a name="l01157"></a>01157 
<a name="l01158"></a>01158         <span class="comment">// Resets pointer Index to zero.</span>
<a name="l01159"></a>01159         Index(i_row) = -1;
<a name="l01160"></a>01160         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetRowSize(i_row); i++)
<a name="l01161"></a>01161           Index(A.Index(i_row, i)) = -1;
<a name="l01162"></a>01162       }
<a name="l01163"></a>01163 
<a name="l01164"></a>01164   }
<a name="l01165"></a>01165 
<a name="l01166"></a>01166 
<a name="l01167"></a>01167   <span class="keyword">template</span>&lt;<span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator&gt;
<a name="l01168"></a>01168   <span class="keywordtype">void</span> GetMilu0(Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;&amp; A)
<a name="l01169"></a>01169   {
<a name="l01170"></a>01170     <span class="keywordtype">int</span> j_col, jrow, jw, n = A.GetM();
<a name="l01171"></a>01171     IVect Index(n), ju(n);
<a name="l01172"></a>01172 
<a name="l01173"></a>01173     cplx czero, cone;
<a name="l01174"></a>01174     SetComplexZero(czero);
<a name="l01175"></a>01175     SetComplexOne(cone);
<a name="l01176"></a>01176 
<a name="l01177"></a>01177     <span class="comment">// Initializes work vector to zero&#39;s.</span>
<a name="l01178"></a>01178     Index.Fill(-1); ju.Fill(-1);
<a name="l01179"></a>01179     cplx tl;
<a name="l01180"></a>01180 
<a name="l01181"></a>01181     <span class="comment">// Main loop.</span>
<a name="l01182"></a>01182     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i_row = 0 ; i_row &lt; n ; i_row++)
<a name="l01183"></a>01183       {
<a name="l01184"></a>01184         <span class="comment">// Generating row number i_row of L and U.</span>
<a name="l01185"></a>01185         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i_row); j++ )
<a name="l01186"></a>01186           {
<a name="l01187"></a>01187             j_col = A.Index(i_row, j);
<a name="l01188"></a>01188             <span class="keywordflow">if</span> (j_col == i_row)
<a name="l01189"></a>01189               ju(i_row) = j;
<a name="l01190"></a>01190 
<a name="l01191"></a>01191             Index(j_col) = j;
<a name="l01192"></a>01192           }
<a name="l01193"></a>01193 
<a name="l01194"></a>01194         <span class="keywordtype">int</span> jm = ju(i_row)-1;
<a name="l01195"></a>01195         <span class="comment">// Exit if diagonal element is reached.</span>
<a name="l01196"></a>01196         <span class="comment">// s accumulates fill-in values.</span>
<a name="l01197"></a>01197         cplx s(0);
<a name="l01198"></a>01198         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt;= jm; j++)
<a name="l01199"></a>01199           {
<a name="l01200"></a>01200             jrow = A.Index(i_row, j);
<a name="l01201"></a>01201             tl = A.Value(i_row, j)*A.Value(jrow, ju(jrow));
<a name="l01202"></a>01202             A.Value(i_row, j) = tl;
<a name="l01203"></a>01203 
<a name="l01204"></a>01204             <span class="comment">// Performs linear combination.</span>
<a name="l01205"></a>01205             <span class="keywordflow">for</span> ( j_col = (ju(jrow)+1); j_col &lt; A.GetRowSize(jrow); j_col++ )
<a name="l01206"></a>01206               {
<a name="l01207"></a>01207                 jw = Index(A.Index(jrow, j_col));
<a name="l01208"></a>01208                 <span class="keywordflow">if</span> (jw != -1)
<a name="l01209"></a>01209                   A.Value(i_row, jw) -= tl*A.Value(jrow, j_col);
<a name="l01210"></a>01210                 <span class="keywordflow">else</span>
<a name="l01211"></a>01211                   s += tl*A.Value(jrow, j_col);
<a name="l01212"></a>01212               }
<a name="l01213"></a>01213           }
<a name="l01214"></a>01214 
<a name="l01215"></a>01215         <span class="comment">// Inverts and stores diagonal element.</span>
<a name="l01216"></a>01216         A.Value(i_row, ju(i_row)) -= s;
<a name="l01217"></a>01217         <span class="keywordflow">if</span> (A.Value(i_row, ju(i_row)) == czero)
<a name="l01218"></a>01218           {
<a name="l01219"></a>01219             cout &lt;&lt; <span class="stringliteral">&quot;Factorization fails because we found a null coefficient&quot;</span>
<a name="l01220"></a>01220                  &lt;&lt; <span class="stringliteral">&quot; on diagonal &quot;</span> &lt;&lt; i_row &lt;&lt; endl;
<a name="l01221"></a>01221             abort();
<a name="l01222"></a>01222           }
<a name="l01223"></a>01223 
<a name="l01224"></a>01224         A.Value(i_row, ju(i_row)) = cone /A.Value(i_row, ju(i_row));
<a name="l01225"></a>01225 
<a name="l01226"></a>01226         <span class="comment">// Resets pointer Index to zero.</span>
<a name="l01227"></a>01227         Index(i_row) = -1;
<a name="l01228"></a>01228         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetRowSize(i_row); i++)
<a name="l01229"></a>01229           Index(A.Index(i_row, i)) = -1;
<a name="l01230"></a>01230       }
<a name="l01231"></a>01231   }
<a name="l01232"></a>01232 
<a name="l01233"></a>01233 
<a name="l01235"></a>01235 
<a name="l01238"></a><a class="code" href="namespace_seldon.php#a089ff611296e04262f0de1581c314765">01238</a>   <span class="keyword">template</span>&lt;<span class="keyword">class </span>real, <span class="keyword">class </span>cplx, <span class="keyword">class </span>Allocator,
<a name="l01239"></a>01239            <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01240"></a>01240   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;real, General, ArrayRowSparse, Allocator&gt;</a>&amp; A,
<a name="l01241"></a>01241                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;cplx, Storage2, Allocator2&gt;</a>&amp; x)
<a name="l01242"></a>01242   {
<a name="l01243"></a>01243     <span class="keywordtype">int</span> i, k, n, k_;
<a name="l01244"></a>01244     real inv_diag;
<a name="l01245"></a>01245     n = A.GetM();
<a name="l01246"></a>01246 
<a name="l01247"></a>01247     <span class="comment">// Forward solve.</span>
<a name="l01248"></a>01248     <span class="keywordflow">for</span> (i = 0; i &lt; n; i++)
<a name="l01249"></a>01249       {
<a name="l01250"></a>01250         k_ = 0; k = A.Index(i,k_);
<a name="l01251"></a>01251         <span class="keywordflow">while</span> ( k &lt; i)
<a name="l01252"></a>01252           {
<a name="l01253"></a>01253             x(i) -= A.Value(i,k_)*x(k);
<a name="l01254"></a>01254             k_++;
<a name="l01255"></a>01255             k = A.Index(i,k_);
<a name="l01256"></a>01256           }
<a name="l01257"></a>01257       }
<a name="l01258"></a>01258 
<a name="l01259"></a>01259     <span class="comment">// Backward solve.</span>
<a name="l01260"></a>01260     <span class="keywordflow">for</span> (i = n-1; i &gt;= 0; i--)
<a name="l01261"></a>01261       {
<a name="l01262"></a>01262         k_ = 0; k = A.Index(i,k_);
<a name="l01263"></a>01263         <span class="keywordflow">while</span> ( k &lt; i)
<a name="l01264"></a>01264           {
<a name="l01265"></a>01265             k_++;
<a name="l01266"></a>01266             k = A.Index(i,k_);
<a name="l01267"></a>01267           }
<a name="l01268"></a>01268 
<a name="l01269"></a>01269         inv_diag = A.Value(i,k_);
<a name="l01270"></a>01270         <span class="keywordflow">for</span> ( k = (k_+1); k &lt; A.GetRowSize(i) ; k++)
<a name="l01271"></a>01271           x(i) -= A.Value(i,k) * x(A.Index(i,k));
<a name="l01272"></a>01272 
<a name="l01273"></a>01273         x(i) *= inv_diag;
<a name="l01274"></a>01274       }
<a name="l01275"></a>01275   }
<a name="l01276"></a>01276 
<a name="l01277"></a>01277 
<a name="l01278"></a>01278   <span class="keyword">template</span>&lt;<span class="keyword">class</span> cplx, <span class="keyword">class</span> Allocator, <span class="keyword">class</span> Storage2, <span class="keyword">class</span> Allocator2&gt;
<a name="l01279"></a>01279   <span class="keywordtype">void</span> SolveLU(<span class="keyword">const</span> class_SeldonTrans&amp; transA,
<a name="l01280"></a>01280                <span class="keyword">const</span> Matrix&lt;cplx, General, ArrayRowSparse, Allocator&gt;&amp; A,
<a name="l01281"></a>01281                Vector&lt;cplx,Storage2,Allocator2&gt;&amp; x)
<a name="l01282"></a>01282   {
<a name="l01283"></a>01283     <span class="keywordtype">int</span> i, k, n, k_;
<a name="l01284"></a>01284     n = A.GetM();
<a name="l01285"></a>01285 
<a name="l01286"></a>01286     <span class="comment">// Forward solve (with U^T).</span>
<a name="l01287"></a>01287     <span class="keywordflow">for</span> (i = 0 ; i &lt; n ; i++)
<a name="l01288"></a>01288       {
<a name="l01289"></a>01289         k_ = 0; k = A.Index(i,k_);
<a name="l01290"></a>01290         <span class="keywordflow">while</span> ( k &lt; i)
<a name="l01291"></a>01291           {
<a name="l01292"></a>01292             k_++;
<a name="l01293"></a>01293             k = A.Index(i,k_);
<a name="l01294"></a>01294           }
<a name="l01295"></a>01295 
<a name="l01296"></a>01296         x(i) *= A.Value(i,k_);
<a name="l01297"></a>01297         <span class="keywordflow">for</span> ( k = (k_+1); k &lt; A.GetRowSize(i) ; k++)
<a name="l01298"></a>01298           {
<a name="l01299"></a>01299             x(A.Index(i,k)) -= A.Value(i,k) * x(i);
<a name="l01300"></a>01300           }
<a name="l01301"></a>01301       }
<a name="l01302"></a>01302 
<a name="l01303"></a>01303     <span class="comment">// Backward solve (with L^T).</span>
<a name="l01304"></a>01304     <span class="keywordflow">for</span> (i = n-1 ; i&gt;=0  ; i--)
<a name="l01305"></a>01305       {
<a name="l01306"></a>01306         k_ = 0; k = A.Index(i,k_);
<a name="l01307"></a>01307         <span class="keywordflow">while</span> ( k &lt; i)
<a name="l01308"></a>01308           {
<a name="l01309"></a>01309             x(k) -= A.Value(i,k_)*x(i);
<a name="l01310"></a>01310             k_++;
<a name="l01311"></a>01311             k = A.Index(i,k_);
<a name="l01312"></a>01312           }
<a name="l01313"></a>01313       }
<a name="l01314"></a>01314   }
<a name="l01315"></a>01315 
<a name="l01316"></a>01316 }
<a name="l01317"></a>01317 
<a name="l01318"></a>01318 <span class="preprocessor">#define SELDON_FILE_ILUT_PRECONDITIONING_CXX</span>
<a name="l01319"></a>01319 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
