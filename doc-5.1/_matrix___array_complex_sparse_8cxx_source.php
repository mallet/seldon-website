<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ArrayComplexSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_ArrayComplexSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">  Matrix_ArrayComplexSparse</a>()
<a name="l00041"></a>00041     : val_real_(), val_imag_()
<a name="l00042"></a>00042   {
<a name="l00043"></a>00043     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00044"></a>00044     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00045"></a>00045   }
<a name="l00046"></a>00046 
<a name="l00047"></a>00047 
<a name="l00049"></a>00049 
<a name="l00054"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a410f415f4976918fc7678bc770611043">00054</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00055"></a>00055   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00056"></a>00056 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a415bb8c2cb2379647933ddc5d9dbcc32" title="Default constructor.">  Matrix_ArrayComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00057"></a>00057     val_real_(Storage::GetFirst(i, j)), val_imag_(Storage::GetFirst(i, j))
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00060"></a>00060     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00061"></a>00061   }
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00064"></a>00064   <span class="comment">/**************</span>
<a name="l00065"></a>00065 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00066"></a>00066 <span class="comment">   **************/</span>
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 
<a name="l00070"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0">00070</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00071"></a>00071   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00072"></a>00072 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">  ~Matrix_ArrayComplexSparse</a>()
<a name="l00073"></a>00073   {
<a name="l00074"></a>00074     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00075"></a>00075     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00076"></a>00076   }
<a name="l00077"></a>00077 
<a name="l00078"></a>00078 
<a name="l00080"></a>00080 
<a name="l00083"></a>00083   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00084"></a>00084   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00085"></a>00085   {
<a name="l00086"></a>00086     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acec6f5d4aa6488c9389a2065944568b0" title="Destructor.">~Matrix_ArrayComplexSparse</a>();
<a name="l00087"></a>00087   }
<a name="l00088"></a>00088 
<a name="l00089"></a>00089 
<a name="l00090"></a>00090   <span class="comment">/*********************</span>
<a name="l00091"></a>00091 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00092"></a>00092 <span class="comment">   *********************/</span>
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 
<a name="l00096"></a>00096 
<a name="l00102"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287">00102</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00103"></a>00103   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287" title="Reallocates memory to resize the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00104"></a>00104 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad2348fb25206ef8516c7e835b7ce2287" title="Reallocates memory to resize the matrix.">  Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00105"></a>00105   {
<a name="l00106"></a>00106     <span class="comment">// Clears previous entries.</span>
<a name="l00107"></a>00107     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0ee918c8c04a1a9434e157c15fdeb596" title="Clears the matrix.">Clear</a>();
<a name="l00108"></a>00108 
<a name="l00109"></a>00109     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00110"></a>00110     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00111"></a>00111 
<a name="l00112"></a>00112     <span class="keywordtype">int</span> n = Storage::GetFirst(i,j);
<a name="l00113"></a>00113     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.Reallocate(n);
<a name="l00114"></a>00114     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.Reallocate(n);
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00119"></a>00119 
<a name="l00125"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f">00125</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00126"></a>00126   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f" title="Reallocates additional memory to resize the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00127"></a>00127 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a9166689334cc2f731220f76b730d692f" title="Reallocates additional memory to resize the matrix.">  Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00128"></a>00128   {
<a name="l00129"></a>00129     <span class="keywordtype">int</span> n = Storage::GetFirst(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>, <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>);
<a name="l00130"></a>00130     <span class="keywordtype">int</span> new_n = Storage::GetFirst(i, j);
<a name="l00131"></a>00131     <span class="keywordflow">if</span> (n != new_n)
<a name="l00132"></a>00132       {
<a name="l00133"></a>00133         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,
<a name="l00134"></a>00134           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val_real;
<a name="l00135"></a>00135 
<a name="l00136"></a>00136         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, VectSparse, Allocator&gt;</a>, VectFull,
<a name="l00137"></a>00137           <a class="code" href="class_seldon_1_1_new_alloc.php">NewAlloc&lt;Vector&lt;T, VectSparse, Allocator&gt;</a> &gt; &gt; new_val_imag;
<a name="l00138"></a>00138 
<a name="l00139"></a>00139         new_val_real.Reallocate(new_n);
<a name="l00140"></a>00140         new_val_imag.Reallocate(new_n);
<a name="l00141"></a>00141 
<a name="l00142"></a>00142         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0 ; k &lt; min(n, new_n) ; k++)
<a name="l00143"></a>00143           {
<a name="l00144"></a>00144             Swap(new_val_real(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(k));
<a name="l00145"></a>00145             Swap(new_val_imag(k), this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(k));
<a name="l00146"></a>00146           }
<a name="l00147"></a>00147 
<a name="l00148"></a>00148         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.SetData(new_n, new_val_real.GetData());
<a name="l00149"></a>00149         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.SetData(new_n, new_val_imag.GetData());
<a name="l00150"></a>00150         new_val_real.Nullify();
<a name="l00151"></a>00151         new_val_imag.Nullify();
<a name="l00152"></a>00152 
<a name="l00153"></a>00153       }
<a name="l00154"></a>00154 
<a name="l00155"></a>00155     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = i;
<a name="l00156"></a>00156     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = j;
<a name="l00157"></a>00157   }
<a name="l00158"></a>00158 
<a name="l00159"></a>00159 
<a name="l00160"></a>00160   <span class="comment">/*******************</span>
<a name="l00161"></a>00161 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00162"></a>00162 <span class="comment">   *******************/</span>
<a name="l00163"></a>00163 
<a name="l00164"></a>00164 
<a name="l00166"></a>00166 
<a name="l00169"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821">00169</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00170"></a>00170   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00171"></a>00171 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">  ::GetM</a>()<span class="keyword"> const</span>
<a name="l00172"></a>00172 <span class="keyword">  </span>{
<a name="l00173"></a>00173     <span class="keywordflow">return</span> m_;
<a name="l00174"></a>00174   }
<a name="l00175"></a>00175 
<a name="l00176"></a>00176 
<a name="l00178"></a>00178 
<a name="l00181"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1">00181</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00182"></a>00182   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00183"></a>00183 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">  ::GetN</a>()<span class="keyword"> const</span>
<a name="l00184"></a>00184 <span class="keyword">  </span>{
<a name="l00185"></a>00185     <span class="keywordflow">return</span> n_;
<a name="l00186"></a>00186   }
<a name="l00187"></a>00187 
<a name="l00188"></a>00188 
<a name="l00190"></a>00190 
<a name="l00194"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab3c6aad66c9dc6ad9addfb7de9a4ebdd">00194</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00195"></a>00195   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00196"></a>00196 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">  ::GetM</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00197"></a>00197 <span class="keyword">  </span>{
<a name="l00198"></a>00198     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00199"></a>00199       <span class="keywordflow">return</span> m_;
<a name="l00200"></a>00200     <span class="keywordflow">else</span>
<a name="l00201"></a>00201       <span class="keywordflow">return</span> n_;
<a name="l00202"></a>00202   }
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 
<a name="l00206"></a>00206 
<a name="l00210"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a53e20518a8557211c89ca8ad0d607825">00210</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00211"></a>00211   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00212"></a>00212 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ae413915ed95545b40df8b43737fa43c1" title="Returns the number of columns.">  ::GetN</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; status)<span class="keyword"> const</span>
<a name="l00213"></a>00213 <span class="keyword">  </span>{
<a name="l00214"></a>00214     <span class="keywordflow">if</span> (status.NoTrans())
<a name="l00215"></a>00215       <span class="keywordflow">return</span> n_;
<a name="l00216"></a>00216     <span class="keywordflow">else</span>
<a name="l00217"></a>00217       <span class="keywordflow">return</span> m_;
<a name="l00218"></a>00218   }
<a name="l00219"></a>00219 
<a name="l00220"></a>00220 
<a name="l00222"></a>00222 
<a name="l00225"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958">00225</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00226"></a>00226   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00227"></a>00227 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">  GetRealNonZeros</a>()<span class="keyword"> const</span>
<a name="l00228"></a>00228 <span class="keyword">  </span>{
<a name="l00229"></a>00229     <span class="keywordtype">int</span> nnz = 0;
<a name="l00230"></a>00230     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.GetM(); i++)
<a name="l00231"></a>00231       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM();
<a name="l00232"></a>00232 
<a name="l00233"></a>00233     <span class="keywordflow">return</span> nnz;
<a name="l00234"></a>00234   }
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00238"></a>00238 
<a name="l00241"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be">00241</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00242"></a>00242   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00243"></a>00243 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">  GetImagNonZeros</a>()<span class="keyword"> const</span>
<a name="l00244"></a>00244 <span class="keyword">  </span>{
<a name="l00245"></a>00245     <span class="keywordtype">int</span> nnz = 0;
<a name="l00246"></a>00246     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.GetM(); i++)
<a name="l00247"></a>00247       nnz += this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM();
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     <span class="keywordflow">return</span> nnz;
<a name="l00250"></a>00250   }
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 
<a name="l00254"></a>00254 
<a name="l00259"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694">00259</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00260"></a>00260   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694" title="Returns the number of elements stored in memory (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00261"></a>00261 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa40775992c738c7fd1e9543bfddb2694" title="Returns the number of elements stored in memory (real part).">  GetRealDataSize</a>()<span class="keyword"> const</span>
<a name="l00262"></a>00262 <span class="keyword">  </span>{
<a name="l00263"></a>00263     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">GetRealNonZeros</a>();
<a name="l00264"></a>00264   }
<a name="l00265"></a>00265 
<a name="l00266"></a>00266 
<a name="l00268"></a>00268 
<a name="l00273"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340">00273</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00274"></a>00274   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340" title="Returns the number of elements stored in memory (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00275"></a>00275 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad54321edab8e2633985e198f1a576340" title="Returns the number of elements stored in memory (imaginary part).">  GetImagDataSize</a>()<span class="keyword"> const</span>
<a name="l00276"></a>00276 <span class="keyword">  </span>{
<a name="l00277"></a>00277     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">GetImagNonZeros</a>();
<a name="l00278"></a>00278   }
<a name="l00279"></a>00279 
<a name="l00280"></a>00280 
<a name="l00282"></a>00282 
<a name="l00287"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f">00287</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00288"></a>00288   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f" title="Returns the number of elements stored in memory (real+imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00289"></a>00289 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2e5c6c10af220eea29f9c1565b14a93f" title="Returns the number of elements stored in memory (real+imaginary part).">  GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00290"></a>00290 <span class="keyword">  </span>{
<a name="l00291"></a>00291     <span class="keywordflow">return</span> (<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11efc1bee6a95da705238628d7e3d958" title="Returns the number of non-zero elements (real part).">GetRealNonZeros</a>()+<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a33ebac042eda90796d7f674c250e85be" title="Returns the number of non-zero elements (imaginary part).">GetImagNonZeros</a>());
<a name="l00292"></a>00292   }
<a name="l00293"></a>00293 
<a name="l00294"></a>00294 
<a name="l00296"></a>00296 
<a name="l00301"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6">00301</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00302"></a>00302   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6" title="Returns column indices of non-zero entries in row (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00303"></a>00303 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afae45969eda952b43748cf7756f0e6d6" title="Returns column indices of non-zero entries in row (real part).">  GetRealInd</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00304"></a>00304 <span class="keyword">  </span>{
<a name="l00305"></a>00305     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetIndex();
<a name="l00306"></a>00306   }
<a name="l00307"></a>00307 
<a name="l00308"></a>00308 
<a name="l00310"></a>00310 
<a name="l00314"></a>00314   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span> T*
<a name="l00315"></a>00315   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealData</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00316"></a>00316 <span class="keyword">    const</span>
<a name="l00317"></a>00317 <span class="keyword">  </span>{
<a name="l00318"></a>00318     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetData();
<a name="l00319"></a>00319   }
<a name="l00320"></a>00320 
<a name="l00321"></a>00321 
<a name="l00323"></a>00323 
<a name="l00328"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca">00328</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00329"></a>00329   <span class="keyword">inline</span> <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca" title="Returns column indices of non-zero entries in row (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00330"></a>00330 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a0a423d7e93a0ad9c19ed6de2b1052dca" title="Returns column indices of non-zero entries in row (imaginary part).">  GetImagInd</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00331"></a>00331 <span class="keyword">  </span>{
<a name="l00332"></a>00332     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetIndex();
<a name="l00333"></a>00333   }
<a name="l00334"></a>00334 
<a name="l00335"></a>00335 
<a name="l00337"></a>00337 
<a name="l00341"></a>00341   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span> T*
<a name="l00342"></a>00342   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagData</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l00343"></a>00343 <span class="keyword">    const</span>
<a name="l00344"></a>00344 <span class="keyword">  </span>{
<a name="l00345"></a>00345     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetData();
<a name="l00346"></a>00346   }
<a name="l00347"></a>00347 
<a name="l00348"></a>00348 
<a name="l00349"></a>00349   <span class="comment">/**********************************</span>
<a name="l00350"></a>00350 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00351"></a>00351 <span class="comment">   **********************************/</span>
<a name="l00352"></a>00352 
<a name="l00353"></a>00353 
<a name="l00355"></a>00355 
<a name="l00361"></a>00361   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00362"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">00362</a>   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l00363"></a>00363   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::operator</a>()
<a name="l00364"></a>00364     (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>
<a name="l00365"></a>00365   {
<a name="l00366"></a>00366 
<a name="l00367"></a>00367 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00368"></a>00368 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00369"></a>00369       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00370"></a>00370                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00371"></a>00371                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00372"></a>00372 
<a name="l00373"></a>00373     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00374"></a>00374       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00375"></a>00375                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00376"></a>00376                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00377"></a>00377 <span class="preprocessor">#endif</span>
<a name="l00378"></a>00378 <span class="preprocessor"></span>
<a name="l00379"></a>00379     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(Storage::GetFirst(i, j))
<a name="l00380"></a>00380                       (Storage::GetSecond(i, j)),
<a name="l00381"></a>00381                       this-&gt;val_imag_(Storage::GetFirst(i, j))
<a name="l00382"></a>00382                       (Storage::GetSecond(i, j)) );
<a name="l00383"></a>00383   }
<a name="l00384"></a>00384 
<a name="l00385"></a>00385 
<a name="l00387"></a>00387 
<a name="l00393"></a>00393   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00394"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e">00394</a>   <span class="keyword">inline</span> complex&lt;T&gt;&amp;
<a name="l00395"></a>00395   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00396"></a>00396 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00397"></a>00397   {
<a name="l00398"></a>00398     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l00399"></a>00399   }
<a name="l00400"></a>00400 
<a name="l00402"></a>00402 
<a name="l00408"></a>00408   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00409"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a994fbc16dd258d1609f7c7c582205e9f">00409</a>   <span class="keyword">inline</span> <span class="keyword">const</span> complex&lt;T&gt;&amp;
<a name="l00410"></a>00410   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00411"></a>00411 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7eb05d6902b13cfb7cc4f17093fdc17e" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00412"></a>00412 <span class="keyword">  </span>{
<a name="l00413"></a>00413     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l00414"></a>00414   }
<a name="l00415"></a>00415 
<a name="l00416"></a>00416 
<a name="l00418"></a>00418 
<a name="l00423"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a">00423</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00424"></a>00424   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00425"></a>00425 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">  ValueReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00426"></a>00426 <span class="keyword">  </span>{
<a name="l00427"></a>00427 
<a name="l00428"></a>00428 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00429"></a>00429 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00430"></a>00430       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00431"></a>00431                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00432"></a>00432                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00433"></a>00433 
<a name="l00434"></a>00434     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00435"></a>00435       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00436"></a>00436                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00437"></a>00437                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00438"></a>00438 <span class="preprocessor">#endif</span>
<a name="l00439"></a>00439 <span class="preprocessor"></span>
<a name="l00440"></a>00440     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j);
<a name="l00441"></a>00441   }
<a name="l00442"></a>00442 
<a name="l00443"></a>00443 
<a name="l00445"></a>00445 
<a name="l00450"></a>00450   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00451"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a11fe8648ddd3e6e339eb81735d7e544e">00451</a>   <span class="keyword">inline</span> T&amp;
<a name="l00452"></a>00452   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00453"></a>00453 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a041c76033f939293aa871f2fe45ef22a" title="Returns j-th non-zero value of row i (real part).">  ValueReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00454"></a>00454   {
<a name="l00455"></a>00455 
<a name="l00456"></a>00456 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00457"></a>00457 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00458"></a>00458       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00459"></a>00459                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00460"></a>00460                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00461"></a>00461     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00462"></a>00462       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00463"></a>00463                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00464"></a>00464                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00465"></a>00465 <span class="preprocessor">#endif</span>
<a name="l00466"></a>00466 <span class="preprocessor"></span>
<a name="l00467"></a>00467     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j);
<a name="l00468"></a>00468   }
<a name="l00469"></a>00469 
<a name="l00470"></a>00470 
<a name="l00472"></a>00472 
<a name="l00477"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b">00477</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00478"></a>00478   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00479"></a>00479 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">  IndexReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00480"></a>00480 <span class="keyword">  </span>{
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00483"></a>00483 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00484"></a>00484       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00485"></a>00485                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00486"></a>00486                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00487"></a>00487 
<a name="l00488"></a>00488     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00489"></a>00489       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00490"></a>00490                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00491"></a>00491                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00492"></a>00492 <span class="preprocessor">#endif</span>
<a name="l00493"></a>00493 <span class="preprocessor"></span>
<a name="l00494"></a>00494     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j);
<a name="l00495"></a>00495   }
<a name="l00496"></a>00496 
<a name="l00497"></a>00497 
<a name="l00499"></a>00499 
<a name="l00504"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2fdb9482dad6378fb8ced1982b6860b1">00504</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00505"></a>00505   <span class="keyword">inline</span> <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00506"></a>00506 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2a3cd76154ac61a43afa42563805ce3b" title="Returns column number of j-th non-zero value of row i (real part).">  IndexReal</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00507"></a>00507   {
<a name="l00508"></a>00508 
<a name="l00509"></a>00509 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00510"></a>00510 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00511"></a>00511       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00512"></a>00512                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00513"></a>00513                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00514"></a>00514 
<a name="l00515"></a>00515     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00516"></a>00516       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00517"></a>00517                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1)
<a name="l00518"></a>00518                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00519"></a>00519 <span class="preprocessor">#endif</span>
<a name="l00520"></a>00520 <span class="preprocessor"></span>
<a name="l00521"></a>00521     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j);
<a name="l00522"></a>00522   }
<a name="l00523"></a>00523 
<a name="l00524"></a>00524 
<a name="l00526"></a>00526 
<a name="l00531"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d">00531</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00532"></a>00532   <span class="keyword">inline</span> <span class="keyword">const</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00533"></a>00533 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">  ValueImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00534"></a>00534 <span class="keyword">  </span>{
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00537"></a>00537 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00538"></a>00538       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00539"></a>00539                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00540"></a>00540                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00541"></a>00541 
<a name="l00542"></a>00542     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00543"></a>00543       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00544"></a>00544                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00545"></a>00545                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00546"></a>00546 <span class="preprocessor">#endif</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>
<a name="l00548"></a>00548     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j);
<a name="l00549"></a>00549   }
<a name="l00550"></a>00550 
<a name="l00551"></a>00551 
<a name="l00553"></a>00553 
<a name="l00558"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ab410f72a305938081fbc07e868eb161b">00558</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00559"></a>00559   <span class="keyword">inline</span> T&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00560"></a>00560 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa2d9a2e69a269bf9d15b62d520cb560d" title="Returns j-th non-zero value of row i (imaginary part).">  ValueImag</a> (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00561"></a>00561   {
<a name="l00562"></a>00562 
<a name="l00563"></a>00563 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00564"></a>00564 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00565"></a>00565       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00566"></a>00566                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00567"></a>00567                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00568"></a>00568 
<a name="l00569"></a>00569     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00570"></a>00570       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::value&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00571"></a>00571                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00572"></a>00572                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00573"></a>00573 <span class="preprocessor">#endif</span>
<a name="l00574"></a>00574 <span class="preprocessor"></span>
<a name="l00575"></a>00575     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j);
<a name="l00576"></a>00576   }
<a name="l00577"></a>00577 
<a name="l00578"></a>00578 
<a name="l00580"></a>00580 
<a name="l00585"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5">00585</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00586"></a>00586   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00587"></a>00587 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">  IndexImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00588"></a>00588 <span class="keyword">  </span>{
<a name="l00589"></a>00589 
<a name="l00590"></a>00590 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00591"></a>00591 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00592"></a>00592       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l00593"></a>00593                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00594"></a>00594                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00595"></a>00595 
<a name="l00596"></a>00596     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00597"></a>00597       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00598"></a>00598                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00599"></a>00599                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00600"></a>00600 <span class="preprocessor">#endif</span>
<a name="l00601"></a>00601 <span class="preprocessor"></span>
<a name="l00602"></a>00602     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j);
<a name="l00603"></a>00603   }
<a name="l00604"></a>00604 
<a name="l00605"></a>00605 
<a name="l00607"></a>00607 
<a name="l00612"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a011b15c1909f712a693de10f2c0c2328">00612</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00613"></a>00613   <span class="keyword">inline</span> <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00614"></a>00614 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aedba64fa9b01bce6bb75e9e5a14b26c5" title="Returns column number of j-th non-zero value of row i (imaginary part).">  IndexImag</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00615"></a>00615   {
<a name="l00616"></a>00616 
<a name="l00617"></a>00617 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00618"></a>00618 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>)
<a name="l00619"></a>00619       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::index&quot;</span>,
<a name="l00620"></a>00620                      <span class="stringliteral">&quot;Index should be in [0, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>-1)
<a name="l00621"></a>00621                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00622"></a>00622 
<a name="l00623"></a>00623     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>())
<a name="l00624"></a>00624       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::index&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span> +
<a name="l00625"></a>00625                      <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a35ea621d2e5351259ce5bca87febc821" title="Returns the number of rows.">GetM</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00626"></a>00626                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00627"></a>00627 <span class="preprocessor">#endif</span>
<a name="l00628"></a>00628 <span class="preprocessor"></span>
<a name="l00629"></a>00629     <span class="keywordflow">return</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j);
<a name="l00630"></a>00630   }
<a name="l00631"></a>00631 
<a name="l00632"></a>00632 
<a name="l00634"></a>00634 
<a name="l00640"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1ebb3dcbf317ec99e6463f0aafea10b9">00640</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00641"></a>00641   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00642"></a>00642 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">  SetRealData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00643"></a>00643   {
<a name="l00644"></a>00644     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).SetData(n, val, ind);
<a name="l00645"></a>00645   }
<a name="l00646"></a>00646 
<a name="l00647"></a>00647 
<a name="l00649"></a>00649 
<a name="l00655"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a50622f29a8e112821c68e5a28236258f">00655</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00656"></a>00656   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00657"></a>00657 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">  SetImagData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> n, T* val, <span class="keywordtype">int</span>* ind)
<a name="l00658"></a>00658   {
<a name="l00659"></a>00659     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).SetData(n, val, ind);
<a name="l00660"></a>00660   }
<a name="l00661"></a>00661 
<a name="l00662"></a>00662 
<a name="l00664"></a>00664 
<a name="l00668"></a>00668   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00669"></a>00669   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyReal</a>(<span class="keywordtype">int</span> i)
<a name="l00670"></a>00670   {
<a name="l00671"></a>00671     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Nullify();
<a name="l00672"></a>00672   }
<a name="l00673"></a>00673 
<a name="l00674"></a>00674 
<a name="l00676"></a>00676 
<a name="l00680"></a>00680   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00681"></a>00681   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyImag</a>(<span class="keywordtype">int</span> i)
<a name="l00682"></a>00682   {
<a name="l00683"></a>00683     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Nullify();
<a name="l00684"></a>00684   }
<a name="l00685"></a>00685 
<a name="l00686"></a>00686 
<a name="l00688"></a>00688 
<a name="l00693"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea">00693</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00694"></a>00694   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00695"></a>00695 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aab6c31240cd6dcbd442f9a29658f51ea" title="Redefines the real part of the matrix.">  SetRealData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00696"></a>00696   {
<a name="l00697"></a>00697     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = m;
<a name="l00698"></a>00698     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = n;
<a name="l00699"></a>00699     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00700"></a>00700   }
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00704"></a>00704 
<a name="l00709"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2">00709</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00710"></a>00710   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00711"></a>00711 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a8e3c2dfa0ea5aa57cd8607cca15562f2" title="Redefines the imaginary part of the matrix.">  SetImagData</a>(<span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>* val)
<a name="l00712"></a>00712   {
<a name="l00713"></a>00713     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = m;
<a name="l00714"></a>00714     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = n;
<a name="l00715"></a>00715     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.SetData(Storage::GetFirst(m, n), val);
<a name="l00716"></a>00716   }
<a name="l00717"></a>00717 
<a name="l00718"></a>00718 
<a name="l00720"></a>00720 
<a name="l00724"></a>00724   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00725"></a>00725   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aed1192a75fe2c7d19e545ff6054298e2" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyReal</a>()
<a name="l00726"></a>00726   {
<a name="l00727"></a>00727     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00728"></a>00728     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00729"></a>00729     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>.Nullify();
<a name="l00730"></a>00730   }
<a name="l00731"></a>00731 
<a name="l00732"></a>00732 
<a name="l00734"></a>00734 
<a name="l00738"></a>00738   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00739"></a>00739   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#adbdb03e02fd8617a23188d6bc3155ec0" title="Clears the matrix without releasing memory.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::NullifyImag</a>()
<a name="l00740"></a>00740   {
<a name="l00741"></a>00741     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a> = 0;
<a name="l00742"></a>00742     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a> = 0;
<a name="l00743"></a>00743     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>.Nullify();
<a name="l00744"></a>00744   }
<a name="l00745"></a>00745 
<a name="l00746"></a>00746 
<a name="l00747"></a>00747   <span class="comment">/************************</span>
<a name="l00748"></a>00748 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00749"></a>00749 <span class="comment">   ************************/</span>
<a name="l00750"></a>00750 
<a name="l00751"></a>00751 
<a name="l00753"></a>00753 
<a name="l00758"></a>00758   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00759"></a>00759   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ac24abb2e047eb8b91728023074b3fb13" title="Displays the matrix on the standard output.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00760"></a>00760 <span class="keyword">  </span>{
<a name="l00761"></a>00761     <span class="keywordflow">if</span> (Storage::GetFirst(1, 0) == 1)
<a name="l00762"></a>00762       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de" title="Number of rows.">m_</a>; i++)
<a name="l00763"></a>00763         {
<a name="l00764"></a>00764           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l00765"></a>00765             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j)+1
<a name="l00766"></a>00766                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00767"></a>00767 
<a name="l00768"></a>00768           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l00769"></a>00769             cout &lt;&lt; (i+1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j)+1
<a name="l00770"></a>00770                  &lt;&lt; <span class="stringliteral">&quot; (0, &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) &lt;&lt; <span class="stringliteral">&quot;)&quot;</span>&lt;&lt;endl;
<a name="l00771"></a>00771         }
<a name="l00772"></a>00772     <span class="keywordflow">else</span>
<a name="l00773"></a>00773       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98" title="Number of columns.">n_</a>; i++)
<a name="l00774"></a>00774         {
<a name="l00775"></a>00775           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l00776"></a>00776             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00777"></a>00777                  &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) &lt;&lt; endl;
<a name="l00778"></a>00778 
<a name="l00779"></a>00779           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l00780"></a>00780             cout &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Index(j)+1 &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; i+1
<a name="l00781"></a>00781                  &lt;&lt; <span class="stringliteral">&quot; (0, &quot;</span> &lt;&lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) &lt;&lt; <span class="stringliteral">&quot;)&quot;</span>&lt;&lt;endl;
<a name="l00782"></a>00782         }
<a name="l00783"></a>00783   }
<a name="l00784"></a>00784 
<a name="l00785"></a>00785 
<a name="l00787"></a>00787 
<a name="l00791"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce">00791</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00792"></a>00792   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00793"></a>00793 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00794"></a>00794 <span class="keyword">  </span>{
<a name="l00795"></a>00795     ofstream FileStream; FileStream.precision(14);
<a name="l00796"></a>00796     FileStream.open(FileName.c_str());
<a name="l00797"></a>00797 
<a name="l00798"></a>00798 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00799"></a>00799 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00800"></a>00800     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00801"></a>00801       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::WriteText(string FileName)&quot;</span>,
<a name="l00802"></a>00802                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00803"></a>00803 <span class="preprocessor">#endif</span>
<a name="l00804"></a>00804 <span class="preprocessor"></span>
<a name="l00805"></a>00805     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l00806"></a>00806 
<a name="l00807"></a>00807     FileStream.close();
<a name="l00808"></a>00808   }
<a name="l00809"></a>00809 
<a name="l00810"></a>00810 
<a name="l00812"></a>00812 
<a name="l00816"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a884e62c07aa29b59b259d232f7771c94">00816</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00817"></a>00817   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00818"></a>00818 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a7095c684ecdb9ef6e9fe9b84462e22ce" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00819"></a>00819 <span class="keyword">  </span>{
<a name="l00820"></a>00820 
<a name="l00821"></a>00821 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00822"></a>00822 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00823"></a>00823     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00824"></a>00824       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_ArrayComplexSparse::&quot;</span>
<a name="l00825"></a>00825                     <span class="stringliteral">&quot;WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00826"></a>00826                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00827"></a>00827 <span class="preprocessor">#endif</span>
<a name="l00828"></a>00828 <span class="preprocessor"></span>
<a name="l00829"></a>00829     <span class="comment">// Conversion to coordinate format (1-index convention).</span>
<a name="l00830"></a>00830     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow, IndCol; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;T&gt;</a> &gt; Value;
<a name="l00831"></a>00831     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; leaf_class =
<a name="l00832"></a>00832       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span><a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; <span class="keyword">&gt;</span>(*this);
<a name="l00833"></a>00833 
<a name="l00834"></a>00834     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l00835"></a>00835                                  Value, 1, <span class="keyword">true</span>);
<a name="l00836"></a>00836 
<a name="l00837"></a>00837     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l00838"></a>00838       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00839"></a>00839   }
<a name="l00840"></a>00840 
<a name="l00841"></a>00841 
<a name="l00843"></a>00843 
<a name="l00849"></a>00849   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00850"></a>00850   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a889d3baa1c4aa6a96b83ba8ca7b4cf4a" title="Assembles the matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Assemble</a>()
<a name="l00851"></a>00851   {
<a name="l00852"></a>00852     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m_ ; i++)
<a name="l00853"></a>00853       {
<a name="l00854"></a>00854         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Assemble();
<a name="l00855"></a>00855         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Assemble();
<a name="l00856"></a>00856       }
<a name="l00857"></a>00857   }
<a name="l00858"></a>00858 
<a name="l00859"></a>00859 
<a name="l00861"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37">00861</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00862"></a>00862   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37" title="Matrix is initialized to the identity matrix.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00863"></a>00863 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a19b48691b03953dffd387ace5cfddd37" title="Matrix is initialized to the identity matrix.">  SetIdentity</a>()
<a name="l00864"></a>00864   {
<a name="l00865"></a>00865     this-&gt;n_ = this-&gt;m_;
<a name="l00866"></a>00866     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00867"></a>00867       {
<a name="l00868"></a>00868         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Reallocate(1);
<a name="l00869"></a>00869         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Index(0) = i;
<a name="l00870"></a>00870         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(0) = T(1);
<a name="l00871"></a>00871       }
<a name="l00872"></a>00872   }
<a name="l00873"></a>00873 
<a name="l00874"></a>00874 
<a name="l00876"></a>00876   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00877"></a>00877   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#aa6f51cb3c7779c309578058fd34c5d25" title="Non-zero entries are set to 0 (but not removed).">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00878"></a>00878   {
<a name="l00879"></a>00879     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00880"></a>00880       {
<a name="l00881"></a>00881         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Zero();
<a name="l00882"></a>00882         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Zero();
<a name="l00883"></a>00883       }
<a name="l00884"></a>00884   }
<a name="l00885"></a>00885 
<a name="l00886"></a>00886 
<a name="l00888"></a>00888   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00889"></a>00889   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00890"></a>00890   {
<a name="l00891"></a>00891     <span class="keywordtype">int</span> value = 0;
<a name="l00892"></a>00892     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00893"></a>00893       {
<a name="l00894"></a>00894         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).GetM(); j++)
<a name="l00895"></a>00895           <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Value(j) = value++;
<a name="l00896"></a>00896 
<a name="l00897"></a>00897         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).GetM(); j++)
<a name="l00898"></a>00898           <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Value(j) = value++;
<a name="l00899"></a>00899       }
<a name="l00900"></a>00900   }
<a name="l00901"></a>00901 
<a name="l00902"></a>00902 
<a name="l00904"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a08a5f1de809bbb4565e90d954ca053f6">00904</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allo&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00905"></a>00905   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allo&gt;::</a>
<a name="l00906"></a>00906 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a1fce8c713bc87ca4e06df819ced39554" title="Non-zero entries are filled with values 0, 1, 2, 3 ...">  Fill</a>(<span class="keyword">const</span> complex&lt;T0&gt;&amp; x)
<a name="l00907"></a>00907   {
<a name="l00908"></a>00908     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00909"></a>00909       {
<a name="l00910"></a>00910         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Fill(real(x));
<a name="l00911"></a>00911         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Fill(imag(x));
<a name="l00912"></a>00912       }
<a name="l00913"></a>00913   }
<a name="l00914"></a>00914 
<a name="l00915"></a>00915 
<a name="l00917"></a>00917   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00918"></a>00918   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00919"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#a95e0c06209ddfeea67740ecb9c0c1451">00919</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00920"></a>00920   <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::operator</a>=
<a name="l00921"></a>00921   (<span class="keyword">const</span> complex&lt;T0&gt;&amp; x)
<a name="l00922"></a>00922   {
<a name="l00923"></a>00923     this-&gt;Fill(x);
<a name="l00924"></a>00924   }
<a name="l00925"></a>00925 
<a name="l00926"></a>00926 
<a name="l00928"></a><a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb">00928</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00929"></a>00929   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb" title="Non-zero entries take a random value.">Matrix_ArrayComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00930"></a>00930 <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#ad1ff9883f030ca9eaada43616865acdb" title="Non-zero entries take a random value.">  FillRand</a>()
<a name="l00931"></a>00931   {
<a name="l00932"></a>00932     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00933"></a>00933       {
<a name="l00934"></a>00934         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).FillRand();
<a name="l00935"></a>00935         <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).FillRand();
<a name="l00936"></a>00936       }
<a name="l00937"></a>00937   }
<a name="l00938"></a>00938 
<a name="l00939"></a>00939 
<a name="l00941"></a>00941   <span class="comment">// MATRIX&lt;ARRAY_COLCOMPLEXSPARSE&gt; //</span>
<a name="l00943"></a>00943 <span class="comment"></span>
<a name="l00944"></a>00944 
<a name="l00946"></a>00946 
<a name="l00949"></a>00949   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00950"></a>00950   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l00951"></a>00951     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_complex_sparse.php">ArrayColComplexSparse</a>, Allocator&gt;()
<a name="l00952"></a>00952   {
<a name="l00953"></a>00953   }
<a name="l00954"></a>00954 
<a name="l00955"></a>00955 
<a name="l00957"></a>00957 
<a name="l00962"></a>00962   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00963"></a>00963   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a39f7b5286ed8bbdbe816c7809e60872c" title="Default constructor.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00964"></a>00964     Matrix_ArrayComplexSparse&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;(i, j)
<a name="l00965"></a>00965   {
<a name="l00966"></a>00966   }
<a name="l00967"></a>00967 
<a name="l00968"></a>00968 
<a name="l00970"></a>00970   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00971"></a>00971   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a4962f5db5a03299da3789ff2c7ab653b" title="Clears column i.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::ClearRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l00972"></a>00972   {
<a name="l00973"></a>00973     this-&gt;val_real_(i).Clear();
<a name="l00974"></a>00974   }
<a name="l00975"></a>00975 
<a name="l00976"></a>00976 
<a name="l00978"></a>00978   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00979"></a>00979   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a5e616d97f5b568c8adb47ccc15540300" title="Clears column i.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::ClearImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l00980"></a>00980   {
<a name="l00981"></a>00981     this-&gt;val_imag_(i).Clear();
<a name="l00982"></a>00982   }
<a name="l00983"></a>00983 
<a name="l00984"></a>00984 
<a name="l00986"></a>00986 
<a name="l00990"></a>00990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l00991"></a>00991   <span class="keywordtype">void</span> Matrix&lt;T, Prop, ArrayColComplexSparse, Alloc&gt;::ReallocateRealColumn(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l00992"></a>00992   {
<a name="l00993"></a>00993     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6" title="real part rows or columns">val_real_</a>(i).Reallocate(j);
<a name="l00994"></a>00994   }
<a name="l00995"></a>00995 
<a name="l00996"></a>00996 
<a name="l00998"></a>00998 
<a name="l01002"></a>01002   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Alloc&gt; <span class="keyword">inline</span>
<a name="l01003"></a>01003   <span class="keywordtype">void</span> Matrix&lt;T, Prop, ArrayColComplexSparse, Alloc&gt;::ReallocateImagColumn(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01004"></a>01004   {
<a name="l01005"></a>01005     this-&gt;<a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8" title="imaginary part rows or columns">val_imag_</a>(i).Reallocate(j);
<a name="l01006"></a>01006   }
<a name="l01007"></a>01007 
<a name="l01008"></a>01008 
<a name="l01010"></a>01010 
<a name="l01014"></a>01014   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01015"></a>01015   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad7da5d2ec6445de44d3a5a81a0b92128" title="Reallocates column i.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::ResizeRealColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01016"></a>01016   {
<a name="l01017"></a>01017     this-&gt;val_real_(i).Resize(j);
<a name="l01018"></a>01018   }
<a name="l01019"></a>01019 
<a name="l01020"></a>01020 
<a name="l01022"></a>01022 
<a name="l01026"></a>01026   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01027"></a>01027   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ac11baddc386e9569864bb21de1282eb0" title="Reallocates column i.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::ResizeImagColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01028"></a>01028   {
<a name="l01029"></a>01029     this-&gt;val_imag_(i).Resize(j);
<a name="l01030"></a>01030   }
<a name="l01031"></a>01031 
<a name="l01032"></a>01032 
<a name="l01034"></a>01034 
<a name="l01038"></a>01038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01039"></a>01039   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a88a77017d1aedf2ecc8c371e475f0d15" title="Swaps two columns.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::SwapRealColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01040"></a>01040   {
<a name="l01041"></a>01041     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l01042"></a>01042   }
<a name="l01043"></a>01043 
<a name="l01044"></a>01044 
<a name="l01046"></a>01046 
<a name="l01050"></a>01050   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01051"></a>01051   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a77edc2353b0c31b171a3721d9c514dac" title="Swaps two columns.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::SwapImagColumn</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01052"></a>01052   {
<a name="l01053"></a>01053     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l01054"></a>01054   }
<a name="l01055"></a>01055 
<a name="l01056"></a>01056 
<a name="l01058"></a>01058 
<a name="l01062"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a618e575e978846f1ff95f9e8e3fe74c0">01062</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01063"></a>01063   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01064"></a>01064 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01065"></a>01065   {
<a name="l01066"></a>01066     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l01067"></a>01067       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l01068"></a>01068   }
<a name="l01069"></a>01069 
<a name="l01070"></a>01070 
<a name="l01072"></a>01072 
<a name="l01076"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a54da32085a107892efa1828ebb9b43c7">01076</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01077"></a>01077   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01078"></a>01078 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01079"></a>01079   {
<a name="l01080"></a>01080     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l01081"></a>01081       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l01082"></a>01082   }
<a name="l01083"></a>01083 
<a name="l01084"></a>01084 
<a name="l01086"></a>01086 
<a name="l01090"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a790b5cd9f59aed77430dfbceb834a33e">01090</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01091"></a>01091   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01092"></a>01092 <a class="code" href="class_seldon_1_1_matrix.php">  GetRealColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01093"></a>01093 <span class="keyword">  </span>{
<a name="l01094"></a>01094     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l01095"></a>01095   }
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01099"></a>01099 
<a name="l01103"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad6f24240d54e489ab302c91a10b62276">01103</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01104"></a>01104   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01105"></a>01105 <a class="code" href="class_seldon_1_1_matrix.php">  GetImagColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01106"></a>01106 <span class="keyword">  </span>{
<a name="l01107"></a>01107     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l01108"></a>01108   }
<a name="l01109"></a>01109 
<a name="l01110"></a>01110 
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01113"></a>01113   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::PrintRealColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01114"></a>01114 <span class="keyword">  </span>{
<a name="l01115"></a>01115     this-&gt;val_real_(i).Print();
<a name="l01116"></a>01116   }
<a name="l01117"></a>01117 
<a name="l01118"></a>01118 
<a name="l01120"></a>01120   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01121"></a>01121   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a1d651709170e5a488fb2f6a2cffa9ece" title="Displays non-zero values of a column.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::PrintImagColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01122"></a>01122 <span class="keyword">  </span>{
<a name="l01123"></a>01123     this-&gt;val_imag_(i).Print();
<a name="l01124"></a>01124   }
<a name="l01125"></a>01125 
<a name="l01126"></a>01126 
<a name="l01128"></a>01128 
<a name="l01133"></a>01133   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01134"></a>01134   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ad6e1965a8a55307f92757a12bbe9662d" title="Assembles a column.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::AssembleRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01135"></a>01135   {
<a name="l01136"></a>01136     this-&gt;val_real_(i).Assemble();
<a name="l01137"></a>01137   }
<a name="l01138"></a>01138 
<a name="l01139"></a>01139 
<a name="l01141"></a>01141 
<a name="l01146"></a>01146   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01147"></a>01147   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#ac9846afe391f9acb0129f9595d2cf3e2" title="Assembles a column.">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::AssembleImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01148"></a>01148   {
<a name="l01149"></a>01149     this-&gt;val_imag_(i).Assemble();
<a name="l01150"></a>01150   }
<a name="l01151"></a>01151 
<a name="l01152"></a>01152 
<a name="l01154"></a>01154 
<a name="l01159"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a3e34290958f86479984c53107df922cf">01159</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01160"></a>01160   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01161"></a>01161 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l01162"></a>01162   {
<a name="l01163"></a>01163     <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l01164"></a>01164       this-&gt;val_real_(j).AddInteraction(i, real(val));
<a name="l01165"></a>01165 
<a name="l01166"></a>01166     <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l01167"></a>01167       this-&gt;val_imag_(j).AddInteraction(i, imag(val));
<a name="l01168"></a>01168   }
<a name="l01169"></a>01169 
<a name="l01170"></a>01170 
<a name="l01172"></a>01172 
<a name="l01178"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a774a26e19d61dd4a9e7f95de5559368e">01178</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01179"></a>01179   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01180"></a>01180 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01181"></a>01181                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01182"></a>01182   {
<a name="l01183"></a>01183     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01184"></a>01184       AddInteraction(i, col(j), val(j));
<a name="l01185"></a>01185   }
<a name="l01186"></a>01186 
<a name="l01187"></a>01187 
<a name="l01189"></a>01189 
<a name="l01195"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_complex_sparse_00_01_allocator_01_4.php#a6ecf0924c16cfb04913b70cd03fb7a19">01195</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01196"></a>01196   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColComplexSparse, Allocator&gt;::</a>
<a name="l01197"></a>01197 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01198"></a>01198                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01199"></a>01199   {
<a name="l01200"></a>01200     <span class="keywordtype">int</span> nb_real = 0;
<a name="l01201"></a>01201     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l01202"></a>01202     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row_real(nb), row_imag(nb);
<a name="l01203"></a>01203     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l01204"></a>01204     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01205"></a>01205       {
<a name="l01206"></a>01206         <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l01207"></a>01207           {
<a name="l01208"></a>01208             row_real(nb_real) = row(j);
<a name="l01209"></a>01209             val_real(nb_real) = real(val(j));
<a name="l01210"></a>01210             nb_real++;
<a name="l01211"></a>01211           }
<a name="l01212"></a>01212 
<a name="l01213"></a>01213         <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l01214"></a>01214           {
<a name="l01215"></a>01215             row_imag(nb_imag) = row(j);
<a name="l01216"></a>01216             val_imag(nb_imag) = imag(val(j));
<a name="l01217"></a>01217             nb_imag++;
<a name="l01218"></a>01218           }
<a name="l01219"></a>01219       }
<a name="l01220"></a>01220 
<a name="l01221"></a>01221     this-&gt;val_real_(i).AddInteractionRow(nb_real, row_real, val_real);
<a name="l01222"></a>01222     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, row_imag, val_imag);
<a name="l01223"></a>01223   }
<a name="l01224"></a>01224 
<a name="l01225"></a>01225 
<a name="l01227"></a>01227   <span class="comment">// MATRIX&lt;ARRAY_ROWCOMPLEXSPARSE&gt; //</span>
<a name="l01229"></a>01229 <span class="comment"></span>
<a name="l01230"></a>01230 
<a name="l01232"></a>01232 
<a name="l01235"></a>01235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01236"></a>01236   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01237"></a>01237     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_complex_sparse.php">ArrayRowComplexSparse</a>, Allocator&gt;()
<a name="l01238"></a>01238   {
<a name="l01239"></a>01239   }
<a name="l01240"></a>01240 
<a name="l01241"></a>01241 
<a name="l01243"></a>01243 
<a name="l01248"></a>01248   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01249"></a>01249   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab98aecad772f00d59ea17248a04720d3" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01250"></a>01250     Matrix_ArrayComplexSparse&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;(i, j)
<a name="l01251"></a>01251   {
<a name="l01252"></a>01252   }
<a name="l01253"></a>01253 
<a name="l01254"></a>01254 
<a name="l01256"></a>01256   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01257"></a>01257   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a480f759c9bdd3e69206f18d81ed44f5a" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::ClearRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l01258"></a>01258   {
<a name="l01259"></a>01259     this-&gt;val_real_(i).Clear();
<a name="l01260"></a>01260   }
<a name="l01261"></a>01261 
<a name="l01262"></a>01262 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01265"></a>01265   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a16289d6a8f38a4303f75113667f8af74" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::ClearImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l01266"></a>01266   {
<a name="l01267"></a>01267     this-&gt;val_imag_(i).Clear();
<a name="l01268"></a>01268   }
<a name="l01269"></a>01269 
<a name="l01270"></a>01270 
<a name="l01272"></a>01272 
<a name="l01277"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aefd387926aad7c3609a9f6bc062252e0">01277</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01278"></a>01278   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01279"></a>01279 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01280"></a>01280   {
<a name="l01281"></a>01281     this-&gt;val_real_(i).Reallocate(j);
<a name="l01282"></a>01282   }
<a name="l01283"></a>01283 
<a name="l01284"></a>01284 
<a name="l01286"></a>01286 
<a name="l01291"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aa798b00f5793c414777a5e3b421095a0">01291</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01292"></a>01292   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01293"></a>01293 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01294"></a>01294   {
<a name="l01295"></a>01295     this-&gt;val_imag_(i).Reallocate(j);
<a name="l01296"></a>01296   }
<a name="l01297"></a>01297 
<a name="l01298"></a>01298 
<a name="l01300"></a>01300 
<a name="l01305"></a>01305   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01306"></a>01306   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::ResizeRealRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01307"></a>01307   {
<a name="l01308"></a>01308     this-&gt;val_real_(i).Resize(j);
<a name="l01309"></a>01309   }
<a name="l01310"></a>01310 
<a name="l01311"></a>01311 
<a name="l01313"></a>01313 
<a name="l01318"></a>01318   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01319"></a>01319   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#af89faca49416df1ad0b4262d440adec9" title="Changes the size of a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::ResizeImagRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01320"></a>01320   {
<a name="l01321"></a>01321     this-&gt;val_imag_(i).Resize(j);
<a name="l01322"></a>01322   }
<a name="l01323"></a>01323 
<a name="l01324"></a>01324 
<a name="l01326"></a>01326 
<a name="l01330"></a>01330   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01331"></a>01331   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a8ae984b95031fd72c6ecab86418d60ac" title="Swaps two rows.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::SwapRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01332"></a>01332   {
<a name="l01333"></a>01333     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l01334"></a>01334   }
<a name="l01335"></a>01335 
<a name="l01336"></a>01336 
<a name="l01338"></a>01338 
<a name="l01342"></a>01342   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01343"></a>01343   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a4b6b31fa7468c32bddf17fd1a22888ae" title="Swaps two rows.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::SwapImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01344"></a>01344   {
<a name="l01345"></a>01345     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l01346"></a>01346   }
<a name="l01347"></a>01347 
<a name="l01348"></a>01348 
<a name="l01350"></a>01350 
<a name="l01354"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a350b6c00041f9e9db6bf111ac431bdf9">01354</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01355"></a>01355   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01356"></a>01356 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01357"></a>01357   {
<a name="l01358"></a>01358     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l01359"></a>01359       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l01360"></a>01360   }
<a name="l01361"></a>01361 
<a name="l01362"></a>01362 
<a name="l01364"></a>01364 
<a name="l01368"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3e6d41ebaccab9d346d6fd291e28ff5d">01368</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01369"></a>01369   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01370"></a>01370 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01371"></a>01371   {
<a name="l01372"></a>01372     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l01373"></a>01373       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l01374"></a>01374   }
<a name="l01375"></a>01375 
<a name="l01376"></a>01376 
<a name="l01378"></a>01378 
<a name="l01382"></a>01382   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01383"></a>01383   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::GetRealRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01384"></a>01384 <span class="keyword">  </span>{
<a name="l01385"></a>01385     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l01386"></a>01386   }
<a name="l01387"></a>01387 
<a name="l01388"></a>01388 
<a name="l01390"></a>01390 
<a name="l01394"></a>01394   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01395"></a>01395   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a5477a0086e76fbee8db94fcfa1cd9703" title="Returns the number of non-zero entries of a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::GetImagRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01396"></a>01396 <span class="keyword">  </span>{
<a name="l01397"></a>01397     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l01398"></a>01398   }
<a name="l01399"></a>01399 
<a name="l01400"></a>01400 
<a name="l01402"></a>01402   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01403"></a>01403   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aeeca29af2514e61e003925875e7f7c33" title="Displays non-zero values of a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::PrintRealRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01404"></a>01404 <span class="keyword">  </span>{
<a name="l01405"></a>01405     this-&gt;val_real_(i).Print();
<a name="l01406"></a>01406   }
<a name="l01407"></a>01407 
<a name="l01408"></a>01408 
<a name="l01410"></a>01410   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01411"></a>01411   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#aebbf5322f0709878e15c379391a8aec8" title="Displays non-zero values of a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::PrintImagRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01412"></a>01412 <span class="keyword">  </span>{
<a name="l01413"></a>01413     this-&gt;val_imag_(i).Print();
<a name="l01414"></a>01414   }
<a name="l01415"></a>01415 
<a name="l01416"></a>01416 
<a name="l01418"></a>01418 
<a name="l01423"></a>01423   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01424"></a>01424   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a70fa643e503c063be37de261f89aab22" title="Assembles a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::AssembleRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l01425"></a>01425   {
<a name="l01426"></a>01426     this-&gt;val_real_(i).Assemble();
<a name="l01427"></a>01427   }
<a name="l01428"></a>01428 
<a name="l01429"></a>01429 
<a name="l01431"></a>01431 
<a name="l01436"></a>01436   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01437"></a>01437   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a48ce7e0631521cc72c564be088917148" title="Assembles a row.">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::AssembleImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l01438"></a>01438   {
<a name="l01439"></a>01439     this-&gt;val_imag_(i).Assemble();
<a name="l01440"></a>01440   }
<a name="l01441"></a>01441 
<a name="l01442"></a>01442 
<a name="l01444"></a>01444 
<a name="l01449"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#ab2d2137e557ad1acd76fed600f5c3584">01449</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01450"></a>01450   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01451"></a>01451 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l01452"></a>01452   {
<a name="l01453"></a>01453     <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l01454"></a>01454       this-&gt;val_real_(i).AddInteraction(j, real(val));
<a name="l01455"></a>01455 
<a name="l01456"></a>01456     <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l01457"></a>01457       this-&gt;val_imag_(i).AddInteraction(j, imag(val));
<a name="l01458"></a>01458   }
<a name="l01459"></a>01459 
<a name="l01460"></a>01460 
<a name="l01462"></a>01462 
<a name="l01468"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a445de4f86d354e07251b8a9da3eff045">01468</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01469"></a>01469   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01470"></a>01470 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01471"></a>01471                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01472"></a>01472   {
<a name="l01473"></a>01473     <span class="keywordflow">if</span> (nb &lt;= 0)
<a name="l01474"></a>01474       <span class="keywordflow">return</span>;
<a name="l01475"></a>01475 
<a name="l01476"></a>01476     <span class="keywordtype">int</span> nb_real = 0;
<a name="l01477"></a>01477     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l01478"></a>01478     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col_real(nb), col_imag(nb);
<a name="l01479"></a>01479     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l01480"></a>01480     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01481"></a>01481       {
<a name="l01482"></a>01482         <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l01483"></a>01483           {
<a name="l01484"></a>01484             col_real(nb_real) = col(j);
<a name="l01485"></a>01485             val_real(nb_real) = real(val(j));
<a name="l01486"></a>01486             nb_real++;
<a name="l01487"></a>01487           }
<a name="l01488"></a>01488 
<a name="l01489"></a>01489         <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l01490"></a>01490           {
<a name="l01491"></a>01491             col_imag(nb_imag) = col(j);
<a name="l01492"></a>01492             val_imag(nb_imag) = imag(val(j));
<a name="l01493"></a>01493             nb_imag++;
<a name="l01494"></a>01494           }
<a name="l01495"></a>01495       }
<a name="l01496"></a>01496 
<a name="l01497"></a>01497     this-&gt;val_real_(i).AddInteractionRow(nb_real, col_real, val_real);
<a name="l01498"></a>01498     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, col_imag, val_imag);
<a name="l01499"></a>01499   }
<a name="l01500"></a>01500 
<a name="l01501"></a>01501 
<a name="l01503"></a>01503 
<a name="l01509"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_complex_sparse_00_01_allocator_01_4.php#a3a9584691f4c544d683819e70202db20">01509</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01510"></a>01510   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowComplexSparse, Allocator&gt;::</a>
<a name="l01511"></a>01511 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01512"></a>01512                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01513"></a>01513   {
<a name="l01514"></a>01514     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01515"></a>01515       AddInteraction(row(j), i, val(j));
<a name="l01516"></a>01516   }
<a name="l01517"></a>01517 
<a name="l01518"></a>01518 
<a name="l01520"></a>01520   <span class="comment">// MATRIX&lt;ARRAY_COLSYMCOMPLEXSPARSE&gt; //</span>
<a name="l01522"></a>01522 <span class="comment"></span>
<a name="l01523"></a>01523 
<a name="l01525"></a>01525 
<a name="l01528"></a>01528   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01529"></a>01529   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01530"></a>01530     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_col_sym_complex_sparse.php">ArrayColSymComplexSparse</a>, Allocator&gt;()
<a name="l01531"></a>01531   {
<a name="l01532"></a>01532   }
<a name="l01533"></a>01533 
<a name="l01534"></a>01534 
<a name="l01536"></a>01536 
<a name="l01541"></a>01541   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01542"></a>01542   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ac0a88fcc183d15c9d86cfdde270a1d30" title="Default constructor.">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01543"></a>01543     Matrix_ArrayComplexSparse&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;(i, j)
<a name="l01544"></a>01544   {
<a name="l01545"></a>01545   }
<a name="l01546"></a>01546 
<a name="l01547"></a>01547 
<a name="l01548"></a>01548   <span class="comment">/**********************************</span>
<a name="l01549"></a>01549 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01550"></a>01550 <span class="comment">   **********************************/</span>
<a name="l01551"></a>01551 
<a name="l01552"></a>01552 
<a name="l01554"></a>01554 
<a name="l01560"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ace196e6c26320571bf77a782833d7232">01560</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01561"></a>01561   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l01562"></a>01562   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01563"></a>01563 <span class="keyword">    const</span>
<a name="l01564"></a>01564 <span class="keyword">  </span>{
<a name="l01565"></a>01565 
<a name="l01566"></a>01566 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01567"></a>01567 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01568"></a>01568       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01569"></a>01569                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01570"></a>01570                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01571"></a>01571     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01572"></a>01572       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01573"></a>01573                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01574"></a>01574                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01575"></a>01575 <span class="preprocessor">#endif</span>
<a name="l01576"></a>01576 <span class="preprocessor"></span>
<a name="l01577"></a>01577     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01578"></a>01578       <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(j)(i), this-&gt;val_imag_(j)(i));
<a name="l01579"></a>01579 
<a name="l01580"></a>01580     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(i)(j), this-&gt;val_imag_(i)(j));
<a name="l01581"></a>01581   }
<a name="l01582"></a>01582 
<a name="l01583"></a>01583 
<a name="l01585"></a>01585   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01586"></a>01586   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ClearRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01587"></a>01587   {
<a name="l01588"></a>01588     this-&gt;val_real_(i).Clear();
<a name="l01589"></a>01589   }
<a name="l01590"></a>01590 
<a name="l01591"></a>01591 
<a name="l01593"></a>01593   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l01594"></a>01594   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addf758fad26c1595b8398590e138fa2f" title="Clears a column.">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::ClearImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01595"></a>01595   {
<a name="l01596"></a>01596     this-&gt;val_imag_(i).Clear();
<a name="l01597"></a>01597   }
<a name="l01598"></a>01598 
<a name="l01599"></a>01599 
<a name="l01601"></a>01601 
<a name="l01606"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a063f733a818719a8349d0de407a685a0">01606</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01607"></a>01607   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01608"></a>01608 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01609"></a>01609   {
<a name="l01610"></a>01610     this-&gt;val_real_(i).Reallocate(j);
<a name="l01611"></a>01611   }
<a name="l01612"></a>01612 
<a name="l01613"></a>01613 
<a name="l01615"></a>01615 
<a name="l01620"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a460d1b0cadc5cf4915ce024be57b1e58">01620</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01621"></a>01621   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01622"></a>01622 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01623"></a>01623   {
<a name="l01624"></a>01624     this-&gt;val_imag_(i).Reallocate(j);
<a name="l01625"></a>01625   }
<a name="l01626"></a>01626 
<a name="l01627"></a>01627 
<a name="l01629"></a>01629 
<a name="l01633"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab21dd3904c7fb5d4673106b83b5099d4">01633</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01634"></a>01634   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01635"></a>01635 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01636"></a>01636   {
<a name="l01637"></a>01637     this-&gt;val_real_(i).Resize(j);
<a name="l01638"></a>01638   }
<a name="l01639"></a>01639 
<a name="l01640"></a>01640 
<a name="l01642"></a>01642 
<a name="l01646"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a8f954de1b994af747e4e4e6c3bc2d8b9">01646</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01647"></a>01647   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01648"></a>01648 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01649"></a>01649   {
<a name="l01650"></a>01650     this-&gt;val_imag_(i).Resize(j);
<a name="l01651"></a>01651   }
<a name="l01652"></a>01652 
<a name="l01653"></a>01653 
<a name="l01655"></a>01655 
<a name="l01659"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#af2f1c1dc9a20bfed30e186f8d4c7dc44">01659</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01660"></a>01660   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01661"></a>01661 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRealColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01662"></a>01662   {
<a name="l01663"></a>01663     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l01664"></a>01664   }
<a name="l01665"></a>01665 
<a name="l01666"></a>01666 
<a name="l01668"></a>01668 
<a name="l01672"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a7b4cf92d8be151c14508ada2b6b0b72d">01672</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01673"></a>01673   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01674"></a>01674 <a class="code" href="class_seldon_1_1_matrix.php">  SwapImagColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01675"></a>01675   {
<a name="l01676"></a>01676     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l01677"></a>01677   }
<a name="l01678"></a>01678 
<a name="l01679"></a>01679 
<a name="l01681"></a>01681 
<a name="l01685"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#addca588562b05576d4b37711bb4f6820">01685</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01686"></a>01686   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01687"></a>01687 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01688"></a>01688   {
<a name="l01689"></a>01689     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l01690"></a>01690       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l01691"></a>01691   }
<a name="l01692"></a>01692 
<a name="l01693"></a>01693 
<a name="l01695"></a>01695 
<a name="l01699"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a832f6f65f5c46c0cfaa66c0cb76e922e">01699</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01700"></a>01700   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01701"></a>01701 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexColumn</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l01702"></a>01702   {
<a name="l01703"></a>01703     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l01704"></a>01704       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l01705"></a>01705   }
<a name="l01706"></a>01706 
<a name="l01707"></a>01707 
<a name="l01709"></a>01709 
<a name="l01713"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ab488dd8cd29a05f016d63b30e90b4f2d">01713</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01714"></a>01714   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01715"></a>01715 <a class="code" href="class_seldon_1_1_matrix.php">  GetRealColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01716"></a>01716 <span class="keyword">  </span>{
<a name="l01717"></a>01717     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l01718"></a>01718   }
<a name="l01719"></a>01719 
<a name="l01720"></a>01720 
<a name="l01722"></a>01722 
<a name="l01726"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6cebdd4865f5c2c9e90d5b4f6961414a">01726</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01727"></a>01727   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01728"></a>01728 <a class="code" href="class_seldon_1_1_matrix.php">  GetImagColumnSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01729"></a>01729 <span class="keyword">  </span>{
<a name="l01730"></a>01730     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l01731"></a>01731   }
<a name="l01732"></a>01732 
<a name="l01733"></a>01733 
<a name="l01735"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ad080f5381fb2a720b7ecb72b245db852">01735</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01736"></a>01736   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01737"></a>01737 <a class="code" href="class_seldon_1_1_matrix.php">  PrintRealColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01738"></a>01738 <span class="keyword">  </span>{
<a name="l01739"></a>01739     this-&gt;val_real_(i).Print();
<a name="l01740"></a>01740   }
<a name="l01741"></a>01741 
<a name="l01742"></a>01742 
<a name="l01744"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#afa56428c069675df01f46fd68d39be47">01744</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01745"></a>01745   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01746"></a>01746 <a class="code" href="class_seldon_1_1_matrix.php">  PrintImagColumn</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l01747"></a>01747 <span class="keyword">  </span>{
<a name="l01748"></a>01748     this-&gt;val_imag_(i).Print();
<a name="l01749"></a>01749   }
<a name="l01750"></a>01750 
<a name="l01751"></a>01751 
<a name="l01753"></a>01753 
<a name="l01758"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a2ddc33ea923bccadd9b07a7f6b0a8690">01758</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01759"></a>01759   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01760"></a>01760 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleRealColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01761"></a>01761   {
<a name="l01762"></a>01762     this-&gt;val_real_(i).Assemble();
<a name="l01763"></a>01763   }
<a name="l01764"></a>01764 
<a name="l01765"></a>01765 
<a name="l01767"></a>01767 
<a name="l01772"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a1285786e064b8336ba57b6c77097e4d5">01772</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01773"></a>01773   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01774"></a>01774 <a class="code" href="class_seldon_1_1_matrix.php">  AssembleImagColumn</a>(<span class="keywordtype">int</span> i)
<a name="l01775"></a>01775   {
<a name="l01776"></a>01776     this-&gt;val_imag_(i).Assemble();
<a name="l01777"></a>01777   }
<a name="l01778"></a>01778 
<a name="l01779"></a>01779 
<a name="l01781"></a>01781 
<a name="l01786"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a96c7b8d7f0e1e862558b9e3039a32878">01786</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01787"></a>01787   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01788"></a>01788 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l01789"></a>01789   {
<a name="l01790"></a>01790     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01791"></a>01791       {
<a name="l01792"></a>01792         <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l01793"></a>01793           this-&gt;val_real_(j).AddInteraction(i, real(val));
<a name="l01794"></a>01794 
<a name="l01795"></a>01795         <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l01796"></a>01796           this-&gt;val_imag_(j).AddInteraction(i, imag(val));
<a name="l01797"></a>01797       }
<a name="l01798"></a>01798   }
<a name="l01799"></a>01799 
<a name="l01800"></a>01800 
<a name="l01802"></a>01802 
<a name="l01808"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a6dcf362a6feaf5467c8c64d6be823fda">01808</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01809"></a>01809   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01810"></a>01810 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l01811"></a>01811                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01812"></a>01812   {
<a name="l01813"></a>01813     AddInteractionColumn(i, nb, col, val);
<a name="l01814"></a>01814   }
<a name="l01815"></a>01815 
<a name="l01816"></a>01816 
<a name="l01818"></a>01818 
<a name="l01824"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#a703b16f79696af7a7d18da3c1db72671">01824</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l01825"></a>01825   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayColSymComplexSparse, Allocator&gt;::</a>
<a name="l01826"></a>01826 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l01827"></a>01827                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l01828"></a>01828   {
<a name="l01829"></a>01829     <span class="keywordtype">int</span> nb_real = 0;
<a name="l01830"></a>01830     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l01831"></a>01831     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> row_real(nb), row_imag(nb);
<a name="l01832"></a>01832     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l01833"></a>01833     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l01834"></a>01834       <span class="keywordflow">if</span> (row(j) &lt;= i)
<a name="l01835"></a>01835         {
<a name="l01836"></a>01836           <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l01837"></a>01837             {
<a name="l01838"></a>01838               row_real(nb_real) = row(j);
<a name="l01839"></a>01839               val_real(nb_real) = real(val(j));
<a name="l01840"></a>01840               nb_real++;
<a name="l01841"></a>01841             }
<a name="l01842"></a>01842 
<a name="l01843"></a>01843           <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l01844"></a>01844             {
<a name="l01845"></a>01845               row_imag(nb_imag) = row(j);
<a name="l01846"></a>01846               val_imag(nb_imag) = imag(val(j));
<a name="l01847"></a>01847               nb_imag++;
<a name="l01848"></a>01848             }
<a name="l01849"></a>01849         }
<a name="l01850"></a>01850 
<a name="l01851"></a>01851     this-&gt;val_real_(i).AddInteractionRow(nb_real, row_real, val_real);
<a name="l01852"></a>01852     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, row_imag, val_imag);
<a name="l01853"></a>01853   }
<a name="l01854"></a>01854 
<a name="l01855"></a>01855 
<a name="l01857"></a>01857   <span class="comment">// MATRIX&lt;ARRAY_ROWSYMCOMPLEXSPARSE&gt; //</span>
<a name="l01859"></a>01859 <span class="comment"></span>
<a name="l01860"></a>01860 
<a name="l01862"></a>01862 
<a name="l01865"></a>01865   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01866"></a>01866   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01867"></a>01867     <a class="code" href="class_seldon_1_1_matrix___array_complex_sparse.php" title="Sparse Array-matrix class.">Matrix_ArrayComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator&gt;()
<a name="l01868"></a>01868   {
<a name="l01869"></a>01869   }
<a name="l01870"></a>01870 
<a name="l01871"></a>01871 
<a name="l01873"></a>01873 
<a name="l01878"></a>01878   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01879"></a>01879   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5436a5d9f8bf73dbceb229a0649facfe" title="Default constructor.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01880"></a>01880     Matrix_ArrayComplexSparse&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;(i, j)
<a name="l01881"></a>01881   {
<a name="l01882"></a>01882   }
<a name="l01883"></a>01883 
<a name="l01884"></a>01884 
<a name="l01885"></a>01885   <span class="comment">/**********************************</span>
<a name="l01886"></a>01886 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01887"></a>01887 <span class="comment">   **********************************/</span>
<a name="l01888"></a>01888 
<a name="l01889"></a>01889 
<a name="l01891"></a>01891 
<a name="l01897"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5fb8630efa21241d2517927eeb80de5e">01897</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01898"></a>01898   <span class="keyword">inline</span> complex&lt;T&gt;
<a name="l01899"></a>01899   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l01900"></a>01900 <span class="keyword">    const</span>
<a name="l01901"></a>01901 <span class="keyword">  </span>{
<a name="l01902"></a>01902 
<a name="l01903"></a>01903 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01904"></a>01904 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01905"></a>01905       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01906"></a>01906                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01907"></a>01907                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01908"></a>01908     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01909"></a>01909       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ArraySparse::operator()&quot;</span>, <span class="stringliteral">&quot;Index should be in [0, &quot;</span>
<a name="l01910"></a>01910                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01911"></a>01911                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01912"></a>01912 <span class="preprocessor">#endif</span>
<a name="l01913"></a>01913 <span class="preprocessor"></span>
<a name="l01914"></a>01914     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l01915"></a>01915       <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(i)(j), this-&gt;val_imag_(i)(j));
<a name="l01916"></a>01916 
<a name="l01917"></a>01917     <span class="keywordflow">return</span> complex&lt;T&gt;(this-&gt;val_real_(j)(i), this-&gt;val_imag_(j)(i));
<a name="l01918"></a>01918   }
<a name="l01919"></a>01919 
<a name="l01920"></a>01920 
<a name="l01922"></a>01922   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01923"></a>01923   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ClearRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l01924"></a>01924   {
<a name="l01925"></a>01925     this-&gt;val_real_(i).Clear();
<a name="l01926"></a>01926   }
<a name="l01927"></a>01927 
<a name="l01928"></a>01928 
<a name="l01930"></a>01930   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01931"></a>01931   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a47212adda8f7586925272cf2e4c620bb" title="Clears a row.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::ClearImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l01932"></a>01932   {
<a name="l01933"></a>01933     this-&gt;val_imag_(i).Clear();
<a name="l01934"></a>01934   }
<a name="l01935"></a>01935 
<a name="l01936"></a>01936 
<a name="l01938"></a>01938 
<a name="l01943"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a828bff3d9a061706b80cf3cc4a6725eb">01943</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01944"></a>01944   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l01945"></a>01945 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01946"></a>01946   {
<a name="l01947"></a>01947     this-&gt;val_real_(i).Reallocate(j);
<a name="l01948"></a>01948   }
<a name="l01949"></a>01949 
<a name="l01950"></a>01950 
<a name="l01952"></a>01952 
<a name="l01957"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#add6c25193de11ecc3f405c1ba713e0ef">01957</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01958"></a>01958   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l01959"></a>01959 <a class="code" href="class_seldon_1_1_matrix.php">  ReallocateImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01960"></a>01960   {
<a name="l01961"></a>01961     this-&gt;val_imag_(i).Reallocate(j);
<a name="l01962"></a>01962   }
<a name="l01963"></a>01963 
<a name="l01964"></a>01964 
<a name="l01966"></a>01966 
<a name="l01970"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9a7d49e83e0838f63c4e7210ccc42218">01970</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01971"></a>01971   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l01972"></a>01972 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01973"></a>01973   {
<a name="l01974"></a>01974     this-&gt;val_real_(i).Resize(j);
<a name="l01975"></a>01975   }
<a name="l01976"></a>01976 
<a name="l01977"></a>01977 
<a name="l01979"></a>01979 
<a name="l01983"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a770cf431a7508fad913ef06bf487f9d6">01983</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01984"></a>01984   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l01985"></a>01985 <a class="code" href="class_seldon_1_1_matrix.php">  ResizeImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01986"></a>01986   {
<a name="l01987"></a>01987     this-&gt;val_imag_(i).Resize(j);
<a name="l01988"></a>01988   }
<a name="l01989"></a>01989 
<a name="l01990"></a>01990 
<a name="l01992"></a>01992 
<a name="l01996"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adf10d81ed34502289b1d928626921d91">01996</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01997"></a>01997   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l01998"></a>01998 <a class="code" href="class_seldon_1_1_matrix.php">  SwapRealRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l01999"></a>01999   {
<a name="l02000"></a>02000     Swap(this-&gt;val_real_(i), this-&gt;val_real_(j));
<a name="l02001"></a>02001   }
<a name="l02002"></a>02002 
<a name="l02003"></a>02003 
<a name="l02005"></a>02005 
<a name="l02009"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#abd914e374842533215ebeb12ae0fd5e6">02009</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02010"></a>02010   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02011"></a>02011 <a class="code" href="class_seldon_1_1_matrix.php">  SwapImagRow</a>(<span class="keywordtype">int</span> i,<span class="keywordtype">int</span> j)
<a name="l02012"></a>02012   {
<a name="l02013"></a>02013     Swap(this-&gt;val_imag_(i), this-&gt;val_imag_(j));
<a name="l02014"></a>02014   }
<a name="l02015"></a>02015 
<a name="l02016"></a>02016 
<a name="l02018"></a>02018 
<a name="l02022"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5d858213b73625c365f2cf80c955b56c">02022</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02023"></a>02023   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02024"></a>02024 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceRealIndexRow</a>(<span class="keywordtype">int</span> i, <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l02025"></a>02025   {
<a name="l02026"></a>02026     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_real_(i).GetM(); j++)
<a name="l02027"></a>02027       this-&gt;val_real_(i).Index(j) = new_index(j);
<a name="l02028"></a>02028   }
<a name="l02029"></a>02029 
<a name="l02030"></a>02030 
<a name="l02032"></a>02032 
<a name="l02036"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a18e705d5da14b8ae374333f02a1801af">02036</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02037"></a>02037   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02038"></a>02038 <a class="code" href="class_seldon_1_1_matrix.php">  ReplaceImagIndexRow</a>(<span class="keywordtype">int</span> i,<a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; new_index)
<a name="l02039"></a>02039   {
<a name="l02040"></a>02040     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;val_imag_(i).GetM(); j++)
<a name="l02041"></a>02041       this-&gt;val_imag_(i).Index(j) = new_index(j);
<a name="l02042"></a>02042   }
<a name="l02043"></a>02043 
<a name="l02044"></a>02044 
<a name="l02046"></a>02046 
<a name="l02050"></a>02050   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02051"></a>02051   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::GetRealRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02052"></a>02052 <span class="keyword">    const</span>
<a name="l02053"></a>02053 <span class="keyword">  </span>{
<a name="l02054"></a>02054     <span class="keywordflow">return</span> this-&gt;val_real_(i).GetSize();
<a name="l02055"></a>02055   }
<a name="l02056"></a>02056 
<a name="l02057"></a>02057 
<a name="l02059"></a>02059 
<a name="l02063"></a>02063   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02064"></a>02064   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#af38c69e307e68125ec3304929b672e95" title="Returns the number of non-zero entries of a row.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::GetImagRowSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02065"></a>02065 <span class="keyword">    const</span>
<a name="l02066"></a>02066 <span class="keyword">  </span>{
<a name="l02067"></a>02067     <span class="keywordflow">return</span> this-&gt;val_imag_(i).GetSize();
<a name="l02068"></a>02068   }
<a name="l02069"></a>02069 
<a name="l02070"></a>02070 
<a name="l02072"></a>02072   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02073"></a>02073   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4287416fdfc3c14ca814be6ebc29153c" title="Displays non-zero values of a column.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::PrintRealRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02074"></a>02074 <span class="keyword">    const</span>
<a name="l02075"></a>02075 <span class="keyword">  </span>{
<a name="l02076"></a>02076     this-&gt;val_real_(i).Print();
<a name="l02077"></a>02077   }
<a name="l02078"></a>02078 
<a name="l02079"></a>02079 
<a name="l02081"></a>02081   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02082"></a>02082   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a6bf23b591a902626ffb78da822bc4c3e" title="Displays non-zero values of a column.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::PrintImagRow</a>(<span class="keywordtype">int</span> i)<span class="keyword"></span>
<a name="l02083"></a>02083 <span class="keyword">    const</span>
<a name="l02084"></a>02084 <span class="keyword">  </span>{
<a name="l02085"></a>02085     this-&gt;val_imag_(i).Print();
<a name="l02086"></a>02086   }
<a name="l02087"></a>02087 
<a name="l02088"></a>02088 
<a name="l02090"></a>02090 
<a name="l02095"></a>02095   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02096"></a>02096   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a0dd4ca70074352d9df0370707dd05127" title="Assembles a column.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::AssembleRealRow</a>(<span class="keywordtype">int</span> i)
<a name="l02097"></a>02097   {
<a name="l02098"></a>02098     this-&gt;val_real_(i).Assemble();
<a name="l02099"></a>02099   }
<a name="l02100"></a>02100 
<a name="l02101"></a>02101 
<a name="l02103"></a>02103 
<a name="l02108"></a>02108   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02109"></a>02109   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a44db6b452950f6d03227f0b49e8aa427" title="Assembles a column.">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::AssembleImagRow</a>(<span class="keywordtype">int</span> i)
<a name="l02110"></a>02110   {
<a name="l02111"></a>02111     this-&gt;val_imag_(i).Assemble();
<a name="l02112"></a>02112   }
<a name="l02113"></a>02113 
<a name="l02114"></a>02114 
<a name="l02116"></a>02116 
<a name="l02121"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a9cce706f016372a361f025cf92c3cb25">02121</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l02122"></a>02122   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02123"></a>02123 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> complex&lt;T&gt;&amp; val)
<a name="l02124"></a>02124   {
<a name="l02125"></a>02125     <span class="keywordflow">if</span> (i &lt;= j)
<a name="l02126"></a>02126       {
<a name="l02127"></a>02127         <span class="keywordflow">if</span> (real(val) != T(0))
<a name="l02128"></a>02128           this-&gt;val_real_(i).AddInteraction(j, real(val));
<a name="l02129"></a>02129 
<a name="l02130"></a>02130         <span class="keywordflow">if</span> (imag(val) != T(0))
<a name="l02131"></a>02131           this-&gt;val_imag_(i).AddInteraction(j, imag(val));
<a name="l02132"></a>02132       }
<a name="l02133"></a>02133   }
<a name="l02134"></a>02134 
<a name="l02135"></a>02135 
<a name="l02137"></a>02137 
<a name="l02143"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#adb9c19345b5e6f57b6d8f3dbec9bad93">02143</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02144"></a>02144   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02145"></a>02145 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col,
<a name="l02146"></a>02146                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Alloc1&gt;&amp; val)
<a name="l02147"></a>02147   {
<a name="l02148"></a>02148     <span class="keywordtype">int</span> nb_real = 0;
<a name="l02149"></a>02149     <span class="keywordtype">int</span> nb_imag = 0;
<a name="l02150"></a>02150     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> col_real(nb), col_imag(nb);
<a name="l02151"></a>02151     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> val_real(nb), val_imag(nb);
<a name="l02152"></a>02152     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nb; j++)
<a name="l02153"></a>02153       <span class="keywordflow">if</span> (i &lt;= col(j))
<a name="l02154"></a>02154         {
<a name="l02155"></a>02155           <span class="keywordflow">if</span> (real(val(j)) != T(0))
<a name="l02156"></a>02156             {
<a name="l02157"></a>02157               col_real(nb_real) = col(j);
<a name="l02158"></a>02158               val_real(nb_real) = real(val(j));
<a name="l02159"></a>02159               nb_real++;
<a name="l02160"></a>02160             }
<a name="l02161"></a>02161 
<a name="l02162"></a>02162           <span class="keywordflow">if</span> (imag(val(j)) != T(0))
<a name="l02163"></a>02163             {
<a name="l02164"></a>02164               col_imag(nb_imag) = col(j);
<a name="l02165"></a>02165               val_imag(nb_imag) = imag(val(j));
<a name="l02166"></a>02166               nb_imag++;
<a name="l02167"></a>02167             }
<a name="l02168"></a>02168         }
<a name="l02169"></a>02169 
<a name="l02170"></a>02170     this-&gt;val_real_(i).AddInteractionRow(nb_real, col_real, val_real);
<a name="l02171"></a>02171     this-&gt;val_imag_(i).AddInteractionRow(nb_imag, col_imag, val_imag);
<a name="l02172"></a>02172   }
<a name="l02173"></a>02173 
<a name="l02174"></a>02174 
<a name="l02176"></a>02176 
<a name="l02182"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a4703da0eae726628352e0e68340c4ae4">02182</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span> &lt;<span class="keyword">class</span> Alloc1&gt;
<a name="l02183"></a>02183   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ArrayRowSymComplexSparse, Allocator&gt;::</a>
<a name="l02184"></a>02184 <a class="code" href="class_seldon_1_1_matrix.php">  AddInteractionColumn</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> nb, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row,
<a name="l02185"></a>02185                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T&gt;,<a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>,Alloc1&gt;&amp; val)
<a name="l02186"></a>02186   {
<a name="l02187"></a>02187     <span class="comment">// Symmetric matrix, row = column.</span>
<a name="l02188"></a>02188     AddInteractionRow(i, nb, row, val);
<a name="l02189"></a>02189   }
<a name="l02190"></a>02190 
<a name="l02191"></a>02191 } <span class="comment">// namespace Seldon</span>
<a name="l02192"></a>02192 
<a name="l02193"></a>02193 <span class="preprocessor">#define SELDON_FILE_MATRIX_ARRAY_COMPLEX_SPARSE_CXX</span>
<a name="l02194"></a>02194 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
