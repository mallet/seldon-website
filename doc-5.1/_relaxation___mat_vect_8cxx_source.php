<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Relaxation_MatVect.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_RELAXATION_MATVECT_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="comment">/*</span>
<a name="l00024"></a>00024 <span class="comment">  Functions defined in this file</span>
<a name="l00025"></a>00025 <span class="comment"></span>
<a name="l00026"></a>00026 <span class="comment">  SOR(A, X, B, omega, iter, type_ssor)</span>
<a name="l00027"></a>00027 <span class="comment"></span>
<a name="l00028"></a>00028 <span class="comment">*/</span>
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00034"></a>00034 
<a name="l00041"></a>00041   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00042"></a>00042             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00043"></a>00043             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00044"></a><a class="code" href="namespace_seldon.php#a88ee59bd9998a522815ba8d8d4291a2d">00044</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSparse, Allocator0&gt;</a>&amp; A,
<a name="l00045"></a>00045            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00046"></a>00046            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l00047"></a>00047            <span class="keyword">const</span> T3&amp; omega, <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00048"></a>00048   {
<a name="l00049"></a>00049     T1 temp(0);
<a name="l00050"></a>00050 
<a name="l00051"></a>00051     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00054"></a>00054 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00055"></a>00055     <span class="keywordflow">if</span> (na != ma)
<a name="l00056"></a>00056       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00057"></a>00057 
<a name="l00058"></a>00058     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00059"></a>00059       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062     <span class="keywordtype">int</span>* ptr = A.GetPtr();
<a name="l00063"></a>00063     <span class="keywordtype">int</span>* ind = A.GetInd();
<a name="l00064"></a>00064     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSparse, Allocator0&gt;::pointer</a> data
<a name="l00065"></a>00065       = A.GetData();
<a name="l00066"></a>00066     T0 ajj(1);
<a name="l00067"></a>00067 
<a name="l00068"></a>00068     <span class="comment">// Forward sweep.</span>
<a name="l00069"></a>00069     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00070"></a>00070       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00071"></a>00071         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00072"></a>00072           {
<a name="l00073"></a>00073             temp = T1(0);
<a name="l00074"></a>00074             ajj = A(j, j);
<a name="l00075"></a>00075             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j] ; k &lt; ptr[j+1]; k++)
<a name="l00076"></a>00076               temp += data[k] * X(ind[k]);
<a name="l00077"></a>00077 
<a name="l00078"></a>00078             temp = B(j) - temp + ajj * X(j);
<a name="l00079"></a>00079             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00080"></a>00080           }
<a name="l00081"></a>00081 
<a name="l00082"></a>00082     <span class="comment">// Backward sweep.</span>
<a name="l00083"></a>00083     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00084"></a>00084       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00085"></a>00085         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1 ; j &gt;= 0; j--)
<a name="l00086"></a>00086           {
<a name="l00087"></a>00087             temp = T1(0);
<a name="l00088"></a>00088             ajj = A(j, j);
<a name="l00089"></a>00089             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00090"></a>00090               temp += data[k] * X(ind[k]);
<a name="l00091"></a>00091 
<a name="l00092"></a>00092             temp = B(j) - temp + ajj * X(j);
<a name="l00093"></a>00093             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00094"></a>00094           }
<a name="l00095"></a>00095   }
<a name="l00096"></a>00096 
<a name="l00097"></a>00097 
<a name="l00099"></a>00099 
<a name="l00106"></a>00106   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00107"></a>00107             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00108"></a>00108             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00109"></a><a class="code" href="namespace_seldon.php#a427d87b617eb0b5cd01ea595f7532462">00109</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, ArrayRowSparse, Allocator0&gt;</a>&amp; A,
<a name="l00110"></a>00110            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00111"></a>00111            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l00112"></a>00112            <span class="keyword">const</span> T3&amp; omega,
<a name="l00113"></a>00113            <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00114"></a>00114   {
<a name="l00115"></a>00115     T1 temp(0);
<a name="l00116"></a>00116 
<a name="l00117"></a>00117     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00120"></a>00120 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00121"></a>00121     <span class="keywordflow">if</span> (na != ma)
<a name="l00122"></a>00122       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00123"></a>00123 
<a name="l00124"></a>00124     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00125"></a>00125       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00126"></a>00126 <span class="preprocessor">#endif</span>
<a name="l00127"></a>00127 <span class="preprocessor"></span>
<a name="l00128"></a>00128     T0 ajj(1);
<a name="l00129"></a>00129 
<a name="l00130"></a>00130     <span class="comment">// Forward sweep.</span>
<a name="l00131"></a>00131     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00132"></a>00132       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00133"></a>00133         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00134"></a>00134           {
<a name="l00135"></a>00135             temp = T1(0);
<a name="l00136"></a>00136             ajj = T0(0);
<a name="l00137"></a>00137             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00138"></a>00138               {
<a name="l00139"></a>00139                 temp += A.Value(j,k) * X(A.Index(j,k));
<a name="l00140"></a>00140                 <span class="keywordflow">if</span> (A.Index(j,k) == j)
<a name="l00141"></a>00141                   ajj += A.Value(j,k);
<a name="l00142"></a>00142               }
<a name="l00143"></a>00143 
<a name="l00144"></a>00144             temp = B(j) - temp + ajj * X(j);
<a name="l00145"></a>00145             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00146"></a>00146           }
<a name="l00147"></a>00147 
<a name="l00148"></a>00148     <span class="comment">// Backward sweep.</span>
<a name="l00149"></a>00149     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00150"></a>00150       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00151"></a>00151         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00152"></a>00152           {
<a name="l00153"></a>00153             temp = T1(0);
<a name="l00154"></a>00154             ajj = T0(0);
<a name="l00155"></a>00155             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00156"></a>00156               {
<a name="l00157"></a>00157                 temp += A.Value(j,k) * X(A.Index(j,k));
<a name="l00158"></a>00158                 <span class="keywordflow">if</span> (A.Index(j,k) == j)
<a name="l00159"></a>00159                   ajj += A.Value(j,k);
<a name="l00160"></a>00160               }
<a name="l00161"></a>00161 
<a name="l00162"></a>00162             temp = B(j) - temp + ajj * X(j);
<a name="l00163"></a>00163             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00164"></a>00164           }
<a name="l00165"></a>00165   }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 
<a name="l00169"></a>00169 
<a name="l00176"></a>00176   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00177"></a>00177             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00178"></a>00178             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00179"></a><a class="code" href="namespace_seldon.php#a89145322d400b380869edba5377bf0e8">00179</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSymSparse, Allocator0&gt;</a>&amp; A,
<a name="l00180"></a>00180            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00181"></a>00181            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l00182"></a>00182            <span class="keyword">const</span> T3&amp; omega,<span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00183"></a>00183   {
<a name="l00184"></a>00184     T1 temp(0);
<a name="l00185"></a>00185 
<a name="l00186"></a>00186     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00187"></a>00187 
<a name="l00188"></a>00188 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00189"></a>00189 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00190"></a>00190     <span class="keywordflow">if</span> (na != ma)
<a name="l00191"></a>00191       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00192"></a>00192 
<a name="l00193"></a>00193     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00194"></a>00194       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00195"></a>00195 <span class="preprocessor">#endif</span>
<a name="l00196"></a>00196 <span class="preprocessor"></span>
<a name="l00197"></a>00197     <span class="keywordtype">int</span>* ptr = A.GetPtr();
<a name="l00198"></a>00198     <span class="keywordtype">int</span>* ind = A.GetInd();
<a name="l00199"></a>00199     T0* data = A.GetData();
<a name="l00200"></a>00200 
<a name="l00201"></a>00201     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a> Y(ma);
<a name="l00202"></a>00202     Y.Zero();
<a name="l00203"></a>00203     T0 ajj(1);
<a name="l00204"></a>00204     <span class="keywordtype">int</span> p;
<a name="l00205"></a>00205     T0 val(0);
<a name="l00206"></a>00206     <span class="comment">// Let us consider the following splitting : A = D - L - U</span>
<a name="l00207"></a>00207     <span class="comment">// D diagonal of A</span>
<a name="l00208"></a>00208     <span class="comment">// L lower part of A</span>
<a name="l00209"></a>00209     <span class="comment">// U upper part of A, A is symmetric, so L = U^t</span>
<a name="l00210"></a>00210 
<a name="l00211"></a>00211     <span class="comment">// Forward sweep</span>
<a name="l00212"></a>00212     <span class="comment">// (D/omega - L) X^{n+1/2} = (U + (1-omega)/omega D) X^n + B</span>
<a name="l00213"></a>00213     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00214"></a>00214       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00215"></a>00215         {
<a name="l00216"></a>00216           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00217"></a>00217             {
<a name="l00218"></a>00218               <span class="comment">// First we do X = (U + (1-omega)/omega D) X + B</span>
<a name="l00219"></a>00219               temp = T1(0);
<a name="l00220"></a>00220               ajj = T0(0);
<a name="l00221"></a>00221               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00222"></a>00222                 {
<a name="l00223"></a>00223                   p = ind[k];
<a name="l00224"></a>00224                   val = data[k];
<a name="l00225"></a>00225                   <span class="keywordflow">if</span> (p == j)
<a name="l00226"></a>00226                     ajj += val;
<a name="l00227"></a>00227                   <span class="keywordflow">else</span>
<a name="l00228"></a>00228                     temp += val * X(p);
<a name="l00229"></a>00229                 }
<a name="l00230"></a>00230 
<a name="l00231"></a>00231               temp = B(j) - temp;
<a name="l00232"></a>00232               X(j) = (T2(1) - omega) / omega * ajj * X(j) + temp;
<a name="l00233"></a>00233             }
<a name="l00234"></a>00234 
<a name="l00235"></a>00235           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00236"></a>00236             {
<a name="l00237"></a>00237               ajj = T0(0);
<a name="l00238"></a>00238               <span class="comment">// Then we solve (D/omega - L) X = X</span>
<a name="l00239"></a>00239               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00240"></a>00240                 {
<a name="l00241"></a>00241                   p = ind[k];
<a name="l00242"></a>00242                   val = data[k];
<a name="l00243"></a>00243                   <span class="keywordflow">if</span> (p == j)
<a name="l00244"></a>00244                     ajj += val;
<a name="l00245"></a>00245                 }
<a name="l00246"></a>00246               X(j) *= omega / ajj;
<a name="l00247"></a>00247               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00248"></a>00248                 {
<a name="l00249"></a>00249                   p = ind[k];
<a name="l00250"></a>00250                   val = data[k];
<a name="l00251"></a>00251                   <span class="keywordflow">if</span> (p != j)
<a name="l00252"></a>00252                     X(p) -= val*X(j);
<a name="l00253"></a>00253                 }
<a name="l00254"></a>00254             }
<a name="l00255"></a>00255         }
<a name="l00256"></a>00256 
<a name="l00257"></a>00257 
<a name="l00258"></a>00258     <span class="comment">// Backward sweep.</span>
<a name="l00259"></a>00259     <span class="comment">// (D/omega - U) X^{n+1} = (L + (1-omega)/omega D) X^{n+1/2} + B</span>
<a name="l00260"></a>00260     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00261"></a>00261       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00262"></a>00262         {
<a name="l00263"></a>00263           Y.Zero();
<a name="l00264"></a>00264           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00265"></a>00265             {
<a name="l00266"></a>00266               ajj = T0(0);
<a name="l00267"></a>00267               <span class="comment">// Then we compute X = (L + (1-omega)/omega D) X + B.</span>
<a name="l00268"></a>00268               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00269"></a>00269                 {
<a name="l00270"></a>00270                   p = ind[k];
<a name="l00271"></a>00271                   val = data[k];
<a name="l00272"></a>00272                   <span class="keywordflow">if</span> (p == j)
<a name="l00273"></a>00273                     ajj += val;
<a name="l00274"></a>00274                   <span class="keywordflow">else</span>
<a name="l00275"></a>00275                     Y(p) += val*X(j);
<a name="l00276"></a>00276                 }
<a name="l00277"></a>00277 
<a name="l00278"></a>00278               X(j) = (T2(1) - omega) / omega * ajj * X(j) + B(j) - Y(j);
<a name="l00279"></a>00279             }
<a name="l00280"></a>00280 
<a name="l00281"></a>00281           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00282"></a>00282             {
<a name="l00283"></a>00283               temp = T1(0);
<a name="l00284"></a>00284               ajj = T0(0);
<a name="l00285"></a>00285               <span class="comment">// Then we solve (D/omega - U) X = X.</span>
<a name="l00286"></a>00286               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr[j]; k &lt; ptr[j+1]; k++)
<a name="l00287"></a>00287                 {
<a name="l00288"></a>00288                   p = ind[k];
<a name="l00289"></a>00289                   val = data[k];
<a name="l00290"></a>00290                   <span class="keywordflow">if</span> (p == j)
<a name="l00291"></a>00291                     ajj += val;
<a name="l00292"></a>00292                   <span class="keywordflow">else</span>
<a name="l00293"></a>00293                     temp += val*X(p);
<a name="l00294"></a>00294                 }
<a name="l00295"></a>00295               X(j) = (X(j) - temp) * omega / ajj;
<a name="l00296"></a>00296             }
<a name="l00297"></a>00297         }
<a name="l00298"></a>00298   }
<a name="l00299"></a>00299 
<a name="l00300"></a>00300 
<a name="l00302"></a>00302 
<a name="l00309"></a>00309   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00310"></a>00310             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00311"></a>00311             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00312"></a><a class="code" href="namespace_seldon.php#a5406a91fa55dd7eb4b15f2f8dd0fe82c">00312</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, ArrayRowSymSparse, Allocator0&gt;</a>&amp; A,
<a name="l00313"></a>00313            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00314"></a>00314            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l00315"></a>00315            <span class="keyword">const</span> T3&amp; omega,<span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00316"></a>00316   {
<a name="l00317"></a>00317     T1 temp(0);
<a name="l00318"></a>00318 
<a name="l00319"></a>00319     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00320"></a>00320 
<a name="l00321"></a>00321 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00322"></a>00322 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00323"></a>00323     <span class="keywordflow">if</span> (na != ma)
<a name="l00324"></a>00324       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00325"></a>00325 
<a name="l00326"></a>00326     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00327"></a>00327       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00328"></a>00328 <span class="preprocessor">#endif</span>
<a name="l00329"></a>00329 <span class="preprocessor"></span>
<a name="l00330"></a>00330 
<a name="l00331"></a>00331     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2,Storage2,Allocator2&gt;</a> Y(ma);
<a name="l00332"></a>00332     Y.Zero();
<a name="l00333"></a>00333     T0 ajj(1);
<a name="l00334"></a>00334     <span class="keywordtype">int</span> p; T0 val(0);
<a name="l00335"></a>00335     <span class="comment">// Let us consider the following splitting : A = D - L - U</span>
<a name="l00336"></a>00336     <span class="comment">// D diagonal of A</span>
<a name="l00337"></a>00337     <span class="comment">// L lower part of A</span>
<a name="l00338"></a>00338     <span class="comment">// U upper part of A, A is symmetric, so L = U^t</span>
<a name="l00339"></a>00339 
<a name="l00340"></a>00340     <span class="comment">// Forward sweep.</span>
<a name="l00341"></a>00341     <span class="comment">// (D/omega - L) X^{n+1/2} = (U + (1-omega)/omega D) X^n + B</span>
<a name="l00342"></a>00342     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00343"></a>00343       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00344"></a>00344         {
<a name="l00345"></a>00345           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00346"></a>00346             {
<a name="l00347"></a>00347               <span class="comment">// First we do X = (U + (1-omega)/omega D) X + B.</span>
<a name="l00348"></a>00348               temp = T1(0);
<a name="l00349"></a>00349               ajj = T0(0);
<a name="l00350"></a>00350               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00351"></a>00351                 {
<a name="l00352"></a>00352                   p = A.Index(j,k);
<a name="l00353"></a>00353                   val = A.Value(j,k);
<a name="l00354"></a>00354                   <span class="keywordflow">if</span> (p == j)
<a name="l00355"></a>00355                     ajj += val;
<a name="l00356"></a>00356                   <span class="keywordflow">else</span>
<a name="l00357"></a>00357                     temp += val * X(p);
<a name="l00358"></a>00358                 }
<a name="l00359"></a>00359 
<a name="l00360"></a>00360               temp = B(j) - temp;
<a name="l00361"></a>00361               X(j) = (T2(1) - omega) / omega * ajj * X(j) + temp;
<a name="l00362"></a>00362             }
<a name="l00363"></a>00363 
<a name="l00364"></a>00364           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00365"></a>00365             {
<a name="l00366"></a>00366               ajj = T0(0);
<a name="l00367"></a>00367               <span class="comment">// Then we solve (D/omega - L) X = X.</span>
<a name="l00368"></a>00368               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00369"></a>00369                 {
<a name="l00370"></a>00370                   p = A.Index(j,k);
<a name="l00371"></a>00371                   val = A.Value(j,k);
<a name="l00372"></a>00372                   <span class="keywordflow">if</span> (p == j)
<a name="l00373"></a>00373                     ajj += val;
<a name="l00374"></a>00374                 }
<a name="l00375"></a>00375               X(j) *= omega / ajj;
<a name="l00376"></a>00376               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0 ; k &lt; A.GetRowSize(j); k++)
<a name="l00377"></a>00377                 {
<a name="l00378"></a>00378                   p = A.Index(j,k);
<a name="l00379"></a>00379                   val = A.Value(j,k);
<a name="l00380"></a>00380                   <span class="keywordflow">if</span> (p != j)
<a name="l00381"></a>00381                     X(p) -= val*X(j);
<a name="l00382"></a>00382                 }
<a name="l00383"></a>00383             }
<a name="l00384"></a>00384         }
<a name="l00385"></a>00385 
<a name="l00386"></a>00386 
<a name="l00387"></a>00387     <span class="comment">// Backward sweep.</span>
<a name="l00388"></a>00388     <span class="comment">// (D/omega - U) X^{n+1} = (L + (1-omega)/omega D) X^{n+1/2} + B</span>
<a name="l00389"></a>00389     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00390"></a>00390       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00391"></a>00391         {
<a name="l00392"></a>00392           Y.Zero();
<a name="l00393"></a>00393           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00394"></a>00394             {
<a name="l00395"></a>00395               ajj = T0(0);
<a name="l00396"></a>00396               <span class="comment">// Then we compute X = (L + (1-omega)/omega D) X + B.</span>
<a name="l00397"></a>00397               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00398"></a>00398                 {
<a name="l00399"></a>00399                   p = A.Index(j,k);
<a name="l00400"></a>00400                   val = A.Value(j,k);
<a name="l00401"></a>00401                   <span class="keywordflow">if</span> (p == j)
<a name="l00402"></a>00402                     ajj += val;
<a name="l00403"></a>00403                   <span class="keywordflow">else</span>
<a name="l00404"></a>00404                     Y(p) += val*X(j);
<a name="l00405"></a>00405                 }
<a name="l00406"></a>00406 
<a name="l00407"></a>00407               X(j) = (T2(1) - omega) / omega * ajj * X(j) + B(j) - Y(j);
<a name="l00408"></a>00408             }
<a name="l00409"></a>00409 
<a name="l00410"></a>00410           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00411"></a>00411             {
<a name="l00412"></a>00412               temp = T1(0);
<a name="l00413"></a>00413               ajj = T0(0);
<a name="l00414"></a>00414               <span class="comment">// Then we solve (D/omega - U) X = X.</span>
<a name="l00415"></a>00415               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRowSize(j); k++)
<a name="l00416"></a>00416                 {
<a name="l00417"></a>00417                   p = A.Index(j,k);
<a name="l00418"></a>00418                   val = A.Value(j,k);
<a name="l00419"></a>00419                   <span class="keywordflow">if</span> (p == j)
<a name="l00420"></a>00420                     ajj += val;
<a name="l00421"></a>00421                   <span class="keywordflow">else</span>
<a name="l00422"></a>00422                     temp += val * X(p);
<a name="l00423"></a>00423                 }
<a name="l00424"></a>00424               X(j) = (X(j) - temp) * omega / ajj;
<a name="l00425"></a>00425             }
<a name="l00426"></a>00426         }
<a name="l00427"></a>00427 
<a name="l00428"></a>00428   }
<a name="l00429"></a>00429 
<a name="l00430"></a>00430 
<a name="l00432"></a>00432 
<a name="l00439"></a>00439   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00440"></a>00440             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00441"></a>00441             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00442"></a><a class="code" href="namespace_seldon.php#a03ea5195e9dc501c961cf679ae2d6846">00442</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowComplexSparse, Allocator0&gt;</a>&amp; A,
<a name="l00443"></a>00443            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T2&gt;, Storage2, Allocator2&gt;&amp; X,
<a name="l00444"></a>00444            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; B,
<a name="l00445"></a>00445            <span class="keyword">const</span> T3&amp; omega, <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00446"></a>00446   {
<a name="l00447"></a>00447     complex&lt;T1&gt; temp(0);
<a name="l00448"></a>00448 
<a name="l00449"></a>00449     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00450"></a>00450 
<a name="l00451"></a>00451 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00452"></a>00452 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00453"></a>00453     <span class="keywordflow">if</span> (na != ma)
<a name="l00454"></a>00454       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00455"></a>00455 
<a name="l00456"></a>00456     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00457"></a>00457       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00458"></a>00458 <span class="preprocessor">#endif</span>
<a name="l00459"></a>00459 <span class="preprocessor"></span>
<a name="l00460"></a>00460     <span class="keywordtype">int</span>* ptr_real = A.GetRealPtr();
<a name="l00461"></a>00461     <span class="keywordtype">int</span>* ptr_imag = A.GetImagPtr();
<a name="l00462"></a>00462     <span class="keywordtype">int</span>* ind_real = A.GetRealInd();
<a name="l00463"></a>00463     <span class="keywordtype">int</span>* ind_imag = A.GetImagInd();
<a name="l00464"></a>00464     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowComplexSparse, Allocator0&gt;::pointer</a>
<a name="l00465"></a>00465       data_real = A.GetRealData();
<a name="l00466"></a>00466     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowComplexSparse, Allocator0&gt;::pointer</a>
<a name="l00467"></a>00467       data_imag = A.GetImagData();
<a name="l00468"></a>00468     complex&lt;T0&gt; ajj(1);
<a name="l00469"></a>00469 
<a name="l00470"></a>00470     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00471"></a>00471       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00472"></a>00472         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00473"></a>00473           {
<a name="l00474"></a>00474             temp = complex&lt;T1&gt;(0);
<a name="l00475"></a>00475             ajj = A(j,j);
<a name="l00476"></a>00476             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00477"></a>00477               temp += data_real[k] * X(ind_real[k]);
<a name="l00478"></a>00478 
<a name="l00479"></a>00479             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00480"></a>00480               temp += complex&lt;T1&gt;(0, data_imag[k]) * X(ind_imag[k]);
<a name="l00481"></a>00481 
<a name="l00482"></a>00482             temp = B(j) - temp + ajj*X(j);
<a name="l00483"></a>00483             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00484"></a>00484           }
<a name="l00485"></a>00485 
<a name="l00486"></a>00486     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00487"></a>00487       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00488"></a>00488         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00489"></a>00489           {
<a name="l00490"></a>00490             temp = complex&lt;T1&gt;(0);
<a name="l00491"></a>00491             ajj = A(j,j);
<a name="l00492"></a>00492             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00493"></a>00493               temp += data_real[k] * X(ind_real[k]);
<a name="l00494"></a>00494 
<a name="l00495"></a>00495             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00496"></a>00496               temp += complex&lt;T1&gt;(0, data_imag[k]) * X(ind_imag[k]);
<a name="l00497"></a>00497 
<a name="l00498"></a>00498             temp = B(j) - temp + ajj * X(j);
<a name="l00499"></a>00499             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00500"></a>00500           }
<a name="l00501"></a>00501   }
<a name="l00502"></a>00502 
<a name="l00503"></a>00503 
<a name="l00505"></a>00505 
<a name="l00512"></a>00512   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00513"></a>00513             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00514"></a>00514             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00515"></a><a class="code" href="namespace_seldon.php#a434279d95f7ec5d0fe47ea33ebb36014">00515</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, ArrayRowComplexSparse, Allocator0&gt;</a>&amp; A,
<a name="l00516"></a>00516            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T2&gt;, Storage2, Allocator2&gt;&amp; X,
<a name="l00517"></a>00517            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; B,
<a name="l00518"></a>00518            <span class="keyword">const</span> T3&amp; omega, <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00519"></a>00519   {
<a name="l00520"></a>00520     complex&lt;T1&gt; temp(0);
<a name="l00521"></a>00521 
<a name="l00522"></a>00522     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00523"></a>00523 
<a name="l00524"></a>00524 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00525"></a>00525 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00526"></a>00526     <span class="keywordflow">if</span> (na != ma)
<a name="l00527"></a>00527       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00528"></a>00528 
<a name="l00529"></a>00529     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00530"></a>00530       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00531"></a>00531 <span class="preprocessor">#endif</span>
<a name="l00532"></a>00532 <span class="preprocessor"></span>
<a name="l00533"></a>00533     complex&lt;T0&gt; ajj(1);
<a name="l00534"></a>00534 
<a name="l00535"></a>00535     <span class="keywordflow">if</span> (type_ssor%2 == 0)
<a name="l00536"></a>00536       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00537"></a>00537         {
<a name="l00538"></a>00538           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00539"></a>00539             {
<a name="l00540"></a>00540               temp = complex&lt;T1&gt;(0);
<a name="l00541"></a>00541               ajj = complex&lt;T0&gt;(0);
<a name="l00542"></a>00542               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00543"></a>00543                 {
<a name="l00544"></a>00544                   temp += A.ValueReal(j,k) * X(A.IndexReal(j,k));
<a name="l00545"></a>00545                   <span class="keywordflow">if</span> (A.IndexReal(j,k) == j)
<a name="l00546"></a>00546                     ajj += complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00547"></a>00547                 }
<a name="l00548"></a>00548               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00549"></a>00549                 {
<a name="l00550"></a>00550                   temp += complex&lt;T0&gt;(0,A.ValueImag(j,k))
<a name="l00551"></a>00551                     * X(A.IndexImag(j,k));
<a name="l00552"></a>00552                   <span class="keywordflow">if</span> (A.IndexImag(j,k) == j)
<a name="l00553"></a>00553                     ajj += complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00554"></a>00554                 }
<a name="l00555"></a>00555 
<a name="l00556"></a>00556               temp = B(j) - temp + ajj * X(j);
<a name="l00557"></a>00557               X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00558"></a>00558             }
<a name="l00559"></a>00559         }
<a name="l00560"></a>00560 
<a name="l00561"></a>00561     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00562"></a>00562       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00563"></a>00563         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00564"></a>00564           {
<a name="l00565"></a>00565             temp = complex&lt;T1&gt;(0);
<a name="l00566"></a>00566             ajj = complex&lt;T0&gt;(0);
<a name="l00567"></a>00567             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00568"></a>00568               {
<a name="l00569"></a>00569                 temp += A.ValueReal(j,k) * X(A.IndexReal(j,k));
<a name="l00570"></a>00570                 <span class="keywordflow">if</span> (A.IndexReal(j,k) == j)
<a name="l00571"></a>00571                   ajj += complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00572"></a>00572               }
<a name="l00573"></a>00573             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00574"></a>00574               {
<a name="l00575"></a>00575                 temp += complex&lt;T0&gt;(0, A.ValueImag(j,k))
<a name="l00576"></a>00576                   * X(A.IndexImag(j,k));
<a name="l00577"></a>00577                 <span class="keywordflow">if</span> (A.IndexImag(j,k) == j)
<a name="l00578"></a>00578                   ajj += complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00579"></a>00579               }
<a name="l00580"></a>00580 
<a name="l00581"></a>00581             temp = B(j) - temp + ajj * X(j);
<a name="l00582"></a>00582             X(j) = (T2(1) - omega) * X(j) + omega * temp / ajj;
<a name="l00583"></a>00583           }
<a name="l00584"></a>00584   }
<a name="l00585"></a>00585 
<a name="l00586"></a>00586 
<a name="l00588"></a>00588 
<a name="l00595"></a>00595   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00596"></a>00596             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00597"></a>00597             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00598"></a><a class="code" href="namespace_seldon.php#aee738d3acf493593417c39ca9313f882">00598</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSymComplexSparse, Allocator0&gt;</a>&amp; A,
<a name="l00599"></a>00599            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T2&gt;, Storage2, Allocator2&gt;&amp; X,
<a name="l00600"></a>00600            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; B,
<a name="l00601"></a>00601            <span class="keyword">const</span> T3&amp; omega, <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00602"></a>00602   {
<a name="l00603"></a>00603     complex&lt;T1&gt; temp(0);
<a name="l00604"></a>00604 
<a name="l00605"></a>00605     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00606"></a>00606 
<a name="l00607"></a>00607 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00608"></a>00608 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00609"></a>00609     <span class="keywordflow">if</span> (na != ma)
<a name="l00610"></a>00610       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00611"></a>00611 
<a name="l00612"></a>00612     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00613"></a>00613       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00614"></a>00614 <span class="preprocessor">#endif</span>
<a name="l00615"></a>00615 <span class="preprocessor"></span>
<a name="l00616"></a>00616     <span class="keywordtype">int</span>* ptr_real = A.GetRealPtr();
<a name="l00617"></a>00617     <span class="keywordtype">int</span>* ptr_imag = A.GetImagPtr();
<a name="l00618"></a>00618     <span class="keywordtype">int</span>* ind_real = A.GetRealInd();
<a name="l00619"></a>00619     <span class="keywordtype">int</span>* ind_imag = A.GetImagInd();
<a name="l00620"></a>00620     T0* data_real = A.GetRealData();
<a name="l00621"></a>00621     T0* data_imag = A.GetImagData();
<a name="l00622"></a>00622 
<a name="l00623"></a>00623     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;T2&gt;</a>, Storage2, Allocator2&gt; Y(ma);
<a name="l00624"></a>00624     Y.Zero();
<a name="l00625"></a>00625     complex&lt;T0&gt; ajj(1);
<a name="l00626"></a>00626     <span class="keywordtype">int</span> p;
<a name="l00627"></a>00627     complex&lt;T0&gt; val(0);
<a name="l00628"></a>00628 
<a name="l00629"></a>00629     <span class="comment">// Let us consider the following splitting : A = D - L - U</span>
<a name="l00630"></a>00630     <span class="comment">// D diagonal of A</span>
<a name="l00631"></a>00631     <span class="comment">// L lower part of A</span>
<a name="l00632"></a>00632     <span class="comment">// U upper part of A, A is symmetric, so L = U^t</span>
<a name="l00633"></a>00633     <span class="comment">// forward sweep</span>
<a name="l00634"></a>00634     <span class="comment">// (D/omega - L) X^{n+1/2} = (U + (1-omega)/omega D) X^n + B</span>
<a name="l00635"></a>00635     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00636"></a>00636       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00637"></a>00637         {
<a name="l00638"></a>00638           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00639"></a>00639             {
<a name="l00640"></a>00640               <span class="comment">// first we do X = (U + (1-omega)/omega D) X + B</span>
<a name="l00641"></a>00641               temp = complex&lt;T1&gt;(0);
<a name="l00642"></a>00642               ajj = complex&lt;T0&gt;(0);
<a name="l00643"></a>00643               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00644"></a>00644                 {
<a name="l00645"></a>00645                   p = ind_real[k];
<a name="l00646"></a>00646                   val = complex&lt;T0&gt;(data_real[k], 0);
<a name="l00647"></a>00647                   <span class="keywordflow">if</span> (p == j)
<a name="l00648"></a>00648                     ajj += val;
<a name="l00649"></a>00649                   <span class="keywordflow">else</span>
<a name="l00650"></a>00650                     temp += val * X(p);
<a name="l00651"></a>00651                 }
<a name="l00652"></a>00652               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00653"></a>00653                 {
<a name="l00654"></a>00654                   p = ind_imag[k];
<a name="l00655"></a>00655                   val = complex&lt;T0&gt;(0, data_imag[k]);
<a name="l00656"></a>00656                   <span class="keywordflow">if</span> (p == j)
<a name="l00657"></a>00657                     ajj += val;
<a name="l00658"></a>00658                   <span class="keywordflow">else</span>
<a name="l00659"></a>00659                     temp += val * X(p);
<a name="l00660"></a>00660                 }
<a name="l00661"></a>00661 
<a name="l00662"></a>00662               temp = B(j) - temp;
<a name="l00663"></a>00663               X(j) = (T2(1) - omega) / omega * ajj * X(j) + temp;
<a name="l00664"></a>00664             }
<a name="l00665"></a>00665 
<a name="l00666"></a>00666           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00667"></a>00667             {
<a name="l00668"></a>00668               ajj = complex&lt;T0&gt;(0);
<a name="l00669"></a>00669               <span class="comment">// Then we solve (D/omega - L) X = X.</span>
<a name="l00670"></a>00670               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00671"></a>00671                 {
<a name="l00672"></a>00672                   p = ind_real[k];
<a name="l00673"></a>00673                   val = complex&lt;T0&gt;(data_real[k], 0);
<a name="l00674"></a>00674                   <span class="keywordflow">if</span> (p == j)
<a name="l00675"></a>00675                     ajj += val;
<a name="l00676"></a>00676                 }
<a name="l00677"></a>00677 
<a name="l00678"></a>00678               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00679"></a>00679                 {
<a name="l00680"></a>00680                   p = ind_imag[k];
<a name="l00681"></a>00681                   val = complex&lt;T0&gt;(0, data_imag[k]);
<a name="l00682"></a>00682                   <span class="keywordflow">if</span> (p == j)
<a name="l00683"></a>00683                     ajj += val;
<a name="l00684"></a>00684                 }
<a name="l00685"></a>00685               X(j) *= omega / ajj;
<a name="l00686"></a>00686 
<a name="l00687"></a>00687               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00688"></a>00688                 {
<a name="l00689"></a>00689                   p = ind_real[k];
<a name="l00690"></a>00690                   val = complex&lt;T0&gt;(data_real[k], 0);
<a name="l00691"></a>00691                   <span class="keywordflow">if</span> (p != j)
<a name="l00692"></a>00692                     X(p) -= val*X(j);
<a name="l00693"></a>00693                 }
<a name="l00694"></a>00694 
<a name="l00695"></a>00695               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00696"></a>00696                 {
<a name="l00697"></a>00697                   p = ind_imag[k];
<a name="l00698"></a>00698                   val = complex&lt;T0&gt;(0, data_imag[k]);
<a name="l00699"></a>00699                   <span class="keywordflow">if</span> (p != j)
<a name="l00700"></a>00700                     X(p) -= val*X(j);
<a name="l00701"></a>00701                 }
<a name="l00702"></a>00702             }
<a name="l00703"></a>00703         }
<a name="l00704"></a>00704 
<a name="l00705"></a>00705     <span class="comment">// Backward sweep.</span>
<a name="l00706"></a>00706     <span class="comment">// (D/omega - U) X^{n+1} = (L + (1-omega)/omega D) X^{n+1/2} + B</span>
<a name="l00707"></a>00707     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00708"></a>00708       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00709"></a>00709         {
<a name="l00710"></a>00710           Y.Zero();
<a name="l00711"></a>00711 
<a name="l00712"></a>00712           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00713"></a>00713             {
<a name="l00714"></a>00714               ajj = complex&lt;T0&gt;(0);
<a name="l00715"></a>00715               <span class="comment">// Then we compute X = (L + (1-omega)/omega D) X + B.</span>
<a name="l00716"></a>00716               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00717"></a>00717                 {
<a name="l00718"></a>00718                   p = ind_real[k];
<a name="l00719"></a>00719                   val = complex&lt;T0&gt;(data_real[k], 0);
<a name="l00720"></a>00720                   <span class="keywordflow">if</span> (p == j)
<a name="l00721"></a>00721                     ajj += val;
<a name="l00722"></a>00722                   <span class="keywordflow">else</span>
<a name="l00723"></a>00723                     Y(p) += val * X(j);
<a name="l00724"></a>00724                 }
<a name="l00725"></a>00725 
<a name="l00726"></a>00726               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00727"></a>00727                 {
<a name="l00728"></a>00728                   p = ind_imag[k];
<a name="l00729"></a>00729                   val = complex&lt;T0&gt;(0, data_imag[k]);
<a name="l00730"></a>00730                   <span class="keywordflow">if</span> (p == j)
<a name="l00731"></a>00731                     ajj += val;
<a name="l00732"></a>00732                   <span class="keywordflow">else</span>
<a name="l00733"></a>00733                     Y(p) += val * X(j);
<a name="l00734"></a>00734                 }
<a name="l00735"></a>00735               X(j) = (T2(1) - omega) / omega * ajj * X(j) + B(j) - Y(j);
<a name="l00736"></a>00736             }
<a name="l00737"></a>00737 
<a name="l00738"></a>00738           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = (ma-1); j &gt;= 0; j--)
<a name="l00739"></a>00739             {
<a name="l00740"></a>00740               temp = complex&lt;T1&gt;(0);
<a name="l00741"></a>00741               ajj = complex&lt;T0&gt;(0);
<a name="l00742"></a>00742               <span class="comment">// Then we solve (D/omega - U) X = X.</span>
<a name="l00743"></a>00743               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_real[j]; k &lt; ptr_real[j+1]; k++)
<a name="l00744"></a>00744                 {
<a name="l00745"></a>00745                   p = ind_real[k];
<a name="l00746"></a>00746                   val = complex&lt;T0&gt;(data_real[k], 0);
<a name="l00747"></a>00747                   <span class="keywordflow">if</span> (p == j)
<a name="l00748"></a>00748                     ajj += val;
<a name="l00749"></a>00749                   <span class="keywordflow">else</span>
<a name="l00750"></a>00750                     temp += val * X(p);
<a name="l00751"></a>00751                 }
<a name="l00752"></a>00752 
<a name="l00753"></a>00753               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = ptr_imag[j]; k &lt; ptr_imag[j+1]; k++)
<a name="l00754"></a>00754                 {
<a name="l00755"></a>00755                   p = ind_imag[k];
<a name="l00756"></a>00756                   val = complex&lt;T0&gt;(0, data_imag[k]);
<a name="l00757"></a>00757                   <span class="keywordflow">if</span> (p == j)
<a name="l00758"></a>00758                     ajj += val;
<a name="l00759"></a>00759                   <span class="keywordflow">else</span>
<a name="l00760"></a>00760                     temp += val * X(p);
<a name="l00761"></a>00761                 }
<a name="l00762"></a>00762               X(j) = (X(j) - temp) * omega / ajj;
<a name="l00763"></a>00763             }
<a name="l00764"></a>00764         }
<a name="l00765"></a>00765   }
<a name="l00766"></a>00766 
<a name="l00767"></a>00767 
<a name="l00769"></a>00769 
<a name="l00776"></a>00776   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00777"></a>00777             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00778"></a>00778             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3&gt;
<a name="l00779"></a><a class="code" href="namespace_seldon.php#ac3c0b66d980a47deaddb6b98f0bd176e">00779</a>   <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, ArrayRowSymComplexSparse, Allocator0&gt;</a>&amp; A,
<a name="l00780"></a>00780            <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T2&gt;, Storage2, Allocator2&gt;&amp; X,
<a name="l00781"></a>00781            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;complex&lt;T1&gt;, Storage1, Allocator1&gt;&amp; B,
<a name="l00782"></a>00782            <span class="keyword">const</span> T3&amp; omega, <span class="keywordtype">int</span> iter, <span class="keywordtype">int</span> type_ssor = 2)
<a name="l00783"></a>00783   {
<a name="l00784"></a>00784     complex&lt;T1&gt; temp(0);
<a name="l00785"></a>00785 
<a name="l00786"></a>00786     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l00787"></a>00787 
<a name="l00788"></a>00788 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00789"></a>00789 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l00790"></a>00790     <span class="keywordflow">if</span> (na != ma)
<a name="l00791"></a>00791       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix must be squared.&quot;</span>);
<a name="l00792"></a>00792 
<a name="l00793"></a>00793     <span class="keywordflow">if</span> (ma != X.GetLength() || ma != B.GetLength())
<a name="l00794"></a>00794       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SOR&quot;</span>, <span class="stringliteral">&quot;Matrix and vector dimensions are incompatible.&quot;</span>);
<a name="l00795"></a>00795 <span class="preprocessor">#endif</span>
<a name="l00796"></a>00796 <span class="preprocessor"></span>
<a name="l00797"></a>00797     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;complex&lt;T2&gt;</a>, Storage2, Allocator2&gt; Y(ma);
<a name="l00798"></a>00798     Y.Zero();
<a name="l00799"></a>00799     complex&lt;T0&gt; ajj(1);
<a name="l00800"></a>00800     <span class="keywordtype">int</span> p;
<a name="l00801"></a>00801     complex&lt;T0&gt; val(0);
<a name="l00802"></a>00802     <span class="comment">// Let us consider the following splitting : A = D - L - U</span>
<a name="l00803"></a>00803     <span class="comment">// D diagonal of A</span>
<a name="l00804"></a>00804     <span class="comment">// L lower part of A</span>
<a name="l00805"></a>00805     <span class="comment">// U upper part of A, A is symmetric, so L = U^t</span>
<a name="l00806"></a>00806     <span class="comment">// forward sweep</span>
<a name="l00807"></a>00807     <span class="comment">// (D/omega - L) X^{n+1/2} = (U + (1-omega)/omega D) X^n + B</span>
<a name="l00808"></a>00808     <span class="keywordflow">if</span> (type_ssor % 2 == 0)
<a name="l00809"></a>00809       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00810"></a>00810         {
<a name="l00811"></a>00811           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00812"></a>00812             {
<a name="l00813"></a>00813               <span class="comment">// First we do X = (U + (1-omega)/omega D) X + B</span>
<a name="l00814"></a>00814               temp = complex&lt;T1&gt;(0);
<a name="l00815"></a>00815               ajj = complex&lt;T0&gt;(0);
<a name="l00816"></a>00816               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00817"></a>00817                 {
<a name="l00818"></a>00818                   p = A.IndexReal(j,k);
<a name="l00819"></a>00819                   val = complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00820"></a>00820                   <span class="keywordflow">if</span> (p == j)
<a name="l00821"></a>00821                     ajj += val;
<a name="l00822"></a>00822                   <span class="keywordflow">else</span>
<a name="l00823"></a>00823                     temp += val * X(p);
<a name="l00824"></a>00824                 }
<a name="l00825"></a>00825               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00826"></a>00826                 {
<a name="l00827"></a>00827                   p = A.IndexImag(j,k);
<a name="l00828"></a>00828                   val = complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00829"></a>00829                   <span class="keywordflow">if</span> (p == j)
<a name="l00830"></a>00830                     ajj += val;
<a name="l00831"></a>00831                   <span class="keywordflow">else</span>
<a name="l00832"></a>00832                     temp += val * X(p);
<a name="l00833"></a>00833                 }
<a name="l00834"></a>00834 
<a name="l00835"></a>00835               temp = B(j) - temp;
<a name="l00836"></a>00836               X(j) = (T2(1) - omega) / omega * ajj * X(j) + temp;
<a name="l00837"></a>00837             }
<a name="l00838"></a>00838 
<a name="l00839"></a>00839           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00840"></a>00840             {
<a name="l00841"></a>00841               ajj = complex&lt;T0&gt;(0);
<a name="l00842"></a>00842               <span class="comment">// Then we solve (D/omega - L) X = X.</span>
<a name="l00843"></a>00843               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00844"></a>00844                 {
<a name="l00845"></a>00845                   p = A.IndexReal(j,k);
<a name="l00846"></a>00846                   val = complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00847"></a>00847                   <span class="keywordflow">if</span> (p == j)
<a name="l00848"></a>00848                     ajj += val;
<a name="l00849"></a>00849                 }
<a name="l00850"></a>00850               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00851"></a>00851                 {
<a name="l00852"></a>00852                   p = A.IndexImag(j,k);
<a name="l00853"></a>00853                   val = complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00854"></a>00854                   <span class="keywordflow">if</span> (p == j)
<a name="l00855"></a>00855                     ajj += val;
<a name="l00856"></a>00856                 }
<a name="l00857"></a>00857               X(j) *= omega / ajj;
<a name="l00858"></a>00858               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00859"></a>00859                 {
<a name="l00860"></a>00860                   p = A.IndexReal(j,k);
<a name="l00861"></a>00861                   val = complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00862"></a>00862                   <span class="keywordflow">if</span> (p != j)
<a name="l00863"></a>00863                     X(p) -= val * X(j);
<a name="l00864"></a>00864                 }
<a name="l00865"></a>00865               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00866"></a>00866                 {
<a name="l00867"></a>00867                   p = A.IndexImag(j,k);
<a name="l00868"></a>00868                   val = complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00869"></a>00869                   <span class="keywordflow">if</span> (p != j)
<a name="l00870"></a>00870                     X(p) -= val*X(j);
<a name="l00871"></a>00871                 }
<a name="l00872"></a>00872             }
<a name="l00873"></a>00873         }
<a name="l00874"></a>00874 
<a name="l00875"></a>00875     <span class="comment">// Backward sweep.</span>
<a name="l00876"></a>00876     <span class="comment">// (D/omega - U) X^{n+1} = (L + (1-omega)/omega D) X^{n+1/2} + B</span>
<a name="l00877"></a>00877     <span class="keywordflow">if</span> (type_ssor % 3 == 0)
<a name="l00878"></a>00878       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; iter; i++)
<a name="l00879"></a>00879         {
<a name="l00880"></a>00880           Y.Zero();
<a name="l00881"></a>00881 
<a name="l00882"></a>00882           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00883"></a>00883             {
<a name="l00884"></a>00884               ajj = complex&lt;T0&gt;(0);
<a name="l00885"></a>00885               <span class="comment">// Then we compute X = (L + (1-omega)/omega D) X + B.</span>
<a name="l00886"></a>00886               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00887"></a>00887                 {
<a name="l00888"></a>00888                   p = A.IndexReal(j,k);
<a name="l00889"></a>00889                   val = complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00890"></a>00890                   <span class="keywordflow">if</span> (p == j)
<a name="l00891"></a>00891                     ajj += val;
<a name="l00892"></a>00892                   <span class="keywordflow">else</span>
<a name="l00893"></a>00893                     Y(p) += val * X(j);
<a name="l00894"></a>00894                 }
<a name="l00895"></a>00895               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00896"></a>00896                 {
<a name="l00897"></a>00897                   p = A.IndexImag(j,k);
<a name="l00898"></a>00898                   val = complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00899"></a>00899                   <span class="keywordflow">if</span> (p == j)
<a name="l00900"></a>00900                     ajj += val;
<a name="l00901"></a>00901                   <span class="keywordflow">else</span>
<a name="l00902"></a>00902                     Y(p) += val * X(j);
<a name="l00903"></a>00903                 }
<a name="l00904"></a>00904               X(j) = (T2(1) - omega) / omega * ajj * X(j) + B(j) - Y(j);
<a name="l00905"></a>00905             }
<a name="l00906"></a>00906 
<a name="l00907"></a>00907           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ma-1; j &gt;= 0; j--)
<a name="l00908"></a>00908             {
<a name="l00909"></a>00909               temp = complex&lt;T1&gt;(0);
<a name="l00910"></a>00910               ajj = complex&lt;T0&gt;(0);
<a name="l00911"></a>00911               <span class="comment">// Then we solve (D/omega - U) X = X.</span>
<a name="l00912"></a>00912               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetRealRowSize(j); k++)
<a name="l00913"></a>00913                 {
<a name="l00914"></a>00914                   p = A.IndexReal(j,k);
<a name="l00915"></a>00915                   val = complex&lt;T0&gt;(A.ValueReal(j,k), 0);
<a name="l00916"></a>00916                   <span class="keywordflow">if</span> (p == j)
<a name="l00917"></a>00917                     ajj += val;
<a name="l00918"></a>00918                   <span class="keywordflow">else</span>
<a name="l00919"></a>00919                     temp += val * X(p);
<a name="l00920"></a>00920                 }
<a name="l00921"></a>00921               <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; A.GetImagRowSize(j); k++)
<a name="l00922"></a>00922                 {
<a name="l00923"></a>00923                   p = A.IndexImag(j,k);
<a name="l00924"></a>00924                   val = complex&lt;T0&gt;(0, A.ValueImag(j,k));
<a name="l00925"></a>00925                   <span class="keywordflow">if</span> (p == j)
<a name="l00926"></a>00926                     ajj += val;
<a name="l00927"></a>00927                   <span class="keywordflow">else</span>
<a name="l00928"></a>00928                     temp += val * X(p);
<a name="l00929"></a>00929                 }
<a name="l00930"></a>00930               X(j) = (X(j) - temp) * omega / ajj;
<a name="l00931"></a>00931             }
<a name="l00932"></a>00932         }
<a name="l00933"></a>00933   }
<a name="l00934"></a>00934 
<a name="l00935"></a>00935 
<a name="l00936"></a>00936 } <span class="comment">// end namespace</span>
<a name="l00937"></a>00937 
<a name="l00938"></a>00938 <span class="preprocessor">#define SELDON_FILE_RELAXATION_MATVECT_CXX</span>
<a name="l00939"></a>00939 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
