<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/direct/Mumps.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_MUMPS_HXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="comment">// including Mumps headers</span>
<a name="l00023"></a>00023 <span class="keyword">extern</span> <span class="stringliteral">&quot;C&quot;</span>
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;dmumps_c.h&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;zmumps_c.h&quot;</span>
<a name="l00027"></a>00027 
<a name="l00028"></a>00028   <span class="comment">// including mpi from sequential version of Mumps if the</span>
<a name="l00029"></a>00029   <span class="comment">// compilation is not made on a parallel machine</span>
<a name="l00030"></a>00030 <span class="preprocessor">#ifndef SELDON_WITH_MPI</span>
<a name="l00031"></a>00031 <span class="preprocessor"></span><span class="preprocessor">#include &quot;mpi.h&quot;</span>
<a name="l00032"></a>00032 <span class="preprocessor">#endif</span>
<a name="l00033"></a>00033 <span class="preprocessor"></span>
<a name="l00034"></a>00034 }
<a name="l00035"></a>00035 
<a name="l00036"></a>00036 <span class="keyword">namespace </span>Seldon
<a name="l00037"></a>00037 {
<a name="l00038"></a>00038   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00039"></a><a class="code" href="class_seldon_1_1_type_mumps.php">00039</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_type_mumps.php">TypeMumps</a>
<a name="l00040"></a>00040   {
<a name="l00041"></a>00041   };
<a name="l00042"></a>00042 
<a name="l00043"></a>00043 
<a name="l00045"></a>00045   <span class="keyword">template</span>&lt;&gt;
<a name="l00046"></a><a class="code" href="class_seldon_1_1_type_mumps_3_01double_01_4.php">00046</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_type_mumps.php">TypeMumps</a>&lt;double&gt;
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048   <span class="keyword">public</span> :
<a name="l00049"></a>00049     <span class="keyword">typedef</span> DMUMPS_STRUC_C data;
<a name="l00050"></a>00050     <span class="keyword">typedef</span> <span class="keywordtype">double</span>* pointer;
<a name="l00051"></a>00051   };
<a name="l00052"></a>00052 
<a name="l00053"></a>00053 
<a name="l00055"></a>00055   <span class="keyword">template</span>&lt;&gt;
<a name="l00056"></a><a class="code" href="class_seldon_1_1_type_mumps_3_01complex_3_01double_01_4_01_4.php">00056</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_type_mumps.php">TypeMumps</a>&lt;complex&lt;double&gt; &gt;
<a name="l00057"></a>00057   {
<a name="l00058"></a>00058   <span class="keyword">public</span> :
<a name="l00059"></a>00059     <span class="keyword">typedef</span> ZMUMPS_STRUC_C data;
<a name="l00060"></a>00060     <span class="keyword">typedef</span> mumps_double_complex* pointer;
<a name="l00061"></a>00061   };
<a name="l00062"></a>00062 
<a name="l00063"></a>00063 
<a name="l00065"></a>00065   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T&gt;
<a name="l00066"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php">00066</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_matrix_mumps.php" title="object used to solve linear system by calling mumps subroutines">MatrixMumps</a>
<a name="l00067"></a>00067   {
<a name="l00068"></a>00068   <span class="keyword">protected</span> :
<a name="l00069"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68">00069</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68" title="rank of processor">rank</a>; 
<a name="l00070"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c">00070</a>     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c" title="ordering scheme (AMD, Metis, etc) //! object containing Mumps data structure">type_ordering</a>; 
<a name="l00071"></a>00071 
<a name="l00072"></a>00072     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_type_mumps.php">TypeMumps&lt;T&gt;::data</a> struct_mumps;
<a name="l00074"></a><a class="code" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737">00074</a>     <span class="keyword">typedef</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_type_mumps.php">TypeMumps&lt;T&gt;::pointer</a> pointer;
<a name="l00075"></a>00075     <span class="keywordtype">int</span> print_level;
<a name="l00076"></a>00076     <span class="keywordtype">bool</span> out_of_core;
<a name="l00077"></a>00077 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00078"></a>00078 <span class="preprocessor"></span>    MPI_Group single_group;
<a name="l00079"></a>00079     MPI_Comm single_comm;
<a name="l00080"></a>00080 <span class="preprocessor">#endif</span>
<a name="l00081"></a>00081 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_vector.php">IVect</a> num_row_glob, num_col_glob;
<a name="l00082"></a>00082     <span class="keywordtype">bool</span> new_communicator;
<a name="l00083"></a>00083 
<a name="l00084"></a>00084     <span class="comment">// internal methods</span>
<a name="l00085"></a>00085     <span class="keywordtype">void</span> CallMumps();
<a name="l00086"></a>00086     <span class="keyword">template</span>&lt;<span class="keyword">class</span> MatrixSparse&gt;
<a name="l00087"></a>00087     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2" title="initialization of the computation">InitMatrix</a>(<span class="keyword">const</span> MatrixSparse&amp;, <span class="keywordtype">bool</span> dist = <span class="keyword">false</span>);
<a name="l00088"></a>00088 
<a name="l00089"></a>00089   <span class="keyword">public</span> :
<a name="l00090"></a>00090     <a class="code" href="class_seldon_1_1_matrix_mumps.php#af1dbd7261f5a31bdc48ba7149b952b32" title="initialization">MatrixMumps</a>();
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_matrix_mumps.php#af9f7189c4f700303976d3f2ffd13945a" title="clears factorization">~MatrixMumps</a>();
<a name="l00092"></a>00092 
<a name="l00093"></a>00093     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee" title="clears factorization">Clear</a>();
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a752caecc1023fe5da2a71fc83ae8cbf9" title="selects another ordering scheme">SelectOrdering</a>(<span class="keywordtype">int</span> num_ordering);
<a name="l00096"></a>00096 
<a name="l00097"></a>00097     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a693fc679e4e3703162cd7e8e976b30d0" title="no display from Mumps">HideMessages</a>();
<a name="l00098"></a>00098     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a9375b89f5cbe3908db7120d22f033beb" title="standard display">ShowMessages</a>();
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="keywordtype">void</span> EnableOutOfCore();
<a name="l00101"></a>00101     <span class="keywordtype">void</span> DisableOutOfCore();
<a name="l00102"></a>00102 
<a name="l00103"></a>00103     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#aab6219d8e5473f80dae2957a3d003ab0" title="returns information about factorization performed">GetInfoFactorization</a>() <span class="keyword">const</span>;
<a name="l00104"></a>00104 
<a name="l00105"></a>00105     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00106"></a>00106     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a3b7b913e2e23e1d02890df3c332c6637" title="computes row numbers">FindOrdering</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00107"></a>00107                       <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; numbers, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00108"></a>00108 
<a name="l00109"></a>00109     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00110"></a>00110     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a712e9edf500c0a63ddded960762707c5" title="factorization of a given matrix">FactorizeMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat,
<a name="l00111"></a>00111                          <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00112"></a>00112 
<a name="l00113"></a>00113     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00114"></a>00114     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0" title="Symbolic factorization.">PerformAnalysis</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat);
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00117"></a>00117     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00" title="Numerical factorization.">PerformFactorization</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> &amp; mat);
<a name="l00118"></a>00118 
<a name="l00119"></a>00119     <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00120"></a>00120              <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00121"></a>00121     <span class="keywordtype">void</span> GetSchurMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop1, Storage1, Allocator1&gt;</a>&amp; mat,
<a name="l00122"></a>00122                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; num,
<a name="l00123"></a>00123                         <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop2, Storage2, Allocator2&gt;</a> &amp; mat_schur,
<a name="l00124"></a>00124                         <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00125"></a>00125 
<a name="l00126"></a>00126     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00127"></a>00127     <span class="keywordtype">void</span> Solve(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00128"></a>00128 
<a name="l00129"></a>00129     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00130"></a>00130     <span class="keywordtype">void</span> Solve(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00131"></a>00131                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x);
<a name="l00132"></a>00132 
<a name="l00133"></a>00133     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status, <span class="keyword">class</span> Prop&gt;
<a name="l00134"></a>00134     <span class="keywordtype">void</span> Solve(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00135"></a>00135                <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator2&gt;</a>&amp; x);
<a name="l00136"></a>00136 
<a name="l00137"></a>00137 <span class="preprocessor">#ifdef SELDON_WITH_MPI</span>
<a name="l00138"></a>00138 <span class="preprocessor"></span>    <span class="keyword">template</span>&lt;<span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00139"></a>00139     <span class="keywordtype">void</span> FactorizeDistributedMatrix(<a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, <a class="code" href="class_seldon_1_1_general.php">General</a>,
<a name="l00140"></a>00140                                     <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt; &amp; mat,
<a name="l00141"></a>00141                                     <span class="keyword">const</span> Prop&amp; sym, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; glob_number,
<a name="l00142"></a>00142                                     <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00143"></a>00143 
<a name="l00144"></a>00144     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Alloc1, <span class="keyword">class</span> Alloc2, <span class="keyword">class</span> Alloc3, <span class="keyword">class</span> T<span class="keywordtype">int</span>&gt;
<a name="l00145"></a>00145     <span class="keywordtype">void</span> FactorizeDistributedMatrix(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, Alloc1&gt;</a>&amp;,
<a name="l00146"></a>00146                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, Alloc2&gt;</a>&amp;,
<a name="l00147"></a>00147                                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Alloc3&gt;</a>&amp;,
<a name="l00148"></a>00148                                     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Tint&gt;</a>&amp; glob_number,
<a name="l00149"></a>00149                                     <span class="keywordtype">bool</span> sym, <span class="keywordtype">bool</span> keep_matrix = <span class="keyword">false</span>);
<a name="l00150"></a>00150 
<a name="l00151"></a>00151     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2, <span class="keyword">class</span> Transpose_status&gt;
<a name="l00152"></a>00152     <span class="keywordtype">void</span> SolveDistributed(<span class="keyword">const</span> Transpose_status&amp; TransA,
<a name="l00153"></a>00153                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x,
<a name="l00154"></a>00154                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; glob_num);
<a name="l00155"></a>00155 
<a name="l00156"></a>00156     <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00157"></a>00157     <span class="keywordtype">void</span> SolveDistributed(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; x, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; );
<a name="l00158"></a>00158 
<a name="l00159"></a>00159 <span class="preprocessor">#endif</span>
<a name="l00160"></a>00160 <span class="preprocessor"></span>
<a name="l00161"></a>00161   };
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 }
<a name="l00164"></a>00164 
<a name="l00165"></a>00165 <span class="preprocessor">#define SELDON_FILE_MUMPS_HXX</span>
<a name="l00166"></a>00166 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00167"></a>00167 <span class="preprocessor"></span>
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
