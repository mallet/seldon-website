<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a361729faf34aaedf14ad0b3f044e093c">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c">Copy</a>(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#aff7bbc1161aeb1ffcf7894e4b21a270f">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a656390ed0505de78a077a6533586428e">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e">GetDataSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#af93c5e73fc737a2f6e4f0b15164dd46d">GetLD</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33">Matrix_Pointers</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a70433b5aff5b052203136dd43783ac7d">Matrix_Pointers</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ad8f9783bed7ab3a8a3c781b8bab3ce62">Matrix_Pointers</a>(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>me_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2898e791b8d8e9b282bc36a59da5a0ec">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a1152a779c807df6022715b8635be7bf7">operator()</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3">operator=</a>(const Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#af98cb1a88c5cd8675ce73ccff9f37db9">operator[]</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e">Print</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ac26e9b3c26a513df732755cec5821a6d">Print</a>(int a, int b, int m, int n) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ab905f1761772bdd8607e212860d3b25d">Print</a>(int l) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6">Read</a>(string FileName, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a697168859e965ebfe3e0c722544217de">Read</a>(istream &amp;FileStream, bool with_size=true)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a96dc6a908187241ae7fea78ed2f12e93">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a35fc7bad6fe6cf252737916dfafae0ff">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a3d83977c9b496a6c1745a8e41f79af5c">Val</a>(int i, int j) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44">Write</a>(string FileName, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#a150457071742a4d3a4bfd503a0e10c29">Write</a>(ostream &amp;FileStream, bool with_size=true) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e">WriteText</a>(string FileName) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#af1aa0dd65f43e9ab7fc47dddc38d2cd6">WriteText</a>(ostream &amp;FileStream) const </td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#add248e51552e9cec9a6214a8c1328ed0">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca">~Matrix_Pointers</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___pointers.php">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
