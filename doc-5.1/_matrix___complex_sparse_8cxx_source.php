<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_ComplexSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_COMPLEXSPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_ComplexSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     real_nz_ = 0;
<a name="l00043"></a>00043     imag_nz_ = 0;
<a name="l00044"></a>00044     real_ptr_ = NULL;
<a name="l00045"></a>00045     imag_ptr_ = NULL;
<a name="l00046"></a>00046     real_ind_ = NULL;
<a name="l00047"></a>00047     imag_ind_ = NULL;
<a name="l00048"></a>00048     real_data_ = NULL;
<a name="l00049"></a>00049     imag_data_ = NULL;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00059"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a0105ca0b54784973e1272931f9c9e420">00059</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00060"></a>00060   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00061"></a>00061 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00062"></a>00062   {
<a name="l00063"></a>00063     real_nz_ = 0;
<a name="l00064"></a>00064     imag_nz_ = 0;
<a name="l00065"></a>00065     real_ptr_ = NULL;
<a name="l00066"></a>00066     imag_ptr_ = NULL;
<a name="l00067"></a>00067     real_ind_ = NULL;
<a name="l00068"></a>00068     imag_ind_ = NULL;
<a name="l00069"></a>00069     real_data_ = NULL;
<a name="l00070"></a>00070     imag_data_ = NULL;
<a name="l00071"></a>00071   }
<a name="l00072"></a>00072 
<a name="l00073"></a>00073 
<a name="l00075"></a>00075 
<a name="l00085"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a728b0dd58af72224fc92a6ab299ad274">00085</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00086"></a>00086   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00087"></a>00087 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz):
<a name="l00088"></a>00088     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00089"></a>00089   {
<a name="l00090"></a>00090     this-&gt;real_nz_ = real_nz;
<a name="l00091"></a>00091     this-&gt;imag_nz_ = imag_nz;
<a name="l00092"></a>00092 
<a name="l00093"></a>00093 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00094"></a>00094 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (real_nz_ &lt; 0 || imag_nz_ &lt; 0)
<a name="l00095"></a>00095       {
<a name="l00096"></a>00096         this-&gt;m_ = 0;
<a name="l00097"></a>00097         this-&gt;n_ = 0;
<a name="l00098"></a>00098         real_nz_ = 0;
<a name="l00099"></a>00099         imag_nz_ = 0;
<a name="l00100"></a>00100         real_ptr_ = NULL;
<a name="l00101"></a>00101         imag_ptr_ = NULL;
<a name="l00102"></a>00102         real_ind_ = NULL;
<a name="l00103"></a>00103         imag_ind_ = NULL;
<a name="l00104"></a>00104         this-&gt;real_data_ = NULL;
<a name="l00105"></a>00105         this-&gt;imag_data_ = NULL;
<a name="l00106"></a>00106         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00107"></a>00107                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00108"></a>00108                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span>
<a name="l00109"></a>00109                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz) + <span class="stringliteral">&quot; in the real part and &quot;</span>
<a name="l00110"></a>00110                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz) + <span class="stringliteral">&quot; in the imaginary part.&quot;</span>);
<a name="l00111"></a>00111       }
<a name="l00112"></a>00112     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00113"></a>00113          &amp;&amp; (j == 0
<a name="l00114"></a>00114              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00115"></a>00115              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00116"></a>00116         ||
<a name="l00117"></a>00117         (imag_nz_ &gt; 0
<a name="l00118"></a>00118          &amp;&amp; (j == 0
<a name="l00119"></a>00119              || static_cast&lt;long int&gt;(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00120"></a>00120              &gt;= static_cast&lt;long int&gt;(i))))
<a name="l00121"></a>00121       {
<a name="l00122"></a>00122         this-&gt;m_ = 0;
<a name="l00123"></a>00123         this-&gt;n_ = 0;
<a name="l00124"></a>00124         real_nz_ = 0;
<a name="l00125"></a>00125         imag_nz_ = 0;
<a name="l00126"></a>00126         real_ptr_ = NULL;
<a name="l00127"></a>00127         imag_ptr_ = NULL;
<a name="l00128"></a>00128         real_ind_ = NULL;
<a name="l00129"></a>00129         imag_ind_ = NULL;
<a name="l00130"></a>00130         this-&gt;real_data_ = NULL;
<a name="l00131"></a>00131         this-&gt;imag_data_ = NULL;
<a name="l00132"></a>00132         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00133"></a>00133                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00134"></a>00134                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00135"></a>00135                        + <span class="stringliteral">&quot; values for the real part and &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l00136"></a>00136                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary part) than&quot;</span>)
<a name="l00137"></a>00137                        + <span class="stringliteral">&quot; elements in the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00138"></a>00138                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00139"></a>00139       }
<a name="l00140"></a>00140 <span class="preprocessor">#endif</span>
<a name="l00141"></a>00141 <span class="preprocessor"></span>
<a name="l00142"></a>00142 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00143"></a>00143 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00144"></a>00144       {
<a name="l00145"></a>00145 <span class="preprocessor">#endif</span>
<a name="l00146"></a>00146 <span class="preprocessor"></span>
<a name="l00147"></a>00147         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00148"></a>00148                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00151"></a>00151 <span class="preprocessor"></span>      }
<a name="l00152"></a>00152     <span class="keywordflow">catch</span> (...)
<a name="l00153"></a>00153       {
<a name="l00154"></a>00154         this-&gt;m_ = 0;
<a name="l00155"></a>00155         this-&gt;n_ = 0;
<a name="l00156"></a>00156         real_nz_ = 0;
<a name="l00157"></a>00157         imag_nz_ = 0;
<a name="l00158"></a>00158         real_ptr_ = NULL;
<a name="l00159"></a>00159         imag_ptr_ = NULL;
<a name="l00160"></a>00160         real_ind_ = NULL;
<a name="l00161"></a>00161         imag_ind_ = NULL;
<a name="l00162"></a>00162         this-&gt;real_data_ = NULL;
<a name="l00163"></a>00163         this-&gt;imag_data_ = NULL;
<a name="l00164"></a>00164       }
<a name="l00165"></a>00165     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l00166"></a>00166       {
<a name="l00167"></a>00167         this-&gt;m_ = 0;
<a name="l00168"></a>00168         this-&gt;n_ = 0;
<a name="l00169"></a>00169         real_nz_ = 0;
<a name="l00170"></a>00170         imag_nz_ = 0;
<a name="l00171"></a>00171         imag_ptr_ = 0;
<a name="l00172"></a>00172         real_ind_ = NULL;
<a name="l00173"></a>00173         imag_ind_ = NULL;
<a name="l00174"></a>00174         this-&gt;real_data_ = NULL;
<a name="l00175"></a>00175         this-&gt;imag_data_ = NULL;
<a name="l00176"></a>00176       }
<a name="l00177"></a>00177     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00178"></a>00178       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00179"></a>00179                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00180"></a>00180                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00181"></a>00181                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l00182"></a>00182                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00183"></a>00183                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the real&quot;</span>)
<a name="l00184"></a>00184                      + <span class="stringliteral">&quot; part), for a &quot;</span>
<a name="l00185"></a>00185                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00186"></a>00186 <span class="preprocessor">#endif</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>
<a name="l00188"></a>00188 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00189"></a>00189 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00190"></a>00190       {
<a name="l00191"></a>00191 <span class="preprocessor">#endif</span>
<a name="l00192"></a>00192 <span class="preprocessor"></span>
<a name="l00193"></a>00193         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00194"></a>00194                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>      }
<a name="l00198"></a>00198     <span class="keywordflow">catch</span> (...)
<a name="l00199"></a>00199       {
<a name="l00200"></a>00200         this-&gt;m_ = 0;
<a name="l00201"></a>00201         this-&gt;n_ = 0;
<a name="l00202"></a>00202         real_nz_ = 0;
<a name="l00203"></a>00203         imag_nz_ = 0;
<a name="l00204"></a>00204         free(real_ptr_);
<a name="l00205"></a>00205         real_ptr_ = NULL;
<a name="l00206"></a>00206         imag_ptr_ = NULL;
<a name="l00207"></a>00207         real_ind_ = NULL;
<a name="l00208"></a>00208         imag_ind_ = NULL;
<a name="l00209"></a>00209         this-&gt;real_data_ = NULL;
<a name="l00210"></a>00210         this-&gt;imag_data_ = NULL;
<a name="l00211"></a>00211       }
<a name="l00212"></a>00212     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l00213"></a>00213       {
<a name="l00214"></a>00214         this-&gt;m_ = 0;
<a name="l00215"></a>00215         this-&gt;n_ = 0;
<a name="l00216"></a>00216         real_nz_ = 0;
<a name="l00217"></a>00217         imag_nz_ = 0;
<a name="l00218"></a>00218         free(real_ptr_);
<a name="l00219"></a>00219         real_ptr_ = 0;
<a name="l00220"></a>00220         real_ind_ = NULL;
<a name="l00221"></a>00221         imag_ind_ = NULL;
<a name="l00222"></a>00222         this-&gt;real_data_ = NULL;
<a name="l00223"></a>00223         this-&gt;imag_data_ = NULL;
<a name="l00224"></a>00224       }
<a name="l00225"></a>00225     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00226"></a>00226       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00227"></a>00227                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00228"></a>00228                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00229"></a>00229                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l00230"></a>00230                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00231"></a>00231                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the&quot;</span>)
<a name="l00232"></a>00232                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; imaginary part), for a &quot;</span>)
<a name="l00233"></a>00233                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00234"></a>00234 <span class="preprocessor">#endif</span>
<a name="l00235"></a>00235 <span class="preprocessor"></span>
<a name="l00236"></a>00236 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00237"></a>00237 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00238"></a>00238       {
<a name="l00239"></a>00239 <span class="preprocessor">#endif</span>
<a name="l00240"></a>00240 <span class="preprocessor"></span>
<a name="l00241"></a>00241         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00242"></a>00242 
<a name="l00243"></a>00243 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00244"></a>00244 <span class="preprocessor"></span>      }
<a name="l00245"></a>00245     <span class="keywordflow">catch</span> (...)
<a name="l00246"></a>00246       {
<a name="l00247"></a>00247         this-&gt;m_ = 0;
<a name="l00248"></a>00248         this-&gt;n_ = 0;
<a name="l00249"></a>00249         real_nz_ = 0;
<a name="l00250"></a>00250         imag_nz_ = 0;
<a name="l00251"></a>00251         free(real_ptr_);
<a name="l00252"></a>00252         free(imag_ptr_);
<a name="l00253"></a>00253         real_ptr_ = NULL;
<a name="l00254"></a>00254         imag_ptr_ = NULL;
<a name="l00255"></a>00255         real_ind_ = NULL;
<a name="l00256"></a>00256         imag_ind_ = NULL;
<a name="l00257"></a>00257         this-&gt;real_data_ = NULL;
<a name="l00258"></a>00258         this-&gt;imag_data_ = NULL;
<a name="l00259"></a>00259       }
<a name="l00260"></a>00260     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l00261"></a>00261       {
<a name="l00262"></a>00262         this-&gt;m_ = 0;
<a name="l00263"></a>00263         this-&gt;n_ = 0;
<a name="l00264"></a>00264         real_nz_ = 0;
<a name="l00265"></a>00265         imag_nz_ = 0;
<a name="l00266"></a>00266         free(real_ptr_);
<a name="l00267"></a>00267         free(imag_ptr_);
<a name="l00268"></a>00268         real_ptr_ = NULL;
<a name="l00269"></a>00269         imag_ptr_ = NULL;
<a name="l00270"></a>00270         this-&gt;real_data_ = NULL;
<a name="l00271"></a>00271         this-&gt;imag_data_ = NULL;
<a name="l00272"></a>00272       }
<a name="l00273"></a>00273     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00274"></a>00274       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00275"></a>00275                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00276"></a>00276                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00277"></a>00277                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz)
<a name="l00278"></a>00278                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00279"></a>00279                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l00280"></a>00280                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00281"></a>00281 <span class="preprocessor">#endif</span>
<a name="l00282"></a>00282 <span class="preprocessor"></span>
<a name="l00283"></a>00283 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00284"></a>00284 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00285"></a>00285       {
<a name="l00286"></a>00286 <span class="preprocessor">#endif</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>
<a name="l00288"></a>00288         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00289"></a>00289 
<a name="l00290"></a>00290 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00291"></a>00291 <span class="preprocessor"></span>      }
<a name="l00292"></a>00292     <span class="keywordflow">catch</span> (...)
<a name="l00293"></a>00293       {
<a name="l00294"></a>00294         this-&gt;m_ = 0;
<a name="l00295"></a>00295         this-&gt;n_ = 0;
<a name="l00296"></a>00296         real_nz_ = 0;
<a name="l00297"></a>00297         imag_nz_ = 0;
<a name="l00298"></a>00298         free(real_ptr_);
<a name="l00299"></a>00299         free(imag_ptr_);
<a name="l00300"></a>00300         real_ptr_ = NULL;
<a name="l00301"></a>00301         imag_ptr_ = NULL;
<a name="l00302"></a>00302         free(imag_ind_);
<a name="l00303"></a>00303         real_ind_ = NULL;
<a name="l00304"></a>00304         imag_ind_ = NULL;
<a name="l00305"></a>00305         this-&gt;real_data_ = NULL;
<a name="l00306"></a>00306         this-&gt;imag_data_ = NULL;
<a name="l00307"></a>00307       }
<a name="l00308"></a>00308     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l00309"></a>00309       {
<a name="l00310"></a>00310         this-&gt;m_ = 0;
<a name="l00311"></a>00311         this-&gt;n_ = 0;
<a name="l00312"></a>00312         real_nz_ = 0;
<a name="l00313"></a>00313         imag_nz_ = 0;
<a name="l00314"></a>00314         free(real_ptr_);
<a name="l00315"></a>00315         free(imag_ptr_);
<a name="l00316"></a>00316         real_ptr_ = NULL;
<a name="l00317"></a>00317         imag_ptr_ = NULL;
<a name="l00318"></a>00318         free(imag_ind_);
<a name="l00319"></a>00319         imag_ind_ = NULL;
<a name="l00320"></a>00320         this-&gt;real_data_ = NULL;
<a name="l00321"></a>00321         this-&gt;imag_data_ = NULL;
<a name="l00322"></a>00322       }
<a name="l00323"></a>00323     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00324"></a>00324       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00325"></a>00325                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00326"></a>00326                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00327"></a>00327                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz)
<a name="l00328"></a>00328                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l00329"></a>00329                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l00330"></a>00330                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00331"></a>00331 <span class="preprocessor">#endif</span>
<a name="l00332"></a>00332 <span class="preprocessor"></span>
<a name="l00333"></a>00333 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00334"></a>00334 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00335"></a>00335       {
<a name="l00336"></a>00336 <span class="preprocessor">#endif</span>
<a name="l00337"></a>00337 <span class="preprocessor"></span>
<a name="l00338"></a>00338         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00341"></a>00341 <span class="preprocessor"></span>      }
<a name="l00342"></a>00342     <span class="keywordflow">catch</span> (...)
<a name="l00343"></a>00343       {
<a name="l00344"></a>00344         this-&gt;m_ = 0;
<a name="l00345"></a>00345         this-&gt;n_ = 0;
<a name="l00346"></a>00346         free(real_ptr_);
<a name="l00347"></a>00347         free(imag_ptr_);
<a name="l00348"></a>00348         real_ptr_ = NULL;
<a name="l00349"></a>00349         imag_ptr_ = NULL;
<a name="l00350"></a>00350         free(real_ind_);
<a name="l00351"></a>00351         free(imag_ind_);
<a name="l00352"></a>00352         real_ind_ = NULL;
<a name="l00353"></a>00353         imag_ind_ = NULL;
<a name="l00354"></a>00354         this-&gt;real_data_ = NULL;
<a name="l00355"></a>00355         this-&gt;imag_data_ = NULL;
<a name="l00356"></a>00356       }
<a name="l00357"></a>00357     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l00358"></a>00358       {
<a name="l00359"></a>00359         this-&gt;m_ = 0;
<a name="l00360"></a>00360         this-&gt;n_ = 0;
<a name="l00361"></a>00361         free(real_ptr_);
<a name="l00362"></a>00362         free(imag_ptr_);
<a name="l00363"></a>00363         real_ptr_ = NULL;
<a name="l00364"></a>00364         imag_ptr_ = NULL;
<a name="l00365"></a>00365         free(real_ind_);
<a name="l00366"></a>00366         free(imag_ind_);
<a name="l00367"></a>00367         real_ind_ = NULL;
<a name="l00368"></a>00368         imag_ind_ = NULL;
<a name="l00369"></a>00369         imag_data_ = NULL;
<a name="l00370"></a>00370       }
<a name="l00371"></a>00371     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00372"></a>00372       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00373"></a>00373                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00374"></a>00374                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00375"></a>00375                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz)
<a name="l00376"></a>00376                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00377"></a>00377                      + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l00378"></a>00378                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00379"></a>00379 <span class="preprocessor">#endif</span>
<a name="l00380"></a>00380 <span class="preprocessor"></span>
<a name="l00381"></a>00381 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00382"></a>00382 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00383"></a>00383       {
<a name="l00384"></a>00384 <span class="preprocessor">#endif</span>
<a name="l00385"></a>00385 <span class="preprocessor"></span>
<a name="l00386"></a>00386         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l00387"></a>00387 
<a name="l00388"></a>00388 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00389"></a>00389 <span class="preprocessor"></span>      }
<a name="l00390"></a>00390     <span class="keywordflow">catch</span> (...)
<a name="l00391"></a>00391       {
<a name="l00392"></a>00392         this-&gt;m_ = 0;
<a name="l00393"></a>00393         this-&gt;n_ = 0;
<a name="l00394"></a>00394         free(real_ptr_);
<a name="l00395"></a>00395         free(imag_ptr_);
<a name="l00396"></a>00396         real_ptr_ = NULL;
<a name="l00397"></a>00397         imag_ptr_ = NULL;
<a name="l00398"></a>00398         free(real_ind_);
<a name="l00399"></a>00399         free(imag_ind_);
<a name="l00400"></a>00400         real_ind_ = NULL;
<a name="l00401"></a>00401         imag_ind_ = NULL;
<a name="l00402"></a>00402         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00403"></a>00403         this-&gt;real_data_ = NULL;
<a name="l00404"></a>00404         this-&gt;imag_data_ = NULL;
<a name="l00405"></a>00405       }
<a name="l00406"></a>00406     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l00407"></a>00407       {
<a name="l00408"></a>00408         this-&gt;m_ = 0;
<a name="l00409"></a>00409         this-&gt;n_ = 0;
<a name="l00410"></a>00410         free(real_ptr_);
<a name="l00411"></a>00411         free(imag_ptr_);
<a name="l00412"></a>00412         real_ptr_ = NULL;
<a name="l00413"></a>00413         imag_ptr_ = NULL;
<a name="l00414"></a>00414         free(real_ind_);
<a name="l00415"></a>00415         free(imag_ind_);
<a name="l00416"></a>00416         real_ind_ = NULL;
<a name="l00417"></a>00417         imag_ind_ = NULL;
<a name="l00418"></a>00418         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00419"></a>00419         real_data_ = NULL;
<a name="l00420"></a>00420       }
<a name="l00421"></a>00421     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00422"></a>00422       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00423"></a>00423                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00424"></a>00424                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00425"></a>00425                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz)
<a name="l00426"></a>00426                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l00427"></a>00427                      + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l00428"></a>00428                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00429"></a>00429 <span class="preprocessor">#endif</span>
<a name="l00430"></a>00430 <span class="preprocessor"></span>
<a name="l00431"></a>00431   }
<a name="l00432"></a>00432 
<a name="l00433"></a>00433 
<a name="l00435"></a>00435 
<a name="l00453"></a>00453   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00454"></a>00454   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00455"></a>00455             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00456"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a079f7006a23ebdc5e63c2337f88f9bfc">00456</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00457"></a>00457   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00458"></a>00458 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  Matrix_ComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00459"></a>00459                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00460"></a>00460                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00461"></a>00461                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00462"></a>00462                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00463"></a>00463                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00464"></a>00464                        <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l00465"></a>00465     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00466"></a>00466   {
<a name="l00467"></a>00467 
<a name="l00468"></a>00468     real_nz_ = real_values.GetLength();
<a name="l00469"></a>00469     imag_nz_ = imag_values.GetLength();
<a name="l00470"></a>00470 
<a name="l00471"></a>00471 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00472"></a>00472 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00473"></a>00473 
<a name="l00474"></a>00474     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00475"></a>00475       {
<a name="l00476"></a>00476         this-&gt;m_ = 0;
<a name="l00477"></a>00477         this-&gt;n_ = 0;
<a name="l00478"></a>00478         real_nz_ = 0;
<a name="l00479"></a>00479         imag_nz_ = 0;
<a name="l00480"></a>00480         real_ptr_ = NULL;
<a name="l00481"></a>00481         imag_ptr_ = NULL;
<a name="l00482"></a>00482         real_ind_ = NULL;
<a name="l00483"></a>00483         imag_ind_ = NULL;
<a name="l00484"></a>00484         this-&gt;real_data_ = NULL;
<a name="l00485"></a>00485         this-&gt;imag_data_ = NULL;
<a name="l00486"></a>00486         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00487"></a>00487                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00488"></a>00488                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00489"></a>00489                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00490"></a>00490                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00491"></a>00491                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00492"></a>00492                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00493"></a>00493                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00494"></a>00494       }
<a name="l00495"></a>00495 
<a name="l00496"></a>00496     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00497"></a>00497       {
<a name="l00498"></a>00498         this-&gt;m_ = 0;
<a name="l00499"></a>00499         this-&gt;n_ = 0;
<a name="l00500"></a>00500         real_nz_ = 0;
<a name="l00501"></a>00501         imag_nz_ = 0;
<a name="l00502"></a>00502         real_ptr_ = NULL;
<a name="l00503"></a>00503         imag_ptr_ = NULL;
<a name="l00504"></a>00504         real_ind_ = NULL;
<a name="l00505"></a>00505         imag_ind_ = NULL;
<a name="l00506"></a>00506         this-&gt;real_data_ = NULL;
<a name="l00507"></a>00507         this-&gt;imag_data_ = NULL;
<a name="l00508"></a>00508         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00509"></a>00509                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00510"></a>00510                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00511"></a>00511                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00512"></a>00512                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00513"></a>00513                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00514"></a>00514                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00515"></a>00515                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00516"></a>00516       }
<a name="l00517"></a>00517 
<a name="l00518"></a>00518     <span class="keywordflow">if</span> (real_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00519"></a>00519       {
<a name="l00520"></a>00520         this-&gt;m_ = 0;
<a name="l00521"></a>00521         this-&gt;n_ = 0;
<a name="l00522"></a>00522         real_nz_ = 0;
<a name="l00523"></a>00523         imag_nz_ = 0;
<a name="l00524"></a>00524         real_ptr_ = NULL;
<a name="l00525"></a>00525         imag_ptr_ = NULL;
<a name="l00526"></a>00526         real_ind_ = NULL;
<a name="l00527"></a>00527         imag_ind_ = NULL;
<a name="l00528"></a>00528         this-&gt;real_data_ = NULL;
<a name="l00529"></a>00529         this-&gt;imag_data_ = NULL;
<a name="l00530"></a>00530         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00531"></a>00531                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00532"></a>00532                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00533"></a>00533                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00534"></a>00534                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00535"></a>00535                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength()-1)
<a name="l00536"></a>00536                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00537"></a>00537                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00538"></a>00538                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00539"></a>00539                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00540"></a>00540                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00541"></a>00541       }
<a name="l00542"></a>00542 
<a name="l00543"></a>00543     <span class="keywordflow">if</span> (imag_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00544"></a>00544       {
<a name="l00545"></a>00545         this-&gt;m_ = 0;
<a name="l00546"></a>00546         this-&gt;n_ = 0;
<a name="l00547"></a>00547         real_nz_ = 0;
<a name="l00548"></a>00548         imag_nz_ = 0;
<a name="l00549"></a>00549         real_ptr_ = NULL;
<a name="l00550"></a>00550         imag_ptr_ = NULL;
<a name="l00551"></a>00551         real_ind_ = NULL;
<a name="l00552"></a>00552         imag_ind_ = NULL;
<a name="l00553"></a>00553         this-&gt;real_data_ = NULL;
<a name="l00554"></a>00554         this-&gt;imag_data_ = NULL;
<a name="l00555"></a>00555         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00556"></a>00556                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00557"></a>00557                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00558"></a>00558                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00559"></a>00559                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00560"></a>00560                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00561"></a>00561                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00562"></a>00562                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00563"></a>00563                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00564"></a>00564                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00565"></a>00565                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00566"></a>00566       }
<a name="l00567"></a>00567 
<a name="l00568"></a>00568     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00569"></a>00569          &amp;&amp; (j == 0
<a name="l00570"></a>00570              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00571"></a>00571              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00572"></a>00572         ||
<a name="l00573"></a>00573         (imag_nz_ &gt; 0
<a name="l00574"></a>00574          &amp;&amp; (j == 0
<a name="l00575"></a>00575              || <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00576"></a>00576              &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i))))
<a name="l00577"></a>00577       {
<a name="l00578"></a>00578         this-&gt;m_ = 0;
<a name="l00579"></a>00579         this-&gt;n_ = 0;
<a name="l00580"></a>00580         real_nz_ = 0;
<a name="l00581"></a>00581         imag_nz_ = 0;
<a name="l00582"></a>00582         real_ptr_ = NULL;
<a name="l00583"></a>00583         imag_ptr_ = NULL;
<a name="l00584"></a>00584         real_ind_ = NULL;
<a name="l00585"></a>00585         imag_ind_ = NULL;
<a name="l00586"></a>00586         this-&gt;real_data_ = NULL;
<a name="l00587"></a>00587         this-&gt;imag_data_ = NULL;
<a name="l00588"></a>00588         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l00589"></a>00589                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, &quot;</span>)
<a name="l00590"></a>00590                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00591"></a>00591                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00592"></a>00592                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00593"></a>00593                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00594"></a>00594                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00595"></a>00595                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values&quot;</span>)
<a name="l00596"></a>00596                        + string(<span class="stringliteral">&quot; for the imaginary part) than elements&quot;</span>)
<a name="l00597"></a>00597                        + <span class="stringliteral">&quot; in the matrix (&quot;</span>
<a name="l00598"></a>00598                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00599"></a>00599       }
<a name="l00600"></a>00600 <span class="preprocessor">#endif</span>
<a name="l00601"></a>00601 <span class="preprocessor"></span>
<a name="l00602"></a>00602     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00603"></a>00603     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00604"></a>00604     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00605"></a>00605     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00606"></a>00606     this-&gt;real_data_ = real_values.GetData();
<a name="l00607"></a>00607     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00608"></a>00608 
<a name="l00609"></a>00609     real_ptr.Nullify();
<a name="l00610"></a>00610     imag_ptr.Nullify();
<a name="l00611"></a>00611     real_ind.Nullify();
<a name="l00612"></a>00612     imag_ind.Nullify();
<a name="l00613"></a>00613     real_values.Nullify();
<a name="l00614"></a>00614     imag_values.Nullify();
<a name="l00615"></a>00615   }
<a name="l00616"></a>00616 
<a name="l00617"></a>00617 
<a name="l00619"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3df0b27785c83bf1b8809cdd263c48c5">00619</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00620"></a>00620   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00621"></a>00621 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad680112fe370f8ce4621a70bbc5ce2d9" title="Default constructor.">  ::Matrix_ComplexSparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop,
<a name="l00622"></a>00622                          Storage, Allocator&gt;&amp; A)
<a name="l00623"></a>00623   {
<a name="l00624"></a>00624     this-&gt;m_ = 0;
<a name="l00625"></a>00625     this-&gt;n_ = 0;
<a name="l00626"></a>00626     real_nz_ = 0;
<a name="l00627"></a>00627     imag_nz_ = 0;
<a name="l00628"></a>00628     real_ptr_ = NULL;
<a name="l00629"></a>00629     imag_ptr_ = NULL;
<a name="l00630"></a>00630     real_ind_ = NULL;
<a name="l00631"></a>00631     imag_ind_ = NULL;
<a name="l00632"></a>00632     real_data_ = NULL;
<a name="l00633"></a>00633     imag_data_ = NULL;
<a name="l00634"></a>00634 
<a name="l00635"></a>00635     this-&gt;Copy(A);
<a name="l00636"></a>00636   }
<a name="l00637"></a>00637 
<a name="l00638"></a>00638 
<a name="l00639"></a>00639   <span class="comment">/**************</span>
<a name="l00640"></a>00640 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00641"></a>00641 <span class="comment">   **************/</span>
<a name="l00642"></a>00642 
<a name="l00643"></a>00643 
<a name="l00645"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75">00645</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00646"></a>00646   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00647"></a>00647 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">  ::~Matrix_ComplexSparse</a>()
<a name="l00648"></a>00648   {
<a name="l00649"></a>00649     this-&gt;m_ = 0;
<a name="l00650"></a>00650     this-&gt;n_ = 0;
<a name="l00651"></a>00651 
<a name="l00652"></a>00652 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00653"></a>00653 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00654"></a>00654       {
<a name="l00655"></a>00655 <span class="preprocessor">#endif</span>
<a name="l00656"></a>00656 <span class="preprocessor"></span>
<a name="l00657"></a>00657         <span class="keywordflow">if</span> (real_ptr_ != NULL)
<a name="l00658"></a>00658           {
<a name="l00659"></a>00659             free(real_ptr_);
<a name="l00660"></a>00660             real_ptr_ = NULL;
<a name="l00661"></a>00661           }
<a name="l00662"></a>00662 
<a name="l00663"></a>00663 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00664"></a>00664 <span class="preprocessor"></span>      }
<a name="l00665"></a>00665     <span class="keywordflow">catch</span> (...)
<a name="l00666"></a>00666       {
<a name="l00667"></a>00667         real_ptr_ = NULL;
<a name="l00668"></a>00668       }
<a name="l00669"></a>00669 <span class="preprocessor">#endif</span>
<a name="l00670"></a>00670 <span class="preprocessor"></span>
<a name="l00671"></a>00671 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00672"></a>00672 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00673"></a>00673       {
<a name="l00674"></a>00674 <span class="preprocessor">#endif</span>
<a name="l00675"></a>00675 <span class="preprocessor"></span>
<a name="l00676"></a>00676         <span class="keywordflow">if</span> (imag_ptr_ != NULL)
<a name="l00677"></a>00677           {
<a name="l00678"></a>00678             free(imag_ptr_);
<a name="l00679"></a>00679             imag_ptr_ = NULL;
<a name="l00680"></a>00680           }
<a name="l00681"></a>00681 
<a name="l00682"></a>00682 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00683"></a>00683 <span class="preprocessor"></span>      }
<a name="l00684"></a>00684     <span class="keywordflow">catch</span> (...)
<a name="l00685"></a>00685       {
<a name="l00686"></a>00686         imag_ptr_ = NULL;
<a name="l00687"></a>00687       }
<a name="l00688"></a>00688 <span class="preprocessor">#endif</span>
<a name="l00689"></a>00689 <span class="preprocessor"></span>
<a name="l00690"></a>00690 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00691"></a>00691 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00692"></a>00692       {
<a name="l00693"></a>00693 <span class="preprocessor">#endif</span>
<a name="l00694"></a>00694 <span class="preprocessor"></span>
<a name="l00695"></a>00695         <span class="keywordflow">if</span> (real_ind_ != NULL)
<a name="l00696"></a>00696           {
<a name="l00697"></a>00697             free(real_ind_);
<a name="l00698"></a>00698             real_ind_ = NULL;
<a name="l00699"></a>00699           }
<a name="l00700"></a>00700 
<a name="l00701"></a>00701 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00702"></a>00702 <span class="preprocessor"></span>      }
<a name="l00703"></a>00703     <span class="keywordflow">catch</span> (...)
<a name="l00704"></a>00704       {
<a name="l00705"></a>00705         real_ind_ = NULL;
<a name="l00706"></a>00706       }
<a name="l00707"></a>00707 <span class="preprocessor">#endif</span>
<a name="l00708"></a>00708 <span class="preprocessor"></span>
<a name="l00709"></a>00709 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00710"></a>00710 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00711"></a>00711       {
<a name="l00712"></a>00712 <span class="preprocessor">#endif</span>
<a name="l00713"></a>00713 <span class="preprocessor"></span>
<a name="l00714"></a>00714         <span class="keywordflow">if</span> (imag_ind_ != NULL)
<a name="l00715"></a>00715           {
<a name="l00716"></a>00716             free(imag_ind_);
<a name="l00717"></a>00717             imag_ind_ = NULL;
<a name="l00718"></a>00718           }
<a name="l00719"></a>00719 
<a name="l00720"></a>00720 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00721"></a>00721 <span class="preprocessor"></span>      }
<a name="l00722"></a>00722     <span class="keywordflow">catch</span> (...)
<a name="l00723"></a>00723       {
<a name="l00724"></a>00724         imag_ind_ = NULL;
<a name="l00725"></a>00725       }
<a name="l00726"></a>00726 <span class="preprocessor">#endif</span>
<a name="l00727"></a>00727 <span class="preprocessor"></span>
<a name="l00728"></a>00728 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00729"></a>00729 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00730"></a>00730       {
<a name="l00731"></a>00731 <span class="preprocessor">#endif</span>
<a name="l00732"></a>00732 <span class="preprocessor"></span>
<a name="l00733"></a>00733         <span class="keywordflow">if</span> (this-&gt;real_data_ != NULL)
<a name="l00734"></a>00734           {
<a name="l00735"></a>00735             this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00736"></a>00736             this-&gt;real_data_ = NULL;
<a name="l00737"></a>00737           }
<a name="l00738"></a>00738 
<a name="l00739"></a>00739 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00740"></a>00740 <span class="preprocessor"></span>      }
<a name="l00741"></a>00741     <span class="keywordflow">catch</span> (...)
<a name="l00742"></a>00742       {
<a name="l00743"></a>00743         this-&gt;real_nz_ = 0;
<a name="l00744"></a>00744         this-&gt;real_data_ = NULL;
<a name="l00745"></a>00745       }
<a name="l00746"></a>00746 <span class="preprocessor">#endif</span>
<a name="l00747"></a>00747 <span class="preprocessor"></span>
<a name="l00748"></a>00748 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00749"></a>00749 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00750"></a>00750       {
<a name="l00751"></a>00751 <span class="preprocessor">#endif</span>
<a name="l00752"></a>00752 <span class="preprocessor"></span>
<a name="l00753"></a>00753         <span class="keywordflow">if</span> (this-&gt;imag_data_ != NULL)
<a name="l00754"></a>00754           {
<a name="l00755"></a>00755             this-&gt;allocator_.deallocate(this-&gt;imag_data_, imag_nz_);
<a name="l00756"></a>00756             this-&gt;imag_data_ = NULL;
<a name="l00757"></a>00757           }
<a name="l00758"></a>00758 
<a name="l00759"></a>00759 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00760"></a>00760 <span class="preprocessor"></span>      }
<a name="l00761"></a>00761     <span class="keywordflow">catch</span> (...)
<a name="l00762"></a>00762       {
<a name="l00763"></a>00763         this-&gt;imag_nz_ = 0;
<a name="l00764"></a>00764         this-&gt;imag_data_ = NULL;
<a name="l00765"></a>00765       }
<a name="l00766"></a>00766 <span class="preprocessor">#endif</span>
<a name="l00767"></a>00767 <span class="preprocessor"></span>
<a name="l00768"></a>00768     this-&gt;real_nz_ = 0;
<a name="l00769"></a>00769     this-&gt;imag_nz_ = 0;
<a name="l00770"></a>00770   }
<a name="l00771"></a>00771 
<a name="l00772"></a>00772 
<a name="l00774"></a>00774 
<a name="l00777"></a>00777   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00778"></a>00778   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00779"></a>00779   {
<a name="l00780"></a>00780     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3b785e50c55d9c0e00180bc7d3332d75" title="Destructor.">~Matrix_ComplexSparse</a>();
<a name="l00781"></a>00781   }
<a name="l00782"></a>00782 
<a name="l00783"></a>00783 
<a name="l00784"></a>00784   <span class="comment">/*********************</span>
<a name="l00785"></a>00785 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00786"></a>00786 <span class="comment">   *********************/</span>
<a name="l00787"></a>00787 
<a name="l00788"></a>00788 
<a name="l00790"></a>00790 
<a name="l00807"></a>00807   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00808"></a>00808   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00809"></a>00809             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00810"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85">00810</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00811"></a>00811   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00812"></a>00812 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00813"></a>00813           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00814"></a>00814           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00815"></a>00815           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00816"></a>00816           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00817"></a>00817           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00818"></a>00818           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind)
<a name="l00819"></a>00819   {
<a name="l00820"></a>00820     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l00821"></a>00821     this-&gt;m_ = i;
<a name="l00822"></a>00822     this-&gt;n_ = j;
<a name="l00823"></a>00823     real_nz_ = real_values.GetLength();
<a name="l00824"></a>00824     imag_nz_ = imag_values.GetLength();
<a name="l00825"></a>00825 
<a name="l00826"></a>00826 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00827"></a>00827 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00828"></a>00828 
<a name="l00829"></a>00829     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00830"></a>00830       {
<a name="l00831"></a>00831         this-&gt;m_ = 0;
<a name="l00832"></a>00832         this-&gt;n_ = 0;
<a name="l00833"></a>00833         real_nz_ = 0;
<a name="l00834"></a>00834         imag_nz_ = 0;
<a name="l00835"></a>00835         real_ptr_ = NULL;
<a name="l00836"></a>00836         imag_ptr_ = NULL;
<a name="l00837"></a>00837         real_ind_ = NULL;
<a name="l00838"></a>00838         imag_ind_ = NULL;
<a name="l00839"></a>00839         this-&gt;real_data_ = NULL;
<a name="l00840"></a>00840         this-&gt;imag_data_ = NULL;
<a name="l00841"></a>00841         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00842"></a>00842                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00843"></a>00843                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00844"></a>00844                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00845"></a>00845                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00846"></a>00846                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00847"></a>00847                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00848"></a>00848       }
<a name="l00849"></a>00849 
<a name="l00850"></a>00850     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00851"></a>00851       {
<a name="l00852"></a>00852         this-&gt;m_ = 0;
<a name="l00853"></a>00853         this-&gt;n_ = 0;
<a name="l00854"></a>00854         real_nz_ = 0;
<a name="l00855"></a>00855         imag_nz_ = 0;
<a name="l00856"></a>00856         real_ptr_ = NULL;
<a name="l00857"></a>00857         imag_ptr_ = NULL;
<a name="l00858"></a>00858         real_ind_ = NULL;
<a name="l00859"></a>00859         imag_ind_ = NULL;
<a name="l00860"></a>00860         this-&gt;real_data_ = NULL;
<a name="l00861"></a>00861         this-&gt;imag_data_ = NULL;
<a name="l00862"></a>00862         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00863"></a>00863                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00864"></a>00864                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00865"></a>00865                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00866"></a>00866                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00867"></a>00867                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00868"></a>00868                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00869"></a>00869       }
<a name="l00870"></a>00870 
<a name="l00871"></a>00871     <span class="keywordflow">if</span> (real_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00872"></a>00872       {
<a name="l00873"></a>00873         this-&gt;m_ = 0;
<a name="l00874"></a>00874         this-&gt;n_ = 0;
<a name="l00875"></a>00875         real_nz_ = 0;
<a name="l00876"></a>00876         imag_nz_ = 0;
<a name="l00877"></a>00877         real_ptr_ = NULL;
<a name="l00878"></a>00878         imag_ptr_ = NULL;
<a name="l00879"></a>00879         real_ind_ = NULL;
<a name="l00880"></a>00880         imag_ind_ = NULL;
<a name="l00881"></a>00881         this-&gt;real_data_ = NULL;
<a name="l00882"></a>00882         this-&gt;imag_data_ = NULL;
<a name="l00883"></a>00883         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00884"></a>00884                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00885"></a>00885                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00886"></a>00886                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00887"></a>00887                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength()-1)
<a name="l00888"></a>00888                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00889"></a>00889                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00890"></a>00890                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00891"></a>00891                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00892"></a>00892                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00893"></a>00893       }
<a name="l00894"></a>00894 
<a name="l00895"></a>00895     <span class="keywordflow">if</span> (imag_ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00896"></a>00896       {
<a name="l00897"></a>00897         this-&gt;m_ = 0;
<a name="l00898"></a>00898         this-&gt;n_ = 0;
<a name="l00899"></a>00899         real_nz_ = 0;
<a name="l00900"></a>00900         imag_nz_ = 0;
<a name="l00901"></a>00901         real_ptr_ = NULL;
<a name="l00902"></a>00902         imag_ptr_ = NULL;
<a name="l00903"></a>00903         real_ind_ = NULL;
<a name="l00904"></a>00904         imag_ind_ = NULL;
<a name="l00905"></a>00905         this-&gt;real_data_ = NULL;
<a name="l00906"></a>00906         this-&gt;imag_data_ = NULL;
<a name="l00907"></a>00907         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00908"></a>00908                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00909"></a>00909                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00910"></a>00910                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00911"></a>00911                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00912"></a>00912                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00913"></a>00913                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00914"></a>00914                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00915"></a>00915                        + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00916"></a>00916                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00917"></a>00917       }
<a name="l00918"></a>00918 
<a name="l00919"></a>00919     <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l00920"></a>00920          &amp;&amp; (j == 0
<a name="l00921"></a>00921              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00922"></a>00922              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00923"></a>00923         ||
<a name="l00924"></a>00924         (imag_nz_ &gt; 0
<a name="l00925"></a>00925          &amp;&amp; (j == 0
<a name="l00926"></a>00926              || <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00927"></a>00927              &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i))))
<a name="l00928"></a>00928       {
<a name="l00929"></a>00929         this-&gt;m_ = 0;
<a name="l00930"></a>00930         this-&gt;n_ = 0;
<a name="l00931"></a>00931         real_nz_ = 0;
<a name="l00932"></a>00932         imag_nz_ = 0;
<a name="l00933"></a>00933         real_ptr_ = NULL;
<a name="l00934"></a>00934         imag_ptr_ = NULL;
<a name="l00935"></a>00935         real_ind_ = NULL;
<a name="l00936"></a>00936         imag_ind_ = NULL;
<a name="l00937"></a>00937         this-&gt;real_data_ = NULL;
<a name="l00938"></a>00938         this-&gt;imag_data_ = NULL;
<a name="l00939"></a>00939         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00940"></a>00940                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00941"></a>00941                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00942"></a>00942                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00943"></a>00943                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00944"></a>00944                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00945"></a>00945                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values&quot;</span>)
<a name="l00946"></a>00946                        + string(<span class="stringliteral">&quot; for the imaginary part) than elements&quot;</span>)
<a name="l00947"></a>00947                        + <span class="stringliteral">&quot; in the matrix (&quot;</span>
<a name="l00948"></a>00948                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00949"></a>00949       }
<a name="l00950"></a>00950 <span class="preprocessor">#endif</span>
<a name="l00951"></a>00951 <span class="preprocessor"></span>
<a name="l00952"></a>00952     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00953"></a>00953     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00954"></a>00954     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00955"></a>00955     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00956"></a>00956     this-&gt;real_data_ = real_values.GetData();
<a name="l00957"></a>00957     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00958"></a>00958 
<a name="l00959"></a>00959     real_ptr.Nullify();
<a name="l00960"></a>00960     imag_ptr.Nullify();
<a name="l00961"></a>00961     real_ind.Nullify();
<a name="l00962"></a>00962     imag_ind.Nullify();
<a name="l00963"></a>00963     real_values.Nullify();
<a name="l00964"></a>00964     imag_values.Nullify();
<a name="l00965"></a>00965   }
<a name="l00966"></a>00966 
<a name="l00967"></a>00967 
<a name="l00969"></a>00969 
<a name="l00990"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a50bafb538cfd1b8e6bb18bba62451dff">00990</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00991"></a>00991   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00992"></a>00992 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3729058de11ead808a09e98a10702d85" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz,
<a name="l00993"></a>00993           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00994"></a>00994           ::pointer real_values,
<a name="l00995"></a>00995           <span class="keywordtype">int</span>* real_ptr, <span class="keywordtype">int</span>* real_ind, <span class="keywordtype">int</span> imag_nz,
<a name="l00996"></a>00996           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00997"></a>00997           ::pointer imag_values,
<a name="l00998"></a>00998           <span class="keywordtype">int</span>* imag_ptr, <span class="keywordtype">int</span>* imag_ind)
<a name="l00999"></a>00999   {
<a name="l01000"></a>01000     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l01001"></a>01001 
<a name="l01002"></a>01002     this-&gt;m_ = i;
<a name="l01003"></a>01003     this-&gt;n_ = j;
<a name="l01004"></a>01004 
<a name="l01005"></a>01005     this-&gt;real_nz_ = real_nz;
<a name="l01006"></a>01006     this-&gt;imag_nz_ = imag_nz;
<a name="l01007"></a>01007 
<a name="l01008"></a>01008     real_data_ = real_values;
<a name="l01009"></a>01009     imag_data_ = imag_values;
<a name="l01010"></a>01010     real_ind_ = real_ind;
<a name="l01011"></a>01011     imag_ind_ = imag_ind;
<a name="l01012"></a>01012     real_ptr_ = real_ptr;
<a name="l01013"></a>01013     imag_ptr_ = imag_ptr;
<a name="l01014"></a>01014   }
<a name="l01015"></a>01015 
<a name="l01016"></a>01016 
<a name="l01018"></a>01018 
<a name="l01022"></a>01022   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01023"></a>01023   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a10672a9a570060c9bd8f17eb54dc46fa" title="Clears the matrix without releasing memory.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l01024"></a>01024   {
<a name="l01025"></a>01025     this-&gt;data_ = NULL;
<a name="l01026"></a>01026     this-&gt;m_ = 0;
<a name="l01027"></a>01027     this-&gt;n_ = 0;
<a name="l01028"></a>01028     real_nz_ = 0;
<a name="l01029"></a>01029     real_ptr_ = NULL;
<a name="l01030"></a>01030     real_ind_ = NULL;
<a name="l01031"></a>01031     imag_nz_ = 0;
<a name="l01032"></a>01032     imag_ptr_ = NULL;
<a name="l01033"></a>01033     imag_ind_ = NULL;
<a name="l01034"></a>01034     real_data_ = NULL;
<a name="l01035"></a>01035     imag_data_ = NULL;
<a name="l01036"></a>01036   }
<a name="l01037"></a>01037 
<a name="l01038"></a>01038 
<a name="l01040"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f">01040</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01041"></a>01041   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f" title="Copies a matrix.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01042"></a>01042 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae583202802b373763ce3780fe2382f4f" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01043"></a>01043   {
<a name="l01044"></a>01044     this-&gt;<a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a8940cdf1faedc44051919b9774132fa9" title="Clears the matrix.">Clear</a>();
<a name="l01045"></a>01045     <span class="keywordtype">int</span> i = A.m_;
<a name="l01046"></a>01046     <span class="keywordtype">int</span> j = A.n_;
<a name="l01047"></a>01047     real_nz_ = A.real_nz_;
<a name="l01048"></a>01048     imag_nz_ = A.imag_nz_;
<a name="l01049"></a>01049     this-&gt;m_ = i;
<a name="l01050"></a>01050     this-&gt;n_ = j;
<a name="l01051"></a>01051     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l01052"></a>01052       {
<a name="l01053"></a>01053         this-&gt;m_ = 0;
<a name="l01054"></a>01054         this-&gt;n_ = 0;
<a name="l01055"></a>01055         this-&gt;real_nz_ = 0;
<a name="l01056"></a>01056         this-&gt;imag_nz_ = 0;
<a name="l01057"></a>01057         <span class="keywordflow">return</span>;
<a name="l01058"></a>01058       }
<a name="l01059"></a>01059 
<a name="l01060"></a>01060 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01061"></a>01061 <span class="preprocessor"></span>    <span class="keywordflow">if</span> ((real_nz_ &gt; 0
<a name="l01062"></a>01062          &amp;&amp; (j == 0
<a name="l01063"></a>01063              || static_cast&lt;long int&gt;(real_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l01064"></a>01064              &gt;= static_cast&lt;long int&gt;(i)))
<a name="l01065"></a>01065         ||
<a name="l01066"></a>01066         (imag_nz_ &gt; 0
<a name="l01067"></a>01067          &amp;&amp; (j == 0
<a name="l01068"></a>01068              || static_cast&lt;long int&gt;(imag_nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l01069"></a>01069              &gt;= static_cast&lt;long int&gt;(i))))
<a name="l01070"></a>01070       {
<a name="l01071"></a>01071         this-&gt;m_ = 0;
<a name="l01072"></a>01072         this-&gt;n_ = 0;
<a name="l01073"></a>01073         real_nz_ = 0;
<a name="l01074"></a>01074         imag_nz_ = 0;
<a name="l01075"></a>01075         real_ptr_ = NULL;
<a name="l01076"></a>01076         imag_ptr_ = NULL;
<a name="l01077"></a>01077         real_ind_ = NULL;
<a name="l01078"></a>01078         imag_ind_ = NULL;
<a name="l01079"></a>01079         this-&gt;real_data_ = NULL;
<a name="l01080"></a>01080         this-&gt;imag_data_ = NULL;
<a name="l01081"></a>01081         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01082"></a>01082                        + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01083"></a>01083                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01084"></a>01084                        + <span class="stringliteral">&quot; values for the real part and &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01085"></a>01085                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary part) than&quot;</span>)
<a name="l01086"></a>01086                        + <span class="stringliteral">&quot; elements in the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l01087"></a>01087                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l01088"></a>01088       }
<a name="l01089"></a>01089 <span class="preprocessor">#endif</span>
<a name="l01090"></a>01090 <span class="preprocessor"></span>
<a name="l01091"></a>01091 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01092"></a>01092 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01093"></a>01093       {
<a name="l01094"></a>01094 <span class="preprocessor">#endif</span>
<a name="l01095"></a>01095 <span class="preprocessor"></span>
<a name="l01096"></a>01096         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l01097"></a>01097                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01098"></a>01098         memcpy(this-&gt;real_ptr_, A.real_ptr_,
<a name="l01099"></a>01099                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01100"></a>01100 
<a name="l01101"></a>01101 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01102"></a>01102 <span class="preprocessor"></span>      }
<a name="l01103"></a>01103     <span class="keywordflow">catch</span> (...)
<a name="l01104"></a>01104       {
<a name="l01105"></a>01105         this-&gt;m_ = 0;
<a name="l01106"></a>01106         this-&gt;n_ = 0;
<a name="l01107"></a>01107         real_nz_ = 0;
<a name="l01108"></a>01108         imag_nz_ = 0;
<a name="l01109"></a>01109         real_ptr_ = NULL;
<a name="l01110"></a>01110         imag_ptr_ = NULL;
<a name="l01111"></a>01111         real_ind_ = NULL;
<a name="l01112"></a>01112         imag_ind_ = NULL;
<a name="l01113"></a>01113         this-&gt;real_data_ = NULL;
<a name="l01114"></a>01114         this-&gt;imag_data_ = NULL;
<a name="l01115"></a>01115       }
<a name="l01116"></a>01116     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l01117"></a>01117       {
<a name="l01118"></a>01118         this-&gt;m_ = 0;
<a name="l01119"></a>01119         this-&gt;n_ = 0;
<a name="l01120"></a>01120         real_nz_ = 0;
<a name="l01121"></a>01121         imag_nz_ = 0;
<a name="l01122"></a>01122         imag_ptr_ = 0;
<a name="l01123"></a>01123         real_ind_ = NULL;
<a name="l01124"></a>01124         imag_ind_ = NULL;
<a name="l01125"></a>01125         this-&gt;real_data_ = NULL;
<a name="l01126"></a>01126         this-&gt;imag_data_ = NULL;
<a name="l01127"></a>01127       }
<a name="l01128"></a>01128     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01129"></a>01129       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01130"></a>01130                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01131"></a>01131                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01132"></a>01132                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01133"></a>01133                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01134"></a>01134                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the real&quot;</span>)
<a name="l01135"></a>01135                      + <span class="stringliteral">&quot; part), for a &quot;</span>
<a name="l01136"></a>01136                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01137"></a>01137 <span class="preprocessor">#endif</span>
<a name="l01138"></a>01138 <span class="preprocessor"></span>
<a name="l01139"></a>01139 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01140"></a>01140 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01141"></a>01141       {
<a name="l01142"></a>01142 <span class="preprocessor">#endif</span>
<a name="l01143"></a>01143 <span class="preprocessor"></span>
<a name="l01144"></a>01144         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l01145"></a>01145                                                    <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01146"></a>01146         memcpy(this-&gt;imag_ptr_, A.imag_ptr_,
<a name="l01147"></a>01147                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01148"></a>01148 
<a name="l01149"></a>01149 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01150"></a>01150 <span class="preprocessor"></span>      }
<a name="l01151"></a>01151     <span class="keywordflow">catch</span> (...)
<a name="l01152"></a>01152       {
<a name="l01153"></a>01153         this-&gt;m_ = 0;
<a name="l01154"></a>01154         this-&gt;n_ = 0;
<a name="l01155"></a>01155         real_nz_ = 0;
<a name="l01156"></a>01156         imag_nz_ = 0;
<a name="l01157"></a>01157         free(real_ptr_);
<a name="l01158"></a>01158         real_ptr_ = NULL;
<a name="l01159"></a>01159         imag_ptr_ = NULL;
<a name="l01160"></a>01160         real_ind_ = NULL;
<a name="l01161"></a>01161         imag_ind_ = NULL;
<a name="l01162"></a>01162         this-&gt;real_data_ = NULL;
<a name="l01163"></a>01163         this-&gt;imag_data_ = NULL;
<a name="l01164"></a>01164       }
<a name="l01165"></a>01165     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l01166"></a>01166       {
<a name="l01167"></a>01167         this-&gt;m_ = 0;
<a name="l01168"></a>01168         this-&gt;n_ = 0;
<a name="l01169"></a>01169         real_nz_ = 0;
<a name="l01170"></a>01170         imag_nz_ = 0;
<a name="l01171"></a>01171         free(real_ptr_);
<a name="l01172"></a>01172         real_ptr_ = 0;
<a name="l01173"></a>01173         real_ind_ = NULL;
<a name="l01174"></a>01174         imag_ind_ = NULL;
<a name="l01175"></a>01175         this-&gt;real_data_ = NULL;
<a name="l01176"></a>01176         this-&gt;imag_data_ = NULL;
<a name="l01177"></a>01177       }
<a name="l01178"></a>01178     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01179"></a>01179       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01180"></a>01180                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01181"></a>01181                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01182"></a>01182                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1))
<a name="l01183"></a>01183                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l01184"></a>01184                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (for the&quot;</span>)
<a name="l01185"></a>01185                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; imaginary part), for a &quot;</span>)
<a name="l01186"></a>01186                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01187"></a>01187 <span class="preprocessor">#endif</span>
<a name="l01188"></a>01188 <span class="preprocessor"></span>
<a name="l01189"></a>01189 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01190"></a>01190 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01191"></a>01191       {
<a name="l01192"></a>01192 <span class="preprocessor">#endif</span>
<a name="l01193"></a>01193 <span class="preprocessor"></span>
<a name="l01194"></a>01194         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01195"></a>01195         memcpy(this-&gt;real_ind_, A.real_ind_, real_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01196"></a>01196 
<a name="l01197"></a>01197 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01198"></a>01198 <span class="preprocessor"></span>      }
<a name="l01199"></a>01199     <span class="keywordflow">catch</span> (...)
<a name="l01200"></a>01200       {
<a name="l01201"></a>01201         this-&gt;m_ = 0;
<a name="l01202"></a>01202         this-&gt;n_ = 0;
<a name="l01203"></a>01203         real_nz_ = 0;
<a name="l01204"></a>01204         imag_nz_ = 0;
<a name="l01205"></a>01205         free(real_ptr_);
<a name="l01206"></a>01206         free(imag_ptr_);
<a name="l01207"></a>01207         real_ptr_ = NULL;
<a name="l01208"></a>01208         imag_ptr_ = NULL;
<a name="l01209"></a>01209         real_ind_ = NULL;
<a name="l01210"></a>01210         imag_ind_ = NULL;
<a name="l01211"></a>01211         this-&gt;real_data_ = NULL;
<a name="l01212"></a>01212         this-&gt;imag_data_ = NULL;
<a name="l01213"></a>01213       }
<a name="l01214"></a>01214     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01215"></a>01215       {
<a name="l01216"></a>01216         this-&gt;m_ = 0;
<a name="l01217"></a>01217         this-&gt;n_ = 0;
<a name="l01218"></a>01218         real_nz_ = 0;
<a name="l01219"></a>01219         imag_nz_ = 0;
<a name="l01220"></a>01220         free(real_ptr_);
<a name="l01221"></a>01221         free(imag_ptr_);
<a name="l01222"></a>01222         real_ptr_ = NULL;
<a name="l01223"></a>01223         imag_ptr_ = NULL;
<a name="l01224"></a>01224         this-&gt;real_data_ = NULL;
<a name="l01225"></a>01225         this-&gt;imag_data_ = NULL;
<a name="l01226"></a>01226       }
<a name="l01227"></a>01227     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01228"></a>01228       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01229"></a>01229                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01230"></a>01230                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01231"></a>01231                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_)
<a name="l01232"></a>01232                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01233"></a>01233                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l01234"></a>01234                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01235"></a>01235 <span class="preprocessor">#endif</span>
<a name="l01236"></a>01236 <span class="preprocessor"></span>
<a name="l01237"></a>01237 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01238"></a>01238 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01239"></a>01239       {
<a name="l01240"></a>01240 <span class="preprocessor">#endif</span>
<a name="l01241"></a>01241 <span class="preprocessor"></span>
<a name="l01242"></a>01242         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01243"></a>01243         memcpy(this-&gt;imag_ind_, A.imag_ind_, imag_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01244"></a>01244 
<a name="l01245"></a>01245 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01246"></a>01246 <span class="preprocessor"></span>      }
<a name="l01247"></a>01247     <span class="keywordflow">catch</span> (...)
<a name="l01248"></a>01248       {
<a name="l01249"></a>01249         this-&gt;m_ = 0;
<a name="l01250"></a>01250         this-&gt;n_ = 0;
<a name="l01251"></a>01251         real_nz_ = 0;
<a name="l01252"></a>01252         imag_nz_ = 0;
<a name="l01253"></a>01253         free(real_ptr_);
<a name="l01254"></a>01254         free(imag_ptr_);
<a name="l01255"></a>01255         real_ptr_ = NULL;
<a name="l01256"></a>01256         imag_ptr_ = NULL;
<a name="l01257"></a>01257         free(imag_ind_);
<a name="l01258"></a>01258         real_ind_ = NULL;
<a name="l01259"></a>01259         imag_ind_ = NULL;
<a name="l01260"></a>01260         this-&gt;real_data_ = NULL;
<a name="l01261"></a>01261         this-&gt;imag_data_ = NULL;
<a name="l01262"></a>01262       }
<a name="l01263"></a>01263     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01264"></a>01264       {
<a name="l01265"></a>01265         this-&gt;m_ = 0;
<a name="l01266"></a>01266         this-&gt;n_ = 0;
<a name="l01267"></a>01267         real_nz_ = 0;
<a name="l01268"></a>01268         imag_nz_ = 0;
<a name="l01269"></a>01269         free(real_ptr_);
<a name="l01270"></a>01270         free(imag_ptr_);
<a name="l01271"></a>01271         real_ptr_ = NULL;
<a name="l01272"></a>01272         imag_ptr_ = NULL;
<a name="l01273"></a>01273         free(imag_ind_);
<a name="l01274"></a>01274         imag_ind_ = NULL;
<a name="l01275"></a>01275         this-&gt;real_data_ = NULL;
<a name="l01276"></a>01276         this-&gt;imag_data_ = NULL;
<a name="l01277"></a>01277       }
<a name="l01278"></a>01278     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01279"></a>01279       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01280"></a>01280                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01281"></a>01281                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01282"></a>01282                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_)
<a name="l01283"></a>01283                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01284"></a>01284                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l01285"></a>01285                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01286"></a>01286 <span class="preprocessor">#endif</span>
<a name="l01287"></a>01287 <span class="preprocessor"></span>
<a name="l01288"></a>01288 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01289"></a>01289 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01290"></a>01290       {
<a name="l01291"></a>01291 <span class="preprocessor">#endif</span>
<a name="l01292"></a>01292 <span class="preprocessor"></span>
<a name="l01293"></a>01293         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l01294"></a>01294         this-&gt;allocator_.memorycpy(this-&gt;real_data_, A.real_data_, real_nz_);
<a name="l01295"></a>01295 
<a name="l01296"></a>01296 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01297"></a>01297 <span class="preprocessor"></span>      }
<a name="l01298"></a>01298     <span class="keywordflow">catch</span> (...)
<a name="l01299"></a>01299       {
<a name="l01300"></a>01300         this-&gt;m_ = 0;
<a name="l01301"></a>01301         this-&gt;n_ = 0;
<a name="l01302"></a>01302         free(real_ptr_);
<a name="l01303"></a>01303         free(imag_ptr_);
<a name="l01304"></a>01304         real_ptr_ = NULL;
<a name="l01305"></a>01305         imag_ptr_ = NULL;
<a name="l01306"></a>01306         free(real_ind_);
<a name="l01307"></a>01307         free(imag_ind_);
<a name="l01308"></a>01308         real_ind_ = NULL;
<a name="l01309"></a>01309         imag_ind_ = NULL;
<a name="l01310"></a>01310         this-&gt;real_data_ = NULL;
<a name="l01311"></a>01311         this-&gt;imag_data_ = NULL;
<a name="l01312"></a>01312       }
<a name="l01313"></a>01313     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01314"></a>01314       {
<a name="l01315"></a>01315         this-&gt;m_ = 0;
<a name="l01316"></a>01316         this-&gt;n_ = 0;
<a name="l01317"></a>01317         free(real_ptr_);
<a name="l01318"></a>01318         free(imag_ptr_);
<a name="l01319"></a>01319         real_ptr_ = NULL;
<a name="l01320"></a>01320         imag_ptr_ = NULL;
<a name="l01321"></a>01321         free(real_ind_);
<a name="l01322"></a>01322         free(imag_ind_);
<a name="l01323"></a>01323         real_ind_ = NULL;
<a name="l01324"></a>01324         imag_ind_ = NULL;
<a name="l01325"></a>01325         imag_data_ = NULL;
<a name="l01326"></a>01326       }
<a name="l01327"></a>01327     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01328"></a>01328       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01329"></a>01329                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01330"></a>01330                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01331"></a>01331                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_)
<a name="l01332"></a>01332                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01333"></a>01333                      + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l01334"></a>01334                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01335"></a>01335 <span class="preprocessor">#endif</span>
<a name="l01336"></a>01336 <span class="preprocessor"></span>
<a name="l01337"></a>01337 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01338"></a>01338 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01339"></a>01339       {
<a name="l01340"></a>01340 <span class="preprocessor">#endif</span>
<a name="l01341"></a>01341 <span class="preprocessor"></span>
<a name="l01342"></a>01342         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l01343"></a>01343         this-&gt;allocator_.memorycpy(this-&gt;imag_data_, A.imag_data_, imag_nz_);
<a name="l01344"></a>01344 
<a name="l01345"></a>01345 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01346"></a>01346 <span class="preprocessor"></span>      }
<a name="l01347"></a>01347     <span class="keywordflow">catch</span> (...)
<a name="l01348"></a>01348       {
<a name="l01349"></a>01349         this-&gt;m_ = 0;
<a name="l01350"></a>01350         this-&gt;n_ = 0;
<a name="l01351"></a>01351         free(real_ptr_);
<a name="l01352"></a>01352         free(imag_ptr_);
<a name="l01353"></a>01353         real_ptr_ = NULL;
<a name="l01354"></a>01354         imag_ptr_ = NULL;
<a name="l01355"></a>01355         free(real_ind_);
<a name="l01356"></a>01356         free(imag_ind_);
<a name="l01357"></a>01357         real_ind_ = NULL;
<a name="l01358"></a>01358         imag_ind_ = NULL;
<a name="l01359"></a>01359         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01360"></a>01360         this-&gt;real_data_ = NULL;
<a name="l01361"></a>01361         this-&gt;imag_data_ = NULL;
<a name="l01362"></a>01362       }
<a name="l01363"></a>01363     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01364"></a>01364       {
<a name="l01365"></a>01365         this-&gt;m_ = 0;
<a name="l01366"></a>01366         this-&gt;n_ = 0;
<a name="l01367"></a>01367         free(real_ptr_);
<a name="l01368"></a>01368         free(imag_ptr_);
<a name="l01369"></a>01369         real_ptr_ = NULL;
<a name="l01370"></a>01370         imag_ptr_ = NULL;
<a name="l01371"></a>01371         free(real_ind_);
<a name="l01372"></a>01372         free(imag_ind_);
<a name="l01373"></a>01373         real_ind_ = NULL;
<a name="l01374"></a>01374         imag_ind_ = NULL;
<a name="l01375"></a>01375         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01376"></a>01376         real_data_ = NULL;
<a name="l01377"></a>01377       }
<a name="l01378"></a>01378     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l01379"></a>01379       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_ComplexSparse::&quot;</span>)
<a name="l01380"></a>01380                      + <span class="stringliteral">&quot;Matrix_ComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01381"></a>01381                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01382"></a>01382                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_)
<a name="l01383"></a>01383                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01384"></a>01384                      + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l01385"></a>01385                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01386"></a>01386 <span class="preprocessor">#endif</span>
<a name="l01387"></a>01387 <span class="preprocessor"></span>
<a name="l01388"></a>01388   }
<a name="l01389"></a>01389 
<a name="l01390"></a>01390 
<a name="l01391"></a>01391   <span class="comment">/*******************</span>
<a name="l01392"></a>01392 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l01393"></a>01393 <span class="comment">   *******************/</span>
<a name="l01394"></a>01394 
<a name="l01395"></a>01395 
<a name="l01397"></a>01397 
<a name="l01403"></a>01403   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01404"></a>01404   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a543d8834abfbd7888c6d2516612bde7b" title="Returns the number of elements stored in memory.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l01405"></a>01405 <span class="keyword">  </span>{
<a name="l01406"></a>01406     <span class="keywordflow">return</span> real_nz_ + imag_nz_;
<a name="l01407"></a>01407   }
<a name="l01408"></a>01408 
<a name="l01409"></a>01409 
<a name="l01411"></a>01411 
<a name="l01415"></a>01415   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01416"></a>01416   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#afa783bff3e823f8ad2480a50d4d28929" title="Returns (row or column) start indices for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealPtr</a>()<span class="keyword"> const</span>
<a name="l01417"></a>01417 <span class="keyword">  </span>{
<a name="l01418"></a>01418     <span class="keywordflow">return</span> real_ptr_;
<a name="l01419"></a>01419   }
<a name="l01420"></a>01420 
<a name="l01421"></a>01421 
<a name="l01423"></a>01423 
<a name="l01427"></a>01427   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01428"></a>01428   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a6766b6db59aa35b4fa7458a12e531e9b" title="Returns (row or column) start indices for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagPtr</a>()<span class="keyword"> const</span>
<a name="l01429"></a>01429 <span class="keyword">  </span>{
<a name="l01430"></a>01430     <span class="keywordflow">return</span> imag_ptr_;
<a name="l01431"></a>01431   }
<a name="l01432"></a>01432 
<a name="l01433"></a>01433 
<a name="l01435"></a>01435 
<a name="l01442"></a>01442   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01443"></a>01443   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a4eb0b0535cb51c9c0886da147f6e0cb0" title="Returns (row or column) indices of non-zero entries for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealInd</a>()<span class="keyword"> const</span>
<a name="l01444"></a>01444 <span class="keyword">  </span>{
<a name="l01445"></a>01445     <span class="keywordflow">return</span> real_ind_;
<a name="l01446"></a>01446   }
<a name="l01447"></a>01447 
<a name="l01448"></a>01448 
<a name="l01451"></a>01451 
<a name="l01458"></a>01458   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01459"></a>01459   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a08f3d91609557b1650066765343909db" title="Returns (row or column) indices of non-zero entries //! for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagInd</a>()<span class="keyword"> const</span>
<a name="l01460"></a>01460 <span class="keyword">  </span>{
<a name="l01461"></a>01461     <span class="keywordflow">return</span> imag_ind_;
<a name="l01462"></a>01462   }
<a name="l01463"></a>01463 
<a name="l01464"></a>01464 
<a name="l01466"></a>01466 
<a name="l01469"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882">01469</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01470"></a>01470   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882" title="Returns the length of the array of start indices for the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01471"></a>01471 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3be4bf190b7c5957d81ddf7e7a7b7882" title="Returns the length of the array of start indices for the real part.">  ::GetRealPtrSize</a>()<span class="keyword"> const</span>
<a name="l01472"></a>01472 <span class="keyword">  </span>{
<a name="l01473"></a>01473     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l01474"></a>01474   }
<a name="l01475"></a>01475 
<a name="l01476"></a>01476 
<a name="l01478"></a>01478 
<a name="l01481"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2">01481</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01482"></a>01482   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2" title="Returns the length of the array of start indices for the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01483"></a>01483 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae97357d4afa167ca95d28e8897e44ef2" title="Returns the length of the array of start indices for the imaginary part.">  ::GetImagPtrSize</a>()<span class="keyword"> const</span>
<a name="l01484"></a>01484 <span class="keyword">  </span>{
<a name="l01485"></a>01485     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l01486"></a>01486   }
<a name="l01487"></a>01487 
<a name="l01488"></a>01488 
<a name="l01491"></a>01491 
<a name="l01500"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684">01500</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01501"></a>01501   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684" title="Returns the length of the array of (column or row) indices //! for the real part...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01502"></a>01502 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ae573e32219c095276106254a84e7f684" title="Returns the length of the array of (column or row) indices //! for the real part...">  ::GetRealIndSize</a>()<span class="keyword"> const</span>
<a name="l01503"></a>01503 <span class="keyword">  </span>{
<a name="l01504"></a>01504     <span class="keywordflow">return</span> real_nz_;
<a name="l01505"></a>01505   }
<a name="l01506"></a>01506 
<a name="l01507"></a>01507 
<a name="l01510"></a>01510 
<a name="l01519"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764">01519</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01520"></a>01520   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764" title="Returns the length of the array of (column or row) indices //! for the imaginary...">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01521"></a>01521 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad61069ad96f1bfd7b57f6a4e9fe85764" title="Returns the length of the array of (column or row) indices //! for the imaginary...">  ::GetImagIndSize</a>()<span class="keyword"> const</span>
<a name="l01522"></a>01522 <span class="keyword">  </span>{
<a name="l01523"></a>01523     <span class="keywordflow">return</span> imag_nz_;
<a name="l01524"></a>01524   }
<a name="l01525"></a>01525 
<a name="l01526"></a>01526 
<a name="l01528"></a>01528 
<a name="l01531"></a>01531   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01532"></a>01532   T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a74325c4f424c0043682b5cb3ad3df501" title="Returns the array of values of the real part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealData</a>()<span class="keyword"> const</span>
<a name="l01533"></a>01533 <span class="keyword">  </span>{
<a name="l01534"></a>01534     <span class="keywordflow">return</span> real_data_;
<a name="l01535"></a>01535   }
<a name="l01536"></a>01536 
<a name="l01537"></a>01537 
<a name="l01539"></a>01539 
<a name="l01542"></a>01542   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01543"></a>01543   T* <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a5637e2dd95b0e62a7f42c0f3fd1383b1" title="Returns the array of values of the imaginary part.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagData</a>()<span class="keyword"> const</span>
<a name="l01544"></a>01544 <span class="keyword">  </span>{
<a name="l01545"></a>01545     <span class="keywordflow">return</span> imag_data_;
<a name="l01546"></a>01546   }
<a name="l01547"></a>01547 
<a name="l01548"></a>01548 
<a name="l01549"></a>01549   <span class="comment">/**********************************</span>
<a name="l01550"></a>01550 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01551"></a>01551 <span class="comment">   **********************************/</span>
<a name="l01552"></a>01552 
<a name="l01553"></a>01553 
<a name="l01555"></a>01555 
<a name="l01561"></a>01561   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01562"></a>01562   <span class="keyword">inline</span> complex&lt;typename Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01563"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea">01563</a>                  ::value_type&gt;
<a name="l01564"></a>01564   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea" title="Access operator.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01565"></a>01565 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01566"></a>01566 <span class="keyword">  </span>{
<a name="l01567"></a>01567 
<a name="l01568"></a>01568 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01569"></a>01569 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01570"></a>01570       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::operator()&quot;</span>,
<a name="l01571"></a>01571                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01572"></a>01572                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01573"></a>01573     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01574"></a>01574       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::operator()&quot;</span>,
<a name="l01575"></a>01575                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01576"></a>01576                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01577"></a>01577 <span class="preprocessor">#endif</span>
<a name="l01578"></a>01578 <span class="preprocessor"></span>
<a name="l01579"></a>01579     <span class="keywordtype">int</span> real_k, imag_k, l;
<a name="l01580"></a>01580     <span class="keywordtype">int</span> real_a, real_b;
<a name="l01581"></a>01581     <span class="keywordtype">int</span> imag_a, imag_b;
<a name="l01582"></a>01582 
<a name="l01583"></a>01583     real_a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l01584"></a>01584     real_b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01585"></a>01585 
<a name="l01586"></a>01586     imag_a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l01587"></a>01587     imag_b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01588"></a>01588 
<a name="l01589"></a>01589     <span class="keywordflow">if</span> (real_a != real_b)
<a name="l01590"></a>01590       {
<a name="l01591"></a>01591         l = Storage::GetSecond(i, j);
<a name="l01592"></a>01592         <span class="keywordflow">for</span> (real_k = real_a;
<a name="l01593"></a>01593              (real_k &lt;real_b-1) &amp;&amp; (real_ind_[real_k] &lt; l);
<a name="l01594"></a>01594              real_k++);
<a name="l01595"></a>01595         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l01596"></a>01596           {
<a name="l01597"></a>01597             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l01598"></a>01598                  (imag_k &lt; imag_b-1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l01599"></a>01599                  imag_k++);
<a name="l01600"></a>01600             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l01601"></a>01601               {
<a name="l01602"></a>01602                 <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01603"></a>01603                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], imag_data_[imag_k]);
<a name="l01604"></a>01604                 <span class="keywordflow">else</span>
<a name="l01605"></a>01605                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l01606"></a>01606               }
<a name="l01607"></a>01607             <span class="keywordflow">else</span>
<a name="l01608"></a>01608               <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01609"></a>01609                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l01610"></a>01610               <span class="keywordflow">else</span>
<a name="l01611"></a>01611                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01612"></a>01612           }
<a name="l01613"></a>01613         <span class="keywordflow">else</span>
<a name="l01614"></a>01614           {
<a name="l01615"></a>01615             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l01616"></a>01616               <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l01617"></a>01617             <span class="keywordflow">else</span>
<a name="l01618"></a>01618               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01619"></a>01619           }
<a name="l01620"></a>01620       }
<a name="l01621"></a>01621     <span class="keywordflow">else</span>
<a name="l01622"></a>01622       {
<a name="l01623"></a>01623         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l01624"></a>01624           {
<a name="l01625"></a>01625             l = Storage::GetSecond(i, j);
<a name="l01626"></a>01626             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l01627"></a>01627                  (imag_k &lt; imag_b-1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l01628"></a>01628                  imag_k++);
<a name="l01629"></a>01629             <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01630"></a>01630               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l01631"></a>01631             <span class="keywordflow">else</span>
<a name="l01632"></a>01632               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01633"></a>01633           }
<a name="l01634"></a>01634         <span class="keywordflow">else</span>
<a name="l01635"></a>01635           <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01636"></a>01636       }
<a name="l01637"></a>01637 
<a name="l01638"></a>01638   }
<a name="l01639"></a>01639 
<a name="l01640"></a>01640 
<a name="l01642"></a>01642 
<a name="l01648"></a>01648   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01649"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9">01649</a>   <span class="keyword">inline</span> complex&lt;typename Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01650"></a>01650                  ::value_type&gt;&amp;
<a name="l01651"></a>01651   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9" title="Unavailable access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01652"></a>01652   {
<a name="l01653"></a>01653     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l01654"></a>01654   }
<a name="l01655"></a>01655 
<a name="l01656"></a>01656 
<a name="l01658"></a>01658 
<a name="l01664"></a>01664   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01665"></a>01665   <span class="keyword">inline</span>
<a name="l01666"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a61f989f4b8128edac8f6b873ea88030d">01666</a>   <span class="keyword">const</span> complex&lt;typename Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01667"></a>01667                 ::value_type&gt;&amp;
<a name="l01668"></a>01668   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a63364150adf37907e6622c21d5f0a5f9" title="Unavailable access method.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01669"></a>01669 <span class="keyword">  </span>{
<a name="l01670"></a>01670     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_ComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l01671"></a>01671   }
<a name="l01672"></a>01672 
<a name="l01673"></a>01673 
<a name="l01675"></a>01675 
<a name="l01680"></a>01680   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01681"></a><a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383">01681</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l01682"></a>01682   <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383" title="Duplicates a matrix (assignment operator).">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01683"></a>01683 <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01684"></a>01684   {
<a name="l01685"></a>01685     this-&gt;Copy(A);
<a name="l01686"></a>01686 
<a name="l01687"></a>01687     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01688"></a>01688   }
<a name="l01689"></a>01689 
<a name="l01690"></a>01690 
<a name="l01691"></a>01691   <span class="comment">/************************</span>
<a name="l01692"></a>01692 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l01693"></a>01693 <span class="comment">   ************************/</span>
<a name="l01694"></a>01694 
<a name="l01695"></a>01695 
<a name="l01697"></a>01697 
<a name="l01702"></a>01702   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01703"></a>01703   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php#a3bf9cba0b13332169dd7bf67810387dd" title="Displays the matrix on the standard output.">Matrix_ComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01704"></a>01704 <span class="keyword">  </span>{
<a name="l01705"></a>01705     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01706"></a>01706       {
<a name="l01707"></a>01707         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l01708"></a>01708           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l01709"></a>01709         cout &lt;&lt; endl;
<a name="l01710"></a>01710       }
<a name="l01711"></a>01711   }
<a name="l01712"></a>01712 
<a name="l01713"></a>01713 
<a name="l01714"></a>01714 
<a name="l01716"></a>01716   <span class="comment">// MATRIX&lt;COLCOMPLEXSPARSE&gt; //</span>
<a name="l01718"></a>01718 <span class="comment"></span>
<a name="l01719"></a>01719 
<a name="l01720"></a>01720   <span class="comment">/****************</span>
<a name="l01721"></a>01721 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01722"></a>01722 <span class="comment">   ****************/</span>
<a name="l01723"></a>01723 
<a name="l01725"></a>01725 
<a name="l01728"></a>01728   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01729"></a>01729   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93" title="Default constructor.">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01730"></a>01730     Matrix_ComplexSparse&lt;T, Prop, ColComplexSparse, Allocator&gt;()
<a name="l01731"></a>01731   {
<a name="l01732"></a>01732   }
<a name="l01733"></a>01733 
<a name="l01734"></a>01734 
<a name="l01736"></a>01736 
<a name="l01740"></a>01740   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01741"></a>01741   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93" title="Default constructor.">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01742"></a>01742     Matrix_ComplexSparse&lt;T, Prop, ColComplexSparse, Allocator&gt;(i, j, 0, 0)
<a name="l01743"></a>01743   {
<a name="l01744"></a>01744   }
<a name="l01745"></a>01745 
<a name="l01746"></a>01746 
<a name="l01748"></a>01748 
<a name="l01756"></a>01756   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01757"></a>01757   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a3480b30876e7433f0ce962194bbf7a93" title="Default constructor.">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01758"></a>01758                                                        <span class="keywordtype">int</span> real_nz,
<a name="l01759"></a>01759                                                        <span class="keywordtype">int</span> imag_nz):
<a name="l01760"></a>01760     Matrix_ComplexSparse&lt;T, Prop, ColComplexSparse, Allocator&gt;(i, j,
<a name="l01761"></a>01761                                                                real_nz,
<a name="l01762"></a>01762                                                                imag_nz)
<a name="l01763"></a>01763   {
<a name="l01764"></a>01764   }
<a name="l01765"></a>01765 
<a name="l01766"></a>01766 
<a name="l01768"></a>01768 
<a name="l01786"></a>01786   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01787"></a>01787   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01788"></a>01788             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01789"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_complex_sparse_00_01_allocator_01_4.php#a35df5579e443bcd49dcdc381869ae507">01789</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01790"></a>01790   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColComplexSparse, Allocator&gt;::</a>
<a name="l01791"></a>01791 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01792"></a>01792          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l01793"></a>01793          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l01794"></a>01794          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l01795"></a>01795          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l01796"></a>01796          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l01797"></a>01797          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l01798"></a>01798     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_complex_sparse.php">ColComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01799"></a>01799                                                                real_values,
<a name="l01800"></a>01800                                                                real_ptr,
<a name="l01801"></a>01801                                                                real_ind,
<a name="l01802"></a>01802                                                                imag_values,
<a name="l01803"></a>01803                                                                imag_ptr,
<a name="l01804"></a>01804                                                                imag_ind)
<a name="l01805"></a>01805   {
<a name="l01806"></a>01806   }
<a name="l01807"></a>01807 
<a name="l01808"></a>01808 
<a name="l01809"></a>01809 
<a name="l01811"></a>01811   <span class="comment">// MATRIX&lt;ROWCOMPLEXSPARSE&gt; //</span>
<a name="l01813"></a>01813 <span class="comment"></span>
<a name="l01814"></a>01814 
<a name="l01815"></a>01815   <span class="comment">/****************</span>
<a name="l01816"></a>01816 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01817"></a>01817 <span class="comment">   ****************/</span>
<a name="l01818"></a>01818 
<a name="l01820"></a>01820 
<a name="l01823"></a>01823   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01824"></a>01824   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01825"></a>01825     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a>, Allocator&gt;()
<a name="l01826"></a>01826   {
<a name="l01827"></a>01827   }
<a name="l01828"></a>01828 
<a name="l01829"></a>01829 
<a name="l01831"></a>01831 
<a name="l01835"></a>01835   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01836"></a>01836   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#ac7efc201b54c0a4eeea3753a1d9c5412" title="Default constructor.">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01837"></a>01837     Matrix_ComplexSparse&lt;T, Prop, RowComplexSparse, Allocator&gt;(i, j, 0, 0)
<a name="l01838"></a>01838   {
<a name="l01839"></a>01839   }
<a name="l01840"></a>01840 
<a name="l01841"></a>01841 
<a name="l01850"></a>01850   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01851"></a>01851   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#ac7efc201b54c0a4eeea3753a1d9c5412" title="Default constructor.">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01852"></a>01852                                                        <span class="keywordtype">int</span> real_nz,
<a name="l01853"></a>01853                                                        <span class="keywordtype">int</span> imag_nz):
<a name="l01854"></a>01854     Matrix_ComplexSparse&lt;T, Prop, RowComplexSparse, Allocator&gt;(i, j,
<a name="l01855"></a>01855                                                                real_nz,
<a name="l01856"></a>01856                                                                imag_nz)
<a name="l01857"></a>01857   {
<a name="l01858"></a>01858   }
<a name="l01859"></a>01859 
<a name="l01860"></a>01860 
<a name="l01862"></a>01862 
<a name="l01880"></a>01880   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01881"></a>01881   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01882"></a>01882             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01883"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_complex_sparse_00_01_allocator_01_4.php#a4b28e5078f55b5d146712d0e046a0124">01883</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01884"></a>01884   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowComplexSparse, Allocator&gt;::</a>
<a name="l01885"></a>01885 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01886"></a>01886          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l01887"></a>01887          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l01888"></a>01888          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l01889"></a>01889          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l01890"></a>01890          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l01891"></a>01891          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l01892"></a>01892     <a class="code" href="class_seldon_1_1_matrix___complex_sparse.php" title="Complex sparse-matrix class.">Matrix_ComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_complex_sparse.php">RowComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01893"></a>01893                                                                real_values,
<a name="l01894"></a>01894                                                                real_ptr,
<a name="l01895"></a>01895                                                                real_ind,
<a name="l01896"></a>01896                                                                imag_values,
<a name="l01897"></a>01897                                                                imag_ptr,
<a name="l01898"></a>01898                                                                imag_ind)
<a name="l01899"></a>01899   {
<a name="l01900"></a>01900   }
<a name="l01901"></a>01901 
<a name="l01902"></a>01902 
<a name="l01903"></a>01903 } <span class="comment">// namespace Seldon.</span>
<a name="l01904"></a>01904 
<a name="l01905"></a>01905 <span class="preprocessor">#define SELDON_FILE_MATRIX_COMPLEXSPARSE_CXX</span>
<a name="l01906"></a>01906 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
