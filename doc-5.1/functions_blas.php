<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>BLAS Functions </h1>  </div>
</div>
<div class="contents">
<p>Those functions are available both for dense and sparse vectors. In the case of dense vectors, Blas subroutines are called if <code>SELDON_WITH_BLAS</code> (or, for backward compatibility, <code>SELDON_WITH_CBLAS</code>) is defined.</p>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#mlt">Mlt </a> </td><td class="category-table-td">multiplies the elements of the vector/matrix by a scalar, or performs a matrix-vector product   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#mltadd">MltAdd </a> </td><td class="category-table-td">performs a matrix-vector product or a matrix-matrix product  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#add">Add </a> </td><td class="category-table-td">adds two vectors or two matrices   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#copy">Copy </a> </td><td class="category-table-td">copies a vector into another one, or a matrix into another matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#swap">Swap </a> </td><td class="category-table-td">exchanges two vectors   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#genrot">GenRot </a> </td><td class="category-table-td">computes givens rotation   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#genrot">GenModifRot </a> </td><td class="category-table-td">computes givens roation   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#genrot">ApplyRot </a> </td><td class="category-table-td">applies givens rotation   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#genrot">ApplyModifRot </a> </td><td class="category-table-td">applies givens rotation   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#dotprod">DotProd </a> </td><td class="category-table-td">scalar product between two vectors  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#dotprod">DotProdConj </a> </td><td class="category-table-td">scalar product between two vectors, first vector being conjugated  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#conjugate">Conjugate </a> </td><td class="category-table-td">conjugates all elements of a vector  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getmaxabsindex">GetMaxAbsIndex </a> </td><td class="category-table-td">returns index where highest absolute value is reached   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#norm1">Norm1 </a> </td><td class="category-table-td">returns 1-norm of a vector   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#norm2">Norm2 </a> </td><td class="category-table-td">returns 2-norm of a vector  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#rank1update">Rank1Update </a> </td><td class="category-table-td">Adds a contribution X.Y' to a matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#rank2update">Rank2Update </a> </td><td class="category-table-td">Adds a contribution X.Y' + Y.X' to a symmetric matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#solve">Solve </a> </td><td class="category-table-td">solves a triangular system   </td></tr>
</table>
<div class="separator"><a class="anchor" id="mlt"></a></div><h3>Mlt</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Mlt(T0, Vector&amp;);
  void Mlt(T0, Matrix&amp;);
  void Mlt(Matrix&amp;, const Vector&amp;, Vector&amp;);
</pre><p>This function multiplies all elements of vector/matrix by a constant. It can also perform a matrix-vector product. This function is implemented both for dense and sparse vectors/matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; X(3);
X.Fill();
// we multiply all elements by 2
Mlt(2.0, X);

// similar method for matrix
Matrix&lt;double&gt; A(3,3);
A.Fill();
// we multiply all elements by -3.4
Mlt(-3.4, A);

// and sparse matrix as well
Matrix&lt;double, General, ArrayRowSparse&gt; Asp(3,3);
Asp(0, 1) = 1.4;
Asp(1, 1) = -0.3; 
Asp(2, 0) = 2.1;
Mlt(2.1, Asp);

// with Mlt, you can compute Y = Asp*X or Y = A*X
Vector&lt;double&gt; Y(3);
Mlt(A, X, Y);
Mlt(Asp, X, Y);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="class_vector.php#operator">Vector operators</a><br/>
 <a href="class_matrix.php#operator">Matrix operators</a><br/>
 <a href="#mltadd">MltAdd</a></p>
<h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_functions___mat_vect_8cxx_source.php">Functions_MatVect.cxx</a><br/>
 <a class="el" href="_functions___matrix_array_8cxx_source.php">Functions_MatrixArray.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a><br/>
 <a class="el" href="_blas__2_8cxx_source.php">Blas_2.cxx</a></p>
<div class="separator"><a class="anchor" id="mltadd"></a></div><h3>MltAdd</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void MltAdd(T0, const Matrix&amp;, const Vector&amp;, T0, Vector&amp;);
  void MltAdd(T0, SeldonTrans, const Matrix&amp;, const Vector&amp;, T0, Vector&amp;);
  void MltAdd(T0, const Matrix&amp;, const Matrix&amp;, T0, Matrix&amp;);
  void MltAdd(T0, SeldonTrans, const Matrix&amp;, SeldonTrans, const Matrix&amp;,
              T0, Matrix&amp;);
</pre><p>This function performs a matrix-vector product or a matrix-matrix product. You can specify a product with the transpose of the matrix, or the conjugate of the transpose. The size of matrices need to be compatible in order to complete the matrix-matrix product. The matrix-matrix product is not available for sparse matrices.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// matrix-vector product
Matrix&lt;double&gt; A(3,3);
Vector&lt;double&gt; X(3), B(3);
A.Fill();
X.Fill();
B.Fill(1.5);
double beta = 2, alpha = 3;
// computation of B = beta*B + alpha*A*X
MltAdd(alpha, A, X, beta, B);

// same function for sparse matrices
Matrix&lt;double, General, RowSparse&gt; Asp(3, 3, nnz, values, ptr, ind);
// computation of B = beta*B + alpha*Asp*X
MltAdd(alpha, Asp, X, beta, B);

// you can compute B = beta*B + alpha*transpose(Asp)*X
MltAdd(alpha, SeldonTrans, Asp, X, beta, B);

// similar method for matrix-matrix product
Matrix&lt;double&gt; M(3,4), N(4,3);
// computation of A = beta*A + alpha*M*N
MltAdd(alpha, M, N, beta, A);

// and you can use SeldonTrans, SeldonNoTrans, SeldonConjTrans

// computation of A = beta*A + alpha*transpose(M)*N
MltAdd(alpha, SeldonTrans, M, SeldonNoTrans, N, beta, A);

// SeldonConjTrans is meaningfull for complex matrices
Matrix&lt;complex&lt;double&gt; &gt; Ac(3, 3), Bc(3, 3), Cc(3, 3);

// computation of Ac = beta * Ac
//                     + alpha * conjugate(transpose(Bc)) * transpose(Cc)
MltAdd(alpha, SeldonConjTrans, Bc, SeldonTrans, Cc, beta, Ac);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="class_vector.php#operator">Vector operators</a><br/>
 <a href="class_matrix.php#operator">Matrix operators</a><br/>
 <a href="#mlt">Mlt</a></p>
<h4>Location :</h4>
<p><a class="el" href="_functions___mat_vect_8cxx_source.php">Functions_MatVect.cxx</a><br/>
 <a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a><br/>
 <a class="el" href="_functions___matrix_array_8cxx_source.php">Functions_MatrixArray.cxx</a><br/>
 <a class="el" href="_blas__2_8cxx_source.php">Blas_2.cxx</a><br/>
 <a class="el" href="_blas__3_8cxx_source.php">Blas_3.cxx</a></p>
<div class="separator"><a class="anchor" id="add"></a></div><h3>Add</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void Add(T0, const Vector&amp;, Vector&amp;);
  void Add(T0, const Matrix&amp;, Matrix&amp;);
</pre><p>This function adds two vectors or two matrices. This function is available both for sparse or dense vectors/matrices. The size of the vectors or the matrices to add should be the same.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Vector&lt;double&gt; X(3), Y(3);
X.Fill(); Y.Fill(1);
double alpha = 2;
// computation of Y = Y + alpha*X
Add(alpha, X, Y);

// similar method for matrix
Matrix&lt;double&gt; A(3,3), B(3,3);
A.Fill();
B.SetIdentity();
Add(alpha, A, B);

// and sparse matrix as well
Matrix&lt;double, General, ArrayRowSparse&gt; Asp(3,3), Bsp(3,3);
Asp(0, 1) = 1.4;
Asp(1, 1) = -0.3; 
Asp(2, 0) = 2.1;
Bsp.SetIdentity();
Add(alpha, Asp, Bsp);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_functions___matrix_8cxx_source.php">Functions_Matrix.cxx</a><br/>
 <a class="el" href="_functions___matrix_array_8cxx_source.php">Functions_MatrixArray.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="copy"></a></div><h3>Copy</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Copy(const Vector&amp;, Vector&amp;);
  void Copy(const Matrix&amp; Matrix&amp;);
</pre><p>A vector is copied into another one. If BLAS is enabled, the two dense vectors have to be of the same size. The operator = is more flexible. For sparse matrices, the function <code>Copy</code> is overloaded to enable the conversion between the different formats.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// vectors need to have the same size
Vector&lt;float&gt; V(3), W(3);
V.Fill();
Copy(V, W);
// it is equivalent to write W = V;
W = V;

// for sparse matrices, you can use Copy as a convert tool
Matrix&lt;double, General, ArrayRowSparse&gt; A(3, 3);
A.AddInteraction(0, 2, 2.5);
A.AddInteraction(1, 1, -1.0);

Matrix&lt;double, General, RowSparse&gt; B;
Copy(A, B);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="class_vector.php#operator">Vector operators</a></p>
<h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a><br/>
 <a class="el" href="_matrix___conversions_8cxx_source.php">Matrix_Conversions.cxx</a></p>
<div class="separator"><a class="anchor" id="swap"></a></div><h3>Swap</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Swap(Vector&amp; Vector&amp;);
</pre><p>The two vectors are exchanged. If BLAS is enabled, the two dense vectors have to be of the same size.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// vectors need to have the same size
Vector&lt;float&gt; V(3), W(3);
V.Fill(); 
W.Fill(1.0);
Swap(V, W);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="class_vector.php#operator">Vector operators</a></p>
<h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="genrot"></a></div><h3>GenRot, ApplyRot, GenModifRot, ApplyModifRot</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void GenRot(T&amp;, T&amp;, T&amp;, T&amp;);
  void ApplyRot(Vector&amp;, Vector&amp;, T, T); 
  void GenModifRot(T&amp;, T&amp;, T&amp;, T&amp;, T*);
  void ApplyModifRot(Vector&amp;, Vector&amp;, T*); 
</pre><p>This function constructs the Givens rotation G = [cos_, sin_; -sin_, cos_] that zeros the second entry of the two vector (a,b).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
double a, b, cos_, sin_;
a = 2.0;
b = 1.0;
// we compute cos_ and sin_ so that
// G = [cos_, sin_; -sin_, cos_] zeros second entry of vector [a;b]
// i.e. G*[a;b] = [r;0]
GenRot(a, b, cos_, sin_);

// then we can apply rotation G to vectors X, Y
// G*[x_i, y_i] for all i
Vector&lt;double&gt; X(10), Y(10);
X.FillRand(); 
Y.FillRand();
ApplyRot(X, Y, cos_, sin_);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="dotprod"></a></div><h3>DotProd, DotProdConj</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  T DotProd(const Vector&amp;, const Vector&amp;);
  T DotProdConj(const Vector&amp;, const Vector&amp;);
</pre><p>This function returns the scalar product between two vectors. For <code>DotProdConj</code>, the first vector is conjugated. In the case of dense vectors, they have to be of the same size.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// we construct two vectors of same size
Vector&lt;double&gt; X(10), Y(10);
X.FillRand(); 
Y.FillRand();

// computation of X*Y
double scal = DotProd(X, Y);

// for complex numbers
Vector&lt;complex&lt;double&gt; &gt; Xc(10), Yc(10);
Xc.Fill(); Yc.Fill();
// computation of conj(X)*Y
complex&lt;double&gt; scalc = DotProdConj(X, Y);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="conjugate"></a></div><h3>Conjugate</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Conjugate(Vector&amp;);
</pre><p>Each component of the vector is conjugated. In the case of real numbers, the vector is not modified.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// complex vector
Vector&lt;complex&lt;double&gt; &gt; X(10);
X.FillRand(); 

// we take the conjugate
Conjugate(X);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="getmaxabsindex"></a></div><h3>GetMaxAbsIndex</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
   int GetMaxAbsIndex(const Vector&amp;);
</pre><p>This function returns the index for which the vector reached its highest absolute value.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// complex vector
Vector&lt;complex&lt;double&gt; &gt; X(10);
X.FillRand(); 

int imax = GetMaxAbsIndex(X);
// maximum ?
double maximum = abs(X(imax));
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="norm1"></a></div><h3>Norm1</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
   T Norm1(const Vector&amp;);
</pre><p>This function returns the 1-norm of the vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// complex vector
Vector&lt;complex&lt;double&gt; &gt; X(10);
X.Fill(); 

// 1-norm
double norme = Norm1(X);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="norm2"></a></div><h3>Norm2</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
   T Norm2(const Vector&amp;);
</pre><p>This function returns the 2-norm of the vector.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// complex vector
Vector&lt;complex&lt;double&gt; &gt; X(10);
X.Fill(); 

// 2-norm
double norme = Norm2(X);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_functions___vector_8cxx_source.php">Functions_Vector.cxx</a><br/>
 <a class="el" href="_blas__1_8cxx_source.php">Blas_1.cxx</a></p>
<div class="separator"><a class="anchor" id="rank1update"></a></div><h3>Rank1Update</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Rank1Update(T, const Vector&amp;, const Vector&amp;, Matrix&amp;);
  void Rank1Update(T, const Vector&amp;, SeldonConj, const Vector&amp;, Matrix&amp;);
  void Rank1Update(T, const Vector&amp;, Matrix&amp;);
</pre><p>This function adds a contribution of rank 1 to matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// two complex vectors
Vector&lt;complex&lt;double&gt; &gt; X(10), Y(10);
X.FillRand(); 
Y.FillRand(); 

// complex matrix
Matrix&lt;complex&lt;double&gt; &gt; A(10, 10);
A.SetIdentity();
complex&lt;double&gt; alpha(1, 1);

// we compute A = A + alpha*X*transpose(Y)
Rank1Update(alpha, X, Y, A);
// we can also compute A = A + alpha*X*conjugate(transpose(Y))
Rank1Update(alpha, X, SeldonConj, Y, A);

// for hermitian matrices, use of X only
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; B(10, 10);
// we compute B = B + alpha*X*conjugate(transpose(X))
Rank1Update(alpha, X, B);

// for real numbers, use of symmetric matrices
Vector&lt;double&gt; Xr(10);
Xr.FillRand(); 
Matrix&lt;double, Symmetric, RowSymPacked&gt; Br(10, 10);
Br.Zero();
double beta = 2;
// computation of Br = Br + beta*Xr*tranpose(Xr)
Rank1Update(beta, Xr, Br);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_blas__2_8cxx_source.php">Blas_2.cxx</a></p>
<div class="separator"><a class="anchor" id="rank2update"></a></div><h3>Rank2Update</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Rank2Update(T, const Vector&amp;, const Vector&amp;, Matrix&amp;);
</pre><p>This function adds a contribution of rank 2 to a symmetric or Hermitian matrix.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// two complex vectors
Vector&lt;complex&lt;double&gt; &gt; X(10), Y(10);
X.FillRand(); 
Y.FillRand(); 

// hermitian matrix
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; A(10, 10);
A.SetIdentity();
complex&lt;double&gt; alpha(1, 1);

// we compute A = A + alpha * (X * conjugate(transpose(Y))
//                + Y * conjugate(transpose(X)))
Rank2Update(alpha, X, Y, A);

// for real numbers, use of symmetric matrices
Vector&lt;double&gt; Xr(10), Yr(10);
Xr.FillRand(); 
Yr.FillRand(); 
Matrix&lt;double, Symmetric, RowSymPacked&gt; Br(10, 10);
Br.Zero();
double beta = 2;
// computation of Br = Br + beta*(Xr*transpose(Yr) + Yr*transpose(Xr))
Rank2Update(beta, Xr, Yr, Br);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_blas__2_8cxx_source.php">Blas_2.cxx</a></p>
<div class="separator"><a class="anchor" id="solve"></a></div><h3>Solve</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void Solve(const Matrix&amp;, Vector&amp;);
  void Solve(SeldonTrans, SeldonNonUnit, const Matrix&amp;, Vector&amp;);
</pre><p>This function solves a triangular linear system.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// right hand side
Vector&lt;double&gt; B(3);
B.Fill(); 

// triangular matrix
Matrix&lt;double, General, RowUpTriangPacked&gt; T(3, 3);
T.Fill();

// now you can solve T*X = B, the result overwrites B
Solve(T, B);

// if you want to solve transpose(T)*X = B
Solve(SeldonTrans, SeldonNonUnit, T, B);

// SeldonUnit can be used if you are assuming that T has 
// a unitary diagonal (with 1)

// we force unitary diagonal
for (int i = 0; i &lt; T.GetM(); i++)
  T(i, i) = 1.0;

// then we can call Solve with Seldonunit to solve T*X = B
Solve(SeldonNoTrans, SeldonUnit, T, B);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_blas__2_8cxx_source.php">Blas_2.cxx</a> </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
