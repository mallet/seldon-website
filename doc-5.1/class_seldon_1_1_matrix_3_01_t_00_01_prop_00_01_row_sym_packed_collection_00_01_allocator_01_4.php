<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php">Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;" --><!-- doxytag: inherits="MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;" -->
<p>Symetric row-major matrix collection class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.png" usemap="#Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;_map" name="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix_collection.php" alt="Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;" shape="rect" coords="0,56,374,80"/>
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,374,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa88a18e034c43ecf58af89c12caf40ee"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::value_type" ref="aa88a18e034c43ecf58af89c12caf40ee" args="" -->
typedef T::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac10cd04b73114f5da0d646fa86c06db9"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::pointer" ref="ac10cd04b73114f5da0d646fa86c06db9" args="" -->
typedef T::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2e95eec3bc45f7a986dab8ac4469e553"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_pointer" ref="a2e95eec3bc45f7a986dab8ac4469e553" args="" -->
typedef T::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae1bc5d1b104a0d53406ca57817e1700e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::reference" ref="ae1bc5d1b104a0d53406ca57817e1700e" args="" -->
typedef T::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aeb8eb42d6af02fe07be33ba18f864b54"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_reference" ref="aeb8eb42d6af02fe07be33ba18f864b54" args="" -->
typedef T::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a05bdff9b076483174950ec393abe4edb"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::matrix_type" ref="a05bdff9b076483174950ec393abe4edb" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a99f0d5fa0a85e9e3a0c2a399707e25ac"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::matrix_pointer" ref="a99f0d5fa0a85e9e3a0c2a399707e25ac" args="" -->
typedef matrix_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9b8813cb57ad9d0b57f44bd7d50f1dc2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_matrix_pointer" ref="a9b8813cb57ad9d0b57f44bd7d50f1dc2" args="" -->
typedef const matrix_type *&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_matrix_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a08f2deac4107cd2143f6b226f2f636cc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::matrix_reference" ref="a08f2deac4107cd2143f6b226f2f636cc" args="" -->
typedef matrix_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>matrix_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0d8aa06b182fdb00cd02662b6c796c9d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_matrix_reference" ref="a0d8aa06b182fdb00cd02662b6c796c9d" args="" -->
typedef const matrix_type &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_matrix_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa7b493846b0081099137acb2d51cd27d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::collection_type" ref="aa7b493846b0081099137acb2d51cd27d" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; matrix_type, <br class="typebreak"/>
Prop, <a class="el" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a>, <a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a><br class="typebreak"/>
&lt; T &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a663cb4cb4f207cc949b6ba754ec0ba21"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_collection_type" ref="a663cb4cb4f207cc949b6ba754ec0ba21" args="" -->
typedef const <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9bf98196aa77adf6ffc568b68b243637"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::collection_reference" ref="a9bf98196aa77adf6ffc568b68b243637" args="" -->
typedef <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>collection_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac8ea06c664aa78eafedf4ecc9d7a806c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::const_collection_reference" ref="ac8ea06c664aa78eafedf4ecc9d7a806c" args="" -->
typedef const <a class="el" href="class_seldon_1_1_matrix.php">collection_type</a> &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_collection_reference</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#aafc716f71526b10a5221833661109bf8">Matrix</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#aafc716f71526b10a5221833661109bf8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_collection_00_01_allocator_01_4.php#ab9e699fa7ccc95baf087b92bd4834465">Matrix</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#ab9e699fa7ccc95baf087b92bd4834465"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a508ae5c18005383193f2a289bf63d608"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Clear" ref="a508ae5c18005383193f2a289bf63d608" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Clear</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a04f079fbfc7851ed7c1fa48a12936b9f"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Nullify" ref="a04f079fbfc7851ed7c1fa48a12936b9f" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a6b14667b3459659959f968847038524d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Nullify" ref="a6b14667b3459659959f968847038524d" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Nullify</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad1f04e7bbca49f4b4ac310f5a914085e"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Deallocate" ref="ad1f04e7bbca49f4b4ac310f5a914085e" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Deallocate</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">GetM</a> () const</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#ae7cfeea59b5f9181e6148e084a6efcf7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab6cdf0d339a99d8ab7baa9e788c1d5b2"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetM" ref="ab6cdf0d339a99d8ab7baa9e788c1d5b2" args="(int i) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetM</b> (int i) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaa428778f995ce6bbe08f1181fbb51e4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetMmatrix" ref="aaa428778f995ce6bbe08f1181fbb51e4" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMmatrix</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">GetN</a> () const</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a498904993d7b65f5c522c21ca991475d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af2f613864c55870df9232689b81896c4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetN" ref="af2f613864c55870df9232689b81896c4" args="(int j) const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetN</b> (int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a1165a73495d9eb4edfd49ba8e1f9b7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetNmatrix" ref="a4a1165a73495d9eb4edfd49ba8e1f9b7" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetNmatrix</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">GetSize</a> () const</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a897f68c33d448f7961680307ae4d75c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1162607a4f1255a388598a6dc5eea1f8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetDataSize" ref="a1162607a4f1255a388598a6dc5eea1f8" args="() const" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetDataSize</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2cafbea09c87f1798305211e30512994"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Reallocate" ref="a2cafbea09c87f1798305211e30512994" args="(int i, int j)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Reallocate</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a42828d0fb580934560080b2cf15eaa78"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::SetMatrix" ref="a42828d0fb580934560080b2cf15eaa78" args="(int m, int n, const Matrix&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, Storage0, Allocator0 &gt; &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4960e099b8a36df7b772a6a9a7261cda"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::SetMatrix" ref="a4960e099b8a36df7b772a6a9a7261cda" args="(int m, int n, const Matrix&lt; T0, Prop0, RowSparse, Allocator0 &gt; &amp;)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetMatrix</b> (int m, int n, const <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; T0, Prop0, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator0 &gt; &amp;)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad18c52c1ed65330e6443e4519df390a4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetMatrix" ref="ad18c52c1ed65330e6443e4519df390a4" args="(int i, int j)" -->
matrix_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int i, int j)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad4fcca36d06203d8324a1fed15b12a65"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetMatrix" ref="ad4fcca36d06203d8324a1fed15b12a65" args="(int i, int j) const" -->
const_matrix_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>GetMatrix</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a774399178dd07c6e598e6051e8f89009"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::operator()" ref="a774399178dd07c6e598e6051e8f89009" args="(int i, int j) const" -->
value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>operator()</b> (int i, int j) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0654c385392b3f0634a8721a797e249a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Copy" ref="a0654c385392b3f0634a8721a797e249a" args="(const MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt; &amp;A)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Copy</b> (const <a class="el" href="class_seldon_1_1_matrix_collection.php">MatrixCollection</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a>, Allocator &gt; &amp;A)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af85cc972c66fe36630031f480dea28ca"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Print" ref="af85cc972c66fe36630031f480dea28ca" args="() const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> () const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aaf59bd39550a1d4b460bc53e16362890"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Print" ref="aaf59bd39550a1d4b460bc53e16362890" args="(int m, int n) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Print</b> (int m, int n) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab3016490b7df18d3017477352f6b60d6"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Write" ref="ab3016490b7df18d3017477352f6b60d6" args="(string FileName, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (string FileName, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66a1f1f41da42db350225a63f29000f5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Write" ref="a66a1f1f41da42db350225a63f29000f5" args="(ostream &amp;FileStream, bool with_size) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Write</b> (ostream &amp;FileStream, bool with_size) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5c8fd96466c6409871d381feece50e44"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::WriteText" ref="a5c8fd96466c6409871d381feece50e44" args="(string FileName) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (string FileName) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad2d59d8ec2b0aca826f453d8ce646963"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::WriteText" ref="ad2d59d8ec2b0aca826f453d8ce646963" args="(ostream &amp;FileStream) const" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>WriteText</b> (ostream &amp;FileStream) const</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af54798b7169f468681f24203b6688aba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Read" ref="af54798b7169f468681f24203b6688aba" args="(string FileName)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (string FileName)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7df567116f04beb60b90d5397ea16e8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Read" ref="ae7df567116f04beb60b90d5397ea16e8" args="(istream &amp;FileStream)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>Read</b> (istream &amp;FileStream)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a960417c35add0c178e31e8578bd5eb9b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::nz_" ref="a960417c35add0c178e31e8578bd5eb9b" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b">nz_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of non-zero elements. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a094131b6edf39309cf51ec6aea9c271b"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Mmatrix_" ref="a094131b6edf39309cf51ec6aea9c271b" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b">Mmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="abf9ee2f169c7992c2d540d07dd85b104"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Nmatrix_" ref="abf9ee2f169c7992c2d540d07dd85b104" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104">Nmatrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns of matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a234a716260de98d93fc40fc2a3fd44fe"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Mlocal_" ref="a234a716260de98d93fc40fc2a3fd44fe" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe">Mlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa16e80513c77e58a2c1b3974e360f220"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Mlocal_sum_" ref="aa16e80513c77e58a2c1b3974e360f220" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220">Mlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of rows in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="afdcab0ec07a154e7ed23ee20b2f4f574"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Nlocal_" ref="afdcab0ec07a154e7ed23ee20b2f4f574" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574">Nlocal_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2cd7c7804f76b6e8a6966e6597e05244"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Nlocal_sum_" ref="a2cd7c7804f76b6e8a6966e6597e05244" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; int, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_calloc_alloc.php">CallocAlloc</a>&lt; int &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244">Nlocal_sum_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Cumulative number of columns in the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66297d4139bb2b5b07979ef929358399"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::matrix_" ref="a66297d4139bb2b5b07979ef929358399" args="" -->
<a class="el" href="class_seldon_1_1_matrix.php">collection_type</a>&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399">matrix_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Pointers of the underlying matrices. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Allocator&gt;<br/>
 class Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;</h3>

<p>Symetric row-major matrix collection class. </p>

<p>Definition at line <a class="el" href="_matrix_collection_8hxx_source.php#l00176">176</a> of file <a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="aafc716f71526b10a5221833661109bf8"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Matrix" ref="aafc716f71526b10a5221833661109bf8" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed_collection.php">RowSymPackedCollection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00959">959</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab9e699fa7ccc95baf087b92bd4834465"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::Matrix" ref="ab9e699fa7ccc95baf087b92bd4834465" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix.php">Seldon::Matrix</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed_collection.php">RowSymPackedCollection</a>, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j collection matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows of matrices. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns of matrices. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix_collection_8cxx_source.php#l00972">972</a> of file <a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00257">257</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00207">207</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00220">220</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00246">246</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00233">233</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ae7cfeea59b5f9181e6148e084a6efcf7"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetM" ref="ae7cfeea59b5f9181e6148e084a6efcf7" args="() const" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a> , Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of rows. It is the sum of the number of rows in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00129">129</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a498904993d7b65f5c522c21ca991475d"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetN" ref="a498904993d7b65f5c522c21ca991475d" args="() const" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a> , Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the total number of columns. It is the sum of the number of columns in the underlying matrices. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00144">144</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a897f68c33d448f7961680307ae4d75c5"></a><!-- doxytag: member="Seldon::Matrix&lt; T, Prop, RowSymPackedCollection, Allocator &gt;::GetSize" ref="a897f68c33d448f7961680307ae4d75c5" args="() const" -->
<div class="memitem">
<div class="memproto">
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix_collection.php">Seldon::MatrixCollection</a>&lt; T, Prop, <a class="el" href="class_seldon_1_1_row_sym_packed.php">RowSymPacked</a> , Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Reimplemented from <a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix_collection_8hxx_source.php">MatrixCollection.hxx</a></li>
<li>matrix/<a class="el" href="_matrix_collection_8cxx_source.php">MatrixCollection.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
