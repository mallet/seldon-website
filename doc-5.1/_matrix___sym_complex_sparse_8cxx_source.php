<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_SymComplexSparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SYMCOMPLEXSPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_SymComplexSparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8">00038</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00040"></a>00040 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">  ::Matrix_SymComplexSparse</a>(): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     real_nz_ = 0;
<a name="l00043"></a>00043     imag_nz_ = 0;
<a name="l00044"></a>00044     real_ptr_ = NULL;
<a name="l00045"></a>00045     imag_ptr_ = NULL;
<a name="l00046"></a>00046     real_ind_ = NULL;
<a name="l00047"></a>00047     imag_ind_ = NULL;
<a name="l00048"></a>00048     real_data_ = NULL;
<a name="l00049"></a>00049     imag_data_ = NULL;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00060"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ab055efcee796094368d74c476dd28221">00060</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00061"></a>00061   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00062"></a>00062 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">  ::Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064     real_nz_ = 0;
<a name="l00065"></a>00065     imag_nz_ = 0;
<a name="l00066"></a>00066     real_ptr_ = NULL;
<a name="l00067"></a>00067     imag_ptr_ = NULL;
<a name="l00068"></a>00068     real_ind_ = NULL;
<a name="l00069"></a>00069     imag_ind_ = NULL;
<a name="l00070"></a>00070     real_data_ = NULL;
<a name="l00071"></a>00071     imag_data_ = NULL;
<a name="l00072"></a>00072   }
<a name="l00073"></a>00073 
<a name="l00074"></a>00074 
<a name="l00076"></a>00076 
<a name="l00088"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a102b1c069e24ea2b907cefc593243ac4">00088</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00089"></a>00089   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00090"></a>00090 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">  ::Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz):
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00092"></a>00092   {
<a name="l00093"></a>00093     this-&gt;real_nz_ = real_nz;
<a name="l00094"></a>00094     this-&gt;imag_nz_ = imag_nz;
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00097"></a>00097 <span class="preprocessor"></span>    <span class="keywordflow">if</span> ( (static_cast&lt;long int&gt;(2 * real_nz_ - 2) / static_cast&lt;long int&gt;(i+1)
<a name="l00098"></a>00098           &gt;= static_cast&lt;long int&gt;(i)) ||
<a name="l00099"></a>00099          (static_cast&lt;long int&gt;(2 * imag_nz_ - 2) / static_cast&lt;long int&gt;(i+1)
<a name="l00100"></a>00100           &gt;= static_cast&lt;long int&gt;(i)) )
<a name="l00101"></a>00101       {
<a name="l00102"></a>00102         this-&gt;m_ = 0;
<a name="l00103"></a>00103         this-&gt;n_ = 0;
<a name="l00104"></a>00104         real_nz_ = 0;
<a name="l00105"></a>00105         imag_nz_ = 0;
<a name="l00106"></a>00106         real_ptr_ = NULL;
<a name="l00107"></a>00107         imag_ptr_ = NULL;
<a name="l00108"></a>00108         real_ind_ = NULL;
<a name="l00109"></a>00109         imag_ind_ = NULL;
<a name="l00110"></a>00110         this-&gt;real_data_ = NULL;
<a name="l00111"></a>00111         this-&gt;imag_data_ = NULL;
<a name="l00112"></a>00112         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00113"></a>00113                        + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00114"></a>00114                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values to be stored (&quot;</span>)
<a name="l00115"></a>00115                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz) + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00116"></a>00116                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary&quot;</span>)
<a name="l00117"></a>00117                        + <span class="stringliteral">&quot; part) than elements in the matrix (&quot;</span>
<a name="l00118"></a>00118                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00119"></a>00119       }
<a name="l00120"></a>00120 <span class="preprocessor">#endif</span>
<a name="l00121"></a>00121 <span class="preprocessor"></span>
<a name="l00122"></a>00122 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00123"></a>00123 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00124"></a>00124       {
<a name="l00125"></a>00125 <span class="preprocessor">#endif</span>
<a name="l00126"></a>00126 <span class="preprocessor"></span>
<a name="l00127"></a>00127         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>      }
<a name="l00131"></a>00131     <span class="keywordflow">catch</span> (...)
<a name="l00132"></a>00132       {
<a name="l00133"></a>00133         this-&gt;m_ = 0;
<a name="l00134"></a>00134         this-&gt;n_ = 0;
<a name="l00135"></a>00135         real_nz_ = 0;
<a name="l00136"></a>00136         imag_nz_ = 0;
<a name="l00137"></a>00137         real_ptr_ = NULL;
<a name="l00138"></a>00138         imag_ptr_ = NULL;
<a name="l00139"></a>00139         real_ind_ = NULL;
<a name="l00140"></a>00140         imag_ind_ = NULL;
<a name="l00141"></a>00141         this-&gt;real_data_ = NULL;
<a name="l00142"></a>00142         this-&gt;imag_data_ = NULL;
<a name="l00143"></a>00143       }
<a name="l00144"></a>00144     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l00145"></a>00145       {
<a name="l00146"></a>00146         this-&gt;m_ = 0;
<a name="l00147"></a>00147         this-&gt;n_ = 0;
<a name="l00148"></a>00148         real_nz_ = 0;
<a name="l00149"></a>00149         imag_nz_ = 0;
<a name="l00150"></a>00150         imag_ptr_ = 0;
<a name="l00151"></a>00151         real_ind_ = NULL;
<a name="l00152"></a>00152         imag_ind_ = NULL;
<a name="l00153"></a>00153         this-&gt;real_data_ = NULL;
<a name="l00154"></a>00154         this-&gt;imag_data_ = NULL;
<a name="l00155"></a>00155       }
<a name="l00156"></a>00156     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0)
<a name="l00157"></a>00157       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00158"></a>00158                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00159"></a>00159                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00160"></a>00160                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i + 1)) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00161"></a>00161                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i + 1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00162"></a>00162                      + <span class="stringliteral">&quot; start indices (for the real part), for a &quot;</span>
<a name="l00163"></a>00163                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00164"></a>00164 <span class="preprocessor">#endif</span>
<a name="l00165"></a>00165 <span class="preprocessor"></span>
<a name="l00166"></a>00166 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00167"></a>00167 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00168"></a>00168       {
<a name="l00169"></a>00169 <span class="preprocessor">#endif</span>
<a name="l00170"></a>00170 <span class="preprocessor"></span>
<a name="l00171"></a>00171         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00172"></a>00172 
<a name="l00173"></a>00173 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>      }
<a name="l00175"></a>00175     <span class="keywordflow">catch</span> (...)
<a name="l00176"></a>00176       {
<a name="l00177"></a>00177         this-&gt;m_ = 0;
<a name="l00178"></a>00178         this-&gt;n_ = 0;
<a name="l00179"></a>00179         real_nz_ = 0;
<a name="l00180"></a>00180         imag_nz_ = 0;
<a name="l00181"></a>00181         free(real_ptr_);
<a name="l00182"></a>00182         real_ptr_ = NULL;
<a name="l00183"></a>00183         imag_ptr_ = NULL;
<a name="l00184"></a>00184         real_ind_ = NULL;
<a name="l00185"></a>00185         imag_ind_ = NULL;
<a name="l00186"></a>00186         this-&gt;real_data_ = NULL;
<a name="l00187"></a>00187         this-&gt;imag_data_ = NULL;
<a name="l00188"></a>00188       }
<a name="l00189"></a>00189     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l00190"></a>00190       {
<a name="l00191"></a>00191         this-&gt;m_ = 0;
<a name="l00192"></a>00192         this-&gt;n_ = 0;
<a name="l00193"></a>00193         real_nz_ = 0;
<a name="l00194"></a>00194         imag_nz_ = 0;
<a name="l00195"></a>00195         free(real_ptr_);
<a name="l00196"></a>00196         real_ptr_ = 0;
<a name="l00197"></a>00197         real_ind_ = NULL;
<a name="l00198"></a>00198         imag_ind_ = NULL;
<a name="l00199"></a>00199         this-&gt;real_data_ = NULL;
<a name="l00200"></a>00200         this-&gt;imag_data_ = NULL;
<a name="l00201"></a>00201       }
<a name="l00202"></a>00202     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0)
<a name="l00203"></a>00203       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00204"></a>00204                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00205"></a>00205                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00206"></a>00206                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i + 1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00207"></a>00207                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i + 1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00208"></a>00208                      + <span class="stringliteral">&quot; start indices (for the imaginary part), for a &quot;</span>
<a name="l00209"></a>00209                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00210"></a>00210 <span class="preprocessor">#endif</span>
<a name="l00211"></a>00211 <span class="preprocessor"></span>
<a name="l00212"></a>00212 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00213"></a>00213 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00214"></a>00214       {
<a name="l00215"></a>00215 <span class="preprocessor">#endif</span>
<a name="l00216"></a>00216 <span class="preprocessor"></span>
<a name="l00217"></a>00217         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00218"></a>00218 
<a name="l00219"></a>00219 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00220"></a>00220 <span class="preprocessor"></span>      }
<a name="l00221"></a>00221     <span class="keywordflow">catch</span> (...)
<a name="l00222"></a>00222       {
<a name="l00223"></a>00223         this-&gt;m_ = 0;
<a name="l00224"></a>00224         this-&gt;n_ = 0;
<a name="l00225"></a>00225         real_nz_ = 0;
<a name="l00226"></a>00226         imag_nz_ = 0;
<a name="l00227"></a>00227         free(real_ptr_);
<a name="l00228"></a>00228         free(imag_ptr_);
<a name="l00229"></a>00229         real_ptr_ = NULL;
<a name="l00230"></a>00230         imag_ptr_ = NULL;
<a name="l00231"></a>00231         real_ind_ = NULL;
<a name="l00232"></a>00232         imag_ind_ = NULL;
<a name="l00233"></a>00233         this-&gt;real_data_ = NULL;
<a name="l00234"></a>00234         this-&gt;imag_data_ = NULL;
<a name="l00235"></a>00235       }
<a name="l00236"></a>00236     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l00237"></a>00237       {
<a name="l00238"></a>00238         this-&gt;m_ = 0;
<a name="l00239"></a>00239         this-&gt;n_ = 0;
<a name="l00240"></a>00240         real_nz_ = 0;
<a name="l00241"></a>00241         imag_nz_ = 0;
<a name="l00242"></a>00242         free(real_ptr_);
<a name="l00243"></a>00243         free(imag_ptr_);
<a name="l00244"></a>00244         real_ptr_ = NULL;
<a name="l00245"></a>00245         imag_ptr_ = NULL;
<a name="l00246"></a>00246         this-&gt;real_data_ = NULL;
<a name="l00247"></a>00247         this-&gt;imag_data_ = NULL;
<a name="l00248"></a>00248       }
<a name="l00249"></a>00249     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0)
<a name="l00250"></a>00250       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00251"></a>00251                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00252"></a>00252                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00253"></a>00253                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00254"></a>00254                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz)
<a name="l00255"></a>00255                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l00256"></a>00256                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00257"></a>00257 <span class="preprocessor">#endif</span>
<a name="l00258"></a>00258 <span class="preprocessor"></span>
<a name="l00259"></a>00259 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00260"></a>00260 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00261"></a>00261       {
<a name="l00262"></a>00262 <span class="preprocessor">#endif</span>
<a name="l00263"></a>00263 <span class="preprocessor"></span>
<a name="l00264"></a>00264         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00265"></a>00265 
<a name="l00266"></a>00266 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00267"></a>00267 <span class="preprocessor"></span>      }
<a name="l00268"></a>00268     <span class="keywordflow">catch</span> (...)
<a name="l00269"></a>00269       {
<a name="l00270"></a>00270         this-&gt;m_ = 0;
<a name="l00271"></a>00271         this-&gt;n_ = 0;
<a name="l00272"></a>00272         real_nz_ = 0;
<a name="l00273"></a>00273         imag_nz_ = 0;
<a name="l00274"></a>00274         free(real_ptr_);
<a name="l00275"></a>00275         free(imag_ptr_);
<a name="l00276"></a>00276         real_ptr_ = NULL;
<a name="l00277"></a>00277         imag_ptr_ = NULL;
<a name="l00278"></a>00278         free(imag_ind_);
<a name="l00279"></a>00279         real_ind_ = NULL;
<a name="l00280"></a>00280         imag_ind_ = NULL;
<a name="l00281"></a>00281         this-&gt;real_data_ = NULL;
<a name="l00282"></a>00282         this-&gt;imag_data_ = NULL;
<a name="l00283"></a>00283       }
<a name="l00284"></a>00284     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l00285"></a>00285       {
<a name="l00286"></a>00286         this-&gt;m_ = 0;
<a name="l00287"></a>00287         this-&gt;n_ = 0;
<a name="l00288"></a>00288         real_nz_ = 0;
<a name="l00289"></a>00289         imag_nz_ = 0;
<a name="l00290"></a>00290         free(real_ptr_);
<a name="l00291"></a>00291         free(imag_ptr_);
<a name="l00292"></a>00292         real_ptr_ = NULL;
<a name="l00293"></a>00293         imag_ptr_ = NULL;
<a name="l00294"></a>00294         free(imag_ind_);
<a name="l00295"></a>00295         imag_ind_ = NULL;
<a name="l00296"></a>00296         this-&gt;real_data_ = NULL;
<a name="l00297"></a>00297         this-&gt;imag_data_ = NULL;
<a name="l00298"></a>00298       }
<a name="l00299"></a>00299     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0)
<a name="l00300"></a>00300       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00301"></a>00301                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00302"></a>00302                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00303"></a>00303                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00304"></a>00304                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz)
<a name="l00305"></a>00305                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l00306"></a>00306                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00307"></a>00307 <span class="preprocessor">#endif</span>
<a name="l00308"></a>00308 <span class="preprocessor"></span>
<a name="l00309"></a>00309 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00310"></a>00310 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00311"></a>00311       {
<a name="l00312"></a>00312 <span class="preprocessor">#endif</span>
<a name="l00313"></a>00313 <span class="preprocessor"></span>
<a name="l00314"></a>00314         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00317"></a>00317 <span class="preprocessor"></span>      }
<a name="l00318"></a>00318     <span class="keywordflow">catch</span> (...)
<a name="l00319"></a>00319       {
<a name="l00320"></a>00320         this-&gt;m_ = 0;
<a name="l00321"></a>00321         this-&gt;n_ = 0;
<a name="l00322"></a>00322         free(real_ptr_);
<a name="l00323"></a>00323         free(imag_ptr_);
<a name="l00324"></a>00324         real_ptr_ = NULL;
<a name="l00325"></a>00325         imag_ptr_ = NULL;
<a name="l00326"></a>00326         free(real_ind_);
<a name="l00327"></a>00327         free(imag_ind_);
<a name="l00328"></a>00328         real_ind_ = NULL;
<a name="l00329"></a>00329         imag_ind_ = NULL;
<a name="l00330"></a>00330         this-&gt;real_data_ = NULL;
<a name="l00331"></a>00331         this-&gt;imag_data_ = NULL;
<a name="l00332"></a>00332       }
<a name="l00333"></a>00333     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l00334"></a>00334       {
<a name="l00335"></a>00335         this-&gt;m_ = 0;
<a name="l00336"></a>00336         this-&gt;n_ = 0;
<a name="l00337"></a>00337         free(real_ptr_);
<a name="l00338"></a>00338         free(imag_ptr_);
<a name="l00339"></a>00339         real_ptr_ = NULL;
<a name="l00340"></a>00340         imag_ptr_ = NULL;
<a name="l00341"></a>00341         free(real_ind_);
<a name="l00342"></a>00342         free(imag_ind_);
<a name="l00343"></a>00343         real_ind_ = NULL;
<a name="l00344"></a>00344         imag_ind_ = NULL;
<a name="l00345"></a>00345         imag_data_ = NULL;
<a name="l00346"></a>00346       }
<a name="l00347"></a>00347     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0)
<a name="l00348"></a>00348       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00349"></a>00349                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00350"></a>00350                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00351"></a>00351                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00352"></a>00352                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz) + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l00353"></a>00353                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00354"></a>00354 <span class="preprocessor">#endif</span>
<a name="l00355"></a>00355 <span class="preprocessor"></span>
<a name="l00356"></a>00356 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00357"></a>00357 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00358"></a>00358       {
<a name="l00359"></a>00359 <span class="preprocessor">#endif</span>
<a name="l00360"></a>00360 <span class="preprocessor"></span>
<a name="l00361"></a>00361         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l00362"></a>00362 
<a name="l00363"></a>00363 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00364"></a>00364 <span class="preprocessor"></span>      }
<a name="l00365"></a>00365     <span class="keywordflow">catch</span> (...)
<a name="l00366"></a>00366       {
<a name="l00367"></a>00367         this-&gt;m_ = 0;
<a name="l00368"></a>00368         this-&gt;n_ = 0;
<a name="l00369"></a>00369         free(real_ptr_);
<a name="l00370"></a>00370         free(imag_ptr_);
<a name="l00371"></a>00371         real_ptr_ = NULL;
<a name="l00372"></a>00372         imag_ptr_ = NULL;
<a name="l00373"></a>00373         free(real_ind_);
<a name="l00374"></a>00374         free(imag_ind_);
<a name="l00375"></a>00375         real_ind_ = NULL;
<a name="l00376"></a>00376         imag_ind_ = NULL;
<a name="l00377"></a>00377         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00378"></a>00378         this-&gt;real_data_ = NULL;
<a name="l00379"></a>00379         this-&gt;imag_data_ = NULL;
<a name="l00380"></a>00380       }
<a name="l00381"></a>00381     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l00382"></a>00382       {
<a name="l00383"></a>00383         this-&gt;m_ = 0;
<a name="l00384"></a>00384         this-&gt;n_ = 0;
<a name="l00385"></a>00385         free(real_ptr_);
<a name="l00386"></a>00386         free(imag_ptr_);
<a name="l00387"></a>00387         real_ptr_ = NULL;
<a name="l00388"></a>00388         imag_ptr_ = NULL;
<a name="l00389"></a>00389         free(real_ind_);
<a name="l00390"></a>00390         free(imag_ind_);
<a name="l00391"></a>00391         real_ind_ = NULL;
<a name="l00392"></a>00392         imag_ind_ = NULL;
<a name="l00393"></a>00393         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00394"></a>00394         real_data_ = NULL;
<a name="l00395"></a>00395       }
<a name="l00396"></a>00396     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0)
<a name="l00397"></a>00397       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00398"></a>00398                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l00399"></a>00399                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00400"></a>00400                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l00401"></a>00401                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz) + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l00402"></a>00402                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00403"></a>00403 <span class="preprocessor">#endif</span>
<a name="l00404"></a>00404 <span class="preprocessor"></span>
<a name="l00405"></a>00405   }
<a name="l00406"></a>00406 
<a name="l00407"></a>00407 
<a name="l00409"></a>00409 
<a name="l00428"></a>00428   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00429"></a>00429   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00430"></a>00430             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00431"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a063b7c64394f13e35d04d87310cf552e">00431</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00432"></a>00432   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00433"></a>00433 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">  Matrix_SymComplexSparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00434"></a>00434                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00435"></a>00435                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00436"></a>00436                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00437"></a>00437                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00438"></a>00438                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00439"></a>00439                           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l00440"></a>00440     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00441"></a>00441   {
<a name="l00442"></a>00442     real_nz_ = real_values.GetLength();
<a name="l00443"></a>00443     imag_nz_ = imag_values.GetLength();
<a name="l00444"></a>00444 
<a name="l00445"></a>00445 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00446"></a>00446 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00447"></a>00447 
<a name="l00448"></a>00448     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00449"></a>00449       {
<a name="l00450"></a>00450         this-&gt;m_ = 0;
<a name="l00451"></a>00451         this-&gt;n_ = 0;
<a name="l00452"></a>00452         real_nz_ = 0;
<a name="l00453"></a>00453         imag_nz_ = 0;
<a name="l00454"></a>00454         real_ptr_ = NULL;
<a name="l00455"></a>00455         imag_ptr_ = NULL;
<a name="l00456"></a>00456         real_ind_ = NULL;
<a name="l00457"></a>00457         imag_ind_ = NULL;
<a name="l00458"></a>00458         this-&gt;real_data_ = NULL;
<a name="l00459"></a>00459         this-&gt;imag_data_ = NULL;
<a name="l00460"></a>00460         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00461"></a>00461                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, &quot;</span>)
<a name="l00462"></a>00462                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00463"></a>00463                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00464"></a>00464                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00465"></a>00465                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00466"></a>00466                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00467"></a>00467                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00468"></a>00468       }
<a name="l00469"></a>00469 
<a name="l00470"></a>00470     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00471"></a>00471       {
<a name="l00472"></a>00472         this-&gt;m_ = 0;
<a name="l00473"></a>00473         this-&gt;n_ = 0;
<a name="l00474"></a>00474         real_nz_ = 0;
<a name="l00475"></a>00475         imag_nz_ = 0;
<a name="l00476"></a>00476         real_ptr_ = NULL;
<a name="l00477"></a>00477         imag_ptr_ = NULL;
<a name="l00478"></a>00478         real_ind_ = NULL;
<a name="l00479"></a>00479         imag_ind_ = NULL;
<a name="l00480"></a>00480         this-&gt;real_data_ = NULL;
<a name="l00481"></a>00481         this-&gt;imag_data_ = NULL;
<a name="l00482"></a>00482         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00483"></a>00483                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, &quot;</span>)
<a name="l00484"></a>00484                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00485"></a>00485                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00486"></a>00486                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00487"></a>00487                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00488"></a>00488                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00489"></a>00489                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00490"></a>00490       }
<a name="l00491"></a>00491 
<a name="l00492"></a>00492     <span class="keywordflow">if</span> (real_ptr.GetLength() - 1 != i)
<a name="l00493"></a>00493       {
<a name="l00494"></a>00494         this-&gt;m_ = 0;
<a name="l00495"></a>00495         this-&gt;n_ = 0;
<a name="l00496"></a>00496         real_nz_ = 0;
<a name="l00497"></a>00497         imag_nz_ = 0;
<a name="l00498"></a>00498         real_ptr_ = NULL;
<a name="l00499"></a>00499         imag_ptr_ = NULL;
<a name="l00500"></a>00500         real_ind_ = NULL;
<a name="l00501"></a>00501         imag_ind_ = NULL;
<a name="l00502"></a>00502         this-&gt;real_data_ = NULL;
<a name="l00503"></a>00503         this-&gt;imag_data_ = NULL;
<a name="l00504"></a>00504         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00505"></a>00505                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, &quot;</span>)
<a name="l00506"></a>00506                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00507"></a>00507                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00508"></a>00508                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00509"></a>00509                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength()-1)
<a name="l00510"></a>00510                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00511"></a>00511                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00512"></a>00512                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00513"></a>00513                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00514"></a>00514       }
<a name="l00515"></a>00515 
<a name="l00516"></a>00516     <span class="keywordflow">if</span> (imag_ptr.GetLength() - 1 != i)
<a name="l00517"></a>00517       {
<a name="l00518"></a>00518         this-&gt;m_ = 0;
<a name="l00519"></a>00519         this-&gt;n_ = 0;
<a name="l00520"></a>00520         real_nz_ = 0;
<a name="l00521"></a>00521         imag_nz_ = 0;
<a name="l00522"></a>00522         real_ptr_ = NULL;
<a name="l00523"></a>00523         imag_ptr_ = NULL;
<a name="l00524"></a>00524         real_ind_ = NULL;
<a name="l00525"></a>00525         imag_ind_ = NULL;
<a name="l00526"></a>00526         this-&gt;real_data_ = NULL;
<a name="l00527"></a>00527         this-&gt;imag_data_ = NULL;
<a name="l00528"></a>00528         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00529"></a>00529                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, &quot;</span>)
<a name="l00530"></a>00530                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00531"></a>00531                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00532"></a>00532                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00533"></a>00533                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00534"></a>00534                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00535"></a>00535                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00536"></a>00536                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00537"></a>00537                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00538"></a>00538       }
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     <span class="keywordflow">if</span> ( (static_cast&lt;long int&gt;(2 * real_nz_ - 2)
<a name="l00541"></a>00541           / static_cast&lt;long int&gt;(i + 1)
<a name="l00542"></a>00542           &gt;= static_cast&lt;long int&gt;(i)) ||
<a name="l00543"></a>00543          (<span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(2 * imag_nz_ - 2)
<a name="l00544"></a>00544           / static_cast&lt;long int&gt;(i + 1)
<a name="l00545"></a>00545           &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i)) )
<a name="l00546"></a>00546       {
<a name="l00547"></a>00547         this-&gt;m_ = 0;
<a name="l00548"></a>00548         this-&gt;n_ = 0;
<a name="l00549"></a>00549         real_nz_ = 0;
<a name="l00550"></a>00550         imag_nz_ = 0;
<a name="l00551"></a>00551         real_ptr_ = NULL;
<a name="l00552"></a>00552         imag_ptr_ = NULL;
<a name="l00553"></a>00553         real_ind_ = NULL;
<a name="l00554"></a>00554         imag_ind_ = NULL;
<a name="l00555"></a>00555         this-&gt;real_data_ = NULL;
<a name="l00556"></a>00556         this-&gt;imag_data_ = NULL;
<a name="l00557"></a>00557         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l00558"></a>00558                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, &quot;</span>)
<a name="l00559"></a>00559                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00560"></a>00560                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00561"></a>00561                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00562"></a>00562                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00563"></a>00563                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00564"></a>00564                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="stringliteral">&quot; values for&quot;</span>
<a name="l00565"></a>00565                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; the imaginary part) than elements in the&quot;</span>)
<a name="l00566"></a>00566                        + <span class="stringliteral">&quot; matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00567"></a>00567       }
<a name="l00568"></a>00568 <span class="preprocessor">#endif</span>
<a name="l00569"></a>00569 <span class="preprocessor"></span>
<a name="l00570"></a>00570     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00571"></a>00571     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00572"></a>00572     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00573"></a>00573     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00574"></a>00574     this-&gt;real_data_ = real_values.GetData();
<a name="l00575"></a>00575     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00576"></a>00576 
<a name="l00577"></a>00577     real_ptr.Nullify();
<a name="l00578"></a>00578     imag_ptr.Nullify();
<a name="l00579"></a>00579     real_ind.Nullify();
<a name="l00580"></a>00580     imag_ind.Nullify();
<a name="l00581"></a>00581     real_values.Nullify();
<a name="l00582"></a>00582     imag_values.Nullify();
<a name="l00583"></a>00583   }
<a name="l00584"></a>00584 
<a name="l00585"></a>00585 
<a name="l00587"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad373107c8496819b617d13f8d8336c73">00587</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00588"></a>00588   <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00589"></a>00589 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af4fd673569b7398d84f2d4f277de50a8" title="Default constructor.">  ::Matrix_SymComplexSparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop,
<a name="l00590"></a>00590                             Storage, Allocator&gt;&amp; A)
<a name="l00591"></a>00591   {
<a name="l00592"></a>00592     this-&gt;m_ = 0;
<a name="l00593"></a>00593     this-&gt;n_ = 0;
<a name="l00594"></a>00594     real_nz_ = 0;
<a name="l00595"></a>00595     imag_nz_ = 0;
<a name="l00596"></a>00596     real_ptr_ = NULL;
<a name="l00597"></a>00597     imag_ptr_ = NULL;
<a name="l00598"></a>00598     real_ind_ = NULL;
<a name="l00599"></a>00599     imag_ind_ = NULL;
<a name="l00600"></a>00600     real_data_ = NULL;
<a name="l00601"></a>00601     imag_data_ = NULL;
<a name="l00602"></a>00602 
<a name="l00603"></a>00603     this-&gt;Copy(A);
<a name="l00604"></a>00604   }
<a name="l00605"></a>00605 
<a name="l00606"></a>00606 
<a name="l00607"></a>00607   <span class="comment">/**************</span>
<a name="l00608"></a>00608 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00609"></a>00609 <span class="comment">   **************/</span>
<a name="l00610"></a>00610 
<a name="l00611"></a>00611 
<a name="l00613"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa">00613</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00614"></a>00614   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa" title="Destructor.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00615"></a>00615 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa" title="Destructor.">  ::~Matrix_SymComplexSparse</a>()
<a name="l00616"></a>00616   {
<a name="l00617"></a>00617     this-&gt;m_ = 0;
<a name="l00618"></a>00618     this-&gt;n_ = 0;
<a name="l00619"></a>00619 
<a name="l00620"></a>00620 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00621"></a>00621 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00622"></a>00622       {
<a name="l00623"></a>00623 <span class="preprocessor">#endif</span>
<a name="l00624"></a>00624 <span class="preprocessor"></span>
<a name="l00625"></a>00625         <span class="keywordflow">if</span> (real_ptr_ != NULL)
<a name="l00626"></a>00626           {
<a name="l00627"></a>00627             free(real_ptr_);
<a name="l00628"></a>00628             real_ptr_ = NULL;
<a name="l00629"></a>00629           }
<a name="l00630"></a>00630 
<a name="l00631"></a>00631 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00632"></a>00632 <span class="preprocessor"></span>      }
<a name="l00633"></a>00633     <span class="keywordflow">catch</span> (...)
<a name="l00634"></a>00634       {
<a name="l00635"></a>00635         real_ptr_ = NULL;
<a name="l00636"></a>00636       }
<a name="l00637"></a>00637 <span class="preprocessor">#endif</span>
<a name="l00638"></a>00638 <span class="preprocessor"></span>
<a name="l00639"></a>00639 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00640"></a>00640 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00641"></a>00641       {
<a name="l00642"></a>00642 <span class="preprocessor">#endif</span>
<a name="l00643"></a>00643 <span class="preprocessor"></span>
<a name="l00644"></a>00644         <span class="keywordflow">if</span> (imag_ptr_ != NULL)
<a name="l00645"></a>00645           {
<a name="l00646"></a>00646             free(imag_ptr_);
<a name="l00647"></a>00647             imag_ptr_ = NULL;
<a name="l00648"></a>00648           }
<a name="l00649"></a>00649 
<a name="l00650"></a>00650 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00651"></a>00651 <span class="preprocessor"></span>      }
<a name="l00652"></a>00652     <span class="keywordflow">catch</span> (...)
<a name="l00653"></a>00653       {
<a name="l00654"></a>00654         imag_ptr_ = NULL;
<a name="l00655"></a>00655       }
<a name="l00656"></a>00656 <span class="preprocessor">#endif</span>
<a name="l00657"></a>00657 <span class="preprocessor"></span>
<a name="l00658"></a>00658 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00659"></a>00659 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00660"></a>00660       {
<a name="l00661"></a>00661 <span class="preprocessor">#endif</span>
<a name="l00662"></a>00662 <span class="preprocessor"></span>
<a name="l00663"></a>00663         <span class="keywordflow">if</span> (real_ind_ != NULL)
<a name="l00664"></a>00664           {
<a name="l00665"></a>00665             free(real_ind_);
<a name="l00666"></a>00666             real_ind_ = NULL;
<a name="l00667"></a>00667           }
<a name="l00668"></a>00668 
<a name="l00669"></a>00669 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00670"></a>00670 <span class="preprocessor"></span>      }
<a name="l00671"></a>00671     <span class="keywordflow">catch</span> (...)
<a name="l00672"></a>00672       {
<a name="l00673"></a>00673         real_ind_ = NULL;
<a name="l00674"></a>00674       }
<a name="l00675"></a>00675 <span class="preprocessor">#endif</span>
<a name="l00676"></a>00676 <span class="preprocessor"></span>
<a name="l00677"></a>00677 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00678"></a>00678 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00679"></a>00679       {
<a name="l00680"></a>00680 <span class="preprocessor">#endif</span>
<a name="l00681"></a>00681 <span class="preprocessor"></span>
<a name="l00682"></a>00682         <span class="keywordflow">if</span> (imag_ind_ != NULL)
<a name="l00683"></a>00683           {
<a name="l00684"></a>00684             free(imag_ind_);
<a name="l00685"></a>00685             imag_ind_ = NULL;
<a name="l00686"></a>00686           }
<a name="l00687"></a>00687 
<a name="l00688"></a>00688 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00689"></a>00689 <span class="preprocessor"></span>      }
<a name="l00690"></a>00690     <span class="keywordflow">catch</span> (...)
<a name="l00691"></a>00691       {
<a name="l00692"></a>00692         imag_ind_ = NULL;
<a name="l00693"></a>00693       }
<a name="l00694"></a>00694 <span class="preprocessor">#endif</span>
<a name="l00695"></a>00695 <span class="preprocessor"></span>
<a name="l00696"></a>00696 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00697"></a>00697 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00698"></a>00698       {
<a name="l00699"></a>00699 <span class="preprocessor">#endif</span>
<a name="l00700"></a>00700 <span class="preprocessor"></span>
<a name="l00701"></a>00701         <span class="keywordflow">if</span> (this-&gt;real_data_ != NULL)
<a name="l00702"></a>00702           {
<a name="l00703"></a>00703             this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l00704"></a>00704             this-&gt;real_data_ = NULL;
<a name="l00705"></a>00705           }
<a name="l00706"></a>00706 
<a name="l00707"></a>00707 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00708"></a>00708 <span class="preprocessor"></span>      }
<a name="l00709"></a>00709     <span class="keywordflow">catch</span> (...)
<a name="l00710"></a>00710       {
<a name="l00711"></a>00711         this-&gt;real_nz_ = 0;
<a name="l00712"></a>00712         this-&gt;real_data_ = NULL;
<a name="l00713"></a>00713       }
<a name="l00714"></a>00714 <span class="preprocessor">#endif</span>
<a name="l00715"></a>00715 <span class="preprocessor"></span>
<a name="l00716"></a>00716 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00717"></a>00717 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00718"></a>00718       {
<a name="l00719"></a>00719 <span class="preprocessor">#endif</span>
<a name="l00720"></a>00720 <span class="preprocessor"></span>
<a name="l00721"></a>00721         <span class="keywordflow">if</span> (this-&gt;imag_data_ != NULL)
<a name="l00722"></a>00722           {
<a name="l00723"></a>00723             this-&gt;allocator_.deallocate(this-&gt;imag_data_, imag_nz_);
<a name="l00724"></a>00724             this-&gt;imag_data_ = NULL;
<a name="l00725"></a>00725           }
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00728"></a>00728 <span class="preprocessor"></span>      }
<a name="l00729"></a>00729     <span class="keywordflow">catch</span> (...)
<a name="l00730"></a>00730       {
<a name="l00731"></a>00731         this-&gt;imag_nz_ = 0;
<a name="l00732"></a>00732         this-&gt;imag_data_ = NULL;
<a name="l00733"></a>00733       }
<a name="l00734"></a>00734 <span class="preprocessor">#endif</span>
<a name="l00735"></a>00735 <span class="preprocessor"></span>
<a name="l00736"></a>00736     this-&gt;real_nz_ = 0;
<a name="l00737"></a>00737     this-&gt;imag_nz_ = 0;
<a name="l00738"></a>00738   }
<a name="l00739"></a>00739 
<a name="l00740"></a>00740 
<a name="l00742"></a>00742 
<a name="l00745"></a>00745   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00746"></a>00746   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732" title="Clears the matrix.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00747"></a>00747   {
<a name="l00748"></a>00748     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afcf6a5d81a59ad003dfa7f905e0b5aaa" title="Destructor.">~Matrix_SymComplexSparse</a>();
<a name="l00749"></a>00749   }
<a name="l00750"></a>00750 
<a name="l00751"></a>00751 
<a name="l00752"></a>00752   <span class="comment">/*********************</span>
<a name="l00753"></a>00753 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00754"></a>00754 <span class="comment">   *********************/</span>
<a name="l00755"></a>00755 
<a name="l00756"></a>00756 
<a name="l00758"></a>00758 
<a name="l00776"></a>00776   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00777"></a>00777   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00778"></a>00778             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00779"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b">00779</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00780"></a>00780   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00781"></a>00781 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00782"></a>00782           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l00783"></a>00783           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l00784"></a>00784           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l00785"></a>00785           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l00786"></a>00786           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l00787"></a>00787           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind)
<a name="l00788"></a>00788   {
<a name="l00789"></a>00789     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732" title="Clears the matrix.">Clear</a>();
<a name="l00790"></a>00790     this-&gt;m_ = i;
<a name="l00791"></a>00791     this-&gt;n_ = i;
<a name="l00792"></a>00792     real_nz_ = real_values.GetLength();
<a name="l00793"></a>00793     imag_nz_ = imag_values.GetLength();
<a name="l00794"></a>00794 
<a name="l00795"></a>00795 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00796"></a>00796 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00797"></a>00797 
<a name="l00798"></a>00798     <span class="keywordflow">if</span> (real_ind.GetLength() != real_nz_)
<a name="l00799"></a>00799       {
<a name="l00800"></a>00800         this-&gt;m_ = 0;
<a name="l00801"></a>00801         this-&gt;n_ = 0;
<a name="l00802"></a>00802         real_nz_ = 0;
<a name="l00803"></a>00803         imag_nz_ = 0;
<a name="l00804"></a>00804         real_ptr_ = NULL;
<a name="l00805"></a>00805         imag_ptr_ = NULL;
<a name="l00806"></a>00806         real_ind_ = NULL;
<a name="l00807"></a>00807         imag_ind_ = NULL;
<a name="l00808"></a>00808         this-&gt;real_data_ = NULL;
<a name="l00809"></a>00809         this-&gt;imag_data_ = NULL;
<a name="l00810"></a>00810         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00811"></a>00811                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00812"></a>00812                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00813"></a>00813                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l00814"></a>00814                        + <span class="stringliteral">&quot; values (real part) but &quot;</span>
<a name="l00815"></a>00815                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ind.GetLength())
<a name="l00816"></a>00816                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00817"></a>00817       }
<a name="l00818"></a>00818 
<a name="l00819"></a>00819     <span class="keywordflow">if</span> (imag_ind.GetLength() != imag_nz_)
<a name="l00820"></a>00820       {
<a name="l00821"></a>00821         this-&gt;m_ = 0;
<a name="l00822"></a>00822         this-&gt;n_ = 0;
<a name="l00823"></a>00823         real_nz_ = 0;
<a name="l00824"></a>00824         imag_nz_ = 0;
<a name="l00825"></a>00825         real_ptr_ = NULL;
<a name="l00826"></a>00826         imag_ptr_ = NULL;
<a name="l00827"></a>00827         real_ind_ = NULL;
<a name="l00828"></a>00828         imag_ind_ = NULL;
<a name="l00829"></a>00829         this-&gt;real_data_ = NULL;
<a name="l00830"></a>00830         this-&gt;imag_data_ = NULL;
<a name="l00831"></a>00831         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00832"></a>00832                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00833"></a>00833                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00834"></a>00834                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l00835"></a>00835                        + <span class="stringliteral">&quot; values (imaginary part) but &quot;</span>
<a name="l00836"></a>00836                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ind.GetLength())
<a name="l00837"></a>00837                        + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00838"></a>00838       }
<a name="l00839"></a>00839 
<a name="l00840"></a>00840     <span class="keywordflow">if</span> (real_ptr.GetLength() - 1 != i)
<a name="l00841"></a>00841       {
<a name="l00842"></a>00842         this-&gt;m_ = 0;
<a name="l00843"></a>00843         this-&gt;n_ = 0;
<a name="l00844"></a>00844         real_nz_ = 0;
<a name="l00845"></a>00845         imag_nz_ = 0;
<a name="l00846"></a>00846         real_ptr_ = NULL;
<a name="l00847"></a>00847         imag_ptr_ = NULL;
<a name="l00848"></a>00848         real_ind_ = NULL;
<a name="l00849"></a>00849         imag_ind_ = NULL;
<a name="l00850"></a>00850         this-&gt;real_data_ = NULL;
<a name="l00851"></a>00851         this-&gt;imag_data_ = NULL;
<a name="l00852"></a>00852         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00853"></a>00853                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00854"></a>00854                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00855"></a>00855                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (real part)&quot;</span>)
<a name="l00856"></a>00856                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_ptr.GetLength() - 1)
<a name="l00857"></a>00857                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00858"></a>00858                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00859"></a>00859                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00860"></a>00860                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00861"></a>00861       }
<a name="l00862"></a>00862 
<a name="l00863"></a>00863     <span class="keywordflow">if</span> (imag_ptr.GetLength() - 1 != i)
<a name="l00864"></a>00864       {
<a name="l00865"></a>00865         this-&gt;m_ = 0;
<a name="l00866"></a>00866         this-&gt;n_ = 0;
<a name="l00867"></a>00867         real_nz_ = 0;
<a name="l00868"></a>00868         imag_nz_ = 0;
<a name="l00869"></a>00869         real_ptr_ = NULL;
<a name="l00870"></a>00870         imag_ptr_ = NULL;
<a name="l00871"></a>00871         real_ind_ = NULL;
<a name="l00872"></a>00872         imag_ind_ = NULL;
<a name="l00873"></a>00873         this-&gt;real_data_ = NULL;
<a name="l00874"></a>00874         this-&gt;imag_data_ = NULL;
<a name="l00875"></a>00875         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00876"></a>00876                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00877"></a>00877                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00878"></a>00878                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices (imaginary part)&quot;</span>)
<a name="l00879"></a>00879                        + <span class="stringliteral">&quot; contains &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_ptr.GetLength()-1)
<a name="l00880"></a>00880                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column start indices (plus the&quot;</span>)
<a name="l00881"></a>00881                        + <span class="stringliteral">&quot; number of non-zero entries) but there are &quot;</span>
<a name="l00882"></a>00882                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; rows or columns (&quot;</span>
<a name="l00883"></a>00883                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00884"></a>00884       }
<a name="l00885"></a>00885 
<a name="l00886"></a>00886     <span class="keywordflow">if</span> ( (static_cast&lt;long int&gt;(2 * real_nz_ - 2) / static_cast&lt;long int&gt;(i)
<a name="l00887"></a>00887           &gt;= static_cast&lt;long int&gt;(i + 1)) ||
<a name="l00888"></a>00888          (<span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(2 * imag_nz_ - 2) / static_cast&lt;long int&gt;(i)
<a name="l00889"></a>00889           &gt;= <span class="keyword">static_cast&lt;</span><span class="keywordtype">long</span> <span class="keywordtype">int</span><span class="keyword">&gt;</span>(i + 1)) )
<a name="l00890"></a>00890       {
<a name="l00891"></a>00891         this-&gt;m_ = 0;
<a name="l00892"></a>00892         this-&gt;n_ = 0;
<a name="l00893"></a>00893         real_nz_ = 0;
<a name="l00894"></a>00894         imag_nz_ = 0;
<a name="l00895"></a>00895         real_ptr_ = NULL;
<a name="l00896"></a>00896         imag_ptr_ = NULL;
<a name="l00897"></a>00897         real_ind_ = NULL;
<a name="l00898"></a>00898         imag_ind_ = NULL;
<a name="l00899"></a>00899         this-&gt;real_data_ = NULL;
<a name="l00900"></a>00900         this-&gt;imag_data_ = NULL;
<a name="l00901"></a>00901         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::SetData(int, int, &quot;</span>)
<a name="l00902"></a>00902                        + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;&quot;</span>)
<a name="l00903"></a>00903                        + <span class="stringliteral">&quot;, const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00904"></a>00904                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00905"></a>00905                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength())
<a name="l00906"></a>00906                        + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l00907"></a>00907                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_values.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values&quot;</span>)
<a name="l00908"></a>00908                        + string(<span class="stringliteral">&quot; for the imaginary part) than elements in&quot;</span>)
<a name="l00909"></a>00909                        + <span class="stringliteral">&quot; the matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00910"></a>00910                        + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00911"></a>00911       }
<a name="l00912"></a>00912 <span class="preprocessor">#endif</span>
<a name="l00913"></a>00913 <span class="preprocessor"></span>
<a name="l00914"></a>00914     this-&gt;real_ptr_ = real_ptr.GetData();
<a name="l00915"></a>00915     this-&gt;imag_ptr_ = imag_ptr.GetData();
<a name="l00916"></a>00916     this-&gt;real_ind_ = real_ind.GetData();
<a name="l00917"></a>00917     this-&gt;imag_ind_ = imag_ind.GetData();
<a name="l00918"></a>00918     this-&gt;real_data_ = real_values.GetData();
<a name="l00919"></a>00919     this-&gt;imag_data_ = imag_values.GetData();
<a name="l00920"></a>00920 
<a name="l00921"></a>00921     real_ptr.Nullify();
<a name="l00922"></a>00922     imag_ptr.Nullify();
<a name="l00923"></a>00923     real_ind.Nullify();
<a name="l00924"></a>00924     imag_ind.Nullify();
<a name="l00925"></a>00925     real_values.Nullify();
<a name="l00926"></a>00926     imag_values.Nullify();
<a name="l00927"></a>00927   }
<a name="l00928"></a>00928 
<a name="l00929"></a>00929 
<a name="l00931"></a>00931 
<a name="l00953"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a288b0c3b764e1db7eff81cb5e56e7c63">00953</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00954"></a>00954   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00955"></a>00955 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3f202fe96b9289912e1cb6acfe24f18b" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz,
<a name="l00956"></a>00956           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00957"></a>00957           ::pointer real_values,
<a name="l00958"></a>00958           <span class="keywordtype">int</span>* real_ptr, <span class="keywordtype">int</span>* real_ind, <span class="keywordtype">int</span> imag_nz,
<a name="l00959"></a>00959           <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00960"></a>00960           ::pointer imag_values,
<a name="l00961"></a>00961           <span class="keywordtype">int</span>* imag_ptr, <span class="keywordtype">int</span>* imag_ind)
<a name="l00962"></a>00962   {
<a name="l00963"></a>00963     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732" title="Clears the matrix.">Clear</a>();
<a name="l00964"></a>00964 
<a name="l00965"></a>00965     this-&gt;m_ = i;
<a name="l00966"></a>00966     this-&gt;n_ = i;
<a name="l00967"></a>00967 
<a name="l00968"></a>00968     this-&gt;real_nz_ = real_nz;
<a name="l00969"></a>00969     this-&gt;imag_nz_ = imag_nz;
<a name="l00970"></a>00970 
<a name="l00971"></a>00971     real_data_ = real_values;
<a name="l00972"></a>00972     imag_data_ = imag_values;
<a name="l00973"></a>00973     real_ind_ = real_ind;
<a name="l00974"></a>00974     imag_ind_ = imag_ind;
<a name="l00975"></a>00975     real_ptr_ = real_ptr;
<a name="l00976"></a>00976     imag_ptr_ = imag_ptr;
<a name="l00977"></a>00977   }
<a name="l00978"></a>00978 
<a name="l00979"></a>00979 
<a name="l00981"></a>00981 
<a name="l00985"></a>00985   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00986"></a>00986   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af9adb37bee7561a855fef31642b85bfd" title="Clears the matrix without releasing memory.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00987"></a>00987   {
<a name="l00988"></a>00988     this-&gt;data_ = NULL;
<a name="l00989"></a>00989     this-&gt;m_ = 0;
<a name="l00990"></a>00990     this-&gt;n_ = 0;
<a name="l00991"></a>00991     real_nz_ = 0;
<a name="l00992"></a>00992     real_ptr_ = NULL;
<a name="l00993"></a>00993     real_ind_ = NULL;
<a name="l00994"></a>00994     imag_nz_ = 0;
<a name="l00995"></a>00995     imag_ptr_ = NULL;
<a name="l00996"></a>00996     imag_ind_ = NULL;
<a name="l00997"></a>00997     real_data_ = NULL;
<a name="l00998"></a>00998     imag_data_ = NULL;
<a name="l00999"></a>00999   }
<a name="l01000"></a>01000 
<a name="l01001"></a>01001 
<a name="l01003"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62">01003</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01004"></a>01004   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62" title="Copies a matrix.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01005"></a>01005 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a5e1062c93e237401d6bf6b1797861c62" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01006"></a>01006   {
<a name="l01007"></a>01007     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a77439fdc40537903681e7cd912c5f732" title="Clears the matrix.">Clear</a>();
<a name="l01008"></a>01008     <span class="keywordtype">int</span> i = A.m_;
<a name="l01009"></a>01009     <span class="keywordtype">int</span> j = A.n_;
<a name="l01010"></a>01010     real_nz_ = A.real_nz_;
<a name="l01011"></a>01011     imag_nz_ = A.imag_nz_;
<a name="l01012"></a>01012     this-&gt;m_ = i;
<a name="l01013"></a>01013     this-&gt;n_ = j;
<a name="l01014"></a>01014     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l01015"></a>01015       {
<a name="l01016"></a>01016         this-&gt;m_ = 0;
<a name="l01017"></a>01017         this-&gt;n_ = 0;
<a name="l01018"></a>01018         this-&gt;real_nz_ = 0;
<a name="l01019"></a>01019         this-&gt;imag_nz_ = 0;
<a name="l01020"></a>01020         <span class="keywordflow">return</span>;
<a name="l01021"></a>01021       }
<a name="l01022"></a>01022 
<a name="l01023"></a>01023 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01024"></a>01024 <span class="preprocessor"></span>    <span class="keywordflow">if</span> ( (static_cast&lt;long int&gt;(2 * real_nz_ - 2) / static_cast&lt;long int&gt;(i+1)
<a name="l01025"></a>01025           &gt;= static_cast&lt;long int&gt;(i)) ||
<a name="l01026"></a>01026          (static_cast&lt;long int&gt;(2 * imag_nz_ - 2) / static_cast&lt;long int&gt;(i+1)
<a name="l01027"></a>01027           &gt;= static_cast&lt;long int&gt;(i)) )
<a name="l01028"></a>01028       {
<a name="l01029"></a>01029         this-&gt;m_ = 0;
<a name="l01030"></a>01030         this-&gt;n_ = 0;
<a name="l01031"></a>01031         real_nz_ = 0;
<a name="l01032"></a>01032         imag_nz_ = 0;
<a name="l01033"></a>01033         real_ptr_ = NULL;
<a name="l01034"></a>01034         imag_ptr_ = NULL;
<a name="l01035"></a>01035         real_ind_ = NULL;
<a name="l01036"></a>01036         imag_ind_ = NULL;
<a name="l01037"></a>01037         this-&gt;real_data_ = NULL;
<a name="l01038"></a>01038         this-&gt;imag_data_ = NULL;
<a name="l01039"></a>01039         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01040"></a>01040                        + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01041"></a>01041                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values to be stored (&quot;</span>)
<a name="l01042"></a>01042                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_) + <span class="stringliteral">&quot; values for the real part and &quot;</span>
<a name="l01043"></a>01043                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; values for the imaginary&quot;</span>)
<a name="l01044"></a>01044                        + <span class="stringliteral">&quot; part) than elements in the matrix (&quot;</span>
<a name="l01045"></a>01045                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l01046"></a>01046       }
<a name="l01047"></a>01047 <span class="preprocessor">#endif</span>
<a name="l01048"></a>01048 <span class="preprocessor"></span>
<a name="l01049"></a>01049 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01050"></a>01050 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01051"></a>01051       {
<a name="l01052"></a>01052 <span class="preprocessor">#endif</span>
<a name="l01053"></a>01053 <span class="preprocessor"></span>
<a name="l01054"></a>01054         real_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01055"></a>01055         memcpy(this-&gt;real_ptr_, A.real_ptr_, (i + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01056"></a>01056 
<a name="l01057"></a>01057 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01058"></a>01058 <span class="preprocessor"></span>      }
<a name="l01059"></a>01059     <span class="keywordflow">catch</span> (...)
<a name="l01060"></a>01060       {
<a name="l01061"></a>01061         this-&gt;m_ = 0;
<a name="l01062"></a>01062         this-&gt;n_ = 0;
<a name="l01063"></a>01063         real_nz_ = 0;
<a name="l01064"></a>01064         imag_nz_ = 0;
<a name="l01065"></a>01065         real_ptr_ = NULL;
<a name="l01066"></a>01066         imag_ptr_ = NULL;
<a name="l01067"></a>01067         real_ind_ = NULL;
<a name="l01068"></a>01068         imag_ind_ = NULL;
<a name="l01069"></a>01069         this-&gt;real_data_ = NULL;
<a name="l01070"></a>01070         this-&gt;imag_data_ = NULL;
<a name="l01071"></a>01071       }
<a name="l01072"></a>01072     <span class="keywordflow">if</span> (real_ptr_ == NULL)
<a name="l01073"></a>01073       {
<a name="l01074"></a>01074         this-&gt;m_ = 0;
<a name="l01075"></a>01075         this-&gt;n_ = 0;
<a name="l01076"></a>01076         real_nz_ = 0;
<a name="l01077"></a>01077         imag_nz_ = 0;
<a name="l01078"></a>01078         imag_ptr_ = 0;
<a name="l01079"></a>01079         real_ind_ = NULL;
<a name="l01080"></a>01080         imag_ind_ = NULL;
<a name="l01081"></a>01081         this-&gt;real_data_ = NULL;
<a name="l01082"></a>01082         this-&gt;imag_data_ = NULL;
<a name="l01083"></a>01083       }
<a name="l01084"></a>01084     <span class="keywordflow">if</span> (real_ptr_ == NULL &amp;&amp; i != 0)
<a name="l01085"></a>01085       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01086"></a>01086                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01087"></a>01087                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01088"></a>01088                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i + 1)) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01089"></a>01089                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i + 1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l01090"></a>01090                      + <span class="stringliteral">&quot; start indices (for the real part), for a &quot;</span>
<a name="l01091"></a>01091                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01092"></a>01092 <span class="preprocessor">#endif</span>
<a name="l01093"></a>01093 <span class="preprocessor"></span>
<a name="l01094"></a>01094 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01095"></a>01095 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01096"></a>01096       {
<a name="l01097"></a>01097 <span class="preprocessor">#endif</span>
<a name="l01098"></a>01098 <span class="preprocessor"></span>
<a name="l01099"></a>01099         imag_ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(i + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01100"></a>01100         memcpy(this-&gt;imag_ptr_, A.imag_ptr_, (i + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01101"></a>01101 
<a name="l01102"></a>01102 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01103"></a>01103 <span class="preprocessor"></span>      }
<a name="l01104"></a>01104     <span class="keywordflow">catch</span> (...)
<a name="l01105"></a>01105       {
<a name="l01106"></a>01106         this-&gt;m_ = 0;
<a name="l01107"></a>01107         this-&gt;n_ = 0;
<a name="l01108"></a>01108         real_nz_ = 0;
<a name="l01109"></a>01109         imag_nz_ = 0;
<a name="l01110"></a>01110         free(real_ptr_);
<a name="l01111"></a>01111         real_ptr_ = NULL;
<a name="l01112"></a>01112         imag_ptr_ = NULL;
<a name="l01113"></a>01113         real_ind_ = NULL;
<a name="l01114"></a>01114         imag_ind_ = NULL;
<a name="l01115"></a>01115         this-&gt;real_data_ = NULL;
<a name="l01116"></a>01116         this-&gt;imag_data_ = NULL;
<a name="l01117"></a>01117       }
<a name="l01118"></a>01118     <span class="keywordflow">if</span> (imag_ptr_ == NULL)
<a name="l01119"></a>01119       {
<a name="l01120"></a>01120         this-&gt;m_ = 0;
<a name="l01121"></a>01121         this-&gt;n_ = 0;
<a name="l01122"></a>01122         real_nz_ = 0;
<a name="l01123"></a>01123         imag_nz_ = 0;
<a name="l01124"></a>01124         free(real_ptr_);
<a name="l01125"></a>01125         real_ptr_ = 0;
<a name="l01126"></a>01126         real_ind_ = NULL;
<a name="l01127"></a>01127         imag_ind_ = NULL;
<a name="l01128"></a>01128         this-&gt;real_data_ = NULL;
<a name="l01129"></a>01129         this-&gt;imag_data_ = NULL;
<a name="l01130"></a>01130       }
<a name="l01131"></a>01131     <span class="keywordflow">if</span> (imag_ptr_ == NULL &amp;&amp; i != 0)
<a name="l01132"></a>01132       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01133"></a>01133                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01134"></a>01134                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01135"></a>01135                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (i + 1) ) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01136"></a>01136                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i + 1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l01137"></a>01137                      + <span class="stringliteral">&quot; start indices (for the imaginary part), for a &quot;</span>
<a name="l01138"></a>01138                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01139"></a>01139 <span class="preprocessor">#endif</span>
<a name="l01140"></a>01140 <span class="preprocessor"></span>
<a name="l01141"></a>01141 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01142"></a>01142 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01143"></a>01143       {
<a name="l01144"></a>01144 <span class="preprocessor">#endif</span>
<a name="l01145"></a>01145 <span class="preprocessor"></span>
<a name="l01146"></a>01146         real_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(real_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01147"></a>01147         memcpy(this-&gt;real_ind_, A.real_ind_, real_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01148"></a>01148 
<a name="l01149"></a>01149 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01150"></a>01150 <span class="preprocessor"></span>      }
<a name="l01151"></a>01151     <span class="keywordflow">catch</span> (...)
<a name="l01152"></a>01152       {
<a name="l01153"></a>01153         this-&gt;m_ = 0;
<a name="l01154"></a>01154         this-&gt;n_ = 0;
<a name="l01155"></a>01155         real_nz_ = 0;
<a name="l01156"></a>01156         imag_nz_ = 0;
<a name="l01157"></a>01157         free(real_ptr_);
<a name="l01158"></a>01158         free(imag_ptr_);
<a name="l01159"></a>01159         real_ptr_ = NULL;
<a name="l01160"></a>01160         imag_ptr_ = NULL;
<a name="l01161"></a>01161         real_ind_ = NULL;
<a name="l01162"></a>01162         imag_ind_ = NULL;
<a name="l01163"></a>01163         this-&gt;real_data_ = NULL;
<a name="l01164"></a>01164         this-&gt;imag_data_ = NULL;
<a name="l01165"></a>01165       }
<a name="l01166"></a>01166     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01167"></a>01167       {
<a name="l01168"></a>01168         this-&gt;m_ = 0;
<a name="l01169"></a>01169         this-&gt;n_ = 0;
<a name="l01170"></a>01170         real_nz_ = 0;
<a name="l01171"></a>01171         imag_nz_ = 0;
<a name="l01172"></a>01172         free(real_ptr_);
<a name="l01173"></a>01173         free(imag_ptr_);
<a name="l01174"></a>01174         real_ptr_ = NULL;
<a name="l01175"></a>01175         imag_ptr_ = NULL;
<a name="l01176"></a>01176         this-&gt;real_data_ = NULL;
<a name="l01177"></a>01177         this-&gt;imag_data_ = NULL;
<a name="l01178"></a>01178       }
<a name="l01179"></a>01179     <span class="keywordflow">if</span> (real_ind_ == NULL &amp;&amp; i != 0)
<a name="l01180"></a>01180       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01181"></a>01181                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01182"></a>01182                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01183"></a>01183                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01184"></a>01184                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_)
<a name="l01185"></a>01185                      + <span class="stringliteral">&quot; row or column indices (real part), for a &quot;</span>
<a name="l01186"></a>01186                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01187"></a>01187 <span class="preprocessor">#endif</span>
<a name="l01188"></a>01188 <span class="preprocessor"></span>
<a name="l01189"></a>01189 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01190"></a>01190 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01191"></a>01191       {
<a name="l01192"></a>01192 <span class="preprocessor">#endif</span>
<a name="l01193"></a>01193 <span class="preprocessor"></span>
<a name="l01194"></a>01194         imag_ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(imag_nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l01195"></a>01195         memcpy(this-&gt;imag_ind_, A.imag_ind_, imag_nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01196"></a>01196 
<a name="l01197"></a>01197 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01198"></a>01198 <span class="preprocessor"></span>      }
<a name="l01199"></a>01199     <span class="keywordflow">catch</span> (...)
<a name="l01200"></a>01200       {
<a name="l01201"></a>01201         this-&gt;m_ = 0;
<a name="l01202"></a>01202         this-&gt;n_ = 0;
<a name="l01203"></a>01203         real_nz_ = 0;
<a name="l01204"></a>01204         imag_nz_ = 0;
<a name="l01205"></a>01205         free(real_ptr_);
<a name="l01206"></a>01206         free(imag_ptr_);
<a name="l01207"></a>01207         real_ptr_ = NULL;
<a name="l01208"></a>01208         imag_ptr_ = NULL;
<a name="l01209"></a>01209         free(imag_ind_);
<a name="l01210"></a>01210         real_ind_ = NULL;
<a name="l01211"></a>01211         imag_ind_ = NULL;
<a name="l01212"></a>01212         this-&gt;real_data_ = NULL;
<a name="l01213"></a>01213         this-&gt;imag_data_ = NULL;
<a name="l01214"></a>01214       }
<a name="l01215"></a>01215     <span class="keywordflow">if</span> (real_ind_ == NULL)
<a name="l01216"></a>01216       {
<a name="l01217"></a>01217         this-&gt;m_ = 0;
<a name="l01218"></a>01218         this-&gt;n_ = 0;
<a name="l01219"></a>01219         real_nz_ = 0;
<a name="l01220"></a>01220         imag_nz_ = 0;
<a name="l01221"></a>01221         free(real_ptr_);
<a name="l01222"></a>01222         free(imag_ptr_);
<a name="l01223"></a>01223         real_ptr_ = NULL;
<a name="l01224"></a>01224         imag_ptr_ = NULL;
<a name="l01225"></a>01225         free(imag_ind_);
<a name="l01226"></a>01226         imag_ind_ = NULL;
<a name="l01227"></a>01227         this-&gt;real_data_ = NULL;
<a name="l01228"></a>01228         this-&gt;imag_data_ = NULL;
<a name="l01229"></a>01229       }
<a name="l01230"></a>01230     <span class="keywordflow">if</span> (imag_ind_ == NULL &amp;&amp; i != 0)
<a name="l01231"></a>01231       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01232"></a>01232                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01233"></a>01233                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01234"></a>01234                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01235"></a>01235                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_)
<a name="l01236"></a>01236                      + <span class="stringliteral">&quot; row or column indices (imaginary part), for a &quot;</span>
<a name="l01237"></a>01237                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01238"></a>01238 <span class="preprocessor">#endif</span>
<a name="l01239"></a>01239 <span class="preprocessor"></span>
<a name="l01240"></a>01240 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01241"></a>01241 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01242"></a>01242       {
<a name="l01243"></a>01243 <span class="preprocessor">#endif</span>
<a name="l01244"></a>01244 <span class="preprocessor"></span>
<a name="l01245"></a>01245         this-&gt;real_data_ = this-&gt;allocator_.allocate(real_nz_, <span class="keyword">this</span>);
<a name="l01246"></a>01246         this-&gt;allocator_.memorycpy(this-&gt;real_data_, A.real_data_, real_nz_);
<a name="l01247"></a>01247 
<a name="l01248"></a>01248 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01249"></a>01249 <span class="preprocessor"></span>      }
<a name="l01250"></a>01250     <span class="keywordflow">catch</span> (...)
<a name="l01251"></a>01251       {
<a name="l01252"></a>01252         this-&gt;m_ = 0;
<a name="l01253"></a>01253         this-&gt;n_ = 0;
<a name="l01254"></a>01254         free(real_ptr_);
<a name="l01255"></a>01255         free(imag_ptr_);
<a name="l01256"></a>01256         real_ptr_ = NULL;
<a name="l01257"></a>01257         imag_ptr_ = NULL;
<a name="l01258"></a>01258         free(real_ind_);
<a name="l01259"></a>01259         free(imag_ind_);
<a name="l01260"></a>01260         real_ind_ = NULL;
<a name="l01261"></a>01261         imag_ind_ = NULL;
<a name="l01262"></a>01262         this-&gt;real_data_ = NULL;
<a name="l01263"></a>01263         this-&gt;imag_data_ = NULL;
<a name="l01264"></a>01264       }
<a name="l01265"></a>01265     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01266"></a>01266       {
<a name="l01267"></a>01267         this-&gt;m_ = 0;
<a name="l01268"></a>01268         this-&gt;n_ = 0;
<a name="l01269"></a>01269         free(real_ptr_);
<a name="l01270"></a>01270         free(imag_ptr_);
<a name="l01271"></a>01271         real_ptr_ = NULL;
<a name="l01272"></a>01272         imag_ptr_ = NULL;
<a name="l01273"></a>01273         free(real_ind_);
<a name="l01274"></a>01274         free(imag_ind_);
<a name="l01275"></a>01275         real_ind_ = NULL;
<a name="l01276"></a>01276         imag_ind_ = NULL;
<a name="l01277"></a>01277         imag_data_ = NULL;
<a name="l01278"></a>01278       }
<a name="l01279"></a>01279     <span class="keywordflow">if</span> (real_data_ == NULL &amp;&amp; i != 0)
<a name="l01280"></a>01280       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01281"></a>01281                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01282"></a>01282                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01283"></a>01283                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * real_nz_) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01284"></a>01284                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(real_nz_) + <span class="stringliteral">&quot; values (real part), for a &quot;</span>
<a name="l01285"></a>01285                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01286"></a>01286 <span class="preprocessor">#endif</span>
<a name="l01287"></a>01287 <span class="preprocessor"></span>
<a name="l01288"></a>01288 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01289"></a>01289 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01290"></a>01290       {
<a name="l01291"></a>01291 <span class="preprocessor">#endif</span>
<a name="l01292"></a>01292 <span class="preprocessor"></span>
<a name="l01293"></a>01293         this-&gt;imag_data_ = this-&gt;allocator_.allocate(imag_nz_, <span class="keyword">this</span>);
<a name="l01294"></a>01294         this-&gt;allocator_.memorycpy(this-&gt;imag_data_, A.imag_data_, imag_nz_);
<a name="l01295"></a>01295 
<a name="l01296"></a>01296 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01297"></a>01297 <span class="preprocessor"></span>      }
<a name="l01298"></a>01298     <span class="keywordflow">catch</span> (...)
<a name="l01299"></a>01299       {
<a name="l01300"></a>01300         this-&gt;m_ = 0;
<a name="l01301"></a>01301         this-&gt;n_ = 0;
<a name="l01302"></a>01302         free(real_ptr_);
<a name="l01303"></a>01303         free(imag_ptr_);
<a name="l01304"></a>01304         real_ptr_ = NULL;
<a name="l01305"></a>01305         imag_ptr_ = NULL;
<a name="l01306"></a>01306         free(real_ind_);
<a name="l01307"></a>01307         free(imag_ind_);
<a name="l01308"></a>01308         real_ind_ = NULL;
<a name="l01309"></a>01309         imag_ind_ = NULL;
<a name="l01310"></a>01310         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01311"></a>01311         this-&gt;real_data_ = NULL;
<a name="l01312"></a>01312         this-&gt;imag_data_ = NULL;
<a name="l01313"></a>01313       }
<a name="l01314"></a>01314     <span class="keywordflow">if</span> (real_data_ == NULL)
<a name="l01315"></a>01315       {
<a name="l01316"></a>01316         this-&gt;m_ = 0;
<a name="l01317"></a>01317         this-&gt;n_ = 0;
<a name="l01318"></a>01318         free(real_ptr_);
<a name="l01319"></a>01319         free(imag_ptr_);
<a name="l01320"></a>01320         real_ptr_ = NULL;
<a name="l01321"></a>01321         imag_ptr_ = NULL;
<a name="l01322"></a>01322         free(real_ind_);
<a name="l01323"></a>01323         free(imag_ind_);
<a name="l01324"></a>01324         real_ind_ = NULL;
<a name="l01325"></a>01325         imag_ind_ = NULL;
<a name="l01326"></a>01326         this-&gt;allocator_.deallocate(this-&gt;real_data_, real_nz_);
<a name="l01327"></a>01327         real_data_ = NULL;
<a name="l01328"></a>01328       }
<a name="l01329"></a>01329     <span class="keywordflow">if</span> (imag_data_ == NULL &amp;&amp; i != 0)
<a name="l01330"></a>01330       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::&quot;</span>)
<a name="l01331"></a>01331                      + <span class="stringliteral">&quot;Matrix_SymComplexSparse(int, int, int, int)&quot;</span>,
<a name="l01332"></a>01332                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l01333"></a>01333                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * imag_nz_) + <span class="stringliteral">&quot; bytes to store &quot;</span>
<a name="l01334"></a>01334                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(imag_nz_) + <span class="stringliteral">&quot; values (imaginary part), for a &quot;</span>
<a name="l01335"></a>01335                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01336"></a>01336 <span class="preprocessor">#endif</span>
<a name="l01337"></a>01337 <span class="preprocessor"></span>
<a name="l01338"></a>01338   }
<a name="l01339"></a>01339 
<a name="l01340"></a>01340 
<a name="l01341"></a>01341   <span class="comment">/*******************</span>
<a name="l01342"></a>01342 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l01343"></a>01343 <span class="comment">   *******************/</span>
<a name="l01344"></a>01344 
<a name="l01345"></a>01345 
<a name="l01347"></a>01347 
<a name="l01353"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846">01353</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01354"></a>01354   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846" title="Returns the number of elements stored in memory.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01355"></a>01355 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#afda1dbd968e2e4b3047c014ab5931846" title="Returns the number of elements stored in memory.">  ::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l01356"></a>01356 <span class="keyword">  </span>{
<a name="l01357"></a>01357     <span class="keywordflow">return</span> real_nz_ + imag_nz_;
<a name="l01358"></a>01358   }
<a name="l01359"></a>01359 
<a name="l01360"></a>01360 
<a name="l01362"></a>01362 
<a name="l01366"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c">01366</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01367"></a>01367   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c" title="Returns (row or column) start indices for the real part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01368"></a>01368 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a005a3b7389aecf38482cc16e4984787c" title="Returns (row or column) start indices for the real part.">  ::GetRealPtr</a>()<span class="keyword"> const</span>
<a name="l01369"></a>01369 <span class="keyword">  </span>{
<a name="l01370"></a>01370     <span class="keywordflow">return</span> real_ptr_;
<a name="l01371"></a>01371   }
<a name="l01372"></a>01372 
<a name="l01373"></a>01373 
<a name="l01375"></a>01375 
<a name="l01379"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac">01379</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01380"></a>01380   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac" title="Returns (row or column) start indices for the imaginary part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01381"></a>01381 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad1bb43e80676e9ee420ea692ba06d0ac" title="Returns (row or column) start indices for the imaginary part.">  ::GetImagPtr</a>()<span class="keyword"> const</span>
<a name="l01382"></a>01382 <span class="keyword">  </span>{
<a name="l01383"></a>01383     <span class="keywordflow">return</span> imag_ptr_;
<a name="l01384"></a>01384   }
<a name="l01385"></a>01385 
<a name="l01386"></a>01386 
<a name="l01388"></a>01388 
<a name="l01395"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b">01395</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01396"></a>01396   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b" title="Returns (row or column) indices of non-zero entries for the real part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01397"></a>01397 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a72f50ab4230ffe4af23c8bbf2203ad0b" title="Returns (row or column) indices of non-zero entries for the real part.">  ::GetRealInd</a>()<span class="keyword"> const</span>
<a name="l01398"></a>01398 <span class="keyword">  </span>{
<a name="l01399"></a>01399     <span class="keywordflow">return</span> real_ind_;
<a name="l01400"></a>01400   }
<a name="l01401"></a>01401 
<a name="l01402"></a>01402 
<a name="l01405"></a>01405 
<a name="l01412"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b">01412</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01413"></a>01413   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b" title="Returns (row or column) indices of non-zero entries for / the imaginary part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01414"></a>01414 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a2bb61950179c75be2150cfae734bb05b" title="Returns (row or column) indices of non-zero entries for / the imaginary part.">  ::GetImagInd</a>()<span class="keyword"> const</span>
<a name="l01415"></a>01415 <span class="keyword">  </span>{
<a name="l01416"></a>01416     <span class="keywordflow">return</span> imag_ind_;
<a name="l01417"></a>01417   }
<a name="l01418"></a>01418 
<a name="l01419"></a>01419 
<a name="l01421"></a>01421 
<a name="l01424"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d">01424</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01425"></a>01425   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d" title="Returns the length of the array of start indices for the real part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01426"></a>01426 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a913aadd75d1ed2e076ea4b014e5fb32d" title="Returns the length of the array of start indices for the real part.">  ::GetRealPtrSize</a>()<span class="keyword"> const</span>
<a name="l01427"></a>01427 <span class="keyword">  </span>{
<a name="l01428"></a>01428     <span class="keywordflow">return</span> (this-&gt;m_ + 1);
<a name="l01429"></a>01429   }
<a name="l01430"></a>01430 
<a name="l01431"></a>01431 
<a name="l01433"></a>01433 
<a name="l01436"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f">01436</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01437"></a>01437   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f" title="Returns the length of the array of start indices for the imaginary part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01438"></a>01438 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acde6ac68023f92f32eceb4f0a422580f" title="Returns the length of the array of start indices for the imaginary part.">  ::GetImagPtrSize</a>()<span class="keyword"> const</span>
<a name="l01439"></a>01439 <span class="keyword">  </span>{
<a name="l01440"></a>01440     <span class="keywordflow">return</span> (this-&gt;m_ + 1);
<a name="l01441"></a>01441   }
<a name="l01442"></a>01442 
<a name="l01443"></a>01443 
<a name="l01446"></a>01446 
<a name="l01456"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807">01456</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01457"></a>01457   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807" title="Returns the length of the array of (column or row) indices for //! the real part...">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01458"></a>01458 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#acdb80353a65a45dfc11b886e7c75a807" title="Returns the length of the array of (column or row) indices for //! the real part...">  ::GetRealIndSize</a>()<span class="keyword"> const</span>
<a name="l01459"></a>01459 <span class="keyword">  </span>{
<a name="l01460"></a>01460     <span class="keywordflow">return</span> real_nz_;
<a name="l01461"></a>01461   }
<a name="l01462"></a>01462 
<a name="l01463"></a>01463 
<a name="l01466"></a>01466 
<a name="l01476"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc">01476</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01477"></a>01477   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc" title="Returns the length of the array of (column or row) indices //! for the imaginary...">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01478"></a>01478 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#af1c1a606649e03827a3710751238a5bc" title="Returns the length of the array of (column or row) indices //! for the imaginary...">  ::GetImagIndSize</a>()<span class="keyword"> const</span>
<a name="l01479"></a>01479 <span class="keyword">  </span>{
<a name="l01480"></a>01480     <span class="keywordflow">return</span> imag_nz_;
<a name="l01481"></a>01481   }
<a name="l01482"></a>01482 
<a name="l01483"></a>01483 
<a name="l01485"></a>01485 
<a name="l01488"></a>01488   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01489"></a>01489   T* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a1a98f711de4b7daf42ea42c8493ca08d" title="Returns the array of values of the real part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetRealData</a>()<span class="keyword"> const</span>
<a name="l01490"></a>01490 <span class="keyword">  </span>{
<a name="l01491"></a>01491     <span class="keywordflow">return</span> real_data_;
<a name="l01492"></a>01492   }
<a name="l01493"></a>01493 
<a name="l01494"></a>01494 
<a name="l01496"></a>01496 
<a name="l01499"></a>01499   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01500"></a>01500   T* <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a411656148afd39425bd6df81a013d180" title="Returns the array of values of the imaginary part.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::GetImagData</a>()<span class="keyword"> const</span>
<a name="l01501"></a>01501 <span class="keyword">  </span>{
<a name="l01502"></a>01502     <span class="keywordflow">return</span> imag_data_;
<a name="l01503"></a>01503   }
<a name="l01504"></a>01504 
<a name="l01505"></a>01505 
<a name="l01506"></a>01506   <span class="comment">/**********************************</span>
<a name="l01507"></a>01507 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l01508"></a>01508 <span class="comment">   **********************************/</span>
<a name="l01509"></a>01509 
<a name="l01510"></a>01510 
<a name="l01512"></a>01512 
<a name="l01518"></a>01518   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01519"></a>01519   <span class="keyword">inline</span> complex&lt;typename Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01520"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369">01520</a>                  ::value_type&gt;
<a name="l01521"></a>01521   <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369" title="Access operator.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01522"></a>01522 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01523"></a>01523 <span class="keyword">  </span>{
<a name="l01524"></a>01524 
<a name="l01525"></a>01525 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01526"></a>01526 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l01527"></a>01527       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::operator()&quot;</span>,
<a name="l01528"></a>01528                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l01529"></a>01529                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01530"></a>01530     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l01531"></a>01531       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::operator()&quot;</span>,
<a name="l01532"></a>01532                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l01533"></a>01533                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01534"></a>01534 <span class="preprocessor">#endif</span>
<a name="l01535"></a>01535 <span class="preprocessor"></span>
<a name="l01536"></a>01536     <span class="keywordtype">int</span> real_k, imag_k, l;
<a name="l01537"></a>01537     <span class="keywordtype">int</span> real_a, real_b;
<a name="l01538"></a>01538     <span class="keywordtype">int</span> imag_a, imag_b;
<a name="l01539"></a>01539 
<a name="l01540"></a>01540     <span class="comment">// Only the upper part is stored.</span>
<a name="l01541"></a>01541     <span class="keywordflow">if</span> (i &gt; j)
<a name="l01542"></a>01542       {
<a name="l01543"></a>01543         l = i;
<a name="l01544"></a>01544         i = j;
<a name="l01545"></a>01545         j = l;
<a name="l01546"></a>01546       }
<a name="l01547"></a>01547 
<a name="l01548"></a>01548     real_a = real_ptr_[Storage::GetFirst(i, j)];
<a name="l01549"></a>01549     real_b = real_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01550"></a>01550 
<a name="l01551"></a>01551     imag_a = imag_ptr_[Storage::GetFirst(i, j)];
<a name="l01552"></a>01552     imag_b = imag_ptr_[Storage::GetFirst(i, j) + 1];
<a name="l01553"></a>01553 
<a name="l01554"></a>01554     <span class="keywordflow">if</span> (real_a != real_b)
<a name="l01555"></a>01555       {
<a name="l01556"></a>01556         l = Storage::GetSecond(i, j);
<a name="l01557"></a>01557         <span class="keywordflow">for</span> (real_k = real_a;
<a name="l01558"></a>01558              (real_k &lt; real_b - 1) &amp;&amp; (real_ind_[real_k] &lt; l);
<a name="l01559"></a>01559              real_k++);
<a name="l01560"></a>01560         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l01561"></a>01561           {
<a name="l01562"></a>01562             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l01563"></a>01563                  (imag_k &lt; imag_b - 1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l01564"></a>01564                  imag_k++);
<a name="l01565"></a>01565             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l01566"></a>01566               {
<a name="l01567"></a>01567                 <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01568"></a>01568                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], imag_data_[imag_k]);
<a name="l01569"></a>01569                 <span class="keywordflow">else</span>
<a name="l01570"></a>01570                   <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l01571"></a>01571               }
<a name="l01572"></a>01572             <span class="keywordflow">else</span>
<a name="l01573"></a>01573               <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01574"></a>01574                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l01575"></a>01575               <span class="keywordflow">else</span>
<a name="l01576"></a>01576                 <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01577"></a>01577           }
<a name="l01578"></a>01578         <span class="keywordflow">else</span>
<a name="l01579"></a>01579           {
<a name="l01580"></a>01580             <span class="keywordflow">if</span> (real_ind_[real_k] == l)
<a name="l01581"></a>01581               <span class="keywordflow">return</span> complex&lt;T&gt;(real_data_[real_k], T(0));
<a name="l01582"></a>01582             <span class="keywordflow">else</span>
<a name="l01583"></a>01583               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01584"></a>01584           }
<a name="l01585"></a>01585       }
<a name="l01586"></a>01586     <span class="keywordflow">else</span>
<a name="l01587"></a>01587       {
<a name="l01588"></a>01588         <span class="keywordflow">if</span> (imag_a != imag_b)
<a name="l01589"></a>01589           {
<a name="l01590"></a>01590             l = Storage::GetSecond(i, j);
<a name="l01591"></a>01591             <span class="keywordflow">for</span> (imag_k = imag_a;
<a name="l01592"></a>01592                  (imag_k &lt; imag_b - 1) &amp;&amp; (imag_ind_[imag_k] &lt; l);
<a name="l01593"></a>01593                  imag_k++);
<a name="l01594"></a>01594             <span class="keywordflow">if</span> (imag_ind_[imag_k] == l)
<a name="l01595"></a>01595               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), imag_data_[imag_k]);
<a name="l01596"></a>01596             <span class="keywordflow">else</span>
<a name="l01597"></a>01597               <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01598"></a>01598           }
<a name="l01599"></a>01599         <span class="keywordflow">else</span>
<a name="l01600"></a>01600           <span class="keywordflow">return</span> complex&lt;T&gt;(T(0), T(0));
<a name="l01601"></a>01601       }
<a name="l01602"></a>01602   }
<a name="l01603"></a>01603 
<a name="l01604"></a>01604 
<a name="l01606"></a>01606 
<a name="l01612"></a>01612   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01613"></a>01613   <span class="keyword">inline</span> complex&lt;typename Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01614"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3b6f94e76e99c7948f1e78a31012e53c">01614</a>                  ::value_type&gt;&amp;
<a name="l01615"></a>01615   <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3b6f94e76e99c7948f1e78a31012e53c" title="Unavailable access method.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01616"></a>01616 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3b6f94e76e99c7948f1e78a31012e53c" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01617"></a>01617   {
<a name="l01618"></a>01618     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l01619"></a>01619   }
<a name="l01620"></a>01620 
<a name="l01621"></a>01621 
<a name="l01623"></a>01623 
<a name="l01629"></a>01629   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01630"></a>01630   <span class="keyword">inline</span>
<a name="l01631"></a>01631   <span class="keyword">const</span> complex&lt;typename Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;
<a name="l01632"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a05132fdf9928e62518a9369f213fad00">01632</a>                 ::value_type&gt;&amp;
<a name="l01633"></a>01633   <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3b6f94e76e99c7948f1e78a31012e53c" title="Unavailable access method.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01634"></a>01634 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#a3b6f94e76e99c7948f1e78a31012e53c" title="Unavailable access method.">  ::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l01635"></a>01635 <span class="keyword">  </span>{
<a name="l01636"></a>01636     <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix_SymComplexSparse::Val(int i, int j)&quot;</span>);
<a name="l01637"></a>01637   }
<a name="l01638"></a>01638 
<a name="l01639"></a>01639 
<a name="l01641"></a>01641 
<a name="l01646"></a>01646   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01647"></a><a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67">01647</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l01648"></a>01648   <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67" title="Duplicates a matrix (assignment operator).">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01649"></a>01649 <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01650"></a>01650   {
<a name="l01651"></a>01651     this-&gt;Copy(A);
<a name="l01652"></a>01652 
<a name="l01653"></a>01653     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01654"></a>01654   }
<a name="l01655"></a>01655 
<a name="l01656"></a>01656 
<a name="l01657"></a>01657   <span class="comment">/************************</span>
<a name="l01658"></a>01658 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l01659"></a>01659 <span class="comment">   ************************/</span>
<a name="l01660"></a>01660 
<a name="l01661"></a>01661 
<a name="l01663"></a>01663 
<a name="l01668"></a>01668   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01669"></a>01669   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php#abe86d6545d4aeca07257de2d8e7a85f2" title="Displays the matrix on the standard output.">Matrix_SymComplexSparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01670"></a>01670 <span class="keyword">  </span>{
<a name="l01671"></a>01671     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01672"></a>01672       {
<a name="l01673"></a>01673         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l01674"></a>01674           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l01675"></a>01675         cout &lt;&lt; endl;
<a name="l01676"></a>01676       }
<a name="l01677"></a>01677   }
<a name="l01678"></a>01678 
<a name="l01679"></a>01679 
<a name="l01680"></a>01680 
<a name="l01682"></a>01682   <span class="comment">// MATRIX&lt;COLSYMCOMPLEXSPARSE&gt; //</span>
<a name="l01684"></a>01684 <span class="comment"></span>
<a name="l01685"></a>01685 
<a name="l01686"></a>01686   <span class="comment">/****************</span>
<a name="l01687"></a>01687 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01688"></a>01688 <span class="comment">   ****************/</span>
<a name="l01689"></a>01689 
<a name="l01691"></a>01691 
<a name="l01694"></a>01694   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01695"></a>01695   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php#a9cde0f59f5bc4e5d5547d6bcc30afbcb" title="Default constructor.">Matrix&lt;T, Prop, ColSymComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01696"></a>01696     Matrix_SymComplexSparse&lt;T, Prop, ColSymComplexSparse, Allocator&gt;()
<a name="l01697"></a>01697   {
<a name="l01698"></a>01698   }
<a name="l01699"></a>01699 
<a name="l01700"></a>01700 
<a name="l01702"></a>01702 
<a name="l01706"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php#a8ccbbb4606ddce0153a76cabb638ffdd">01706</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01707"></a>01707   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymComplexSparse, Allocator&gt;</a>
<a name="l01708"></a>01708 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01709"></a>01709     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">ColSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01710"></a>01710                                                                      0, 0)
<a name="l01711"></a>01711   {
<a name="l01712"></a>01712   }
<a name="l01713"></a>01713 
<a name="l01714"></a>01714 
<a name="l01716"></a>01716 
<a name="l01727"></a>01727   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01728"></a>01728   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymComplexSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01729"></a>01729                                                           <span class="keywordtype">int</span> real_nz,
<a name="l01730"></a>01730                                                           <span class="keywordtype">int</span> imag_nz):
<a name="l01731"></a>01731     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop,
<a name="l01732"></a>01732     <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">ColSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01733"></a>01733                                     real_nz, imag_nz)
<a name="l01734"></a>01734   {
<a name="l01735"></a>01735   }
<a name="l01736"></a>01736 
<a name="l01737"></a>01737 
<a name="l01739"></a>01739 
<a name="l01758"></a>01758   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01759"></a>01759   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01760"></a>01760             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01761"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_complex_sparse_00_01_allocator_01_4.php#a7c363b2b4a1eb568622d617999fdcfad">01761</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01762"></a>01762   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSymComplexSparse, Allocator&gt;::</a>
<a name="l01763"></a>01763 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01764"></a>01764          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l01765"></a>01765          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l01766"></a>01766          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l01767"></a>01767          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l01768"></a>01768          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l01769"></a>01769          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l01770"></a>01770     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop,
<a name="l01771"></a>01771     <a class="code" href="class_seldon_1_1_col_sym_complex_sparse.php">ColSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01772"></a>01772                                     real_values,
<a name="l01773"></a>01773                                     real_ptr,
<a name="l01774"></a>01774                                     real_ind,
<a name="l01775"></a>01775                                     imag_values,
<a name="l01776"></a>01776                                     imag_ptr,
<a name="l01777"></a>01777                                     imag_ind)
<a name="l01778"></a>01778   {
<a name="l01779"></a>01779   }
<a name="l01780"></a>01780 
<a name="l01781"></a>01781 
<a name="l01782"></a>01782 
<a name="l01784"></a>01784   <span class="comment">// MATRIX&lt;ROWSYMCOMPLEXSPARSE&gt; //</span>
<a name="l01786"></a>01786 <span class="comment"></span>
<a name="l01787"></a>01787 
<a name="l01788"></a>01788   <span class="comment">/****************</span>
<a name="l01789"></a>01789 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01790"></a>01790 <span class="comment">   ****************/</span>
<a name="l01791"></a>01791 
<a name="l01793"></a>01793 
<a name="l01796"></a>01796   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01797"></a>01797   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymComplexSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01798"></a>01798     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator&gt;()
<a name="l01799"></a>01799   {
<a name="l01800"></a>01800   }
<a name="l01801"></a>01801 
<a name="l01802"></a>01802 
<a name="l01804"></a>01804 
<a name="l01808"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a47f8fde52ccac1a7747c5b66c4c0b46e">01808</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01809"></a>01809   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymComplexSparse, Allocator&gt;</a>
<a name="l01810"></a>01810 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01811"></a>01811     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01812"></a>01812                                                                      0, 0)
<a name="l01813"></a>01813   {
<a name="l01814"></a>01814   }
<a name="l01815"></a>01815 
<a name="l01816"></a>01816 
<a name="l01828"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a10616155b7471e78fdf59f4f2a760304">01828</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01829"></a>01829   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymComplexSparse, Allocator&gt;</a>
<a name="l01830"></a>01830 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> real_nz, <span class="keywordtype">int</span> imag_nz):
<a name="l01831"></a>01831     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01832"></a>01832                                                                      real_nz,
<a name="l01833"></a>01833                                                                      imag_nz)
<a name="l01834"></a>01834   {
<a name="l01835"></a>01835   }
<a name="l01836"></a>01836 
<a name="l01837"></a>01837 
<a name="l01839"></a>01839 
<a name="l01858"></a>01858   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01859"></a>01859   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01860"></a>01860             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01861"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_complex_sparse_00_01_allocator_01_4.php#a0df1be9842a16925408e0ed5761218f3">01861</a>             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01862"></a>01862   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSymComplexSparse, Allocator&gt;::</a>
<a name="l01863"></a>01863 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01864"></a>01864          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; real_values,
<a name="l01865"></a>01865          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; real_ptr,
<a name="l01866"></a>01866          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; real_ind,
<a name="l01867"></a>01867          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; imag_values,
<a name="l01868"></a>01868          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; imag_ptr,
<a name="l01869"></a>01869          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; imag_ind):
<a name="l01870"></a>01870     <a class="code" href="class_seldon_1_1_matrix___sym_complex_sparse.php" title="Symmetric complex sparse-matrix class.">Matrix_SymComplexSparse</a>&lt;T, Prop,
<a name="l01871"></a>01871     <a class="code" href="class_seldon_1_1_row_sym_complex_sparse.php">RowSymComplexSparse</a>, Allocator&gt;(i, j,
<a name="l01872"></a>01872                                     real_values,
<a name="l01873"></a>01873                                     real_ptr,
<a name="l01874"></a>01874                                     real_ind,
<a name="l01875"></a>01875                                     imag_values,
<a name="l01876"></a>01876                                     imag_ptr,
<a name="l01877"></a>01877                                     imag_ind)
<a name="l01878"></a>01878   {
<a name="l01879"></a>01879   }
<a name="l01880"></a>01880 
<a name="l01881"></a>01881 
<a name="l01882"></a>01882 } <span class="comment">// namespace Seldon.</span>
<a name="l01883"></a>01883 
<a name="l01884"></a>01884 <span class="preprocessor">#define SELDON_FILE_MATRIX_SYMCOMPLEXSPARSE_CXX</span>
<a name="l01885"></a>01885 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
