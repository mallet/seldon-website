<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a71256dab229c463457ff8848aa6d1fa7">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a13715cb4e64fbe58e24fa86897131e25">Copy</a>(const Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a8950827192f33de71909f783032c9f5f">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a9d4ddb6035ed47fadc9f35df03c3764e">GetInd</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a6ffb37230f5629d9f85a6b962bc8127d">GetIndSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a4be3dd5024addbf8bfa332193c4030cd">GetNonZeros</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ae6576503580320224c84cc46d675781d">GetPtr</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a046fce2e283dc38a9ed0ce5b570e6f11">GetPtrSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>ind_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a11e9550e00334880bb869d69aaf76546">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a2e7a7c8983b464cba895701796f49e3b">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#a9c00a889dfa3e697a50f156a7224759c">Matrix</a>(int i, int j, int nz)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php#aca58932742ad99038531a482692d83f7">Matrix</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aaffa73e2f7d10d2f2dfff4a701e72405">Matrix_SymSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#afa0d65cf9cf27bd033588a54dae20f09">Matrix_SymSparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a14fd7e8b9ecea1af9d25faf494dc6431">Matrix_SymSparse</a>(int i, int j, int nz)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a626e591de87845d39c44796cd9b19bc9">Matrix_SymSparse</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a6895a30236bf5f7ec632cba1916c2e4b">Matrix_SymSparse</a>(const Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a99f9900aa0c5bd7c6040686a53d39a85">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>nz_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75">operator=</a>(const Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a20b7ac2a821a2f54f4e325c07846b6cd">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>ptr_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aeb3fdc796d3c31e4b4729d3e9e15834c">SetData</a>(int i, int j, Vector&lt; T, Storage0, Allocator0 &gt; &amp;values, Vector&lt; int, Storage1, Allocator1 &gt; &amp;ptr, Vector&lt; int, Storage2, Allocator2 &gt; &amp;ind)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, int nz, pointer values, int *ptr, int *ind) (defined in <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aaca494c5d6a5c03b5a76c3cfbfc412fe">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a30ac9be8e0a8f70f3c2eae3f2e6657bb">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ab1a9893cc7b8823fe6a2a0eee9520460">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aa28e902cd297fe56f7a88cea69c154d5">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#a6f46818c1d832ff9be950a5b6f0fc9c7">~Matrix_SymSparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___sym_sparse.php">Seldon::Matrix_SymSparse&lt; T, Prop, RowSymSparse, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
