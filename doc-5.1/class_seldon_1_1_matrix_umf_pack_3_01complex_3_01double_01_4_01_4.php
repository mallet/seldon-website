<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php">MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;" --><!-- doxytag: inherits="MatrixUmfPack_Base&lt; complex&lt; double &gt; &gt;" -->
<p>class to solve linear system in complex double precision with UmfPack  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.png" usemap="#Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;_map" alt=""/>
  <map id="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;_map" name="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;_map">
<area href="class_seldon_1_1_matrix_umf_pack___base.php" alt="Seldon::MatrixUmfPack_Base&lt; complex&lt; double &gt; &gt;" shape="rect" coords="0,0,308,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a83049b296e185003b45f3d02d664adb2"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::MatrixUmfPack" ref="a83049b296e185003b45f3d02d664adb2" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a83049b296e185003b45f3d02d664adb2">MatrixUmfPack</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">constructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a2cc80beb8d893f7b1b8e68872de1290a"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::~MatrixUmfPack" ref="a2cc80beb8d893f7b1b8e68872de1290a" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2cc80beb8d893f7b1b8e68872de1290a">~MatrixUmfPack</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">destructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a490fccbcfd57c7e3ad07c8ea3739afab"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Clear" ref="a490fccbcfd57c7e3ad07c8ea3739afab" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a490fccbcfd57c7e3ad07c8ea3739afab">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">we clear present factorization if any <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a0efe83ef21a51ee358dec89fc0086ea4"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::FactorizeMatrix" ref="a0efe83ef21a51ee358dec89fc0086ea4" args="(Matrix&lt; complex&lt; double &gt;, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a0efe83ef21a51ee358dec89fc0086ea4">FactorizeMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; complex&lt; double &gt;, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">LU factorization using UmfPack in double complex precision. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a2e24772a5a8588f8fa826bc060fd8cbb"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Solve" ref="a2e24772a5a8588f8fa826bc060fd8cbb" args="(Vector&lt; complex&lt; double &gt;, VectFull, Allocator2 &gt; &amp;x)" -->
template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a2e24772a5a8588f8fa826bc060fd8cbb">Solve</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; complex&lt; double &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">solves linear system in complex double precision using UmfPack <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad88543986d6cf6273c9f56654820b2f1"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::HideMessages" ref="ad88543986d6cf6273c9f56654820b2f1" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>HideMessages</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af5a60fc35fefccab49c6888cdb148c94"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::ShowMessages" ref="af5a60fc35fefccab49c6888cdb148c94" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ShowMessages</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a86ccadbb48712eec2c6f335236f70a"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Control" ref="a4a86ccadbb48712eec2c6f335236f70a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Control</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a6b504d370b37ef4951626d2a3ce439"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Info" ref="a4a6b504d370b37ef4951626d2a3ce439" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439">Info</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">parameters for UmfPack <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4657c453f55d8b8077af14d70aa1f3e0"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Symbolic" ref="a4657c453f55d8b8077af14d70aa1f3e0" args="" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>Symbolic</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a91b3ba9d23982fc5b75ebf8f4d2eea"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::Numeric" ref="a7a91b3ba9d23982fc5b75ebf8f4d2eea" args="" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea">Numeric</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">pointers of UmfPack objects <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa442e0754359877423d73f857aa75731"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::n" ref="aa442e0754359877423d73f857aa75731" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731">n</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">number of rows in the matrix <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00b2252a6082727f89fcba9ca33fbf7d"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::display_info" ref="a00b2252a6082727f89fcba9ca33fbf7d" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a00b2252a6082727f89fcba9ca33fbf7d">display_info</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">true if display is allowed <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac6a40597353446887c3b8c0c210aae74"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::transpose" ref="ac6a40597353446887c3b8c0c210aae74" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><b>transpose</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a9a9a32db0c5d5edb1f6c7bc5e7560876"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::ptr_" ref="a9a9a32db0c5d5edb1f6c7bc5e7560876" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a9a9a32db0c5d5edb1f6c7bc5e7560876">ptr_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">arrays containing matrix pattern in csc format <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a3defa58571a0e29450a732fbdb0415aa"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::ind_" ref="a3defa58571a0e29450a732fbdb0415aa" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>ind_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7cfdb169fedc7f0b9a6233645011c637"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::data_real_" ref="a7cfdb169fedc7f0b9a6233645011c637" args="" -->
double *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a7cfdb169fedc7f0b9a6233645011c637">data_real_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">non-zero values <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a0048ab9374fd1ccbb3a60e4989b42665"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;::data_imag_" ref="a0048ab9374fd1ccbb3a60e4989b42665" args="" -->
double *&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_imag_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;&gt;<br/>
 class Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</h3>

<p>class to solve linear system in complex double precision with UmfPack </p>

<p>Definition at line <a class="el" href="_umf_pack_8hxx_source.php#l00091">91</a> of file <a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
