<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/clapack.h</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Headers from CLAPACK, downloaded at http://www.netlib.org/clapack/.</span>
<a name="l00002"></a>00002 
<a name="l00003"></a>00003 <span class="comment">// Modifications (by Vivien Mallet):</span>
<a name="l00004"></a>00004 <span class="comment">// Replacements:</span>
<a name="l00005"></a>00005 <span class="comment">//    integer       --&gt; LAPACK_INTEGER</span>
<a name="l00006"></a>00006 <span class="comment">//    real          --&gt; LAPACK_REAL</span>
<a name="l00007"></a>00007 <span class="comment">//    doublereal    --&gt; LAPACK_DOUBLEREAL</span>
<a name="l00008"></a>00008 <span class="comment">//    complex       --&gt; LAPACK_COMPLEX</span>
<a name="l00009"></a>00009 <span class="comment">//    doublecomplex --&gt; LAPACK_DOUBLECOMPLEX</span>
<a name="l00010"></a>00010 <span class="comment">//    logical       --&gt; LAPACK_LOGICAL</span>
<a name="l00011"></a>00011 <span class="comment">//    L_fp          --&gt; LAPACK_L_FP</span>
<a name="l00012"></a>00012 <span class="comment">//    ftnlen        --&gt; LAPACK_FTNLEN</span>
<a name="l00013"></a>00013 
<a name="l00014"></a>00014 <span class="preprocessor">#ifndef __CLAPACK_H</span>
<a name="l00015"></a>00015 <span class="preprocessor"></span><span class="preprocessor">#define __CLAPACK_H</span>
<a name="l00016"></a>00016 <span class="preprocessor"></span> 
<a name="l00017"></a>00017 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cbdsqr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ncvt, LAPACK_INTEGER *
<a name="l00018"></a>00018                              nru, LAPACK_INTEGER *ncc, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *vt, LAPACK_INTEGER *ldvt, 
<a name="l00019"></a>00019                              LAPACK_COMPLEX *u, LAPACK_INTEGER *ldu, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *rwork, 
<a name="l00020"></a>00020                              LAPACK_INTEGER *info);
<a name="l00021"></a>00021  
<a name="l00022"></a>00022 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbbrd_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *ncc,
<a name="l00023"></a>00023                              LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *d__, 
<a name="l00024"></a>00024                              LAPACK_REAL *e, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *pt, LAPACK_INTEGER *ldpt, 
<a name="l00025"></a>00025                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00026"></a>00026  
<a name="l00027"></a>00027 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l00028"></a>00028                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, 
<a name="l00029"></a>00029                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00030"></a>00030  
<a name="l00031"></a>00031 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l00032"></a>00032                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL 
<a name="l00033"></a>00033                              *colcnd, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l00034"></a>00034  
<a name="l00035"></a>00035 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l00036"></a>00036                              ku, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *afb, LAPACK_INTEGER *
<a name="l00037"></a>00037                              ldafb, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *
<a name="l00038"></a>00038                              ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *
<a name="l00039"></a>00039                              info);
<a name="l00040"></a>00040  
<a name="l00041"></a>00041 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_INTEGER *
<a name="l00042"></a>00042                             nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *
<a name="l00043"></a>00043                             ldb, LAPACK_INTEGER *info);
<a name="l00044"></a>00044  
<a name="l00045"></a>00045 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl,
<a name="l00046"></a>00046                              LAPACK_INTEGER *ku, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *afb,
<a name="l00047"></a>00047                              LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, LAPACK_REAL *r__, LAPACK_REAL *c__, 
<a name="l00048"></a>00048                              LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL 
<a name="l00049"></a>00049                              *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00050"></a>00050  
<a name="l00051"></a>00051 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbtf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l00052"></a>00052                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00053"></a>00053  
<a name="l00054"></a>00054 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbtrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l00055"></a>00055                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00056"></a>00056  
<a name="l00057"></a>00057 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgbtrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l00058"></a>00058                              ku, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX 
<a name="l00059"></a>00059                              *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00060"></a>00060  
<a name="l00061"></a>00061 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgebak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l00062"></a>00062                              LAPACK_INTEGER *ihi, LAPACK_REAL *scale, LAPACK_INTEGER *m, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, 
<a name="l00063"></a>00063                              LAPACK_INTEGER *info);
<a name="l00064"></a>00064  
<a name="l00065"></a>00065 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgebal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l00066"></a>00066                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *scale, LAPACK_INTEGER *info);
<a name="l00067"></a>00067  
<a name="l00068"></a>00068 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgebd2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00069"></a>00069                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *tauq, LAPACK_COMPLEX *taup, LAPACK_COMPLEX *work, 
<a name="l00070"></a>00070                              LAPACK_INTEGER *info);
<a name="l00071"></a>00071  
<a name="l00072"></a>00072 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgebrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00073"></a>00073                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *tauq, LAPACK_COMPLEX *taup, LAPACK_COMPLEX *work, 
<a name="l00074"></a>00074                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00075"></a>00075  
<a name="l00076"></a>00076 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgecon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00077"></a>00077                              LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00078"></a>00078  
<a name="l00079"></a>00079 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00080"></a>00080                              LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *colcnd, LAPACK_REAL *amax, 
<a name="l00081"></a>00081                              LAPACK_INTEGER *info);
<a name="l00082"></a>00082  
<a name="l00083"></a>00083 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgees_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, LAPACK_INTEGER *n, 
<a name="l00084"></a>00084                             LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_COMPLEX *w, LAPACK_COMPLEX *vs, 
<a name="l00085"></a>00085                             LAPACK_INTEGER *ldvs, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_LOGICAL *
<a name="l00086"></a>00086                             bwork, LAPACK_INTEGER *info);
<a name="l00087"></a>00087  
<a name="l00088"></a>00088 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeesx_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, <span class="keywordtype">char</span> *
<a name="l00089"></a>00089                              sense, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_COMPLEX *
<a name="l00090"></a>00090                              w, LAPACK_COMPLEX *vs, LAPACK_INTEGER *ldvs, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_COMPLEX *
<a name="l00091"></a>00091                              work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l00092"></a>00092  
<a name="l00093"></a>00093 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00094"></a>00094                             LAPACK_INTEGER *lda, LAPACK_COMPLEX *w, LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, 
<a name="l00095"></a>00095                             LAPACK_INTEGER *ldvr, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *
<a name="l00096"></a>00096                             info);
<a name="l00097"></a>00097  
<a name="l00098"></a>00098 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l00099"></a>00099                              sense, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *w, LAPACK_COMPLEX *vl, 
<a name="l00100"></a>00100                              LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi,
<a name="l00101"></a>00101                              LAPACK_REAL *scale, LAPACK_REAL *abnrm, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_COMPLEX *work, 
<a name="l00102"></a>00102                              LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00103"></a>00103  
<a name="l00104"></a>00104 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgegs_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, LAPACK_INTEGER *n, LAPACK_COMPLEX *
<a name="l00105"></a>00105                             a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *
<a name="l00106"></a>00106                             beta, LAPACK_COMPLEX *vsl, LAPACK_INTEGER *ldvsl, LAPACK_COMPLEX *vsr, LAPACK_INTEGER *ldvsr, 
<a name="l00107"></a>00107                             LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00108"></a>00108  
<a name="l00109"></a>00109 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgegv_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00110"></a>00110                             LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta,
<a name="l00111"></a>00111                             LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_COMPLEX *
<a name="l00112"></a>00112                             work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00113"></a>00113  
<a name="l00114"></a>00114 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgehd2_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *
<a name="l00115"></a>00115                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00116"></a>00116  
<a name="l00117"></a>00117 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgehrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *
<a name="l00118"></a>00118                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l00119"></a>00119                              *info);
<a name="l00120"></a>00120  
<a name="l00121"></a>00121 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgelq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00122"></a>00122                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00123"></a>00123  
<a name="l00124"></a>00124 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgelqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00125"></a>00125                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00126"></a>00126  
<a name="l00127"></a>00127 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgels_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00128"></a>00128                             nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *
<a name="l00129"></a>00129                             work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00130"></a>00130  
<a name="l00131"></a>00131 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgelsx_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00132"></a>00132                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *jpvt, LAPACK_REAL *rcond,
<a name="l00133"></a>00133                              LAPACK_INTEGER *rank, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00134"></a>00134  
<a name="l00135"></a>00135 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgelsy_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00136"></a>00136                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *jpvt, LAPACK_REAL *rcond,
<a name="l00137"></a>00137                              LAPACK_INTEGER *rank, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *
<a name="l00138"></a>00138                              info);
<a name="l00139"></a>00139  
<a name="l00140"></a>00140 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeql2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00141"></a>00141                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00142"></a>00142  
<a name="l00143"></a>00143 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeqlf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00144"></a>00144                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00145"></a>00145  
<a name="l00146"></a>00146 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeqp3_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00147"></a>00147                              LAPACK_INTEGER *jpvt, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *
<a name="l00148"></a>00148                              rwork, LAPACK_INTEGER *info);
<a name="l00149"></a>00149  
<a name="l00150"></a>00150 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeqpf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00151"></a>00151                              LAPACK_INTEGER *jpvt, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *
<a name="l00152"></a>00152                              info);
<a name="l00153"></a>00153  
<a name="l00154"></a>00154 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeqr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00155"></a>00155                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00156"></a>00156  
<a name="l00157"></a>00157 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgeqrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00158"></a>00158                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00159"></a>00159  
<a name="l00160"></a>00160 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgerfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00161"></a>00161                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *
<a name="l00162"></a>00162                              b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00163"></a>00163                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00164"></a>00164  
<a name="l00165"></a>00165 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgerq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00166"></a>00166                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00167"></a>00167  
<a name="l00168"></a>00168 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgerqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00169"></a>00169                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00170"></a>00170  
<a name="l00171"></a>00171 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgesc2_(LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *
<a name="l00172"></a>00172                              rhs, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv, LAPACK_REAL *scale);
<a name="l00173"></a>00173  
<a name="l00174"></a>00174 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgesv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *
<a name="l00175"></a>00175                             lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00176"></a>00176  
<a name="l00177"></a>00177 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgesvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobvt, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l00178"></a>00178                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_REAL *s, LAPACK_COMPLEX *u, LAPACK_INTEGER *
<a name="l00179"></a>00179                              ldu, LAPACK_COMPLEX *vt, LAPACK_INTEGER *ldvt, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l00180"></a>00180                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00181"></a>00181  
<a name="l00182"></a>00182 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00183"></a>00183                              nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *
<a name="l00184"></a>00184                              ipiv, <span class="keywordtype">char</span> *equed, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l00185"></a>00185                              LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00186"></a>00186                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00187"></a>00187  
<a name="l00188"></a>00188 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgetc2_(LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *
<a name="l00189"></a>00189                              ipiv, LAPACK_INTEGER *jpiv, LAPACK_INTEGER *info);
<a name="l00190"></a>00190  
<a name="l00191"></a>00191 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgetf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00192"></a>00192                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00193"></a>00193  
<a name="l00194"></a>00194 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgetrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00195"></a>00195                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00196"></a>00196  
<a name="l00197"></a>00197 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgetri_(LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *
<a name="l00198"></a>00198                              ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00199"></a>00199  
<a name="l00200"></a>00200 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgetrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00201"></a>00201                              a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l00202"></a>00202                              info);
<a name="l00203"></a>00203  
<a name="l00204"></a>00204 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggbak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l00205"></a>00205                              LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, LAPACK_REAL *rscale, LAPACK_INTEGER *m, LAPACK_COMPLEX *v, 
<a name="l00206"></a>00206                              LAPACK_INTEGER *ldv, LAPACK_INTEGER *info);
<a name="l00207"></a>00207  
<a name="l00208"></a>00208 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggbal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l00209"></a>00209                              LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, 
<a name="l00210"></a>00210                              LAPACK_REAL *rscale, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l00211"></a>00211  
<a name="l00212"></a>00212 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgges_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l00213"></a>00213                             selctg, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *
<a name="l00214"></a>00214                             ldb, LAPACK_INTEGER *sdim, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *vsl, 
<a name="l00215"></a>00215                             LAPACK_INTEGER *ldvsl, LAPACK_COMPLEX *vsr, LAPACK_INTEGER *ldvsr, LAPACK_COMPLEX *work, LAPACK_INTEGER *
<a name="l00216"></a>00216                             lwork, LAPACK_REAL *rwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l00217"></a>00217  
<a name="l00218"></a>00218 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggesx_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l00219"></a>00219                              selctg, <span class="keywordtype">char</span> *sense, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b,
<a name="l00220"></a>00220                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *
<a name="l00221"></a>00221                              vsl, LAPACK_INTEGER *ldvsl, LAPACK_COMPLEX *vsr, LAPACK_INTEGER *ldvsr, LAPACK_REAL *rconde, LAPACK_REAL 
<a name="l00222"></a>00222                              *rcondv, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, 
<a name="l00223"></a>00223                              LAPACK_INTEGER *liwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l00224"></a>00224  
<a name="l00225"></a>00225 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00226"></a>00226                             LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta,
<a name="l00227"></a>00227                             LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_COMPLEX *
<a name="l00228"></a>00228                             work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00229"></a>00229  
<a name="l00230"></a>00230 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l00231"></a>00231                              sense, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb,
<a name="l00232"></a>00232                              LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *
<a name="l00233"></a>00233                              vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, LAPACK_REAL *
<a name="l00234"></a>00234                              rscale, LAPACK_REAL *abnrm, LAPACK_REAL *bbnrm, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_COMPLEX 
<a name="l00235"></a>00235                              *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_LOGICAL *bwork, 
<a name="l00236"></a>00236                              LAPACK_INTEGER *info);
<a name="l00237"></a>00237  
<a name="l00238"></a>00238 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggglm_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_COMPLEX *a, 
<a name="l00239"></a>00239                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *x, 
<a name="l00240"></a>00240                              LAPACK_COMPLEX *y, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00241"></a>00241  
<a name="l00242"></a>00242 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgghrd_(<span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00243"></a>00243                              ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb,
<a name="l00244"></a>00244                              LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *info);
<a name="l00245"></a>00245  
<a name="l00246"></a>00246 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgglse_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_COMPLEX *a, 
<a name="l00247"></a>00247                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *c__, LAPACK_COMPLEX *d__, 
<a name="l00248"></a>00248                              LAPACK_COMPLEX *x, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00249"></a>00249  
<a name="l00250"></a>00250 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggqrf_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_COMPLEX *a, 
<a name="l00251"></a>00251                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *taua, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *taub, 
<a name="l00252"></a>00252                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00253"></a>00253  
<a name="l00254"></a>00254 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00255"></a>00255                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *taua, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *taub, 
<a name="l00256"></a>00256                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00257"></a>00257  
<a name="l00258"></a>00258 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggsvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l00259"></a>00259                              LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *a, LAPACK_INTEGER *
<a name="l00260"></a>00260                              lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_REAL *alpha, LAPACK_REAL *beta, LAPACK_COMPLEX *u, 
<a name="l00261"></a>00261                              LAPACK_INTEGER *ldu, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l00262"></a>00262                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l00263"></a>00263  
<a name="l00264"></a>00264 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cggsvp_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l00265"></a>00265                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER 
<a name="l00266"></a>00266                              *ldb, LAPACK_REAL *tola, LAPACK_REAL *tolb, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *u, 
<a name="l00267"></a>00267                              LAPACK_INTEGER *ldu, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l00268"></a>00268                              LAPACK_INTEGER *iwork, LAPACK_REAL *rwork, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *
<a name="l00269"></a>00269                              info);
<a name="l00270"></a>00270  
<a name="l00271"></a>00271 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgtcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_COMPLEX *dl, LAPACK_COMPLEX *
<a name="l00272"></a>00272                              d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *
<a name="l00273"></a>00273                              rcond, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00274"></a>00274  
<a name="l00275"></a>00275 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgtrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00276"></a>00276                              dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *dlf, LAPACK_COMPLEX *df, LAPACK_COMPLEX *
<a name="l00277"></a>00277                              duf, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *
<a name="l00278"></a>00278                              x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00279"></a>00279                              LAPACK_INTEGER *info);
<a name="l00280"></a>00280  
<a name="l00281"></a>00281 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgtsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *dl, LAPACK_COMPLEX *
<a name="l00282"></a>00282                             d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00283"></a>00283  
<a name="l00284"></a>00284 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgtsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00285"></a>00285                              nrhs, LAPACK_COMPLEX *dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *dlf, LAPACK_COMPLEX *
<a name="l00286"></a>00286                              df, LAPACK_COMPLEX *duf, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *
<a name="l00287"></a>00287                              ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00288"></a>00288                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00289"></a>00289  
<a name="l00290"></a>00290 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgttrf_(LAPACK_INTEGER *n, LAPACK_COMPLEX *dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *
<a name="l00291"></a>00291                              du, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00292"></a>00292  
<a name="l00293"></a>00293 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgttrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00294"></a>00294                              dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *
<a name="l00295"></a>00295                              b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00296"></a>00296  
<a name="l00297"></a>00297 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cgtts2_(LAPACK_INTEGER *itrans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l00298"></a>00298                              LAPACK_COMPLEX *dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *du2, LAPACK_INTEGER *ipiv, 
<a name="l00299"></a>00299                              LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l00300"></a>00300  
<a name="l00301"></a>00301 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l00302"></a>00302                             LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00303"></a>00303                             LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00304"></a>00304  
<a name="l00305"></a>00305 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l00306"></a>00306                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00307"></a>00307                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *
<a name="l00308"></a>00308                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l00309"></a>00309  
<a name="l00310"></a>00310 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l00311"></a>00311                              LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l00312"></a>00312                              LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *
<a name="l00313"></a>00313                              m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00314"></a>00314                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l00315"></a>00315  
<a name="l00316"></a>00316 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbgst_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l00317"></a>00317                              LAPACK_INTEGER *kb, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *bb, LAPACK_INTEGER *ldbb, 
<a name="l00318"></a>00318                              LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00319"></a>00319  
<a name="l00320"></a>00320 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbgv_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l00321"></a>00321                             LAPACK_INTEGER *kb, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *bb, LAPACK_INTEGER *ldbb, 
<a name="l00322"></a>00322                             LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00323"></a>00323                             LAPACK_INTEGER *info);
<a name="l00324"></a>00324  
<a name="l00325"></a>00325 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbgvx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l00326"></a>00326                              LAPACK_INTEGER *ka, LAPACK_INTEGER *kb, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *bb, 
<a name="l00327"></a>00327                              LAPACK_INTEGER *ldbb, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *
<a name="l00328"></a>00328                              il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, 
<a name="l00329"></a>00329                              LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l00330"></a>00330                              ifail, LAPACK_INTEGER *info);
<a name="l00331"></a>00331  
<a name="l00332"></a>00332 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chbtrd_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l00333"></a>00333                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *q, LAPACK_INTEGER *
<a name="l00334"></a>00334                              ldq, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00335"></a>00335  
<a name="l00336"></a>00336 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> checon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00337"></a>00337                              LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_INTEGER *
<a name="l00338"></a>00338                              info);
<a name="l00339"></a>00339  
<a name="l00340"></a>00340 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cheev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00341"></a>00341                             LAPACK_INTEGER *lda, LAPACK_REAL *w, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, 
<a name="l00342"></a>00342                             LAPACK_INTEGER *info);
<a name="l00343"></a>00343  
<a name="l00344"></a>00344 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cheevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00345"></a>00345                              LAPACK_INTEGER *lda, LAPACK_REAL *w, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, 
<a name="l00346"></a>00346                              LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l00347"></a>00347  
<a name="l00348"></a>00348 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cheevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l00349"></a>00349                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *
<a name="l00350"></a>00350                              iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00351"></a>00351                              LAPACK_INTEGER *isuppz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *
<a name="l00352"></a>00352                              lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l00353"></a>00353  
<a name="l00354"></a>00354 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cheevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l00355"></a>00355                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *
<a name="l00356"></a>00356                              iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00357"></a>00357                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l00358"></a>00358                              ifail, LAPACK_INTEGER *info);
<a name="l00359"></a>00359  
<a name="l00360"></a>00360 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chegs2_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *
<a name="l00361"></a>00361                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00362"></a>00362  
<a name="l00363"></a>00363 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chegst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *
<a name="l00364"></a>00364                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00365"></a>00365  
<a name="l00366"></a>00366 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chegv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l00367"></a>00367                             n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_REAL *w, 
<a name="l00368"></a>00368                             LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00369"></a>00369  
<a name="l00370"></a>00370 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chegvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l00371"></a>00371                              n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_REAL *w, 
<a name="l00372"></a>00372                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *
<a name="l00373"></a>00373                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l00374"></a>00374  
<a name="l00375"></a>00375 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chegvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l00376"></a>00376                              uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l00377"></a>00377                              LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *
<a name="l00378"></a>00378                              m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l00379"></a>00379                              LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l00380"></a>00380  
<a name="l00381"></a>00381 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cherfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00382"></a>00382                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *
<a name="l00383"></a>00383                              b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00384"></a>00384                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00385"></a>00385  
<a name="l00386"></a>00386 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chesv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a,
<a name="l00387"></a>00387                             LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *work,
<a name="l00388"></a>00388                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00389"></a>00389  
<a name="l00390"></a>00390 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00391"></a>00391                              nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *
<a name="l00392"></a>00392                              ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond,
<a name="l00393"></a>00393                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, 
<a name="l00394"></a>00394                              LAPACK_INTEGER *info);
<a name="l00395"></a>00395  
<a name="l00396"></a>00396 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chetf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00397"></a>00397                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00398"></a>00398  
<a name="l00399"></a>00399 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chetrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00400"></a>00400                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l00401"></a>00401                              LAPACK_INTEGER *info);
<a name="l00402"></a>00402  
<a name="l00403"></a>00403 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chetrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00404"></a>00404                              LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00405"></a>00405  
<a name="l00406"></a>00406 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chetri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00407"></a>00407                              LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00408"></a>00408  
<a name="l00409"></a>00409 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chetrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00410"></a>00410                              a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l00411"></a>00411                              info);
<a name="l00412"></a>00412  
<a name="l00413"></a>00413 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chgeqz_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, 
<a name="l00414"></a>00414                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, 
<a name="l00415"></a>00415                              LAPACK_INTEGER *ldb, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq,
<a name="l00416"></a>00416                              LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *
<a name="l00417"></a>00417                              rwork, LAPACK_INTEGER *info);
<a name="l00418"></a>00418  
<a name="l00419"></a>00419 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00420"></a>00420                              ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00421"></a>00421  
<a name="l00422"></a>00422 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, 
<a name="l00423"></a>00423                             LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00424"></a>00424                             LAPACK_INTEGER *info);
<a name="l00425"></a>00425  
<a name="l00426"></a>00426 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, 
<a name="l00427"></a>00427                              LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l00428"></a>00428                              LAPACK_REAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, 
<a name="l00429"></a>00429                              LAPACK_INTEGER *info);
<a name="l00430"></a>00430  
<a name="l00431"></a>00431 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l00432"></a>00432                              LAPACK_COMPLEX *ap, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *
<a name="l00433"></a>00433                              abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *
<a name="l00434"></a>00434                              work, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l00435"></a>00435  
<a name="l00436"></a>00436 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpgst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *
<a name="l00437"></a>00437                              ap, LAPACK_COMPLEX *bp, LAPACK_INTEGER *info);
<a name="l00438"></a>00438  
<a name="l00439"></a>00439 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpgv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l00440"></a>00440                             n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *bp, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00441"></a>00441                             LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00442"></a>00442  
<a name="l00443"></a>00443 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpgvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l00444"></a>00444                              n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *bp, LAPACK_REAL *w, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00445"></a>00445                              LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *
<a name="l00446"></a>00446                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l00447"></a>00447  
<a name="l00448"></a>00448 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpgvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l00449"></a>00449                              uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *bp, LAPACK_REAL *vl, LAPACK_REAL *vu, 
<a name="l00450"></a>00450                              LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_COMPLEX *
<a name="l00451"></a>00451                              z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, 
<a name="l00452"></a>00452                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l00453"></a>00453  
<a name="l00454"></a>00454 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00455"></a>00455                              ap, LAPACK_COMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x,
<a name="l00456"></a>00456                              LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00457"></a>00457                              LAPACK_INTEGER *info);
<a name="l00458"></a>00458  
<a name="l00459"></a>00459 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00460"></a>00460                             ap, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00461"></a>00461  
<a name="l00462"></a>00462 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chpsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00463"></a>00463                              nrhs, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *
<a name="l00464"></a>00464                              ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00465"></a>00465                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00466"></a>00466  
<a name="l00467"></a>00467 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chptrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_REAL *d__, 
<a name="l00468"></a>00468                              LAPACK_REAL *e, LAPACK_COMPLEX *tau, LAPACK_INTEGER *info);
<a name="l00469"></a>00469  
<a name="l00470"></a>00470 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00471"></a>00471                              ipiv, LAPACK_INTEGER *info);
<a name="l00472"></a>00472  
<a name="l00473"></a>00473 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00474"></a>00474                              ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00475"></a>00475  
<a name="l00476"></a>00476 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00477"></a>00477                              ap, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00478"></a>00478  
<a name="l00479"></a>00479 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chsein_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *eigsrc, <span class="keywordtype">char</span> *initv, LAPACK_LOGICAL *
<a name="l00480"></a>00480                              select, LAPACK_INTEGER *n, LAPACK_COMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_COMPLEX *w, LAPACK_COMPLEX *
<a name="l00481"></a>00481                              vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *
<a name="l00482"></a>00482                              m, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *ifaill, LAPACK_INTEGER *ifailr, 
<a name="l00483"></a>00483                              LAPACK_INTEGER *info);
<a name="l00484"></a>00484  
<a name="l00485"></a>00485 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> chseqr_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo,
<a name="l00486"></a>00486                              LAPACK_INTEGER *ihi, LAPACK_COMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_COMPLEX *w, LAPACK_COMPLEX *z__, 
<a name="l00487"></a>00487                              LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00488"></a>00488  
<a name="l00489"></a>00489 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clabrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_COMPLEX *a, 
<a name="l00490"></a>00490                              LAPACK_INTEGER *lda, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_COMPLEX *tauq, LAPACK_COMPLEX *taup, 
<a name="l00491"></a>00491                              LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_COMPLEX *y, LAPACK_INTEGER *ldy);
<a name="l00492"></a>00492  
<a name="l00493"></a>00493 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacgv_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx);
<a name="l00494"></a>00494  
<a name="l00495"></a>00495 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacon_(LAPACK_INTEGER *n, LAPACK_COMPLEX *v, LAPACK_COMPLEX *x, LAPACK_REAL *est, 
<a name="l00496"></a>00496                              LAPACK_INTEGER *kase);
<a name="l00497"></a>00497  
<a name="l00498"></a>00498 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacp2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l00499"></a>00499                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l00500"></a>00500  
<a name="l00501"></a>00501 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacpy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l00502"></a>00502                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l00503"></a>00503  
<a name="l00504"></a>00504 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacrm_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00505"></a>00505                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *rwork);
<a name="l00506"></a>00506  
<a name="l00507"></a>00507 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clacrt_(LAPACK_INTEGER *n, LAPACK_COMPLEX *cx, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00508"></a>00508                              cy, LAPACK_INTEGER *incy, LAPACK_COMPLEX *c__, LAPACK_COMPLEX *s);
<a name="l00509"></a>00509  
<a name="l00510"></a>00510 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claed0_(LAPACK_INTEGER *qsiz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l00511"></a>00511                              LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *qstore, LAPACK_INTEGER *ldqs, LAPACK_REAL *rwork,
<a name="l00512"></a>00512                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l00513"></a>00513  
<a name="l00514"></a>00514 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claed7_(LAPACK_INTEGER *n, LAPACK_INTEGER *cutpnt, LAPACK_INTEGER *qsiz, 
<a name="l00515"></a>00515                              LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, LAPACK_INTEGER *curpbm, LAPACK_REAL *d__, LAPACK_COMPLEX *
<a name="l00516"></a>00516                              q, LAPACK_INTEGER *ldq, LAPACK_REAL *rho, LAPACK_INTEGER *indxq, LAPACK_REAL *qstore, LAPACK_INTEGER *
<a name="l00517"></a>00517                              qptr, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *
<a name="l00518"></a>00518                              givcol, LAPACK_REAL *givnum, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *iwork, 
<a name="l00519"></a>00519                              LAPACK_INTEGER *info);
<a name="l00520"></a>00520  
<a name="l00521"></a>00521 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claed8_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *qsiz, LAPACK_COMPLEX *
<a name="l00522"></a>00522                              q, LAPACK_INTEGER *ldq, LAPACK_REAL *d__, LAPACK_REAL *rho, LAPACK_INTEGER *cutpnt, LAPACK_REAL *z__, 
<a name="l00523"></a>00523                              LAPACK_REAL *dlamda, LAPACK_COMPLEX *q2, LAPACK_INTEGER *ldq2, LAPACK_REAL *w, LAPACK_INTEGER *indxp, 
<a name="l00524"></a>00524                              LAPACK_INTEGER *indx, LAPACK_INTEGER *indxq, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, 
<a name="l00525"></a>00525                              LAPACK_INTEGER *givcol, LAPACK_REAL *givnum, LAPACK_INTEGER *info);
<a name="l00526"></a>00526  
<a name="l00527"></a>00527 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claein_(LAPACK_LOGICAL *rightv, LAPACK_LOGICAL *noinit, LAPACK_INTEGER *n, 
<a name="l00528"></a>00528                              LAPACK_COMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_COMPLEX *w, LAPACK_COMPLEX *v, LAPACK_COMPLEX *b, 
<a name="l00529"></a>00529                              LAPACK_INTEGER *ldb, LAPACK_REAL *rwork, LAPACK_REAL *eps3, LAPACK_REAL *smlnum, LAPACK_INTEGER *info);
<a name="l00530"></a>00530  
<a name="l00531"></a>00531 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claesy_(LAPACK_COMPLEX *a, LAPACK_COMPLEX *b, LAPACK_COMPLEX *c__, LAPACK_COMPLEX *
<a name="l00532"></a>00532                              rt1, LAPACK_COMPLEX *rt2, LAPACK_COMPLEX *evscal, LAPACK_COMPLEX *cs1, LAPACK_COMPLEX *sn1);
<a name="l00533"></a>00533  
<a name="l00534"></a>00534 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claev2_(LAPACK_COMPLEX *a, LAPACK_COMPLEX *b, LAPACK_COMPLEX *c__, LAPACK_REAL *rt1, 
<a name="l00535"></a>00535                              LAPACK_REAL *rt2, LAPACK_REAL *cs1, LAPACK_COMPLEX *sn1);
<a name="l00536"></a>00536  
<a name="l00537"></a>00537 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clags2_(LAPACK_LOGICAL *upper, LAPACK_REAL *a1, LAPACK_COMPLEX *a2, LAPACK_REAL *a3, 
<a name="l00538"></a>00538                              LAPACK_REAL *b1, LAPACK_COMPLEX *b2, LAPACK_REAL *b3, LAPACK_REAL *csu, LAPACK_COMPLEX *snu, LAPACK_REAL *csv, 
<a name="l00539"></a>00539                              LAPACK_COMPLEX *snv, LAPACK_REAL *csq, LAPACK_COMPLEX *snq);
<a name="l00540"></a>00540  
<a name="l00541"></a>00541 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clagtm_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *
<a name="l00542"></a>00542                              alpha, LAPACK_COMPLEX *dl, LAPACK_COMPLEX *d__, LAPACK_COMPLEX *du, LAPACK_COMPLEX *x, LAPACK_INTEGER *
<a name="l00543"></a>00543                              ldx, LAPACK_REAL *beta, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l00544"></a>00544  
<a name="l00545"></a>00545 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clahef_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l00546"></a>00546                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *w, LAPACK_INTEGER *ldw, 
<a name="l00547"></a>00547                              LAPACK_INTEGER *info);
<a name="l00548"></a>00548  
<a name="l00549"></a>00549 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clahqr_(LAPACK_LOGICAL *wantt, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l00550"></a>00550                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_COMPLEX *w, 
<a name="l00551"></a>00551                              LAPACK_INTEGER *iloz, LAPACK_INTEGER *ihiz, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *
<a name="l00552"></a>00552                              info);
<a name="l00553"></a>00553  
<a name="l00554"></a>00554 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clahrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *nb, LAPACK_COMPLEX *a, 
<a name="l00555"></a>00555                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *y, 
<a name="l00556"></a>00556                              LAPACK_INTEGER *ldy);
<a name="l00557"></a>00557  
<a name="l00558"></a>00558 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claic1_(LAPACK_INTEGER *job, LAPACK_INTEGER *j, LAPACK_COMPLEX *x, LAPACK_REAL *sest,
<a name="l00559"></a>00559                              LAPACK_COMPLEX *w, LAPACK_COMPLEX *gamma, LAPACK_REAL *sestpr, LAPACK_COMPLEX *s, LAPACK_COMPLEX *c__);
<a name="l00560"></a>00560  
<a name="l00561"></a>00561 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clals0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l00562"></a>00562                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *bx, 
<a name="l00563"></a>00563                              LAPACK_INTEGER *ldbx, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l00564"></a>00564                              LAPACK_INTEGER *ldgcol, LAPACK_REAL *givnum, LAPACK_INTEGER *ldgnum, LAPACK_REAL *poles, LAPACK_REAL *
<a name="l00565"></a>00565                              difl, LAPACK_REAL *difr, LAPACK_REAL *z__, LAPACK_INTEGER *k, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *
<a name="l00566"></a>00566                              rwork, LAPACK_INTEGER *info);
<a name="l00567"></a>00567  
<a name="l00568"></a>00568 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clalsa_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l00569"></a>00569                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *bx, LAPACK_INTEGER *ldbx, 
<a name="l00570"></a>00570                              LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, LAPACK_INTEGER *k, LAPACK_REAL *difl, LAPACK_REAL *difr, 
<a name="l00571"></a>00571                              LAPACK_REAL *z__, LAPACK_REAL *poles, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *
<a name="l00572"></a>00572                              ldgcol, LAPACK_INTEGER *perm, LAPACK_REAL *givnum, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *rwork, 
<a name="l00573"></a>00573                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l00574"></a>00574  
<a name="l00575"></a>00575 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clapll_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00576"></a>00576                              y, LAPACK_INTEGER *incy, LAPACK_REAL *ssmin);
<a name="l00577"></a>00577  
<a name="l00578"></a>00578 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clapmt_(LAPACK_LOGICAL *forwrd, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX 
<a name="l00579"></a>00579                              *x, LAPACK_INTEGER *ldx, LAPACK_INTEGER *k);
<a name="l00580"></a>00580  
<a name="l00581"></a>00581 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqgb_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l00582"></a>00582                              LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL 
<a name="l00583"></a>00583                              *colcnd, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00584"></a>00584  
<a name="l00585"></a>00585 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqge_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00586"></a>00586                              LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *colcnd, LAPACK_REAL *amax, <span class="keywordtype">char</span> *
<a name="l00587"></a>00587                              equed);
<a name="l00588"></a>00588  
<a name="l00589"></a>00589 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqhb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00590"></a>00590                              LAPACK_INTEGER *ldab, LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00591"></a>00591  
<a name="l00592"></a>00592 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqhe_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00593"></a>00593                              LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00594"></a>00594  
<a name="l00595"></a>00595 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqhp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_REAL *s, 
<a name="l00596"></a>00596                              LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00597"></a>00597  
<a name="l00598"></a>00598 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqp2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_COMPLEX 
<a name="l00599"></a>00599                              *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_COMPLEX *tau, LAPACK_REAL *vn1, LAPACK_REAL *vn2, 
<a name="l00600"></a>00600                              LAPACK_COMPLEX *work);
<a name="l00601"></a>00601  
<a name="l00602"></a>00602 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqps_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_INTEGER 
<a name="l00603"></a>00603                              *nb, LAPACK_INTEGER *kb, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_COMPLEX *
<a name="l00604"></a>00604                              tau, LAPACK_REAL *vn1, LAPACK_REAL *vn2, LAPACK_COMPLEX *auxv, LAPACK_COMPLEX *f, LAPACK_INTEGER *ldf);
<a name="l00605"></a>00605  
<a name="l00606"></a>00606 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqsb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00607"></a>00607                              LAPACK_INTEGER *ldab, LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00608"></a>00608  
<a name="l00609"></a>00609 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqsp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_REAL *s, 
<a name="l00610"></a>00610                              LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00611"></a>00611  
<a name="l00612"></a>00612 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claqsy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00613"></a>00613                              LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l00614"></a>00614  
<a name="l00615"></a>00615 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clar1v_(LAPACK_INTEGER *n, LAPACK_INTEGER *b1, LAPACK_INTEGER *bn, LAPACK_REAL *
<a name="l00616"></a>00616                              sigma, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_REAL *ld, LAPACK_REAL *lld, LAPACK_REAL *gersch, LAPACK_COMPLEX 
<a name="l00617"></a>00617                              *z__, LAPACK_REAL *ztz, LAPACK_REAL *mingma, LAPACK_INTEGER *r__, LAPACK_INTEGER *isuppz, LAPACK_REAL *
<a name="l00618"></a>00618                              work);
<a name="l00619"></a>00619  
<a name="l00620"></a>00620 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clar2v_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_COMPLEX *y, LAPACK_COMPLEX *z__,
<a name="l00621"></a>00621                              LAPACK_INTEGER *incx, LAPACK_REAL *c__, LAPACK_COMPLEX *s, LAPACK_INTEGER *incc);
<a name="l00622"></a>00622  
<a name="l00623"></a>00623 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarcm_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l00624"></a>00624                              LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *rwork);
<a name="l00625"></a>00625  
<a name="l00626"></a>00626 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarf_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *v, 
<a name="l00627"></a>00627                             LAPACK_INTEGER *incv, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *
<a name="l00628"></a>00628                             work);
<a name="l00629"></a>00629  
<a name="l00630"></a>00630 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarfb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l00631"></a>00631                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, 
<a name="l00632"></a>00632                              LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, 
<a name="l00633"></a>00633                              LAPACK_INTEGER *ldwork);
<a name="l00634"></a>00634  
<a name="l00635"></a>00635 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarfg_(LAPACK_INTEGER *n, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *x, LAPACK_INTEGER *
<a name="l00636"></a>00636                              incx, LAPACK_COMPLEX *tau);
<a name="l00637"></a>00637  
<a name="l00638"></a>00638 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarft_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00639"></a>00639                              k, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt);
<a name="l00640"></a>00640  
<a name="l00641"></a>00641 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarfx_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *v, 
<a name="l00642"></a>00642                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work);
<a name="l00643"></a>00643  
<a name="l00644"></a>00644 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clargv_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00645"></a>00645                              y, LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_INTEGER *incc);
<a name="l00646"></a>00646  
<a name="l00647"></a>00647 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarnv_(LAPACK_INTEGER *idist, LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, 
<a name="l00648"></a>00648                              LAPACK_COMPLEX *x);
<a name="l00649"></a>00649  
<a name="l00650"></a>00650 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarrv_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_INTEGER *isplit, 
<a name="l00651"></a>00651                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_INTEGER *iblock, LAPACK_REAL *gersch, LAPACK_REAL *tol, 
<a name="l00652"></a>00652                              LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l00653"></a>00653                              iwork, LAPACK_INTEGER *info);
<a name="l00654"></a>00654  
<a name="l00655"></a>00655 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clartg_(LAPACK_COMPLEX *f, LAPACK_COMPLEX *g, LAPACK_REAL *cs, LAPACK_COMPLEX *sn, 
<a name="l00656"></a>00656                              LAPACK_COMPLEX *r__);
<a name="l00657"></a>00657  
<a name="l00658"></a>00658 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clartv_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00659"></a>00659                              y, LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_COMPLEX *s, LAPACK_INTEGER *incc);
<a name="l00660"></a>00660  
<a name="l00661"></a>00661 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarz_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, 
<a name="l00662"></a>00662                             LAPACK_COMPLEX *v, LAPACK_INTEGER *incv, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, 
<a name="l00663"></a>00663                             LAPACK_COMPLEX *work);
<a name="l00664"></a>00664  
<a name="l00665"></a>00665 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarzb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l00666"></a>00666                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *v, 
<a name="l00667"></a>00667                              LAPACK_INTEGER *ldv, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, 
<a name="l00668"></a>00668                              LAPACK_COMPLEX *work, LAPACK_INTEGER *ldwork);
<a name="l00669"></a>00669  
<a name="l00670"></a>00670 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clarzt_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00671"></a>00671                              k, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt);
<a name="l00672"></a>00672  
<a name="l00673"></a>00673 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clascl_(<span class="keywordtype">char</span> *type__, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_REAL *
<a name="l00674"></a>00674                              cfrom, LAPACK_REAL *cto, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l00675"></a>00675                              LAPACK_INTEGER *info);
<a name="l00676"></a>00676  
<a name="l00677"></a>00677 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claset_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *
<a name="l00678"></a>00678                              alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda);
<a name="l00679"></a>00679  
<a name="l00680"></a>00680 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clasr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *pivot, <span class="keywordtype">char</span> *direct, LAPACK_INTEGER *m,
<a name="l00681"></a>00681                             LAPACK_INTEGER *n, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda);
<a name="l00682"></a>00682  
<a name="l00683"></a>00683 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> classq_(LAPACK_INTEGER *n, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_REAL *
<a name="l00684"></a>00684                              scale, LAPACK_REAL *sumsq);
<a name="l00685"></a>00685  
<a name="l00686"></a>00686 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> claswp_(LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *
<a name="l00687"></a>00687                              k1, LAPACK_INTEGER *k2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *incx);
<a name="l00688"></a>00688  
<a name="l00689"></a>00689 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clasyf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l00690"></a>00690                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *w, LAPACK_INTEGER *ldw, 
<a name="l00691"></a>00691                              LAPACK_INTEGER *info);
<a name="l00692"></a>00692  
<a name="l00693"></a>00693 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatbs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l00694"></a>00694                              normin, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *
<a name="l00695"></a>00695                              x, LAPACK_REAL *scale, LAPACK_REAL *cnorm, LAPACK_INTEGER *info);
<a name="l00696"></a>00696  
<a name="l00697"></a>00697 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatdf_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *n, LAPACK_COMPLEX *z__, LAPACK_INTEGER 
<a name="l00698"></a>00698                              *ldz, LAPACK_COMPLEX *rhs, LAPACK_REAL *rdsum, LAPACK_REAL *rdscal, LAPACK_INTEGER *ipiv, LAPACK_INTEGER 
<a name="l00699"></a>00699                              *jpiv);
<a name="l00700"></a>00700  
<a name="l00701"></a>00701 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatps_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l00702"></a>00702                              normin, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *x, LAPACK_REAL *scale, LAPACK_REAL *cnorm,
<a name="l00703"></a>00703                              LAPACK_INTEGER *info);
<a name="l00704"></a>00704  
<a name="l00705"></a>00705 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_COMPLEX *a, 
<a name="l00706"></a>00706                              LAPACK_INTEGER *lda, LAPACK_REAL *e, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *w, LAPACK_INTEGER *ldw);
<a name="l00707"></a>00707  
<a name="l00708"></a>00708 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l00709"></a>00709                              normin, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *x, LAPACK_REAL *scale,
<a name="l00710"></a>00710                              LAPACK_REAL *cnorm, LAPACK_INTEGER *info);
<a name="l00711"></a>00711  
<a name="l00712"></a>00712 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatrz_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, LAPACK_COMPLEX *a, 
<a name="l00713"></a>00713                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work);
<a name="l00714"></a>00714  
<a name="l00715"></a>00715 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clatzm_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *v, 
<a name="l00716"></a>00716                              LAPACK_INTEGER *incv, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c1, LAPACK_COMPLEX *c2, LAPACK_INTEGER *ldc, 
<a name="l00717"></a>00717                              LAPACK_COMPLEX *work);
<a name="l00718"></a>00718  
<a name="l00719"></a>00719 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clauu2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00720"></a>00720                              LAPACK_INTEGER *info);
<a name="l00721"></a>00721  
<a name="l00722"></a>00722 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> clauum_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00723"></a>00723                              LAPACK_INTEGER *info);
<a name="l00724"></a>00724  
<a name="l00725"></a>00725 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00726"></a>00726                              LAPACK_INTEGER *ldab, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00727"></a>00727                              LAPACK_INTEGER *info);
<a name="l00728"></a>00728  
<a name="l00729"></a>00729 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00730"></a>00730                              LAPACK_INTEGER *ldab, LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l00731"></a>00731  
<a name="l00732"></a>00732 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l00733"></a>00733                              nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *afb, LAPACK_INTEGER *ldafb, 
<a name="l00734"></a>00734                              LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *
<a name="l00735"></a>00735                              berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00736"></a>00736  
<a name="l00737"></a>00737 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbstf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00738"></a>00738                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l00739"></a>00739  
<a name="l00740"></a>00740 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l00741"></a>00741                             nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l00742"></a>00742                             info);
<a name="l00743"></a>00743  
<a name="l00744"></a>00744 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l00745"></a>00745                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *afb, LAPACK_INTEGER *
<a name="l00746"></a>00746                              ldafb, <span class="keywordtype">char</span> *equed, LAPACK_REAL *s, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, 
<a name="l00747"></a>00747                              LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, 
<a name="l00748"></a>00748                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00749"></a>00749  
<a name="l00750"></a>00750 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbtf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00751"></a>00751                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l00752"></a>00752  
<a name="l00753"></a>00753 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbtrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab,
<a name="l00754"></a>00754                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l00755"></a>00755  
<a name="l00756"></a>00756 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpbtrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l00757"></a>00757                              nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l00758"></a>00758                              info);
<a name="l00759"></a>00759  
<a name="l00760"></a>00760 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpocon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00761"></a>00761                              LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00762"></a>00762  
<a name="l00763"></a>00763 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpoequ_(LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_REAL *s, 
<a name="l00764"></a>00764                              LAPACK_REAL *scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l00765"></a>00765  
<a name="l00766"></a>00766 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cporfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00767"></a>00767                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb,
<a name="l00768"></a>00768                              LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, 
<a name="l00769"></a>00769                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00770"></a>00770  
<a name="l00771"></a>00771 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cposv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a,
<a name="l00772"></a>00772                             LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00773"></a>00773  
<a name="l00774"></a>00774 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cposvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00775"></a>00775                              nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, <span class="keywordtype">char</span> *
<a name="l00776"></a>00776                              equed, LAPACK_REAL *s, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l00777"></a>00777                              LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00778"></a>00778                              LAPACK_INTEGER *info);
<a name="l00779"></a>00779  
<a name="l00780"></a>00780 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpotf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00781"></a>00781                              LAPACK_INTEGER *info);
<a name="l00782"></a>00782  
<a name="l00783"></a>00783 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpotrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00784"></a>00784                              LAPACK_INTEGER *info);
<a name="l00785"></a>00785  
<a name="l00786"></a>00786 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpotri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00787"></a>00787                              LAPACK_INTEGER *info);
<a name="l00788"></a>00788  
<a name="l00789"></a>00789 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpotrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00790"></a>00790                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00791"></a>00791  
<a name="l00792"></a>00792 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cppcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_REAL *anorm,
<a name="l00793"></a>00793                              LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00794"></a>00794  
<a name="l00795"></a>00795 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cppequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_REAL *s, 
<a name="l00796"></a>00796                              LAPACK_REAL *scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l00797"></a>00797  
<a name="l00798"></a>00798 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00799"></a>00799                              ap, LAPACK_COMPLEX *afp, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l00800"></a>00800                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00801"></a>00801  
<a name="l00802"></a>00802 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cppsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00803"></a>00803                             ap, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00804"></a>00804  
<a name="l00805"></a>00805 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cppsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00806"></a>00806                              nrhs, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *afp, <span class="keywordtype">char</span> *equed, LAPACK_REAL *s, LAPACK_COMPLEX *b, 
<a name="l00807"></a>00807                              LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL 
<a name="l00808"></a>00808                              *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00809"></a>00809  
<a name="l00810"></a>00810 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00811"></a>00811                              info);
<a name="l00812"></a>00812  
<a name="l00813"></a>00813 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00814"></a>00814                              info);
<a name="l00815"></a>00815  
<a name="l00816"></a>00816 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00817"></a>00817                              ap, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00818"></a>00818  
<a name="l00819"></a>00819 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cptcon_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_COMPLEX *e, LAPACK_REAL *anorm, 
<a name="l00820"></a>00820                              LAPACK_REAL *rcond, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00821"></a>00821  
<a name="l00822"></a>00822 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cptrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__,
<a name="l00823"></a>00823                              LAPACK_COMPLEX *e, LAPACK_REAL *df, LAPACK_COMPLEX *ef, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX 
<a name="l00824"></a>00824                              *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00825"></a>00825                              LAPACK_INTEGER *info);
<a name="l00826"></a>00826  
<a name="l00827"></a>00827 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cptsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__, LAPACK_COMPLEX *e, 
<a name="l00828"></a>00828                             LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00829"></a>00829  
<a name="l00830"></a>00830 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cptsvx_(<span class="keywordtype">char</span> *fact, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__,
<a name="l00831"></a>00831                              LAPACK_COMPLEX *e, LAPACK_REAL *df, LAPACK_COMPLEX *ef, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX 
<a name="l00832"></a>00832                              *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, 
<a name="l00833"></a>00833                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00834"></a>00834  
<a name="l00835"></a>00835 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpttrf_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_COMPLEX *e, LAPACK_INTEGER *info);
<a name="l00836"></a>00836  
<a name="l00837"></a>00837 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cpttrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__,
<a name="l00838"></a>00838                              LAPACK_COMPLEX *e, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00839"></a>00839  
<a name="l00840"></a>00840 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cptts2_(LAPACK_INTEGER *iuplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *
<a name="l00841"></a>00841                              d__, LAPACK_COMPLEX *e, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l00842"></a>00842  
<a name="l00843"></a>00843 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> crot_(LAPACK_INTEGER *n, LAPACK_COMPLEX *cx, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00844"></a>00844                            cy, LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_COMPLEX *s);
<a name="l00845"></a>00845  
<a name="l00846"></a>00846 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cspcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00847"></a>00847                              ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00848"></a>00848  
<a name="l00849"></a>00849 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cspmv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *
<a name="l00850"></a>00850                             ap, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *y, LAPACK_INTEGER *
<a name="l00851"></a>00851                             incy);
<a name="l00852"></a>00852  
<a name="l00853"></a>00853 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cspr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *x,
<a name="l00854"></a>00854                            LAPACK_INTEGER *incx, LAPACK_COMPLEX *ap);
<a name="l00855"></a>00855  
<a name="l00856"></a>00856 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00857"></a>00857                              ap, LAPACK_COMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x,
<a name="l00858"></a>00858                              LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00859"></a>00859                              LAPACK_INTEGER *info);
<a name="l00860"></a>00860  
<a name="l00861"></a>00861 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cspsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00862"></a>00862                             ap, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00863"></a>00863  
<a name="l00864"></a>00864 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cspsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00865"></a>00865                              nrhs, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *
<a name="l00866"></a>00866                              ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00867"></a>00867                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00868"></a>00868  
<a name="l00869"></a>00869 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00870"></a>00870                              ipiv, LAPACK_INTEGER *info);
<a name="l00871"></a>00871  
<a name="l00872"></a>00872 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_INTEGER *
<a name="l00873"></a>00873                              ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00874"></a>00874  
<a name="l00875"></a>00875 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00876"></a>00876                              ap, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00877"></a>00877  
<a name="l00878"></a>00878 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csrot_(LAPACK_INTEGER *n, LAPACK_COMPLEX *cx, LAPACK_INTEGER *incx, LAPACK_COMPLEX *
<a name="l00879"></a>00879                             cy, LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_REAL *s);
<a name="l00880"></a>00880  
<a name="l00881"></a>00881 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csrscl_(LAPACK_INTEGER *n, LAPACK_REAL *sa, LAPACK_COMPLEX *sx, LAPACK_INTEGER *incx);
<a name="l00882"></a>00882  
<a name="l00883"></a>00883 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cstedc_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l00884"></a>00884                              LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *
<a name="l00885"></a>00885                              rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *
<a name="l00886"></a>00886                              info);
<a name="l00887"></a>00887  
<a name="l00888"></a>00888 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cstein_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_INTEGER *m, LAPACK_REAL 
<a name="l00889"></a>00889                              *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *isplit, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l00890"></a>00890                              LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l00891"></a>00891  
<a name="l00892"></a>00892 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l00893"></a>00893                              LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l00894"></a>00894  
<a name="l00895"></a>00895 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csycon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00896"></a>00896                              LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_INTEGER *
<a name="l00897"></a>00897                              info);
<a name="l00898"></a>00898  
<a name="l00899"></a>00899 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csymv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *
<a name="l00900"></a>00900                             a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *x, LAPACK_INTEGER *incx, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *y,
<a name="l00901"></a>00901                             LAPACK_INTEGER *incy);
<a name="l00902"></a>00902  
<a name="l00903"></a>00903 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csyr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *x,
<a name="l00904"></a>00904                            LAPACK_INTEGER *incx, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda);
<a name="l00905"></a>00905  
<a name="l00906"></a>00906 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csyrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00907"></a>00907                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *
<a name="l00908"></a>00908                              b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00909"></a>00909                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00910"></a>00910  
<a name="l00911"></a>00911 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csysv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a,
<a name="l00912"></a>00912                             LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *work,
<a name="l00913"></a>00913                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00914"></a>00914  
<a name="l00915"></a>00915 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csysvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l00916"></a>00916                              nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *
<a name="l00917"></a>00917                              ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond,
<a name="l00918"></a>00918                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_REAL *rwork, 
<a name="l00919"></a>00919                              LAPACK_INTEGER *info);
<a name="l00920"></a>00920  
<a name="l00921"></a>00921 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csytf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00922"></a>00922                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l00923"></a>00923  
<a name="l00924"></a>00924 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csytrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00925"></a>00925                              LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l00926"></a>00926  
<a name="l00927"></a>00927 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csytri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l00928"></a>00928                              LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l00929"></a>00929  
<a name="l00930"></a>00930 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> csytrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *
<a name="l00931"></a>00931                              a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l00932"></a>00932                              info);
<a name="l00933"></a>00933  
<a name="l00934"></a>00934 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctbcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l00935"></a>00935                              LAPACK_INTEGER *kd, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, 
<a name="l00936"></a>00936                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00937"></a>00937  
<a name="l00938"></a>00938 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctbrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l00939"></a>00939                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *b, 
<a name="l00940"></a>00940                              LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, 
<a name="l00941"></a>00941                              LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00942"></a>00942  
<a name="l00943"></a>00943 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctbtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l00944"></a>00944                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_COMPLEX *b, 
<a name="l00945"></a>00945                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l00946"></a>00946  
<a name="l00947"></a>00947 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l00948"></a>00948                              LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l00949"></a>00949                              LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, 
<a name="l00950"></a>00950                              LAPACK_INTEGER *m, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00951"></a>00951  
<a name="l00952"></a>00952 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgex2_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l00953"></a>00953                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *q, 
<a name="l00954"></a>00954                              LAPACK_INTEGER *ldq, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *j1, LAPACK_INTEGER *info);
<a name="l00955"></a>00955  
<a name="l00956"></a>00956 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgexc_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l00957"></a>00957                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *q, 
<a name="l00958"></a>00958                              LAPACK_INTEGER *ldq, LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *ifst, LAPACK_INTEGER *
<a name="l00959"></a>00959                              ilst, LAPACK_INTEGER *info);
<a name="l00960"></a>00960  
<a name="l00961"></a>00961 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgsen_(LAPACK_INTEGER *ijob, LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, 
<a name="l00962"></a>00962                              LAPACK_LOGICAL *select, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, 
<a name="l00963"></a>00963                              LAPACK_INTEGER *ldb, LAPACK_COMPLEX *alpha, LAPACK_COMPLEX *beta, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq,
<a name="l00964"></a>00964                              LAPACK_COMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *m, LAPACK_REAL *pl, LAPACK_REAL *pr, LAPACK_REAL *
<a name="l00965"></a>00965                              dif, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, 
<a name="l00966"></a>00966                              LAPACK_INTEGER *info);
<a name="l00967"></a>00967  
<a name="l00968"></a>00968 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgsja_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l00969"></a>00969                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *a, LAPACK_INTEGER *
<a name="l00970"></a>00970                              lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_REAL *tola, LAPACK_REAL *tolb, LAPACK_REAL *alpha, 
<a name="l00971"></a>00971                              LAPACK_REAL *beta, LAPACK_COMPLEX *u, LAPACK_INTEGER *ldu, LAPACK_COMPLEX *v, LAPACK_INTEGER *ldv, 
<a name="l00972"></a>00972                              LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *work, LAPACK_INTEGER *ncycle, LAPACK_INTEGER *
<a name="l00973"></a>00973                              info);
<a name="l00974"></a>00974  
<a name="l00975"></a>00975 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l00976"></a>00976                              LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l00977"></a>00977                              LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_REAL *s, LAPACK_REAL 
<a name="l00978"></a>00978                              *dif, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l00979"></a>00979                              *iwork, LAPACK_INTEGER *info);
<a name="l00980"></a>00980  
<a name="l00981"></a>00981 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgsy2_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l00982"></a>00982                              n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *c__, 
<a name="l00983"></a>00983                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *d__, LAPACK_INTEGER *ldd, LAPACK_COMPLEX *e, LAPACK_INTEGER *lde, 
<a name="l00984"></a>00984                              LAPACK_COMPLEX *f, LAPACK_INTEGER *ldf, LAPACK_REAL *scale, LAPACK_REAL *rdsum, LAPACK_REAL *rdscal, 
<a name="l00985"></a>00985                              LAPACK_INTEGER *info);
<a name="l00986"></a>00986  
<a name="l00987"></a>00987 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctgsyl_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l00988"></a>00988                              n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *c__, 
<a name="l00989"></a>00989                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *d__, LAPACK_INTEGER *ldd, LAPACK_COMPLEX *e, LAPACK_INTEGER *lde, 
<a name="l00990"></a>00990                              LAPACK_COMPLEX *f, LAPACK_INTEGER *ldf, LAPACK_REAL *scale, LAPACK_REAL *dif, LAPACK_COMPLEX *work, 
<a name="l00991"></a>00991                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l00992"></a>00992  
<a name="l00993"></a>00993 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctpcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l00994"></a>00994                              LAPACK_COMPLEX *ap, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l00995"></a>00995  
<a name="l00996"></a>00996 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctprfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l00997"></a>00997                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_COMPLEX *x, 
<a name="l00998"></a>00998                              LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l00999"></a>00999                              LAPACK_INTEGER *info);
<a name="l01000"></a>01000  
<a name="l01001"></a>01001 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctptri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, 
<a name="l01002"></a>01002                              LAPACK_INTEGER *info);
<a name="l01003"></a>01003  
<a name="l01004"></a>01004 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctptrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l01005"></a>01005                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l01006"></a>01006  
<a name="l01007"></a>01007 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l01008"></a>01008                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_REAL *rcond, LAPACK_COMPLEX *work, LAPACK_REAL *rwork, 
<a name="l01009"></a>01009                              LAPACK_INTEGER *info);
<a name="l01010"></a>01010  
<a name="l01011"></a>01011 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l01012"></a>01012                              LAPACK_INTEGER *n, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, 
<a name="l01013"></a>01013                              LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_COMPLEX *work, 
<a name="l01014"></a>01014                              LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l01015"></a>01015  
<a name="l01016"></a>01016 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrexc_(<span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_COMPLEX *t, LAPACK_INTEGER *
<a name="l01017"></a>01017                              ldt, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *ifst, LAPACK_INTEGER *ilst, LAPACK_INTEGER *
<a name="l01018"></a>01018                              info);
<a name="l01019"></a>01019  
<a name="l01020"></a>01020 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l01021"></a>01021                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l01022"></a>01022                              LAPACK_COMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_COMPLEX *work, LAPACK_REAL 
<a name="l01023"></a>01023                              *rwork, LAPACK_INTEGER *info);
<a name="l01024"></a>01024  
<a name="l01025"></a>01025 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrsen_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, LAPACK_LOGICAL *select, LAPACK_INTEGER 
<a name="l01026"></a>01026                              *n, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *w, 
<a name="l01027"></a>01027                              LAPACK_INTEGER *m, LAPACK_REAL *s, LAPACK_REAL *sep, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l01028"></a>01028                              LAPACK_INTEGER *info);
<a name="l01029"></a>01029  
<a name="l01030"></a>01030 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l01031"></a>01031                              LAPACK_INTEGER *n, LAPACK_COMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_COMPLEX *vl, LAPACK_INTEGER *ldvl, 
<a name="l01032"></a>01032                              LAPACK_COMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_REAL *s, LAPACK_REAL *sep, LAPACK_INTEGER *mm, LAPACK_INTEGER *
<a name="l01033"></a>01033                              m, LAPACK_COMPLEX *work, LAPACK_INTEGER *ldwork, LAPACK_REAL *rwork, LAPACK_INTEGER *info);
<a name="l01034"></a>01034  
<a name="l01035"></a>01035 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrsyl_(<span class="keywordtype">char</span> *trana, <span class="keywordtype">char</span> *tranb, LAPACK_INTEGER *isgn, LAPACK_INTEGER 
<a name="l01036"></a>01036                              *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l01037"></a>01037                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *scale, LAPACK_INTEGER *info);
<a name="l01038"></a>01038  
<a name="l01039"></a>01039 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrti2_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l01040"></a>01040                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l01041"></a>01041  
<a name="l01042"></a>01042 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrtri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, 
<a name="l01043"></a>01043                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l01044"></a>01044  
<a name="l01045"></a>01045 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctrtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l01046"></a>01046                              LAPACK_INTEGER *nrhs, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l01047"></a>01047                              LAPACK_INTEGER *info);
<a name="l01048"></a>01048  
<a name="l01049"></a>01049 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctzrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l01050"></a>01050                              LAPACK_COMPLEX *tau, LAPACK_INTEGER *info);
<a name="l01051"></a>01051  
<a name="l01052"></a>01052 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ctzrzf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l01053"></a>01053                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01054"></a>01054  
<a name="l01055"></a>01055 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cung2l_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01056"></a>01056                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01057"></a>01057  
<a name="l01058"></a>01058 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cung2r_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01059"></a>01059                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01060"></a>01060  
<a name="l01061"></a>01061 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungbr_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l01062"></a>01062                              LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l01063"></a>01063                              LAPACK_INTEGER *info);
<a name="l01064"></a>01064  
<a name="l01065"></a>01065 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunghr_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *
<a name="l01066"></a>01066                              a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l01067"></a>01067                              *info);
<a name="l01068"></a>01068  
<a name="l01069"></a>01069 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungl2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01070"></a>01070                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01071"></a>01071  
<a name="l01072"></a>01072 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunglq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01073"></a>01073                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01074"></a>01074                              info);
<a name="l01075"></a>01075  
<a name="l01076"></a>01076 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungql_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01077"></a>01077                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01078"></a>01078                              info);
<a name="l01079"></a>01079  
<a name="l01080"></a>01080 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungqr_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01081"></a>01081                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01082"></a>01082                              info);
<a name="l01083"></a>01083  
<a name="l01084"></a>01084 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01085"></a>01085                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01086"></a>01086  
<a name="l01087"></a>01087 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungrq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, 
<a name="l01088"></a>01088                              LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01089"></a>01089                              info);
<a name="l01090"></a>01090  
<a name="l01091"></a>01091 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cungtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda,
<a name="l01092"></a>01092                              LAPACK_COMPLEX *tau, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01093"></a>01093  
<a name="l01094"></a>01094 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunm2l_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01095"></a>01095                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01096"></a>01096                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01097"></a>01097  
<a name="l01098"></a>01098 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunm2r_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01099"></a>01099                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01100"></a>01100                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01101"></a>01101  
<a name="l01102"></a>01102 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmbr_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l01103"></a>01103                              LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, 
<a name="l01104"></a>01104                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01105"></a>01105                              info);
<a name="l01106"></a>01106  
<a name="l01107"></a>01107 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmhr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01108"></a>01108                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, 
<a name="l01109"></a>01109                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01110"></a>01110                              info);
<a name="l01111"></a>01111  
<a name="l01112"></a>01112 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunml2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01113"></a>01113                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01114"></a>01114                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01115"></a>01115  
<a name="l01116"></a>01116 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmlq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01117"></a>01117                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01118"></a>01118                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01119"></a>01119  
<a name="l01120"></a>01120 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmql_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01121"></a>01121                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01122"></a>01122                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01123"></a>01123  
<a name="l01124"></a>01124 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmqr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01125"></a>01125                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01126"></a>01126                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01127"></a>01127  
<a name="l01128"></a>01128 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmr2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01129"></a>01129                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01130"></a>01130                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01131"></a>01131  
<a name="l01132"></a>01132 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmr3_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01133"></a>01133                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, 
<a name="l01134"></a>01134                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01135"></a>01135  
<a name="l01136"></a>01136 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmrq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01137"></a>01137                              LAPACK_INTEGER *k, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01138"></a>01138                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01139"></a>01139  
<a name="l01140"></a>01140 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmrz_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01141"></a>01141                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, 
<a name="l01142"></a>01142                              LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l01143"></a>01143                              info);
<a name="l01144"></a>01144  
<a name="l01145"></a>01145 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cunmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l01146"></a>01146                              LAPACK_INTEGER *n, LAPACK_COMPLEX *a, LAPACK_INTEGER *lda, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, 
<a name="l01147"></a>01147                              LAPACK_INTEGER *ldc, LAPACK_COMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01148"></a>01148  
<a name="l01149"></a>01149 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cupgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *
<a name="l01150"></a>01150                              tau, LAPACK_COMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01151"></a>01151  
<a name="l01152"></a>01152 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> cupmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l01153"></a>01153                              LAPACK_INTEGER *n, LAPACK_COMPLEX *ap, LAPACK_COMPLEX *tau, LAPACK_COMPLEX *c__, LAPACK_INTEGER *ldc, 
<a name="l01154"></a>01154                              LAPACK_COMPLEX *work, LAPACK_INTEGER *info);
<a name="l01155"></a>01155  
<a name="l01156"></a>01156 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dbdsdc_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01157"></a>01157                              d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, 
<a name="l01158"></a>01158                              LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *iq, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01159"></a>01159                              iwork, LAPACK_INTEGER *info);
<a name="l01160"></a>01160  
<a name="l01161"></a>01161 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dbdsqr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ncvt, LAPACK_INTEGER *
<a name="l01162"></a>01162                              nru, LAPACK_INTEGER *ncc, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *vt, 
<a name="l01163"></a>01163                              LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *
<a name="l01164"></a>01164                              ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01165"></a>01165  
<a name="l01166"></a>01166 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ddisna_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01167"></a>01167                              d__, LAPACK_DOUBLEREAL *sep, LAPACK_INTEGER *info);
<a name="l01168"></a>01168  
<a name="l01169"></a>01169 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbbrd_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *ncc,
<a name="l01170"></a>01170                              LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *
<a name="l01171"></a>01171                              d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *pt, 
<a name="l01172"></a>01172                              LAPACK_INTEGER *ldpt, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, 
<a name="l01173"></a>01173                              LAPACK_INTEGER *info);
<a name="l01174"></a>01174  
<a name="l01175"></a>01175 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l01176"></a>01176                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, 
<a name="l01177"></a>01177                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01178"></a>01178  
<a name="l01179"></a>01179 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l01180"></a>01180                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l01181"></a>01181                              LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *
<a name="l01182"></a>01182                              info);
<a name="l01183"></a>01183  
<a name="l01184"></a>01184 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l01185"></a>01185                              ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *afb, 
<a name="l01186"></a>01186                              LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01187"></a>01187                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l01188"></a>01188                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01189"></a>01189  
<a name="l01190"></a>01190 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_INTEGER *
<a name="l01191"></a>01191                             nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, 
<a name="l01192"></a>01192                             LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l01193"></a>01193  
<a name="l01194"></a>01194 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl,
<a name="l01195"></a>01195                              LAPACK_INTEGER *ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, 
<a name="l01196"></a>01196                              LAPACK_DOUBLEREAL *afb, LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, 
<a name="l01197"></a>01197                              LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01198"></a>01198                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, 
<a name="l01199"></a>01199                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01200"></a>01200  
<a name="l01201"></a>01201 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbtf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l01202"></a>01202                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l01203"></a>01203  
<a name="l01204"></a>01204 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbtrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l01205"></a>01205                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l01206"></a>01206  
<a name="l01207"></a>01207 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgbtrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l01208"></a>01208                              ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, 
<a name="l01209"></a>01209                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l01210"></a>01210  
<a name="l01211"></a>01211 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgebak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l01212"></a>01212                              LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *
<a name="l01213"></a>01213                              ldv, LAPACK_INTEGER *info);
<a name="l01214"></a>01214  
<a name="l01215"></a>01215 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgebal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01216"></a>01216                              lda, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, LAPACK_INTEGER *info);
<a name="l01217"></a>01217  
<a name="l01218"></a>01218 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgebd2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01219"></a>01219                              lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tauq, LAPACK_DOUBLEREAL *
<a name="l01220"></a>01220                              taup, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01221"></a>01221  
<a name="l01222"></a>01222 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgebrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01223"></a>01223                              lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tauq, LAPACK_DOUBLEREAL *
<a name="l01224"></a>01224                              taup, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01225"></a>01225  
<a name="l01226"></a>01226 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgecon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01227"></a>01227                              lda, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01228"></a>01228                              iwork, LAPACK_INTEGER *info);
<a name="l01229"></a>01229  
<a name="l01230"></a>01230 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01231"></a>01231                              lda, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL 
<a name="l01232"></a>01232                              *colcnd, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l01233"></a>01233  
<a name="l01234"></a>01234 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgees_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, LAPACK_INTEGER *n, 
<a name="l01235"></a>01235                             LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_DOUBLEREAL *wr, 
<a name="l01236"></a>01236                             LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *vs, LAPACK_INTEGER *ldvs, LAPACK_DOUBLEREAL *work, 
<a name="l01237"></a>01237                             LAPACK_INTEGER *lwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l01238"></a>01238  
<a name="l01239"></a>01239 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeesx_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, <span class="keywordtype">char</span> *
<a name="l01240"></a>01240                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, 
<a name="l01241"></a>01241                              LAPACK_DOUBLEREAL *wr, LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *vs, LAPACK_INTEGER *ldvs, 
<a name="l01242"></a>01242                              LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *rcondv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01243"></a>01243                              lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l01244"></a>01244  
<a name="l01245"></a>01245 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01246"></a>01246                             a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *wr, LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *vl, 
<a name="l01247"></a>01247                             LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLEREAL *work, 
<a name="l01248"></a>01248                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01249"></a>01249  
<a name="l01250"></a>01250 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l01251"></a>01251                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *wr, 
<a name="l01252"></a>01252                              LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, 
<a name="l01253"></a>01253                              LAPACK_INTEGER *ldvr, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, 
<a name="l01254"></a>01254                              LAPACK_DOUBLEREAL *abnrm, LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *rcondv, LAPACK_DOUBLEREAL 
<a name="l01255"></a>01255                              *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01256"></a>01256  
<a name="l01257"></a>01257 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgegs_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, LAPACK_INTEGER *n, 
<a name="l01258"></a>01258                             LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l01259"></a>01259                             alphar, LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *vsl, 
<a name="l01260"></a>01260                             LAPACK_INTEGER *ldvsl, LAPACK_DOUBLEREAL *vsr, LAPACK_INTEGER *ldvsr, LAPACK_DOUBLEREAL *work, 
<a name="l01261"></a>01261                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01262"></a>01262  
<a name="l01263"></a>01263 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgegv_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01264"></a>01264                             a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, 
<a name="l01265"></a>01265                             LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, 
<a name="l01266"></a>01266                             LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01267"></a>01267                             LAPACK_INTEGER *info);
<a name="l01268"></a>01268  
<a name="l01269"></a>01269 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgehd2_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l01270"></a>01270                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, 
<a name="l01271"></a>01271                              LAPACK_INTEGER *info);
<a name="l01272"></a>01272  
<a name="l01273"></a>01273 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgehrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l01274"></a>01274                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, 
<a name="l01275"></a>01275                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01276"></a>01276  
<a name="l01277"></a>01277 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01278"></a>01278                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01279"></a>01279  
<a name="l01280"></a>01280 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01281"></a>01281                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01282"></a>01282  
<a name="l01283"></a>01283 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgels_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01284"></a>01284                             nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01285"></a>01285                             LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01286"></a>01286  
<a name="l01287"></a>01287 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelsd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01288"></a>01288                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l01289"></a>01289                              s, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork,
<a name="l01290"></a>01290                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01291"></a>01291  
<a name="l01292"></a>01292 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelss_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01293"></a>01293                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l01294"></a>01294                              s, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork,
<a name="l01295"></a>01295                              LAPACK_INTEGER *info);
<a name="l01296"></a>01296  
<a name="l01297"></a>01297 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelsx_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01298"></a>01298                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l01299"></a>01299                              jpvt, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01300"></a>01300                              info);
<a name="l01301"></a>01301  
<a name="l01302"></a>01302 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgelsy_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01303"></a>01303                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l01304"></a>01304                              jpvt, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01305"></a>01305                              lwork, LAPACK_INTEGER *info);
<a name="l01306"></a>01306  
<a name="l01307"></a>01307 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeql2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01308"></a>01308                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01309"></a>01309  
<a name="l01310"></a>01310 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeqlf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01311"></a>01311                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01312"></a>01312  
<a name="l01313"></a>01313 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeqp3_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01314"></a>01314                              lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork,
<a name="l01315"></a>01315                              LAPACK_INTEGER *info);
<a name="l01316"></a>01316  
<a name="l01317"></a>01317 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeqpf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01318"></a>01318                              lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01319"></a>01319  
<a name="l01320"></a>01320 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeqr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01321"></a>01321                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01322"></a>01322  
<a name="l01323"></a>01323 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgeqrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01324"></a>01324                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01325"></a>01325  
<a name="l01326"></a>01326 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgerfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01327"></a>01327                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *
<a name="l01328"></a>01328                              ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, 
<a name="l01329"></a>01329                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l01330"></a>01330                              LAPACK_INTEGER *info);
<a name="l01331"></a>01331  
<a name="l01332"></a>01332 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgerq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01333"></a>01333                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01334"></a>01334  
<a name="l01335"></a>01335 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgerqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01336"></a>01336                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01337"></a>01337  
<a name="l01338"></a>01338 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgesc2_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, 
<a name="l01339"></a>01339                              LAPACK_DOUBLEREAL *rhs, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv, LAPACK_DOUBLEREAL *scale);
<a name="l01340"></a>01340  
<a name="l01341"></a>01341 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgesdd_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01342"></a>01342                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, 
<a name="l01343"></a>01343                              LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01344"></a>01344                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01345"></a>01345  
<a name="l01346"></a>01346 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgesv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER 
<a name="l01347"></a>01347                             *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l01348"></a>01348  
<a name="l01349"></a>01349 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgesvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobvt, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01350"></a>01350                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *
<a name="l01351"></a>01351                              ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01352"></a>01352                              LAPACK_INTEGER *info);
<a name="l01353"></a>01353  
<a name="l01354"></a>01354 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01355"></a>01355                              nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, 
<a name="l01356"></a>01356                              LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l01357"></a>01357                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l01358"></a>01358                              rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01359"></a>01359                              iwork, LAPACK_INTEGER *info);
<a name="l01360"></a>01360  
<a name="l01361"></a>01361 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgetc2_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER 
<a name="l01362"></a>01362                              *ipiv, LAPACK_INTEGER *jpiv, LAPACK_INTEGER *info);
<a name="l01363"></a>01363  
<a name="l01364"></a>01364 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgetf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01365"></a>01365                              lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l01366"></a>01366  
<a name="l01367"></a>01367 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgetrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01368"></a>01368                              lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l01369"></a>01369  
<a name="l01370"></a>01370 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgetri_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER 
<a name="l01371"></a>01371                              *ipiv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01372"></a>01372  
<a name="l01373"></a>01373 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgetrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01374"></a>01374                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l01375"></a>01375                              ldb, LAPACK_INTEGER *info);
<a name="l01376"></a>01376  
<a name="l01377"></a>01377 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggbak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l01378"></a>01378                              LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, LAPACK_INTEGER *m, 
<a name="l01379"></a>01379                              LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *ldv, LAPACK_INTEGER *info);
<a name="l01380"></a>01380  
<a name="l01381"></a>01381 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggbal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01382"></a>01382                              lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l01383"></a>01383                              LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01384"></a>01384                              info);
<a name="l01385"></a>01385  
<a name="l01386"></a>01386 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgges_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l01387"></a>01387                             delctg, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01388"></a>01388                             LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_DOUBLEREAL *alphar, LAPACK_DOUBLEREAL *alphai, 
<a name="l01389"></a>01389                             LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *vsl, LAPACK_INTEGER *ldvsl, LAPACK_DOUBLEREAL *vsr, 
<a name="l01390"></a>01390                             LAPACK_INTEGER *ldvsr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_LOGICAL *bwork, 
<a name="l01391"></a>01391                             LAPACK_INTEGER *info);
<a name="l01392"></a>01392  
<a name="l01393"></a>01393 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggesx_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l01394"></a>01394                              delctg, <span class="keywordtype">char</span> *sense, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, 
<a name="l01395"></a>01395                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_DOUBLEREAL *alphar, 
<a name="l01396"></a>01396                              LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *vsl, LAPACK_INTEGER *ldvsl,
<a name="l01397"></a>01397                              LAPACK_DOUBLEREAL *vsr, LAPACK_INTEGER *ldvsr, LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *
<a name="l01398"></a>01398                              rcondv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l01399"></a>01399                              liwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l01400"></a>01400  
<a name="l01401"></a>01401 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01402"></a>01402                             a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, 
<a name="l01403"></a>01403                             LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, 
<a name="l01404"></a>01404                             LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01405"></a>01405                             LAPACK_INTEGER *info);
<a name="l01406"></a>01406  
<a name="l01407"></a>01407 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l01408"></a>01408                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01409"></a>01409                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *
<a name="l01410"></a>01410                              beta, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, 
<a name="l01411"></a>01411                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, 
<a name="l01412"></a>01412                              LAPACK_DOUBLEREAL *abnrm, LAPACK_DOUBLEREAL *bbnrm, LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *
<a name="l01413"></a>01413                              rcondv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_LOGICAL *
<a name="l01414"></a>01414                              bwork, LAPACK_INTEGER *info);
<a name="l01415"></a>01415  
<a name="l01416"></a>01416 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggglm_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_DOUBLEREAL *
<a name="l01417"></a>01417                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *d__, 
<a name="l01418"></a>01418                              LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *y, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01419"></a>01419                              LAPACK_INTEGER *info);
<a name="l01420"></a>01420  
<a name="l01421"></a>01421 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgghrd_(<span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01422"></a>01422                              ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01423"></a>01423                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *
<a name="l01424"></a>01424                              ldz, LAPACK_INTEGER *info);
<a name="l01425"></a>01425  
<a name="l01426"></a>01426 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgglse_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_DOUBLEREAL *
<a name="l01427"></a>01427                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *c__, 
<a name="l01428"></a>01428                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01429"></a>01429                              LAPACK_INTEGER *info);
<a name="l01430"></a>01430  
<a name="l01431"></a>01431 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggqrf_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_DOUBLEREAL *
<a name="l01432"></a>01432                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *taua, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01433"></a>01433                              LAPACK_DOUBLEREAL *taub, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01434"></a>01434  
<a name="l01435"></a>01435 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01436"></a>01436                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *taua, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01437"></a>01437                              LAPACK_DOUBLEREAL *taub, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01438"></a>01438  
<a name="l01439"></a>01439 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggsvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l01440"></a>01440                              LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *a, 
<a name="l01441"></a>01441                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alpha, 
<a name="l01442"></a>01442                              LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER 
<a name="l01443"></a>01443                              *ldv, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l01444"></a>01444                              LAPACK_INTEGER *info);
<a name="l01445"></a>01445  
<a name="l01446"></a>01446 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dggsvp_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l01447"></a>01447                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01448"></a>01448                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *tola, LAPACK_DOUBLEREAL *tolb, LAPACK_INTEGER *k, LAPACK_INTEGER 
<a name="l01449"></a>01449                              *l, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *ldv, 
<a name="l01450"></a>01450                              LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *iwork, LAPACK_DOUBLEREAL *tau, 
<a name="l01451"></a>01451                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01452"></a>01452  
<a name="l01453"></a>01453 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgtcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *dl, 
<a name="l01454"></a>01454                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *du2, LAPACK_INTEGER *ipiv, 
<a name="l01455"></a>01455                              LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01456"></a>01456                              iwork, LAPACK_INTEGER *info);
<a name="l01457"></a>01457  
<a name="l01458"></a>01458 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgtrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01459"></a>01459                              LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *dlf, 
<a name="l01460"></a>01460                              LAPACK_DOUBLEREAL *df, LAPACK_DOUBLEREAL *duf, LAPACK_DOUBLEREAL *du2, LAPACK_INTEGER *ipiv, 
<a name="l01461"></a>01461                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l01462"></a>01462                              ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l01463"></a>01463                              info);
<a name="l01464"></a>01464  
<a name="l01465"></a>01465 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgtsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *dl, 
<a name="l01466"></a>01466                             LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER 
<a name="l01467"></a>01467                             *info);
<a name="l01468"></a>01468  
<a name="l01469"></a>01469 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgtsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01470"></a>01470                              nrhs, LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *
<a name="l01471"></a>01471                              dlf, LAPACK_DOUBLEREAL *df, LAPACK_DOUBLEREAL *duf, LAPACK_DOUBLEREAL *du2, LAPACK_INTEGER *ipiv, 
<a name="l01472"></a>01472                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l01473"></a>01473                              rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01474"></a>01474                              iwork, LAPACK_INTEGER *info);
<a name="l01475"></a>01475  
<a name="l01476"></a>01476 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgttrf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, 
<a name="l01477"></a>01477                              LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *du2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l01478"></a>01478  
<a name="l01479"></a>01479 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgttrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01480"></a>01480                              LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *du2, 
<a name="l01481"></a>01481                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l01482"></a>01482  
<a name="l01483"></a>01483 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dgtts2_(LAPACK_INTEGER *itrans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01484"></a>01484                              LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, LAPACK_DOUBLEREAL *du2, 
<a name="l01485"></a>01485                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb);
<a name="l01486"></a>01486  
<a name="l01487"></a>01487 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dhgeqz_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, 
<a name="l01488"></a>01488                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *
<a name="l01489"></a>01489                              b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *
<a name="l01490"></a>01490                              beta, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, 
<a name="l01491"></a>01491                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01492"></a>01492  
<a name="l01493"></a>01493 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dhsein_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *eigsrc, <span class="keywordtype">char</span> *initv, LAPACK_LOGICAL *
<a name="l01494"></a>01494                              select, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLEREAL *wr, 
<a name="l01495"></a>01495                              LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, 
<a name="l01496"></a>01496                              LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01497"></a>01497                              ifaill, LAPACK_INTEGER *ifailr, LAPACK_INTEGER *info);
<a name="l01498"></a>01498  
<a name="l01499"></a>01499 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dhseqr_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo,
<a name="l01500"></a>01500                              LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLEREAL *wr, 
<a name="l01501"></a>01501                              LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l01502"></a>01502                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01503"></a>01503  
<a name="l01504"></a>01504 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlabad_(LAPACK_DOUBLEREAL *small, LAPACK_DOUBLEREAL *large);
<a name="l01505"></a>01505  
<a name="l01506"></a>01506 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlabrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_DOUBLEREAL *
<a name="l01507"></a>01507                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tauq, 
<a name="l01508"></a>01508                              LAPACK_DOUBLEREAL *taup, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *y, LAPACK_INTEGER 
<a name="l01509"></a>01509                              *ldy);
<a name="l01510"></a>01510  
<a name="l01511"></a>01511 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlacon_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *v, LAPACK_DOUBLEREAL *x, 
<a name="l01512"></a>01512                              LAPACK_INTEGER *isgn, LAPACK_DOUBLEREAL *est, LAPACK_INTEGER *kase);
<a name="l01513"></a>01513  
<a name="l01514"></a>01514 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlacpy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01515"></a>01515                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb);
<a name="l01516"></a>01516  
<a name="l01517"></a>01517 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dladiv_(LAPACK_DOUBLEREAL *a, LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, 
<a name="l01518"></a>01518                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *p, LAPACK_DOUBLEREAL *q);
<a name="l01519"></a>01519  
<a name="l01520"></a>01520 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlae2_(LAPACK_DOUBLEREAL *a, LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, 
<a name="l01521"></a>01521                             LAPACK_DOUBLEREAL *rt1, LAPACK_DOUBLEREAL *rt2);
<a name="l01522"></a>01522  
<a name="l01523"></a>01523 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaebz_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *nitmax, LAPACK_INTEGER *n, 
<a name="l01524"></a>01524                              LAPACK_INTEGER *mmax, LAPACK_INTEGER *minp, LAPACK_INTEGER *nbmin, LAPACK_DOUBLEREAL *abstol, 
<a name="l01525"></a>01525                              LAPACK_DOUBLEREAL *reltol, LAPACK_DOUBLEREAL *pivmin, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *
<a name="l01526"></a>01526                              e, LAPACK_DOUBLEREAL *e2, LAPACK_INTEGER *nval, LAPACK_DOUBLEREAL *ab, LAPACK_DOUBLEREAL *c__, 
<a name="l01527"></a>01527                              LAPACK_INTEGER *mout, LAPACK_INTEGER *nab, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l01528"></a>01528                              LAPACK_INTEGER *info);
<a name="l01529"></a>01529  
<a name="l01530"></a>01530 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *qsiz, LAPACK_INTEGER *n, 
<a name="l01531"></a>01531                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, 
<a name="l01532"></a>01532                              LAPACK_DOUBLEREAL *qstore, LAPACK_INTEGER *ldqs, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l01533"></a>01533                              LAPACK_INTEGER *info);
<a name="l01534"></a>01534  
<a name="l01535"></a>01535 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed1_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *q, 
<a name="l01536"></a>01536                              LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_DOUBLEREAL *rho, LAPACK_INTEGER *cutpnt, 
<a name="l01537"></a>01537                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01538"></a>01538  
<a name="l01539"></a>01539 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed2_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *n1, LAPACK_DOUBLEREAL *
<a name="l01540"></a>01540                              d__, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_DOUBLEREAL *rho, 
<a name="l01541"></a>01541                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *dlamda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *q2, 
<a name="l01542"></a>01542                              LAPACK_INTEGER *indx, LAPACK_INTEGER *indxc, LAPACK_INTEGER *indxp, LAPACK_INTEGER *coltyp, 
<a name="l01543"></a>01543                              LAPACK_INTEGER *info);
<a name="l01544"></a>01544  
<a name="l01545"></a>01545 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed3_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *n1, LAPACK_DOUBLEREAL *
<a name="l01546"></a>01546                              d__, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *rho, LAPACK_DOUBLEREAL *dlamda,
<a name="l01547"></a>01547                              LAPACK_DOUBLEREAL *q2, LAPACK_INTEGER *indx, LAPACK_INTEGER *ctot, LAPACK_DOUBLEREAL *w, 
<a name="l01548"></a>01548                              LAPACK_DOUBLEREAL *s, LAPACK_INTEGER *info);
<a name="l01549"></a>01549  
<a name="l01550"></a>01550 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed4_(LAPACK_INTEGER *n, LAPACK_INTEGER *i__, LAPACK_DOUBLEREAL *d__, 
<a name="l01551"></a>01551                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *delta, LAPACK_DOUBLEREAL *rho, LAPACK_DOUBLEREAL *dlam,
<a name="l01552"></a>01552                              LAPACK_INTEGER *info);
<a name="l01553"></a>01553  
<a name="l01554"></a>01554 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed5_(LAPACK_INTEGER *i__, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, 
<a name="l01555"></a>01555                              LAPACK_DOUBLEREAL *delta, LAPACK_DOUBLEREAL *rho, LAPACK_DOUBLEREAL *dlam);
<a name="l01556"></a>01556  
<a name="l01557"></a>01557 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed6_(LAPACK_INTEGER *kniter, LAPACK_LOGICAL *orgati, LAPACK_DOUBLEREAL *
<a name="l01558"></a>01558                              rho, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *finit, LAPACK_DOUBLEREAL *
<a name="l01559"></a>01559                              tau, LAPACK_INTEGER *info);
<a name="l01560"></a>01560  
<a name="l01561"></a>01561 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed7_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *n, LAPACK_INTEGER *qsiz, 
<a name="l01562"></a>01562                              LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, LAPACK_INTEGER *curpbm, LAPACK_DOUBLEREAL *d__, 
<a name="l01563"></a>01563                              LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_DOUBLEREAL *rho, LAPACK_INTEGER 
<a name="l01564"></a>01564                              *cutpnt, LAPACK_DOUBLEREAL *qstore, LAPACK_INTEGER *qptr, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *
<a name="l01565"></a>01565                              perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_DOUBLEREAL *givnum, 
<a name="l01566"></a>01566                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01567"></a>01567  
<a name="l01568"></a>01568 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed8_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER 
<a name="l01569"></a>01569                              *qsiz, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, 
<a name="l01570"></a>01570                              LAPACK_DOUBLEREAL *rho, LAPACK_INTEGER *cutpnt, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *dlamda,
<a name="l01571"></a>01571                              LAPACK_DOUBLEREAL *q2, LAPACK_INTEGER *ldq2, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *perm, LAPACK_INTEGER 
<a name="l01572"></a>01572                              *givptr, LAPACK_INTEGER *givcol, LAPACK_DOUBLEREAL *givnum, LAPACK_INTEGER *indxp, LAPACK_INTEGER 
<a name="l01573"></a>01573                              *indx, LAPACK_INTEGER *info);
<a name="l01574"></a>01574  
<a name="l01575"></a>01575 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaed9_(LAPACK_INTEGER *k, LAPACK_INTEGER *kstart, LAPACK_INTEGER *kstop, 
<a name="l01576"></a>01576                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *
<a name="l01577"></a>01577                              rho, LAPACK_DOUBLEREAL *dlamda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *s, LAPACK_INTEGER *lds, 
<a name="l01578"></a>01578                              LAPACK_INTEGER *info);
<a name="l01579"></a>01579  
<a name="l01580"></a>01580 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaeda_(LAPACK_INTEGER *n, LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, 
<a name="l01581"></a>01581                              LAPACK_INTEGER *curpbm, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, 
<a name="l01582"></a>01582                              LAPACK_INTEGER *givcol, LAPACK_DOUBLEREAL *givnum, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *qptr, 
<a name="l01583"></a>01583                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *ztemp, LAPACK_INTEGER *info);
<a name="l01584"></a>01584  
<a name="l01585"></a>01585 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaein_(LAPACK_LOGICAL *rightv, LAPACK_LOGICAL *noinit, LAPACK_INTEGER *n, 
<a name="l01586"></a>01586                              LAPACK_DOUBLEREAL *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLEREAL *wr, LAPACK_DOUBLEREAL *wi, 
<a name="l01587"></a>01587                              LAPACK_DOUBLEREAL *vr, LAPACK_DOUBLEREAL *vi, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01588"></a>01588                              LAPACK_DOUBLEREAL *work, LAPACK_DOUBLEREAL *eps3, LAPACK_DOUBLEREAL *smlnum, LAPACK_DOUBLEREAL *
<a name="l01589"></a>01589                              bignum, LAPACK_INTEGER *info);
<a name="l01590"></a>01590  
<a name="l01591"></a>01591 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaev2_(LAPACK_DOUBLEREAL *a, LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, 
<a name="l01592"></a>01592                              LAPACK_DOUBLEREAL *rt1, LAPACK_DOUBLEREAL *rt2, LAPACK_DOUBLEREAL *cs1, LAPACK_DOUBLEREAL *sn1);
<a name="l01593"></a>01593  
<a name="l01594"></a>01594 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaexc_(LAPACK_LOGICAL *wantq, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *t, 
<a name="l01595"></a>01595                              LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *j1, LAPACK_INTEGER *n1, 
<a name="l01596"></a>01596                              LAPACK_INTEGER *n2, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01597"></a>01597  
<a name="l01598"></a>01598 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlag2_(LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01599"></a>01599                             LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *safmin, LAPACK_DOUBLEREAL *scale1, LAPACK_DOUBLEREAL *
<a name="l01600"></a>01600                             scale2, LAPACK_DOUBLEREAL *wr1, LAPACK_DOUBLEREAL *wr2, LAPACK_DOUBLEREAL *wi);
<a name="l01601"></a>01601  
<a name="l01602"></a>01602 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlags2_(LAPACK_LOGICAL *upper, LAPACK_DOUBLEREAL *a1, LAPACK_DOUBLEREAL *a2, 
<a name="l01603"></a>01603                              LAPACK_DOUBLEREAL *a3, LAPACK_DOUBLEREAL *b1, LAPACK_DOUBLEREAL *b2, LAPACK_DOUBLEREAL *b3, 
<a name="l01604"></a>01604                              LAPACK_DOUBLEREAL *csu, LAPACK_DOUBLEREAL *snu, LAPACK_DOUBLEREAL *csv, LAPACK_DOUBLEREAL *snv, 
<a name="l01605"></a>01605                              LAPACK_DOUBLEREAL *csq, LAPACK_DOUBLEREAL *snq);
<a name="l01606"></a>01606  
<a name="l01607"></a>01607 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlagtf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_DOUBLEREAL *lambda, 
<a name="l01608"></a>01608                              LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *tol, LAPACK_DOUBLEREAL *d__, 
<a name="l01609"></a>01609                              LAPACK_INTEGER *in, LAPACK_INTEGER *info);
<a name="l01610"></a>01610  
<a name="l01611"></a>01611 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlagtm_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l01612"></a>01612                              LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *dl, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *du, 
<a name="l01613"></a>01613                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER 
<a name="l01614"></a>01614                              *ldb);
<a name="l01615"></a>01615  
<a name="l01616"></a>01616 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlagts_(LAPACK_INTEGER *job, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, 
<a name="l01617"></a>01617                              LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *d__, LAPACK_INTEGER *in, 
<a name="l01618"></a>01618                              LAPACK_DOUBLEREAL *y, LAPACK_DOUBLEREAL *tol, LAPACK_INTEGER *info);
<a name="l01619"></a>01619  
<a name="l01620"></a>01620 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlagv2_(LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, 
<a name="l01621"></a>01621                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *
<a name="l01622"></a>01622                              beta, LAPACK_DOUBLEREAL *csl, LAPACK_DOUBLEREAL *snl, LAPACK_DOUBLEREAL *csr, LAPACK_DOUBLEREAL *
<a name="l01623"></a>01623                              snr);
<a name="l01624"></a>01624  
<a name="l01625"></a>01625 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlahqr_(LAPACK_LOGICAL *wantt, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l01626"></a>01626                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLEREAL 
<a name="l01627"></a>01627                              *wr, LAPACK_DOUBLEREAL *wi, LAPACK_INTEGER *iloz, LAPACK_INTEGER *ihiz, LAPACK_DOUBLEREAL *z__, 
<a name="l01628"></a>01628                              LAPACK_INTEGER *ldz, LAPACK_INTEGER *info);
<a name="l01629"></a>01629  
<a name="l01630"></a>01630 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlahrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *nb, LAPACK_DOUBLEREAL *
<a name="l01631"></a>01631                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, 
<a name="l01632"></a>01632                              LAPACK_DOUBLEREAL *y, LAPACK_INTEGER *ldy);
<a name="l01633"></a>01633  
<a name="l01634"></a>01634 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaic1_(LAPACK_INTEGER *job, LAPACK_INTEGER *j, LAPACK_DOUBLEREAL *x, 
<a name="l01635"></a>01635                              LAPACK_DOUBLEREAL *sest, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *gamma, LAPACK_DOUBLEREAL *
<a name="l01636"></a>01636                              sestpr, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *c__);
<a name="l01637"></a>01637  
<a name="l01638"></a>01638 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaln2_(LAPACK_LOGICAL *ltrans, LAPACK_INTEGER *na, LAPACK_INTEGER *nw, 
<a name="l01639"></a>01639                              LAPACK_DOUBLEREAL *smin, LAPACK_DOUBLEREAL *ca, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, 
<a name="l01640"></a>01640                              LAPACK_DOUBLEREAL *d1, LAPACK_DOUBLEREAL *d2, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01641"></a>01641                              LAPACK_DOUBLEREAL *wr, LAPACK_DOUBLEREAL *wi, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, 
<a name="l01642"></a>01642                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *xnorm, LAPACK_INTEGER *info);
<a name="l01643"></a>01643  
<a name="l01644"></a>01644 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlals0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l01645"></a>01645                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL 
<a name="l01646"></a>01646                              *bx, LAPACK_INTEGER *ldbx, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l01647"></a>01647                              LAPACK_INTEGER *ldgcol, LAPACK_DOUBLEREAL *givnum, LAPACK_INTEGER *ldgnum, LAPACK_DOUBLEREAL *
<a name="l01648"></a>01648                              poles, LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *
<a name="l01649"></a>01649                              k, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01650"></a>01650  
<a name="l01651"></a>01651 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlalsa_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l01652"></a>01652                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *bx, LAPACK_INTEGER *
<a name="l01653"></a>01653                              ldbx, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *k, 
<a name="l01654"></a>01654                              LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *
<a name="l01655"></a>01655                              poles, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_INTEGER *
<a name="l01656"></a>01656                              perm, LAPACK_DOUBLEREAL *givnum, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *
<a name="l01657"></a>01657                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01658"></a>01658  
<a name="l01659"></a>01659 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlalsd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, LAPACK_INTEGER 
<a name="l01660"></a>01660                              *nrhs, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l01661"></a>01661                              LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l01662"></a>01662                              LAPACK_INTEGER *info);
<a name="l01663"></a>01663  
<a name="l01664"></a>01664 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlamc1_(LAPACK_INTEGER *beta, LAPACK_INTEGER *t, LAPACK_LOGICAL *rnd, LAPACK_LOGICAL 
<a name="l01665"></a>01665                              *ieee1);
<a name="l01666"></a>01666  
<a name="l01667"></a>01667 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlamc2_(LAPACK_INTEGER *beta, LAPACK_INTEGER *t, LAPACK_LOGICAL *rnd, 
<a name="l01668"></a>01668                              LAPACK_DOUBLEREAL *eps, LAPACK_INTEGER *emin, LAPACK_DOUBLEREAL *rmin, LAPACK_INTEGER *emax, 
<a name="l01669"></a>01669                              LAPACK_DOUBLEREAL *rmax);
<a name="l01670"></a>01670  
<a name="l01671"></a>01671 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlamc4_(LAPACK_INTEGER *emin, LAPACK_DOUBLEREAL *start, LAPACK_INTEGER *base);
<a name="l01672"></a>01672  
<a name="l01673"></a>01673 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlamc5_(LAPACK_INTEGER *beta, LAPACK_INTEGER *p, LAPACK_INTEGER *emin, 
<a name="l01674"></a>01674                              LAPACK_LOGICAL *ieee, LAPACK_INTEGER *emax, LAPACK_DOUBLEREAL *rmax);
<a name="l01675"></a>01675  
<a name="l01676"></a>01676 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlamrg_(LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER 
<a name="l01677"></a>01677                              *dtrd1, LAPACK_INTEGER *dtrd2, LAPACK_INTEGER *index);
<a name="l01678"></a>01678  
<a name="l01679"></a>01679 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlanv2_(LAPACK_DOUBLEREAL *a, LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *c__, 
<a name="l01680"></a>01680                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *rt1r, LAPACK_DOUBLEREAL *rt1i, LAPACK_DOUBLEREAL *rt2r,
<a name="l01681"></a>01681                              LAPACK_DOUBLEREAL *rt2i, LAPACK_DOUBLEREAL *cs, LAPACK_DOUBLEREAL *sn);
<a name="l01682"></a>01682  
<a name="l01683"></a>01683 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlapll_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *incx, 
<a name="l01684"></a>01684                              LAPACK_DOUBLEREAL *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *ssmin);
<a name="l01685"></a>01685  
<a name="l01686"></a>01686 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlapmt_(LAPACK_LOGICAL *forwrd, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01687"></a>01687                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_INTEGER *k);
<a name="l01688"></a>01688  
<a name="l01689"></a>01689 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqgb_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l01690"></a>01690                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l01691"></a>01691                              LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l01692"></a>01692  
<a name="l01693"></a>01693 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqge_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01694"></a>01694                              lda, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL 
<a name="l01695"></a>01695                              *colcnd, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l01696"></a>01696  
<a name="l01697"></a>01697 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqp2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, 
<a name="l01698"></a>01698                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLEREAL *tau, 
<a name="l01699"></a>01699                              LAPACK_DOUBLEREAL *vn1, LAPACK_DOUBLEREAL *vn2, LAPACK_DOUBLEREAL *work);
<a name="l01700"></a>01700  
<a name="l01701"></a>01701 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqps_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_INTEGER 
<a name="l01702"></a>01702                              *nb, LAPACK_INTEGER *kb, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, 
<a name="l01703"></a>01703                              LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *vn1, LAPACK_DOUBLEREAL *vn2, LAPACK_DOUBLEREAL *auxv, 
<a name="l01704"></a>01704                              LAPACK_DOUBLEREAL *f, LAPACK_INTEGER *ldf);
<a name="l01705"></a>01705  
<a name="l01706"></a>01706 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqsb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l01707"></a>01707                              ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax,
<a name="l01708"></a>01708                              <span class="keywordtype">char</span> *equed);
<a name="l01709"></a>01709  
<a name="l01710"></a>01710 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqsp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, 
<a name="l01711"></a>01711                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l01712"></a>01712  
<a name="l01713"></a>01713 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqsy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01714"></a>01714                              lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l01715"></a>01715  
<a name="l01716"></a>01716 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaqtr_(LAPACK_LOGICAL *ltran, LAPACK_LOGICAL *lLAPACK_REAL, LAPACK_INTEGER *n, 
<a name="l01717"></a>01717                              LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *b, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL 
<a name="l01718"></a>01718                              *scale, LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01719"></a>01719  
<a name="l01720"></a>01720 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlar1v_(LAPACK_INTEGER *n, LAPACK_INTEGER *b1, LAPACK_INTEGER *bn, LAPACK_DOUBLEREAL 
<a name="l01721"></a>01721                              *sigma, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, LAPACK_DOUBLEREAL *ld, LAPACK_DOUBLEREAL *
<a name="l01722"></a>01722                              lld, LAPACK_DOUBLEREAL *gersch, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *ztz, LAPACK_DOUBLEREAL 
<a name="l01723"></a>01723                              *mingma, LAPACK_INTEGER *r__, LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work);
<a name="l01724"></a>01724  
<a name="l01725"></a>01725 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlar2v_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *y, 
<a name="l01726"></a>01726                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *incx, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, 
<a name="l01727"></a>01727                              LAPACK_INTEGER *incc);
<a name="l01728"></a>01728  
<a name="l01729"></a>01729 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarf_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *v,
<a name="l01730"></a>01730                             LAPACK_INTEGER *incv, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, 
<a name="l01731"></a>01731                             LAPACK_DOUBLEREAL *work);
<a name="l01732"></a>01732  
<a name="l01733"></a>01733 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarfb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l01734"></a>01734                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *
<a name="l01735"></a>01735                              ldv, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, 
<a name="l01736"></a>01736                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *ldwork);
<a name="l01737"></a>01737  
<a name="l01738"></a>01738 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarfg_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *x, 
<a name="l01739"></a>01739                              LAPACK_INTEGER *incx, LAPACK_DOUBLEREAL *tau);
<a name="l01740"></a>01740  
<a name="l01741"></a>01741 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarft_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01742"></a>01742                              k, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *t, 
<a name="l01743"></a>01743                              LAPACK_INTEGER *ldt);
<a name="l01744"></a>01744  
<a name="l01745"></a>01745 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarfx_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01746"></a>01746                              v, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work);
<a name="l01747"></a>01747  
<a name="l01748"></a>01748 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlargv_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *incx, 
<a name="l01749"></a>01749                              LAPACK_DOUBLEREAL *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *incc);
<a name="l01750"></a>01750  
<a name="l01751"></a>01751 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarnv_(LAPACK_INTEGER *idist, LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, 
<a name="l01752"></a>01752                              LAPACK_DOUBLEREAL *x);
<a name="l01753"></a>01753  
<a name="l01754"></a>01754 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarrb_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, 
<a name="l01755"></a>01755                              LAPACK_DOUBLEREAL *ld, LAPACK_DOUBLEREAL *lld, LAPACK_INTEGER *ifirst, LAPACK_INTEGER *ilast, 
<a name="l01756"></a>01756                              LAPACK_DOUBLEREAL *sigma, LAPACK_DOUBLEREAL *reltol, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *
<a name="l01757"></a>01757                              wgap, LAPACK_DOUBLEREAL *werr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l01758"></a>01758                              info);
<a name="l01759"></a>01759  
<a name="l01760"></a>01760 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarre_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l01761"></a>01761                              LAPACK_DOUBLEREAL *tol, LAPACK_INTEGER *nsplit, LAPACK_INTEGER *isplit, LAPACK_INTEGER *m, 
<a name="l01762"></a>01762                              LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *woff, LAPACK_DOUBLEREAL *gersch, LAPACK_DOUBLEREAL *work,
<a name="l01763"></a>01763                              LAPACK_INTEGER *info);
<a name="l01764"></a>01764  
<a name="l01765"></a>01765 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarrf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, 
<a name="l01766"></a>01766                              LAPACK_DOUBLEREAL *ld, LAPACK_DOUBLEREAL *lld, LAPACK_INTEGER *ifirst, LAPACK_INTEGER *ilast, 
<a name="l01767"></a>01767                              LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *dplus, LAPACK_DOUBLEREAL *lplus, LAPACK_DOUBLEREAL *work,
<a name="l01768"></a>01768                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01769"></a>01769  
<a name="l01770"></a>01770 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarrv_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, 
<a name="l01771"></a>01771                              LAPACK_INTEGER *isplit, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *iblock, 
<a name="l01772"></a>01772                              LAPACK_DOUBLEREAL *gersch, LAPACK_DOUBLEREAL *tol, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, 
<a name="l01773"></a>01773                              LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01774"></a>01774  
<a name="l01775"></a>01775 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlartg_(LAPACK_DOUBLEREAL *f, LAPACK_DOUBLEREAL *g, LAPACK_DOUBLEREAL *cs, 
<a name="l01776"></a>01776                              LAPACK_DOUBLEREAL *sn, LAPACK_DOUBLEREAL *r__);
<a name="l01777"></a>01777  
<a name="l01778"></a>01778 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlartv_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *incx, 
<a name="l01779"></a>01779                              LAPACK_DOUBLEREAL *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_INTEGER 
<a name="l01780"></a>01780                              *incc);
<a name="l01781"></a>01781  
<a name="l01782"></a>01782 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaruv_(LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x);
<a name="l01783"></a>01783  
<a name="l01784"></a>01784 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarz_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, 
<a name="l01785"></a>01785                             LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *incv, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *c__, 
<a name="l01786"></a>01786                             LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work);
<a name="l01787"></a>01787  
<a name="l01788"></a>01788 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarzb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l01789"></a>01789                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *v,
<a name="l01790"></a>01790                              LAPACK_INTEGER *ldv, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *
<a name="l01791"></a>01791                              ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *ldwork);
<a name="l01792"></a>01792  
<a name="l01793"></a>01793 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlarzt_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01794"></a>01794                              k, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *t, 
<a name="l01795"></a>01795                              LAPACK_INTEGER *ldt);
<a name="l01796"></a>01796  
<a name="l01797"></a>01797 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlas2_(LAPACK_DOUBLEREAL *f, LAPACK_DOUBLEREAL *g, LAPACK_DOUBLEREAL *h__, 
<a name="l01798"></a>01798                             LAPACK_DOUBLEREAL *ssmin, LAPACK_DOUBLEREAL *ssmax);
<a name="l01799"></a>01799  
<a name="l01800"></a>01800 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlascl_(<span class="keywordtype">char</span> *type__, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, 
<a name="l01801"></a>01801                              LAPACK_DOUBLEREAL *cfrom, LAPACK_DOUBLEREAL *cto, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l01802"></a>01802                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l01803"></a>01803  
<a name="l01804"></a>01804 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd0_(LAPACK_INTEGER *n, LAPACK_INTEGER *sqre, LAPACK_DOUBLEREAL *d__, 
<a name="l01805"></a>01805                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *
<a name="l01806"></a>01806                              ldvt, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *iwork, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l01807"></a>01807                              info);
<a name="l01808"></a>01808  
<a name="l01809"></a>01809 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd1_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, 
<a name="l01810"></a>01810                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *u, 
<a name="l01811"></a>01811                              LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, LAPACK_INTEGER *idxq, LAPACK_INTEGER *
<a name="l01812"></a>01812                              iwork, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01813"></a>01813  
<a name="l01814"></a>01814 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd2_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, LAPACK_INTEGER 
<a name="l01815"></a>01815                              *k, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *
<a name="l01816"></a>01816                              beta, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, 
<a name="l01817"></a>01817                              LAPACK_DOUBLEREAL *dsigma, LAPACK_DOUBLEREAL *u2, LAPACK_INTEGER *ldu2, LAPACK_DOUBLEREAL *vt2, 
<a name="l01818"></a>01818                              LAPACK_INTEGER *ldvt2, LAPACK_INTEGER *idxp, LAPACK_INTEGER *idx, LAPACK_INTEGER *idxc, LAPACK_INTEGER *
<a name="l01819"></a>01819                              idxq, LAPACK_INTEGER *coltyp, LAPACK_INTEGER *info);
<a name="l01820"></a>01820  
<a name="l01821"></a>01821 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd3_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, LAPACK_INTEGER 
<a name="l01822"></a>01822                              *k, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *dsigma, 
<a name="l01823"></a>01823                              LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *u2, LAPACK_INTEGER *ldu2, 
<a name="l01824"></a>01824                              LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *vt2, LAPACK_INTEGER *ldvt2, 
<a name="l01825"></a>01825                              LAPACK_INTEGER *idxc, LAPACK_INTEGER *ctot, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *info);
<a name="l01826"></a>01826  
<a name="l01827"></a>01827 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd4_(LAPACK_INTEGER *n, LAPACK_INTEGER *i__, LAPACK_DOUBLEREAL *d__, 
<a name="l01828"></a>01828                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *delta, LAPACK_DOUBLEREAL *rho, LAPACK_DOUBLEREAL *
<a name="l01829"></a>01829                              sigma, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01830"></a>01830  
<a name="l01831"></a>01831 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd5_(LAPACK_INTEGER *i__, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, 
<a name="l01832"></a>01832                              LAPACK_DOUBLEREAL *delta, LAPACK_DOUBLEREAL *rho, LAPACK_DOUBLEREAL *dsigma, LAPACK_DOUBLEREAL *
<a name="l01833"></a>01833                              work);
<a name="l01834"></a>01834  
<a name="l01835"></a>01835 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd6_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l01836"></a>01836                              LAPACK_INTEGER *sqre, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *vf, LAPACK_DOUBLEREAL *vl, 
<a name="l01837"></a>01837                              LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *beta, LAPACK_INTEGER *idxq, LAPACK_INTEGER *perm, 
<a name="l01838"></a>01838                              LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_DOUBLEREAL *givnum,
<a name="l01839"></a>01839                              LAPACK_INTEGER *ldgnum, LAPACK_DOUBLEREAL *poles, LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *
<a name="l01840"></a>01840                              difr, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, 
<a name="l01841"></a>01841                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01842"></a>01842  
<a name="l01843"></a>01843 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd7_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l01844"></a>01844                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, 
<a name="l01845"></a>01845                              LAPACK_DOUBLEREAL *zw, LAPACK_DOUBLEREAL *vf, LAPACK_DOUBLEREAL *vfw, LAPACK_DOUBLEREAL *vl, 
<a name="l01846"></a>01846                              LAPACK_DOUBLEREAL *vlw, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *
<a name="l01847"></a>01847                              dsigma, LAPACK_INTEGER *idx, LAPACK_INTEGER *idxp, LAPACK_INTEGER *idxq, LAPACK_INTEGER *perm, 
<a name="l01848"></a>01848                              LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_DOUBLEREAL *givnum,
<a name="l01849"></a>01849                              LAPACK_INTEGER *ldgnum, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_INTEGER *info);
<a name="l01850"></a>01850  
<a name="l01851"></a>01851 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd8_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *d__, 
<a name="l01852"></a>01852                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *vf, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *difl, 
<a name="l01853"></a>01853                              LAPACK_DOUBLEREAL *difr, LAPACK_INTEGER *lddifr, LAPACK_DOUBLEREAL *dsigma, LAPACK_DOUBLEREAL *
<a name="l01854"></a>01854                              work, LAPACK_INTEGER *info);
<a name="l01855"></a>01855  
<a name="l01856"></a>01856 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasd9_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *ldu, LAPACK_INTEGER *k, 
<a name="l01857"></a>01857                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *vf, LAPACK_DOUBLEREAL *vl, 
<a name="l01858"></a>01858                              LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, LAPACK_DOUBLEREAL *dsigma, LAPACK_DOUBLEREAL *
<a name="l01859"></a>01859                              work, LAPACK_INTEGER *info);
<a name="l01860"></a>01860  
<a name="l01861"></a>01861 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasda_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l01862"></a>01862                              LAPACK_INTEGER *sqre, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER 
<a name="l01863"></a>01863                              *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, 
<a name="l01864"></a>01864                              LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *poles, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l01865"></a>01865                              LAPACK_INTEGER *ldgcol, LAPACK_INTEGER *perm, LAPACK_DOUBLEREAL *givnum, LAPACK_DOUBLEREAL *c__, 
<a name="l01866"></a>01866                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l01867"></a>01867  
<a name="l01868"></a>01868 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasdq_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *sqre, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l01869"></a>01869                              ncvt, LAPACK_INTEGER *nru, LAPACK_INTEGER *ncc, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l01870"></a>01870                              LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *ldvt, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, 
<a name="l01871"></a>01871                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01872"></a>01872  
<a name="l01873"></a>01873 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasdt_(LAPACK_INTEGER *n, LAPACK_INTEGER *lvl, LAPACK_INTEGER *nd, LAPACK_INTEGER *
<a name="l01874"></a>01874                              inode, LAPACK_INTEGER *ndiml, LAPACK_INTEGER *ndimr, LAPACK_INTEGER *msub);
<a name="l01875"></a>01875  
<a name="l01876"></a>01876 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaset_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01877"></a>01877                              alpha, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda);
<a name="l01878"></a>01878  
<a name="l01879"></a>01879 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq1_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l01880"></a>01880                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01881"></a>01881  
<a name="l01882"></a>01882 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq2_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *info);
<a name="l01883"></a>01883  
<a name="l01884"></a>01884 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq3_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_DOUBLEREAL *z__, 
<a name="l01885"></a>01885                              LAPACK_INTEGER *pp, LAPACK_DOUBLEREAL *dmin__, LAPACK_DOUBLEREAL *sigma, LAPACK_DOUBLEREAL *desig,
<a name="l01886"></a>01886                              LAPACK_DOUBLEREAL *qmax, LAPACK_INTEGER *nfail, LAPACK_INTEGER *iter, LAPACK_INTEGER *ndiv, 
<a name="l01887"></a>01887                              LAPACK_LOGICAL *ieee);
<a name="l01888"></a>01888  
<a name="l01889"></a>01889 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq4_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_DOUBLEREAL *z__, 
<a name="l01890"></a>01890                              LAPACK_INTEGER *pp, LAPACK_INTEGER *n0in, LAPACK_DOUBLEREAL *dmin__, LAPACK_DOUBLEREAL *dmin1, 
<a name="l01891"></a>01891                              LAPACK_DOUBLEREAL *dmin2, LAPACK_DOUBLEREAL *dn, LAPACK_DOUBLEREAL *dn1, LAPACK_DOUBLEREAL *dn2, 
<a name="l01892"></a>01892                              LAPACK_DOUBLEREAL *tau, LAPACK_INTEGER *ttype);
<a name="l01893"></a>01893  
<a name="l01894"></a>01894 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq5_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_DOUBLEREAL *z__, 
<a name="l01895"></a>01895                              LAPACK_INTEGER *pp, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *dmin__, LAPACK_DOUBLEREAL *dmin1, 
<a name="l01896"></a>01896                              LAPACK_DOUBLEREAL *dmin2, LAPACK_DOUBLEREAL *dn, LAPACK_DOUBLEREAL *dnm1, LAPACK_DOUBLEREAL *dnm2,
<a name="l01897"></a>01897                              LAPACK_LOGICAL *ieee);
<a name="l01898"></a>01898  
<a name="l01899"></a>01899 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasq6_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_DOUBLEREAL *z__, 
<a name="l01900"></a>01900                              LAPACK_INTEGER *pp, LAPACK_DOUBLEREAL *dmin__, LAPACK_DOUBLEREAL *dmin1, LAPACK_DOUBLEREAL *dmin2,
<a name="l01901"></a>01901                              LAPACK_DOUBLEREAL *dn, LAPACK_DOUBLEREAL *dnm1, LAPACK_DOUBLEREAL *dnm2);
<a name="l01902"></a>01902  
<a name="l01903"></a>01903 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *pivot, <span class="keywordtype">char</span> *direct, LAPACK_INTEGER *m,
<a name="l01904"></a>01904                             LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01905"></a>01905                             lda);
<a name="l01906"></a>01906  
<a name="l01907"></a>01907 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasrt_(<span class="keywordtype">char</span> *<span class="keywordtype">id</span>, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_INTEGER *
<a name="l01908"></a>01908                              info);
<a name="l01909"></a>01909  
<a name="l01910"></a>01910 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlassq_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *incx, 
<a name="l01911"></a>01911                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *sumsq);
<a name="l01912"></a>01912  
<a name="l01913"></a>01913 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasv2_(LAPACK_DOUBLEREAL *f, LAPACK_DOUBLEREAL *g, LAPACK_DOUBLEREAL *h__, 
<a name="l01914"></a>01914                              LAPACK_DOUBLEREAL *ssmin, LAPACK_DOUBLEREAL *ssmax, LAPACK_DOUBLEREAL *snr, LAPACK_DOUBLEREAL *
<a name="l01915"></a>01915                              csr, LAPACK_DOUBLEREAL *snl, LAPACK_DOUBLEREAL *csl);
<a name="l01916"></a>01916  
<a name="l01917"></a>01917 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlaswp_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER 
<a name="l01918"></a>01918                              *k1, LAPACK_INTEGER *k2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *incx);
<a name="l01919"></a>01919  
<a name="l01920"></a>01920 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasy2_(LAPACK_LOGICAL *ltranl, LAPACK_LOGICAL *ltranr, LAPACK_INTEGER *isgn, 
<a name="l01921"></a>01921                              LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, LAPACK_DOUBLEREAL *tl, LAPACK_INTEGER *ldtl, LAPACK_DOUBLEREAL *
<a name="l01922"></a>01922                              tr, LAPACK_INTEGER *ldtr, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *scale, 
<a name="l01923"></a>01923                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *xnorm, LAPACK_INTEGER *info);
<a name="l01924"></a>01924  
<a name="l01925"></a>01925 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlasyf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l01926"></a>01926                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *
<a name="l01927"></a>01927                              ldw, LAPACK_INTEGER *info);
<a name="l01928"></a>01928  
<a name="l01929"></a>01929 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatbs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l01930"></a>01930                              normin, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, 
<a name="l01931"></a>01931                              LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l01932"></a>01932  
<a name="l01933"></a>01933 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatdf_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *z__, 
<a name="l01934"></a>01934                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *rhs, LAPACK_DOUBLEREAL *rdsum, LAPACK_DOUBLEREAL *rdscal, 
<a name="l01935"></a>01935                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv);
<a name="l01936"></a>01936  
<a name="l01937"></a>01937 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatps_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l01938"></a>01938                              normin, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *x, LAPACK_DOUBLEREAL *scale, 
<a name="l01939"></a>01939                              LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l01940"></a>01940  
<a name="l01941"></a>01941 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_DOUBLEREAL *
<a name="l01942"></a>01942                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *w, 
<a name="l01943"></a>01943                              LAPACK_INTEGER *ldw);
<a name="l01944"></a>01944  
<a name="l01945"></a>01945 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l01946"></a>01946                              normin, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *x, 
<a name="l01947"></a>01947                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l01948"></a>01948  
<a name="l01949"></a>01949 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatrz_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *
<a name="l01950"></a>01950                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work);
<a name="l01951"></a>01951  
<a name="l01952"></a>01952 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlatzm_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l01953"></a>01953                              v, LAPACK_INTEGER *incv, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *c1, LAPACK_DOUBLEREAL *c2, 
<a name="l01954"></a>01954                              LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work);
<a name="l01955"></a>01955  
<a name="l01956"></a>01956 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlauu2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01957"></a>01957                              lda, LAPACK_INTEGER *info);
<a name="l01958"></a>01958  
<a name="l01959"></a>01959 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dlauum_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l01960"></a>01960                              lda, LAPACK_INTEGER *info);
<a name="l01961"></a>01961  
<a name="l01962"></a>01962 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dopgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, 
<a name="l01963"></a>01963                              LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *work, 
<a name="l01964"></a>01964                              LAPACK_INTEGER *info);
<a name="l01965"></a>01965  
<a name="l01966"></a>01966 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dopmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l01967"></a>01967                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER 
<a name="l01968"></a>01968                              *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01969"></a>01969  
<a name="l01970"></a>01970 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorg2l_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01971"></a>01971                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01972"></a>01972  
<a name="l01973"></a>01973 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorg2r_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01974"></a>01974                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01975"></a>01975  
<a name="l01976"></a>01976 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgbr_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l01977"></a>01977                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, 
<a name="l01978"></a>01978                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01979"></a>01979  
<a name="l01980"></a>01980 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorghr_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l01981"></a>01981                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, 
<a name="l01982"></a>01982                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l01983"></a>01983  
<a name="l01984"></a>01984 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgl2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01985"></a>01985                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l01986"></a>01986  
<a name="l01987"></a>01987 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorglq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01988"></a>01988                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01989"></a>01989                              LAPACK_INTEGER *info);
<a name="l01990"></a>01990  
<a name="l01991"></a>01991 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgql_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01992"></a>01992                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01993"></a>01993                              LAPACK_INTEGER *info);
<a name="l01994"></a>01994  
<a name="l01995"></a>01995 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgqr_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l01996"></a>01996                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l01997"></a>01997                              LAPACK_INTEGER *info);
<a name="l01998"></a>01998  
<a name="l01999"></a>01999 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l02000"></a>02000                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02001"></a>02001  
<a name="l02002"></a>02002 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgrq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *
<a name="l02003"></a>02003                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02004"></a>02004                              LAPACK_INTEGER *info);
<a name="l02005"></a>02005  
<a name="l02006"></a>02006 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02007"></a>02007                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02008"></a>02008  
<a name="l02009"></a>02009 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorm2l_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02010"></a>02010                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02011"></a>02011                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02012"></a>02012  
<a name="l02013"></a>02013 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorm2r_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02014"></a>02014                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02015"></a>02015                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02016"></a>02016  
<a name="l02017"></a>02017 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormbr_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l02018"></a>02018                              LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, 
<a name="l02019"></a>02019                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02020"></a>02020                              LAPACK_INTEGER *info);
<a name="l02021"></a>02021  
<a name="l02022"></a>02022 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormhr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02023"></a>02023                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *
<a name="l02024"></a>02024                              tau, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02025"></a>02025                              LAPACK_INTEGER *info);
<a name="l02026"></a>02026  
<a name="l02027"></a>02027 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dorml2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02028"></a>02028                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02029"></a>02029                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02030"></a>02030  
<a name="l02031"></a>02031 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormlq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02032"></a>02032                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02033"></a>02033                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02034"></a>02034  
<a name="l02035"></a>02035 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormql_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02036"></a>02036                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02037"></a>02037                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02038"></a>02038  
<a name="l02039"></a>02039 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormqr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02040"></a>02040                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02041"></a>02041                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02042"></a>02042  
<a name="l02043"></a>02043 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormr2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02044"></a>02044                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02045"></a>02045                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02046"></a>02046  
<a name="l02047"></a>02047 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormr3_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02048"></a>02048                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, 
<a name="l02049"></a>02049                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02050"></a>02050  
<a name="l02051"></a>02051 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormrq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02052"></a>02052                              LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02053"></a>02053                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02054"></a>02054  
<a name="l02055"></a>02055 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormrz_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02056"></a>02056                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, 
<a name="l02057"></a>02057                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02058"></a>02058                              LAPACK_INTEGER *info);
<a name="l02059"></a>02059  
<a name="l02060"></a>02060 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dormtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l02061"></a>02061                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02062"></a>02062                              c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02063"></a>02063  
<a name="l02064"></a>02064 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l02065"></a>02065                              ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *
<a name="l02066"></a>02066                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02067"></a>02067  
<a name="l02068"></a>02068 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l02069"></a>02069                              ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax,
<a name="l02070"></a>02070                              LAPACK_INTEGER *info);
<a name="l02071"></a>02071  
<a name="l02072"></a>02072 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l02073"></a>02073                              nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *afb, LAPACK_INTEGER *ldafb, 
<a name="l02074"></a>02074                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l02075"></a>02075                              ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l02076"></a>02076                              info);
<a name="l02077"></a>02077  
<a name="l02078"></a>02078 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbstf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l02079"></a>02079                              ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l02080"></a>02080  
<a name="l02081"></a>02081 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l02082"></a>02082                             nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02083"></a>02083                             LAPACK_INTEGER *info);
<a name="l02084"></a>02084  
<a name="l02085"></a>02085 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l02086"></a>02086                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *afb, 
<a name="l02087"></a>02087                              LAPACK_INTEGER *ldafb, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l02088"></a>02088                              ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr,
<a name="l02089"></a>02089                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02090"></a>02090  
<a name="l02091"></a>02091 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbtf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l02092"></a>02092                              ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l02093"></a>02093  
<a name="l02094"></a>02094 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbtrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *
<a name="l02095"></a>02095                              ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l02096"></a>02096  
<a name="l02097"></a>02097 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpbtrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l02098"></a>02098                              nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02099"></a>02099                              LAPACK_INTEGER *info);
<a name="l02100"></a>02100  
<a name="l02101"></a>02101 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpocon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02102"></a>02102                              lda, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l02103"></a>02103                              iwork, LAPACK_INTEGER *info);
<a name="l02104"></a>02104  
<a name="l02105"></a>02105 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpoequ_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, 
<a name="l02106"></a>02106                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l02107"></a>02107  
<a name="l02108"></a>02108 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dporfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02109"></a>02109                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, 
<a name="l02110"></a>02110                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l02111"></a>02111                              ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l02112"></a>02112                              info);
<a name="l02113"></a>02113  
<a name="l02114"></a>02114 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dposv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL 
<a name="l02115"></a>02115                             *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02116"></a>02116  
<a name="l02117"></a>02117 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dposvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02118"></a>02118                              nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, 
<a name="l02119"></a>02119                              <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l02120"></a>02120                              x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *
<a name="l02121"></a>02121                              berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02122"></a>02122  
<a name="l02123"></a>02123 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpotf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02124"></a>02124                              lda, LAPACK_INTEGER *info);
<a name="l02125"></a>02125  
<a name="l02126"></a>02126 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpotrf_(<span class="keyword">const</span> <span class="keywordtype">char</span> *uplo, <span class="keyword">const</span> LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, <span class="keyword">const</span> LAPACK_INTEGER *
<a name="l02127"></a>02127                              lda, LAPACK_INTEGER *info);
<a name="l02128"></a>02128  
<a name="l02129"></a>02129 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpotri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02130"></a>02130                              lda, LAPACK_INTEGER *info);
<a name="l02131"></a>02131  
<a name="l02132"></a>02132 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpotrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02133"></a>02133                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l02134"></a>02134                              info);
<a name="l02135"></a>02135  
<a name="l02136"></a>02136 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dppcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, 
<a name="l02137"></a>02137                              LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l02138"></a>02138                              iwork, LAPACK_INTEGER *info);
<a name="l02139"></a>02139  
<a name="l02140"></a>02140 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dppequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, 
<a name="l02141"></a>02141                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l02142"></a>02142  
<a name="l02143"></a>02143 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02144"></a>02144                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *afp, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02145"></a>02145                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l02146"></a>02146                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02147"></a>02147  
<a name="l02148"></a>02148 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dppsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL 
<a name="l02149"></a>02149                             *ap, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02150"></a>02150  
<a name="l02151"></a>02151 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dppsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02152"></a>02152                              nrhs, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *afp, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *s, 
<a name="l02153"></a>02153                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l02154"></a>02154                              rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l02155"></a>02155                              iwork, LAPACK_INTEGER *info);
<a name="l02156"></a>02156  
<a name="l02157"></a>02157 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *
<a name="l02158"></a>02158                              info);
<a name="l02159"></a>02159  
<a name="l02160"></a>02160 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *
<a name="l02161"></a>02161                              info);
<a name="l02162"></a>02162  
<a name="l02163"></a>02163 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02164"></a>02164                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02165"></a>02165  
<a name="l02166"></a>02166 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dptcon_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l02167"></a>02167                              LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02168"></a>02168  
<a name="l02169"></a>02169 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l02170"></a>02170                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02171"></a>02171                              LAPACK_INTEGER *info);
<a name="l02172"></a>02172  
<a name="l02173"></a>02173 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dptrfs_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *d__, 
<a name="l02174"></a>02174                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *df, LAPACK_DOUBLEREAL *ef, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER 
<a name="l02175"></a>02175                              *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr,
<a name="l02176"></a>02176                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02177"></a>02177  
<a name="l02178"></a>02178 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dptsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *d__, 
<a name="l02179"></a>02179                             LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02180"></a>02180  
<a name="l02181"></a>02181 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dptsvx_(<span class="keywordtype">char</span> *fact, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02182"></a>02182                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *df, LAPACK_DOUBLEREAL *ef, 
<a name="l02183"></a>02183                              LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *
<a name="l02184"></a>02184                              rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l02185"></a>02185                              info);
<a name="l02186"></a>02186  
<a name="l02187"></a>02187 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpttrf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l02188"></a>02188                              LAPACK_INTEGER *info);
<a name="l02189"></a>02189  
<a name="l02190"></a>02190 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dpttrs_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *d__, 
<a name="l02191"></a>02191                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02192"></a>02192  
<a name="l02193"></a>02193 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dptts2_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *d__, 
<a name="l02194"></a>02194                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb);
<a name="l02195"></a>02195  
<a name="l02196"></a>02196 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> drscl_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *sa, LAPACK_DOUBLEREAL *sx, 
<a name="l02197"></a>02197                             LAPACK_INTEGER *incx);
<a name="l02198"></a>02198  
<a name="l02199"></a>02199 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l02200"></a>02200                             LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02201"></a>02201                             LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02202"></a>02202  
<a name="l02203"></a>02203 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l02204"></a>02204                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02205"></a>02205                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l02206"></a>02206                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02207"></a>02207  
<a name="l02208"></a>02208 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02209"></a>02209                              LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *
<a name="l02210"></a>02210                              ldq, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, 
<a name="l02211"></a>02211                              LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02212"></a>02212                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, 
<a name="l02213"></a>02213                              LAPACK_INTEGER *info);
<a name="l02214"></a>02214  
<a name="l02215"></a>02215 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbgst_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l02216"></a>02216                              LAPACK_INTEGER *kb, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *bb, LAPACK_INTEGER *
<a name="l02217"></a>02217                              ldbb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02218"></a>02218  
<a name="l02219"></a>02219 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbgv_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l02220"></a>02220                             LAPACK_INTEGER *kb, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *bb, LAPACK_INTEGER *
<a name="l02221"></a>02221                             ldbb, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02222"></a>02222                             LAPACK_INTEGER *info);
<a name="l02223"></a>02223  
<a name="l02224"></a>02224 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbgvd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l02225"></a>02225                              LAPACK_INTEGER *kb, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *bb, LAPACK_INTEGER *
<a name="l02226"></a>02226                              ldbb, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02227"></a>02227                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02228"></a>02228  
<a name="l02229"></a>02229 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbgvx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02230"></a>02230                              LAPACK_INTEGER *ka, LAPACK_INTEGER *kb, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *
<a name="l02231"></a>02231                              bb, LAPACK_INTEGER *ldbb, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *vl, 
<a name="l02232"></a>02232                              LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER 
<a name="l02233"></a>02233                              *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02234"></a>02234                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02235"></a>02235  
<a name="l02236"></a>02236 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsbtrd_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l02237"></a>02237                              LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l02238"></a>02238                              LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02239"></a>02239  
<a name="l02240"></a>02240 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *
<a name="l02241"></a>02241                              ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER 
<a name="l02242"></a>02242                              *iwork, LAPACK_INTEGER *info);
<a name="l02243"></a>02243  
<a name="l02244"></a>02244 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, <span class="keyword">const</span> LAPACK_DOUBLEREAL *
<a name="l02245"></a>02245                             ap, <span class="keyword">const</span> LAPACK_DOUBLEREAL *w, <span class="keyword">const</span> LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02246"></a>02246                             LAPACK_INTEGER *info);
<a name="l02247"></a>02247  
<a name="l02248"></a>02248 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02249"></a>02249                              ap, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02250"></a>02250                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02251"></a>02251  
<a name="l02252"></a>02252 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02253"></a>02253                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *
<a name="l02254"></a>02254                              iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02255"></a>02255                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, 
<a name="l02256"></a>02256                              LAPACK_INTEGER *info);
<a name="l02257"></a>02257  
<a name="l02258"></a>02258 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspgst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02259"></a>02259                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *bp, LAPACK_INTEGER *info);
<a name="l02260"></a>02260  
<a name="l02261"></a>02261 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspgv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l02262"></a>02262                             n, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *bp, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02263"></a>02263                             LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02264"></a>02264  
<a name="l02265"></a>02265 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspgvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l02266"></a>02266                              n, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *bp, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02267"></a>02267                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l02268"></a>02268                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02269"></a>02269  
<a name="l02270"></a>02270 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspgvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l02271"></a>02271                              uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *bp, LAPACK_DOUBLEREAL *vl, 
<a name="l02272"></a>02272                              LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER 
<a name="l02273"></a>02273                              *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02274"></a>02274                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02275"></a>02275  
<a name="l02276"></a>02276 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02277"></a>02277                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *afp, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, 
<a name="l02278"></a>02278                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, 
<a name="l02279"></a>02279                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02280"></a>02280  
<a name="l02281"></a>02281 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL 
<a name="l02282"></a>02282                             *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02283"></a>02283  
<a name="l02284"></a>02284 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dspsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02285"></a>02285                              nrhs, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *afp, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, 
<a name="l02286"></a>02286                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, 
<a name="l02287"></a>02287                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l02288"></a>02288                              LAPACK_INTEGER *info);
<a name="l02289"></a>02289  
<a name="l02290"></a>02290 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsptrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, 
<a name="l02291"></a>02291                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tau, LAPACK_INTEGER *info);
<a name="l02292"></a>02292  
<a name="l02293"></a>02293 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *
<a name="l02294"></a>02294                              ipiv, LAPACK_INTEGER *info);
<a name="l02295"></a>02295  
<a name="l02296"></a>02296 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *
<a name="l02297"></a>02297                              ipiv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02298"></a>02298  
<a name="l02299"></a>02299 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02300"></a>02300                              LAPACK_DOUBLEREAL *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l02301"></a>02301                              info);
<a name="l02302"></a>02302  
<a name="l02303"></a>02303 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstebz_(<span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *order, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL 
<a name="l02304"></a>02304                              *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, 
<a name="l02305"></a>02305                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_INTEGER *m, LAPACK_INTEGER *nsplit, 
<a name="l02306"></a>02306                              LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *isplit, LAPACK_DOUBLEREAL *work, 
<a name="l02307"></a>02307                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02308"></a>02308  
<a name="l02309"></a>02309 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstedc_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l02310"></a>02310                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02311"></a>02311                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02312"></a>02312  
<a name="l02313"></a>02313 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstegr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02314"></a>02314                              d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, 
<a name="l02315"></a>02315                              LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l02316"></a>02316                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work, 
<a name="l02317"></a>02317                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02318"></a>02318  
<a name="l02319"></a>02319 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstein_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l02320"></a>02320                              LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *isplit, 
<a name="l02321"></a>02321                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l02322"></a>02322                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02323"></a>02323  
<a name="l02324"></a>02324 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l02325"></a>02325                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02326"></a>02326                              LAPACK_INTEGER *info);
<a name="l02327"></a>02327  
<a name="l02328"></a>02328 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsterf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l02329"></a>02329                              LAPACK_INTEGER *info);
<a name="l02330"></a>02330  
<a name="l02331"></a>02331 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstev_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l02332"></a>02332                             LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02333"></a>02333                             LAPACK_INTEGER *info);
<a name="l02334"></a>02334  
<a name="l02335"></a>02335 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstevd_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l02336"></a>02336                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l02337"></a>02337                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02338"></a>02338  
<a name="l02339"></a>02339 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02340"></a>02340                              d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, 
<a name="l02341"></a>02341                              LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l02342"></a>02342                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work, 
<a name="l02343"></a>02343                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02344"></a>02344  
<a name="l02345"></a>02345 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dstevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02346"></a>02346                              d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, 
<a name="l02347"></a>02347                              LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l02348"></a>02348                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l02349"></a>02349                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02350"></a>02350  
<a name="l02351"></a>02351 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsycon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02352"></a>02352                              lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *
<a name="l02353"></a>02353                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02354"></a>02354  
<a name="l02355"></a>02355 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsyev_(<span class="keyword">const</span> <span class="keywordtype">char</span> *jobz, <span class="keyword">const</span> <span class="keywordtype">char</span> *uplo, <span class="keyword">const</span> LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a,
<a name="l02356"></a>02356                             <span class="keyword">const</span> LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *work, <span class="keyword">const</span> LAPACK_INTEGER *lwork, 
<a name="l02357"></a>02357                             LAPACK_INTEGER *info);
<a name="l02358"></a>02358  
<a name="l02359"></a>02359 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsyevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02360"></a>02360                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02361"></a>02361                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02362"></a>02362  
<a name="l02363"></a>02363 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsyevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02364"></a>02364                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *
<a name="l02365"></a>02365                              il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l02366"></a>02366                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work, 
<a name="l02367"></a>02367                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02368"></a>02368  
<a name="l02369"></a>02369 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsyevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02370"></a>02370                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *
<a name="l02371"></a>02371                              il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l02372"></a>02372                              LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, 
<a name="l02373"></a>02373                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02374"></a>02374  
<a name="l02375"></a>02375 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsygs2_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02376"></a>02376                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l02377"></a>02377                              info);
<a name="l02378"></a>02378  
<a name="l02379"></a>02379 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsygst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l02380"></a>02380                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l02381"></a>02381                              info);
<a name="l02382"></a>02382  
<a name="l02383"></a>02383 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsygv_(<span class="keyword">const</span> LAPACK_INTEGER *itype, <span class="keyword">const</span> <span class="keywordtype">char</span> *jobz, <span class="keyword">const</span> <span class="keywordtype">char</span> *uplo, <span class="keyword">const</span> LAPACK_INTEGER *
<a name="l02384"></a>02384                             n, LAPACK_DOUBLEREAL *a, <span class="keyword">const</span> LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, <span class="keyword">const</span> LAPACK_INTEGER *ldb, 
<a name="l02385"></a>02385                             LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *work, <span class="keyword">const</span> LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02386"></a>02386  
<a name="l02387"></a>02387 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsygvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l02388"></a>02388                              n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02389"></a>02389                              LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l02390"></a>02390                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l02391"></a>02391  
<a name="l02392"></a>02392 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsygvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l02393"></a>02393                              uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER 
<a name="l02394"></a>02394                              *ldb, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, 
<a name="l02395"></a>02395                              LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLEREAL *z__, 
<a name="l02396"></a>02396                              LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l02397"></a>02397                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l02398"></a>02398  
<a name="l02399"></a>02399 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsyrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02400"></a>02400                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *
<a name="l02401"></a>02401                              ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, 
<a name="l02402"></a>02402                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l02403"></a>02403                              LAPACK_INTEGER *info);
<a name="l02404"></a>02404  
<a name="l02405"></a>02405 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsysv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL 
<a name="l02406"></a>02406                             *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02407"></a>02407                             LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02408"></a>02408  
<a name="l02409"></a>02409 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsysvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02410"></a>02410                              nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *af, LAPACK_INTEGER *ldaf, 
<a name="l02411"></a>02411                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *
<a name="l02412"></a>02412                              ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l02413"></a>02413                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02414"></a>02414  
<a name="l02415"></a>02415 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytd2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02416"></a>02416                              lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tau, LAPACK_INTEGER *info);
<a name="l02417"></a>02417  
<a name="l02418"></a>02418 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02419"></a>02419                              lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02420"></a>02420  
<a name="l02421"></a>02421 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02422"></a>02422                              lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *
<a name="l02423"></a>02423                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02424"></a>02424  
<a name="l02425"></a>02425 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02426"></a>02426                              lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02427"></a>02427  
<a name="l02428"></a>02428 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02429"></a>02429                              lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02430"></a>02430  
<a name="l02431"></a>02431 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dsytrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l02432"></a>02432                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l02433"></a>02433                              ldb, LAPACK_INTEGER *info);
<a name="l02434"></a>02434  
<a name="l02435"></a>02435 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtbcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02436"></a>02436                              LAPACK_INTEGER *kd, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *rcond, 
<a name="l02437"></a>02437                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02438"></a>02438  
<a name="l02439"></a>02439 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtbrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02440"></a>02440                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL 
<a name="l02441"></a>02441                              *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, 
<a name="l02442"></a>02442                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02443"></a>02443  
<a name="l02444"></a>02444 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtbtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02445"></a>02445                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL 
<a name="l02446"></a>02446                              *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02447"></a>02447  
<a name="l02448"></a>02448 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l02449"></a>02449                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02450"></a>02450                              LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER 
<a name="l02451"></a>02451                              *mm, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02452"></a>02452  
<a name="l02453"></a>02453 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgex2_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l02454"></a>02454                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l02455"></a>02455                              q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *j1, LAPACK_INTEGER *
<a name="l02456"></a>02456                              n1, LAPACK_INTEGER *n2, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02457"></a>02457  
<a name="l02458"></a>02458 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgexc_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l02459"></a>02459                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *
<a name="l02460"></a>02460                              q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *ifst, 
<a name="l02461"></a>02461                              LAPACK_INTEGER *ilst, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02462"></a>02462  
<a name="l02463"></a>02463 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgsen_(LAPACK_INTEGER *ijob, LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, 
<a name="l02464"></a>02464                              LAPACK_LOGICAL *select, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *
<a name="l02465"></a>02465                              b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alphar, LAPACK_DOUBLEREAL *alphai, LAPACK_DOUBLEREAL *
<a name="l02466"></a>02466                              beta, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *z__, LAPACK_INTEGER *ldz, 
<a name="l02467"></a>02467                              LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *pl, LAPACK_DOUBLEREAL *pr, LAPACK_DOUBLEREAL *dif, 
<a name="l02468"></a>02468                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, 
<a name="l02469"></a>02469                              LAPACK_INTEGER *info);
<a name="l02470"></a>02470  
<a name="l02471"></a>02471 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgsja_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l02472"></a>02472                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLEREAL *a, 
<a name="l02473"></a>02473                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *tola, 
<a name="l02474"></a>02474                              LAPACK_DOUBLEREAL *tolb, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLEREAL *u, 
<a name="l02475"></a>02475                              LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *
<a name="l02476"></a>02476                              ldq, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *ncycle, LAPACK_INTEGER *info);
<a name="l02477"></a>02477  
<a name="l02478"></a>02478 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l02479"></a>02479                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02480"></a>02480                              LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, 
<a name="l02481"></a>02481                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *dif, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *
<a name="l02482"></a>02482                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02483"></a>02483  
<a name="l02484"></a>02484 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgsy2_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l02485"></a>02485                              n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02486"></a>02486                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *d__, LAPACK_INTEGER *ldd, 
<a name="l02487"></a>02487                              LAPACK_DOUBLEREAL *e, LAPACK_INTEGER *lde, LAPACK_DOUBLEREAL *f, LAPACK_INTEGER *ldf, LAPACK_DOUBLEREAL *
<a name="l02488"></a>02488                              scale, LAPACK_DOUBLEREAL *rdsum, LAPACK_DOUBLEREAL *rdscal, LAPACK_INTEGER *iwork, LAPACK_INTEGER 
<a name="l02489"></a>02489                              *pq, LAPACK_INTEGER *info);
<a name="l02490"></a>02490  
<a name="l02491"></a>02491 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtgsyl_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l02492"></a>02492                              n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02493"></a>02493                              LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *d__, LAPACK_INTEGER *ldd, 
<a name="l02494"></a>02494                              LAPACK_DOUBLEREAL *e, LAPACK_INTEGER *lde, LAPACK_DOUBLEREAL *f, LAPACK_INTEGER *ldf, LAPACK_DOUBLEREAL *
<a name="l02495"></a>02495                              scale, LAPACK_DOUBLEREAL *dif, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l02496"></a>02496                              iwork, LAPACK_INTEGER *info);
<a name="l02497"></a>02497  
<a name="l02498"></a>02498 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtpcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02499"></a>02499                              LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l02500"></a>02500                              LAPACK_INTEGER *info);
<a name="l02501"></a>02501  
<a name="l02502"></a>02502 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtprfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02503"></a>02503                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, 
<a name="l02504"></a>02504                              LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l02505"></a>02505                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02506"></a>02506  
<a name="l02507"></a>02507 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtptri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02508"></a>02508                              ap, LAPACK_INTEGER *info);
<a name="l02509"></a>02509  
<a name="l02510"></a>02510 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtptrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02511"></a>02511                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *ap, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l02512"></a>02512                              info);
<a name="l02513"></a>02513  
<a name="l02514"></a>02514 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02515"></a>02515                              LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *work, 
<a name="l02516"></a>02516                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02517"></a>02517  
<a name="l02518"></a>02518 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l02519"></a>02519                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *
<a name="l02520"></a>02520                              ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, 
<a name="l02521"></a>02521                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02522"></a>02522  
<a name="l02523"></a>02523 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrexc_(<span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *
<a name="l02524"></a>02524                              ldt, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *ifst, LAPACK_INTEGER *ilst, 
<a name="l02525"></a>02525                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *info);
<a name="l02526"></a>02526  
<a name="l02527"></a>02527 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02528"></a>02528                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l02529"></a>02529                              ldb, LAPACK_DOUBLEREAL *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l02530"></a>02530                              LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02531"></a>02531  
<a name="l02532"></a>02532 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrsen_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, LAPACK_LOGICAL *select, LAPACK_INTEGER 
<a name="l02533"></a>02533                              *n, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *q, LAPACK_INTEGER *ldq, 
<a name="l02534"></a>02534                              LAPACK_DOUBLEREAL *wr, LAPACK_DOUBLEREAL *wi, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL 
<a name="l02535"></a>02535                              *sep, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l02536"></a>02536                              liwork, LAPACK_INTEGER *info);
<a name="l02537"></a>02537  
<a name="l02538"></a>02538 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l02539"></a>02539                              LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLEREAL *vl, LAPACK_INTEGER *
<a name="l02540"></a>02540                              ldvl, LAPACK_DOUBLEREAL *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *sep, 
<a name="l02541"></a>02541                              LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *ldwork, LAPACK_INTEGER *
<a name="l02542"></a>02542                              iwork, LAPACK_INTEGER *info);
<a name="l02543"></a>02543  
<a name="l02544"></a>02544 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrsyl_(<span class="keywordtype">char</span> *trana, <span class="keywordtype">char</span> *tranb, LAPACK_INTEGER *isgn, LAPACK_INTEGER 
<a name="l02545"></a>02545                              *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l02546"></a>02546                              ldb, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *scale, LAPACK_INTEGER *info);
<a name="l02547"></a>02547  
<a name="l02548"></a>02548 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrti2_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02549"></a>02549                              a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l02550"></a>02550  
<a name="l02551"></a>02551 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrtri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l02552"></a>02552                              a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l02553"></a>02553  
<a name="l02554"></a>02554 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtrtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l02555"></a>02555                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *
<a name="l02556"></a>02556                              ldb, LAPACK_INTEGER *info);
<a name="l02557"></a>02557  
<a name="l02558"></a>02558 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtzrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02559"></a>02559                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_INTEGER *info);
<a name="l02560"></a>02560  
<a name="l02561"></a>02561 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> dtzrzf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l02562"></a>02562                              lda, LAPACK_DOUBLEREAL *tau, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02563"></a>02563  
<a name="l02564"></a>02564 LAPACK_INTEGER icmax1_(LAPACK_INTEGER *n, LAPACK_COMPLEX *cx, LAPACK_INTEGER *incx);
<a name="l02565"></a>02565  
<a name="l02566"></a>02566 LAPACK_INTEGER ieeeck_(LAPACK_INTEGER *ispec, LAPACK_REAL *zero, LAPACK_REAL *one);
<a name="l02567"></a>02567  
<a name="l02568"></a>02568 LAPACK_INTEGER ilaenv_(<span class="keyword">const</span> LAPACK_INTEGER *ispec, <span class="keywordtype">char</span> *name__, <span class="keyword">const</span> <span class="keywordtype">char</span> *opts, <span class="keyword">const</span> LAPACK_INTEGER *n1, 
<a name="l02569"></a>02569                        <span class="keyword">const</span> LAPACK_INTEGER *n2, <span class="keyword">const</span> LAPACK_INTEGER *n3, <span class="keyword">const</span> LAPACK_INTEGER *n4, LAPACK_FTNLEN name_len, LAPACK_FTNLEN 
<a name="l02570"></a>02570                        opts_len);
<a name="l02571"></a>02571  
<a name="l02572"></a>02572 LAPACK_INTEGER izmax1_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *cx, LAPACK_INTEGER *incx);
<a name="l02573"></a>02573  
<a name="l02574"></a>02574 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sbdsdc_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_REAL *d__, 
<a name="l02575"></a>02575                              LAPACK_REAL *e, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt, LAPACK_REAL *q, 
<a name="l02576"></a>02576                              LAPACK_INTEGER *iq, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02577"></a>02577  
<a name="l02578"></a>02578 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sbdsqr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ncvt, LAPACK_INTEGER *
<a name="l02579"></a>02579                              nru, LAPACK_INTEGER *ncc, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt, LAPACK_REAL *
<a name="l02580"></a>02580                              u, LAPACK_INTEGER *ldu, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02581"></a>02581  
<a name="l02582"></a>02582 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sdisna_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *d__, 
<a name="l02583"></a>02583                              LAPACK_REAL *sep, LAPACK_INTEGER *info);
<a name="l02584"></a>02584  
<a name="l02585"></a>02585 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbbrd_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *ncc,
<a name="l02586"></a>02586                              LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *d__, LAPACK_REAL *
<a name="l02587"></a>02587                              e, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *pt, LAPACK_INTEGER *ldpt, LAPACK_REAL *c__, LAPACK_INTEGER 
<a name="l02588"></a>02588                              *ldc, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02589"></a>02589  
<a name="l02590"></a>02590 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l02591"></a>02591                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, 
<a name="l02592"></a>02592                              LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02593"></a>02593  
<a name="l02594"></a>02594 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l02595"></a>02595                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *
<a name="l02596"></a>02596                              colcnd, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l02597"></a>02597  
<a name="l02598"></a>02598 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l02599"></a>02599                              ku, LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *afb, LAPACK_INTEGER *ldafb,
<a name="l02600"></a>02600                              LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *
<a name="l02601"></a>02601                              ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02602"></a>02602  
<a name="l02603"></a>02603 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_INTEGER *
<a name="l02604"></a>02604                             nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, 
<a name="l02605"></a>02605                             LAPACK_INTEGER *info);
<a name="l02606"></a>02606  
<a name="l02607"></a>02607 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl,
<a name="l02608"></a>02608                              LAPACK_INTEGER *ku, LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *afb, 
<a name="l02609"></a>02609                              LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, LAPACK_REAL *r__, LAPACK_REAL *c__, 
<a name="l02610"></a>02610                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr,
<a name="l02611"></a>02611                              LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02612"></a>02612  
<a name="l02613"></a>02613 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbtf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l02614"></a>02614                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02615"></a>02615  
<a name="l02616"></a>02616 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbtrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l02617"></a>02617                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02618"></a>02618  
<a name="l02619"></a>02619 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgbtrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l02620"></a>02620                              ku, LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, 
<a name="l02621"></a>02621                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02622"></a>02622  
<a name="l02623"></a>02623 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgebak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l02624"></a>02624                              LAPACK_INTEGER *ihi, LAPACK_REAL *scale, LAPACK_INTEGER *m, LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_INTEGER 
<a name="l02625"></a>02625                              *info);
<a name="l02626"></a>02626  
<a name="l02627"></a>02627 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgebal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02628"></a>02628                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *scale, LAPACK_INTEGER *info);
<a name="l02629"></a>02629  
<a name="l02630"></a>02630 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgebd2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02631"></a>02631                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tauq, LAPACK_REAL *taup, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02632"></a>02632  
<a name="l02633"></a>02633 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgebrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02634"></a>02634                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tauq, LAPACK_REAL *taup, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l02635"></a>02635                              lwork, LAPACK_INTEGER *info);
<a name="l02636"></a>02636  
<a name="l02637"></a>02637 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgecon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02638"></a>02638                              LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02639"></a>02639  
<a name="l02640"></a>02640 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02641"></a>02641                              LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *colcnd, LAPACK_REAL *amax, LAPACK_INTEGER 
<a name="l02642"></a>02642                              *info);
<a name="l02643"></a>02643  
<a name="l02644"></a>02644 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgees_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, LAPACK_INTEGER *n, 
<a name="l02645"></a>02645                             LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *vs, 
<a name="l02646"></a>02646                             LAPACK_INTEGER *ldvs, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *
<a name="l02647"></a>02647                             info);
<a name="l02648"></a>02648  
<a name="l02649"></a>02649 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeesx_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, <span class="keywordtype">char</span> *
<a name="l02650"></a>02650                              sense, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_REAL *wr, 
<a name="l02651"></a>02651                              LAPACK_REAL *wi, LAPACK_REAL *vs, LAPACK_INTEGER *ldvs, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_REAL *
<a name="l02652"></a>02652                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_LOGICAL *bwork,
<a name="l02653"></a>02653                              LAPACK_INTEGER *info);
<a name="l02654"></a>02654  
<a name="l02655"></a>02655 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02656"></a>02656                             LAPACK_INTEGER *lda, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, 
<a name="l02657"></a>02657                             LAPACK_INTEGER *ldvr, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02658"></a>02658  
<a name="l02659"></a>02659 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l02660"></a>02660                              sense, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *
<a name="l02661"></a>02661                              vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *ilo, LAPACK_INTEGER *
<a name="l02662"></a>02662                              ihi, LAPACK_REAL *scale, LAPACK_REAL *abnrm, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_REAL *work,
<a name="l02663"></a>02663                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02664"></a>02664  
<a name="l02665"></a>02665 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgegs_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02666"></a>02666                             LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL 
<a name="l02667"></a>02667                             *beta, LAPACK_REAL *vsl, LAPACK_INTEGER *ldvsl, LAPACK_REAL *vsr, LAPACK_INTEGER *ldvsr, LAPACK_REAL *
<a name="l02668"></a>02668                             work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02669"></a>02669  
<a name="l02670"></a>02670 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgegv_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02671"></a>02671                             LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL 
<a name="l02672"></a>02672                             *beta, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_REAL *work, 
<a name="l02673"></a>02673                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02674"></a>02674  
<a name="l02675"></a>02675 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgehd2_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, 
<a name="l02676"></a>02676                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02677"></a>02677  
<a name="l02678"></a>02678 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgehrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, 
<a name="l02679"></a>02679                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02680"></a>02680  
<a name="l02681"></a>02681 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02682"></a>02682                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02683"></a>02683  
<a name="l02684"></a>02684 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02685"></a>02685                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02686"></a>02686  
<a name="l02687"></a>02687 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgels_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02688"></a>02688                             nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *work, 
<a name="l02689"></a>02689                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02690"></a>02690  
<a name="l02691"></a>02691 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelsd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02692"></a>02692                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *s, LAPACK_REAL *rcond, LAPACK_INTEGER *
<a name="l02693"></a>02693                              rank, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02694"></a>02694  
<a name="l02695"></a>02695 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelss_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02696"></a>02696                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *s, LAPACK_REAL *rcond, LAPACK_INTEGER *
<a name="l02697"></a>02697                              rank, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02698"></a>02698  
<a name="l02699"></a>02699 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelsx_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02700"></a>02700                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *jpvt, LAPACK_REAL *rcond, 
<a name="l02701"></a>02701                              LAPACK_INTEGER *rank, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02702"></a>02702  
<a name="l02703"></a>02703 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgelsy_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02704"></a>02704                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *jpvt, LAPACK_REAL *rcond, 
<a name="l02705"></a>02705                              LAPACK_INTEGER *rank, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02706"></a>02706  
<a name="l02707"></a>02707 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeql2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02708"></a>02708                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02709"></a>02709  
<a name="l02710"></a>02710 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeqlf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02711"></a>02711                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02712"></a>02712  
<a name="l02713"></a>02713 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeqp3_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02714"></a>02714                              LAPACK_INTEGER *jpvt, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02715"></a>02715  
<a name="l02716"></a>02716 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeqpf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02717"></a>02717                              LAPACK_INTEGER *jpvt, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02718"></a>02718  
<a name="l02719"></a>02719 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeqr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02720"></a>02720                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02721"></a>02721  
<a name="l02722"></a>02722 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgeqrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02723"></a>02723                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02724"></a>02724  
<a name="l02725"></a>02725 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgerfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02726"></a>02726                              LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, 
<a name="l02727"></a>02727                              LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *
<a name="l02728"></a>02728                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02729"></a>02729  
<a name="l02730"></a>02730 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgerq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02731"></a>02731                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02732"></a>02732  
<a name="l02733"></a>02733 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgerqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02734"></a>02734                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02735"></a>02735  
<a name="l02736"></a>02736 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgesc2_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *rhs, 
<a name="l02737"></a>02737                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv, LAPACK_REAL *scale);
<a name="l02738"></a>02738  
<a name="l02739"></a>02739 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgesdd_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02740"></a>02740                              LAPACK_INTEGER *lda, LAPACK_REAL *s, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt,
<a name="l02741"></a>02741                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02742"></a>02742  
<a name="l02743"></a>02743 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgesv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02744"></a>02744                             LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02745"></a>02745  
<a name="l02746"></a>02746 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgesvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobvt, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l02747"></a>02747                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *s, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, 
<a name="l02748"></a>02748                              LAPACK_INTEGER *ldvt, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02749"></a>02749  
<a name="l02750"></a>02750 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02751"></a>02751                              nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, 
<a name="l02752"></a>02752                              <span class="keywordtype">char</span> *equed, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, 
<a name="l02753"></a>02753                              LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, 
<a name="l02754"></a>02754                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02755"></a>02755  
<a name="l02756"></a>02756 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgetc2_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv,
<a name="l02757"></a>02757                              LAPACK_INTEGER *jpiv, LAPACK_INTEGER *info);
<a name="l02758"></a>02758  
<a name="l02759"></a>02759 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgetf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02760"></a>02760                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02761"></a>02761  
<a name="l02762"></a>02762 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgetrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02763"></a>02763                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02764"></a>02764  
<a name="l02765"></a>02765 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgetri_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv,
<a name="l02766"></a>02766                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02767"></a>02767  
<a name="l02768"></a>02768 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgetrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l02769"></a>02769                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02770"></a>02770  
<a name="l02771"></a>02771 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggbak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l02772"></a>02772                              LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, LAPACK_REAL *rscale, LAPACK_INTEGER *m, LAPACK_REAL *v, 
<a name="l02773"></a>02773                              LAPACK_INTEGER *ldv, LAPACK_INTEGER *info);
<a name="l02774"></a>02774  
<a name="l02775"></a>02775 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggbal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l02776"></a>02776                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, LAPACK_REAL 
<a name="l02777"></a>02777                              *rscale, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02778"></a>02778  
<a name="l02779"></a>02779 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgges_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l02780"></a>02780                             selctg, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, 
<a name="l02781"></a>02781                             LAPACK_INTEGER *sdim, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, LAPACK_REAL *vsl, 
<a name="l02782"></a>02782                             LAPACK_INTEGER *ldvsl, LAPACK_REAL *vsr, LAPACK_INTEGER *ldvsr, LAPACK_REAL *work, LAPACK_INTEGER *lwork,
<a name="l02783"></a>02783                             LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l02784"></a>02784  
<a name="l02785"></a>02785 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggesx_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l02786"></a>02786                              selctg, <span class="keywordtype">char</span> *sense, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, 
<a name="l02787"></a>02787                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, 
<a name="l02788"></a>02788                              LAPACK_REAL *vsl, LAPACK_INTEGER *ldvsl, LAPACK_REAL *vsr, LAPACK_INTEGER *ldvsr, LAPACK_REAL *rconde, 
<a name="l02789"></a>02789                              LAPACK_REAL *rcondv, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l02790"></a>02790                              liwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l02791"></a>02791  
<a name="l02792"></a>02792 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02793"></a>02793                             LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL 
<a name="l02794"></a>02794                             *beta, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_REAL *work, 
<a name="l02795"></a>02795                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02796"></a>02796  
<a name="l02797"></a>02797 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l02798"></a>02798                              sense, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL 
<a name="l02799"></a>02799                              *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, 
<a name="l02800"></a>02800                              LAPACK_INTEGER *ldvr, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *lscale, LAPACK_REAL *rscale,
<a name="l02801"></a>02801                              LAPACK_REAL *abnrm, LAPACK_REAL *bbnrm, LAPACK_REAL *rconde, LAPACK_REAL *rcondv, LAPACK_REAL *work, 
<a name="l02802"></a>02802                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l02803"></a>02803  
<a name="l02804"></a>02804 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggglm_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_REAL *a, 
<a name="l02805"></a>02805                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *d__, LAPACK_REAL *x, LAPACK_REAL *y, 
<a name="l02806"></a>02806                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02807"></a>02807  
<a name="l02808"></a>02808 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgghrd_(<span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02809"></a>02809                              ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL 
<a name="l02810"></a>02810                              *q, LAPACK_INTEGER *ldq, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *info);
<a name="l02811"></a>02811  
<a name="l02812"></a>02812 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgglse_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_REAL *a, 
<a name="l02813"></a>02813                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *c__, LAPACK_REAL *d__, LAPACK_REAL *x, 
<a name="l02814"></a>02814                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02815"></a>02815  
<a name="l02816"></a>02816 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggqrf_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_REAL *a, 
<a name="l02817"></a>02817                              LAPACK_INTEGER *lda, LAPACK_REAL *taua, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *taub, LAPACK_REAL *
<a name="l02818"></a>02818                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02819"></a>02819  
<a name="l02820"></a>02820 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02821"></a>02821                              LAPACK_INTEGER *lda, LAPACK_REAL *taua, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *taub, LAPACK_REAL *
<a name="l02822"></a>02822                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02823"></a>02823  
<a name="l02824"></a>02824 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggsvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l02825"></a>02825                              LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *a, LAPACK_INTEGER *lda,
<a name="l02826"></a>02826                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *alpha, LAPACK_REAL *beta, LAPACK_REAL *u, LAPACK_INTEGER *
<a name="l02827"></a>02827                              ldu, LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *work, 
<a name="l02828"></a>02828                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02829"></a>02829  
<a name="l02830"></a>02830 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sggsvp_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l02831"></a>02831                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, 
<a name="l02832"></a>02832                              LAPACK_REAL *tola, LAPACK_REAL *tolb, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *u, LAPACK_INTEGER *ldu,
<a name="l02833"></a>02833                              LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *iwork, LAPACK_REAL *
<a name="l02834"></a>02834                              tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02835"></a>02835  
<a name="l02836"></a>02836 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgtcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_REAL *dl, LAPACK_REAL *d__, 
<a name="l02837"></a>02837                              LAPACK_REAL *du, LAPACK_REAL *du2, LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *
<a name="l02838"></a>02838                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02839"></a>02839  
<a name="l02840"></a>02840 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgtrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *dl,
<a name="l02841"></a>02841                              LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *dlf, LAPACK_REAL *df, LAPACK_REAL *duf, LAPACK_REAL *du2, 
<a name="l02842"></a>02842                              LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *
<a name="l02843"></a>02843                              ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02844"></a>02844  
<a name="l02845"></a>02845 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgtsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *dl, LAPACK_REAL *d__, 
<a name="l02846"></a>02846                             LAPACK_REAL *du, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l02847"></a>02847  
<a name="l02848"></a>02848 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgtsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l02849"></a>02849                              nrhs, LAPACK_REAL *dl, LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *dlf, LAPACK_REAL *df, LAPACK_REAL *duf, 
<a name="l02850"></a>02850                              LAPACK_REAL *du2, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *
<a name="l02851"></a>02851                              ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l02852"></a>02852                              LAPACK_INTEGER *info);
<a name="l02853"></a>02853  
<a name="l02854"></a>02854 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgttrf_(LAPACK_INTEGER *n, LAPACK_REAL *dl, LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *
<a name="l02855"></a>02855                              du2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l02856"></a>02856  
<a name="l02857"></a>02857 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgttrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *dl,
<a name="l02858"></a>02858                              LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *du2, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb,
<a name="l02859"></a>02859                              LAPACK_INTEGER *info);
<a name="l02860"></a>02860  
<a name="l02861"></a>02861 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sgtts2_(LAPACK_INTEGER *itrans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL 
<a name="l02862"></a>02862                              *dl, LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *du2, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *
<a name="l02863"></a>02863                              ldb);
<a name="l02864"></a>02864  
<a name="l02865"></a>02865 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> shgeqz_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, 
<a name="l02866"></a>02866                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *
<a name="l02867"></a>02867                              ldb, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, LAPACK_REAL *q, LAPACK_INTEGER *ldq, 
<a name="l02868"></a>02868                              LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02869"></a>02869  
<a name="l02870"></a>02870 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> shsein_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *eigsrc, <span class="keywordtype">char</span> *initv, LAPACK_LOGICAL *
<a name="l02871"></a>02871                              select, LAPACK_INTEGER *n, LAPACK_REAL *h__, LAPACK_INTEGER *ldh, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL 
<a name="l02872"></a>02872                              *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, 
<a name="l02873"></a>02873                              LAPACK_REAL *work, LAPACK_INTEGER *ifaill, LAPACK_INTEGER *ifailr, LAPACK_INTEGER *info);
<a name="l02874"></a>02874  
<a name="l02875"></a>02875 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> shseqr_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo,
<a name="l02876"></a>02876                              LAPACK_INTEGER *ihi, LAPACK_REAL *h__, LAPACK_INTEGER *ldh, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *z__,
<a name="l02877"></a>02877                              LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l02878"></a>02878  
<a name="l02879"></a>02879 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slabad_(LAPACK_REAL *small, LAPACK_REAL *large);
<a name="l02880"></a>02880  
<a name="l02881"></a>02881 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slabrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_REAL *a, 
<a name="l02882"></a>02882                              LAPACK_INTEGER *lda, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tauq, LAPACK_REAL *taup, LAPACK_REAL *x, 
<a name="l02883"></a>02883                              LAPACK_INTEGER *ldx, LAPACK_REAL *y, LAPACK_INTEGER *ldy);
<a name="l02884"></a>02884  
<a name="l02885"></a>02885 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slacon_(LAPACK_INTEGER *n, LAPACK_REAL *v, LAPACK_REAL *x, LAPACK_INTEGER *isgn, 
<a name="l02886"></a>02886                              LAPACK_REAL *est, LAPACK_INTEGER *kase);
<a name="l02887"></a>02887  
<a name="l02888"></a>02888 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slacpy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l02889"></a>02889                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb);
<a name="l02890"></a>02890  
<a name="l02891"></a>02891 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sladiv_(LAPACK_REAL *a, LAPACK_REAL *b, LAPACK_REAL *c__, LAPACK_REAL *d__, LAPACK_REAL *p, 
<a name="l02892"></a>02892                              LAPACK_REAL *q);
<a name="l02893"></a>02893  
<a name="l02894"></a>02894 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slae2_(LAPACK_REAL *a, LAPACK_REAL *b, LAPACK_REAL *c__, LAPACK_REAL *rt1, LAPACK_REAL *rt2);
<a name="l02895"></a>02895  
<a name="l02896"></a>02896 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaebz_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *nitmax, LAPACK_INTEGER *n, 
<a name="l02897"></a>02897                              LAPACK_INTEGER *mmax, LAPACK_INTEGER *minp, LAPACK_INTEGER *nbmin, LAPACK_REAL *abstol, LAPACK_REAL *
<a name="l02898"></a>02898                              reltol, LAPACK_REAL *pivmin, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *e2, LAPACK_INTEGER *nval, 
<a name="l02899"></a>02899                              LAPACK_REAL *ab, LAPACK_REAL *c__, LAPACK_INTEGER *mout, LAPACK_INTEGER *nab, LAPACK_REAL *work, LAPACK_INTEGER 
<a name="l02900"></a>02900                              *iwork, LAPACK_INTEGER *info);
<a name="l02901"></a>02901  
<a name="l02902"></a>02902 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *qsiz, LAPACK_INTEGER *n, LAPACK_REAL 
<a name="l02903"></a>02903                              *d__, LAPACK_REAL *e, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *qstore, LAPACK_INTEGER *ldqs, 
<a name="l02904"></a>02904                              LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l02905"></a>02905  
<a name="l02906"></a>02906 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed1_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *q, LAPACK_INTEGER *ldq, 
<a name="l02907"></a>02907                              LAPACK_INTEGER *indxq, LAPACK_REAL *rho, LAPACK_INTEGER *cutpnt, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l02908"></a>02908                              iwork, LAPACK_INTEGER *info);
<a name="l02909"></a>02909  
<a name="l02910"></a>02910 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed2_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *n1, LAPACK_REAL *d__, 
<a name="l02911"></a>02911                              LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_REAL *rho, LAPACK_REAL *z__, LAPACK_REAL *
<a name="l02912"></a>02912                              dlamda, LAPACK_REAL *w, LAPACK_REAL *q2, LAPACK_INTEGER *indx, LAPACK_INTEGER *indxc, LAPACK_INTEGER *
<a name="l02913"></a>02913                              indxp, LAPACK_INTEGER *coltyp, LAPACK_INTEGER *info);
<a name="l02914"></a>02914  
<a name="l02915"></a>02915 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed3_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *n1, LAPACK_REAL *d__, 
<a name="l02916"></a>02916                              LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *rho, LAPACK_REAL *dlamda, LAPACK_REAL *q2, LAPACK_INTEGER *
<a name="l02917"></a>02917                              indx, LAPACK_INTEGER *ctot, LAPACK_REAL *w, LAPACK_REAL *s, LAPACK_INTEGER *info);
<a name="l02918"></a>02918  
<a name="l02919"></a>02919 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed4_(LAPACK_INTEGER *n, LAPACK_INTEGER *i__, LAPACK_REAL *d__, LAPACK_REAL *z__, 
<a name="l02920"></a>02920                              LAPACK_REAL *delta, LAPACK_REAL *rho, LAPACK_REAL *dlam, LAPACK_INTEGER *info);
<a name="l02921"></a>02921  
<a name="l02922"></a>02922 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed5_(LAPACK_INTEGER *i__, LAPACK_REAL *d__, LAPACK_REAL *z__, LAPACK_REAL *delta, 
<a name="l02923"></a>02923                              LAPACK_REAL *rho, LAPACK_REAL *dlam);
<a name="l02924"></a>02924  
<a name="l02925"></a>02925 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed6_(LAPACK_INTEGER *kniter, LAPACK_LOGICAL *orgati, LAPACK_REAL *rho, 
<a name="l02926"></a>02926                              LAPACK_REAL *d__, LAPACK_REAL *z__, LAPACK_REAL *finit, LAPACK_REAL *tau, LAPACK_INTEGER *info);
<a name="l02927"></a>02927  
<a name="l02928"></a>02928 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed7_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *n, LAPACK_INTEGER *qsiz, 
<a name="l02929"></a>02929                              LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, LAPACK_INTEGER *curpbm, LAPACK_REAL *d__, LAPACK_REAL *q, 
<a name="l02930"></a>02930                              LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_REAL *rho, LAPACK_INTEGER *cutpnt, LAPACK_REAL *
<a name="l02931"></a>02931                              qstore, LAPACK_INTEGER *qptr, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *perm, LAPACK_INTEGER *
<a name="l02932"></a>02932                              givptr, LAPACK_INTEGER *givcol, LAPACK_REAL *givnum, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l02933"></a>02933                              LAPACK_INTEGER *info);
<a name="l02934"></a>02934  
<a name="l02935"></a>02935 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed8_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER 
<a name="l02936"></a>02936                              *qsiz, LAPACK_REAL *d__, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *indxq, LAPACK_REAL *rho, 
<a name="l02937"></a>02937                              LAPACK_INTEGER *cutpnt, LAPACK_REAL *z__, LAPACK_REAL *dlamda, LAPACK_REAL *q2, LAPACK_INTEGER *ldq2, 
<a name="l02938"></a>02938                              LAPACK_REAL *w, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_REAL *
<a name="l02939"></a>02939                              givnum, LAPACK_INTEGER *indxp, LAPACK_INTEGER *indx, LAPACK_INTEGER *info);
<a name="l02940"></a>02940  
<a name="l02941"></a>02941 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaed9_(LAPACK_INTEGER *k, LAPACK_INTEGER *kstart, LAPACK_INTEGER *kstop, 
<a name="l02942"></a>02942                              LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *rho, LAPACK_REAL *dlamda,
<a name="l02943"></a>02943                              LAPACK_REAL *w, LAPACK_REAL *s, LAPACK_INTEGER *lds, LAPACK_INTEGER *info);
<a name="l02944"></a>02944  
<a name="l02945"></a>02945 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaeda_(LAPACK_INTEGER *n, LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, 
<a name="l02946"></a>02946                              LAPACK_INTEGER *curpbm, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, 
<a name="l02947"></a>02947                              LAPACK_INTEGER *givcol, LAPACK_REAL *givnum, LAPACK_REAL *q, LAPACK_INTEGER *qptr, LAPACK_REAL *z__, 
<a name="l02948"></a>02948                              LAPACK_REAL *ztemp, LAPACK_INTEGER *info);
<a name="l02949"></a>02949  
<a name="l02950"></a>02950 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaein_(LAPACK_LOGICAL *rightv, LAPACK_LOGICAL *noinit, LAPACK_INTEGER *n, 
<a name="l02951"></a>02951                              LAPACK_REAL *h__, LAPACK_INTEGER *ldh, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *vr, LAPACK_REAL *vi, LAPACK_REAL 
<a name="l02952"></a>02952                              *b, LAPACK_INTEGER *ldb, LAPACK_REAL *work, LAPACK_REAL *eps3, LAPACK_REAL *smlnum, LAPACK_REAL *bignum, 
<a name="l02953"></a>02953                              LAPACK_INTEGER *info);
<a name="l02954"></a>02954  
<a name="l02955"></a>02955 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaev2_(LAPACK_REAL *a, LAPACK_REAL *b, LAPACK_REAL *c__, LAPACK_REAL *rt1, LAPACK_REAL *
<a name="l02956"></a>02956                              rt2, LAPACK_REAL *cs1, LAPACK_REAL *sn1);
<a name="l02957"></a>02957  
<a name="l02958"></a>02958 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaexc_(LAPACK_LOGICAL *wantq, LAPACK_INTEGER *n, LAPACK_REAL *t, LAPACK_INTEGER *
<a name="l02959"></a>02959                              ldt, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *j1, LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, 
<a name="l02960"></a>02960                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l02961"></a>02961  
<a name="l02962"></a>02962 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slag2_(LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, 
<a name="l02963"></a>02963                             LAPACK_REAL *safmin, LAPACK_REAL *scale1, LAPACK_REAL *scale2, LAPACK_REAL *wr1, LAPACK_REAL *wr2, LAPACK_REAL *
<a name="l02964"></a>02964                             wi);
<a name="l02965"></a>02965  
<a name="l02966"></a>02966 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slags2_(LAPACK_LOGICAL *upper, LAPACK_REAL *a1, LAPACK_REAL *a2, LAPACK_REAL *a3, 
<a name="l02967"></a>02967                              LAPACK_REAL *b1, LAPACK_REAL *b2, LAPACK_REAL *b3, LAPACK_REAL *csu, LAPACK_REAL *snu, LAPACK_REAL *csv, LAPACK_REAL *
<a name="l02968"></a>02968                              snv, LAPACK_REAL *csq, LAPACK_REAL *snq);
<a name="l02969"></a>02969  
<a name="l02970"></a>02970 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slagtf_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_REAL *lambda, LAPACK_REAL *b, LAPACK_REAL 
<a name="l02971"></a>02971                              *c__, LAPACK_REAL *tol, LAPACK_REAL *d__, LAPACK_INTEGER *in, LAPACK_INTEGER *info);
<a name="l02972"></a>02972  
<a name="l02973"></a>02973 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slagtm_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *
<a name="l02974"></a>02974                              alpha, LAPACK_REAL *dl, LAPACK_REAL *d__, LAPACK_REAL *du, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *
<a name="l02975"></a>02975                              beta, LAPACK_REAL *b, LAPACK_INTEGER *ldb);
<a name="l02976"></a>02976  
<a name="l02977"></a>02977 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slagts_(LAPACK_INTEGER *job, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_REAL *b, LAPACK_REAL 
<a name="l02978"></a>02978                              *c__, LAPACK_REAL *d__, LAPACK_INTEGER *in, LAPACK_REAL *y, LAPACK_REAL *tol, LAPACK_INTEGER *info);
<a name="l02979"></a>02979  
<a name="l02980"></a>02980 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slagv2_(LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, 
<a name="l02981"></a>02981                              LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, LAPACK_REAL *csl, LAPACK_REAL *snl, LAPACK_REAL *
<a name="l02982"></a>02982                              csr, LAPACK_REAL *snr);
<a name="l02983"></a>02983  
<a name="l02984"></a>02984 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slahqr_(LAPACK_LOGICAL *wantt, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l02985"></a>02985                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *h__, LAPACK_INTEGER *ldh, LAPACK_REAL *wr, LAPACK_REAL *
<a name="l02986"></a>02986                              wi, LAPACK_INTEGER *iloz, LAPACK_INTEGER *ihiz, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *
<a name="l02987"></a>02987                              info);
<a name="l02988"></a>02988  
<a name="l02989"></a>02989 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slahrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *nb, LAPACK_REAL *a, 
<a name="l02990"></a>02990                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *y, LAPACK_INTEGER *ldy);
<a name="l02991"></a>02991  
<a name="l02992"></a>02992 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaic1_(LAPACK_INTEGER *job, LAPACK_INTEGER *j, LAPACK_REAL *x, LAPACK_REAL *sest, 
<a name="l02993"></a>02993                              LAPACK_REAL *w, LAPACK_REAL *gamma, LAPACK_REAL *sestpr, LAPACK_REAL *s, LAPACK_REAL *c__);
<a name="l02994"></a>02994  
<a name="l02995"></a>02995 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaln2_(LAPACK_LOGICAL *ltrans, LAPACK_INTEGER *na, LAPACK_INTEGER *nw, LAPACK_REAL *
<a name="l02996"></a>02996                              smin, LAPACK_REAL *ca, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *d1, LAPACK_REAL *d2, LAPACK_REAL *b, 
<a name="l02997"></a>02997                              LAPACK_INTEGER *ldb, LAPACK_REAL *wr, LAPACK_REAL *wi, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *scale, 
<a name="l02998"></a>02998                              LAPACK_REAL *xnorm, LAPACK_INTEGER *info);
<a name="l02999"></a>02999  
<a name="l03000"></a>03000 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slals0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l03001"></a>03001                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *nrhs, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *bx, 
<a name="l03002"></a>03002                              LAPACK_INTEGER *ldbx, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l03003"></a>03003                              LAPACK_INTEGER *ldgcol, LAPACK_REAL *givnum, LAPACK_INTEGER *ldgnum, LAPACK_REAL *poles, LAPACK_REAL *
<a name="l03004"></a>03004                              difl, LAPACK_REAL *difr, LAPACK_REAL *z__, LAPACK_INTEGER *k, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *
<a name="l03005"></a>03005                              work, LAPACK_INTEGER *info);
<a name="l03006"></a>03006  
<a name="l03007"></a>03007 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slalsa_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l03008"></a>03008                              LAPACK_INTEGER *nrhs, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *bx, LAPACK_INTEGER *ldbx, LAPACK_REAL *
<a name="l03009"></a>03009                              u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, LAPACK_INTEGER *k, LAPACK_REAL *difl, LAPACK_REAL *difr, LAPACK_REAL *
<a name="l03010"></a>03010                              z__, LAPACK_REAL *poles, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, 
<a name="l03011"></a>03011                              LAPACK_INTEGER *perm, LAPACK_REAL *givnum, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03012"></a>03012                              iwork, LAPACK_INTEGER *info);
<a name="l03013"></a>03013  
<a name="l03014"></a>03014 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slalsd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, LAPACK_INTEGER 
<a name="l03015"></a>03015                              *nrhs, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *rcond, 
<a name="l03016"></a>03016                              LAPACK_INTEGER *rank, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03017"></a>03017  
<a name="l03018"></a>03018 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slamc1_(LAPACK_INTEGER *beta, LAPACK_INTEGER *t, LAPACK_LOGICAL *rnd, LAPACK_LOGICAL 
<a name="l03019"></a>03019                              *ieee1);
<a name="l03020"></a>03020  
<a name="l03021"></a>03021 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slamc2_(LAPACK_INTEGER *beta, LAPACK_INTEGER *t, LAPACK_LOGICAL *rnd, LAPACK_REAL *
<a name="l03022"></a>03022                              eps, LAPACK_INTEGER *emin, LAPACK_REAL *rmin, LAPACK_INTEGER *emax, LAPACK_REAL *rmax);
<a name="l03023"></a>03023  
<a name="l03024"></a>03024 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slamc4_(LAPACK_INTEGER *emin, LAPACK_REAL *start, LAPACK_INTEGER *base);
<a name="l03025"></a>03025  
<a name="l03026"></a>03026 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slamc5_(LAPACK_INTEGER *beta, LAPACK_INTEGER *p, LAPACK_INTEGER *emin, 
<a name="l03027"></a>03027                              LAPACK_LOGICAL *ieee, LAPACK_INTEGER *emax, LAPACK_REAL *rmax);
<a name="l03028"></a>03028  
<a name="l03029"></a>03029 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slamrg_(LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, LAPACK_REAL *a, LAPACK_INTEGER *
<a name="l03030"></a>03030                              strd1, LAPACK_INTEGER *strd2, LAPACK_INTEGER *index);
<a name="l03031"></a>03031  
<a name="l03032"></a>03032 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slanv2_(LAPACK_REAL *a, LAPACK_REAL *b, LAPACK_REAL *c__, LAPACK_REAL *d__, LAPACK_REAL *
<a name="l03033"></a>03033                              rt1r, LAPACK_REAL *rt1i, LAPACK_REAL *rt2r, LAPACK_REAL *rt2i, LAPACK_REAL *cs, LAPACK_REAL *sn);
<a name="l03034"></a>03034  
<a name="l03035"></a>03035 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slapll_(LAPACK_INTEGER *n, LAPACK_REAL *x, LAPACK_INTEGER *incx, LAPACK_REAL *y, 
<a name="l03036"></a>03036                              LAPACK_INTEGER *incy, LAPACK_REAL *ssmin);
<a name="l03037"></a>03037  
<a name="l03038"></a>03038 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slapmt_(LAPACK_LOGICAL *forwrd, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *x,
<a name="l03039"></a>03039                              LAPACK_INTEGER *ldx, LAPACK_INTEGER *k);
<a name="l03040"></a>03040  
<a name="l03041"></a>03041 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqgb_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l03042"></a>03042                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *
<a name="l03043"></a>03043                              colcnd, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l03044"></a>03044  
<a name="l03045"></a>03045 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqge_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03046"></a>03046                              LAPACK_REAL *r__, LAPACK_REAL *c__, LAPACK_REAL *rowcnd, LAPACK_REAL *colcnd, LAPACK_REAL *amax, <span class="keywordtype">char</span> *
<a name="l03047"></a>03047                              equed);
<a name="l03048"></a>03048  
<a name="l03049"></a>03049 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqp2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_REAL *a,
<a name="l03050"></a>03050                              LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_REAL *tau, LAPACK_REAL *vn1, LAPACK_REAL *vn2, LAPACK_REAL *
<a name="l03051"></a>03051                              work);
<a name="l03052"></a>03052  
<a name="l03053"></a>03053 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqps_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_INTEGER 
<a name="l03054"></a>03054                              *nb, LAPACK_INTEGER *kb, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_REAL *tau, 
<a name="l03055"></a>03055                              LAPACK_REAL *vn1, LAPACK_REAL *vn2, LAPACK_REAL *auxv, LAPACK_REAL *f, LAPACK_INTEGER *ldf);
<a name="l03056"></a>03056  
<a name="l03057"></a>03057 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqsb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03058"></a>03058                              LAPACK_INTEGER *ldab, LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l03059"></a>03059  
<a name="l03060"></a>03060 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqsp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *s, LAPACK_REAL *
<a name="l03061"></a>03061                              scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l03062"></a>03062  
<a name="l03063"></a>03063 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqsy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03064"></a>03064                              LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l03065"></a>03065  
<a name="l03066"></a>03066 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaqtr_(LAPACK_LOGICAL *ltran, LAPACK_LOGICAL *lLAPACK_REAL, LAPACK_INTEGER *n, LAPACK_REAL 
<a name="l03067"></a>03067                              *t, LAPACK_INTEGER *ldt, LAPACK_REAL *b, LAPACK_REAL *w, LAPACK_REAL *scale, LAPACK_REAL *x, LAPACK_REAL *work, 
<a name="l03068"></a>03068                              LAPACK_INTEGER *info);
<a name="l03069"></a>03069  
<a name="l03070"></a>03070 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slar1v_(LAPACK_INTEGER *n, LAPACK_INTEGER *b1, LAPACK_INTEGER *bn, LAPACK_REAL *
<a name="l03071"></a>03071                              sigma, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_REAL *ld, LAPACK_REAL *lld, LAPACK_REAL *gersch, LAPACK_REAL *
<a name="l03072"></a>03072                              z__, LAPACK_REAL *ztz, LAPACK_REAL *mingma, LAPACK_INTEGER *r__, LAPACK_INTEGER *isuppz, LAPACK_REAL *
<a name="l03073"></a>03073                              work);
<a name="l03074"></a>03074  
<a name="l03075"></a>03075 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slar2v_(LAPACK_INTEGER *n, LAPACK_REAL *x, LAPACK_REAL *y, LAPACK_REAL *z__, LAPACK_INTEGER 
<a name="l03076"></a>03076                              *incx, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_INTEGER *incc);
<a name="l03077"></a>03077  
<a name="l03078"></a>03078 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarf_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *v, 
<a name="l03079"></a>03079                             LAPACK_INTEGER *incv, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work);
<a name="l03080"></a>03080  
<a name="l03081"></a>03081 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarfb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l03082"></a>03082                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *v, LAPACK_INTEGER *ldv, 
<a name="l03083"></a>03083                              LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03084"></a>03084                              ldwork);
<a name="l03085"></a>03085  
<a name="l03086"></a>03086 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarfg_(LAPACK_INTEGER *n, LAPACK_REAL *alpha, LAPACK_REAL *x, LAPACK_INTEGER *incx, 
<a name="l03087"></a>03087                              LAPACK_REAL *tau);
<a name="l03088"></a>03088  
<a name="l03089"></a>03089 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarft_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03090"></a>03090                              k, LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_REAL *tau, LAPACK_REAL *t, LAPACK_INTEGER *ldt);
<a name="l03091"></a>03091  
<a name="l03092"></a>03092 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarfx_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *v, 
<a name="l03093"></a>03093                              LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work);
<a name="l03094"></a>03094  
<a name="l03095"></a>03095 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slargv_(LAPACK_INTEGER *n, LAPACK_REAL *x, LAPACK_INTEGER *incx, LAPACK_REAL *y, 
<a name="l03096"></a>03096                              LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_INTEGER *incc);
<a name="l03097"></a>03097  
<a name="l03098"></a>03098 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarnv_(LAPACK_INTEGER *idist, LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, LAPACK_REAL 
<a name="l03099"></a>03099                              *x);
<a name="l03100"></a>03100  
<a name="l03101"></a>03101 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarrb_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_REAL *ld, LAPACK_REAL *
<a name="l03102"></a>03102                              lld, LAPACK_INTEGER *ifirst, LAPACK_INTEGER *ilast, LAPACK_REAL *sigma, LAPACK_REAL *reltol, LAPACK_REAL 
<a name="l03103"></a>03103                              *w, LAPACK_REAL *wgap, LAPACK_REAL *werr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03104"></a>03104  
<a name="l03105"></a>03105 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarre_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tol, 
<a name="l03106"></a>03106                              LAPACK_INTEGER *nsplit, LAPACK_INTEGER *isplit, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *woff, 
<a name="l03107"></a>03107                              LAPACK_REAL *gersch, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03108"></a>03108  
<a name="l03109"></a>03109 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarrf_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_REAL *ld, LAPACK_REAL *
<a name="l03110"></a>03110                              lld, LAPACK_INTEGER *ifirst, LAPACK_INTEGER *ilast, LAPACK_REAL *w, LAPACK_REAL *dplus, LAPACK_REAL *
<a name="l03111"></a>03111                              lplus, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03112"></a>03112  
<a name="l03113"></a>03113 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarrv_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *l, LAPACK_INTEGER *isplit, 
<a name="l03114"></a>03114                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_INTEGER *iblock, LAPACK_REAL *gersch, LAPACK_REAL *tol, LAPACK_REAL *
<a name="l03115"></a>03115                              z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03116"></a>03116                              LAPACK_INTEGER *info);
<a name="l03117"></a>03117  
<a name="l03118"></a>03118 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slartg_(LAPACK_REAL *f, LAPACK_REAL *g, LAPACK_REAL *cs, LAPACK_REAL *sn, LAPACK_REAL *r__);
<a name="l03119"></a>03119  
<a name="l03120"></a>03120 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slartv_(LAPACK_INTEGER *n, LAPACK_REAL *x, LAPACK_INTEGER *incx, LAPACK_REAL *y, 
<a name="l03121"></a>03121                              LAPACK_INTEGER *incy, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_INTEGER *incc);
<a name="l03122"></a>03122  
<a name="l03123"></a>03123 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaruv_(LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, LAPACK_REAL *x);
<a name="l03124"></a>03124  
<a name="l03125"></a>03125 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarz_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, 
<a name="l03126"></a>03126                             LAPACK_REAL *v, LAPACK_INTEGER *incv, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *
<a name="l03127"></a>03127                             work);
<a name="l03128"></a>03128  
<a name="l03129"></a>03129 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarzb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l03130"></a>03130                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *v, 
<a name="l03131"></a>03131                              LAPACK_INTEGER *ldv, LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *
<a name="l03132"></a>03132                              work, LAPACK_INTEGER *ldwork);
<a name="l03133"></a>03133  
<a name="l03134"></a>03134 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slarzt_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03135"></a>03135                              k, LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_REAL *tau, LAPACK_REAL *t, LAPACK_INTEGER *ldt);
<a name="l03136"></a>03136  
<a name="l03137"></a>03137 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slas2_(LAPACK_REAL *f, LAPACK_REAL *g, LAPACK_REAL *h__, LAPACK_REAL *ssmin, LAPACK_REAL *
<a name="l03138"></a>03138                             ssmax);
<a name="l03139"></a>03139  
<a name="l03140"></a>03140 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slascl_(<span class="keywordtype">char</span> *type__, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_REAL *
<a name="l03141"></a>03141                              cfrom, LAPACK_REAL *cto, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03142"></a>03142                              LAPACK_INTEGER *info);
<a name="l03143"></a>03143  
<a name="l03144"></a>03144 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd0_(LAPACK_INTEGER *n, LAPACK_INTEGER *sqre, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03145"></a>03145                              LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt, LAPACK_INTEGER *smlsiz, 
<a name="l03146"></a>03146                              LAPACK_INTEGER *iwork, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03147"></a>03147  
<a name="l03148"></a>03148 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd1_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, LAPACK_REAL *
<a name="l03149"></a>03149                              d__, LAPACK_REAL *alpha, LAPACK_REAL *beta, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, 
<a name="l03150"></a>03150                              LAPACK_INTEGER *ldvt, LAPACK_INTEGER *idxq, LAPACK_INTEGER *iwork, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03151"></a>03151                              info);
<a name="l03152"></a>03152  
<a name="l03153"></a>03153 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd2_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, LAPACK_INTEGER 
<a name="l03154"></a>03154                              *k, LAPACK_REAL *d__, LAPACK_REAL *z__, LAPACK_REAL *alpha, LAPACK_REAL *beta, LAPACK_REAL *u, LAPACK_INTEGER *
<a name="l03155"></a>03155                              ldu, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt, LAPACK_REAL *dsigma, LAPACK_REAL *u2, LAPACK_INTEGER *ldu2, 
<a name="l03156"></a>03156                              LAPACK_REAL *vt2, LAPACK_INTEGER *ldvt2, LAPACK_INTEGER *idxp, LAPACK_INTEGER *idx, LAPACK_INTEGER *idxc,
<a name="l03157"></a>03157                              LAPACK_INTEGER *idxq, LAPACK_INTEGER *coltyp, LAPACK_INTEGER *info);
<a name="l03158"></a>03158  
<a name="l03159"></a>03159 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd3_(LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, LAPACK_INTEGER *sqre, LAPACK_INTEGER 
<a name="l03160"></a>03160                              *k, LAPACK_REAL *d__, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *dsigma, LAPACK_REAL *u, LAPACK_INTEGER *
<a name="l03161"></a>03161                              ldu, LAPACK_REAL *u2, LAPACK_INTEGER *ldu2, LAPACK_REAL *vt, LAPACK_INTEGER *ldvt, LAPACK_REAL *vt2, 
<a name="l03162"></a>03162                              LAPACK_INTEGER *ldvt2, LAPACK_INTEGER *idxc, LAPACK_INTEGER *ctot, LAPACK_REAL *z__, LAPACK_INTEGER *
<a name="l03163"></a>03163                              info);
<a name="l03164"></a>03164  
<a name="l03165"></a>03165 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd4_(LAPACK_INTEGER *n, LAPACK_INTEGER *i__, LAPACK_REAL *d__, LAPACK_REAL *z__, 
<a name="l03166"></a>03166                              LAPACK_REAL *delta, LAPACK_REAL *rho, LAPACK_REAL *sigma, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03167"></a>03167  
<a name="l03168"></a>03168 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd5_(LAPACK_INTEGER *i__, LAPACK_REAL *d__, LAPACK_REAL *z__, LAPACK_REAL *delta, 
<a name="l03169"></a>03169                              LAPACK_REAL *rho, LAPACK_REAL *dsigma, LAPACK_REAL *work);
<a name="l03170"></a>03170  
<a name="l03171"></a>03171 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd6_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l03172"></a>03172                              LAPACK_INTEGER *sqre, LAPACK_REAL *d__, LAPACK_REAL *vf, LAPACK_REAL *vl, LAPACK_REAL *alpha, LAPACK_REAL *beta,
<a name="l03173"></a>03173                              LAPACK_INTEGER *idxq, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l03174"></a>03174                              LAPACK_INTEGER *ldgcol, LAPACK_REAL *givnum, LAPACK_INTEGER *ldgnum, LAPACK_REAL *poles, LAPACK_REAL *
<a name="l03175"></a>03175                              difl, LAPACK_REAL *difr, LAPACK_REAL *z__, LAPACK_INTEGER *k, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *
<a name="l03176"></a>03176                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03177"></a>03177  
<a name="l03178"></a>03178 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd7_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l03179"></a>03179                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *k, LAPACK_REAL *d__, LAPACK_REAL *z__, LAPACK_REAL *zw, LAPACK_REAL *vf, 
<a name="l03180"></a>03180                              LAPACK_REAL *vfw, LAPACK_REAL *vl, LAPACK_REAL *vlw, LAPACK_REAL *alpha, LAPACK_REAL *beta, LAPACK_REAL *dsigma,
<a name="l03181"></a>03181                              LAPACK_INTEGER *idx, LAPACK_INTEGER *idxp, LAPACK_INTEGER *idxq, LAPACK_INTEGER *perm, LAPACK_INTEGER *
<a name="l03182"></a>03182                              givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_REAL *givnum, LAPACK_INTEGER *
<a name="l03183"></a>03183                              ldgnum, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_INTEGER *info);
<a name="l03184"></a>03184  
<a name="l03185"></a>03185 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd8_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *k, LAPACK_REAL *d__, LAPACK_REAL *
<a name="l03186"></a>03186                              z__, LAPACK_REAL *vf, LAPACK_REAL *vl, LAPACK_REAL *difl, LAPACK_REAL *difr, LAPACK_INTEGER *lddifr, 
<a name="l03187"></a>03187                              LAPACK_REAL *dsigma, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03188"></a>03188  
<a name="l03189"></a>03189 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasd9_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *ldu, LAPACK_INTEGER *k, LAPACK_REAL *
<a name="l03190"></a>03190                              d__, LAPACK_REAL *z__, LAPACK_REAL *vf, LAPACK_REAL *vl, LAPACK_REAL *difl, LAPACK_REAL *difr, LAPACK_REAL *
<a name="l03191"></a>03191                              dsigma, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03192"></a>03192  
<a name="l03193"></a>03193 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasda_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l03194"></a>03194                              LAPACK_INTEGER *sqre, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *vt, 
<a name="l03195"></a>03195                              LAPACK_INTEGER *k, LAPACK_REAL *difl, LAPACK_REAL *difr, LAPACK_REAL *z__, LAPACK_REAL *poles, LAPACK_INTEGER *
<a name="l03196"></a>03196                              givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_INTEGER *perm, LAPACK_REAL *givnum,
<a name="l03197"></a>03197                              LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03198"></a>03198  
<a name="l03199"></a>03199 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasdq_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *sqre, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03200"></a>03200                              ncvt, LAPACK_INTEGER *nru, LAPACK_INTEGER *ncc, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *vt, 
<a name="l03201"></a>03201                              LAPACK_INTEGER *ldvt, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *
<a name="l03202"></a>03202                              work, LAPACK_INTEGER *info);
<a name="l03203"></a>03203  
<a name="l03204"></a>03204 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasdt_(LAPACK_INTEGER *n, LAPACK_INTEGER *lvl, LAPACK_INTEGER *nd, LAPACK_INTEGER *
<a name="l03205"></a>03205                              inode, LAPACK_INTEGER *ndiml, LAPACK_INTEGER *ndimr, LAPACK_INTEGER *msub);
<a name="l03206"></a>03206  
<a name="l03207"></a>03207 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaset_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *alpha, 
<a name="l03208"></a>03208                              LAPACK_REAL *beta, LAPACK_REAL *a, LAPACK_INTEGER *lda);
<a name="l03209"></a>03209  
<a name="l03210"></a>03210 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq1_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *work, 
<a name="l03211"></a>03211                              LAPACK_INTEGER *info);
<a name="l03212"></a>03212  
<a name="l03213"></a>03213 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq2_(LAPACK_INTEGER *n, LAPACK_REAL *z__, LAPACK_INTEGER *info);
<a name="l03214"></a>03214  
<a name="l03215"></a>03215 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq3_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_REAL *z__, LAPACK_INTEGER *pp,
<a name="l03216"></a>03216                              LAPACK_REAL *dmin__, LAPACK_REAL *sigma, LAPACK_REAL *desig, LAPACK_REAL *qmax, LAPACK_INTEGER *nfail, 
<a name="l03217"></a>03217                              LAPACK_INTEGER *iter, LAPACK_INTEGER *ndiv, LAPACK_LOGICAL *ieee);
<a name="l03218"></a>03218  
<a name="l03219"></a>03219 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq4_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_REAL *z__, LAPACK_INTEGER *pp,
<a name="l03220"></a>03220                              LAPACK_INTEGER *n0in, LAPACK_REAL *dmin__, LAPACK_REAL *dmin1, LAPACK_REAL *dmin2, LAPACK_REAL *dn, 
<a name="l03221"></a>03221                              LAPACK_REAL *dn1, LAPACK_REAL *dn2, LAPACK_REAL *tau, LAPACK_INTEGER *ttype);
<a name="l03222"></a>03222  
<a name="l03223"></a>03223 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq5_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_REAL *z__, LAPACK_INTEGER *pp,
<a name="l03224"></a>03224                              LAPACK_REAL *tau, LAPACK_REAL *dmin__, LAPACK_REAL *dmin1, LAPACK_REAL *dmin2, LAPACK_REAL *dn, LAPACK_REAL *
<a name="l03225"></a>03225                              dnm1, LAPACK_REAL *dnm2, LAPACK_LOGICAL *ieee);
<a name="l03226"></a>03226  
<a name="l03227"></a>03227 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasq6_(LAPACK_INTEGER *i0, LAPACK_INTEGER *n0, LAPACK_REAL *z__, LAPACK_INTEGER *pp,
<a name="l03228"></a>03228                              LAPACK_REAL *dmin__, LAPACK_REAL *dmin1, LAPACK_REAL *dmin2, LAPACK_REAL *dn, LAPACK_REAL *dnm1, LAPACK_REAL *
<a name="l03229"></a>03229                              dnm2);
<a name="l03230"></a>03230  
<a name="l03231"></a>03231 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *pivot, <span class="keywordtype">char</span> *direct, LAPACK_INTEGER *m,
<a name="l03232"></a>03232                             LAPACK_INTEGER *n, LAPACK_REAL *c__, LAPACK_REAL *s, LAPACK_REAL *a, LAPACK_INTEGER *lda);
<a name="l03233"></a>03233  
<a name="l03234"></a>03234 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasrt_(<span class="keywordtype">char</span> *<span class="keywordtype">id</span>, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_INTEGER *info);
<a name="l03235"></a>03235  
<a name="l03236"></a>03236 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slassq_(LAPACK_INTEGER *n, LAPACK_REAL *x, LAPACK_INTEGER *incx, LAPACK_REAL *scale, 
<a name="l03237"></a>03237                              LAPACK_REAL *sumsq);
<a name="l03238"></a>03238  
<a name="l03239"></a>03239 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasv2_(LAPACK_REAL *f, LAPACK_REAL *g, LAPACK_REAL *h__, LAPACK_REAL *ssmin, LAPACK_REAL *
<a name="l03240"></a>03240                              ssmax, LAPACK_REAL *snr, LAPACK_REAL *csr, LAPACK_REAL *snl, LAPACK_REAL *csl);
<a name="l03241"></a>03241  
<a name="l03242"></a>03242 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slaswp_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *k1, 
<a name="l03243"></a>03243                              LAPACK_INTEGER *k2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *incx);
<a name="l03244"></a>03244  
<a name="l03245"></a>03245 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasy2_(LAPACK_LOGICAL *ltranl, LAPACK_LOGICAL *ltranr, LAPACK_INTEGER *isgn, 
<a name="l03246"></a>03246                              LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, LAPACK_REAL *tl, LAPACK_INTEGER *ldtl, LAPACK_REAL *tr, LAPACK_INTEGER *
<a name="l03247"></a>03247                              ldtr, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *scale, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL 
<a name="l03248"></a>03248                              *xnorm, LAPACK_INTEGER *info);
<a name="l03249"></a>03249  
<a name="l03250"></a>03250 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slasyf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l03251"></a>03251                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_REAL *w, LAPACK_INTEGER *ldw, LAPACK_INTEGER 
<a name="l03252"></a>03252                              *info);
<a name="l03253"></a>03253  
<a name="l03254"></a>03254 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatbs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l03255"></a>03255                              normin, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *x, 
<a name="l03256"></a>03256                              LAPACK_REAL *scale, LAPACK_REAL *cnorm, LAPACK_INTEGER *info);
<a name="l03257"></a>03257  
<a name="l03258"></a>03258 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatdf_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *n, LAPACK_REAL *z__, LAPACK_INTEGER *
<a name="l03259"></a>03259                              ldz, LAPACK_REAL *rhs, LAPACK_REAL *rdsum, LAPACK_REAL *rdscal, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *
<a name="l03260"></a>03260                              jpiv);
<a name="l03261"></a>03261  
<a name="l03262"></a>03262 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatps_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l03263"></a>03263                              normin, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *x, LAPACK_REAL *scale, LAPACK_REAL *cnorm, 
<a name="l03264"></a>03264                              LAPACK_INTEGER *info);
<a name="l03265"></a>03265  
<a name="l03266"></a>03266 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_REAL *a, 
<a name="l03267"></a>03267                              LAPACK_INTEGER *lda, LAPACK_REAL *e, LAPACK_REAL *tau, LAPACK_REAL *w, LAPACK_INTEGER *ldw);
<a name="l03268"></a>03268  
<a name="l03269"></a>03269 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l03270"></a>03270                              normin, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *x, LAPACK_REAL *scale, LAPACK_REAL 
<a name="l03271"></a>03271                              *cnorm, LAPACK_INTEGER *info);
<a name="l03272"></a>03272  
<a name="l03273"></a>03273 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatrz_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, LAPACK_REAL *a, 
<a name="l03274"></a>03274                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work);
<a name="l03275"></a>03275  
<a name="l03276"></a>03276 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slatzm_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *v, 
<a name="l03277"></a>03277                              LAPACK_INTEGER *incv, LAPACK_REAL *tau, LAPACK_REAL *c1, LAPACK_REAL *c2, LAPACK_INTEGER *ldc, LAPACK_REAL *
<a name="l03278"></a>03278                              work);
<a name="l03279"></a>03279  
<a name="l03280"></a>03280 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slauu2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03281"></a>03281                              LAPACK_INTEGER *info);
<a name="l03282"></a>03282  
<a name="l03283"></a>03283 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> slauum_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03284"></a>03284                              LAPACK_INTEGER *info);
<a name="l03285"></a>03285  
<a name="l03286"></a>03286 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sopgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *tau, 
<a name="l03287"></a>03287                              LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03288"></a>03288  
<a name="l03289"></a>03289 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sopmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l03290"></a>03290                              LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work, 
<a name="l03291"></a>03291                              LAPACK_INTEGER *info);
<a name="l03292"></a>03292  
<a name="l03293"></a>03293 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorg2l_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03294"></a>03294                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03295"></a>03295  
<a name="l03296"></a>03296 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorg2r_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03297"></a>03297                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03298"></a>03298  
<a name="l03299"></a>03299 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgbr_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l03300"></a>03300                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l03301"></a>03301                              *info);
<a name="l03302"></a>03302  
<a name="l03303"></a>03303 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorghr_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, 
<a name="l03304"></a>03304                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03305"></a>03305  
<a name="l03306"></a>03306 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgl2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03307"></a>03307                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03308"></a>03308  
<a name="l03309"></a>03309 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorglq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03310"></a>03310                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03311"></a>03311  
<a name="l03312"></a>03312 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgql_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03313"></a>03313                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03314"></a>03314  
<a name="l03315"></a>03315 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgqr_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03316"></a>03316                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03317"></a>03317  
<a name="l03318"></a>03318 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03319"></a>03319                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03320"></a>03320  
<a name="l03321"></a>03321 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgrq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, 
<a name="l03322"></a>03322                              LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03323"></a>03323  
<a name="l03324"></a>03324 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03325"></a>03325                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03326"></a>03326  
<a name="l03327"></a>03327 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorm2l_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03328"></a>03328                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03329"></a>03329                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03330"></a>03330  
<a name="l03331"></a>03331 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorm2r_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03332"></a>03332                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03333"></a>03333                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03334"></a>03334  
<a name="l03335"></a>03335 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormbr_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l03336"></a>03336                              LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, 
<a name="l03337"></a>03337                              LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03338"></a>03338  
<a name="l03339"></a>03339 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormhr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03340"></a>03340                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *
<a name="l03341"></a>03341                              c__, LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03342"></a>03342  
<a name="l03343"></a>03343 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sorml2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03344"></a>03344                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03345"></a>03345                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03346"></a>03346  
<a name="l03347"></a>03347 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormlq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03348"></a>03348                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03349"></a>03349                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03350"></a>03350  
<a name="l03351"></a>03351 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormql_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03352"></a>03352                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03353"></a>03353                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03354"></a>03354  
<a name="l03355"></a>03355 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormqr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03356"></a>03356                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03357"></a>03357                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03358"></a>03358  
<a name="l03359"></a>03359 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormr2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03360"></a>03360                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03361"></a>03361                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03362"></a>03362  
<a name="l03363"></a>03363 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormr3_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03364"></a>03364                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, 
<a name="l03365"></a>03365                              LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03366"></a>03366  
<a name="l03367"></a>03367 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormrq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03368"></a>03368                              LAPACK_INTEGER *k, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03369"></a>03369                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03370"></a>03370  
<a name="l03371"></a>03371 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormrz_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l03372"></a>03372                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, 
<a name="l03373"></a>03373                              LAPACK_INTEGER *ldc, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03374"></a>03374  
<a name="l03375"></a>03375 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sormtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l03376"></a>03376                              LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *tau, LAPACK_REAL *c__, LAPACK_INTEGER *ldc,
<a name="l03377"></a>03377                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03378"></a>03378  
<a name="l03379"></a>03379 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03380"></a>03380                              LAPACK_INTEGER *ldab, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03381"></a>03381                              LAPACK_INTEGER *info);
<a name="l03382"></a>03382  
<a name="l03383"></a>03383 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03384"></a>03384                              LAPACK_INTEGER *ldab, LAPACK_REAL *s, LAPACK_REAL *scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l03385"></a>03385  
<a name="l03386"></a>03386 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l03387"></a>03387                              nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *afb, LAPACK_INTEGER *ldafb, LAPACK_REAL *b, 
<a name="l03388"></a>03388                              LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *
<a name="l03389"></a>03389                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03390"></a>03390  
<a name="l03391"></a>03391 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbstf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03392"></a>03392                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l03393"></a>03393  
<a name="l03394"></a>03394 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l03395"></a>03395                             nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03396"></a>03396  
<a name="l03397"></a>03397 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l03398"></a>03398                              LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *afb, LAPACK_INTEGER *ldafb, 
<a name="l03399"></a>03399                              <span class="keywordtype">char</span> *equed, LAPACK_REAL *s, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, 
<a name="l03400"></a>03400                              LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03401"></a>03401                              LAPACK_INTEGER *info);
<a name="l03402"></a>03402  
<a name="l03403"></a>03403 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbtf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03404"></a>03404                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l03405"></a>03405  
<a name="l03406"></a>03406 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbtrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_REAL *ab, 
<a name="l03407"></a>03407                              LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l03408"></a>03408  
<a name="l03409"></a>03409 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spbtrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l03410"></a>03410                              nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03411"></a>03411  
<a name="l03412"></a>03412 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spocon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03413"></a>03413                              LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03414"></a>03414  
<a name="l03415"></a>03415 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spoequ_(LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *s, LAPACK_REAL 
<a name="l03416"></a>03416                              *scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l03417"></a>03417  
<a name="l03418"></a>03418 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sporfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03419"></a>03419                              LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x,
<a name="l03420"></a>03420                              LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03421"></a>03421                              LAPACK_INTEGER *info);
<a name="l03422"></a>03422  
<a name="l03423"></a>03423 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sposv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03424"></a>03424                             LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03425"></a>03425  
<a name="l03426"></a>03426 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sposvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03427"></a>03427                              nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, <span class="keywordtype">char</span> *equed, 
<a name="l03428"></a>03428                              LAPACK_REAL *s, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, 
<a name="l03429"></a>03429                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03430"></a>03430  
<a name="l03431"></a>03431 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spotf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03432"></a>03432                              LAPACK_INTEGER *info);
<a name="l03433"></a>03433  
<a name="l03434"></a>03434 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spotrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03435"></a>03435                              LAPACK_INTEGER *info);
<a name="l03436"></a>03436  
<a name="l03437"></a>03437 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spotri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03438"></a>03438                              LAPACK_INTEGER *info);
<a name="l03439"></a>03439  
<a name="l03440"></a>03440 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spotrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03441"></a>03441                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03442"></a>03442  
<a name="l03443"></a>03443 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sppcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *anorm, 
<a name="l03444"></a>03444                              LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03445"></a>03445  
<a name="l03446"></a>03446 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sppequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *s, LAPACK_REAL *
<a name="l03447"></a>03447                              scond, LAPACK_REAL *amax, LAPACK_INTEGER *info);
<a name="l03448"></a>03448  
<a name="l03449"></a>03449 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03450"></a>03450                              LAPACK_REAL *afp, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, 
<a name="l03451"></a>03451                              LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03452"></a>03452  
<a name="l03453"></a>03453 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sppsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03454"></a>03454                             LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03455"></a>03455  
<a name="l03456"></a>03456 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sppsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03457"></a>03457                              nrhs, LAPACK_REAL *ap, LAPACK_REAL *afp, <span class="keywordtype">char</span> *equed, LAPACK_REAL *s, LAPACK_REAL *b, LAPACK_INTEGER *
<a name="l03458"></a>03458                              ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL 
<a name="l03459"></a>03459                              *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03460"></a>03460  
<a name="l03461"></a>03461 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_INTEGER *info);
<a name="l03462"></a>03462  
<a name="l03463"></a>03463 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_INTEGER *info);
<a name="l03464"></a>03464  
<a name="l03465"></a>03465 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03466"></a>03466                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03467"></a>03467  
<a name="l03468"></a>03468 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sptcon_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *anorm, 
<a name="l03469"></a>03469                              LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03470"></a>03470  
<a name="l03471"></a>03471 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03472"></a>03472                              LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03473"></a>03473  
<a name="l03474"></a>03474 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sptrfs_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03475"></a>03475                              LAPACK_REAL *df, LAPACK_REAL *ef, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, 
<a name="l03476"></a>03476                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03477"></a>03477  
<a name="l03478"></a>03478 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sptsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03479"></a>03479                             LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03480"></a>03480  
<a name="l03481"></a>03481 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sptsvx_(<span class="keywordtype">char</span> *fact, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__,
<a name="l03482"></a>03482                              LAPACK_REAL *e, LAPACK_REAL *df, LAPACK_REAL *ef, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER 
<a name="l03483"></a>03483                              *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03484"></a>03484  
<a name="l03485"></a>03485 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spttrf_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_INTEGER *info);
<a name="l03486"></a>03486  
<a name="l03487"></a>03487 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> spttrs_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03488"></a>03488                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03489"></a>03489  
<a name="l03490"></a>03490 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sptts2_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03491"></a>03491                              LAPACK_REAL *b, LAPACK_INTEGER *ldb);
<a name="l03492"></a>03492  
<a name="l03493"></a>03493 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> srscl_(LAPACK_INTEGER *n, LAPACK_REAL *sa, LAPACK_REAL *sx, LAPACK_INTEGER *incx);
<a name="l03494"></a>03494  
<a name="l03495"></a>03495 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l03496"></a>03496                             LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work,
<a name="l03497"></a>03497                             LAPACK_INTEGER *info);
<a name="l03498"></a>03498  
<a name="l03499"></a>03499 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l03500"></a>03500                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work,
<a name="l03501"></a>03501                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03502"></a>03502  
<a name="l03503"></a>03503 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l03504"></a>03504                              LAPACK_INTEGER *kd, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *vl,
<a name="l03505"></a>03505                              LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *
<a name="l03506"></a>03506                              w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l03507"></a>03507                              ifail, LAPACK_INTEGER *info);
<a name="l03508"></a>03508  
<a name="l03509"></a>03509 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbgst_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l03510"></a>03510                              LAPACK_INTEGER *kb, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *bb, LAPACK_INTEGER *ldbb, LAPACK_REAL *
<a name="l03511"></a>03511                              x, LAPACK_INTEGER *ldx, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03512"></a>03512  
<a name="l03513"></a>03513 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbgv_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l03514"></a>03514                             LAPACK_INTEGER *kb, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *bb, LAPACK_INTEGER *ldbb, LAPACK_REAL *
<a name="l03515"></a>03515                             w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03516"></a>03516  
<a name="l03517"></a>03517 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbgvd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l03518"></a>03518                              LAPACK_INTEGER *kb, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *bb, LAPACK_INTEGER *ldbb, LAPACK_REAL *
<a name="l03519"></a>03519                              w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l03520"></a>03520                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03521"></a>03521  
<a name="l03522"></a>03522 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbgvx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l03523"></a>03523                              LAPACK_INTEGER *ka, LAPACK_INTEGER *kb, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *bb, LAPACK_INTEGER *
<a name="l03524"></a>03524                              ldbb, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER 
<a name="l03525"></a>03525                              *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL 
<a name="l03526"></a>03526                              *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03527"></a>03527  
<a name="l03528"></a>03528 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssbtrd_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l03529"></a>03529                              LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *q, LAPACK_INTEGER *ldq, 
<a name="l03530"></a>03530                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03531"></a>03531  
<a name="l03532"></a>03532 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_INTEGER *ipiv, 
<a name="l03533"></a>03533                              LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03534"></a>03534  
<a name="l03535"></a>03535 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, 
<a name="l03536"></a>03536                             LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03537"></a>03537  
<a name="l03538"></a>03538 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, 
<a name="l03539"></a>03539                              LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l03540"></a>03540                              *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03541"></a>03541  
<a name="l03542"></a>03542 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l03543"></a>03543                              LAPACK_REAL *ap, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, 
<a name="l03544"></a>03544                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03545"></a>03545                              iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03546"></a>03546  
<a name="l03547"></a>03547 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspgst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap,
<a name="l03548"></a>03548                              LAPACK_REAL *bp, LAPACK_INTEGER *info);
<a name="l03549"></a>03549  
<a name="l03550"></a>03550 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspgv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l03551"></a>03551                             n, LAPACK_REAL *ap, LAPACK_REAL *bp, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, 
<a name="l03552"></a>03552                             LAPACK_INTEGER *info);
<a name="l03553"></a>03553  
<a name="l03554"></a>03554 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspgvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l03555"></a>03555                              n, LAPACK_REAL *ap, LAPACK_REAL *bp, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, 
<a name="l03556"></a>03556                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03557"></a>03557  
<a name="l03558"></a>03558 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspgvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l03559"></a>03559                              uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *bp, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il,
<a name="l03560"></a>03560                              LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *
<a name="l03561"></a>03561                              ldz, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03562"></a>03562  
<a name="l03563"></a>03563 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03564"></a>03564                              LAPACK_REAL *afp, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *
<a name="l03565"></a>03565                              ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l03566"></a>03566                              info);
<a name="l03567"></a>03567  
<a name="l03568"></a>03568 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03569"></a>03569                             LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03570"></a>03570  
<a name="l03571"></a>03571 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sspsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03572"></a>03572                              nrhs, LAPACK_REAL *ap, LAPACK_REAL *afp, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL 
<a name="l03573"></a>03573                              *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, 
<a name="l03574"></a>03574                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03575"></a>03575  
<a name="l03576"></a>03576 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssptrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_REAL *d__, 
<a name="l03577"></a>03577                              LAPACK_REAL *e, LAPACK_REAL *tau, LAPACK_INTEGER *info);
<a name="l03578"></a>03578  
<a name="l03579"></a>03579 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_INTEGER *ipiv, 
<a name="l03580"></a>03580                              LAPACK_INTEGER *info);
<a name="l03581"></a>03581  
<a name="l03582"></a>03582 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *ap, LAPACK_INTEGER *ipiv, 
<a name="l03583"></a>03583                              LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03584"></a>03584  
<a name="l03585"></a>03585 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, 
<a name="l03586"></a>03586                              LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03587"></a>03587  
<a name="l03588"></a>03588 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstebz_(<span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *order, LAPACK_INTEGER *n, LAPACK_REAL *vl, 
<a name="l03589"></a>03589                              LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03590"></a>03590                              LAPACK_INTEGER *m, LAPACK_INTEGER *nsplit, LAPACK_REAL *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *
<a name="l03591"></a>03591                              isplit, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03592"></a>03592  
<a name="l03593"></a>03593 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstedc_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03594"></a>03594                              LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l03595"></a>03595                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03596"></a>03596  
<a name="l03597"></a>03597 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstegr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_REAL *d__, 
<a name="l03598"></a>03598                              LAPACK_REAL *e, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, 
<a name="l03599"></a>03599                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_REAL *
<a name="l03600"></a>03600                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03601"></a>03601  
<a name="l03602"></a>03602 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstein_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_INTEGER *m, LAPACK_REAL 
<a name="l03603"></a>03603                              *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *isplit, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *
<a name="l03604"></a>03604                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03605"></a>03605  
<a name="l03606"></a>03606 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, 
<a name="l03607"></a>03607                              LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03608"></a>03608  
<a name="l03609"></a>03609 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssterf_(LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_INTEGER *info);
<a name="l03610"></a>03610  
<a name="l03611"></a>03611 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstev_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *
<a name="l03612"></a>03612                             z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03613"></a>03613  
<a name="l03614"></a>03614 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstevd_(<span class="keywordtype">char</span> *jobz, LAPACK_INTEGER *n, LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL 
<a name="l03615"></a>03615                              *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l03616"></a>03616                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03617"></a>03617  
<a name="l03618"></a>03618 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_REAL *d__, 
<a name="l03619"></a>03619                              LAPACK_REAL *e, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, 
<a name="l03620"></a>03620                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_REAL *
<a name="l03621"></a>03621                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03622"></a>03622  
<a name="l03623"></a>03623 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> sstevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, LAPACK_INTEGER *n, LAPACK_REAL *d__, 
<a name="l03624"></a>03624                              LAPACK_REAL *e, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, 
<a name="l03625"></a>03625                              LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03626"></a>03626                              iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03627"></a>03627  
<a name="l03628"></a>03628 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssycon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03629"></a>03629                              LAPACK_INTEGER *ipiv, LAPACK_REAL *anorm, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03630"></a>03630                              LAPACK_INTEGER *info);
<a name="l03631"></a>03631  
<a name="l03632"></a>03632 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssyev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03633"></a>03633                             LAPACK_INTEGER *lda, LAPACK_REAL *w, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03634"></a>03634  
<a name="l03635"></a>03635 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssyevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03636"></a>03636                              LAPACK_INTEGER *lda, LAPACK_REAL *w, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, 
<a name="l03637"></a>03637                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03638"></a>03638  
<a name="l03639"></a>03639 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssyevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l03640"></a>03640                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, 
<a name="l03641"></a>03641                              LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *
<a name="l03642"></a>03642                              isuppz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, 
<a name="l03643"></a>03643                              LAPACK_INTEGER *info);
<a name="l03644"></a>03644  
<a name="l03645"></a>03645 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssyevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l03646"></a>03646                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, 
<a name="l03647"></a>03647                              LAPACK_REAL *abstol, LAPACK_INTEGER *m, LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *
<a name="l03648"></a>03648                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03649"></a>03649  
<a name="l03650"></a>03650 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssygs2_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03651"></a>03651                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03652"></a>03652  
<a name="l03653"></a>03653 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssygst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03654"></a>03654                              LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03655"></a>03655  
<a name="l03656"></a>03656 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssygv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l03657"></a>03657                             n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *w, LAPACK_REAL *work, 
<a name="l03658"></a>03658                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03659"></a>03659  
<a name="l03660"></a>03660 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssygvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l03661"></a>03661                              n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *w, LAPACK_REAL *work, 
<a name="l03662"></a>03662                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03663"></a>03663  
<a name="l03664"></a>03664 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssygvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l03665"></a>03665                              uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *
<a name="l03666"></a>03666                              vl, LAPACK_REAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_REAL *abstol, LAPACK_INTEGER *m, 
<a name="l03667"></a>03667                              LAPACK_REAL *w, LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER 
<a name="l03668"></a>03668                              *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l03669"></a>03669  
<a name="l03670"></a>03670 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssyrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03671"></a>03671                              LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, 
<a name="l03672"></a>03672                              LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *
<a name="l03673"></a>03673                              work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03674"></a>03674  
<a name="l03675"></a>03675 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssysv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03676"></a>03676                             LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *work, 
<a name="l03677"></a>03677                             LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03678"></a>03678  
<a name="l03679"></a>03679 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssysvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03680"></a>03680                              nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *af, LAPACK_INTEGER *ldaf, LAPACK_INTEGER *ipiv, 
<a name="l03681"></a>03681                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *rcond, LAPACK_REAL *ferr,
<a name="l03682"></a>03682                              LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l03683"></a>03683                              info);
<a name="l03684"></a>03684  
<a name="l03685"></a>03685 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytd2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03686"></a>03686                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tau, LAPACK_INTEGER *info);
<a name="l03687"></a>03687  
<a name="l03688"></a>03688 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03689"></a>03689                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l03690"></a>03690  
<a name="l03691"></a>03691 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03692"></a>03692                              LAPACK_REAL *d__, LAPACK_REAL *e, LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l03693"></a>03693                              info);
<a name="l03694"></a>03694  
<a name="l03695"></a>03695 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03696"></a>03696                              LAPACK_INTEGER *ipiv, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03697"></a>03697  
<a name="l03698"></a>03698 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03699"></a>03699                              LAPACK_INTEGER *ipiv, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03700"></a>03700  
<a name="l03701"></a>03701 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ssytrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_REAL *a, 
<a name="l03702"></a>03702                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03703"></a>03703  
<a name="l03704"></a>03704 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stbcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03705"></a>03705                              LAPACK_INTEGER *kd, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *rcond, LAPACK_REAL *work, 
<a name="l03706"></a>03706                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03707"></a>03707  
<a name="l03708"></a>03708 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stbrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03709"></a>03709                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *b, LAPACK_INTEGER 
<a name="l03710"></a>03710                              *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, 
<a name="l03711"></a>03711                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03712"></a>03712  
<a name="l03713"></a>03713 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stbtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03714"></a>03714                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_REAL *ab, LAPACK_INTEGER *ldab, LAPACK_REAL *b, LAPACK_INTEGER 
<a name="l03715"></a>03715                              *ldb, LAPACK_INTEGER *info);
<a name="l03716"></a>03716  
<a name="l03717"></a>03717 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l03718"></a>03718                              LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *vl, 
<a name="l03719"></a>03719                              LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_REAL 
<a name="l03720"></a>03720                              *work, LAPACK_INTEGER *info);
<a name="l03721"></a>03721  
<a name="l03722"></a>03722 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgex2_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, LAPACK_REAL 
<a name="l03723"></a>03723                              *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *
<a name="l03724"></a>03724                              z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *j1, LAPACK_INTEGER *n1, LAPACK_INTEGER *n2, LAPACK_REAL *work, 
<a name="l03725"></a>03725                              LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03726"></a>03726  
<a name="l03727"></a>03727 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgexc_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, LAPACK_REAL 
<a name="l03728"></a>03728                              *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *
<a name="l03729"></a>03729                              z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *ifst, LAPACK_INTEGER *ilst, LAPACK_REAL *work, LAPACK_INTEGER *
<a name="l03730"></a>03730                              lwork, LAPACK_INTEGER *info);
<a name="l03731"></a>03731  
<a name="l03732"></a>03732 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgsen_(LAPACK_INTEGER *ijob, LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, 
<a name="l03733"></a>03733                              LAPACK_LOGICAL *select, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *
<a name="l03734"></a>03734                              ldb, LAPACK_REAL *alphar, LAPACK_REAL *alphai, LAPACK_REAL *beta, LAPACK_REAL *q, LAPACK_INTEGER *ldq, 
<a name="l03735"></a>03735                              LAPACK_REAL *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *m, LAPACK_REAL *pl, LAPACK_REAL *pr, LAPACK_REAL *dif, 
<a name="l03736"></a>03736                              LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *
<a name="l03737"></a>03737                              info);
<a name="l03738"></a>03738  
<a name="l03739"></a>03739 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgsja_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l03740"></a>03740                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_REAL *a, LAPACK_INTEGER *lda,
<a name="l03741"></a>03741                              LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *tola, LAPACK_REAL *tolb, LAPACK_REAL *alpha, LAPACK_REAL *
<a name="l03742"></a>03742                              beta, LAPACK_REAL *u, LAPACK_INTEGER *ldu, LAPACK_REAL *v, LAPACK_INTEGER *ldv, LAPACK_REAL *q, LAPACK_INTEGER *
<a name="l03743"></a>03743                              ldq, LAPACK_REAL *work, LAPACK_INTEGER *ncycle, LAPACK_INTEGER *info);
<a name="l03744"></a>03744  
<a name="l03745"></a>03745 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l03746"></a>03746                              LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *vl, 
<a name="l03747"></a>03747                              LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, LAPACK_INTEGER *ldvr, LAPACK_REAL *s, LAPACK_REAL *dif, LAPACK_INTEGER *
<a name="l03748"></a>03748                              mm, LAPACK_INTEGER *m, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l03749"></a>03749                              info);
<a name="l03750"></a>03750  
<a name="l03751"></a>03751 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgsy2_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l03752"></a>03752                              n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *c__, LAPACK_INTEGER *
<a name="l03753"></a>03753                              ldc, LAPACK_REAL *d__, LAPACK_INTEGER *ldd, LAPACK_REAL *e, LAPACK_INTEGER *lde, LAPACK_REAL *f, LAPACK_INTEGER 
<a name="l03754"></a>03754                              *ldf, LAPACK_REAL *scale, LAPACK_REAL *rdsum, LAPACK_REAL *rdscal, LAPACK_INTEGER *iwork, LAPACK_INTEGER 
<a name="l03755"></a>03755                              *pq, LAPACK_INTEGER *info);
<a name="l03756"></a>03756  
<a name="l03757"></a>03757 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stgsyl_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l03758"></a>03758                              n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *c__, LAPACK_INTEGER *
<a name="l03759"></a>03759                              ldc, LAPACK_REAL *d__, LAPACK_INTEGER *ldd, LAPACK_REAL *e, LAPACK_INTEGER *lde, LAPACK_REAL *f, LAPACK_INTEGER 
<a name="l03760"></a>03760                              *ldf, LAPACK_REAL *scale, LAPACK_REAL *dif, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l03761"></a>03761                              iwork, LAPACK_INTEGER *info);
<a name="l03762"></a>03762  
<a name="l03763"></a>03763 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stpcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03764"></a>03764                              LAPACK_REAL *ap, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03765"></a>03765  
<a name="l03766"></a>03766 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stprfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03767"></a>03767                              LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, LAPACK_INTEGER *ldx,
<a name="l03768"></a>03768                              LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03769"></a>03769  
<a name="l03770"></a>03770 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stptri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_REAL *ap, 
<a name="l03771"></a>03771                              LAPACK_INTEGER *info);
<a name="l03772"></a>03772  
<a name="l03773"></a>03773 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stptrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03774"></a>03774                              LAPACK_INTEGER *nrhs, LAPACK_REAL *ap, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03775"></a>03775  
<a name="l03776"></a>03776 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03777"></a>03777                              LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *rcond, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03778"></a>03778                              LAPACK_INTEGER *info);
<a name="l03779"></a>03779  
<a name="l03780"></a>03780 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l03781"></a>03781                              LAPACK_INTEGER *n, LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, 
<a name="l03782"></a>03782                              LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_REAL *work, LAPACK_INTEGER *info);
<a name="l03783"></a>03783  
<a name="l03784"></a>03784 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strexc_(<span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_REAL *t, LAPACK_INTEGER *ldt, 
<a name="l03785"></a>03785                              LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *ifst, LAPACK_INTEGER *ilst, LAPACK_REAL *work, 
<a name="l03786"></a>03786                              LAPACK_INTEGER *info);
<a name="l03787"></a>03787  
<a name="l03788"></a>03788 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03789"></a>03789                              LAPACK_INTEGER *nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *x, 
<a name="l03790"></a>03790                              LAPACK_INTEGER *ldx, LAPACK_REAL *ferr, LAPACK_REAL *berr, LAPACK_REAL *work, LAPACK_INTEGER *iwork, 
<a name="l03791"></a>03791                              LAPACK_INTEGER *info);
<a name="l03792"></a>03792  
<a name="l03793"></a>03793 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strsen_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, LAPACK_LOGICAL *select, LAPACK_INTEGER 
<a name="l03794"></a>03794                              *n, LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *q, LAPACK_INTEGER *ldq, LAPACK_REAL *wr, LAPACK_REAL *wi, 
<a name="l03795"></a>03795                              LAPACK_INTEGER *m, LAPACK_REAL *s, LAPACK_REAL *sep, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l03796"></a>03796                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l03797"></a>03797  
<a name="l03798"></a>03798 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l03799"></a>03799                              LAPACK_INTEGER *n, LAPACK_REAL *t, LAPACK_INTEGER *ldt, LAPACK_REAL *vl, LAPACK_INTEGER *ldvl, LAPACK_REAL *vr, 
<a name="l03800"></a>03800                              LAPACK_INTEGER *ldvr, LAPACK_REAL *s, LAPACK_REAL *sep, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_REAL *
<a name="l03801"></a>03801                              work, LAPACK_INTEGER *ldwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l03802"></a>03802  
<a name="l03803"></a>03803 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strsyl_(<span class="keywordtype">char</span> *trana, <span class="keywordtype">char</span> *tranb, LAPACK_INTEGER *isgn, LAPACK_INTEGER 
<a name="l03804"></a>03804                              *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_REAL *
<a name="l03805"></a>03805                              c__, LAPACK_INTEGER *ldc, LAPACK_REAL *scale, LAPACK_INTEGER *info);
<a name="l03806"></a>03806  
<a name="l03807"></a>03807 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strti2_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03808"></a>03808                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l03809"></a>03809  
<a name="l03810"></a>03810 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strtri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, LAPACK_REAL *a, 
<a name="l03811"></a>03811                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l03812"></a>03812  
<a name="l03813"></a>03813 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> strtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l03814"></a>03814                              LAPACK_INTEGER *nrhs, LAPACK_REAL *a, LAPACK_INTEGER *lda, LAPACK_REAL *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l03815"></a>03815                              info);
<a name="l03816"></a>03816  
<a name="l03817"></a>03817 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stzrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03818"></a>03818                              LAPACK_REAL *tau, LAPACK_INTEGER *info);
<a name="l03819"></a>03819  
<a name="l03820"></a>03820 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> stzrzf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_REAL *a, LAPACK_INTEGER *lda, 
<a name="l03821"></a>03821                              LAPACK_REAL *tau, LAPACK_REAL *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03822"></a>03822  
<a name="l03823"></a>03823 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> xerbla_(<span class="keywordtype">char</span> *srname, LAPACK_INTEGER *info);
<a name="l03824"></a>03824  
<a name="l03825"></a>03825 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zbdsqr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ncvt, LAPACK_INTEGER *
<a name="l03826"></a>03826                              nru, LAPACK_INTEGER *ncc, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *vt, 
<a name="l03827"></a>03827                              LAPACK_INTEGER *ldvt, LAPACK_DOUBLECOMPLEX *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLECOMPLEX *c__, 
<a name="l03828"></a>03828                              LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03829"></a>03829  
<a name="l03830"></a>03830 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zdrot_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *cx, LAPACK_INTEGER *incx, 
<a name="l03831"></a>03831                             LAPACK_DOUBLECOMPLEX *cy, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s);
<a name="l03832"></a>03832  
<a name="l03833"></a>03833 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zdrscl_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *sa, LAPACK_DOUBLECOMPLEX *sx, 
<a name="l03834"></a>03834                              LAPACK_INTEGER *incx);
<a name="l03835"></a>03835  
<a name="l03836"></a>03836 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbbrd_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *ncc,
<a name="l03837"></a>03837                              LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l03838"></a>03838                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l03839"></a>03839                              LAPACK_DOUBLECOMPLEX *pt, LAPACK_INTEGER *ldpt, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, 
<a name="l03840"></a>03840                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03841"></a>03841  
<a name="l03842"></a>03842 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l03843"></a>03843                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, 
<a name="l03844"></a>03844                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l03845"></a>03845                              info);
<a name="l03846"></a>03846  
<a name="l03847"></a>03847 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l03848"></a>03848                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l03849"></a>03849                              LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *
<a name="l03850"></a>03850                              info);
<a name="l03851"></a>03851  
<a name="l03852"></a>03852 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l03853"></a>03853                              ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *
<a name="l03854"></a>03854                              afb, LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03855"></a>03855                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l03856"></a>03856                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03857"></a>03857  
<a name="l03858"></a>03858 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, LAPACK_INTEGER *
<a name="l03859"></a>03859                             nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *
<a name="l03860"></a>03860                             b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03861"></a>03861  
<a name="l03862"></a>03862 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl,
<a name="l03863"></a>03863                              LAPACK_INTEGER *ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l03864"></a>03864                              LAPACK_DOUBLECOMPLEX *afb, LAPACK_INTEGER *ldafb, LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, 
<a name="l03865"></a>03865                              LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03866"></a>03866                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, 
<a name="l03867"></a>03867                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l03868"></a>03868                              info);
<a name="l03869"></a>03869  
<a name="l03870"></a>03870 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbtf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l03871"></a>03871                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l03872"></a>03872  
<a name="l03873"></a>03873 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbtrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l03874"></a>03874                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l03875"></a>03875  
<a name="l03876"></a>03876 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgbtrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *
<a name="l03877"></a>03877                              ku, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *ipiv, 
<a name="l03878"></a>03878                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l03879"></a>03879  
<a name="l03880"></a>03880 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgebak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l03881"></a>03881                              LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, LAPACK_INTEGER *m, LAPACK_DOUBLECOMPLEX *v, 
<a name="l03882"></a>03882                              LAPACK_INTEGER *ldv, LAPACK_INTEGER *info);
<a name="l03883"></a>03883  
<a name="l03884"></a>03884 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgebal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER 
<a name="l03885"></a>03885                              *lda, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, LAPACK_INTEGER *info);
<a name="l03886"></a>03886  
<a name="l03887"></a>03887 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgebd2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03888"></a>03888                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *tauq, 
<a name="l03889"></a>03889                              LAPACK_DOUBLECOMPLEX *taup, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l03890"></a>03890  
<a name="l03891"></a>03891 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgebrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03892"></a>03892                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *tauq, 
<a name="l03893"></a>03893                              LAPACK_DOUBLECOMPLEX *taup, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *
<a name="l03894"></a>03894                              info);
<a name="l03895"></a>03895  
<a name="l03896"></a>03896 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgecon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03897"></a>03897                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *
<a name="l03898"></a>03898                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03899"></a>03899  
<a name="l03900"></a>03900 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeequ_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03901"></a>03901                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *rowcnd, 
<a name="l03902"></a>03902                              LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l03903"></a>03903  
<a name="l03904"></a>03904 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgees_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, LAPACK_INTEGER *n, 
<a name="l03905"></a>03905                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, LAPACK_DOUBLECOMPLEX *w, 
<a name="l03906"></a>03906                             LAPACK_DOUBLECOMPLEX *vs, LAPACK_INTEGER *ldvs, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l03907"></a>03907                             LAPACK_DOUBLEREAL *rwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l03908"></a>03908  
<a name="l03909"></a>03909 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeesx_(<span class="keywordtype">char</span> *jobvs, <span class="keywordtype">char</span> *sort, LAPACK_L_FP select, <span class="keywordtype">char</span> *
<a name="l03910"></a>03910                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *sdim, 
<a name="l03911"></a>03911                              LAPACK_DOUBLECOMPLEX *w, LAPACK_DOUBLECOMPLEX *vs, LAPACK_INTEGER *ldvs, LAPACK_DOUBLEREAL *
<a name="l03912"></a>03912                              rconde, LAPACK_DOUBLEREAL *rcondv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l03913"></a>03913                              LAPACK_DOUBLEREAL *rwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l03914"></a>03914  
<a name="l03915"></a>03915 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, 
<a name="l03916"></a>03916                             <span class="keyword">const</span> LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, <span class="keyword">const</span> LAPACK_DOUBLECOMPLEX *w, <span class="keyword">const</span> LAPACK_DOUBLECOMPLEX *vl, 
<a name="l03917"></a>03917                             LAPACK_INTEGER *ldvl, <span class="keyword">const</span> LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, <span class="keyword">const</span> LAPACK_DOUBLECOMPLEX *work, 
<a name="l03918"></a>03918                             LAPACK_INTEGER *lwork, <span class="keyword">const</span> LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03919"></a>03919  
<a name="l03920"></a>03920 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l03921"></a>03921                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *w, 
<a name="l03922"></a>03922                              LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, 
<a name="l03923"></a>03923                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *abnrm, 
<a name="l03924"></a>03924                              LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *rcondv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l03925"></a>03925                              lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03926"></a>03926  
<a name="l03927"></a>03927 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgegs_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, LAPACK_INTEGER *n, 
<a name="l03928"></a>03928                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03929"></a>03929                             LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *vsl, 
<a name="l03930"></a>03930                             LAPACK_INTEGER *ldvsl, LAPACK_DOUBLECOMPLEX *vsr, LAPACK_INTEGER *ldvsr, LAPACK_DOUBLECOMPLEX *
<a name="l03931"></a>03931                             work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03932"></a>03932  
<a name="l03933"></a>03933 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgegv_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, 
<a name="l03934"></a>03934                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03935"></a>03935                             LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER 
<a name="l03936"></a>03936                             *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER 
<a name="l03937"></a>03937                             *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03938"></a>03938  
<a name="l03939"></a>03939 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgehd2_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l03940"></a>03940                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l03941"></a>03941                              work, LAPACK_INTEGER *info);
<a name="l03942"></a>03942  
<a name="l03943"></a>03943 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgehrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l03944"></a>03944                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l03945"></a>03945                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03946"></a>03946  
<a name="l03947"></a>03947 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgelq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03948"></a>03948                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l03949"></a>03949  
<a name="l03950"></a>03950 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgelqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03951"></a>03951                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l03952"></a>03952                              LAPACK_INTEGER *info);
<a name="l03953"></a>03953  
<a name="l03954"></a>03954 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgels_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l03955"></a>03955                             nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03956"></a>03956                             LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l03957"></a>03957  
<a name="l03958"></a>03958 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgelsx_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l03959"></a>03959                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03960"></a>03960                              LAPACK_INTEGER *jpvt, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLECOMPLEX *work, 
<a name="l03961"></a>03961                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03962"></a>03962  
<a name="l03963"></a>03963 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgelsy_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l03964"></a>03964                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l03965"></a>03965                              LAPACK_INTEGER *jpvt, LAPACK_DOUBLEREAL *rcond, LAPACK_INTEGER *rank, LAPACK_DOUBLECOMPLEX *work, 
<a name="l03966"></a>03966                              LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03967"></a>03967  
<a name="l03968"></a>03968 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeql2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03969"></a>03969                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l03970"></a>03970  
<a name="l03971"></a>03971 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeqlf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03972"></a>03972                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l03973"></a>03973                              LAPACK_INTEGER *info);
<a name="l03974"></a>03974  
<a name="l03975"></a>03975 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeqp3_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03976"></a>03976                              LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, 
<a name="l03977"></a>03977                              LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03978"></a>03978  
<a name="l03979"></a>03979 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeqpf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03980"></a>03980                              LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, 
<a name="l03981"></a>03981                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03982"></a>03982  
<a name="l03983"></a>03983 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeqr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03984"></a>03984                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l03985"></a>03985  
<a name="l03986"></a>03986 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgeqrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03987"></a>03987                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l03988"></a>03988                              LAPACK_INTEGER *info);
<a name="l03989"></a>03989  
<a name="l03990"></a>03990 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgerfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l03991"></a>03991                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *ldaf, 
<a name="l03992"></a>03992                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, 
<a name="l03993"></a>03993                              LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work,
<a name="l03994"></a>03994                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l03995"></a>03995  
<a name="l03996"></a>03996 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgerq2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l03997"></a>03997                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l03998"></a>03998  
<a name="l03999"></a>03999 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgerqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04000"></a>04000                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l04001"></a>04001                              LAPACK_INTEGER *info);
<a name="l04002"></a>04002  
<a name="l04003"></a>04003 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgesc2_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04004"></a>04004                              LAPACK_DOUBLECOMPLEX *rhs, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv, LAPACK_DOUBLEREAL *scale);
<a name="l04005"></a>04005  
<a name="l04006"></a>04006 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgesv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04007"></a>04007                             LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *
<a name="l04008"></a>04008                             info);
<a name="l04009"></a>04009  
<a name="l04010"></a>04010 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgesvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobvt, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04011"></a>04011                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLECOMPLEX *u, LAPACK_INTEGER *
<a name="l04012"></a>04012                              ldu, LAPACK_DOUBLECOMPLEX *vt, LAPACK_INTEGER *ldvt, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l04013"></a>04013                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04014"></a>04014  
<a name="l04015"></a>04015 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04016"></a>04016                              nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *
<a name="l04017"></a>04017                              ldaf, LAPACK_INTEGER *ipiv, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l04018"></a>04018                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04019"></a>04019                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *
<a name="l04020"></a>04020                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04021"></a>04021  
<a name="l04022"></a>04022 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgetc2_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04023"></a>04023                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv, LAPACK_INTEGER *info);
<a name="l04024"></a>04024  
<a name="l04025"></a>04025 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgetf2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04026"></a>04026                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04027"></a>04027  
<a name="l04028"></a>04028 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgetrf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04029"></a>04029                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04030"></a>04030  
<a name="l04031"></a>04031 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgetri_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04032"></a>04032                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04033"></a>04033  
<a name="l04034"></a>04034 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgetrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04035"></a>04035                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04036"></a>04036                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04037"></a>04037  
<a name="l04038"></a>04038 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggbak_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *side, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, 
<a name="l04039"></a>04039                              LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, LAPACK_INTEGER *m, 
<a name="l04040"></a>04040                              LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_INTEGER *info);
<a name="l04041"></a>04041  
<a name="l04042"></a>04042 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggbal_(<span class="keywordtype">char</span> *job, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER 
<a name="l04043"></a>04043                              *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l04044"></a>04044                              LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *
<a name="l04045"></a>04045                              info);
<a name="l04046"></a>04046  
<a name="l04047"></a>04047 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgges_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l04048"></a>04048                             delctg, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04049"></a>04049                             LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *
<a name="l04050"></a>04050                             beta, LAPACK_DOUBLECOMPLEX *vsl, LAPACK_INTEGER *ldvsl, LAPACK_DOUBLECOMPLEX *vsr, LAPACK_INTEGER 
<a name="l04051"></a>04051                             *ldvsr, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, 
<a name="l04052"></a>04052                             LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l04053"></a>04053  
<a name="l04054"></a>04054 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggesx_(<span class="keywordtype">char</span> *jobvsl, <span class="keywordtype">char</span> *jobvsr, <span class="keywordtype">char</span> *sort, LAPACK_L_FP 
<a name="l04055"></a>04055                              delctg, <span class="keywordtype">char</span> *sense, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04056"></a>04056                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *sdim, LAPACK_DOUBLECOMPLEX *alpha, 
<a name="l04057"></a>04057                              LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *vsl, LAPACK_INTEGER *ldvsl, 
<a name="l04058"></a>04058                              LAPACK_DOUBLECOMPLEX *vsr, LAPACK_INTEGER *ldvsr, LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *
<a name="l04059"></a>04059                              rcondv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, 
<a name="l04060"></a>04060                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l04061"></a>04061  
<a name="l04062"></a>04062 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggev_(<span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, LAPACK_INTEGER *n, 
<a name="l04063"></a>04063                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04064"></a>04064                             LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER 
<a name="l04065"></a>04065                             *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER 
<a name="l04066"></a>04066                             *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04067"></a>04067  
<a name="l04068"></a>04068 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggevx_(<span class="keywordtype">char</span> *balanc, <span class="keywordtype">char</span> *jobvl, <span class="keywordtype">char</span> *jobvr, <span class="keywordtype">char</span> *
<a name="l04069"></a>04069                              sense, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04070"></a>04070                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *beta, 
<a name="l04071"></a>04071                              LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, 
<a name="l04072"></a>04072                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLEREAL *lscale, LAPACK_DOUBLEREAL *rscale, 
<a name="l04073"></a>04073                              LAPACK_DOUBLEREAL *abnrm, LAPACK_DOUBLEREAL *bbnrm, LAPACK_DOUBLEREAL *rconde, LAPACK_DOUBLEREAL *
<a name="l04074"></a>04074                              rcondv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, 
<a name="l04075"></a>04075                              LAPACK_INTEGER *iwork, LAPACK_LOGICAL *bwork, LAPACK_INTEGER *info);
<a name="l04076"></a>04076  
<a name="l04077"></a>04077 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggglm_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, 
<a name="l04078"></a>04078                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04079"></a>04079                              LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *x, LAPACK_DOUBLECOMPLEX *y, LAPACK_DOUBLECOMPLEX 
<a name="l04080"></a>04080                              *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04081"></a>04081  
<a name="l04082"></a>04082 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgghrd_(<span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04083"></a>04083                              ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04084"></a>04084                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *z__, 
<a name="l04085"></a>04085                              LAPACK_INTEGER *ldz, LAPACK_INTEGER *info);
<a name="l04086"></a>04086  
<a name="l04087"></a>04087 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgglse_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *p, 
<a name="l04088"></a>04088                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04089"></a>04089                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04090"></a>04090                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04091"></a>04091  
<a name="l04092"></a>04092 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggqrf_(LAPACK_INTEGER *n, LAPACK_INTEGER *m, LAPACK_INTEGER *p, 
<a name="l04093"></a>04093                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *taua, LAPACK_DOUBLECOMPLEX *b,
<a name="l04094"></a>04094                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *taub, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l04095"></a>04095                              lwork, LAPACK_INTEGER *info);
<a name="l04096"></a>04096  
<a name="l04097"></a>04097 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *p, LAPACK_INTEGER *n, 
<a name="l04098"></a>04098                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *taua, LAPACK_DOUBLECOMPLEX *b,
<a name="l04099"></a>04099                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *taub, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l04100"></a>04100                              lwork, LAPACK_INTEGER *info);
<a name="l04101"></a>04101  
<a name="l04102"></a>04102 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggsvd_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l04103"></a>04103                              LAPACK_INTEGER *n, LAPACK_INTEGER *p, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04104"></a>04104                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *alpha, 
<a name="l04105"></a>04105                              LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLECOMPLEX *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLECOMPLEX *v, 
<a name="l04106"></a>04106                              LAPACK_INTEGER *ldv, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04107"></a>04107                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04108"></a>04108  
<a name="l04109"></a>04109 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zggsvp_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l04110"></a>04110                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX 
<a name="l04111"></a>04111                              *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *tola, LAPACK_DOUBLEREAL *tolb, LAPACK_INTEGER *k, 
<a name="l04112"></a>04112                              LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER 
<a name="l04113"></a>04113                              *ldv, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *iwork, LAPACK_DOUBLEREAL *
<a name="l04114"></a>04114                              rwork, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04115"></a>04115  
<a name="l04116"></a>04116 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgtcon_(<span class="keywordtype">char</span> *norm, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *dl, 
<a name="l04117"></a>04117                              LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *
<a name="l04118"></a>04118                              ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04119"></a>04119                              LAPACK_INTEGER *info);
<a name="l04120"></a>04120  
<a name="l04121"></a>04121 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgtrfs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04122"></a>04122                              LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, 
<a name="l04123"></a>04123                              LAPACK_DOUBLECOMPLEX *dlf, LAPACK_DOUBLECOMPLEX *df, LAPACK_DOUBLECOMPLEX *duf, 
<a name="l04124"></a>04124                              LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04125"></a>04125                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l04126"></a>04126                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04127"></a>04127  
<a name="l04128"></a>04128 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgtsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *dl, 
<a name="l04129"></a>04129                             LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb,
<a name="l04130"></a>04130                             LAPACK_INTEGER *info);
<a name="l04131"></a>04131  
<a name="l04132"></a>04132 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgtsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04133"></a>04133                              nrhs, LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, 
<a name="l04134"></a>04134                              LAPACK_DOUBLECOMPLEX *dlf, LAPACK_DOUBLECOMPLEX *df, LAPACK_DOUBLECOMPLEX *duf, 
<a name="l04135"></a>04135                              LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04136"></a>04136                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, 
<a name="l04137"></a>04137                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04138"></a>04138                              info);
<a name="l04139"></a>04139  
<a name="l04140"></a>04140 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgttrf_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *
<a name="l04141"></a>04141                              d__, LAPACK_DOUBLECOMPLEX *du, LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *
<a name="l04142"></a>04142                              info);
<a name="l04143"></a>04143  
<a name="l04144"></a>04144 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgttrs_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04145"></a>04145                              LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, 
<a name="l04146"></a>04146                              LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04147"></a>04147                              LAPACK_INTEGER *info);
<a name="l04148"></a>04148  
<a name="l04149"></a>04149 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zgtts2_(LAPACK_INTEGER *itrans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04150"></a>04150                              LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *d__, LAPACK_DOUBLECOMPLEX *du, 
<a name="l04151"></a>04151                              LAPACK_DOUBLECOMPLEX *du2, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l04152"></a>04152  
<a name="l04153"></a>04153 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04154"></a>04154                             LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, 
<a name="l04155"></a>04155                             LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04156"></a>04156  
<a name="l04157"></a>04157 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04158"></a>04158                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, 
<a name="l04159"></a>04159                              LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, 
<a name="l04160"></a>04160                              LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04161"></a>04161  
<a name="l04162"></a>04162 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04163"></a>04163                              LAPACK_INTEGER *kd, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *q, 
<a name="l04164"></a>04164                              LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *
<a name="l04165"></a>04165                              iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__,
<a name="l04166"></a>04166                              LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork,
<a name="l04167"></a>04167                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l04168"></a>04168  
<a name="l04169"></a>04169 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbgst_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l04170"></a>04170                              LAPACK_INTEGER *kb, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *bb, 
<a name="l04171"></a>04171                              LAPACK_INTEGER *ldbb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04172"></a>04172                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04173"></a>04173  
<a name="l04174"></a>04174 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbgv_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *ka, 
<a name="l04175"></a>04175                             LAPACK_INTEGER *kb, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *bb, 
<a name="l04176"></a>04176                             LAPACK_INTEGER *ldbb, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04177"></a>04177                             LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04178"></a>04178  
<a name="l04179"></a>04179 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbgvx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04180"></a>04180                              LAPACK_INTEGER *ka, LAPACK_INTEGER *kb, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l04181"></a>04181                              LAPACK_DOUBLECOMPLEX *bb, LAPACK_INTEGER *ldbb, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l04182"></a>04182                              LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *
<a name="l04183"></a>04183                              abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04184"></a>04184                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l04185"></a>04185                              ifail, LAPACK_INTEGER *info);
<a name="l04186"></a>04186  
<a name="l04187"></a>04187 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhbtrd_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04188"></a>04188                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l04189"></a>04189                              LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04190"></a>04190  
<a name="l04191"></a>04191 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhecon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04192"></a>04192                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, 
<a name="l04193"></a>04193                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04194"></a>04194  
<a name="l04195"></a>04195 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zheev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX 
<a name="l04196"></a>04196                             *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l04197"></a>04197                             LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04198"></a>04198  
<a name="l04199"></a>04199 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zheevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04200"></a>04200                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04201"></a>04201                              LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, 
<a name="l04202"></a>04202                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04203"></a>04203  
<a name="l04204"></a>04204 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zheevr_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04205"></a>04205                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, 
<a name="l04206"></a>04206                              LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *
<a name="l04207"></a>04207                              w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_INTEGER *isuppz, LAPACK_DOUBLECOMPLEX *
<a name="l04208"></a>04208                              work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *
<a name="l04209"></a>04209                              iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04210"></a>04210  
<a name="l04211"></a>04211 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zheevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04212"></a>04212                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, 
<a name="l04213"></a>04213                              LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *
<a name="l04214"></a>04214                              w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l04215"></a>04215                              lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *
<a name="l04216"></a>04216                              info);
<a name="l04217"></a>04217  
<a name="l04218"></a>04218 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhegs2_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04219"></a>04219                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04220"></a>04220                              LAPACK_INTEGER *info);
<a name="l04221"></a>04221  
<a name="l04222"></a>04222 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhegst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04223"></a>04223                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04224"></a>04224                              LAPACK_INTEGER *info);
<a name="l04225"></a>04225  
<a name="l04226"></a>04226 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhegv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l04227"></a>04227                             n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04228"></a>04228                             LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork,
<a name="l04229"></a>04229                             LAPACK_INTEGER *info);
<a name="l04230"></a>04230  
<a name="l04231"></a>04231 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhegvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l04232"></a>04232                              n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04233"></a>04233                              LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork,
<a name="l04234"></a>04234                              LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04235"></a>04235  
<a name="l04236"></a>04236 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhegvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l04237"></a>04237                              uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04238"></a>04238                              LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *
<a name="l04239"></a>04239                              iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__,
<a name="l04240"></a>04240                              LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork,
<a name="l04241"></a>04241                              LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l04242"></a>04242  
<a name="l04243"></a>04243 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zherfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04244"></a>04244                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *ldaf, 
<a name="l04245"></a>04245                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04246"></a>04246                              LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work,
<a name="l04247"></a>04247                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04248"></a>04248  
<a name="l04249"></a>04249 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhesv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04250"></a>04250                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04251"></a>04251                             LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04252"></a>04252  
<a name="l04253"></a>04253 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhesvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04254"></a>04254                              nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *
<a name="l04255"></a>04255                              ldaf, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x,
<a name="l04256"></a>04256                              LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l04257"></a>04257                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04258"></a>04258  
<a name="l04259"></a>04259 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhetf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04260"></a>04260                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04261"></a>04261  
<a name="l04262"></a>04262 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhetrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04263"></a>04263                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l04264"></a>04264                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04265"></a>04265  
<a name="l04266"></a>04266 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhetrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04267"></a>04267                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l04268"></a>04268                              LAPACK_INTEGER *info);
<a name="l04269"></a>04269  
<a name="l04270"></a>04270 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhetri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04271"></a>04271                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04272"></a>04272  
<a name="l04273"></a>04273 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhetrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04274"></a>04274                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04275"></a>04275                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04276"></a>04276  
<a name="l04277"></a>04277 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhgeqz_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, 
<a name="l04278"></a>04278                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04279"></a>04279                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *
<a name="l04280"></a>04280                              beta, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *
<a name="l04281"></a>04281                              ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04282"></a>04282                              info);
<a name="l04283"></a>04283  
<a name="l04284"></a>04284 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04285"></a>04285                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *
<a name="l04286"></a>04286                              work, LAPACK_INTEGER *info);
<a name="l04287"></a>04287  
<a name="l04288"></a>04288 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpev_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX 
<a name="l04289"></a>04289                             *ap, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *
<a name="l04290"></a>04290                             work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04291"></a>04291  
<a name="l04292"></a>04292 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpevd_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04293"></a>04293                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04294"></a>04294                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04295"></a>04295                              lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04296"></a>04296  
<a name="l04297"></a>04297 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpevx_(<span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04298"></a>04298                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLEREAL *vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, 
<a name="l04299"></a>04299                              LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, 
<a name="l04300"></a>04300                              LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *
<a name="l04301"></a>04301                              rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l04302"></a>04302  
<a name="l04303"></a>04303 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpgst_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, 
<a name="l04304"></a>04304                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *bp, LAPACK_INTEGER *info);
<a name="l04305"></a>04305  
<a name="l04306"></a>04306 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpgv_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l04307"></a>04307                             n, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *bp, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX 
<a name="l04308"></a>04308                             *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04309"></a>04309                             info);
<a name="l04310"></a>04310  
<a name="l04311"></a>04311 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpgvd_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *
<a name="l04312"></a>04312                              n, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *bp, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX 
<a name="l04313"></a>04313                              *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *
<a name="l04314"></a>04314                              rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, LAPACK_INTEGER *
<a name="l04315"></a>04315                              info);
<a name="l04316"></a>04316  
<a name="l04317"></a>04317 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpgvx_(LAPACK_INTEGER *itype, <span class="keywordtype">char</span> *jobz, <span class="keywordtype">char</span> *range, <span class="keywordtype">char</span> *
<a name="l04318"></a>04318                              uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *bp, LAPACK_DOUBLEREAL *
<a name="l04319"></a>04319                              vl, LAPACK_DOUBLEREAL *vu, LAPACK_INTEGER *il, LAPACK_INTEGER *iu, LAPACK_DOUBLEREAL *abstol, 
<a name="l04320"></a>04320                              LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04321"></a>04321                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *
<a name="l04322"></a>04322                              ifail, LAPACK_INTEGER *info);
<a name="l04323"></a>04323  
<a name="l04324"></a>04324 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04325"></a>04325                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *
<a name="l04326"></a>04326                              b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, 
<a name="l04327"></a>04327                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04328"></a>04328                              info);
<a name="l04329"></a>04329  
<a name="l04330"></a>04330 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04331"></a>04331                             LAPACK_DOUBLECOMPLEX *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04332"></a>04332                             LAPACK_INTEGER *info);
<a name="l04333"></a>04333  
<a name="l04334"></a>04334 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhpsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04335"></a>04335                              nrhs, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, LAPACK_INTEGER *ipiv, 
<a name="l04336"></a>04336                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04337"></a>04337                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *
<a name="l04338"></a>04338                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04339"></a>04339  
<a name="l04340"></a>04340 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhptrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04341"></a>04341                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *tau, LAPACK_INTEGER *info);
<a name="l04342"></a>04342  
<a name="l04343"></a>04343 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04344"></a>04344                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04345"></a>04345  
<a name="l04346"></a>04346 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04347"></a>04347                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04348"></a>04348  
<a name="l04349"></a>04349 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04350"></a>04350                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04351"></a>04351                              LAPACK_INTEGER *info);
<a name="l04352"></a>04352  
<a name="l04353"></a>04353 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhsein_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *eigsrc, <span class="keywordtype">char</span> *initv, LAPACK_LOGICAL *
<a name="l04354"></a>04354                              select, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLECOMPLEX *
<a name="l04355"></a>04355                              w, LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr,
<a name="l04356"></a>04356                              LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, 
<a name="l04357"></a>04357                              LAPACK_INTEGER *ifaill, LAPACK_INTEGER *ifailr, LAPACK_INTEGER *info);
<a name="l04358"></a>04358  
<a name="l04359"></a>04359 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zhseqr_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_INTEGER *ilo,
<a name="l04360"></a>04360                              LAPACK_INTEGER *ihi, LAPACK_DOUBLECOMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLECOMPLEX *w, 
<a name="l04361"></a>04361                              LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l04362"></a>04362                              LAPACK_INTEGER *info);
<a name="l04363"></a>04363  
<a name="l04364"></a>04364 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlabrd_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, 
<a name="l04365"></a>04365                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l04366"></a>04366                              LAPACK_DOUBLECOMPLEX *tauq, LAPACK_DOUBLECOMPLEX *taup, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *
<a name="l04367"></a>04367                              ldx, LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *ldy);
<a name="l04368"></a>04368  
<a name="l04369"></a>04369 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacgv_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx);
<a name="l04370"></a>04370  
<a name="l04371"></a>04371 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacon_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *v, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04372"></a>04372                              LAPACK_DOUBLEREAL *est, LAPACK_INTEGER *kase);
<a name="l04373"></a>04373  
<a name="l04374"></a>04374 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacp2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *
<a name="l04375"></a>04375                              a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l04376"></a>04376  
<a name="l04377"></a>04377 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacpy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04378"></a>04378                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l04379"></a>04379  
<a name="l04380"></a>04380 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacrm_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04381"></a>04381                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *c__, 
<a name="l04382"></a>04382                              LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *rwork);
<a name="l04383"></a>04383  
<a name="l04384"></a>04384 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlacrt_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *cx, LAPACK_INTEGER *incx, 
<a name="l04385"></a>04385                              LAPACK_DOUBLECOMPLEX *cy, LAPACK_INTEGER *incy, LAPACK_DOUBLECOMPLEX *c__, LAPACK_DOUBLECOMPLEX *
<a name="l04386"></a>04386                              s);
<a name="l04387"></a>04387  
<a name="l04388"></a>04388 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaed0_(LAPACK_INTEGER *qsiz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l04389"></a>04389                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *qstore, 
<a name="l04390"></a>04390                              LAPACK_INTEGER *ldqs, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04391"></a>04391  
<a name="l04392"></a>04392 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaed7_(LAPACK_INTEGER *n, LAPACK_INTEGER *cutpnt, LAPACK_INTEGER *qsiz, 
<a name="l04393"></a>04393                              LAPACK_INTEGER *tlvls, LAPACK_INTEGER *curlvl, LAPACK_INTEGER *curpbm, LAPACK_DOUBLEREAL *d__, 
<a name="l04394"></a>04394                              LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *rho, LAPACK_INTEGER *indxq, 
<a name="l04395"></a>04395                              LAPACK_DOUBLEREAL *qstore, LAPACK_INTEGER *qptr, LAPACK_INTEGER *prmptr, LAPACK_INTEGER *perm, 
<a name="l04396"></a>04396                              LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_DOUBLEREAL *givnum, LAPACK_DOUBLECOMPLEX *
<a name="l04397"></a>04397                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04398"></a>04398  
<a name="l04399"></a>04399 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaed8_(LAPACK_INTEGER *k, LAPACK_INTEGER *n, LAPACK_INTEGER *qsiz, 
<a name="l04400"></a>04400                              LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *rho, 
<a name="l04401"></a>04401                              LAPACK_INTEGER *cutpnt, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *dlamda, LAPACK_DOUBLECOMPLEX *
<a name="l04402"></a>04402                              q2, LAPACK_INTEGER *ldq2, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *indxp, LAPACK_INTEGER *indx, 
<a name="l04403"></a>04403                              LAPACK_INTEGER *indxq, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, 
<a name="l04404"></a>04404                              LAPACK_DOUBLEREAL *givnum, LAPACK_INTEGER *info);
<a name="l04405"></a>04405  
<a name="l04406"></a>04406 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaein_(LAPACK_LOGICAL *rightv, LAPACK_LOGICAL *noinit, LAPACK_INTEGER *n, 
<a name="l04407"></a>04407                              LAPACK_DOUBLECOMPLEX *h__, LAPACK_INTEGER *ldh, LAPACK_DOUBLECOMPLEX *w, LAPACK_DOUBLECOMPLEX *v, 
<a name="l04408"></a>04408                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *rwork, LAPACK_DOUBLEREAL *eps3, 
<a name="l04409"></a>04409                              LAPACK_DOUBLEREAL *smlnum, LAPACK_INTEGER *info);
<a name="l04410"></a>04410  
<a name="l04411"></a>04411 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaesy_(LAPACK_DOUBLECOMPLEX *a, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04412"></a>04412                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_DOUBLECOMPLEX *rt1, LAPACK_DOUBLECOMPLEX *rt2, 
<a name="l04413"></a>04413                              LAPACK_DOUBLECOMPLEX *evscal, LAPACK_DOUBLECOMPLEX *cs1, LAPACK_DOUBLECOMPLEX *sn1);
<a name="l04414"></a>04414  
<a name="l04415"></a>04415 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaev2_(LAPACK_DOUBLECOMPLEX *a, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04416"></a>04416                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_DOUBLEREAL *rt1, LAPACK_DOUBLEREAL *rt2, LAPACK_DOUBLEREAL *cs1,
<a name="l04417"></a>04417                              LAPACK_DOUBLECOMPLEX *sn1);
<a name="l04418"></a>04418  
<a name="l04419"></a>04419 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlags2_(LAPACK_LOGICAL *upper, LAPACK_DOUBLEREAL *a1, LAPACK_DOUBLECOMPLEX *
<a name="l04420"></a>04420                              a2, LAPACK_DOUBLEREAL *a3, LAPACK_DOUBLEREAL *b1, LAPACK_DOUBLECOMPLEX *b2, LAPACK_DOUBLEREAL *b3,
<a name="l04421"></a>04421                              LAPACK_DOUBLEREAL *csu, LAPACK_DOUBLECOMPLEX *snu, LAPACK_DOUBLEREAL *csv, LAPACK_DOUBLECOMPLEX *
<a name="l04422"></a>04422                              snv, LAPACK_DOUBLEREAL *csq, LAPACK_DOUBLECOMPLEX *snq);
<a name="l04423"></a>04423  
<a name="l04424"></a>04424 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlagtm_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04425"></a>04425                              LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLECOMPLEX *dl, LAPACK_DOUBLECOMPLEX *d__, 
<a name="l04426"></a>04426                              LAPACK_DOUBLECOMPLEX *du, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *beta, 
<a name="l04427"></a>04427                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l04428"></a>04428  
<a name="l04429"></a>04429 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlahef_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l04430"></a>04430                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *w, 
<a name="l04431"></a>04431                              LAPACK_INTEGER *ldw, LAPACK_INTEGER *info);
<a name="l04432"></a>04432  
<a name="l04433"></a>04433 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlahqr_(LAPACK_LOGICAL *wantt, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l04434"></a>04434                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLECOMPLEX *h__, LAPACK_INTEGER *ldh, 
<a name="l04435"></a>04435                              LAPACK_DOUBLECOMPLEX *w, LAPACK_INTEGER *iloz, LAPACK_INTEGER *ihiz, LAPACK_DOUBLECOMPLEX *z__, 
<a name="l04436"></a>04436                              LAPACK_INTEGER *ldz, LAPACK_INTEGER *info);
<a name="l04437"></a>04437  
<a name="l04438"></a>04438 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlahrd_(LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *nb, 
<a name="l04439"></a>04439                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *t, 
<a name="l04440"></a>04440                              LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *ldy);
<a name="l04441"></a>04441  
<a name="l04442"></a>04442 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaic1_(LAPACK_INTEGER *job, LAPACK_INTEGER *j, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04443"></a>04443                              LAPACK_DOUBLEREAL *sest, LAPACK_DOUBLECOMPLEX *w, LAPACK_DOUBLECOMPLEX *gamma, LAPACK_DOUBLEREAL *
<a name="l04444"></a>04444                              sestpr, LAPACK_DOUBLECOMPLEX *s, LAPACK_DOUBLECOMPLEX *c__);
<a name="l04445"></a>04445  
<a name="l04446"></a>04446 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlals0_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *nl, LAPACK_INTEGER *nr, 
<a name="l04447"></a>04447                              LAPACK_INTEGER *sqre, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04448"></a>04448                              LAPACK_DOUBLECOMPLEX *bx, LAPACK_INTEGER *ldbx, LAPACK_INTEGER *perm, LAPACK_INTEGER *givptr, 
<a name="l04449"></a>04449                              LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_DOUBLEREAL *givnum, LAPACK_INTEGER *ldgnum,
<a name="l04450"></a>04450                              LAPACK_DOUBLEREAL *poles, LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, LAPACK_DOUBLEREAL *
<a name="l04451"></a>04451                              z__, LAPACK_INTEGER *k, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *rwork, 
<a name="l04452"></a>04452                              LAPACK_INTEGER *info);
<a name="l04453"></a>04453  
<a name="l04454"></a>04454 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlalsa_(LAPACK_INTEGER *icompq, LAPACK_INTEGER *smlsiz, LAPACK_INTEGER *n, 
<a name="l04455"></a>04455                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *bx, 
<a name="l04456"></a>04456                              LAPACK_INTEGER *ldbx, LAPACK_DOUBLEREAL *u, LAPACK_INTEGER *ldu, LAPACK_DOUBLEREAL *vt, LAPACK_INTEGER *
<a name="l04457"></a>04457                              k, LAPACK_DOUBLEREAL *difl, LAPACK_DOUBLEREAL *difr, LAPACK_DOUBLEREAL *z__, LAPACK_DOUBLEREAL *
<a name="l04458"></a>04458                              poles, LAPACK_INTEGER *givptr, LAPACK_INTEGER *givcol, LAPACK_INTEGER *ldgcol, LAPACK_INTEGER *
<a name="l04459"></a>04459                              perm, LAPACK_DOUBLEREAL *givnum, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *
<a name="l04460"></a>04460                              rwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04461"></a>04461  
<a name="l04462"></a>04462 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlapll_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, 
<a name="l04463"></a>04463                              LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *ssmin);
<a name="l04464"></a>04464  
<a name="l04465"></a>04465 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlapmt_(LAPACK_LOGICAL *forwrd, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04466"></a>04466                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_INTEGER *k);
<a name="l04467"></a>04467  
<a name="l04468"></a>04468 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqgb_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku,
<a name="l04469"></a>04469                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, 
<a name="l04470"></a>04470                              LAPACK_DOUBLEREAL *rowcnd, LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04471"></a>04471  
<a name="l04472"></a>04472 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqge_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04473"></a>04473                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *r__, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *rowcnd, 
<a name="l04474"></a>04474                              LAPACK_DOUBLEREAL *colcnd, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04475"></a>04475  
<a name="l04476"></a>04476 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqhb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04477"></a>04477                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, 
<a name="l04478"></a>04478                              LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04479"></a>04479  
<a name="l04480"></a>04480 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqhe_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04481"></a>04481                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, 
<a name="l04482"></a>04482                              <span class="keywordtype">char</span> *equed);
<a name="l04483"></a>04483  
<a name="l04484"></a>04484 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqhp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04485"></a>04485                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04486"></a>04486  
<a name="l04487"></a>04487 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqp2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, 
<a name="l04488"></a>04488                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l04489"></a>04489                              LAPACK_DOUBLEREAL *vn1, LAPACK_DOUBLEREAL *vn2, LAPACK_DOUBLECOMPLEX *work);
<a name="l04490"></a>04490  
<a name="l04491"></a>04491 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqps_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *offset, LAPACK_INTEGER 
<a name="l04492"></a>04492                              *nb, LAPACK_INTEGER *kb, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *jpvt, 
<a name="l04493"></a>04493                              LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLEREAL *vn1, LAPACK_DOUBLEREAL *vn2, LAPACK_DOUBLECOMPLEX *
<a name="l04494"></a>04494                              auxv, LAPACK_DOUBLECOMPLEX *f, LAPACK_INTEGER *ldf);
<a name="l04495"></a>04495  
<a name="l04496"></a>04496 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqsb_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04497"></a>04497                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, 
<a name="l04498"></a>04498                              LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04499"></a>04499  
<a name="l04500"></a>04500 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqsp_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04501"></a>04501                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, <span class="keywordtype">char</span> *equed);
<a name="l04502"></a>04502  
<a name="l04503"></a>04503 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaqsy_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04504"></a>04504                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, 
<a name="l04505"></a>04505                              <span class="keywordtype">char</span> *equed);
<a name="l04506"></a>04506  
<a name="l04507"></a>04507 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlar1v_(LAPACK_INTEGER *n, LAPACK_INTEGER *b1, LAPACK_INTEGER *bn, LAPACK_DOUBLEREAL 
<a name="l04508"></a>04508                              *sigma, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, LAPACK_DOUBLEREAL *ld, LAPACK_DOUBLEREAL *
<a name="l04509"></a>04509                              lld, LAPACK_DOUBLEREAL *gersch, LAPACK_DOUBLECOMPLEX *z__, LAPACK_DOUBLEREAL *ztz, 
<a name="l04510"></a>04510                              LAPACK_DOUBLEREAL *mingma, LAPACK_INTEGER *r__, LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work);
<a name="l04511"></a>04511  
<a name="l04512"></a>04512 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlar2v_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_DOUBLECOMPLEX *y, 
<a name="l04513"></a>04513                              LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *incx, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLECOMPLEX *s, 
<a name="l04514"></a>04514                              LAPACK_INTEGER *incc);
<a name="l04515"></a>04515  
<a name="l04516"></a>04516 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarcm_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *a, LAPACK_INTEGER *
<a name="l04517"></a>04517                              lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc,
<a name="l04518"></a>04518                              LAPACK_DOUBLEREAL *rwork);
<a name="l04519"></a>04519  
<a name="l04520"></a>04520 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarf_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX 
<a name="l04521"></a>04521                             *v, LAPACK_INTEGER *incv, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *
<a name="l04522"></a>04522                             ldc, LAPACK_DOUBLECOMPLEX *work);
<a name="l04523"></a>04523  
<a name="l04524"></a>04524 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarfb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l04525"></a>04525                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER 
<a name="l04526"></a>04526                              *ldv, LAPACK_DOUBLECOMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *
<a name="l04527"></a>04527                              ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *ldwork);
<a name="l04528"></a>04528  
<a name="l04529"></a>04529 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarfg_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *
<a name="l04530"></a>04530                              x, LAPACK_INTEGER *incx, LAPACK_DOUBLECOMPLEX *tau);
<a name="l04531"></a>04531  
<a name="l04532"></a>04532 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarft_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04533"></a>04533                              k, LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04534"></a>04534                              t, LAPACK_INTEGER *ldt);
<a name="l04535"></a>04535  
<a name="l04536"></a>04536 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarfx_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04537"></a>04537                              LAPACK_DOUBLECOMPLEX *v, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *
<a name="l04538"></a>04538                              ldc, LAPACK_DOUBLECOMPLEX *work);
<a name="l04539"></a>04539  
<a name="l04540"></a>04540 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlargv_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, 
<a name="l04541"></a>04541                              LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_INTEGER *incc);
<a name="l04542"></a>04542  
<a name="l04543"></a>04543 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarnv_(LAPACK_INTEGER *idist, LAPACK_INTEGER *iseed, LAPACK_INTEGER *n, 
<a name="l04544"></a>04544                              LAPACK_DOUBLECOMPLEX *x);
<a name="l04545"></a>04545  
<a name="l04546"></a>04546 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarrv_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *l, 
<a name="l04547"></a>04547                              LAPACK_INTEGER *isplit, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *iblock, 
<a name="l04548"></a>04548                              LAPACK_DOUBLEREAL *gersch, LAPACK_DOUBLEREAL *tol, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz,
<a name="l04549"></a>04549                              LAPACK_INTEGER *isuppz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04550"></a>04550  
<a name="l04551"></a>04551 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlartg_(LAPACK_DOUBLECOMPLEX *f, LAPACK_DOUBLECOMPLEX *g, LAPACK_DOUBLEREAL *
<a name="l04552"></a>04552                              cs, LAPACK_DOUBLECOMPLEX *sn, LAPACK_DOUBLECOMPLEX *r__);
<a name="l04553"></a>04553  
<a name="l04554"></a>04554 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlartv_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, 
<a name="l04555"></a>04555                              LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLECOMPLEX *s, 
<a name="l04556"></a>04556                              LAPACK_INTEGER *incc);
<a name="l04557"></a>04557  
<a name="l04558"></a>04558 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarz_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, 
<a name="l04559"></a>04559                             LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *incv, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04560"></a>04560                             c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work);
<a name="l04561"></a>04561  
<a name="l04562"></a>04562 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarzb_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *
<a name="l04563"></a>04563                              storev, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX 
<a name="l04564"></a>04564                              *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLECOMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *c__, 
<a name="l04565"></a>04565                              LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *ldwork);
<a name="l04566"></a>04566  
<a name="l04567"></a>04567 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlarzt_(<span class="keywordtype">char</span> *direct, <span class="keywordtype">char</span> *storev, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04568"></a>04568                              k, LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04569"></a>04569                              t, LAPACK_INTEGER *ldt);
<a name="l04570"></a>04570  
<a name="l04571"></a>04571 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlascl_(<span class="keywordtype">char</span> *type__, LAPACK_INTEGER *kl, LAPACK_INTEGER *ku, 
<a name="l04572"></a>04572                              LAPACK_DOUBLEREAL *cfrom, LAPACK_DOUBLEREAL *cto, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04573"></a>04573                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04574"></a>04574  
<a name="l04575"></a>04575 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaset_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04576"></a>04576                              LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *
<a name="l04577"></a>04577                              lda);
<a name="l04578"></a>04578  
<a name="l04579"></a>04579 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlasr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *pivot, <span class="keywordtype">char</span> *direct, LAPACK_INTEGER *m,
<a name="l04580"></a>04580                             LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04581"></a>04581                             LAPACK_INTEGER *lda);
<a name="l04582"></a>04582  
<a name="l04583"></a>04583 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlassq_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, 
<a name="l04584"></a>04584                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *sumsq);
<a name="l04585"></a>04585  
<a name="l04586"></a>04586 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlaswp_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04587"></a>04587                              LAPACK_INTEGER *k1, LAPACK_INTEGER *k2, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *incx);
<a name="l04588"></a>04588  
<a name="l04589"></a>04589 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlasyf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, LAPACK_INTEGER *kb,
<a name="l04590"></a>04590                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *w, 
<a name="l04591"></a>04591                              LAPACK_INTEGER *ldw, LAPACK_INTEGER *info);
<a name="l04592"></a>04592  
<a name="l04593"></a>04593 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatbs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l04594"></a>04594                              normin, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l04595"></a>04595                              LAPACK_DOUBLECOMPLEX *x, LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l04596"></a>04596  
<a name="l04597"></a>04597 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatdf_(LAPACK_INTEGER *ijob, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *z__, 
<a name="l04598"></a>04598                              LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *rhs, LAPACK_DOUBLEREAL *rdsum, LAPACK_DOUBLEREAL *
<a name="l04599"></a>04599                              rdscal, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *jpiv);
<a name="l04600"></a>04600  
<a name="l04601"></a>04601 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatps_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l04602"></a>04602                              normin, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *x, LAPACK_DOUBLEREAL *
<a name="l04603"></a>04603                              scale, LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l04604"></a>04604  
<a name="l04605"></a>04605 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatrd_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nb, 
<a name="l04606"></a>04606                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l04607"></a>04607                              LAPACK_DOUBLECOMPLEX *w, LAPACK_INTEGER *ldw);
<a name="l04608"></a>04608  
<a name="l04609"></a>04609 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, <span class="keywordtype">char</span> *
<a name="l04610"></a>04610                              normin, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04611"></a>04611                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *cnorm, LAPACK_INTEGER *info);
<a name="l04612"></a>04612  
<a name="l04613"></a>04613 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatrz_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *l, 
<a name="l04614"></a>04614                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04615"></a>04615                              work);
<a name="l04616"></a>04616  
<a name="l04617"></a>04617 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlatzm_(<span class="keywordtype">char</span> *side, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l04618"></a>04618                              LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *incv, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04619"></a>04619                              c1, LAPACK_DOUBLECOMPLEX *c2, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work);
<a name="l04620"></a>04620  
<a name="l04621"></a>04621 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlauu2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04622"></a>04622                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04623"></a>04623  
<a name="l04624"></a>04624 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zlauum_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04625"></a>04625                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04626"></a>04626  
<a name="l04627"></a>04627 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04628"></a>04628                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *
<a name="l04629"></a>04629                              rcond, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04630"></a>04630  
<a name="l04631"></a>04631 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04632"></a>04632                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, 
<a name="l04633"></a>04633                              LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l04634"></a>04634  
<a name="l04635"></a>04635 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l04636"></a>04636                              nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *afb, LAPACK_INTEGER *
<a name="l04637"></a>04637                              ldafb, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx,
<a name="l04638"></a>04638                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *
<a name="l04639"></a>04639                              rwork, LAPACK_INTEGER *info);
<a name="l04640"></a>04640  
<a name="l04641"></a>04641 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbstf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04642"></a>04642                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l04643"></a>04643  
<a name="l04644"></a>04644 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l04645"></a>04645                             nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *
<a name="l04646"></a>04646                             ldb, LAPACK_INTEGER *info);
<a name="l04647"></a>04647  
<a name="l04648"></a>04648 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04649"></a>04649                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *afb, 
<a name="l04650"></a>04650                              LAPACK_INTEGER *ldafb, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER 
<a name="l04651"></a>04651                              *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *
<a name="l04652"></a>04652                              ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, 
<a name="l04653"></a>04653                              LAPACK_INTEGER *info);
<a name="l04654"></a>04654  
<a name="l04655"></a>04655 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbtf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04656"></a>04656                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l04657"></a>04657  
<a name="l04658"></a>04658 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbtrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, 
<a name="l04659"></a>04659                              LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_INTEGER *info);
<a name="l04660"></a>04660  
<a name="l04661"></a>04661 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpbtrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *kd, LAPACK_INTEGER *
<a name="l04662"></a>04662                              nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *
<a name="l04663"></a>04663                              ldb, LAPACK_INTEGER *info);
<a name="l04664"></a>04664  
<a name="l04665"></a>04665 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpocon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04666"></a>04666                              LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *
<a name="l04667"></a>04667                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04668"></a>04668  
<a name="l04669"></a>04669 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpoequ_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04670"></a>04670                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l04671"></a>04671  
<a name="l04672"></a>04672 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zporfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04673"></a>04673                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *ldaf, 
<a name="l04674"></a>04674                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04675"></a>04675                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *
<a name="l04676"></a>04676                              rwork, LAPACK_INTEGER *info);
<a name="l04677"></a>04677  
<a name="l04678"></a>04678 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zposv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04679"></a>04679                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04680"></a>04680                             LAPACK_INTEGER *info);
<a name="l04681"></a>04681  
<a name="l04682"></a>04682 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zposvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04683"></a>04683                              nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *
<a name="l04684"></a>04684                              ldaf, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04685"></a>04685                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, 
<a name="l04686"></a>04686                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04687"></a>04687                              info);
<a name="l04688"></a>04688  
<a name="l04689"></a>04689 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpotf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04690"></a>04690                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04691"></a>04691  
<a name="l04692"></a>04692 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpotrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04693"></a>04693                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04694"></a>04694  
<a name="l04695"></a>04695 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpotri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04696"></a>04696                              LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04697"></a>04697  
<a name="l04698"></a>04698 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpotrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04699"></a>04699                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04700"></a>04700                              LAPACK_INTEGER *info);
<a name="l04701"></a>04701  
<a name="l04702"></a>04702 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zppcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04703"></a>04703                              LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL 
<a name="l04704"></a>04704                              *rwork, LAPACK_INTEGER *info);
<a name="l04705"></a>04705  
<a name="l04706"></a>04706 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zppequ_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04707"></a>04707                              LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *scond, LAPACK_DOUBLEREAL *amax, LAPACK_INTEGER *info);
<a name="l04708"></a>04708  
<a name="l04709"></a>04709 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04710"></a>04710                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb,
<a name="l04711"></a>04711                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l04712"></a>04712                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04713"></a>04713  
<a name="l04714"></a>04714 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zppsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04715"></a>04715                             LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04716"></a>04716  
<a name="l04717"></a>04717 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zppsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04718"></a>04718                              nrhs, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, <span class="keywordtype">char</span> *equed, LAPACK_DOUBLEREAL *
<a name="l04719"></a>04719                              s, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04720"></a>04720                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *
<a name="l04721"></a>04721                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04722"></a>04722  
<a name="l04723"></a>04723 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04724"></a>04724                              LAPACK_INTEGER *info);
<a name="l04725"></a>04725  
<a name="l04726"></a>04726 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04727"></a>04727                              LAPACK_INTEGER *info);
<a name="l04728"></a>04728  
<a name="l04729"></a>04729 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04730"></a>04730                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04731"></a>04731  
<a name="l04732"></a>04732 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zptcon_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, 
<a name="l04733"></a>04733                              LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04734"></a>04734                              info);
<a name="l04735"></a>04735  
<a name="l04736"></a>04736 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zptrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04737"></a>04737                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, LAPACK_DOUBLEREAL *df, LAPACK_DOUBLECOMPLEX *ef, 
<a name="l04738"></a>04738                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04739"></a>04739                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *
<a name="l04740"></a>04740                              rwork, LAPACK_INTEGER *info);
<a name="l04741"></a>04741  
<a name="l04742"></a>04742 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zptsv_(LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, LAPACK_DOUBLEREAL *d__, 
<a name="l04743"></a>04743                             LAPACK_DOUBLECOMPLEX *e, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04744"></a>04744  
<a name="l04745"></a>04745 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zptsvx_(<span class="keywordtype">char</span> *fact, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04746"></a>04746                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, LAPACK_DOUBLEREAL *df, LAPACK_DOUBLECOMPLEX *ef, 
<a name="l04747"></a>04747                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04748"></a>04748                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *
<a name="l04749"></a>04749                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04750"></a>04750  
<a name="l04751"></a>04751 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpttrf_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, 
<a name="l04752"></a>04752                              LAPACK_INTEGER *info);
<a name="l04753"></a>04753  
<a name="l04754"></a>04754 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zpttrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04755"></a>04755                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04756"></a>04756                              LAPACK_INTEGER *info);
<a name="l04757"></a>04757  
<a name="l04758"></a>04758 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zptts2_(LAPACK_INTEGER *iuplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04759"></a>04759                              LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLECOMPLEX *e, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb);
<a name="l04760"></a>04760  
<a name="l04761"></a>04761 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zrot_(LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *cx, LAPACK_INTEGER *incx, 
<a name="l04762"></a>04762                            LAPACK_DOUBLECOMPLEX *cy, LAPACK_INTEGER *incy, LAPACK_DOUBLEREAL *c__, LAPACK_DOUBLECOMPLEX *s);
<a name="l04763"></a>04763  
<a name="l04764"></a>04764 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zspcon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04765"></a>04765                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *
<a name="l04766"></a>04766                              work, LAPACK_INTEGER *info);
<a name="l04767"></a>04767  
<a name="l04768"></a>04768 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zspmv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *alpha, 
<a name="l04769"></a>04769                             LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, LAPACK_DOUBLECOMPLEX *
<a name="l04770"></a>04770                             beta, LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *incy);
<a name="l04771"></a>04771  
<a name="l04772"></a>04772 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zspr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *alpha, 
<a name="l04773"></a>04773                            LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, LAPACK_DOUBLECOMPLEX *ap);
<a name="l04774"></a>04774  
<a name="l04775"></a>04775 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsprfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04776"></a>04776                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *
<a name="l04777"></a>04777                              b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, 
<a name="l04778"></a>04778                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04779"></a>04779                              info);
<a name="l04780"></a>04780  
<a name="l04781"></a>04781 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zspsv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04782"></a>04782                             LAPACK_DOUBLECOMPLEX *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04783"></a>04783                             LAPACK_INTEGER *info);
<a name="l04784"></a>04784  
<a name="l04785"></a>04785 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zspsvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04786"></a>04786                              nrhs, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *afp, LAPACK_INTEGER *ipiv, 
<a name="l04787"></a>04787                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04788"></a>04788                              LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *
<a name="l04789"></a>04789                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04790"></a>04790  
<a name="l04791"></a>04791 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsptrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04792"></a>04792                              LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04793"></a>04793  
<a name="l04794"></a>04794 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsptri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l04795"></a>04795                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04796"></a>04796  
<a name="l04797"></a>04797 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsptrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04798"></a>04798                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04799"></a>04799                              LAPACK_INTEGER *info);
<a name="l04800"></a>04800  
<a name="l04801"></a>04801 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zstedc_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l04802"></a>04802                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04803"></a>04803                              LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *lrwork, LAPACK_INTEGER *iwork, 
<a name="l04804"></a>04804                              LAPACK_INTEGER *liwork, LAPACK_INTEGER *info);
<a name="l04805"></a>04805  
<a name="l04806"></a>04806 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zstein_(LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, LAPACK_DOUBLEREAL *e, 
<a name="l04807"></a>04807                              LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *w, LAPACK_INTEGER *iblock, LAPACK_INTEGER *isplit, 
<a name="l04808"></a>04808                              LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, LAPACK_INTEGER *iwork, 
<a name="l04809"></a>04809                              LAPACK_INTEGER *ifail, LAPACK_INTEGER *info);
<a name="l04810"></a>04810  
<a name="l04811"></a>04811 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsteqr_(<span class="keywordtype">char</span> *compz, LAPACK_INTEGER *n, LAPACK_DOUBLEREAL *d__, 
<a name="l04812"></a>04812                              LAPACK_DOUBLEREAL *e, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, LAPACK_DOUBLEREAL *work, 
<a name="l04813"></a>04813                              LAPACK_INTEGER *info);
<a name="l04814"></a>04814  
<a name="l04815"></a>04815 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsycon_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04816"></a>04816                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLEREAL *anorm, LAPACK_DOUBLEREAL *rcond, 
<a name="l04817"></a>04817                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04818"></a>04818  
<a name="l04819"></a>04819 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsymv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *alpha, 
<a name="l04820"></a>04820                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, 
<a name="l04821"></a>04821                             LAPACK_DOUBLECOMPLEX *beta, LAPACK_DOUBLECOMPLEX *y, LAPACK_INTEGER *incy);
<a name="l04822"></a>04822  
<a name="l04823"></a>04823 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsyr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *alpha, 
<a name="l04824"></a>04824                            LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *incx, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda);
<a name="l04825"></a>04825  
<a name="l04826"></a>04826 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsyrfs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04827"></a>04827                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *ldaf, 
<a name="l04828"></a>04828                              LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, 
<a name="l04829"></a>04829                              LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work,
<a name="l04830"></a>04830                              LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04831"></a>04831  
<a name="l04832"></a>04832 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsysv_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04833"></a>04833                             LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04834"></a>04834                             LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04835"></a>04835  
<a name="l04836"></a>04836 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsysvx_(<span class="keywordtype">char</span> *fact, <span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *
<a name="l04837"></a>04837                              nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *af, LAPACK_INTEGER *
<a name="l04838"></a>04838                              ldaf, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x,
<a name="l04839"></a>04839                              LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l04840"></a>04840                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04841"></a>04841  
<a name="l04842"></a>04842 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsytf2_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04843"></a>04843                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_INTEGER *info);
<a name="l04844"></a>04844  
<a name="l04845"></a>04845 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsytrf_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04846"></a>04846                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, 
<a name="l04847"></a>04847                              LAPACK_INTEGER *info);
<a name="l04848"></a>04848  
<a name="l04849"></a>04849 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsytri_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04850"></a>04850                              LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l04851"></a>04851  
<a name="l04852"></a>04852 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zsytrs_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_INTEGER *nrhs, 
<a name="l04853"></a>04853                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *ipiv, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04854"></a>04854                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04855"></a>04855  
<a name="l04856"></a>04856 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztbcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04857"></a>04857                              LAPACK_INTEGER *kd, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, LAPACK_DOUBLEREAL *rcond, 
<a name="l04858"></a>04858                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04859"></a>04859  
<a name="l04860"></a>04860 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztbrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04861"></a>04861                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l04862"></a>04862                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, 
<a name="l04863"></a>04863                              LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *
<a name="l04864"></a>04864                              rwork, LAPACK_INTEGER *info);
<a name="l04865"></a>04865  
<a name="l04866"></a>04866 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztbtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04867"></a>04867                              LAPACK_INTEGER *kd, LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ab, LAPACK_INTEGER *ldab, 
<a name="l04868"></a>04868                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04869"></a>04869  
<a name="l04870"></a>04870 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l04871"></a>04871                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER 
<a name="l04872"></a>04872                              *ldb, LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *
<a name="l04873"></a>04873                              ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork,
<a name="l04874"></a>04874                              LAPACK_INTEGER *info);
<a name="l04875"></a>04875  
<a name="l04876"></a>04876 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgex2_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l04877"></a>04877                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04878"></a>04878                              LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04879"></a>04879                              LAPACK_INTEGER *j1, LAPACK_INTEGER *info);
<a name="l04880"></a>04880  
<a name="l04881"></a>04881 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgexc_(LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, LAPACK_INTEGER *n, 
<a name="l04882"></a>04882                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04883"></a>04883                              LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *ldz, 
<a name="l04884"></a>04884                              LAPACK_INTEGER *ifst, LAPACK_INTEGER *ilst, LAPACK_INTEGER *info);
<a name="l04885"></a>04885  
<a name="l04886"></a>04886 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgsen_(LAPACK_INTEGER *ijob, LAPACK_LOGICAL *wantq, LAPACK_LOGICAL *wantz, 
<a name="l04887"></a>04887                              LAPACK_LOGICAL *select, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l04888"></a>04888                              LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *alpha, LAPACK_DOUBLECOMPLEX *
<a name="l04889"></a>04889                              beta, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *z__, LAPACK_INTEGER *
<a name="l04890"></a>04890                              ldz, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *pl, LAPACK_DOUBLEREAL *pr, LAPACK_DOUBLEREAL *dif, 
<a name="l04891"></a>04891                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *liwork, 
<a name="l04892"></a>04892                              LAPACK_INTEGER *info);
<a name="l04893"></a>04893  
<a name="l04894"></a>04894 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgsja_(<span class="keywordtype">char</span> *jobu, <span class="keywordtype">char</span> *jobv, <span class="keywordtype">char</span> *jobq, LAPACK_INTEGER *m, 
<a name="l04895"></a>04895                              LAPACK_INTEGER *p, LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04896"></a>04896                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, LAPACK_DOUBLEREAL *tola, 
<a name="l04897"></a>04897                              LAPACK_DOUBLEREAL *tolb, LAPACK_DOUBLEREAL *alpha, LAPACK_DOUBLEREAL *beta, LAPACK_DOUBLECOMPLEX *
<a name="l04898"></a>04898                              u, LAPACK_INTEGER *ldu, LAPACK_DOUBLECOMPLEX *v, LAPACK_INTEGER *ldv, LAPACK_DOUBLECOMPLEX *q, 
<a name="l04899"></a>04899                              LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *ncycle, LAPACK_INTEGER *info);
<a name="l04900"></a>04900  
<a name="l04901"></a>04901 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l04902"></a>04902                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER 
<a name="l04903"></a>04903                              *ldb, LAPACK_DOUBLECOMPLEX *vl, LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *
<a name="l04904"></a>04904                              ldvr, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *dif, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, 
<a name="l04905"></a>04905                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04906"></a>04906  
<a name="l04907"></a>04907 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgsy2_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l04908"></a>04908                              n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04909"></a>04909                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *d__, LAPACK_INTEGER *ldd, 
<a name="l04910"></a>04910                              LAPACK_DOUBLECOMPLEX *e, LAPACK_INTEGER *lde, LAPACK_DOUBLECOMPLEX *f, LAPACK_INTEGER *ldf, 
<a name="l04911"></a>04911                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *rdsum, LAPACK_DOUBLEREAL *rdscal, LAPACK_INTEGER *
<a name="l04912"></a>04912                              info);
<a name="l04913"></a>04913  
<a name="l04914"></a>04914 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztgsyl_(<span class="keywordtype">char</span> *trans, LAPACK_INTEGER *ijob, LAPACK_INTEGER *m, LAPACK_INTEGER *
<a name="l04915"></a>04915                              n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04916"></a>04916                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *d__, LAPACK_INTEGER *ldd, 
<a name="l04917"></a>04917                              LAPACK_DOUBLECOMPLEX *e, LAPACK_INTEGER *lde, LAPACK_DOUBLECOMPLEX *f, LAPACK_INTEGER *ldf, 
<a name="l04918"></a>04918                              LAPACK_DOUBLEREAL *scale, LAPACK_DOUBLEREAL *dif, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l04919"></a>04919                              lwork, LAPACK_INTEGER *iwork, LAPACK_INTEGER *info);
<a name="l04920"></a>04920  
<a name="l04921"></a>04921 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztpcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04922"></a>04922                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL 
<a name="l04923"></a>04923                              *rwork, LAPACK_INTEGER *info);
<a name="l04924"></a>04924  
<a name="l04925"></a>04925 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztprfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04926"></a>04926                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04927"></a>04927                              LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, LAPACK_DOUBLEREAL *berr, 
<a name="l04928"></a>04928                              LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04929"></a>04929  
<a name="l04930"></a>04930 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztptri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04931"></a>04931                              LAPACK_DOUBLECOMPLEX *ap, LAPACK_INTEGER *info);
<a name="l04932"></a>04932  
<a name="l04933"></a>04933 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztptrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04934"></a>04934                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *b, LAPACK_INTEGER *ldb, 
<a name="l04935"></a>04935                              LAPACK_INTEGER *info);
<a name="l04936"></a>04936  
<a name="l04937"></a>04937 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrcon_(<span class="keywordtype">char</span> *norm, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04938"></a>04938                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLEREAL *rcond, LAPACK_DOUBLECOMPLEX *
<a name="l04939"></a>04939                              work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04940"></a>04940  
<a name="l04941"></a>04941 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrevc_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l04942"></a>04942                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *vl, 
<a name="l04943"></a>04943                              LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_INTEGER *mm, LAPACK_INTEGER 
<a name="l04944"></a>04944                              *m, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04945"></a>04945  
<a name="l04946"></a>04946 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrexc_(<span class="keywordtype">char</span> *compq, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *t, 
<a name="l04947"></a>04947                              LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_INTEGER *ifst, LAPACK_INTEGER *
<a name="l04948"></a>04948                              ilst, LAPACK_INTEGER *info);
<a name="l04949"></a>04949  
<a name="l04950"></a>04950 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrrfs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04951"></a>04951                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04952"></a>04952                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *x, LAPACK_INTEGER *ldx, LAPACK_DOUBLEREAL *ferr, 
<a name="l04953"></a>04953                              LAPACK_DOUBLEREAL *berr, LAPACK_DOUBLECOMPLEX *work, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *
<a name="l04954"></a>04954                              info);
<a name="l04955"></a>04955  
<a name="l04956"></a>04956 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrsen_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *compq, LAPACK_LOGICAL *select, LAPACK_INTEGER 
<a name="l04957"></a>04957                              *n, LAPACK_DOUBLECOMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, 
<a name="l04958"></a>04958                              LAPACK_DOUBLECOMPLEX *w, LAPACK_INTEGER *m, LAPACK_DOUBLEREAL *s, LAPACK_DOUBLEREAL *sep, 
<a name="l04959"></a>04959                              LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l04960"></a>04960  
<a name="l04961"></a>04961 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrsna_(<span class="keywordtype">char</span> *job, <span class="keywordtype">char</span> *howmny, LAPACK_LOGICAL *select, 
<a name="l04962"></a>04962                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *t, LAPACK_INTEGER *ldt, LAPACK_DOUBLECOMPLEX *vl, 
<a name="l04963"></a>04963                              LAPACK_INTEGER *ldvl, LAPACK_DOUBLECOMPLEX *vr, LAPACK_INTEGER *ldvr, LAPACK_DOUBLEREAL *s, 
<a name="l04964"></a>04964                              LAPACK_DOUBLEREAL *sep, LAPACK_INTEGER *mm, LAPACK_INTEGER *m, LAPACK_DOUBLECOMPLEX *work, 
<a name="l04965"></a>04965                              LAPACK_INTEGER *ldwork, LAPACK_DOUBLEREAL *rwork, LAPACK_INTEGER *info);
<a name="l04966"></a>04966  
<a name="l04967"></a>04967 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrsyl_(<span class="keywordtype">char</span> *trana, <span class="keywordtype">char</span> *tranb, LAPACK_INTEGER *isgn, LAPACK_INTEGER 
<a name="l04968"></a>04968                              *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04969"></a>04969                              LAPACK_INTEGER *ldb, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLEREAL *scale, 
<a name="l04970"></a>04970                              LAPACK_INTEGER *info);
<a name="l04971"></a>04971  
<a name="l04972"></a>04972 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrti2_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04973"></a>04973                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04974"></a>04974  
<a name="l04975"></a>04975 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrtri_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04976"></a>04976                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_INTEGER *info);
<a name="l04977"></a>04977  
<a name="l04978"></a>04978 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztrtrs_(<span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, <span class="keywordtype">char</span> *diag, LAPACK_INTEGER *n, 
<a name="l04979"></a>04979                              LAPACK_INTEGER *nrhs, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *b, 
<a name="l04980"></a>04980                              LAPACK_INTEGER *ldb, LAPACK_INTEGER *info);
<a name="l04981"></a>04981  
<a name="l04982"></a>04982 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztzrqf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04983"></a>04983                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_INTEGER *info);
<a name="l04984"></a>04984  
<a name="l04985"></a>04985 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> ztzrzf_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l04986"></a>04986                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l04987"></a>04987                              LAPACK_INTEGER *info);
<a name="l04988"></a>04988  
<a name="l04989"></a>04989 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zung2l_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l04990"></a>04990                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04991"></a>04991                              work, LAPACK_INTEGER *info);
<a name="l04992"></a>04992  
<a name="l04993"></a>04993 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zung2r_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l04994"></a>04994                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04995"></a>04995                              work, LAPACK_INTEGER *info);
<a name="l04996"></a>04996  
<a name="l04997"></a>04997 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungbr_(<span class="keywordtype">char</span> *vect, LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l04998"></a>04998                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l04999"></a>04999                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05000"></a>05000  
<a name="l05001"></a>05001 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunghr_(LAPACK_INTEGER *n, LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, 
<a name="l05002"></a>05002                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05003"></a>05003                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05004"></a>05004  
<a name="l05005"></a>05005 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungl2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05006"></a>05006                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05007"></a>05007                              work, LAPACK_INTEGER *info);
<a name="l05008"></a>05008  
<a name="l05009"></a>05009 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunglq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05010"></a>05010                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05011"></a>05011                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05012"></a>05012  
<a name="l05013"></a>05013 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungql_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05014"></a>05014                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05015"></a>05015                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05016"></a>05016  
<a name="l05017"></a>05017 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungqr_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05018"></a>05018                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05019"></a>05019                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05020"></a>05020  
<a name="l05021"></a>05021 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungr2_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05022"></a>05022                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05023"></a>05023                              work, LAPACK_INTEGER *info);
<a name="l05024"></a>05024  
<a name="l05025"></a>05025 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungrq_(LAPACK_INTEGER *m, LAPACK_INTEGER *n, LAPACK_INTEGER *k, 
<a name="l05026"></a>05026                              LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *
<a name="l05027"></a>05027                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05028"></a>05028  
<a name="l05029"></a>05029 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zungtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, 
<a name="l05030"></a>05030                              LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05031"></a>05031                              LAPACK_INTEGER *info);
<a name="l05032"></a>05032  
<a name="l05033"></a>05033 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunm2l_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05034"></a>05034                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05035"></a>05035                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l05036"></a>05036  
<a name="l05037"></a>05037 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunm2r_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05038"></a>05038                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05039"></a>05039                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l05040"></a>05040  
<a name="l05041"></a>05041 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmbr_(<span class="keywordtype">char</span> *vect, <span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l05042"></a>05042                              LAPACK_INTEGER *n, LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX 
<a name="l05043"></a>05043                              *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l05044"></a>05044                              lwork, LAPACK_INTEGER *info);
<a name="l05045"></a>05045  
<a name="l05046"></a>05046 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmhr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05047"></a>05047                              LAPACK_INTEGER *ilo, LAPACK_INTEGER *ihi, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, 
<a name="l05048"></a>05048                              LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *
<a name="l05049"></a>05049                              work, LAPACK_INTEGER *lwork, LAPACK_INTEGER *info);
<a name="l05050"></a>05050  
<a name="l05051"></a>05051 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunml2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05052"></a>05052                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05053"></a>05053                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l05054"></a>05054  
<a name="l05055"></a>05055 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmlq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05056"></a>05056                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05057"></a>05057                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05058"></a>05058                              LAPACK_INTEGER *info);
<a name="l05059"></a>05059  
<a name="l05060"></a>05060 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmql_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05061"></a>05061                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05062"></a>05062                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05063"></a>05063                              LAPACK_INTEGER *info);
<a name="l05064"></a>05064  
<a name="l05065"></a>05065 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmqr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05066"></a>05066                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05067"></a>05067                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05068"></a>05068                              LAPACK_INTEGER *info);
<a name="l05069"></a>05069  
<a name="l05070"></a>05070 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmr2_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05071"></a>05071                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05072"></a>05072                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l05073"></a>05073  
<a name="l05074"></a>05074 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmr3_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05075"></a>05075                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX 
<a name="l05076"></a>05076                              *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l05077"></a>05077                              info);
<a name="l05078"></a>05078  
<a name="l05079"></a>05079 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmrq_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05080"></a>05080                              LAPACK_INTEGER *k, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05081"></a>05081                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05082"></a>05082                              LAPACK_INTEGER *info);
<a name="l05083"></a>05083  
<a name="l05084"></a>05084 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmrz_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, LAPACK_INTEGER *n, 
<a name="l05085"></a>05085                              LAPACK_INTEGER *k, LAPACK_INTEGER *l, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX 
<a name="l05086"></a>05086                              *tau, LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *
<a name="l05087"></a>05087                              lwork, LAPACK_INTEGER *info);
<a name="l05088"></a>05088  
<a name="l05089"></a>05089 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zunmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l05090"></a>05090                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *a, LAPACK_INTEGER *lda, LAPACK_DOUBLECOMPLEX *tau, 
<a name="l05091"></a>05091                              LAPACK_DOUBLECOMPLEX *c__, LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *lwork,
<a name="l05092"></a>05092                              LAPACK_INTEGER *info);
<a name="l05093"></a>05093  
<a name="l05094"></a>05094 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zupgtr_(<span class="keywordtype">char</span> *uplo, LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, 
<a name="l05095"></a>05095                              LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *q, LAPACK_INTEGER *ldq, LAPACK_DOUBLECOMPLEX *
<a name="l05096"></a>05096                              work, LAPACK_INTEGER *info);
<a name="l05097"></a>05097  
<a name="l05098"></a>05098 <span class="comment">/* Subroutine */</span> <span class="keywordtype">int</span> zupmtr_(<span class="keywordtype">char</span> *side, <span class="keywordtype">char</span> *uplo, <span class="keywordtype">char</span> *trans, LAPACK_INTEGER *m, 
<a name="l05099"></a>05099                              LAPACK_INTEGER *n, LAPACK_DOUBLECOMPLEX *ap, LAPACK_DOUBLECOMPLEX *tau, LAPACK_DOUBLECOMPLEX *c__,
<a name="l05100"></a>05100                              LAPACK_INTEGER *ldc, LAPACK_DOUBLECOMPLEX *work, LAPACK_INTEGER *info);
<a name="l05101"></a>05101 
<a name="l05102"></a>05102 <span class="preprocessor">#endif </span><span class="comment">/* __CLAPACK_H */</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
