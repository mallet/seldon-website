<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">collection_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf">Copy</a>(const HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588">Deallocate</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_dense_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6">double_dense_c_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_dense_m</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_sparse_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482">double_sparse_c_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>double_sparse_m</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_dense_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d">float_dense_c_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_dense_m</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_sparse_c</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85">float_sparse_c_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>float_sparse_m</b> typedef (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f">GetDoubleDense</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bc398080b4454e0bcbf968fd25fbd78">GetDoubleDense</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af">GetDoubleSparse</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1a4f404541bb2b70a36cc1a193ee5cb7">GetDoubleSparse</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6">GetFloatDense</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa53a233cf9ea59c8711247cc013d75b7">GetFloatDense</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24">GetFloatSparse</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#afbdb423cb862ab27779c8e526f4f05e6">GetFloatSparse</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">GetM</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac368f84e127c6738f7f06c74bbc6e7da">GetM</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;::GetM</a>(const Seldon::SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetMatrix</b>(int m, int n, float_dense_m &amp;) const (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetMatrix</b>(int m, int n, float_sparse_m &amp;) const (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetMatrix</b>(int m, int n, double_dense_m &amp;) const (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetMatrix</b>(int m, int n, double_sparse_m &amp;) const (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa">GetMmatrix</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">GetN</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3b788a9ac72e2f32842dcc9251c6ca0f">GetN</a>(int j) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;::GetN</a>(const Seldon::SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe">GetNmatrix</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">GetSize</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e">GetType</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553">HeterogeneousMatrixCollection</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a686bd977ef7310cb3339289ec07eefc2">HeterogeneousMatrixCollection</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa473473c04fea0716d88de40c87b282b">HeterogeneousMatrixCollection</a>(const HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a20314db5f8dd0fcf4dd175e4b1a4852f">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a80ce2837902243f345157a46095ac8ba">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php">Seldon::Matrix&lt; FloatDouble, General, DenseSparseCollection, Allocator&lt; double &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td><code> [explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; double, Allocator&lt; double &gt; &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">Mlocal_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">Mlocal_sum_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">Mmatrix_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09">Nlocal_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82">Nlocal_sum_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252">Nmatrix_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a19f1fe843dbb872c589db2116cd204f9">Nullify</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e">nz_</a></td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#accc5a22477245a42cd0e017157f7d17d">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e">operator=</a>(const HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2c15862a4b7347bff455204fab3ea161">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetMatrix</b>(int m, int n, const float_dense_m &amp;) (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetMatrix</b>(int m, int n, const float_sparse_m &amp;) (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetMatrix</b>(int m, int n, const double_dense_m &amp;) (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetMatrix</b>(int m, int n, const double_sparse_m &amp;) (defined in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544">Write</a>(string FileName, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0c289b98c4bd4ea56dab00a13b88761b">Write</a>(ostream &amp;FileStream, bool with_size) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a85ddf0c1055330e18f77175da318432e">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89">~HeterogeneousMatrixCollection</a>()</td><td><a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; double, Allocator&lt; double &gt; &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
