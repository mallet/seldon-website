<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a> &#124;
<a href="#pro-static-attribs">Static Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_Triangular" --><!-- doxytag: inherits="Seldon::Matrix_Base" -->
<p>Triangular matrix stored in a full matrix.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix___triangular.png" usemap="#Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;_map" alt=""/>
  <map id="Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;_map" name="Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;_map">
<area href="class_seldon_1_1_matrix___base.php" alt="Seldon::Matrix_Base&lt; T, Allocator &gt;" shape="rect" coords="0,0,337,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix___triangular-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af2ec39e479b40f4de27ccdbd43aac61c"></a><!-- doxytag: member="Seldon::Matrix_Triangular::value_type" ref="af2ec39e479b40f4de27ccdbd43aac61c" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a678d8564dae9321d01855ddaedd3f2e5"></a><!-- doxytag: member="Seldon::Matrix_Triangular::pointer" ref="a678d8564dae9321d01855ddaedd3f2e5" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acfe6fc31719beba161a7a239ecc19898"></a><!-- doxytag: member="Seldon::Matrix_Triangular::const_pointer" ref="acfe6fc31719beba161a7a239ecc19898" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a51954aa8763823c2988800aaebbe954f"></a><!-- doxytag: member="Seldon::Matrix_Triangular::reference" ref="a51954aa8763823c2988800aaebbe954f" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a656d4a2bdebc8540f43fa438a86f91a3"></a><!-- doxytag: member="Seldon::Matrix_Triangular::const_reference" ref="a656d4a2bdebc8540f43fa438a86f91a3" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a036630b94293d1cc355175d6723b9dc6"></a><!-- doxytag: member="Seldon::Matrix_Triangular::entry_type" ref="a036630b94293d1cc355175d6723b9dc6" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4091ee95578345786b7eeb6989849da2"></a><!-- doxytag: member="Seldon::Matrix_Triangular::access_type" ref="a4091ee95578345786b7eeb6989849da2" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a66164aa03a0f8a73eeb700dce9da34e7"></a><!-- doxytag: member="Seldon::Matrix_Triangular::const_access_type" ref="a66164aa03a0f8a73eeb700dce9da34e7" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14">Matrix_Triangular</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a60a394e341c7e0425b5c51f89e93dd14"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a3ecbb30f1d5aee888ef396bbfde1f61b">Matrix_Triangular</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Main constructor.  <a href="#a3ecbb30f1d5aee888ef396bbfde1f61b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae2727885c2d249d8f386e55403133a7d"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Matrix_Triangular" ref="ae2727885c2d249d8f386e55403133a7d" args="(const Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ae2727885c2d249d8f386e55403133a7d">Matrix_Triangular</a> (const <a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Copy constructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a1bf20e38fe0a64a4db93991819fba679"></a><!-- doxytag: member="Seldon::Matrix_Triangular::~Matrix_Triangular" ref="a1bf20e38fe0a64a4db93991819fba679" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a1bf20e38fe0a64a4db93991819fba679">~Matrix_Triangular</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a01cf3952c69288e68376db61cf322c77">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a01cf3952c69288e68376db61cf322c77"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a812d8809554264f3b8f64429231b4e39"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#a472fa39bd3faee779cae713a5bd1aeb3"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix and keeps previous entries.  <a href="#a30b10bb11499845df9d647c3e0ef8915"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acd9c558870f264868aea4e12de896ed7"></a><!-- doxytag: member="Seldon::Matrix_Triangular::SetData" ref="acd9c558870f264868aea4e12de896ed7" args="(int i, int j, pointer data)" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>SetData</b> (int i, int j, pointer data)</td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5380ef67c34e203bc186a8ca351abbbb">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#a5380ef67c34e203bc186a8ca351abbbb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a5f07919d9f3d4d8ac483247e4d9d752f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">value_type&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a6cc7b33b27d2a8d51293614d176ebfc0">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a6cc7b33b27d2a8d51293614d176ebfc0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#aca8b82d5764a1943b2ec9a36d502816f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab2e953becbaab6664f72752932e14f5c">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#ab2e953becbaab6664f72752932e14f5c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c">operator[]</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#a30661e664fd84702ea900809e4c7764c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab4f150251c1afd88a7a59a382ac4f623">operator[]</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access to elements of the data array.  <a href="#ab4f150251c1afd88a7a59a382ac4f623"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4">operator=</a> (const <a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix (assignment operator).  <a href="#ab379631da575bbb4b2404f1fc48c37a4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d">Copy</a> (const <a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp;A)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Duplicates a matrix.  <a href="#a22a6a81034ad25df2123e33d1fb9052d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#af668e8676a93c1a99ebee4794994cc6e">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets all elements to zero.  <a href="#af668e8676a93c1a99ebee4794994cc6e"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a17b39f20590f545d3dce529aeaab9d5f"></a><!-- doxytag: member="Seldon::Matrix_Triangular::SetIdentity" ref="a17b39f20590f545d3dce529aeaab9d5f" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a17b39f20590f545d3dce529aeaab9d5f">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Sets the matrix to the identity. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with 0, 1, 2, ...  <a href="#a12f60cdce0a9a1dc924cfab291af4178"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#abddc5c5f65873a308e5f6919f5dc4023">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#abddc5c5f65873a308e5f6919f5dc4023"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a2f25eaae12f9e30ddad62426278dee62">operator=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix with a given value.  <a href="#a2f25eaae12f9e30ddad62426278dee62"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ad9f5e4d6ffc7dff60d8946e26102f71b">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Fills the matrix randomly.  <a href="#ad9f5e4d6ffc7dff60d8946e26102f71b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#ad1b8f26eb7bfda6d3eee2a70707dea73"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#aae8a9e344c29a49dac68392d5bb076be">Print</a> (int a, int b, int m, int n) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a sub-matrix on the standard output.  <a href="#aae8a9e344c29a49dac68392d5bb076be"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a9071a92d72aec94dc95549701a404db8">Print</a> (int l) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays a square sub-matrix on the standard output.  <a href="#a9071a92d72aec94dc95549701a404db8"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a9696365a66c66029580049b9f282a4c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a5415533e02cb7b5445e3005ce1f54bda">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a5415533e02cb7b5445e3005ce1f54bda"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#ac192355d2d110149854ca8bb9406b076"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#abc80799bd9b45574598b3350187715da">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#abc80799bd9b45574598b3350187715da"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#afd699e58126e4a75b734c72a7b07652a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a1fad405d3c4a66ef2f1aab8ad0d2c054">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a1fad405d3c4a66ef2f1aab8ad0d2c054"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#aa1064e91f58256717c26641aff9f8dbb"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___triangular.php#a16abbe61e55e1a6a1f0a173f32457eed">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a16abbe61e55e1a6a1f0a173f32457eed"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a65e9c3f0db9c7c8c3fa10ead777dd927"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a6b134070d1b890ba6b7eb72fee170984"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a7d27412bc9384a5ce0c0db1ee310d31c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements in the matrix.  <a href="#a0fbc3f7030583174eaa69429f655a1b0"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer to the data array.  <a href="#a453a269dfe7fadba249064363d5ab92a"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a const pointer to the data array.  <a href="#a0f2796430deb08e565df8ced656097bf"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "void*" to the data array.  <a href="#a505cdfee34741c463e0dc1943337bedc"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns a pointer of type "const void*" to the data array.  <a href="#a5979450cd8801810229f4a24c36e6639"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">Allocator &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the allocator of the matrix.  <a href="#af88748a55208349367d6860ad76fd691"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab946d91b3c0b9ca767af94fc58654c46"></a><!-- doxytag: member="Seldon::Matrix_Triangular::me_" ref="ab946d91b3c0b9ca767af94fc58654c46" args="" -->
pointer *&nbsp;</td><td class="memItemRight" valign="bottom"><b>me_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96b39ff494d4b7d3e30f320a1c7b4aba"></a><!-- doxytag: member="Seldon::Matrix_Triangular::m_" ref="a96b39ff494d4b7d3e30f320a1c7b4aba" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>m_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a11cb5d502050e5f14482ff0c9cef5a76"></a><!-- doxytag: member="Seldon::Matrix_Triangular::n_" ref="a11cb5d502050e5f14482ff0c9cef5a76" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><b>n_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acb12a8b74699fae7ae3b2b67376795a1"></a><!-- doxytag: member="Seldon::Matrix_Triangular::data_" ref="acb12a8b74699fae7ae3b2b67376795a1" args="" -->
pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>data_</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-static-attribs"></a>
Static Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a132fa01ce120f352d434fd920e5ad9b4"></a><!-- doxytag: member="Seldon::Matrix_Triangular::allocator_" ref="a132fa01ce120f352d434fd920e5ad9b4" args="" -->
static Allocator&nbsp;</td><td class="memItemRight" valign="bottom"><b>allocator_</b></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Triangular matrix stored in a full matrix. </p>

<p>Definition at line <a class="el" href="_matrix___triangular_8hxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a60a394e341c7e0425b5c51f89e93dd14"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Matrix_Triangular" ref="a60a394e341c7e0425b5c51f89e93dd14" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>On exit, the matrix is an empty 0x0 matrix. </p>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00037">37</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3ecbb30f1d5aee888ef396bbfde1f61b"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Matrix_Triangular" ref="a3ecbb30f1d5aee888ef396bbfde1f61b" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Main constructor. </p>
<p>Builds a i x j full matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>'j' is assumed to be equal to 'i' and is therefore discarded. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00052">52</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="a01cf3952c69288e68376db61cf322c77"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Clear" ref="a01cf3952c69288e68376db61cf322c77" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>Destructs the matrix. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>On exit, the matrix is an empty 0x0 matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00203">203</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a22a6a81034ad25df2123e33d1fb9052d"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Copy" ref="a22a6a81034ad25df2123e33d1fb9052d" args="(const Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Copy </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00684">684</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a12f60cdce0a9a1dc924cfab291af4178"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Fill" ref="a12f60cdce0a9a1dc924cfab291af4178" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with 0, 1, 2, ... </p>
<p>On exit, the matrix is filled with 0, 1, 2, 3, ... The order of those numbers depends on the storage. </p>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00728">728</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abddc5c5f65873a308e5f6919f5dc4023"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Fill" ref="abddc5c5f65873a308e5f6919f5dc4023" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Fill </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<p>On exit, the matrix is filled with 'x'. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00742">742</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad9f5e4d6ffc7dff60d8946e26102f71b"></a><!-- doxytag: member="Seldon::Matrix_Triangular::FillRand" ref="ad9f5e4d6ffc7dff60d8946e26102f71b" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::FillRand </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix randomly. </p>
<dl class="note"><dt><b>Note:</b></dt><dd>The random generator is very basic. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00770">770</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af88748a55208349367d6860ad76fd691"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetAllocator" ref="af88748a55208349367d6860ad76fd691" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">Allocator &amp; <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetAllocator </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the allocator of the matrix. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The allocator. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00257">257</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a453a269dfe7fadba249064363d5ab92a"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetData" ref="a453a269dfe7fadba249064363d5ab92a" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer to the data array. </p>
<p>Returns a pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00207">207</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0f2796430deb08e565df8ced656097bf"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetDataConst" ref="a0f2796430deb08e565df8ced656097bf" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___base.php">Matrix_Base</a>&lt; T, Allocator &gt;::const_pointer <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConst </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a const pointer to the data array. </p>
<p>Returns a const pointer to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00220">220</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5979450cd8801810229f4a24c36e6639"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetDataConstVoid" ref="a5979450cd8801810229f4a24c36e6639" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataConstVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "const void*" to the data array. </p>
<p>Returns a pointer of type "const void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A const pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00246">246</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a812d8809554264f3b8f64429231b4e39"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetDataSize" ref="a812d8809554264f3b8f64429231b4e39" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the number of rows multiplied by the number of columns because the matrix is full. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00224">224</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a505cdfee34741c463e0dc1943337bedc"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetDataVoid" ref="a505cdfee34741c463e0dc1943337bedc" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void * <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetDataVoid </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns a pointer of type "void*" to the data array. </p>
<p>Returns a pointer of type "void*" to data, i.e. the data array 'data_'. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>A pointer of type "void*" to the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00233">233</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a65e9c3f0db9c7c8c3fa10ead777dd927"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetM" ref="a65e9c3f0db9c7c8c3fa10ead777dd927" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a8c781e99310aae01786eac0e8e5aa50c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#ae7cfeea59b5f9181e6148e084a6efcf7">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00106">106</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab6f6c5bba065ca82dad7c3abdbc8ea79"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetM" ref="ab6f6c5bba065ca82dad7c3abdbc8ea79" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aecf3e8c636dbb83335ea588afe2558a8">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00129">129</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6b134070d1b890ba6b7eb72fee170984"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetN" ref="a6b134070d1b890ba6b7eb72fee170984" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a40580a92202044416e379c1304ff0220">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a498904993d7b65f5c522c21ca991475d">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00117">117</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a7d27412bc9384a5ce0c0db1ee310d31c"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetN" ref="a7d27412bc9384a5ce0c0db1ee310d31c" args="(const Seldon::SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">Seldon::SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a867066f7bf531bf7ad15bdcd034dc3c0">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00144">144</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0fbc3f7030583174eaa69429f655a1b0"></a><!-- doxytag: member="Seldon::Matrix_Triangular::GetSize" ref="a0fbc3f7030583174eaa69429f655a1b0" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base</a>&lt; T, Allocator &gt;::GetSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inherited]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements in the matrix. </p>
<p>Returns the number of elements in the matrix, i.e. the number of rows multiplied by the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements in the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">Seldon::HeterogeneousMatrixCollection&lt; General, RowMajor, General, RowSparse, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColMajor, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, ColSymPacked, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_collection.php#a897f68c33d448f7961680307ae4d75c5">Seldon::MatrixCollection&lt; T, Prop, RowSymPacked, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___base_8cxx_source.php#l00194">194</a> of file <a class="el" href="_matrix___base_8cxx_source.php">Matrix_Base.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5380ef67c34e203bc186a8ca351abbbb"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Nullify" ref="a5380ef67c34e203bc186a8ca351abbbb" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Memory is not released except for me_. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00435">435</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6cc7b33b27d2a8d51293614d176ebfc0"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator()" ref="a6cc7b33b27d2a8d51293614d176ebfc0" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00520">520</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5f07919d9f3d4d8ac483247e4d9d752f"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator()" ref="a5f07919d9f3d4d8ac483247e4d9d752f" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::value_type <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00479">479</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2f25eaae12f9e30ddad62426278dee62"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator=" ref="a2f25eaae12f9e30ddad62426278dee62" args="(const T0 &amp;x)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>x</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Fills the matrix with a given value. </p>
<p>On exit, the matrix is filled with 'x'. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>x</em>&nbsp;</td><td>the value to fill the matrix with. </td></tr>
  </table>
  </dd>
</dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#ac7f70aeb83a8db1f3b8e157f731d76f9">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#ac04f5c5815fbc6085da6b7f0c62a0281">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a>, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1c1975aa196d966318a70af7ccfa177b">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#ae36017d3afd1be1b01bfb6a63c86f4c9">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00757">757</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab379631da575bbb4b2404f1fc48c37a4"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator=" ref="ab379631da575bbb4b2404f1fc48c37a4" args="(const Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt; &amp;A)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop, class Storage, class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp; <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator= </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt; &amp;&nbsp;</td>
          <td class="paramname"> <em>A</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Duplicates a matrix (assignment operator). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>A</em>&nbsp;</td><td>matrix to be copied. </td></tr>
  </table>
  </dd>
</dl>
<dl class="note"><dt><b>Note:</b></dt><dd>Memory is duplicated: 'A' is therefore independent from the current instance after the copy. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00668">668</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab4f150251c1afd88a7a59a382ac4f623"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator[]" ref="ab4f150251c1afd88a7a59a382ac4f623" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00644">644</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a30661e664fd84702ea900809e4c7764c"></a><!-- doxytag: member="Seldon::Matrix_Triangular::operator[]" ref="a30661e664fd84702ea900809e4c7764c" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::operator[] </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access to elements of the data array. </p>
<p>Provides a direct access to the data array. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>i-th element of the data array. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00620">620</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9071a92d72aec94dc95549701a404db8"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Print" ref="a9071a92d72aec94dc95549701a404db8" args="(int l) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>l</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a square sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its bottom-right corner (l, l). So, elements with indices in [0, 0] x [l, l] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>l</em>&nbsp;</td><td>dimension of the square matrix to be displayed. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00831">831</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad1b8f26eb7bfda6d3eee2a70707dea73"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Print" ref="ad1b8f26eb7bfda6d3eee2a70707dea73" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00785">785</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aae8a9e344c29a49dac68392d5bb076be"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Print" ref="aae8a9e344c29a49dac68392d5bb076be" args="(int a, int b, int m, int n) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>a</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>b</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays a sub-matrix on the standard output. </p>
<p>The sub-matrix is defined by its upper-left corner (a, b) and its bottom-right corner (m, n). So, elements with indices in [a, m] x [b, n] are displayed on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>a</em>&nbsp;</td><td>row index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>b</em>&nbsp;</td><td>column index of the upper-left corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>m</em>&nbsp;</td><td>row index of the bottom-right corner. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>n</em>&nbsp;</td><td>column index of the bottom-right corner. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00810">810</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afd699e58126e4a75b734c72a7b07652a"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Read" ref="afd699e58126e4a75b734c72a7b07652a" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00989">989</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1fad405d3c4a66ef2f1aab8ad0d2c054"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Read" ref="a1fad405d3c4a66ef2f1aab8ad0d2c054" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. The number of rows (integer) and the number of columns (integer) are read, and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l01017">1017</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a16abbe61e55e1a6a1f0a173f32457eed"></a><!-- doxytag: member="Seldon::Matrix_Triangular::ReadText" ref="a16abbe61e55e1a6a1f0a173f32457eed" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in text format from an input stream. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l01078">1078</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aa1064e91f58256717c26641aff9f8dbb"></a><!-- doxytag: member="Seldon::Matrix_Triangular::ReadText" ref="aa1064e91f58256717c26641aff9f8dbb" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in text format in a file. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>input file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l01053">1053</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a472fa39bd3faee779cae713a5bd1aeb3"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Reallocate" ref="a472fa39bd3faee779cae713a5bd1aeb3" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x i matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Depending on your allocator, data may be lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00244">244</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a30b10bb11499845df9d647c3e0ef8915"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Resize" ref="a30b10bb11499845df9d647c3e0ef8915" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix and keeps previous entries. </p>
<p>On exit, the matrix is a i x i matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>The previous entries are kept, extra-entries may not be initialized (depending of the allocator). </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00342">342</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aca8b82d5764a1943b2ec9a36d502816f"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Val" ref="aca8b82d5764a1943b2ec9a36d502816f" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::const_reference <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>It returns the value stored i, memory. Even if the value is in the part of the matrix that should be filled with zeros, the value could be non-zero. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00564">564</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ab2e953becbaab6664f72752932e14f5c"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Val" ref="ab2e953becbaab6664f72752932e14f5c" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___triangular.php">Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::reference <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>
<dl class="note"><dt><b>Note:</b></dt><dd>It returns the value stored i, memory. Even if the value is in the part of the matrix that should be filled with zeros, the value could be non-zero. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00594">594</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5415533e02cb7b5445e3005ce1f54bda"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Write" ref="a5415533e02cb7b5445e3005ce1f54bda" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00880">880</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9696365a66c66029580049b9f282a4c5"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Write" ref="a9696365a66c66029580049b9f282a4c5" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written, and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00852">852</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac192355d2d110149854ca8bb9406b076"></a><!-- doxytag: member="Seldon::Matrix_Triangular::WriteText" ref="ac192355d2d110149854ca8bb9406b076" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00920">920</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abc80799bd9b45574598b3350187715da"></a><!-- doxytag: member="Seldon::Matrix_Triangular::WriteText" ref="abc80799bd9b45574598b3350187715da" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Writes the matrix to an output stream in text format. Only matrix elements are written (not dimensions). Each row is written on a single line and elements of a row are delimited by tabulations. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00950">950</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="af668e8676a93c1a99ebee4794994cc6e"></a><!-- doxytag: member="Seldon::Matrix_Triangular::Zero" ref="af668e8676a93c1a99ebee4794994cc6e" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___triangular.php">Seldon::Matrix_Triangular</a>&lt; T, Prop, Storage, Allocator &gt;::Zero </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Sets all elements to zero. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>It fills the memory with zeros. If the matrix stores complex structures, use 'Fill' instead. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___triangular_8cxx_source.php#l00703">703</a> of file <a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix/<a class="el" href="_matrix___triangular_8hxx_source.php">Matrix_Triangular.hxx</a></li>
<li>matrix/<a class="el" href="_matrix___triangular_8cxx_source.php">Matrix_Triangular.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
