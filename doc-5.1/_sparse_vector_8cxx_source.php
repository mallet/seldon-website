<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/SparseVector.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_SPARSE_VECTOR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;SparseVector.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aee3f9a5bfd70e6370ae0c559257624f7">00039</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Vector</a>()  throw():
<a name="l00040"></a>00040     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     index_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00050"></a>00050   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00051"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aa33cfa4d38f149671d6d0ea82f5106d8">00051</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Vector</a>(<span class="keywordtype">int</span> i):
<a name="l00052"></a>00052     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&gt;(i)
<a name="l00053"></a>00053   {
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00056"></a>00056 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00057"></a>00057       {
<a name="l00058"></a>00058 <span class="preprocessor">#endif</span>
<a name="l00059"></a>00059 <span class="preprocessor"></span>
<a name="l00060"></a>00060         this-&gt;index_ = index_allocator_.allocate(i, <span class="keyword">this</span>);
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00063"></a>00063 <span class="preprocessor"></span>      }
<a name="l00064"></a>00064     <span class="keywordflow">catch</span> (...)
<a name="l00065"></a>00065       {
<a name="l00066"></a>00066         this-&gt;m_ = 0;
<a name="l00067"></a>00067         this-&gt;index_ = NULL;
<a name="l00068"></a>00068         this-&gt;data_ = NULL;
<a name="l00069"></a>00069       }
<a name="l00070"></a>00070 
<a name="l00071"></a>00071     <span class="keywordflow">if</span> (this-&gt;index_ == NULL)
<a name="l00072"></a>00072       {
<a name="l00073"></a>00073         this-&gt;m_ = 0;
<a name="l00074"></a>00074         this-&gt;data_ = NULL;
<a name="l00075"></a>00075       }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00078"></a>00078       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Vector(int)&quot;</span>,
<a name="l00079"></a>00079                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a vector of size &quot;</span>)
<a name="l00080"></a>00080                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i * <span class="keyword">sizeof</span>(T)) + <span class="stringliteral">&quot; bytes (&quot;</span>
<a name="l00081"></a>00081                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00082"></a>00082 <span class="preprocessor">#endif</span>
<a name="l00083"></a>00083 <span class="preprocessor"></span>
<a name="l00084"></a>00084   }
<a name="l00085"></a>00085 
<a name="l00086"></a>00086 
<a name="l00088"></a>00088 
<a name="l00091"></a>00091   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00092"></a>00092   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::</a>
<a name="l00093"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a95b6b2c8c2be95a9d5dcdbd892f195e2">00093</a> <a class="code" href="class_seldon_1_1_vector.php">  Vector</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; V) :
<a name="l00094"></a>00094     <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;T, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator&gt;()
<a name="l00095"></a>00095   {
<a name="l00096"></a>00096     this-&gt;index_ = NULL;
<a name="l00097"></a>00097     Copy(V);
<a name="l00098"></a>00098   }
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 
<a name="l00101"></a>00101   <span class="comment">/**************</span>
<a name="l00102"></a>00102 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00103"></a>00103 <span class="comment">   **************/</span>
<a name="l00104"></a>00104 
<a name="l00105"></a>00105 
<a name="l00107"></a>00107   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00108"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af0eb42557b7578cfefabbf4d15ad2073">00108</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::~Vector</a>()
<a name="l00109"></a>00109   {
<a name="l00110"></a>00110     <span class="comment">// &#39;data_&#39; is released.</span>
<a name="l00111"></a>00111 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00112"></a>00112 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00113"></a>00113       {
<a name="l00114"></a>00114 <span class="preprocessor">#endif</span>
<a name="l00115"></a>00115 <span class="preprocessor"></span>        <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00116"></a>00116           {
<a name="l00117"></a>00117             this-&gt;vect_allocator_.deallocate(this-&gt;data_, this-&gt;m_);
<a name="l00118"></a>00118             this-&gt;data_ = NULL;
<a name="l00119"></a>00119           }
<a name="l00120"></a>00120 
<a name="l00121"></a>00121         <span class="keywordflow">if</span> (index_ != NULL)
<a name="l00122"></a>00122           {
<a name="l00123"></a>00123             index_allocator_.deallocate(index_, this-&gt;m_);
<a name="l00124"></a>00124             index_ = NULL;
<a name="l00125"></a>00125           }
<a name="l00126"></a>00126 
<a name="l00127"></a>00127         this-&gt;m_ = 0;
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00130"></a>00130 <span class="preprocessor"></span>      }
<a name="l00131"></a>00131     <span class="keywordflow">catch</span> (...)
<a name="l00132"></a>00132       {
<a name="l00133"></a>00133         this-&gt;data_ = NULL;
<a name="l00134"></a>00134         index_ = NULL;
<a name="l00135"></a>00135         this-&gt;m_ = 0;
<a name="l00136"></a>00136         <span class="keywordflow">return</span>;
<a name="l00137"></a>00137       }
<a name="l00138"></a>00138 <span class="preprocessor">#endif</span>
<a name="l00139"></a>00139 <span class="preprocessor"></span>
<a name="l00140"></a>00140   }
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 
<a name="l00143"></a>00143   <span class="comment">/*********************</span>
<a name="l00144"></a>00144 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00145"></a>00145 <span class="comment">   *********************/</span>
<a name="l00146"></a>00146 
<a name="l00147"></a>00147 
<a name="l00149"></a>00149 
<a name="l00153"></a>00153   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00154"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a548fa99edadcd9b418912d607b7aa19b">00154</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Clear</a>()
<a name="l00155"></a>00155   {
<a name="l00156"></a>00156     this-&gt;~<a class="code" href="class_seldon_1_1_vector.php">Vector</a>();
<a name="l00157"></a>00157   }
<a name="l00158"></a>00158 
<a name="l00159"></a>00159 
<a name="l00161"></a>00161 
<a name="l00167"></a>00167   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00168"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afcc8fef4210e7703493ad95acf389d19">00168</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Reallocate</a>(<span class="keywordtype">int</span> i)
<a name="l00169"></a>00169   {
<a name="l00170"></a>00170 
<a name="l00171"></a>00171     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00172"></a>00172       {
<a name="l00173"></a>00173 
<a name="l00174"></a>00174         this-&gt;m_ = i;
<a name="l00175"></a>00175 
<a name="l00176"></a>00176 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00177"></a>00177 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00178"></a>00178           {
<a name="l00179"></a>00179 <span class="preprocessor">#endif</span>
<a name="l00180"></a>00180 <span class="preprocessor"></span>
<a name="l00181"></a>00181             this-&gt;data_ =
<a name="l00182"></a>00182               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;vect_allocator_
<a name="l00183"></a>00183                                         .reallocate(this-&gt;data_, i, <span class="keyword">this</span>));
<a name="l00184"></a>00184 
<a name="l00185"></a>00185             index_
<a name="l00186"></a>00186               = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(this-&gt;index_allocator_
<a name="l00187"></a>00187                                        .reallocate(index_, i, <span class="keyword">this</span>));
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>          }
<a name="l00191"></a>00191         <span class="keywordflow">catch</span> (...)
<a name="l00192"></a>00192           {
<a name="l00193"></a>00193             this-&gt;m_ = 0;
<a name="l00194"></a>00194             this-&gt;data_ = NULL;
<a name="l00195"></a>00195             this-&gt;index_ = NULL;
<a name="l00196"></a>00196             <span class="keywordflow">return</span>;
<a name="l00197"></a>00197           }
<a name="l00198"></a>00198         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00199"></a>00199           {
<a name="l00200"></a>00200             this-&gt;m_ = 0;
<a name="l00201"></a>00201             this-&gt;index_ = NULL;
<a name="l00202"></a>00202             <span class="keywordflow">return</span>;
<a name="l00203"></a>00203           }
<a name="l00204"></a>00204 <span class="preprocessor">#endif</span>
<a name="l00205"></a>00205 <span class="preprocessor"></span>
<a name="l00206"></a>00206       }
<a name="l00207"></a>00207   }
<a name="l00208"></a>00208 
<a name="l00209"></a>00209 
<a name="l00211"></a>00211 
<a name="l00216"></a>00216   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00217"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3637911015d5a0fafa4811ee2204bbc5">00217</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Resize</a>(<span class="keywordtype">int</span> n)
<a name="l00218"></a>00218   {
<a name="l00219"></a>00219 
<a name="l00220"></a>00220     <span class="keywordflow">if</span> (n == this-&gt;m_)
<a name="l00221"></a>00221       <span class="keywordflow">return</span>;
<a name="l00222"></a>00222 
<a name="l00223"></a>00223     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> new_value(n);
<a name="l00224"></a>00224     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> new_index(n);
<a name="l00225"></a>00225     <span class="keywordtype">int</span> Nmin = min(this-&gt;m_, n);
<a name="l00226"></a>00226     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Nmin; i++)
<a name="l00227"></a>00227       {
<a name="l00228"></a>00228         new_value(i) = this-&gt;data_[i];
<a name="l00229"></a>00229         new_index(i) = index_[i];
<a name="l00230"></a>00230       }
<a name="l00231"></a>00231 
<a name="l00232"></a>00232     SetData(new_value, new_index);
<a name="l00233"></a>00233   }
<a name="l00234"></a>00234 
<a name="l00235"></a>00235 
<a name="l00252"></a>00252   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00253"></a>00253   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;</a>
<a name="l00254"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a73034fa2fb67eec70a2d25684f2a0c8c">00254</a> <a class="code" href="class_seldon_1_1_vector.php">  ::SetData</a>(<span class="keywordtype">int</span> i, T* data, <span class="keywordtype">int</span>* index)
<a name="l00255"></a>00255   {
<a name="l00256"></a>00256     this-&gt;Clear();
<a name="l00257"></a>00257 
<a name="l00258"></a>00258     this-&gt;m_ = i;
<a name="l00259"></a>00259 
<a name="l00260"></a>00260     this-&gt;data_ = data;
<a name="l00261"></a>00261     this-&gt;index_ = index;
<a name="l00262"></a>00262   }
<a name="l00263"></a>00263 
<a name="l00264"></a>00264 
<a name="l00276"></a>00276   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00277"></a>00277   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00278"></a>00278   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;</a>
<a name="l00279"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#af6c784522606f8bf451b129a8b5069d1">00279</a> <a class="code" href="class_seldon_1_1_vector.php">  ::SetData</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator2&gt;</a>&amp; data, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; index)
<a name="l00280"></a>00280   {
<a name="l00281"></a>00281 
<a name="l00282"></a>00282 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00283"></a>00283 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (data.GetM() != index.GetM())
<a name="l00284"></a>00284       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::SetData &quot;</span>,
<a name="l00285"></a>00285                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The data vector and the index vector should&quot;</span>)
<a name="l00286"></a>00286                      + <span class="stringliteral">&quot; have the same size.\n  Size of the data vector: &quot;</span>
<a name="l00287"></a>00287                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(data.GetM()) + <span class="stringliteral">&quot;\n  Size of index vector: &quot;</span>
<a name="l00288"></a>00288                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(index.GetM()));
<a name="l00289"></a>00289 <span class="preprocessor">#endif</span>
<a name="l00290"></a>00290 <span class="preprocessor"></span>
<a name="l00291"></a>00291     SetData(data.GetM(), data.GetData(), index.GetData());
<a name="l00292"></a>00292     data.Nullify();
<a name="l00293"></a>00293     index.Nullify();
<a name="l00294"></a>00294   }
<a name="l00295"></a>00295 
<a name="l00296"></a>00296 
<a name="l00308"></a>00308   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00309"></a>00309   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator2&gt;
<a name="l00310"></a>00310   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;</a>
<a name="l00311"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab4c128e288cce25e0e8f1998ccf38a1c">00311</a> <a class="code" href="class_seldon_1_1_vector.php">  ::SetData</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator2&gt;</a>&amp; V)
<a name="l00312"></a>00312   {
<a name="l00313"></a>00313     SetData(V.GetM(), V.GetData(), V.GetIndex());
<a name="l00314"></a>00314   }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 
<a name="l00318"></a>00318 
<a name="l00323"></a>00323   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00324"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a985afdfd20e655bb551cf44669acd0e6">00324</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Nullify</a>()
<a name="l00325"></a>00325   {
<a name="l00326"></a>00326     this-&gt;m_ = 0;
<a name="l00327"></a>00327     this-&gt;data_ = NULL;
<a name="l00328"></a>00328     this-&gt;index_ = NULL;
<a name="l00329"></a>00329   }
<a name="l00330"></a>00330 
<a name="l00331"></a>00331 
<a name="l00332"></a>00332   <span class="comment">/**********************************</span>
<a name="l00333"></a>00333 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00334"></a>00334 <span class="comment">   **********************************/</span>
<a name="l00335"></a>00335 
<a name="l00336"></a>00336 
<a name="l00338"></a>00338 
<a name="l00342"></a>00342   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00343"></a>00343   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::reference</a>
<a name="l00344"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07">00344</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Value</a>(<span class="keywordtype">int</span> i)
<a name="l00345"></a>00345   {
<a name="l00346"></a>00346 
<a name="l00347"></a>00347 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00348"></a>00348 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00349"></a>00349       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Value(int)&quot;</span>,
<a name="l00350"></a>00350                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00351"></a>00351                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00352"></a>00352 <span class="preprocessor">#endif</span>
<a name="l00353"></a>00353 <span class="preprocessor"></span>
<a name="l00354"></a>00354     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00355"></a>00355   }
<a name="l00356"></a>00356 
<a name="l00357"></a>00357 
<a name="l00359"></a>00359 
<a name="l00363"></a>00363   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00364"></a>00364   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::const_reference</a>
<a name="l00365"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ad733400e0cde2e94b5efd076dc5d956e">00365</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Value</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00366"></a>00366 <span class="keyword">  </span>{
<a name="l00367"></a>00367 
<a name="l00368"></a>00368 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00369"></a>00369 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00370"></a>00370       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Value(int)&quot;</span>,
<a name="l00371"></a>00371                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00372"></a>00372                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00373"></a>00373 <span class="preprocessor">#endif</span>
<a name="l00374"></a>00374 <span class="preprocessor"></span>
<a name="l00375"></a>00375     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00376"></a>00376   }
<a name="l00377"></a>00377 
<a name="l00378"></a>00378 
<a name="l00380"></a>00380 
<a name="l00384"></a>00384   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00385"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a72d964fb6cbd4e4189473e0298989929">00385</a>   <span class="keyword">inline</span> <span class="keywordtype">int</span>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i)
<a name="l00386"></a>00386   {
<a name="l00387"></a>00387 
<a name="l00388"></a>00388 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00389"></a>00389 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00390"></a>00390       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Index(int)&quot;</span>,
<a name="l00391"></a>00391                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00392"></a>00392                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00393"></a>00393 <span class="preprocessor">#endif</span>
<a name="l00394"></a>00394 <span class="preprocessor"></span>
<a name="l00395"></a>00395     <span class="keywordflow">return</span> this-&gt;index_[i];
<a name="l00396"></a>00396   }
<a name="l00397"></a>00397 
<a name="l00398"></a>00398 
<a name="l00400"></a>00400 
<a name="l00404"></a>00404   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00405"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f0337de5aaabd69cb32432377b5e4d3">00405</a>   <span class="keyword">inline</span> <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Index</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00406"></a>00406 <span class="keyword">  </span>{
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00409"></a>00409 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00410"></a>00410       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Index(int)&quot;</span>,
<a name="l00411"></a>00411                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00412"></a>00412                        + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00413"></a>00413 <span class="preprocessor">#endif</span>
<a name="l00414"></a>00414 <span class="preprocessor"></span>
<a name="l00415"></a>00415     <span class="keywordflow">return</span> this-&gt;index_[i];
<a name="l00416"></a>00416   }
<a name="l00417"></a>00417 
<a name="l00418"></a>00418 
<a name="l00420"></a>00420 
<a name="l00424"></a>00424   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00425"></a>00425   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::reference</a>
<a name="l00426"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afd992c6ba8e53a2c556459388fc42293">00426</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00427"></a>00427   {
<a name="l00428"></a>00428     <span class="keywordtype">int</span> k = 0;
<a name="l00429"></a>00429     <span class="comment">// Searching for the entry.</span>
<a name="l00430"></a>00430     <span class="keywordflow">while</span> (k &lt; this-&gt;m_ &amp;&amp; index_[k] &lt; i)
<a name="l00431"></a>00431       k++;
<a name="l00432"></a>00432 
<a name="l00433"></a>00433     <span class="keywordflow">if</span> (k &gt;= this-&gt;m_ || index_[k] != i)
<a name="l00434"></a>00434       <span class="comment">// The entry does not exist yet, so a zero entry is introduced.</span>
<a name="l00435"></a>00435       AddInteraction(i, T(0));
<a name="l00436"></a>00436 
<a name="l00437"></a>00437     <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00438"></a>00438   }
<a name="l00439"></a>00439 
<a name="l00440"></a>00440 
<a name="l00442"></a>00442 
<a name="l00446"></a>00446   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00447"></a>00447   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::value_type</a>
<a name="l00448"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a001a7d1629309ad90c6d66d695a69e8f">00448</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00449"></a>00449 <span class="keyword">  </span>{
<a name="l00450"></a>00450     <span class="keywordtype">int</span> k = 0;
<a name="l00451"></a>00451     <span class="comment">// Searching for the entry.</span>
<a name="l00452"></a>00452     <span class="keywordflow">while</span> (k &lt; this-&gt;m_ &amp;&amp; index_[k] &lt; i)
<a name="l00453"></a>00453       k++;
<a name="l00454"></a>00454 
<a name="l00455"></a>00455     <span class="keywordflow">if</span> (k &gt;= this-&gt;m_ || index_[k] != i)
<a name="l00456"></a>00456       <span class="comment">// The entry does not exist, a zero is returned.</span>
<a name="l00457"></a>00457       <span class="keywordflow">return</span> T(0);
<a name="l00458"></a>00458 
<a name="l00459"></a>00459     <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00460"></a>00460   }
<a name="l00461"></a>00461 
<a name="l00462"></a>00462 
<a name="l00464"></a>00464 
<a name="l00468"></a>00468   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00469"></a>00469   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::reference</a>
<a name="l00470"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a5805ff818cc9c57a65bc6006c58564a7">00470</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i)
<a name="l00471"></a>00471   {
<a name="l00472"></a>00472     <span class="keywordflow">return</span> (*<span class="keyword">this</span>)(i);
<a name="l00473"></a>00473   }
<a name="l00474"></a>00474 
<a name="l00475"></a>00475 
<a name="l00477"></a>00477 
<a name="l00483"></a>00483   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00484"></a>00484   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::const_reference</a>
<a name="l00485"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a21f192e6ea09ce5be473e6bb1135c7a9">00485</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00486"></a>00486 <span class="keyword">  </span>{
<a name="l00487"></a>00487     <span class="keywordtype">int</span> k = 0;
<a name="l00488"></a>00488     <span class="comment">// Searching for the entry.</span>
<a name="l00489"></a>00489     <span class="keywordflow">while</span> (k &lt; this-&gt;m_ &amp;&amp; index_[k] &lt; i)
<a name="l00490"></a>00490       k++;
<a name="l00491"></a>00491 
<a name="l00492"></a>00492     <span class="keywordflow">if</span> (k &gt;= this-&gt;m_ || index_[k] != i)
<a name="l00493"></a>00493       <span class="comment">// The entry does not exist, no reference can be returned.</span>
<a name="l00494"></a>00494       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Val(int)&quot;</span>,
<a name="l00495"></a>00495                           <span class="stringliteral">&quot;No reference to element &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00496"></a>00496                           + <span class="stringliteral">&quot; can be returned: it is a zero entry.&quot;</span>);
<a name="l00497"></a>00497 
<a name="l00498"></a>00498     <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00499"></a>00499   }
<a name="l00500"></a>00500 
<a name="l00501"></a>00501 
<a name="l00503"></a>00503 
<a name="l00508"></a>00508   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00509"></a>00509   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;</a>
<a name="l00510"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a87ef82c6607c68b77a5ec18dbcb15eb0">00510</a> <a class="code" href="class_seldon_1_1_vector.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; X)
<a name="l00511"></a>00511   {
<a name="l00512"></a>00512     this-&gt;Copy(X);
<a name="l00513"></a>00513 
<a name="l00514"></a>00514     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00515"></a>00515   }
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00519"></a>00519 
<a name="l00524"></a>00524   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00525"></a>00525   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;</a>
<a name="l00526"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#aefe82d8a7c33e932c20c9fd9e6f499f5">00526</a> <a class="code" href="class_seldon_1_1_vector.php">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; X)
<a name="l00527"></a>00527   {
<a name="l00528"></a>00528     this-&gt;Reallocate(X.<a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">GetLength</a>());
<a name="l00529"></a>00529 
<a name="l00530"></a>00530     this-&gt;vect_allocator_.memorycpy(this-&gt;data_, X.<a class="code" href="class_seldon_1_1_vector___base.php#a4128ad4898e42211d22a7531c9f5f80a" title="Returns a pointer to data_ (stored data).">GetData</a>(), this-&gt;m_);
<a name="l00531"></a>00531     this-&gt;index_allocator_.memorycpy(this-&gt;index_, X.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6a4ed85be713e5a300f94eca786a2c2b" title="Returns a pointer to the array containing the indices of the non-zero entries.">GetIndex</a>(), this-&gt;m_);
<a name="l00532"></a>00532   }
<a name="l00533"></a>00533 
<a name="l00534"></a>00534 
<a name="l00535"></a>00535   <span class="comment">/*******************</span>
<a name="l00536"></a>00536 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00537"></a>00537 <span class="comment">   *******************/</span>
<a name="l00538"></a>00538 
<a name="l00539"></a>00539 
<a name="l00545"></a>00545   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00546"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6a4ed85be713e5a300f94eca786a2c2b">00546</a>   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::GetIndex</a>()<span class="keyword"> const</span>
<a name="l00547"></a>00547 <span class="keyword">  </span>{
<a name="l00548"></a>00548     <span class="keywordflow">return</span> this-&gt;index_;
<a name="l00549"></a>00549   }
<a name="l00550"></a>00550 
<a name="l00551"></a>00551 
<a name="l00552"></a>00552   <span class="comment">/************************</span>
<a name="l00553"></a>00553 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00554"></a>00554 <span class="comment">   ************************/</span>
<a name="l00555"></a>00555 
<a name="l00556"></a>00556 
<a name="l00558"></a>00558 
<a name="l00561"></a>00561   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00562"></a>00562   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00563"></a>00563   <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp;
<a name="l00564"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1d0196721350369a3154b60dfa105b37">00564</a>   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00565"></a>00565   {
<a name="l00566"></a>00566     this-&gt;Fill(x);
<a name="l00567"></a>00567 
<a name="l00568"></a>00568     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00569"></a>00569   }
<a name="l00570"></a>00570 
<a name="l00571"></a>00571 
<a name="l00573"></a>00573   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00574"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a8c5f87f1d06974d31d8ebe8dcc0fcb8e">00574</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00575"></a>00575 <span class="keyword">  </span>{
<a name="l00576"></a>00576     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetLength(); i++)
<a name="l00577"></a>00577       cout &lt;&lt; (Index(i) + 1) &lt;&lt; <span class="charliteral">&#39; &#39;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00578"></a>00578   }
<a name="l00579"></a>00579 
<a name="l00580"></a>00580 
<a name="l00582"></a>00582 
<a name="l00586"></a>00586   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00587"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3f4b6ec8424c938251af5e4b1b130702">00587</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Vector&lt;T, VectSparse, Allocator&gt;::Assemble</a>()
<a name="l00588"></a>00588   {
<a name="l00589"></a>00589     <span class="keywordtype">int</span> new_size = this-&gt;m_;
<a name="l00590"></a>00590     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values(new_size);
<a name="l00591"></a>00591     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> index(new_size);
<a name="l00592"></a>00592     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; new_size; i++)
<a name="l00593"></a>00593       {
<a name="l00594"></a>00594         values(i) = this-&gt;data_[i];
<a name="l00595"></a>00595         index(i) = index_[i];
<a name="l00596"></a>00596       }
<a name="l00597"></a>00597 
<a name="l00598"></a>00598     <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Seldon::Assemble</a>(new_size, index, values);
<a name="l00599"></a>00599     index.Resize(new_size);
<a name="l00600"></a>00600     values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(new_size);
<a name="l00601"></a>00601     SetData(values, index);
<a name="l00602"></a>00602   }
<a name="l00603"></a>00603 
<a name="l00604"></a>00604 
<a name="l00606"></a>00606 
<a name="l00610"></a>00610   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l00611"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a6ff2b64975624f059a305a7661c24573">00611</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::RemoveSmallEntry</a>(<span class="keyword">const</span> T0&amp; epsilon)
<a name="l00612"></a>00612   {
<a name="l00613"></a>00613     <span class="keywordtype">int</span> new_size = this-&gt;m_;
<a name="l00614"></a>00614     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values(new_size);
<a name="l00615"></a>00615     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> index(new_size);
<a name="l00616"></a>00616     new_size = 0;
<a name="l00617"></a>00617     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00618"></a>00618       <span class="keywordflow">if</span> (abs(this-&gt;data_[i]) &gt; epsilon)
<a name="l00619"></a>00619         {
<a name="l00620"></a>00620           values(new_size) = this-&gt;data_[i];
<a name="l00621"></a>00621           index(new_size) = index_[i];
<a name="l00622"></a>00622           new_size++;
<a name="l00623"></a>00623         }
<a name="l00624"></a>00624 
<a name="l00625"></a>00625     index.Resize(new_size);
<a name="l00626"></a>00626     values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(new_size);
<a name="l00627"></a>00627     SetData(values, index);
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00632"></a>00632 
<a name="l00637"></a>00637   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00638"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a34201f29286c8829e7e8a7e31a266d5b">00638</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> T&amp; val)
<a name="l00639"></a>00639   {
<a name="l00640"></a>00640     <span class="comment">// Searching for the position where the entry may be.</span>
<a name="l00641"></a>00641     <span class="keywordtype">int</span> pos = 0;
<a name="l00642"></a>00642     <span class="keywordflow">while</span> (pos &lt; this-&gt;m_ &amp;&amp; index_[pos] &lt; i)
<a name="l00643"></a>00643       pos++;
<a name="l00644"></a>00644 
<a name="l00645"></a>00645     <span class="comment">// If the entry already exists, adds &#39;val&#39;.</span>
<a name="l00646"></a>00646     <span class="keywordflow">if</span> (pos &lt; this-&gt;m_ &amp;&amp; index_[pos] == i)
<a name="l00647"></a>00647       {
<a name="l00648"></a>00648         this-&gt;data_[pos] += val;
<a name="l00649"></a>00649         <span class="keywordflow">return</span>;
<a name="l00650"></a>00650       }
<a name="l00651"></a>00651 
<a name="l00652"></a>00652     <span class="keywordtype">int</span> k;
<a name="l00653"></a>00653 
<a name="l00654"></a>00654     <span class="comment">// If the entry does not exist, the vector is reallocated.</span>
<a name="l00655"></a>00655     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> new_val(this-&gt;m_ + 1);
<a name="l00656"></a>00656     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> new_ind(this-&gt;m_ + 1);
<a name="l00657"></a>00657     <span class="keywordflow">for</span> (k = 0; k &lt; pos; k++)
<a name="l00658"></a>00658       {
<a name="l00659"></a>00659         new_ind(k) = index_[k];
<a name="l00660"></a>00660         new_val(k) = this-&gt;data_[k];
<a name="l00661"></a>00661       }
<a name="l00662"></a>00662 
<a name="l00663"></a>00663     <span class="comment">// The new entry.</span>
<a name="l00664"></a>00664     new_ind(pos) = i;
<a name="l00665"></a>00665     new_val(pos) = val;
<a name="l00666"></a>00666 
<a name="l00667"></a>00667     <span class="comment">// Other values in the vector.</span>
<a name="l00668"></a>00668     <span class="keywordflow">for</span> (k = pos + 1; k &lt;= this-&gt;m_; k++)
<a name="l00669"></a>00669       {
<a name="l00670"></a>00670         new_ind(k) = index_[k - 1];
<a name="l00671"></a>00671         new_val(k) = this-&gt;data_[k - 1];
<a name="l00672"></a>00672       }
<a name="l00673"></a>00673 
<a name="l00674"></a>00674     SetData(new_val, new_ind);
<a name="l00675"></a>00675   }
<a name="l00676"></a>00676 
<a name="l00677"></a>00677 
<a name="l00679"></a>00679 
<a name="l00688"></a>00688   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt; <span class="keyword">inline</span>
<a name="l00689"></a>00689   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::</a>
<a name="l00690"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a1a60608da32cc0da4be4a1acdbbebc06">00690</a> <a class="code" href="class_seldon_1_1_vector.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> n, <span class="keywordtype">int</span>* index, T* value, <span class="keywordtype">bool</span> already_sorted)
<a name="l00691"></a>00691   {
<a name="l00692"></a>00692     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> ind;
<a name="l00693"></a>00693     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> val;
<a name="l00694"></a>00694     ind.SetData(n, index);
<a name="l00695"></a>00695     val.SetData(n, value);
<a name="l00696"></a>00696     AddInteractionRow(n, ind, val, already_sorted);
<a name="l00697"></a>00697     ind.Nullify();
<a name="l00698"></a>00698     val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l00699"></a>00699   }
<a name="l00700"></a>00700 
<a name="l00701"></a>00701 
<a name="l00703"></a>00703 
<a name="l00712"></a>00712   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00713"></a>00713   <span class="keyword">template</span>&lt;<span class="keyword">class</span> Allocator0&gt;
<a name="l00714"></a>00714   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::</a>
<a name="l00715"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4625cf1134f2121385cbee020c7fe834">00715</a> <a class="code" href="class_seldon_1_1_vector.php">  AddInteractionRow</a>(<span class="keywordtype">int</span> n, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> index,
<a name="l00716"></a>00716                     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectFull, Allocator0&gt;</a> value,
<a name="l00717"></a>00717                     <span class="keywordtype">bool</span> already_sorted)
<a name="l00718"></a>00718   {
<a name="l00719"></a>00719     <span class="keywordflow">if</span> (!already_sorted)
<a name="l00720"></a>00720       <span class="comment">// Sorts the values to be added according to their indices.</span>
<a name="l00721"></a>00721       <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Seldon::Assemble</a>(n, index, value);
<a name="l00722"></a>00722 
<a name="l00723"></a>00723     <span class="comment">/***  Values that already have an entry ***/</span>
<a name="l00724"></a>00724 
<a name="l00725"></a>00725     <span class="comment">// Number of values to be added without entry.</span>
<a name="l00726"></a>00726     <span class="keywordtype">int</span> Nnew = 0;
<a name="l00727"></a>00727     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;bool&gt;</a> new_index(n);
<a name="l00728"></a>00728     new_index.Fill(<span class="keyword">true</span>);
<a name="l00729"></a>00729     <span class="keywordtype">int</span> k = 0;
<a name="l00730"></a>00730     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00731"></a>00731       {
<a name="l00732"></a>00732         <span class="keywordflow">while</span> (k &lt; this-&gt;m_ &amp;&amp; index_[k] &lt; index(j))
<a name="l00733"></a>00733           k++;
<a name="l00734"></a>00734 
<a name="l00735"></a>00735         <span class="keywordflow">if</span> (k &lt; this-&gt;m_ &amp;&amp; index(j) == index_[k])
<a name="l00736"></a>00736           {
<a name="l00737"></a>00737             new_index(j) = <span class="keyword">false</span>;
<a name="l00738"></a>00738             this-&gt;data_[k] += value(j);
<a name="l00739"></a>00739           }
<a name="l00740"></a>00740         <span class="keywordflow">else</span>
<a name="l00741"></a>00741           Nnew++;
<a name="l00742"></a>00742       }
<a name="l00743"></a>00743 
<a name="l00744"></a>00744     <span class="keywordflow">if</span> (Nnew &gt; 0)
<a name="l00745"></a>00745       {
<a name="l00746"></a>00746         <span class="comment">// Some values to be added have no entry yet.</span>
<a name="l00747"></a>00747         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> new_val(this-&gt;m_ + Nnew);
<a name="l00748"></a>00748         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> new_ind(this-&gt;m_ + Nnew);
<a name="l00749"></a>00749         <span class="keywordtype">int</span> nb = 0;
<a name="l00750"></a>00750         k = 0;
<a name="l00751"></a>00751         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00752"></a>00752           <span class="keywordflow">if</span> (new_index(j))
<a name="l00753"></a>00753             {
<a name="l00754"></a>00754               <span class="keywordflow">while</span> (k &lt; this-&gt;m_ &amp;&amp; index_[k] &lt; index(j))
<a name="l00755"></a>00755                 {
<a name="l00756"></a>00756                   new_ind(nb) = index_[k];
<a name="l00757"></a>00757                   new_val(nb) = this-&gt;data_[k];
<a name="l00758"></a>00758                   k++;
<a name="l00759"></a>00759                   nb++;
<a name="l00760"></a>00760                 }
<a name="l00761"></a>00761 
<a name="l00762"></a>00762               <span class="comment">// The new entry.</span>
<a name="l00763"></a>00763               new_ind(nb) = index(j);
<a name="l00764"></a>00764               new_val(nb) = value(j);
<a name="l00765"></a>00765               nb++;
<a name="l00766"></a>00766             }
<a name="l00767"></a>00767 
<a name="l00768"></a>00768         <span class="comment">// Last entries.</span>
<a name="l00769"></a>00769         <span class="keywordflow">while</span> (k &lt; this-&gt;m_)
<a name="l00770"></a>00770           {
<a name="l00771"></a>00771             new_ind(nb) = index_[k];
<a name="l00772"></a>00772             new_val(nb) = this-&gt;data_[k];
<a name="l00773"></a>00773             k++;
<a name="l00774"></a>00774             nb++;
<a name="l00775"></a>00775           }
<a name="l00776"></a>00776 
<a name="l00777"></a>00777         SetData(new_val, new_ind);
<a name="l00778"></a>00778       }
<a name="l00779"></a>00779   }
<a name="l00780"></a>00780 
<a name="l00781"></a>00781 
<a name="l00782"></a>00782   <span class="comment">/**************************</span>
<a name="l00783"></a>00783 <span class="comment">   * OUTPUT/INPUT FUNCTIONS *</span>
<a name="l00784"></a>00784 <span class="comment">   **************************/</span>
<a name="l00785"></a>00785 
<a name="l00786"></a>00786 
<a name="l00788"></a>00788 
<a name="l00793"></a>00793   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00794"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a246553d78195c2e1c51be7a4e84409c8">00794</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00795"></a>00795 <span class="keyword">  </span>{
<a name="l00796"></a>00796     ofstream FileStream;
<a name="l00797"></a>00797     FileStream.open(FileName.c_str());
<a name="l00798"></a>00798 
<a name="l00799"></a>00799 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00800"></a>00800 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00801"></a>00801     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00802"></a>00802       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Write(string FileName)&quot;</span>,
<a name="l00803"></a>00803                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00804"></a>00804 <span class="preprocessor">#endif</span>
<a name="l00805"></a>00805 <span class="preprocessor"></span>
<a name="l00806"></a>00806     this-&gt;Write(FileStream);
<a name="l00807"></a>00807 
<a name="l00808"></a>00808     FileStream.close();
<a name="l00809"></a>00809   }
<a name="l00810"></a>00810 
<a name="l00811"></a>00811 
<a name="l00813"></a>00813 
<a name="l00818"></a>00818   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00819"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a27f19795d0cf6489ab076a6c43ef6756">00819</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Write</a>(ostream&amp; stream)<span class="keyword"> const</span>
<a name="l00820"></a>00820 <span class="keyword">  </span>{
<a name="l00821"></a>00821 
<a name="l00822"></a>00822 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00823"></a>00823 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00824"></a>00824     <span class="keywordflow">if</span> (!stream.good())
<a name="l00825"></a>00825       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Write(ostream&amp; stream)&quot;</span>,
<a name="l00826"></a>00826                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00827"></a>00827 <span class="preprocessor">#endif</span>
<a name="l00828"></a>00828 <span class="preprocessor"></span>
<a name="l00829"></a>00829     stream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00830"></a>00830                  <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00831"></a>00831 
<a name="l00832"></a>00832     stream.write(reinterpret_cast&lt;char*&gt;(this-&gt;index_),
<a name="l00833"></a>00833                  this-&gt;m_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00834"></a>00834 
<a name="l00835"></a>00835     stream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00836"></a>00836                  this-&gt;m_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00837"></a>00837 
<a name="l00838"></a>00838 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00839"></a>00839 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00840"></a>00840     <span class="keywordflow">if</span> (!stream.good())
<a name="l00841"></a>00841       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Write(ostream&amp; stream)&quot;</span>,
<a name="l00842"></a>00842                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00843"></a>00843 <span class="preprocessor">#endif</span>
<a name="l00844"></a>00844 <span class="preprocessor"></span>
<a name="l00845"></a>00845   }
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 
<a name="l00849"></a>00849 
<a name="l00854"></a>00854   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00855"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#ab770c6961b3c24de5679fc93530e6e80">00855</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00856"></a>00856 <span class="keyword">  </span>{
<a name="l00857"></a>00857     ofstream FileStream;
<a name="l00858"></a>00858     FileStream.precision(cout.precision());
<a name="l00859"></a>00859     FileStream.flags(cout.flags());
<a name="l00860"></a>00860     FileStream.open(FileName.c_str());
<a name="l00861"></a>00861 
<a name="l00862"></a>00862 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00863"></a>00863 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00864"></a>00864     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00865"></a>00865       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::WriteText(string FileName)&quot;</span>,
<a name="l00866"></a>00866                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00867"></a>00867 <span class="preprocessor">#endif</span>
<a name="l00868"></a>00868 <span class="preprocessor"></span>
<a name="l00869"></a>00869     this-&gt;WriteText(FileStream);
<a name="l00870"></a>00870 
<a name="l00871"></a>00871     FileStream.close();
<a name="l00872"></a>00872   }
<a name="l00873"></a>00873 
<a name="l00874"></a>00874 
<a name="l00876"></a>00876 
<a name="l00881"></a>00881   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00882"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a71b2e3e54ed55920f6b34912382fac80">00882</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::WriteText</a>(ostream&amp; stream)<span class="keyword"> const</span>
<a name="l00883"></a>00883 <span class="keyword">  </span>{
<a name="l00884"></a>00884 
<a name="l00885"></a>00885 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00886"></a>00886 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00887"></a>00887     <span class="keywordflow">if</span> (!stream.good())
<a name="l00888"></a>00888       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::WriteText(ostream&amp; stream)&quot;</span>,
<a name="l00889"></a>00889                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00890"></a>00890 <span class="preprocessor">#endif</span>
<a name="l00891"></a>00891 <span class="preprocessor"></span>
<a name="l00892"></a>00892     <span class="comment">// First entries.</span>
<a name="l00893"></a>00893     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_ - 1; i++)
<a name="l00894"></a>00894       stream &lt;&lt; (Index(i) + 1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l00895"></a>00895 
<a name="l00896"></a>00896     <span class="comment">// Last entry is a special case: there should be no empty line at the end</span>
<a name="l00897"></a>00897     <span class="comment">// of the stream.</span>
<a name="l00898"></a>00898     <span class="keywordflow">if</span> (this-&gt;m_ &gt; 0)
<a name="l00899"></a>00899       stream &lt;&lt; (Index(this-&gt;m_ - 1) + 1) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(this-&gt;m_ - 1);
<a name="l00900"></a>00900 
<a name="l00901"></a>00901 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00902"></a>00902 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00903"></a>00903     <span class="keywordflow">if</span> (!stream.good())
<a name="l00904"></a>00904       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::WriteText(ostream&amp; stream)&quot;</span>,
<a name="l00905"></a>00905                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00906"></a>00906 <span class="preprocessor">#endif</span>
<a name="l00907"></a>00907 <span class="preprocessor"></span>
<a name="l00908"></a>00908   }
<a name="l00909"></a>00909 
<a name="l00910"></a>00910 
<a name="l00912"></a>00912 
<a name="l00916"></a>00916   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00917"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#adc80b24a206e7ef91dd9b51aa33763d0">00917</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00918"></a>00918   {
<a name="l00919"></a>00919     ifstream FileStream;
<a name="l00920"></a>00920     FileStream.open(FileName.c_str());
<a name="l00921"></a>00921 
<a name="l00922"></a>00922 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00923"></a>00923 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00924"></a>00924     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00925"></a>00925       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Read(string FileName)&quot;</span>,
<a name="l00926"></a>00926                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00927"></a>00927 <span class="preprocessor">#endif</span>
<a name="l00928"></a>00928 <span class="preprocessor"></span>
<a name="l00929"></a>00929     this-&gt;Read(FileStream);
<a name="l00930"></a>00930 
<a name="l00931"></a>00931     FileStream.close();
<a name="l00932"></a>00932   }
<a name="l00933"></a>00933 
<a name="l00934"></a>00934 
<a name="l00936"></a>00936 
<a name="l00940"></a>00940   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00941"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a4335b7afce7863794f8ffbf76db91cbd">00941</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::Read</a>(istream&amp; stream)
<a name="l00942"></a>00942   {
<a name="l00943"></a>00943 
<a name="l00944"></a>00944 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00945"></a>00945 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00946"></a>00946     <span class="keywordflow">if</span> (!stream.good())
<a name="l00947"></a>00947       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Read(istream&amp; stream)&quot;</span>,
<a name="l00948"></a>00948                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00949"></a>00949 <span class="preprocessor">#endif</span>
<a name="l00950"></a>00950 <span class="preprocessor"></span>
<a name="l00951"></a>00951     <span class="keywordtype">int</span> m;
<a name="l00952"></a>00952     stream.read(reinterpret_cast&lt;char*&gt;(&amp;m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00953"></a>00953     this-&gt;Reallocate(m);
<a name="l00954"></a>00954 
<a name="l00955"></a>00955     stream.read(reinterpret_cast&lt;char*&gt;(this-&gt;index_), m * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00956"></a>00956 
<a name="l00957"></a>00957     stream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00958"></a>00958                 m * <span class="keyword">sizeof</span>(value_type));
<a name="l00959"></a>00959 
<a name="l00960"></a>00960 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00961"></a>00961 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l00962"></a>00962     <span class="keywordflow">if</span> (!stream.good())
<a name="l00963"></a>00963       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::Read(istream&amp; stream)&quot;</span>,
<a name="l00964"></a>00964                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00965"></a>00965 <span class="preprocessor">#endif</span>
<a name="l00966"></a>00966 <span class="preprocessor"></span>
<a name="l00967"></a>00967   }
<a name="l00968"></a>00968 
<a name="l00969"></a>00969 
<a name="l00971"></a>00971 
<a name="l00975"></a>00975   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00976"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a3b9e7d2529315dfaa75a09d3c34da18a">00976</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l00977"></a>00977   {
<a name="l00978"></a>00978     ifstream FileStream;
<a name="l00979"></a>00979     FileStream.open(FileName.c_str());
<a name="l00980"></a>00980 
<a name="l00981"></a>00981 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00982"></a>00982 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00983"></a>00983     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00984"></a>00984       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::ReadText(string FileName)&quot;</span>,
<a name="l00985"></a>00985                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00986"></a>00986 <span class="preprocessor">#endif</span>
<a name="l00987"></a>00987 <span class="preprocessor"></span>
<a name="l00988"></a>00988     this-&gt;ReadText(FileStream);
<a name="l00989"></a>00989 
<a name="l00990"></a>00990     FileStream.close();
<a name="l00991"></a>00991   }
<a name="l00992"></a>00992 
<a name="l00993"></a>00993 
<a name="l00995"></a>00995 
<a name="l00999"></a>00999   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01000"></a><a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a554c1d3659fc50f3e119ef14437ad54a">01000</a>   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, VectSparse, Allocator&gt;::ReadText</a>(istream&amp; stream)
<a name="l01001"></a>01001   {
<a name="l01002"></a>01002     Clear();
<a name="l01003"></a>01003 
<a name="l01004"></a>01004 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01005"></a>01005 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01006"></a>01006     <span class="keywordflow">if</span> (!stream.good())
<a name="l01007"></a>01007       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::ReadText(istream&amp; stream)&quot;</span>,
<a name="l01008"></a>01008                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01009"></a>01009 <span class="preprocessor">#endif</span>
<a name="l01010"></a>01010 <span class="preprocessor"></span>
<a name="l01011"></a>01011     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values;
<a name="l01012"></a>01012     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> index;
<a name="l01013"></a>01013     T entry;
<a name="l01014"></a>01014     <span class="keywordtype">int</span> ind = 0;
<a name="l01015"></a>01015     <span class="keywordtype">int</span> nb_elt = 0;
<a name="l01016"></a>01016     <span class="keywordflow">while</span> (!stream.eof())
<a name="l01017"></a>01017       {
<a name="l01018"></a>01018         <span class="comment">// New entry is read.</span>
<a name="l01019"></a>01019         stream &gt;&gt; ind &gt;&gt; entry;
<a name="l01020"></a>01020 
<a name="l01021"></a>01021         <span class="keywordflow">if</span> (stream.fail())
<a name="l01022"></a>01022           <span class="keywordflow">break</span>;
<a name="l01023"></a>01023         <span class="keywordflow">else</span>
<a name="l01024"></a>01024           {
<a name="l01025"></a>01025 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01026"></a>01026 <span class="preprocessor"></span>            <span class="keywordflow">if</span> (ind &lt; 1)
<a name="l01027"></a>01027               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Vector&lt;VectSparse&gt;::ReadText&quot;</span>) +
<a name="l01028"></a>01028                             <span class="stringliteral">&quot;(istream&amp; stream)&quot;</span>,
<a name="l01029"></a>01029                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be greater &quot;</span>)
<a name="l01030"></a>01030                             + <span class="stringliteral">&quot;than 0 but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01031"></a>01031 <span class="preprocessor">#endif</span>
<a name="l01032"></a>01032 <span class="preprocessor"></span>
<a name="l01033"></a>01033             nb_elt++;
<a name="l01034"></a>01034             ind--;
<a name="l01035"></a>01035 
<a name="l01036"></a>01036             <span class="comment">// Inserting a new element.</span>
<a name="l01037"></a>01037             <span class="keywordflow">if</span> (nb_elt &gt; values.<a class="code" href="class_seldon_1_1_vector___base.php#a51f50515f0c6d0663465c2cc7de71bae" title="Returns the number of elements.">GetM</a>())
<a name="l01038"></a>01038               {
<a name="l01039"></a>01039                 values.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a99dda1b202ce562645f890897c593515" title="Changes the length of the vector, and keeps previous values.">Resize</a>(2 * nb_elt);
<a name="l01040"></a>01040                 index.Resize(2 * nb_elt);
<a name="l01041"></a>01041               }
<a name="l01042"></a>01042 
<a name="l01043"></a>01043             values(nb_elt - 1) = entry;
<a name="l01044"></a>01044             index(nb_elt - 1) = ind;
<a name="l01045"></a>01045           }
<a name="l01046"></a>01046       }
<a name="l01047"></a>01047 
<a name="l01048"></a>01048     <span class="keywordflow">if</span> (nb_elt &gt; 0)
<a name="l01049"></a>01049       {
<a name="l01050"></a>01050         <span class="comment">// Allocating to the right size.</span>
<a name="l01051"></a>01051         this-&gt;Reallocate(nb_elt);
<a name="l01052"></a>01052         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nb_elt; i++)
<a name="l01053"></a>01053           {
<a name="l01054"></a>01054             Index(i) = index(i);
<a name="l01055"></a>01055             Value(i) = values(i);
<a name="l01056"></a>01056           }
<a name="l01057"></a>01057       }
<a name="l01058"></a>01058   }
<a name="l01059"></a>01059 
<a name="l01060"></a>01060 
<a name="l01062"></a>01062 
<a name="l01067"></a>01067   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01068"></a>01068   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l01069"></a>01069                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php" title="Sparse vector class.">Vector&lt;T, VectSparse, Allocator&gt;</a>&amp; V)
<a name="l01070"></a>01070   {
<a name="l01071"></a>01071     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; V.<a class="code" href="class_seldon_1_1_vector___base.php#a075daae3607a4767a77a3de60ab30ba7" title="Returns the number of elements.">GetLength</a>(); i++)
<a name="l01072"></a>01072       out &lt;&lt; (V.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a72d964fb6cbd4e4189473e0298989929" title="Access operator.">Index</a>(i) + 1) &lt;&lt; <span class="charliteral">&#39; &#39;</span> &lt;&lt; V.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a51c6d9be9bfc1234e17d997f9bcdff07" title="Access operator.">Value</a>(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01073"></a>01073 
<a name="l01074"></a>01074     <span class="keywordflow">return</span> out;
<a name="l01075"></a>01075   }
<a name="l01076"></a>01076 
<a name="l01077"></a>01077 
<a name="l01078"></a>01078 } <span class="comment">// namespace Seldon.</span>
<a name="l01079"></a>01079 
<a name="l01080"></a>01080 <span class="preprocessor">#define SELDON_FILE_SPARSE_VECTOR_CXX</span>
<a name="l01081"></a>01081 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
