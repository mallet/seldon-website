<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/solver/iterative/QCgs.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_ITERATIVE_QCGS_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="keyword">namespace </span>Seldon
<a name="l00024"></a>00024 {
<a name="l00025"></a>00025 
<a name="l00027"></a>00027 
<a name="l00046"></a>00046   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Titer, <span class="keyword">class</span> Matrix1, <span class="keyword">class</span> Vector1, <span class="keyword">class</span> Preconditioner&gt;
<a name="l00047"></a><a class="code" href="namespace_seldon.php#a728a4da8996d872bb1f954136dc87794">00047</a>   <span class="keywordtype">int</span> <a class="code" href="namespace_seldon.php#a728a4da8996d872bb1f954136dc87794" title="Solves linear system using Quasi-minimized Conjugate Gradient Squared.">QCgs</a>(Matrix1&amp; A, Vector1&amp; x, <span class="keyword">const</span> Vector1&amp; b,
<a name="l00048"></a>00048            Preconditioner&amp; M, <a class="code" href="class_seldon_1_1_iteration.php" title="Class containing parameters for an iterative resolution.">Iteration&lt;Titer&gt;</a> &amp; iter)
<a name="l00049"></a>00049   {
<a name="l00050"></a>00050     <span class="keyword">const</span> <span class="keywordtype">int</span> N = A.GetM();
<a name="l00051"></a>00051     <span class="keywordflow">if</span> (N &lt;= 0)
<a name="l00052"></a>00052       <span class="keywordflow">return</span> 0;
<a name="l00053"></a>00053 
<a name="l00054"></a>00054     <span class="keyword">typedef</span> <span class="keyword">typename</span> Vector1::value_type Complexe;
<a name="l00055"></a>00055     Complexe rho_1, rho_2, mu,nu,alpha,beta,sigma,delta;
<a name="l00056"></a>00056     Vector1 p(b), q(b), r(b), rtilde(b), u(b),
<a name="l00057"></a>00057       phat(b), r_qcgs(b), x_qcgs(b), v(b);
<a name="l00058"></a>00058 
<a name="l00059"></a>00059     <span class="comment">// we initialize iter</span>
<a name="l00060"></a>00060     <span class="keywordtype">int</span> success_init = iter.<a class="code" href="class_seldon_1_1_iteration.php#a047b05d60ecc8b6f8f0789255472bfe3" title="Initialization with the right hand side.">Init</a>(b);
<a name="l00061"></a>00061     <span class="keywordflow">if</span> (success_init != 0)
<a name="l00062"></a>00062       <span class="keywordflow">return</span> iter.<a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">ErrorCode</a>();
<a name="l00063"></a>00063 
<a name="l00064"></a>00064     <span class="comment">// we compute the residual r = b - Ax</span>
<a name="l00065"></a>00065     Copy(b,r);
<a name="l00066"></a>00066     <span class="keywordflow">if</span> (!iter.<a class="code" href="class_seldon_1_1_iteration.php#aa02c78783891fabcb4f7517f60f4912e" title="Returns true if the initial guess is null.">IsInitGuess_Null</a>())
<a name="l00067"></a>00067       <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(Complexe(-1), A, x, Complexe(1), r);
<a name="l00068"></a>00068     <span class="keywordflow">else</span>
<a name="l00069"></a>00069       x.Zero();
<a name="l00070"></a>00070 
<a name="l00071"></a>00071     Copy(r, rtilde);
<a name="l00072"></a>00072     rho_1 = Complexe(1);
<a name="l00073"></a>00073     q.Zero(); p.Zero();
<a name="l00074"></a>00074     Copy(r, r_qcgs); Copy(x, x_qcgs);
<a name="l00075"></a>00075     <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;Complexe, Symmetric, RowSymPacked&gt;</a> bt_b(2,2),bt_b_m1(2,2);
<a name="l00076"></a>00076     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Complexe&gt;</a> bt_rn(2);
<a name="l00077"></a>00077 
<a name="l00078"></a>00078     iter.<a class="code" href="class_seldon_1_1_iteration.php#a24c69ffb62504d708354f8874a5f0f00" title="Changes the number of iterations.">SetNumberIteration</a>(0);
<a name="l00079"></a>00079     <span class="comment">// Loop until the stopping criteria are reached</span>
<a name="l00080"></a>00080     <span class="keywordflow">while</span> (! iter.<a class="code" href="class_seldon_1_1_iteration.php#a07c3b0af0412cfc7fe6fd3ed5476c4e0" title="Returns true if the iterative solver has reached its end.">Finished</a>(r_qcgs))
<a name="l00081"></a>00081       {
<a name="l00082"></a>00082         rho_2 = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(rtilde, r);
<a name="l00083"></a>00083 
<a name="l00084"></a>00084         <span class="keywordflow">if</span> (rho_1 == Complexe(0))
<a name="l00085"></a>00085           {
<a name="l00086"></a>00086             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(1, <span class="stringliteral">&quot;QCgs breakdown #1&quot;</span>);
<a name="l00087"></a>00087             <span class="keywordflow">break</span>;
<a name="l00088"></a>00088           }
<a name="l00089"></a>00089         beta = rho_2/rho_1;
<a name="l00090"></a>00090 
<a name="l00091"></a>00091         <span class="comment">// u = r + beta*q</span>
<a name="l00092"></a>00092         <span class="comment">// p = beta*(beta*p +q) + u  where beta = rho_i/rho_{i-1}</span>
<a name="l00093"></a>00093         Copy(r, u);
<a name="l00094"></a>00094         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(beta, q, u);
<a name="l00095"></a>00095         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, p);
<a name="l00096"></a>00096         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), q, p);
<a name="l00097"></a>00097         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, p);
<a name="l00098"></a>00098         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), u, p);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100         <span class="comment">// preconditioning phat = M^{-1} p</span>
<a name="l00101"></a>00101         M.Solve(A, p, phat);
<a name="l00102"></a>00102 
<a name="l00103"></a>00103         <span class="comment">// product matrix vector vhat = A*phat</span>
<a name="l00104"></a>00104         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, phat, v); ++iter;
<a name="l00105"></a>00105         sigma = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(rtilde, v);
<a name="l00106"></a>00106         <span class="keywordflow">if</span> (sigma == Complexe(0))
<a name="l00107"></a>00107           {
<a name="l00108"></a>00108             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(2, <span class="stringliteral">&quot;Qcgs breakdown #2&quot;</span>);
<a name="l00109"></a>00109             <span class="keywordflow">break</span>;
<a name="l00110"></a>00110           }
<a name="l00111"></a>00111         <span class="comment">// q = u-alpha*v  where alpha = rho_i/(rtilde,v)</span>
<a name="l00112"></a>00112         alpha = rho_2 /sigma;
<a name="l00113"></a>00113         Copy(u, q);
<a name="l00114"></a>00114         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-alpha, v, q);
<a name="l00115"></a>00115 
<a name="l00116"></a>00116         <span class="comment">// u = u +q</span>
<a name="l00117"></a>00117         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1), q, u);
<a name="l00118"></a>00118         <span class="comment">// x = x + alpha u</span>
<a name="l00119"></a>00119         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, u, x);
<a name="l00120"></a>00120         <span class="comment">// preconditioning phat = M^{-1} u</span>
<a name="l00121"></a>00121         M.Solve(A, u, phat);
<a name="l00122"></a>00122         <span class="comment">// product matrix vector q = A*phat</span>
<a name="l00123"></a>00123         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, phat, u);
<a name="l00124"></a>00124 
<a name="l00125"></a>00125         <span class="comment">// r = r - alpha u</span>
<a name="l00126"></a>00126         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-alpha, u, r);
<a name="l00127"></a>00127         <span class="comment">// u = r_qcgs - r_n</span>
<a name="l00128"></a>00128         Copy(r_qcgs, u);
<a name="l00129"></a>00129         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-Complexe(1), r, u);
<a name="l00130"></a>00130         bt_b(0,0) = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(u,u);
<a name="l00131"></a>00131         bt_b(1,1) = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(v,v);
<a name="l00132"></a>00132         bt_b(1,0) = <a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(u,v);
<a name="l00133"></a>00133 
<a name="l00134"></a>00134         <span class="comment">// we compute inverse of bt_b</span>
<a name="l00135"></a>00135         delta = bt_b(0,0)*bt_b(1,1) - bt_b(1,0)*bt_b(0,1);
<a name="l00136"></a>00136         <span class="keywordflow">if</span> (delta == Complexe(0))
<a name="l00137"></a>00137           {
<a name="l00138"></a>00138             iter.<a class="code" href="class_seldon_1_1_iteration.php#a32dd5ec2e2b6216c50fbc1d0e27607a7" title="Informs of a failure in the iterative solver.">Fail</a>(3, <span class="stringliteral">&quot;Qcgs breakdown #3&quot;</span>);
<a name="l00139"></a>00139             <span class="keywordflow">break</span>;
<a name="l00140"></a>00140           }
<a name="l00141"></a>00141         bt_b_m1(0,0) = bt_b(1,1)/delta;
<a name="l00142"></a>00142         bt_b_m1(1,1) = bt_b(0,0)/delta;
<a name="l00143"></a>00143         bt_b_m1(1,0) = -bt_b(1,0)/delta;
<a name="l00144"></a>00144 
<a name="l00145"></a>00145         bt_rn(0) = -<a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(u, r); bt_rn(1) = -<a class="code" href="namespace_seldon.php#aa6fd91e25ed22a795e78e2ecef68ed40" title="Scalar product between two vectors.">DotProd</a>(v, r);
<a name="l00146"></a>00146         mu = bt_b_m1(0,0)*bt_rn(0)+bt_b_m1(0,1)*bt_rn(1);
<a name="l00147"></a>00147         nu = bt_b_m1(1,0)*bt_rn(0)+bt_b_m1(1,1)*bt_rn(1);
<a name="l00148"></a>00148 
<a name="l00149"></a>00149         <span class="comment">// smoothing step</span>
<a name="l00150"></a>00150         <span class="comment">// r_qcgs = r + mu (r_qcgs - r) + nu v</span>
<a name="l00151"></a>00151         Copy(r, r_qcgs); <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(mu, u, r_qcgs);
<a name="l00152"></a>00152         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(nu, v, r_qcgs);
<a name="l00153"></a>00153         <span class="comment">// x_qcgs = x + mu (x_qcgs - x) - nu p</span>
<a name="l00154"></a>00154         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(mu,x_qcgs);
<a name="l00155"></a>00155         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(Complexe(1)-mu, x, x_qcgs);
<a name="l00156"></a>00156         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(-nu, p, x_qcgs);
<a name="l00157"></a>00157 
<a name="l00158"></a>00158         rho_1 = rho_2;
<a name="l00159"></a>00159 
<a name="l00160"></a>00160         ++iter;
<a name="l00161"></a>00161       }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163     M.Solve(A, x_qcgs, x);
<a name="l00164"></a>00164     <span class="keywordflow">return</span> iter.<a class="code" href="class_seldon_1_1_iteration.php#aa84736520dcca50ab5b8c72449ef8ed4" title="Returns the error code (if an error occured).">ErrorCode</a>();
<a name="l00165"></a>00165   }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 } <span class="comment">// end namespace</span>
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 <span class="preprocessor">#define SELDON_FILE_ITERATIVE_QCGS_CXX</span>
<a name="l00170"></a>00170 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00171"></a>00171 <span class="preprocessor"></span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
