<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/HeterogeneousMatrixCollection.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_HETEROGENEOUS_MATRIX_COLLECTION_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;HeterogeneousMatrixCollection.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00030"></a>00030   <span class="comment">// HETEROGENEOUSMATRIXCOLLECTION //</span>
<a name="l00032"></a>00032 <span class="comment"></span>
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="comment">/****************</span>
<a name="l00035"></a>00035 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00036"></a>00036 <span class="comment">   ****************/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 
<a name="l00040"></a>00040 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00044"></a>00044             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00045"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553">00045</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span>
<a name="l00046"></a>00046   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00047"></a>00047 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">  ::HeterogeneousMatrixCollection</a>():
<a name="l00048"></a>00048     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;double, Allocator&lt;double&gt; &gt;(), Mlocal_(), Mlocal_sum_(1),
<a name="l00049"></a>00049     Nlocal_(), Nlocal_sum_(1), collection_(), float_dense_c_(),
<a name="l00050"></a>00050     float_sparse_c_(), double_dense_c_(), double_sparse_c_()
<a name="l00051"></a>00051   {
<a name="l00052"></a>00052     nz_ = 0;
<a name="l00053"></a>00053     Mmatrix_ = 0;
<a name="l00054"></a>00054     Nmatrix_ = 0;
<a name="l00055"></a>00055     Mlocal_sum_.Fill(0);
<a name="l00056"></a>00056     Nlocal_sum_.Fill(0);
<a name="l00057"></a>00057     collection_.Fill(-1);
<a name="l00058"></a>00058   }
<a name="l00059"></a>00059 
<a name="l00060"></a>00060 
<a name="l00062"></a>00062 
<a name="l00066"></a>00066   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00067"></a>00067             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00068"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a686bd977ef7310cb3339289ec07eefc2">00068</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span>
<a name="l00069"></a>00069   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00070"></a>00070 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">  ::HeterogeneousMatrixCollection</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l00071"></a>00071     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;double, Allocator&lt;double&gt; &gt;(i, j),
<a name="l00072"></a>00072     Mlocal_(i), Mlocal_sum_(i + 1),
<a name="l00073"></a>00073     Nlocal_(j), Nlocal_sum_(j + 1), collection_(i, j), float_dense_c_(i, j),
<a name="l00074"></a>00074     float_sparse_c_(i, j), double_dense_c_(i, j), double_sparse_c_(i, j)
<a name="l00075"></a>00075   {
<a name="l00076"></a>00076     nz_ = 0;
<a name="l00077"></a>00077     Mmatrix_ = i;
<a name="l00078"></a>00078     Nmatrix_ = j;
<a name="l00079"></a>00079     Mlocal_.Fill(0);
<a name="l00080"></a>00080     Nlocal_.Fill(0);
<a name="l00081"></a>00081     Mlocal_sum_.Fill(0);
<a name="l00082"></a>00082     Nlocal_sum_.Fill(0);
<a name="l00083"></a>00083     collection_.Fill(-1);
<a name="l00084"></a>00084   }
<a name="l00085"></a>00085 
<a name="l00086"></a>00086 
<a name="l00088"></a>00088 
<a name="l00093"></a>00093   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00094"></a>00094             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00095"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa473473c04fea0716d88de40c87b282b">00095</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span>
<a name="l00096"></a>00096   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00097"></a>00097 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa958b2bb3412cda25f49f24938294553" title="Default constructor.">  ::HeterogeneousMatrixCollection</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0,
<a name="l00098"></a>00098                                   Storage0, Prop1, Storage1, Allocator&gt;&amp; A):
<a name="l00099"></a>00099     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;double, Allocator&lt;double&gt; &gt;()
<a name="l00100"></a>00100   {
<a name="l00101"></a>00101     this-&gt;Copy(A);
<a name="l00102"></a>00102   }
<a name="l00103"></a>00103 
<a name="l00104"></a>00104 
<a name="l00105"></a>00105   <span class="comment">/**************</span>
<a name="l00106"></a>00106 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00107"></a>00107 <span class="comment">   **************/</span>
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 
<a name="l00111"></a>00111   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00112"></a>00112             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00113"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89">00113</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span>
<a name="l00114"></a>00114   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89" title="Destructor.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00115"></a>00115 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aeec9a8f4343637e860a9dc627d60ed89" title="Destructor.">  ::~HeterogeneousMatrixCollection</a>()
<a name="l00116"></a>00116   {
<a name="l00117"></a>00117     this-&gt;Clear();
<a name="l00118"></a>00118   }
<a name="l00119"></a>00119 
<a name="l00120"></a>00120 
<a name="l00122"></a>00122   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00123"></a>00123             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00124"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1">00124</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00125"></a>00125   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1" title="Clears the matrix collection without releasing memory.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00126"></a>00126 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0324574cd55a0799aa9746b36b9b20a1" title="Clears the matrix collection without releasing memory.">  ::Clear</a>()
<a name="l00127"></a>00127   {
<a name="l00128"></a>00128     float_dense_c_.Nullify();
<a name="l00129"></a>00129     float_sparse_c_.Nullify();
<a name="l00130"></a>00130     double_dense_c_.Nullify();
<a name="l00131"></a>00131     double_sparse_c_.Nullify();
<a name="l00132"></a>00132 
<a name="l00133"></a>00133     nz_ = 0;
<a name="l00134"></a>00134     Mmatrix_ = 0;
<a name="l00135"></a>00135     Nmatrix_ = 0;
<a name="l00136"></a>00136     Mlocal_.Clear();
<a name="l00137"></a>00137     Nlocal_.Clear();
<a name="l00138"></a>00138     Mlocal_sum_.Clear();
<a name="l00139"></a>00139     Nlocal_sum_.Clear();
<a name="l00140"></a>00140     collection_.Clear();
<a name="l00141"></a>00141   }
<a name="l00142"></a>00142 
<a name="l00143"></a>00143 
<a name="l00145"></a>00145   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00146"></a>00146             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00147"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc">00147</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00148"></a>00148   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00149"></a>00149 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">  ::Nullify</a>()
<a name="l00150"></a>00150   {
<a name="l00151"></a>00151     float_dense_c_.Nullify();
<a name="l00152"></a>00152     float_sparse_c_.Nullify();
<a name="l00153"></a>00153     double_dense_c_.Nullify();
<a name="l00154"></a>00154     double_sparse_c_.Nullify();
<a name="l00155"></a>00155 
<a name="l00156"></a>00156     nz_ = 0;
<a name="l00157"></a>00157     Mmatrix_ = 0;
<a name="l00158"></a>00158     Nmatrix_ = 0;
<a name="l00159"></a>00159     Mlocal_.Clear();
<a name="l00160"></a>00160     Nlocal_.Clear();
<a name="l00161"></a>00161     Mlocal_sum_.Clear();
<a name="l00162"></a>00162     Nlocal_sum_.Clear();
<a name="l00163"></a>00163     collection_.Clear();
<a name="l00164"></a>00164   }
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 
<a name="l00168"></a>00168 
<a name="l00172"></a>00172   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00173"></a>00173             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00174"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a19f1fe843dbb872c589db2116cd204f9">00174</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00175"></a>00175   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00176"></a>00176 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3e48adb67f58b6355dc7ce39611585cc" title="Clears the matrix collection without releasing memory.">  ::Nullify</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00177"></a>00177   {
<a name="l00178"></a>00178 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00179"></a>00179 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00180"></a>00180       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::Nullify()&quot;</span>,
<a name="l00181"></a>00181                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00182"></a>00182                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00183"></a>00183                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00184"></a>00184     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00185"></a>00185       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::Nullify()&quot;</span>,
<a name="l00186"></a>00186                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00187"></a>00187                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00188"></a>00188                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00189"></a>00189 <span class="preprocessor">#endif</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>
<a name="l00191"></a>00191     <span class="keywordflow">switch</span> (collection_(i, j))
<a name="l00192"></a>00192       {
<a name="l00193"></a>00193       <span class="keywordflow">case</span> 0:
<a name="l00194"></a>00194         nz_ -= float_dense_c_.GetMatrix(i, j).GetDataSize();
<a name="l00195"></a>00195         float_dense_c_.Nullify(i, j);
<a name="l00196"></a>00196       <span class="keywordflow">case</span> 1:
<a name="l00197"></a>00197         nz_ -= float_sparse_c_.GetMatrix(i, j).GetDataSize();
<a name="l00198"></a>00198         float_sparse_c_.Nullify(i, j);
<a name="l00199"></a>00199         <span class="keywordflow">break</span>;
<a name="l00200"></a>00200       <span class="keywordflow">case</span> 2:
<a name="l00201"></a>00201         nz_ -= double_dense_c_.GetMatrix(i, j).GetDataSize();
<a name="l00202"></a>00202         double_dense_c_.Nullify(i, j);
<a name="l00203"></a>00203         <span class="keywordflow">break</span>;
<a name="l00204"></a>00204       <span class="keywordflow">case</span> 3:
<a name="l00205"></a>00205         nz_ -= double_sparse_c_.GetMatrix(i, j).GetDataSize();
<a name="l00206"></a>00206         double_sparse_c_.Nullify(i, j);
<a name="l00207"></a>00207         <span class="keywordflow">break</span>;
<a name="l00208"></a>00208       }
<a name="l00209"></a>00209 
<a name="l00210"></a>00210     collection_(i, j) = -1;
<a name="l00211"></a>00211   }
<a name="l00212"></a>00212 
<a name="l00213"></a>00213 
<a name="l00215"></a>00215   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00216"></a>00216             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00217"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588">00217</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00218"></a>00218   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588" title="Deallocates underlying the matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00219"></a>00219 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a34264880bdeb3febb8c73b2cad0b4588" title="Deallocates underlying the matrices.">  ::Deallocate</a>()
<a name="l00220"></a>00220   {
<a name="l00221"></a>00221     float_dense_c_.Deallocate();
<a name="l00222"></a>00222     float_sparse_c_.Deallocate();
<a name="l00223"></a>00223     double_dense_c_.Deallocate();
<a name="l00224"></a>00224     double_sparse_c_.Deallocate();
<a name="l00225"></a>00225     this-&gt;~<a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>();
<a name="l00226"></a>00226   }
<a name="l00227"></a>00227 
<a name="l00228"></a>00228 
<a name="l00229"></a>00229   <span class="comment">/*******************</span>
<a name="l00230"></a>00230 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00231"></a>00231 <span class="comment">   *******************/</span>
<a name="l00232"></a>00232 
<a name="l00233"></a>00233 
<a name="l00235"></a>00235 
<a name="l00239"></a>00239   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00240"></a>00240             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00241"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e">00241</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00242"></a>00242   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00243"></a>00243 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">  ::GetM</a>()<span class="keyword"> const</span>
<a name="l00244"></a>00244 <span class="keyword">  </span>{
<a name="l00245"></a>00245     <span class="keywordflow">return</span> this-&gt;m_;
<a name="l00246"></a>00246   }
<a name="l00247"></a>00247 
<a name="l00248"></a>00248 
<a name="l00250"></a>00250 
<a name="l00254"></a>00254   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00255"></a>00255             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00256"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa">00256</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00257"></a>00257   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa" title="Returns the number of rows.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00258"></a>00258 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a38820efd26db52feaeb8998d2fd43daa" title="Returns the number of rows.">  ::GetMmatrix</a>()<span class="keyword"> const</span>
<a name="l00259"></a>00259 <span class="keyword">  </span>{
<a name="l00260"></a>00260     <span class="keywordflow">return</span> Mmatrix_;
<a name="l00261"></a>00261   }
<a name="l00262"></a>00262 
<a name="l00263"></a>00263 
<a name="l00265"></a>00265 
<a name="l00269"></a>00269   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00270"></a>00270             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00271"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac368f84e127c6738f7f06c74bbc6e7da">00271</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00272"></a>00272   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00273"></a>00273 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a7b22f363d1535f911ee271f1e0743c6e" title="Returns the number of rows.">  ::GetM</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00274"></a>00274 <span class="keyword">  </span>{
<a name="l00275"></a>00275 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00276"></a>00276 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00277"></a>00277       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetM()&quot;</span>,
<a name="l00278"></a>00278                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00279"></a>00279                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00280"></a>00280                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00281"></a>00281 <span class="preprocessor">#endif</span>
<a name="l00282"></a>00282 <span class="preprocessor"></span>
<a name="l00283"></a>00283     <span class="keywordflow">return</span> Mlocal_(i);
<a name="l00284"></a>00284   }
<a name="l00285"></a>00285 
<a name="l00286"></a>00286 
<a name="l00288"></a>00288 
<a name="l00292"></a>00292   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00293"></a>00293             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00294"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d">00294</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00295"></a>00295   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00296"></a>00296 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">  ::GetN</a>()<span class="keyword"> const</span>
<a name="l00297"></a>00297 <span class="keyword">  </span>{
<a name="l00298"></a>00298     <span class="keywordflow">return</span> this-&gt;n_;
<a name="l00299"></a>00299   }
<a name="l00300"></a>00300 
<a name="l00301"></a>00301 
<a name="l00303"></a>00303 
<a name="l00307"></a>00307   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00308"></a>00308             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00309"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe">00309</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00310"></a>00310   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe" title="Returns the number of columns.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00311"></a>00311 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6c59c77733659c863477c9a34fc927fe" title="Returns the number of columns.">  ::GetNmatrix</a>()<span class="keyword"> const</span>
<a name="l00312"></a>00312 <span class="keyword">  </span>{
<a name="l00313"></a>00313     <span class="keywordflow">return</span> Nmatrix_;
<a name="l00314"></a>00314   }
<a name="l00315"></a>00315 
<a name="l00316"></a>00316 
<a name="l00318"></a>00318 
<a name="l00323"></a>00323   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00324"></a>00324             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00325"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3b788a9ac72e2f32842dcc9251c6ca0f">00325</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00326"></a>00326   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00327"></a>00327 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a03dbde09b4beb73bd66628b4db00df0d" title="Returns the number of columns.">  ::GetN</a>(<span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00328"></a>00328 <span class="keyword">  </span>{
<a name="l00329"></a>00329 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00330"></a>00330 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00331"></a>00331       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetN()&quot;</span>,
<a name="l00332"></a>00332                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00333"></a>00333                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00334"></a>00334                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00335"></a>00335 <span class="preprocessor">#endif</span>
<a name="l00336"></a>00336 <span class="preprocessor"></span>
<a name="l00337"></a>00337     <span class="keywordflow">return</span> Nlocal_(j);
<a name="l00338"></a>00338   }
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 
<a name="l00342"></a>00342 
<a name="l00345"></a>00345   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00346"></a>00346             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00347"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8">00347</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00348"></a>00348   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8" title="Returns the number of elements stored in memory.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00349"></a>00349 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae5570f5b9a4dac52024ced4061cfe5a8" title="Returns the number of elements stored in memory.">  ::GetSize</a>()<span class="keyword"> const</span>
<a name="l00350"></a>00350 <span class="keyword">  </span>{
<a name="l00351"></a>00351     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00352"></a>00352   }
<a name="l00353"></a>00353 
<a name="l00354"></a>00354 
<a name="l00356"></a>00356 
<a name="l00359"></a>00359   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00360"></a>00360             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00361"></a>00361             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l00362"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd">00362</a>   <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00363"></a>00363   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd" title="Returns the number of elements stored in memory.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00364"></a>00364 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2a36130a12ef3b4e4a4971f5898ca0bd" title="Returns the number of elements stored in memory.">  ::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00365"></a>00365 <span class="keyword">  </span>{
<a name="l00366"></a>00366     <span class="keywordflow">return</span> nz_;
<a name="l00367"></a>00367   }
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 
<a name="l00371"></a>00371 
<a name="l00380"></a>00380   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00381"></a>00381             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00382"></a>00382             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l00383"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e">00383</a>   <span class="keyword">inline</span> <span class="keywordtype">int</span>
<a name="l00384"></a>00384   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e" title="Returns the type of a given underlying matrix.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00385"></a>00385 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69858677f0823951f46671248091be2e" title="Returns the type of a given underlying matrix.">  ::GetType</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00386"></a>00386 <span class="keyword">  </span>{
<a name="l00387"></a>00387 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00388"></a>00388 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00389"></a>00389       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetType()&quot;</span>,
<a name="l00390"></a>00390                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00391"></a>00391                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00392"></a>00392                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00393"></a>00393     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00394"></a>00394       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetType()&quot;</span>,
<a name="l00395"></a>00395                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00396"></a>00396                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00397"></a>00397                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00398"></a>00398 <span class="preprocessor">#endif</span>
<a name="l00399"></a>00399 <span class="preprocessor"></span>    <span class="keywordflow">return</span> collection_(i, j);
<a name="l00400"></a>00400   }
<a name="l00401"></a>00401 
<a name="l00402"></a>00402 
<a name="l00403"></a>00403 
<a name="l00405"></a>00405 
<a name="l00408"></a>00408   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00409"></a>00409             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00410"></a>00410             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00411"></a>00411   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00412"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6">00412</a>   ::float_dense_c&amp;
<a name="l00413"></a>00413   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00414"></a>00414 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">  ::GetFloatDense</a>()
<a name="l00415"></a>00415   {
<a name="l00416"></a>00416     <span class="keywordflow">return</span> float_dense_c_;
<a name="l00417"></a>00417   }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00421"></a>00421 
<a name="l00424"></a>00424   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00425"></a>00425             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00426"></a>00426             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span>
<a name="l00427"></a>00427   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00428"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aa53a233cf9ea59c8711247cc013d75b7">00428</a>   ::float_dense_c&amp;
<a name="l00429"></a>00429   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00430"></a>00430 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bd6c070c0738b3425e5e9b3e0118ec6" title="Returns the collection of float dense underlying matrices.">  ::GetFloatDense</a>()<span class="keyword"> const</span>
<a name="l00431"></a>00431 <span class="keyword">  </span>{
<a name="l00432"></a>00432     <span class="keywordflow">return</span> float_dense_c_;
<a name="l00433"></a>00433   }
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00437"></a>00437 
<a name="l00440"></a>00440   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00441"></a>00441             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00442"></a>00442             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00443"></a>00443   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00444"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24">00444</a>   ::float_sparse_c&amp;
<a name="l00445"></a>00445   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00446"></a>00446 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">  ::GetFloatSparse</a>()
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448     <span class="keywordflow">return</span> float_sparse_c_;
<a name="l00449"></a>00449   }
<a name="l00450"></a>00450 
<a name="l00451"></a>00451 
<a name="l00453"></a>00453 
<a name="l00456"></a>00456   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00457"></a>00457             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00458"></a>00458             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span>
<a name="l00459"></a>00459   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00460"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#afbdb423cb862ab27779c8e526f4f05e6">00460</a>   ::float_sparse_c&amp;
<a name="l00461"></a>00461   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00462"></a>00462 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a69cf73dbc6d9055bc8ad28633de18f24" title="Returns the collection of float sparse underlying matrices.">  ::GetFloatSparse</a>()<span class="keyword"> const</span>
<a name="l00463"></a>00463 <span class="keyword">  </span>{
<a name="l00464"></a>00464     <span class="keywordflow">return</span> float_sparse_c_;
<a name="l00465"></a>00465   }
<a name="l00466"></a>00466 
<a name="l00467"></a>00467 
<a name="l00469"></a>00469 
<a name="l00472"></a>00472   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00473"></a>00473             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00474"></a>00474             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00475"></a>00475   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00476"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f">00476</a>   ::double_dense_c&amp;
<a name="l00477"></a>00477   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00478"></a>00478 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">  ::GetDoubleDense</a>()
<a name="l00479"></a>00479   {
<a name="l00480"></a>00480     <span class="keywordflow">return</span> double_dense_c_;
<a name="l00481"></a>00481   }
<a name="l00482"></a>00482 
<a name="l00483"></a>00483 
<a name="l00485"></a>00485 
<a name="l00488"></a>00488   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00489"></a>00489             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00490"></a>00490             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span>
<a name="l00491"></a>00491   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00492"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9bc398080b4454e0bcbf968fd25fbd78">00492</a>   ::double_dense_c&amp;
<a name="l00493"></a>00493   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00494"></a>00494 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a289e9f762721738e42b9907450e72e9f" title="Returns the collection of double dense underlying matrices.">  ::GetDoubleDense</a>()<span class="keyword"> const</span>
<a name="l00495"></a>00495 <span class="keyword">  </span>{
<a name="l00496"></a>00496     <span class="keywordflow">return</span> double_dense_c_;
<a name="l00497"></a>00497   }
<a name="l00498"></a>00498 
<a name="l00499"></a>00499 
<a name="l00501"></a>00501 
<a name="l00504"></a>00504   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00505"></a>00505             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00506"></a>00506             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">typename</span>
<a name="l00507"></a>00507   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00508"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af">00508</a>   ::double_sparse_c&amp;
<a name="l00509"></a>00509   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00510"></a>00510 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">  ::GetDoubleSparse</a>()
<a name="l00511"></a>00511   {
<a name="l00512"></a>00512     <span class="keywordflow">return</span> double_sparse_c_;
<a name="l00513"></a>00513   }
<a name="l00514"></a>00514 
<a name="l00515"></a>00515 
<a name="l00517"></a>00517 
<a name="l00520"></a>00520   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00521"></a>00521             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00522"></a>00522             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keyword">const</span> <span class="keyword">typename</span>
<a name="l00523"></a>00523   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00524"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1a4f404541bb2b70a36cc1a193ee5cb7">00524</a>   ::double_sparse_c&amp;
<a name="l00525"></a>00525   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00526"></a>00526 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a39301f5774586f67d31f64a3d4acb0af" title="Returns the collection of double sparse underlying matrices.">  ::GetDoubleSparse</a>()<span class="keyword"> const</span>
<a name="l00527"></a>00527 <span class="keyword">  </span>{
<a name="l00528"></a>00528     <span class="keywordflow">return</span> double_sparse_c_;
<a name="l00529"></a>00529   }
<a name="l00530"></a>00530 
<a name="l00531"></a>00531 
<a name="l00532"></a>00532   <span class="comment">/*********************</span>
<a name="l00533"></a>00533 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00534"></a>00534 <span class="comment">   *********************/</span>
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 
<a name="l00538"></a>00538 
<a name="l00544"></a>00544   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00545"></a>00545             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00546"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60">00546</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00547"></a>00547   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60" title="Reallocates memory to resize the matrix collection.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00548"></a>00548 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ae600c754d08ba0af893d118cd3a38c60" title="Reallocates memory to resize the matrix collection.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00549"></a>00549   {
<a name="l00550"></a>00550     nz_ = 0;
<a name="l00551"></a>00551     Mmatrix_ = i;
<a name="l00552"></a>00552     Nmatrix_ = j;
<a name="l00553"></a>00553     Mlocal_.Reallocate(i);
<a name="l00554"></a>00554     Nlocal_.Reallocate(j);
<a name="l00555"></a>00555     Mlocal_sum_.Reallocate(i + 1);
<a name="l00556"></a>00556     Nlocal_sum_.Reallocate(j + 1);
<a name="l00557"></a>00557     Mlocal_.Fill(0);
<a name="l00558"></a>00558     Nlocal_.Fill(0);
<a name="l00559"></a>00559     Mlocal_sum_.Fill(0);
<a name="l00560"></a>00560     Nlocal_sum_.Fill(0);
<a name="l00561"></a>00561 
<a name="l00562"></a>00562     collection_.Reallocate(i, j);
<a name="l00563"></a>00563     float_dense_c_.Reallocate(i, j);
<a name="l00564"></a>00564     float_sparse_c_.Reallocate(i, j);
<a name="l00565"></a>00565     double_dense_c_.Reallocate(i, j);
<a name="l00566"></a>00566     double_sparse_c_.Reallocate(i, j);
<a name="l00567"></a>00567   }
<a name="l00568"></a>00568 
<a name="l00569"></a>00569 
<a name="l00571"></a>00571 
<a name="l00576"></a>00576   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00577"></a>00577             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00578"></a>00578             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00579"></a>00579   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l00580"></a>00580 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">  ::SetMatrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;
<a name="l00581"></a>00581               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::float_dense_m&amp; A)
<a name="l00582"></a>00582   {
<a name="l00583"></a>00583 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00584"></a>00584 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00585"></a>00585       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00586"></a>00586                      <span class="stringliteral">&quot;SetMatrix(float_dense_m)&quot;</span>,
<a name="l00587"></a>00587                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00588"></a>00588                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00589"></a>00589                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00590"></a>00590     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00591"></a>00591       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00592"></a>00592                      <span class="stringliteral">&quot;SetMatrix(float_dense_m)&quot;</span>,
<a name="l00593"></a>00593                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00594"></a>00594                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00595"></a>00595                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00596"></a>00596     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00597"></a>00597       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00598"></a>00598                      <span class="stringliteral">&quot;SetMatrix(float_dense_m)&quot;</span>,
<a name="l00599"></a>00599                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00600"></a>00600                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; lines, but has &quot;</span>
<a name="l00601"></a>00601                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; lines.&quot;</span>);
<a name="l00602"></a>00602     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00603"></a>00603       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00604"></a>00604                      <span class="stringliteral">&quot;SetMatrix(float_dense_m)&quot;</span>,
<a name="l00605"></a>00605                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00606"></a>00606                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00607"></a>00607                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00608"></a>00608 <span class="preprocessor">#endif</span>
<a name="l00609"></a>00609 <span class="preprocessor"></span>
<a name="l00610"></a>00610     Nullify(i, j);
<a name="l00611"></a>00611 
<a name="l00612"></a>00612     collection_(i, j) = 0;
<a name="l00613"></a>00613 
<a name="l00614"></a>00614     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00615"></a>00615     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00616"></a>00616 
<a name="l00617"></a>00617     Mlocal_(i) = A.GetM();
<a name="l00618"></a>00618     Nlocal_(j) = A.GetN();
<a name="l00619"></a>00619 
<a name="l00620"></a>00620     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00621"></a>00621       Mlocal_sum_(k) += Mdiff;
<a name="l00622"></a>00622 
<a name="l00623"></a>00623     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00624"></a>00624       Nlocal_sum_(k) += Ndiff;
<a name="l00625"></a>00625 
<a name="l00626"></a>00626     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00627"></a>00627     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00628"></a>00628 
<a name="l00629"></a>00629     float_dense_c_.SetMatrix(i, j, A);
<a name="l00630"></a>00630   }
<a name="l00631"></a>00631 
<a name="l00632"></a>00632 
<a name="l00634"></a>00634 
<a name="l00639"></a>00639   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00640"></a>00640             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00641"></a>00641             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00642"></a>00642   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00643"></a>00643   ::SetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;
<a name="l00644"></a>00644               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::float_sparse_m&amp; A)
<a name="l00645"></a>00645   {
<a name="l00646"></a>00646 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00647"></a>00647 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00648"></a>00648       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00649"></a>00649                      <span class="stringliteral">&quot;SetMatrix(float_sparse_m)&quot;</span>,
<a name="l00650"></a>00650                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00651"></a>00651                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00652"></a>00652                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00653"></a>00653     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00654"></a>00654       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00655"></a>00655                      <span class="stringliteral">&quot;SetMatrix(float_sparse_m)&quot;</span>,
<a name="l00656"></a>00656                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00657"></a>00657                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00658"></a>00658                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00659"></a>00659     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00660"></a>00660       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00661"></a>00661                      <span class="stringliteral">&quot;SetMatrix(float_sparse_m)&quot;</span>,
<a name="l00662"></a>00662                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00663"></a>00663                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; lines, but has &quot;</span>
<a name="l00664"></a>00664                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; lines.&quot;</span>);
<a name="l00665"></a>00665     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00666"></a>00666       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00667"></a>00667                      <span class="stringliteral">&quot;SetMatrix(float_sparse_m)&quot;</span>,
<a name="l00668"></a>00668                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00669"></a>00669                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00670"></a>00670                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00671"></a>00671 <span class="preprocessor">#endif</span>
<a name="l00672"></a>00672 <span class="preprocessor"></span>
<a name="l00673"></a>00673     Nullify(i, j);
<a name="l00674"></a>00674 
<a name="l00675"></a>00675     collection_(i, j) = 1;
<a name="l00676"></a>00676 
<a name="l00677"></a>00677     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00678"></a>00678     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00679"></a>00679 
<a name="l00680"></a>00680     Mlocal_(i) = A.GetM();
<a name="l00681"></a>00681     Nlocal_(j) = A.GetN();
<a name="l00682"></a>00682 
<a name="l00683"></a>00683     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00684"></a>00684       Mlocal_sum_(k) += Mdiff;
<a name="l00685"></a>00685 
<a name="l00686"></a>00686     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00687"></a>00687       Nlocal_sum_(k) += Ndiff;
<a name="l00688"></a>00688 
<a name="l00689"></a>00689     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00690"></a>00690     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00691"></a>00691 
<a name="l00692"></a>00692     float_sparse_c_.SetMatrix(i, j, A);
<a name="l00693"></a>00693   }
<a name="l00694"></a>00694 
<a name="l00695"></a>00695 
<a name="l00697"></a>00697 
<a name="l00702"></a>00702   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00703"></a>00703             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00704"></a>00704             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00705"></a>00705   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00706"></a>00706   ::SetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;
<a name="l00707"></a>00707               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::double_dense_m&amp; A)
<a name="l00708"></a>00708   {
<a name="l00709"></a>00709 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00710"></a>00710 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00711"></a>00711       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00712"></a>00712                      <span class="stringliteral">&quot;SetMatrix(double_dense_m)&quot;</span>,
<a name="l00713"></a>00713                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00714"></a>00714                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00715"></a>00715                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00716"></a>00716     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00717"></a>00717       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00718"></a>00718                      <span class="stringliteral">&quot;SetMatrix(double_dense_m)&quot;</span>,
<a name="l00719"></a>00719                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00720"></a>00720                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00721"></a>00721                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00722"></a>00722     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00723"></a>00723       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00724"></a>00724                      <span class="stringliteral">&quot;SetMatrix(double_dense_m)&quot;</span>,
<a name="l00725"></a>00725                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00726"></a>00726                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; lines, but has &quot;</span>
<a name="l00727"></a>00727                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; lines.&quot;</span>);
<a name="l00728"></a>00728     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00729"></a>00729       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00730"></a>00730                      <span class="stringliteral">&quot;SetMatrix(double_dense_m)&quot;</span>,
<a name="l00731"></a>00731                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00732"></a>00732                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00733"></a>00733                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00734"></a>00734 <span class="preprocessor">#endif</span>
<a name="l00735"></a>00735 <span class="preprocessor"></span>
<a name="l00736"></a>00736     Nullify(i, j);
<a name="l00737"></a>00737 
<a name="l00738"></a>00738     collection_(i, j) = 2;
<a name="l00739"></a>00739 
<a name="l00740"></a>00740     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00741"></a>00741     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00742"></a>00742 
<a name="l00743"></a>00743     Mlocal_(i) = A.GetM();
<a name="l00744"></a>00744     Nlocal_(j) = A.GetN();
<a name="l00745"></a>00745 
<a name="l00746"></a>00746     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00747"></a>00747       Mlocal_sum_(k) += Mdiff;
<a name="l00748"></a>00748 
<a name="l00749"></a>00749     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00750"></a>00750       Nlocal_sum_(k) += Ndiff;
<a name="l00751"></a>00751 
<a name="l00752"></a>00752     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00753"></a>00753     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00754"></a>00754 
<a name="l00755"></a>00755     double_dense_c_.SetMatrix(i, j, A);
<a name="l00756"></a>00756   }
<a name="l00757"></a>00757 
<a name="l00758"></a>00758 
<a name="l00760"></a>00760 
<a name="l00765"></a>00765   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00766"></a>00766             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00767"></a>00767             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00768"></a>00768   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00769"></a>00769   ::SetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00770"></a>00770               <span class="keyword">const</span> <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt; Prop0, Storage0,
<a name="l00771"></a>00771               Prop1, Storage1, Allocator&gt;::double_sparse_m&amp; A)
<a name="l00772"></a>00772   {
<a name="l00773"></a>00773 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00774"></a>00774 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00775"></a>00775       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00776"></a>00776                      <span class="stringliteral">&quot;SetMatrix(double_sparse_m)&quot;</span>,
<a name="l00777"></a>00777                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00778"></a>00778                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00779"></a>00779                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00780"></a>00780     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00781"></a>00781       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00782"></a>00782                      <span class="stringliteral">&quot;SetMatrix(double_sparse_m)&quot;</span>,
<a name="l00783"></a>00783                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00784"></a>00784                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00785"></a>00785                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00786"></a>00786     <span class="keywordflow">if</span> ((Mlocal_(i) != 0) &amp;&amp; (Mlocal_(i) != A.GetM()))
<a name="l00787"></a>00787       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00788"></a>00788                      <span class="stringliteral">&quot;SetMatrix(double_sparse_m)&quot;</span>,
<a name="l00789"></a>00789                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00790"></a>00790                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_(i)) + <span class="stringliteral">&quot; lines, but has &quot;</span>
<a name="l00791"></a>00791                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; lines.&quot;</span>);
<a name="l00792"></a>00792     <span class="keywordflow">if</span> ((Nlocal_(j) != 0) &amp;&amp; (Nlocal_(j) != A.GetN()))
<a name="l00793"></a>00793       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00794"></a>00794                      <span class="stringliteral">&quot;SetMatrix(double_sparse_m)&quot;</span>,
<a name="l00795"></a>00795                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The matrix expected should have &quot;</span>)
<a name="l00796"></a>00796                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_(j)) + <span class="stringliteral">&quot; columns, but has &quot;</span>
<a name="l00797"></a>00797                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; columns.&quot;</span>);
<a name="l00798"></a>00798 <span class="preprocessor">#endif</span>
<a name="l00799"></a>00799 <span class="preprocessor"></span>
<a name="l00800"></a>00800     Nullify(i, j);
<a name="l00801"></a>00801 
<a name="l00802"></a>00802     collection_(i, j) = 3;
<a name="l00803"></a>00803 
<a name="l00804"></a>00804     <span class="keywordtype">int</span> Mdiff = A.GetM() - Mlocal_(i);
<a name="l00805"></a>00805     <span class="keywordtype">int</span> Ndiff = A.GetN() - Nlocal_(j);
<a name="l00806"></a>00806 
<a name="l00807"></a>00807     Mlocal_(i) = A.GetM();
<a name="l00808"></a>00808     Nlocal_(j) = A.GetN();
<a name="l00809"></a>00809 
<a name="l00810"></a>00810     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = i + 1; k &lt; Mmatrix_ + 1; k++)
<a name="l00811"></a>00811       Mlocal_sum_(k) += Mdiff;
<a name="l00812"></a>00812 
<a name="l00813"></a>00813     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = j + 1; k &lt; Nmatrix_ + 1; k++)
<a name="l00814"></a>00814       Nlocal_sum_(k) += Ndiff;
<a name="l00815"></a>00815 
<a name="l00816"></a>00816     this-&gt;m_ = Mlocal_sum_(Mmatrix_);
<a name="l00817"></a>00817     this-&gt;n_ = Nlocal_sum_(Nmatrix_);
<a name="l00818"></a>00818 
<a name="l00819"></a>00819     double_sparse_c_.SetMatrix(i, j, A);
<a name="l00820"></a>00820   }
<a name="l00821"></a>00821 
<a name="l00822"></a>00822 
<a name="l00823"></a>00823   <span class="comment">/**********************************</span>
<a name="l00824"></a>00824 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00825"></a>00825 <span class="comment">   **********************************/</span>
<a name="l00826"></a>00826 
<a name="l00827"></a>00827 
<a name="l00829"></a>00829 
<a name="l00835"></a>00835   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00836"></a>00836             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00837"></a>00837             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00838"></a>00838   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00839"></a>00839   ::GetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;
<a name="l00840"></a>00840               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::float_dense_m&amp; M)<span class="keyword"></span>
<a name="l00841"></a>00841 <span class="keyword">    const</span>
<a name="l00842"></a>00842 <span class="keyword">  </span>{
<a name="l00843"></a>00843 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00844"></a>00844 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00845"></a>00845       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00846"></a>00846                      <span class="stringliteral">&quot;GetMatrix(float_dense_m)&quot;</span>,
<a name="l00847"></a>00847                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00848"></a>00848                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00849"></a>00849                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00850"></a>00850     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00851"></a>00851       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00852"></a>00852                      <span class="stringliteral">&quot;GetMatrix(float_dense_m)&quot;</span>,
<a name="l00853"></a>00853                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00854"></a>00854                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00855"></a>00855                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00856"></a>00856 <span class="preprocessor">#endif</span>
<a name="l00857"></a>00857 <span class="preprocessor"></span>
<a name="l00858"></a>00858     <span class="keywordflow">if</span> (collection_(i, j) != 0)
<a name="l00859"></a>00859       {
<a name="l00860"></a>00860         <span class="keywordtype">string</span> matrix_type;
<a name="l00861"></a>00861         <span class="keywordflow">switch</span>(collection_(i, j))
<a name="l00862"></a>00862           {
<a name="l00863"></a>00863           <span class="keywordflow">case</span> 1:
<a name="l00864"></a>00864             matrix_type = <span class="stringliteral">&quot;float_sparse&quot;</span>;
<a name="l00865"></a>00865             <span class="keywordflow">break</span>;
<a name="l00866"></a>00866           <span class="keywordflow">case</span> 2:
<a name="l00867"></a>00867             matrix_type = <span class="stringliteral">&quot;double_dense&quot;</span>;
<a name="l00868"></a>00868             <span class="keywordflow">break</span>;
<a name="l00869"></a>00869           <span class="keywordflow">case</span> 3:
<a name="l00870"></a>00870             matrix_type = <span class="stringliteral">&quot;double_sparse&quot;</span>;
<a name="l00871"></a>00871             <span class="keywordflow">break</span>;
<a name="l00872"></a>00872           <span class="keywordflow">default</span>:
<a name="l00873"></a>00873             <span class="keywordflow">throw</span>
<a name="l00874"></a>00874               WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j,&quot;</span>
<a name="l00875"></a>00875                             <span class="stringliteral">&quot;Matrix&lt;Float, Dense&gt; M)&quot;</span>,
<a name="l00876"></a>00876                             <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00877"></a>00877                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00878"></a>00878           }
<a name="l00879"></a>00879 
<a name="l00880"></a>00880         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j, &quot;</span>
<a name="l00881"></a>00881                             <span class="stringliteral">&quot;Matrix&lt;Float, Dense&gt; M)&quot;</span>,
<a name="l00882"></a>00882                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Wrong type for matrix &quot;</span>)
<a name="l00883"></a>00883                             + matrix_type + <span class="stringliteral">&quot; M.&quot;</span>);
<a name="l00884"></a>00884       }
<a name="l00885"></a>00885 
<a name="l00886"></a>00886     M.SetData(Mlocal_(i), Nlocal_(j),
<a name="l00887"></a>00887               float_dense_c_.GetMatrix(i, j).GetData());
<a name="l00888"></a>00888   }
<a name="l00889"></a>00889 
<a name="l00890"></a>00890 
<a name="l00892"></a>00892 
<a name="l00898"></a>00898   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00899"></a>00899             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00900"></a>00900             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00901"></a>00901   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00902"></a>00902   ::GetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;
<a name="l00903"></a>00903               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::float_sparse_m&amp; M)<span class="keyword"></span>
<a name="l00904"></a>00904 <span class="keyword">    const</span>
<a name="l00905"></a>00905 <span class="keyword">  </span>{
<a name="l00906"></a>00906 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00907"></a>00907 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00908"></a>00908       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00909"></a>00909                      <span class="stringliteral">&quot;GetMatrix(float_sparse_m)&quot;</span>,
<a name="l00910"></a>00910                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00911"></a>00911                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00912"></a>00912                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00913"></a>00913     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00914"></a>00914       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00915"></a>00915                      <span class="stringliteral">&quot;GetMatrix(float_sparse_m)&quot;</span>,
<a name="l00916"></a>00916                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00917"></a>00917                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00918"></a>00918                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00919"></a>00919 <span class="preprocessor">#endif</span>
<a name="l00920"></a>00920 <span class="preprocessor"></span>
<a name="l00921"></a>00921     <span class="keywordflow">if</span> (collection_(i, j) != 1)
<a name="l00922"></a>00922       {
<a name="l00923"></a>00923         <span class="keywordtype">string</span> matrix_type;
<a name="l00924"></a>00924         <span class="keywordflow">switch</span>(collection_(i, j))
<a name="l00925"></a>00925           {
<a name="l00926"></a>00926           <span class="keywordflow">case</span> 0:
<a name="l00927"></a>00927             matrix_type = <span class="stringliteral">&quot;float_dense&quot;</span>;
<a name="l00928"></a>00928             <span class="keywordflow">break</span>;
<a name="l00929"></a>00929           <span class="keywordflow">case</span> 2:
<a name="l00930"></a>00930             matrix_type = <span class="stringliteral">&quot;double_dense&quot;</span>;
<a name="l00931"></a>00931             <span class="keywordflow">break</span>;
<a name="l00932"></a>00932           <span class="keywordflow">case</span> 3:
<a name="l00933"></a>00933             matrix_type = <span class="stringliteral">&quot;double_sparse&quot;</span>;
<a name="l00934"></a>00934             <span class="keywordflow">break</span>;
<a name="l00935"></a>00935           <span class="keywordflow">default</span>:
<a name="l00936"></a>00936             <span class="keywordflow">throw</span>
<a name="l00937"></a>00937               WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j, &quot;</span>
<a name="l00938"></a>00938                             <span class="stringliteral">&quot;Matrix&lt;Float, Sparse&gt; M)&quot;</span>,
<a name="l00939"></a>00939                             <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00940"></a>00940                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00941"></a>00941           }
<a name="l00942"></a>00942 
<a name="l00943"></a>00943         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j, &quot;</span>
<a name="l00944"></a>00944                             <span class="stringliteral">&quot;Matrix&lt;Float, Sparse&gt; M)&quot;</span>,
<a name="l00945"></a>00945                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Wrong type for matrix &quot;</span>)
<a name="l00946"></a>00946                             + matrix_type + <span class="stringliteral">&quot; M.&quot;</span>);
<a name="l00947"></a>00947       }
<a name="l00948"></a>00948 
<a name="l00949"></a>00949     M.SetData(float_sparse_c_.GetMatrix(i, j).GetM(),
<a name="l00950"></a>00950               float_sparse_c_.GetMatrix(i, j).GetN(),
<a name="l00951"></a>00951               float_sparse_c_.GetMatrix(i, j).GetNonZeros(),
<a name="l00952"></a>00952               float_sparse_c_.GetMatrix(i, j).GetData(),
<a name="l00953"></a>00953               float_sparse_c_.GetMatrix(i, j).GetPtr(),
<a name="l00954"></a>00954               float_sparse_c_.GetMatrix(i, j).GetInd());
<a name="l00955"></a>00955   }
<a name="l00956"></a>00956 
<a name="l00957"></a>00957 
<a name="l00959"></a>00959 
<a name="l00965"></a>00965   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l00966"></a>00966             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l00967"></a>00967             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l00968"></a>00968   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l00969"></a>00969   ::GetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;
<a name="l00970"></a>00970               Prop0, Storage0, Prop1, Storage1, Allocator&gt;::double_dense_m&amp; M)<span class="keyword"></span>
<a name="l00971"></a>00971 <span class="keyword">    const</span>
<a name="l00972"></a>00972 <span class="keyword">  </span>{
<a name="l00973"></a>00973 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00974"></a>00974 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l00975"></a>00975       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00976"></a>00976                      <span class="stringliteral">&quot;GetMatrix(double_dense_m)&quot;</span>,
<a name="l00977"></a>00977                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l00978"></a>00978                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00979"></a>00979                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00980"></a>00980     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l00981"></a>00981       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l00982"></a>00982                      <span class="stringliteral">&quot;GetMatrix(double_dense_m)&quot;</span>,
<a name="l00983"></a>00983                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l00984"></a>00984                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00985"></a>00985                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00986"></a>00986 <span class="preprocessor">#endif</span>
<a name="l00987"></a>00987 <span class="preprocessor"></span>
<a name="l00988"></a>00988     <span class="keywordflow">if</span> (collection_(i, j) != 2)
<a name="l00989"></a>00989       {
<a name="l00990"></a>00990         <span class="keywordtype">string</span> matrix_type;
<a name="l00991"></a>00991         <span class="keywordflow">switch</span>(collection_(i, j))
<a name="l00992"></a>00992           {
<a name="l00993"></a>00993           <span class="keywordflow">case</span> 0:
<a name="l00994"></a>00994             matrix_type = <span class="stringliteral">&quot;float_dense&quot;</span>;
<a name="l00995"></a>00995             <span class="keywordflow">break</span>;
<a name="l00996"></a>00996           <span class="keywordflow">case</span> 1:
<a name="l00997"></a>00997             matrix_type = <span class="stringliteral">&quot;float_sparse&quot;</span>;
<a name="l00998"></a>00998             <span class="keywordflow">break</span>;
<a name="l00999"></a>00999           <span class="keywordflow">case</span> 3:
<a name="l01000"></a>01000             matrix_type = <span class="stringliteral">&quot;double_sparse&quot;</span>;
<a name="l01001"></a>01001             <span class="keywordflow">break</span>;
<a name="l01002"></a>01002           <span class="keywordflow">default</span>:
<a name="l01003"></a>01003             <span class="keywordflow">throw</span>
<a name="l01004"></a>01004               WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j, &quot;</span>
<a name="l01005"></a>01005                             <span class="stringliteral">&quot;Matrix&lt;Double, Dense&gt; M)&quot;</span>,
<a name="l01006"></a>01006                             <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01007"></a>01007                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01008"></a>01008           }
<a name="l01009"></a>01009 
<a name="l01010"></a>01010         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j, &quot;</span>
<a name="l01011"></a>01011                             <span class="stringliteral">&quot;Matrix&lt;Double, Dense&gt; M)&quot;</span>,
<a name="l01012"></a>01012                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Wrong type for matrix &quot;</span>)
<a name="l01013"></a>01013                             + matrix_type + <span class="stringliteral">&quot; M.&quot;</span>);
<a name="l01014"></a>01014       }
<a name="l01015"></a>01015 
<a name="l01016"></a>01016     M.SetData(Mlocal_(i), Nlocal_(j),
<a name="l01017"></a>01017               double_dense_c_.GetMatrix(i, j).GetData());
<a name="l01018"></a>01018   }
<a name="l01019"></a>01019 
<a name="l01020"></a>01020 
<a name="l01022"></a>01022 
<a name="l01028"></a>01028   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01029"></a>01029             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01030"></a>01030             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l01031"></a>01031   HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;
<a name="l01032"></a>01032   ::GetMatrix(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">typename</span> HeterogeneousMatrixCollection&lt;Prop0,
<a name="l01033"></a>01033               Storage0, Prop1, Storage1, Allocator&gt;::double_sparse_m&amp; M)<span class="keyword"></span>
<a name="l01034"></a>01034 <span class="keyword">    const</span>
<a name="l01035"></a>01035 <span class="keyword">  </span>{
<a name="l01036"></a>01036 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01037"></a>01037 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= Mmatrix_)
<a name="l01038"></a>01038       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l01039"></a>01039                      <span class="stringliteral">&quot;GetMatrix(double_sparse_m)&quot;</span>,
<a name="l01040"></a>01040                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Row index should be in [0, &quot;</span>)
<a name="l01041"></a>01041                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Mmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01042"></a>01042                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01043"></a>01043     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= Nmatrix_)
<a name="l01044"></a>01044       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::&quot;</span>
<a name="l01045"></a>01045                      <span class="stringliteral">&quot;GetMatrix(double_sparse_m)&quot;</span>,
<a name="l01046"></a>01046                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Column index should be in [0, &quot;</span>)
<a name="l01047"></a>01047                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nmatrix_ - 1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01048"></a>01048                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01049"></a>01049 <span class="preprocessor">#endif</span>
<a name="l01050"></a>01050 <span class="preprocessor"></span>
<a name="l01051"></a>01051     <span class="keywordflow">if</span> (collection_(i, j) != 3)
<a name="l01052"></a>01052       {
<a name="l01053"></a>01053         <span class="keywordtype">string</span> matrix_type;
<a name="l01054"></a>01054         <span class="keywordflow">switch</span>(collection_(i, j))
<a name="l01055"></a>01055           {
<a name="l01056"></a>01056           <span class="keywordflow">case</span> 0:
<a name="l01057"></a>01057             matrix_type = <span class="stringliteral">&quot;float_dense&quot;</span>;
<a name="l01058"></a>01058             <span class="keywordflow">break</span>;
<a name="l01059"></a>01059           <span class="keywordflow">case</span> 1:
<a name="l01060"></a>01060             matrix_type = <span class="stringliteral">&quot;float_sparse&quot;</span>;
<a name="l01061"></a>01061             <span class="keywordflow">break</span>;
<a name="l01062"></a>01062           <span class="keywordflow">case</span> 2:
<a name="l01063"></a>01063             matrix_type = <span class="stringliteral">&quot;double_dense&quot;</span>;
<a name="l01064"></a>01064             <span class="keywordflow">break</span>;
<a name="l01065"></a>01065           <span class="keywordflow">default</span>:
<a name="l01066"></a>01066             <span class="keywordflow">throw</span>
<a name="l01067"></a>01067               WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j,&quot;</span>
<a name="l01068"></a>01068                             <span class="stringliteral">&quot;Matrix&lt;Double, Sparse&gt; M)&quot;</span>,
<a name="l01069"></a>01069                             <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01070"></a>01070                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01071"></a>01071           }
<a name="l01072"></a>01072 
<a name="l01073"></a>01073         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::GetMatrix(i, j,&quot;</span>
<a name="l01074"></a>01074                             <span class="stringliteral">&quot;Matrix&lt;Double, Sparse&gt; M)&quot;</span>,
<a name="l01075"></a>01075                             <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Wrong type for matrix &quot;</span>)
<a name="l01076"></a>01076                             + matrix_type + <span class="stringliteral">&quot; M.&quot;</span>);
<a name="l01077"></a>01077       }
<a name="l01078"></a>01078 
<a name="l01079"></a>01079     M.Nullify();
<a name="l01080"></a>01080     M.SetData(double_sparse_c_.GetMatrix(i, j).GetM(),
<a name="l01081"></a>01081               double_sparse_c_.GetMatrix(i, j).GetN(),
<a name="l01082"></a>01082               double_sparse_c_.GetMatrix(i, j).GetNonZeros(),
<a name="l01083"></a>01083               double_sparse_c_.GetMatrix(i, j).GetData(),
<a name="l01084"></a>01084               double_sparse_c_.GetMatrix(i, j).GetPtr(),
<a name="l01085"></a>01085               double_sparse_c_.GetMatrix(i, j).GetInd());
<a name="l01086"></a>01086   }
<a name="l01087"></a>01087 
<a name="l01088"></a>01088 
<a name="l01090"></a>01090 
<a name="l01096"></a>01096   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01097"></a>01097             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01098"></a>01098             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l01099"></a>01099   <span class="keyword">inline</span>
<a name="l01100"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#accc5a22477245a42cd0e017157f7d17d">01100</a>   <span class="keywordtype">double</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0,
<a name="l01101"></a>01101                                        Prop1, Storage1, Allocator&gt;
<a name="l01102"></a>01102   ::operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>
<a name="l01103"></a>01103   {
<a name="l01104"></a>01104 
<a name="l01105"></a>01105 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01106"></a>01106 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;Mlocal_sum_(Mmatrix_))
<a name="l01107"></a>01107       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::operator()&quot;</span>,
<a name="l01108"></a>01108                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l01109"></a>01109                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Mlocal_sum_(Mmatrix_) - 1)
<a name="l01110"></a>01110                      + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01111"></a>01111                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01112"></a>01112     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;Nlocal_sum_(Nmatrix_))
<a name="l01113"></a>01113       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::operator()&quot;</span>,
<a name="l01114"></a>01114                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l01115"></a>01115                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;Nlocal_sum_(Nmatrix_) - 1)
<a name="l01116"></a>01116                      + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l01117"></a>01117                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l01118"></a>01118 <span class="preprocessor">#endif</span>
<a name="l01119"></a>01119 <span class="preprocessor"></span>
<a name="l01120"></a>01120     <span class="keywordtype">int</span> i_global = 0;
<a name="l01121"></a>01121     <span class="keywordflow">while</span> (i &gt;= Mlocal_sum_(i_global))
<a name="l01122"></a>01122       i_global++;
<a name="l01123"></a>01123     i_global--;
<a name="l01124"></a>01124 
<a name="l01125"></a>01125     <span class="keywordtype">int</span> j_global = 0;
<a name="l01126"></a>01126     <span class="keywordflow">while</span> (j &gt;= Nlocal_sum_(j_global))
<a name="l01127"></a>01127       j_global++;
<a name="l01128"></a>01128     j_global--;
<a name="l01129"></a>01129 
<a name="l01130"></a>01130     <span class="keywordtype">double</span> res = 0.;
<a name="l01131"></a>01131     <span class="keywordflow">switch</span>(collection_(i_global, j_global))
<a name="l01132"></a>01132       {
<a name="l01133"></a>01133       <span class="keywordflow">case</span> 0:
<a name="l01134"></a>01134         res = double(float_dense_c_.GetMatrix(i_global, j_global)
<a name="l01135"></a>01135                      (i - Mlocal_sum_(i_global), j - Nlocal_sum_(j_global)));
<a name="l01136"></a>01136         <span class="keywordflow">break</span>;
<a name="l01137"></a>01137       <span class="keywordflow">case</span> 1:
<a name="l01138"></a>01138         res = double(float_sparse_c_.GetMatrix(i_global, j_global)
<a name="l01139"></a>01139                      (i - Mlocal_sum_(i_global), j - Nlocal_sum_(j_global)));
<a name="l01140"></a>01140         <span class="keywordflow">break</span>;
<a name="l01141"></a>01141       <span class="keywordflow">case</span> 2:
<a name="l01142"></a>01142         res = double_dense_c_.GetMatrix(i_global, j_global)
<a name="l01143"></a>01143           (i - Mlocal_sum_(i_global), j - Nlocal_sum_(j_global));
<a name="l01144"></a>01144         <span class="keywordflow">break</span>;
<a name="l01145"></a>01145       <span class="keywordflow">case</span> 3:
<a name="l01146"></a>01146         res = double_sparse_c_.GetMatrix(i_global, j_global)
<a name="l01147"></a>01147           (i - Mlocal_sum_(i_global), j - Nlocal_sum_(j_global));
<a name="l01148"></a>01148         <span class="keywordflow">break</span>;
<a name="l01149"></a>01149       <span class="keywordflow">default</span>:
<a name="l01150"></a>01150         <span class="keywordflow">throw</span>
<a name="l01151"></a>01151           <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection::operator(int, int)&quot;</span>,
<a name="l01152"></a>01152                         <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01153"></a>01153                         + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01154"></a>01154       }
<a name="l01155"></a>01155     <span class="keywordflow">return</span> res;
<a name="l01156"></a>01156   }
<a name="l01157"></a>01157 
<a name="l01158"></a>01158 
<a name="l01160"></a>01160 
<a name="l01165"></a>01165   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01166"></a>01166             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01167"></a>01167             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l01168"></a>01168   <span class="keyword">inline</span>
<a name="l01169"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e">01169</a>   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>&amp;
<a name="l01170"></a>01170   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e" title="Duplicates a matrix collection (assignment operator).">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01171"></a>01171 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e" title="Duplicates a matrix collection (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0,
<a name="l01172"></a>01172                Prop1, Storage1, Allocator&gt;&amp; A)
<a name="l01173"></a>01173   {
<a name="l01174"></a>01174     this-&gt;Copy(A);
<a name="l01175"></a>01175     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01176"></a>01176   }
<a name="l01177"></a>01177 
<a name="l01178"></a>01178 
<a name="l01180"></a>01180 
<a name="l01185"></a>01185   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01186"></a>01186             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01187"></a>01187             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l01188"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf">01188</a>   <span class="keyword">inline</span> <span class="keywordtype">void</span>
<a name="l01189"></a>01189   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf" title="Duplicates a matrix collection (assignment operator).">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01190"></a>01190 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a91100b6eb7a2cea5e86d191ee58749bf" title="Duplicates a matrix collection (assignment operator).">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;Prop0, Storage0, Prop1,
<a name="l01191"></a>01191          Storage1, Allocator&gt;&amp; A)
<a name="l01192"></a>01192   {
<a name="l01193"></a>01193     Clear();
<a name="l01194"></a>01194     this-&gt;nz_ = A.nz_;
<a name="l01195"></a>01195     Mmatrix_ = A.Mmatrix_;
<a name="l01196"></a>01196     Nmatrix_ = A.Nmatrix_;
<a name="l01197"></a>01197     this-&gt;m_ = A.GetM();
<a name="l01198"></a>01198     this-&gt;n_ = A.GetN();
<a name="l01199"></a>01199 
<a name="l01200"></a>01200     this-&gt;Mlocal_ = A.Mlocal_;
<a name="l01201"></a>01201     this-&gt;Mlocal_sum_ = A.Mlocal_sum_;
<a name="l01202"></a>01202     this-&gt;Nlocal_ = A.Nlocal_;
<a name="l01203"></a>01203     this-&gt;Nlocal_sum_ = A.Nlocal_sum_;
<a name="l01204"></a>01204 
<a name="l01205"></a>01205     collection_.Copy(A.collection_);
<a name="l01206"></a>01206 
<a name="l01207"></a>01207     float_dense_c_.Reallocate(Mmatrix_, Nmatrix_);
<a name="l01208"></a>01208     float_sparse_c_.Reallocate(Mmatrix_, Nmatrix_);
<a name="l01209"></a>01209     double_dense_c_.Reallocate(Mmatrix_, Nmatrix_);
<a name="l01210"></a>01210     double_sparse_c_.Reallocate(Mmatrix_, Nmatrix_);
<a name="l01211"></a>01211 
<a name="l01212"></a>01212     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a> m0a;
<a name="l01213"></a>01213     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a> m1a;
<a name="l01214"></a>01214     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a> m2a;
<a name="l01215"></a>01215     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a> m3a;
<a name="l01216"></a>01216 
<a name="l01217"></a>01217     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mmatrix_; i++ )
<a name="l01218"></a>01218       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nmatrix_; j++)
<a name="l01219"></a>01219         {
<a name="l01220"></a>01220           <span class="keywordflow">switch</span> (A.GetType(i, j))
<a name="l01221"></a>01221             {
<a name="l01222"></a>01222             <span class="keywordflow">case</span> 0:
<a name="l01223"></a>01223               A.GetMatrix(i, j, m0a);
<a name="l01224"></a>01224               SetMatrix(i, j, m0a);
<a name="l01225"></a>01225               m0a.Nullify();
<a name="l01226"></a>01226               <span class="keywordflow">break</span>;
<a name="l01227"></a>01227             <span class="keywordflow">case</span> 1:
<a name="l01228"></a>01228               A.GetMatrix(i, j, m1a);
<a name="l01229"></a>01229               SetMatrix(i, j, m1a);
<a name="l01230"></a>01230               m1a.Nullify();
<a name="l01231"></a>01231               <span class="keywordflow">break</span>;
<a name="l01232"></a>01232             <span class="keywordflow">case</span> 2:
<a name="l01233"></a>01233               A.GetMatrix(i, j, m2a);
<a name="l01234"></a>01234               SetMatrix(i, j, m2a);
<a name="l01235"></a>01235               m2a.Nullify();
<a name="l01236"></a>01236               <span class="keywordflow">break</span>;
<a name="l01237"></a>01237             <span class="keywordflow">case</span> 3:
<a name="l01238"></a>01238               A.GetMatrix(i, j, m3a);
<a name="l01239"></a>01239               SetMatrix(i, j, m3a);
<a name="l01240"></a>01240               m3a.Nullify();
<a name="l01241"></a>01241               <span class="keywordflow">break</span>;
<a name="l01242"></a>01242             <span class="keywordflow">default</span>:
<a name="l01243"></a>01243               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01244"></a>01244                                   <span class="stringliteral">&quot;::MltAdd(alpha, A, B, beta, C) &quot;</span>,
<a name="l01245"></a>01245                                   <span class="stringliteral">&quot;Underlying matrix  C (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01246"></a>01246                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01247"></a>01247             }
<a name="l01248"></a>01248         }
<a name="l01249"></a>01249   }
<a name="l01250"></a>01250 
<a name="l01251"></a>01251 
<a name="l01252"></a>01252   <span class="comment">/************************</span>
<a name="l01253"></a>01253 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l01254"></a>01254 <span class="comment">   ************************/</span>
<a name="l01255"></a>01255 
<a name="l01256"></a>01256 
<a name="l01258"></a>01258 
<a name="l01261"></a>01261   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01262"></a>01262             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01263"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc">01263</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01264"></a>01264   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc" title="Displays the matrix collection on the standard output.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01265"></a>01265 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a788108f4822afc37c6ef4054b37a03bc" title="Displays the matrix collection on the standard output.">  ::Print</a>()<span class="keyword"> const</span>
<a name="l01266"></a>01266 <span class="keyword">  </span>{
<a name="l01267"></a>01267     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Mlocal_sum_(Mmatrix_); i++)
<a name="l01268"></a>01268       {
<a name="l01269"></a>01269         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Nlocal_sum_(Nmatrix_); j++)
<a name="l01270"></a>01270           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; endl;
<a name="l01271"></a>01271         cout &lt;&lt; endl;
<a name="l01272"></a>01272       }
<a name="l01273"></a>01273   }
<a name="l01274"></a>01274 
<a name="l01275"></a>01275 
<a name="l01277"></a>01277 
<a name="l01285"></a>01285   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01286"></a>01286             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01287"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544">01287</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01288"></a>01288   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01289"></a>01289 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l01290"></a>01290 <span class="keyword">  </span>{
<a name="l01291"></a>01291     ofstream FileStream;
<a name="l01292"></a>01292     FileStream.open(FileName.c_str());
<a name="l01293"></a>01293 
<a name="l01294"></a>01294 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01295"></a>01295 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01296"></a>01296     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01297"></a>01297       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(string FileName)&quot;</span>,
<a name="l01298"></a>01298                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01299"></a>01299 <span class="preprocessor">#endif</span>
<a name="l01300"></a>01300 <span class="preprocessor"></span>
<a name="l01301"></a>01301     this-&gt;Write(FileStream, with_size);
<a name="l01302"></a>01302 
<a name="l01303"></a>01303     FileStream.close();
<a name="l01304"></a>01304   }
<a name="l01305"></a>01305 
<a name="l01306"></a>01306 
<a name="l01308"></a>01308 
<a name="l01316"></a>01316   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01317"></a>01317             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01318"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0c289b98c4bd4ea56dab00a13b88761b">01318</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01319"></a>01319   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01320"></a>01320 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3d95d8a80c5fdd1d5aea9decf6d4e544" title="Writes the matrix collection in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size = <span class="keyword">true</span>)<span class="keyword"> const</span>
<a name="l01321"></a>01321 <span class="keyword">  </span>{
<a name="l01322"></a>01322 
<a name="l01323"></a>01323 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01324"></a>01324 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01325"></a>01325     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01326"></a>01326       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01327"></a>01327                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l01328"></a>01328                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01329"></a>01329 <span class="preprocessor">#endif</span>
<a name="l01330"></a>01330 <span class="preprocessor"></span>
<a name="l01331"></a>01331     <span class="keywordflow">if</span> (with_size)
<a name="l01332"></a>01332       {
<a name="l01333"></a>01333         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;Mmatrix_)),
<a name="l01334"></a>01334                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01335"></a>01335         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;Nmatrix_)),
<a name="l01336"></a>01336                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01337"></a>01337       }
<a name="l01338"></a>01338 
<a name="l01339"></a>01339     collection_.Write(FileStream, with_size);
<a name="l01340"></a>01340 
<a name="l01341"></a>01341     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a> m0a;
<a name="l01342"></a>01342     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a> m1a;
<a name="l01343"></a>01343     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a> m2a;
<a name="l01344"></a>01344     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a> m3a;
<a name="l01345"></a>01345 
<a name="l01346"></a>01346     <span class="keywordtype">int</span> i, j;
<a name="l01347"></a>01347     <span class="keywordflow">for</span> (i = 0; i &lt; Mmatrix_; i++)
<a name="l01348"></a>01348       <span class="keywordflow">for</span> (j = 0; j &lt; Nmatrix_; j++)
<a name="l01349"></a>01349         {
<a name="l01350"></a>01350           <span class="keywordflow">switch</span> (GetType(i, j))
<a name="l01351"></a>01351             {
<a name="l01352"></a>01352             <span class="keywordflow">case</span> 0:
<a name="l01353"></a>01353               GetMatrix(i, j, m0a);
<a name="l01354"></a>01354               m0a.Write(FileStream, with_size);
<a name="l01355"></a>01355               m0a.Nullify();
<a name="l01356"></a>01356               <span class="keywordflow">break</span>;
<a name="l01357"></a>01357             <span class="keywordflow">case</span> 1:
<a name="l01358"></a>01358               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01359"></a>01359                               <span class="stringliteral">&quot;Storage0, Prop1, Storage1, Allocator&gt;&quot;</span>
<a name="l01360"></a>01360                               <span class="stringliteral">&quot;::Write(ostream&amp; FileStream, bool &quot;</span>
<a name="l01361"></a>01361                               <span class="stringliteral">&quot;with_size = true) &quot;</span>);
<a name="l01362"></a>01362             <span class="keywordflow">case</span> 2:
<a name="l01363"></a>01363               GetMatrix(i, j, m2a);
<a name="l01364"></a>01364               m2a.Write(FileStream, with_size);
<a name="l01365"></a>01365               m2a.Nullify();
<a name="l01366"></a>01366               <span class="keywordflow">break</span>;
<a name="l01367"></a>01367             <span class="keywordflow">case</span> 3:
<a name="l01368"></a>01368               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01369"></a>01369                               <span class="stringliteral">&quot;Storage0, Prop1, Storage1, Allocator&gt;&quot;</span>
<a name="l01370"></a>01370                               <span class="stringliteral">&quot;::Write(ostream&amp; FileStream, bool &quot;</span>
<a name="l01371"></a>01371                               <span class="stringliteral">&quot;with_size = true) &quot;</span>);
<a name="l01372"></a>01372             <span class="keywordflow">default</span>:
<a name="l01373"></a>01373               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01374"></a>01374                                   <span class="stringliteral">&quot;::Write(ostream&amp; FileStream, &quot;</span>
<a name="l01375"></a>01375                                   <span class="stringliteral">&quot;bool with_size = true) &quot;</span>,
<a name="l01376"></a>01376                                   <span class="stringliteral">&quot;Underlying matrix  A (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01377"></a>01377                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01378"></a>01378             }
<a name="l01379"></a>01379         }
<a name="l01380"></a>01380 
<a name="l01381"></a>01381 
<a name="l01382"></a>01382 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01383"></a>01383 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01384"></a>01384     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01385"></a>01385       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01386"></a>01386                     <span class="stringliteral">&quot;::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l01387"></a>01387                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01388"></a>01388 <span class="preprocessor">#endif</span>
<a name="l01389"></a>01389 <span class="preprocessor"></span>
<a name="l01390"></a>01390   }
<a name="l01391"></a>01391 
<a name="l01392"></a>01392 
<a name="l01394"></a>01394 
<a name="l01399"></a>01399   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01400"></a>01400             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01401"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae">01401</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01402"></a>01402   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01403"></a>01403 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01404"></a>01404 <span class="keyword">  </span>{
<a name="l01405"></a>01405     ofstream FileStream;
<a name="l01406"></a>01406     FileStream.precision(cout.precision());
<a name="l01407"></a>01407     FileStream.flags(cout.flags());
<a name="l01408"></a>01408     FileStream.open(FileName.c_str());
<a name="l01409"></a>01409 
<a name="l01410"></a>01410 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01411"></a>01411 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01412"></a>01412     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01413"></a>01413       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01414"></a>01414                     <span class="stringliteral">&quot;::WriteText(string FileName)&quot;</span>,
<a name="l01415"></a>01415                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01416"></a>01416 <span class="preprocessor">#endif</span>
<a name="l01417"></a>01417 <span class="preprocessor"></span>
<a name="l01418"></a>01418     this-&gt;WriteText(FileStream);
<a name="l01419"></a>01419 
<a name="l01420"></a>01420     FileStream.close();
<a name="l01421"></a>01421   }
<a name="l01422"></a>01422 
<a name="l01423"></a>01423 
<a name="l01425"></a>01425 
<a name="l01431"></a>01431   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01432"></a>01432             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01433"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a85ddf0c1055330e18f77175da318432e">01433</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01434"></a>01434   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01435"></a>01435 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#ac56a7ef93f6f0a217795ce3166b9edae" title="Writes the matrix collection in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01436"></a>01436 <span class="keyword">  </span>{
<a name="l01437"></a>01437 
<a name="l01438"></a>01438 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01439"></a>01439 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l01440"></a>01440     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01441"></a>01441       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01442"></a>01442                     <span class="stringliteral">&quot;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l01443"></a>01443                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01444"></a>01444 <span class="preprocessor">#endif</span>
<a name="l01445"></a>01445 <span class="preprocessor"></span>
<a name="l01446"></a>01446     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a> m0a;
<a name="l01447"></a>01447     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a> m1a;
<a name="l01448"></a>01448     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a> m2a;
<a name="l01449"></a>01449     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a> m3a;
<a name="l01450"></a>01450 
<a name="l01451"></a>01451     <span class="keywordtype">int</span> i, j;
<a name="l01452"></a>01452     <span class="keywordflow">for</span> (i = 0; i &lt; Mmatrix_; i++)
<a name="l01453"></a>01453       <span class="keywordflow">for</span> (j = 0; j &lt; Nmatrix_; j++)
<a name="l01454"></a>01454         {
<a name="l01455"></a>01455           <span class="keywordflow">switch</span> (GetType(i, j))
<a name="l01456"></a>01456             {
<a name="l01457"></a>01457             <span class="keywordflow">case</span> 0:
<a name="l01458"></a>01458               GetMatrix(i, j, m0a);
<a name="l01459"></a>01459               m0a.WriteText(FileStream);
<a name="l01460"></a>01460               m0a.Nullify();
<a name="l01461"></a>01461               <span class="keywordflow">break</span>;
<a name="l01462"></a>01462             <span class="keywordflow">case</span> 1:
<a name="l01463"></a>01463               GetMatrix(i, j, m1a);
<a name="l01464"></a>01464               m1a.WriteText(FileStream);
<a name="l01465"></a>01465               m1a.Nullify();
<a name="l01466"></a>01466               <span class="keywordflow">break</span>;
<a name="l01467"></a>01467             <span class="keywordflow">case</span> 2:
<a name="l01468"></a>01468               GetMatrix(i, j, m2a);
<a name="l01469"></a>01469               m2a.WriteText(FileStream);
<a name="l01470"></a>01470               m2a.Nullify();
<a name="l01471"></a>01471               <span class="keywordflow">break</span>;
<a name="l01472"></a>01472             <span class="keywordflow">case</span> 3:
<a name="l01473"></a>01473               GetMatrix(i, j, m3a);
<a name="l01474"></a>01474               m3a.WriteText(FileStream);
<a name="l01475"></a>01475               m3a.Nullify();
<a name="l01476"></a>01476               <span class="keywordflow">break</span>;
<a name="l01477"></a>01477             <span class="keywordflow">default</span>:
<a name="l01478"></a>01478               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01479"></a>01479                                   <span class="stringliteral">&quot;::Write(ostream&amp; FileStream, &quot;</span>
<a name="l01480"></a>01480                                   <span class="stringliteral">&quot;bool with_size = true) &quot;</span>,
<a name="l01481"></a>01481                                   <span class="stringliteral">&quot;Underlying matrix  A (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01482"></a>01482                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01483"></a>01483             }
<a name="l01484"></a>01484           FileStream &lt;&lt; endl;
<a name="l01485"></a>01485         }
<a name="l01486"></a>01486 
<a name="l01487"></a>01487 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01488"></a>01488 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l01489"></a>01489     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01490"></a>01490       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01491"></a>01491                     <span class="stringliteral">&quot;::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l01492"></a>01492                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01493"></a>01493 <span class="preprocessor">#endif</span>
<a name="l01494"></a>01494 <span class="preprocessor"></span>
<a name="l01495"></a>01495   }
<a name="l01496"></a>01496 
<a name="l01497"></a>01497 
<a name="l01499"></a>01499 
<a name="l01505"></a>01505   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01506"></a>01506             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01507"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24">01507</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01508"></a>01508   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01509"></a>01509 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l01510"></a>01510   {
<a name="l01511"></a>01511     ifstream FileStream;
<a name="l01512"></a>01512     FileStream.open(FileName.c_str());
<a name="l01513"></a>01513 
<a name="l01514"></a>01514 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01515"></a>01515 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01516"></a>01516     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01517"></a>01517       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1,&quot;</span>
<a name="l01518"></a>01518                     <span class="stringliteral">&quot; Storage1, Allocator&gt;::Read(string FileName)&quot;</span>,
<a name="l01519"></a>01519                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01520"></a>01520 <span class="preprocessor">#endif</span>
<a name="l01521"></a>01521 <span class="preprocessor"></span>
<a name="l01522"></a>01522     this-&gt;Read(FileStream);
<a name="l01523"></a>01523 
<a name="l01524"></a>01524     FileStream.close();
<a name="l01525"></a>01525   }
<a name="l01526"></a>01526 
<a name="l01527"></a>01527 
<a name="l01529"></a>01529 
<a name="l01535"></a>01535   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0,
<a name="l01536"></a>01536             <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1,
<a name="l01537"></a><a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a2c15862a4b7347bff455204fab3ea161">01537</a>             <span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt; <span class="keywordtype">void</span>
<a name="l01538"></a>01538   <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1, Storage1, Allocator&gt;</a>
<a name="l01539"></a>01539 <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a11761a3a59b566c4e2d4ae2e020d1b24" title="Reads the matrix collection from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l01540"></a>01540   {
<a name="l01541"></a>01541 
<a name="l01542"></a>01542 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01543"></a>01543 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01544"></a>01544     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01545"></a>01545       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&lt;Prop0, Storage0, Prop1,&quot;</span>
<a name="l01546"></a>01546                     <span class="stringliteral">&quot; Storage1, Allocator&gt;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01547"></a>01547                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01548"></a>01548 <span class="preprocessor">#endif</span>
<a name="l01549"></a>01549 <span class="preprocessor"></span>
<a name="l01550"></a>01550     <span class="keywordtype">int</span> *new_m, *new_n;
<a name="l01551"></a>01551     new_m = <span class="keyword">new</span> int;
<a name="l01552"></a>01552     new_n = <span class="keyword">new</span> int;
<a name="l01553"></a>01553 
<a name="l01554"></a>01554     FileStream.read(reinterpret_cast&lt;char*&gt;(new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01555"></a>01555     FileStream.read(reinterpret_cast&lt;char*&gt;(new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01556"></a>01556 
<a name="l01557"></a>01557     this-&gt;Reallocate(*new_m, *new_n);
<a name="l01558"></a>01558 
<a name="l01559"></a>01559     collection_.Read(FileStream);
<a name="l01560"></a>01560 
<a name="l01561"></a>01561     <a class="code" href="class_seldon_1_1_matrix.php">float_dense_m</a> m0a;
<a name="l01562"></a>01562     <a class="code" href="class_seldon_1_1_matrix.php">float_sparse_m</a> m1a;
<a name="l01563"></a>01563     <a class="code" href="class_seldon_1_1_matrix.php">double_dense_m</a> m2a;
<a name="l01564"></a>01564     <a class="code" href="class_seldon_1_1_matrix.php">double_sparse_m</a> m3a;
<a name="l01565"></a>01565     <span class="keywordtype">int</span> i, j;
<a name="l01566"></a>01566     <span class="keywordflow">for</span> (i = 0; i &lt; Mmatrix_; i++)
<a name="l01567"></a>01567       <span class="keywordflow">for</span> (j = 0; j &lt; Nmatrix_; j++)
<a name="l01568"></a>01568         {
<a name="l01569"></a>01569           <span class="keywordflow">switch</span> (GetType(i, j))
<a name="l01570"></a>01570             {
<a name="l01571"></a>01571             <span class="keywordflow">case</span> 0:
<a name="l01572"></a>01572               m0a.Read(FileStream);
<a name="l01573"></a>01573               SetMatrix(i, j, m0a);
<a name="l01574"></a>01574               m0a.Nullify();
<a name="l01575"></a>01575               <span class="keywordflow">break</span>;
<a name="l01576"></a>01576             <span class="keywordflow">case</span> 1:
<a name="l01577"></a>01577               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01578"></a>01578                               <span class="stringliteral">&quot;Storage0, Prop1, Storage1, Allocator&gt;&quot;</span>
<a name="l01579"></a>01579                               <span class="stringliteral">&quot;::Read(istream&amp; FileStream)&quot;</span>);
<a name="l01580"></a>01580             <span class="keywordflow">case</span> 2:
<a name="l01581"></a>01581               m2a.Read(FileStream);
<a name="l01582"></a>01582               SetMatrix(i, j, m2a);
<a name="l01583"></a>01583               m2a.Nullify();
<a name="l01584"></a>01584               <span class="keywordflow">break</span>;
<a name="l01585"></a>01585             <span class="keywordflow">case</span> 3:
<a name="l01586"></a>01586               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_undefined.php">Undefined</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l01587"></a>01587                               <span class="stringliteral">&quot;Storage0, Prop1, Storage1, Allocator&gt;&quot;</span>
<a name="l01588"></a>01588                               <span class="stringliteral">&quot;::Read(istream&amp; FileStream)&quot;</span>);
<a name="l01589"></a>01589               <span class="keywordflow">break</span>;
<a name="l01590"></a>01590             <span class="keywordflow">default</span>:
<a name="l01591"></a>01591               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&lt;Prop0, &quot;</span>
<a name="l01592"></a>01592                                   <span class="stringliteral">&quot;Storage0, Prop1, Storage1, Allocator&gt;&quot;</span>
<a name="l01593"></a>01593                                   <span class="stringliteral">&quot;::Read(istream&amp; FileStream) &quot;</span>,
<a name="l01594"></a>01594                                   <span class="stringliteral">&quot;Underlying matrix  A (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01595"></a>01595                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01596"></a>01596             }
<a name="l01597"></a>01597         }
<a name="l01598"></a>01598 
<a name="l01599"></a>01599 
<a name="l01600"></a>01600     <span class="keyword">delete</span> new_n;
<a name="l01601"></a>01601     <span class="keyword">delete</span> new_m;
<a name="l01602"></a>01602 
<a name="l01603"></a>01603 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01604"></a>01604 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01605"></a>01605     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01606"></a>01606       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;HeterogeneousMatrixCollection&quot;</span>
<a name="l01607"></a>01607                     <span class="stringliteral">&quot;::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01608"></a>01608                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l01609"></a>01609 <span class="preprocessor">#endif</span>
<a name="l01610"></a>01610 <span class="preprocessor"></span>
<a name="l01611"></a>01611   }
<a name="l01612"></a>01612 
<a name="l01613"></a>01613 
<a name="l01614"></a>01614   <span class="comment">/****************</span>
<a name="l01615"></a>01615 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01616"></a>01616 <span class="comment">   ****************/</span>
<a name="l01617"></a>01617 
<a name="l01618"></a>01618 
<a name="l01620"></a>01620 
<a name="l01623"></a>01623   <span class="keyword">template</span> &lt;<span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l01624"></a><a class="code" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a20314db5f8dd0fcf4dd175e4b1a4852f">01624</a>   <span class="keyword">inline</span>
<a name="l01625"></a>01625   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&lt;double&gt;</a> &gt;<a class="code" href="class_seldon_1_1_matrix.php"></a>
<a name="l01626"></a>01626 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>():
<a name="l01627"></a>01627     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;<a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>,
<a name="l01628"></a>01628                                   <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;()
<a name="l01629"></a>01629   {
<a name="l01630"></a>01630   }
<a name="l01631"></a>01631 
<a name="l01632"></a>01632 
<a name="l01634"></a>01634 
<a name="l01638"></a>01638   <span class="keyword">template</span> &lt;<span class="keyword">template</span> &lt;<span class="keyword">class</span> U&gt; <span class="keyword">class </span>Allocator&gt;
<a name="l01639"></a><a class="code" href="class_seldon_1_1_matrix_3_01_float_double_00_01_general_00_01_dense_sparse_collection_00_01_allocator_3_01double_01_4_01_4.php#a80ce2837902243f345157a46095ac8ba">01639</a>   <span class="keyword">inline</span>
<a name="l01640"></a>01640   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&lt;double&gt;</a> &gt;<a class="code" href="class_seldon_1_1_matrix.php"></a>
<a name="l01641"></a>01641 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01642"></a>01642     <a class="code" href="class_seldon_1_1_heterogeneous_matrix_collection.php" title="Matrix class made of an heterogeneous collection of matrices.">HeterogeneousMatrixCollection</a>&lt;<a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>,
<a name="l01643"></a>01643                                   <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j)
<a name="l01644"></a>01644   {
<a name="l01645"></a>01645   }
<a name="l01646"></a>01646 
<a name="l01647"></a>01647 
<a name="l01648"></a>01648 } <span class="comment">// namespace Seldon.</span>
<a name="l01649"></a>01649 
<a name="l01650"></a>01650 <span class="preprocessor">#define SELDON_FILE_MATRIX_HETEROGENEOUS_COLLECTION_CXX</span>
<a name="l01651"></a>01651 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
