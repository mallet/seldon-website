<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>doc/guide/header.php</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 &lt;?php echo <span class="stringliteral">&#39;&lt;?xml version=&quot;1.0&quot;  encoding=&quot;iso-8859-1&quot;?&#39;</span>.<span class="charliteral">&#39;&gt;&#39;</span> ?&gt;
<a name="l00002"></a>00002 &lt;!DOCTYPE html PUBLIC <span class="stringliteral">&quot;-//W3C//DTD XHTML 1.0 Transitional//EN&quot;</span>
<a name="l00003"></a>00003 <span class="stringliteral">&quot;http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd&quot;</span>&gt;
<a name="l00004"></a>00004 
<a name="l00005"></a>00005 &lt;html xmlns=<span class="stringliteral">&quot;http://www.w3.org/1999/xhtml&quot;</span> xml:lang=<span class="stringliteral">&quot;en&quot;</span> lang=<span class="stringliteral">&quot;en&quot;</span>&gt;
<a name="l00006"></a>00006 
<a name="l00007"></a>00007 &lt;?php $root=<span class="stringliteral">&#39;..&#39;</span>;?&gt;
<a name="l00008"></a>00008 
<a name="l00009"></a>00009 &lt;head&gt;
<a name="l00010"></a>00010 &lt;meta http-equiv=<span class="stringliteral">&quot;Content-Type&quot;</span> content=<span class="stringliteral">&quot;text/html;charset=UTF-8&quot;</span>&gt;
<a name="l00011"></a>00011 &lt;title&gt;Seldon user<span class="stringliteral">&#39;s guide&lt;/title&gt;</span>
<a name="l00012"></a>00012 <span class="stringliteral">&lt;link rel=&quot;stylesheet&quot; type=&quot;text/css&quot; href=&quot;&lt;?php echo $root?&gt;/content.css&quot;&gt;</span>
<a name="l00013"></a>00013 <span class="stringliteral">&lt;link rel=&quot;stylesheet&quot; href=&quot;tabs.css&quot; type=&quot;text/css&quot;&gt;</span>
<a name="l00014"></a>00014 <span class="stringliteral">&lt;link rel=&quot;stylesheet&quot; href=&quot;guide.css&quot; type=&quot;text/css&quot;&gt;</span>
<a name="l00015"></a>00015 <span class="stringliteral">&lt;?php if (file_exists($root.&#39;</span>/prettify.js<span class="stringliteral">&#39;))</span>
<a name="l00016"></a>00016 <span class="stringliteral">  echo &#39;</span>&lt;script type=<span class="stringliteral">&quot;text/javascript&quot;</span> src=<span class="stringliteral">&quot;&#39;.$root.&#39;/prettify.js&quot;</span>&gt;&lt;/script&gt;<span class="stringliteral">&#39;;</span>
<a name="l00017"></a>00017 <span class="stringliteral">else if (file_exists(&#39;</span>prettify.js<span class="stringliteral">&#39;))</span>
<a name="l00018"></a>00018 <span class="stringliteral">  echo &#39;</span>&lt;script type=<span class="stringliteral">&quot;text/javascript&quot;</span> src=<span class="stringliteral">&quot;prettify.js&quot;</span>&gt;&lt;/script&gt;<span class="stringliteral">&#39;; ?&gt;</span>
<a name="l00019"></a>00019 <span class="stringliteral">&lt;/head&gt;</span>
<a name="l00020"></a>00020 <span class="stringliteral"></span>
<a name="l00021"></a>00021 <span class="stringliteral">&lt;body onload=&quot;prettyPrint()&quot;&gt;</span>
<a name="l00022"></a>00022 <span class="stringliteral"></span>
<a name="l00023"></a>00023 <span class="stringliteral">&lt;div class=&quot;page&quot;&gt;</span>
<a name="l00024"></a>00024 <span class="stringliteral"></span>
<a name="l00025"></a>00025 <span class="stringliteral">&lt;?php if (file_exists($root.&#39;</span>/header.php<span class="stringliteral">&#39;))</span>
<a name="l00026"></a>00026 <span class="stringliteral">      include $root.&#39;</span>/header.php<span class="stringliteral">&#39;; ?&gt;</span>
<a name="l00027"></a>00027 <span class="stringliteral"></span>
<a name="l00028"></a>00028 <span class="stringliteral">&lt;div class=&quot;doc&quot;&gt;</span>
<a name="l00029"></a>00029 <span class="stringliteral"></span>
<a name="l00030"></a>00030 <span class="stringliteral">&lt;?php function HL($file_, $section_, $string_)</span>
<a name="l00031"></a>00031 <span class="stringliteral">{</span>
<a name="l00032"></a>00032 <span class="stringliteral">if ($file_ == $section_)</span>
<a name="l00033"></a>00033 <span class="stringliteral">  echo &#39;</span>&lt;em&gt;<span class="stringliteral">&#39;.$string_.&#39;</span> &lt;/em&gt;<span class="stringliteral">&#39;;</span>
<a name="l00034"></a>00034 <span class="stringliteral">else</span>
<a name="l00035"></a>00035 <span class="stringliteral">  echo &#39;</span>&lt;a href=<span class="stringliteral">&quot;&#39;.$section_.&#39;.php&quot;</span>&gt;<span class="stringliteral">&#39;.$string_.&#39;</span>&lt;/a&gt;<span class="stringliteral">&#39;;</span>
<a name="l00036"></a>00036 <span class="stringliteral">}; ?&gt;</span>
<a name="l00037"></a>00037 <span class="stringliteral"></span>
<a name="l00038"></a>00038 <span class="stringliteral">&lt;?php $file=basename($_SERVER[&#39;</span>REQUEST_URI<span class="stringliteral">&#39;], &quot;.php&quot;); $file = explode(&quot;.&quot;, $file); $file = $file[0];?&gt;</span>
<a name="l00039"></a>00039 <span class="stringliteral"></span>
<a name="l00040"></a>00040 <span class="stringliteral">&lt;div class=&quot;nav&quot;&gt;</span>
<a name="l00041"></a>00041 <span class="stringliteral"></span>
<a name="l00042"></a>00042 <span class="stringliteral">&lt;ul&gt;</span>
<a name="l00043"></a>00043 <span class="stringliteral">&lt;li class=&quot;jelly&quot;&gt; &lt;b&gt;USER&#39;</span>S GUIDE&lt;/b&gt; &lt;/li&gt;
<a name="l00044"></a>00044 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;installation&quot;</span>, <span class="stringliteral">&quot;Installation&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00045"></a>00045 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;overview&quot;</span>, <span class="stringliteral">&quot;Overview&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00046"></a>00046 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;vectors&quot;</span>, <span class="stringliteral">&quot;Vectors&quot;</span>);?&gt;
<a name="l00047"></a>00047 
<a name="l00048"></a>00048 &lt;?php <span class="keywordflow">if</span> (basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;vectors&quot;</span>
<a name="l00049"></a>00049 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;class_vector&quot;</span>
<a name="l00050"></a>00050 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;class_sparse_vector&quot;</span>
<a name="l00051"></a>00051 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;functions_vector&quot;</span>)
<a name="l00052"></a>00052 {
<a name="l00053"></a>00053   echo <span class="stringliteral">&#39;&lt;ul class=&quot;navsubul&quot;&gt; &lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00054"></a>00054   HL($file, <span class="stringliteral">&quot;class_vector&quot;</span>, <span class="stringliteral">&quot;Dense Vectors&quot;</span>);
<a name="l00055"></a>00055   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00056"></a>00056   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00057"></a>00057   HL($file, <span class="stringliteral">&quot;class_sparse_vector&quot;</span>, <span class="stringliteral">&quot;Sparse Vectors&quot;</span>);
<a name="l00058"></a>00058   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00059"></a>00059   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00060"></a>00060   HL($file, <span class="stringliteral">&quot;functions_vector&quot;</span>, <span class="stringliteral">&quot;Functions&quot;</span>);
<a name="l00061"></a>00061   echo <span class="stringliteral">&#39;&lt;/li&gt; &lt;/ul&gt;&#39;</span>;
<a name="l00062"></a>00062 } ?&gt;
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 &lt;/li&gt;
<a name="l00065"></a>00065 
<a name="l00066"></a>00066 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;matrices&quot;</span>, <span class="stringliteral">&quot;Matrices&quot;</span>);?&gt;
<a name="l00067"></a>00067 
<a name="l00068"></a>00068 &lt;?php <span class="keywordflow">if</span> (basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;matrices&quot;</span>
<a name="l00069"></a>00069 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;class_matrix&quot;</span>
<a name="l00070"></a>00070 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;class_sparse_matrix&quot;</span>
<a name="l00071"></a>00071 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;functions_matrix&quot;</span>
<a name="l00072"></a>00072 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;submatrix&quot;</span>
<a name="l00073"></a>00073 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;matrix_miscellaneous&quot;</span>)
<a name="l00074"></a>00074 {
<a name="l00075"></a>00075   echo <span class="stringliteral">&#39;&lt;ul class=&quot;navsubul&quot;&gt; &lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00076"></a>00076   HL($file, <span class="stringliteral">&quot;class_matrix&quot;</span>, <span class="stringliteral">&quot;Dense Matrices&quot;</span>);
<a name="l00077"></a>00077   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00078"></a>00078   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00079"></a>00079   HL($file, <span class="stringliteral">&quot;class_sparse_matrix&quot;</span>, <span class="stringliteral">&quot;Sparse Matrices&quot;</span>);
<a name="l00080"></a>00080   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00081"></a>00081   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00082"></a>00082   HL($file, <span class="stringliteral">&quot;functions_matrix&quot;</span>, <span class="stringliteral">&quot;Functions&quot;</span>);
<a name="l00083"></a>00083   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00084"></a>00084   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00085"></a>00085   HL($file, <span class="stringliteral">&quot;submatrix&quot;</span>, <span class="stringliteral">&quot;Sub-Matrices&quot;</span>);
<a name="l00086"></a>00086   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00087"></a>00087   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00088"></a>00088   HL($file, <span class="stringliteral">&quot;matrix_miscellaneous&quot;</span>, <span class="stringliteral">&quot;Miscellaneous&quot;</span>);
<a name="l00089"></a>00089   echo <span class="stringliteral">&#39;&lt;/li&gt; &lt;/ul&gt;&#39;</span>;
<a name="l00090"></a>00090 } ?&gt;
<a name="l00091"></a>00091 
<a name="l00092"></a>00092 &lt;/li&gt;
<a name="l00093"></a>00093 
<a name="l00094"></a>00094 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;other_structures&quot;</span>, <span class="stringliteral">&quot;Other Structures&quot;</span>);?&gt;
<a name="l00095"></a>00095 &lt;?php <span class="keywordflow">if</span> (basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;other_structures&quot;</span>
<a name="l00096"></a>00096 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;vector2&quot;</span>
<a name="l00097"></a>00097 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;array3d&quot;</span>)
<a name="l00098"></a>00098 {
<a name="l00099"></a>00099   echo <span class="stringliteral">&#39;&lt;ul class=&quot;navsubul&quot;&gt; &lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00100"></a>00100   HL($file, <span class="stringliteral">&quot;vector2&quot;</span>, <span class="stringliteral">&quot;Vector2&quot;</span>);
<a name="l00101"></a>00101   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00102"></a>00102   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00103"></a>00103   HL($file, <span class="stringliteral">&quot;array3d&quot;</span>, <span class="stringliteral">&quot;3D&amp;nbsp;Array&quot;</span>);
<a name="l00104"></a>00104   echo <span class="stringliteral">&#39;&lt;/li&gt; &lt;/ul&gt;&#39;</span>;
<a name="l00105"></a>00105 } ?&gt;
<a name="l00106"></a>00106 &lt;/li&gt;
<a name="l00107"></a>00107 
<a name="l00108"></a>00108 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;allocators&quot;</span>, <span class="stringliteral">&quot;Allocators&quot;</span>);?&gt;  &lt;/li&gt;
<a name="l00109"></a>00109 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;exceptions&quot;</span>, <span class="stringliteral">&quot;Exceptions&quot;</span>);?&gt;  &lt;/li&gt;
<a name="l00110"></a>00110 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;computations&quot;</span>, <span class="stringliteral">&quot;Computations&quot;</span>);?&gt;
<a name="l00111"></a>00111 
<a name="l00112"></a>00112 &lt;?php <span class="keywordflow">if</span> (basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;computations&quot;</span>
<a name="l00113"></a>00113 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;functions_blas&quot;</span>
<a name="l00114"></a>00114 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;functions_lapack&quot;</span>
<a name="l00115"></a>00115 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;direct&quot;</span>
<a name="l00116"></a>00116 or basename($_SERVER[<span class="stringliteral">&#39;REQUEST_URI&#39;</span>], <span class="stringliteral">&quot;.php&quot;</span>) == <span class="stringliteral">&quot;iterative&quot;</span>)
<a name="l00117"></a>00117 {
<a name="l00118"></a>00118   echo <span class="stringliteral">&#39;&lt;ul class=&quot;navsubul&quot;&gt; &lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00119"></a>00119   HL($file, <span class="stringliteral">&quot;functions_blas&quot;</span>, <span class="stringliteral">&quot;Blas&quot;</span>);
<a name="l00120"></a>00120   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00121"></a>00121   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00122"></a>00122   HL($file, <span class="stringliteral">&quot;functions_lapack&quot;</span>, <span class="stringliteral">&quot;Lapack&quot;</span>);
<a name="l00123"></a>00123   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00124"></a>00124   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00125"></a>00125   HL($file, <span class="stringliteral">&quot;direct&quot;</span>, <span class="stringliteral">&quot;Direct Solvers&quot;</span>);
<a name="l00126"></a>00126   echo <span class="stringliteral">&#39;&lt;/li&gt;&#39;</span>;
<a name="l00127"></a>00127   echo <span class="stringliteral">&#39;&lt;li class=&quot;jelly&quot;&gt;&#39;</span>;
<a name="l00128"></a>00128   HL($file, <span class="stringliteral">&quot;iterative&quot;</span>, <span class="stringliteral">&quot;Iterative Solvers&quot;</span>);
<a name="l00129"></a>00129   echo <span class="stringliteral">&#39;&lt;/li&gt; &lt;/ul&gt;&#39;</span>;
<a name="l00130"></a>00130 } ?&gt;
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 &lt;/li&gt;
<a name="l00133"></a>00133 
<a name="l00134"></a>00134 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;python&quot;</span>, <span class="stringliteral">&quot;Python Interface&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00135"></a>00135 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;glossary&quot;</span>, <span class="stringliteral">&quot;Index&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00136"></a>00136 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;b&gt;API REFERENCE&lt;/b&gt; &lt;/li&gt;
<a name="l00137"></a>00137 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;annotated&quot;</span>, <span class="stringliteral">&quot;Classes&quot;</span>);?&gt;
<a name="l00138"></a>00138 &lt;ul <span class="keyword">class</span>=<span class="stringliteral">&quot;navsubul&quot;</span>&gt; &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;annotated&quot;</span>, <span class="stringliteral">&quot;Class List&quot;</span>);?&gt; &lt;/li&gt; 
<a name="l00139"></a>00139 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;hierarchy&quot;</span>, <span class="stringliteral">&quot;Class Hierarchy&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00140"></a>00140 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;functions&quot;</span>, <span class="stringliteral">&quot;Class Members&quot;</span>);?&gt;
<a name="l00141"></a>00141 &lt;/li&gt; &lt;/ul&gt; &lt;/li&gt;
<a name="l00142"></a>00142 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;namespacemembers&quot;</span>, <span class="stringliteral">&quot;Functions&quot;</span>);?&gt; &lt;/li&gt;
<a name="l00143"></a>00143 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; Search <span class="keywordflow">for</span> &lt;form action=<span class="stringliteral">&quot;search.php&quot;</span> method=<span class="stringliteral">&quot;get&quot;</span>&gt;
<a name="l00144"></a>00144     &lt;input <span class="keyword">class</span>=<span class="stringliteral">&quot;search&quot;</span> type=<span class="stringliteral">&quot;text&quot;</span> name=<span class="stringliteral">&quot;query&quot;</span> value=<span class="stringliteral">&quot;&quot;</span> size=<span class="stringliteral">&quot;20&quot;</span> accesskey=<span class="stringliteral">&quot;s&quot;</span>&gt;
<a name="l00145"></a>00145   &lt;/form&gt;
<a name="l00146"></a>00146 &lt;/li&gt;
<a name="l00147"></a>00147 &lt;!-- &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;?php HL($file, <span class="stringliteral">&quot;faq&quot;</span>, <span class="stringliteral">&quot;F.A.Q.&quot;</span>);?&gt; &lt;/li&gt;--&gt;
<a name="l00148"></a>00148 &lt;li <span class="keyword">class</span>=<span class="stringliteral">&quot;jelly&quot;</span>&gt; &lt;a
<a name="l00149"></a>00149 href=<span class="stringliteral">&quot;mailto:seldon-help@lists.sourceforge.net&quot;</span>
<a name="l00150"></a>00150 style=<span class="stringliteral">&quot;color:black&quot;</span>&gt;Support&lt;/a&gt;&lt;/li&gt;
<a name="l00151"></a>00151 &lt;/ul&gt;
<a name="l00152"></a>00152 
<a name="l00153"></a>00153 &lt;/div&gt;
<a name="l00154"></a>00154 
<a name="l00155"></a>00155 &lt;div <span class="keyword">class</span>=<span class="stringliteral">&quot;doxygen&quot;</span>&gt;
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
