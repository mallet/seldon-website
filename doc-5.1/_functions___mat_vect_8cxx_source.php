<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_MatVect.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_MATVECT_CXX</span>
<a name="l00021"></a>00021 <span class="preprocessor"></span>
<a name="l00022"></a>00022 <span class="comment">/*</span>
<a name="l00023"></a>00023 <span class="comment">  Functions defined in this file:</span>
<a name="l00024"></a>00024 <span class="comment"></span>
<a name="l00025"></a>00025 <span class="comment">  M X -&gt; Y</span>
<a name="l00026"></a>00026 <span class="comment">  Mlt(M, X, Y)</span>
<a name="l00027"></a>00027 <span class="comment"></span>
<a name="l00028"></a>00028 <span class="comment">  alpha M X -&gt; Y</span>
<a name="l00029"></a>00029 <span class="comment">  Mlt(alpha, M, X, Y)</span>
<a name="l00030"></a>00030 <span class="comment"></span>
<a name="l00031"></a>00031 <span class="comment">  M X -&gt; Y</span>
<a name="l00032"></a>00032 <span class="comment">  M^T X -&gt; Y</span>
<a name="l00033"></a>00033 <span class="comment">  Mlt(Trans, M, X, Y)</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 <span class="comment">  alpha M X + beta Y -&gt; Y</span>
<a name="l00036"></a>00036 <span class="comment">  MltAdd(alpha, M, X, beta, Y)</span>
<a name="l00037"></a>00037 <span class="comment"></span>
<a name="l00038"></a>00038 <span class="comment">  Gauss(M, X)</span>
<a name="l00039"></a>00039 <span class="comment"></span>
<a name="l00040"></a>00040 <span class="comment">  GaussSeidel(M, X, Y, iter)</span>
<a name="l00041"></a>00041 <span class="comment"></span>
<a name="l00042"></a>00042 <span class="comment">  SOR(M, X, Y, omega, iter)</span>
<a name="l00043"></a>00043 <span class="comment"></span>
<a name="l00044"></a>00044 <span class="comment">  SolveLU(M, Y)</span>
<a name="l00045"></a>00045 <span class="comment"></span>
<a name="l00046"></a>00046 <span class="comment">  Solve(M, Y)</span>
<a name="l00047"></a>00047 <span class="comment">*/</span>
<a name="l00048"></a>00048 
<a name="l00049"></a>00049 <span class="keyword">namespace </span>Seldon
<a name="l00050"></a>00050 {
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054   <span class="comment">// MLT //</span>
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 
<a name="l00058"></a>00058 
<a name="l00066"></a>00066   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00067"></a>00067             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00068"></a>00068             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00069"></a><a class="code" href="namespace_seldon.php#a6b94850cd394c6afee2463526ec1a7da">00069</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l00070"></a>00070            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l00071"></a>00071            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y)
<a name="l00072"></a>00072   {
<a name="l00073"></a>00073     Y.Fill(T2(0));
<a name="l00074"></a>00074     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(T2(1), M, X, T2(0), Y);
<a name="l00075"></a>00075   }
<a name="l00076"></a>00076 
<a name="l00077"></a>00077 
<a name="l00079"></a>00079 
<a name="l00090"></a>00090   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00091"></a>00091             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00092"></a>00092             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00093"></a><a class="code" href="namespace_seldon.php#a3b6e38c6a9f412b0b05ed2073a3d0b8e">00093</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T3&amp; alpha,
<a name="l00094"></a>00094            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; M,
<a name="l00095"></a>00095            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00096"></a>00096            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; Y)
<a name="l00097"></a>00097   {
<a name="l00098"></a>00098     Y.Fill(T2(0));
<a name="l00099"></a>00099     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, T3(0), Y);
<a name="l00100"></a>00100   }
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00104"></a>00104 
<a name="l00114"></a>00114   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00115"></a>00115             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00116"></a>00116             <span class="keyword">class </span>T3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00117"></a><a class="code" href="namespace_seldon.php#a6a29e7a8057465569619e864cfb75d9d">00117</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; Trans,
<a name="l00118"></a>00118            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; M,
<a name="l00119"></a>00119            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00120"></a>00120            <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, Storage3, Allocator3&gt;</a>&amp; Y)
<a name="l00121"></a>00121   {
<a name="l00122"></a>00122     Y.Fill(T2(0));
<a name="l00123"></a>00123     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(T2(1), Trans, M, X, T3(0), Y);
<a name="l00124"></a>00124   }
<a name="l00125"></a>00125     
<a name="l00126"></a>00126   
<a name="l00127"></a>00127   <span class="comment">// MLT //</span>
<a name="l00129"></a>00129 <span class="comment"></span>
<a name="l00130"></a>00130 
<a name="l00131"></a>00131 
<a name="l00133"></a>00133   <span class="comment">// MltAdd //</span>
<a name="l00134"></a>00134 
<a name="l00135"></a>00135 
<a name="l00136"></a>00136   <span class="comment">/*** Sparse matrices ***/</span>
<a name="l00137"></a>00137 
<a name="l00138"></a>00138 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00140"></a>00140             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00141"></a>00141             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00142"></a>00142             <span class="keyword">class </span>T3,
<a name="l00143"></a>00143             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00144"></a>00144   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00145"></a>00145               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00146"></a>00146               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00147"></a>00147               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00148"></a>00148   {
<a name="l00149"></a>00149     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00150"></a>00150 
<a name="l00151"></a>00151 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00152"></a>00152 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00153"></a>00153 <span class="preprocessor">#endif</span>
<a name="l00154"></a>00154 <span class="preprocessor"></span>
<a name="l00155"></a>00155     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00156"></a>00156 
<a name="l00157"></a>00157     T4 zero(0);
<a name="l00158"></a>00158     T4 temp;
<a name="l00159"></a>00159 
<a name="l00160"></a>00160     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00161"></a>00161     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00162"></a>00162     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00163"></a>00163       data = M.GetData();
<a name="l00164"></a>00164 
<a name="l00165"></a>00165     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00166"></a>00166       {
<a name="l00167"></a>00167         temp = zero;
<a name="l00168"></a>00168         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = ptr[i]; j &lt; ptr[i+1]; j++)
<a name="l00169"></a>00169           temp += data[j] * X(ind[j]);
<a name="l00170"></a>00170         Y(i) += alpha * temp;
<a name="l00171"></a>00171       }
<a name="l00172"></a>00172   }
<a name="l00173"></a>00173 
<a name="l00174"></a>00174 
<a name="l00175"></a>00175   <span class="comment">/*** Complex sparse matrices ***/</span>
<a name="l00176"></a>00176 
<a name="l00177"></a>00177 
<a name="l00178"></a>00178   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00179"></a>00179             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00180"></a>00180             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00181"></a>00181             <span class="keyword">class </span>T3,
<a name="l00182"></a>00182             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00183"></a>00183   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00184"></a>00184               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00185"></a>00185               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00186"></a>00186               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00187"></a>00187   {
<a name="l00188"></a>00188     <span class="keywordtype">int</span> i, j;
<a name="l00189"></a>00189 
<a name="l00190"></a>00190     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00191"></a>00191 
<a name="l00192"></a>00192 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00193"></a>00193 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00194"></a>00194 <span class="preprocessor">#endif</span>
<a name="l00195"></a>00195 <span class="preprocessor"></span>
<a name="l00196"></a>00196     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00197"></a>00197 
<a name="l00198"></a>00198     complex&lt;T1&gt; zero(0);
<a name="l00199"></a>00199     complex&lt;T1&gt; temp;
<a name="l00200"></a>00200 
<a name="l00201"></a>00201     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00202"></a>00202     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00203"></a>00203     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00204"></a>00204     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00205"></a>00205     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00206"></a>00206       real_data = M.GetRealData();
<a name="l00207"></a>00207     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00208"></a>00208       imag_data = M.GetImagData();
<a name="l00209"></a>00209 
<a name="l00210"></a>00210     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00211"></a>00211       {
<a name="l00212"></a>00212         temp = zero;
<a name="l00213"></a>00213         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00214"></a>00214           temp += real_data[j] * X(real_ind[j]);
<a name="l00215"></a>00215         Y(i) += alpha * temp;
<a name="l00216"></a>00216       }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00219"></a>00219       {
<a name="l00220"></a>00220         temp = zero;
<a name="l00221"></a>00221         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00222"></a>00222           temp += complex&lt;T1&gt;(T1(0), imag_data[j]) * X(imag_ind[j]);
<a name="l00223"></a>00223         Y(i) += alpha * temp;
<a name="l00224"></a>00224       }
<a name="l00225"></a>00225   }
<a name="l00226"></a>00226 
<a name="l00227"></a>00227 
<a name="l00228"></a>00228   <span class="comment">/*** Symmetric sparse matrices ***/</span>
<a name="l00229"></a>00229 
<a name="l00230"></a>00230 
<a name="l00231"></a>00231   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00232"></a>00232             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00233"></a>00233             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00234"></a>00234             <span class="keyword">class </span>T3,
<a name="l00235"></a>00235             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00236"></a>00236   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00237"></a>00237               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00238"></a>00238               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00239"></a>00239               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00240"></a>00240   {
<a name="l00241"></a>00241     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00242"></a>00242 
<a name="l00243"></a>00243 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00244"></a>00244 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00245"></a>00245 <span class="preprocessor">#endif</span>
<a name="l00246"></a>00246 <span class="preprocessor"></span>
<a name="l00247"></a>00247     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     <span class="keywordtype">int</span> i, j;
<a name="l00250"></a>00250     T4 zero(0);
<a name="l00251"></a>00251     T4 temp;
<a name="l00252"></a>00252 
<a name="l00253"></a>00253     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00254"></a>00254     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00255"></a>00255     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00256"></a>00256       data = M.GetData();
<a name="l00257"></a>00257 
<a name="l00258"></a>00258     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00259"></a>00259       {
<a name="l00260"></a>00260         temp = zero;
<a name="l00261"></a>00261         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00262"></a>00262           temp += data[j] * X(ind[j]);
<a name="l00263"></a>00263         Y(i) += alpha * temp;
<a name="l00264"></a>00264       }
<a name="l00265"></a>00265     <span class="keywordflow">for</span> (i = 0; i &lt; ma-1; i++)
<a name="l00266"></a>00266       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00267"></a>00267         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00268"></a>00268           Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00269"></a>00269   }
<a name="l00270"></a>00270 
<a name="l00271"></a>00271 
<a name="l00272"></a>00272   <span class="comment">/*** Symmetric complex sparse matrices ***/</span>
<a name="l00273"></a>00273 
<a name="l00274"></a>00274 
<a name="l00275"></a>00275   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00276"></a>00276             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00277"></a>00277             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00278"></a>00278             <span class="keyword">class </span>T3,
<a name="l00279"></a>00279             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00280"></a>00280   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00281"></a>00281               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00282"></a>00282               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00283"></a>00283               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00284"></a>00284   {
<a name="l00285"></a>00285     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00286"></a>00286 
<a name="l00287"></a>00287 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00288"></a>00288 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00289"></a>00289 <span class="preprocessor">#endif</span>
<a name="l00290"></a>00290 <span class="preprocessor"></span>
<a name="l00291"></a>00291     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00292"></a>00292 
<a name="l00293"></a>00293     <span class="keywordtype">int</span> i, j;
<a name="l00294"></a>00294     complex&lt;T1&gt; zero(0);
<a name="l00295"></a>00295     complex&lt;T1&gt; temp;
<a name="l00296"></a>00296 
<a name="l00297"></a>00297     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00298"></a>00298     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00299"></a>00299     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00300"></a>00300     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00301"></a>00301     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00302"></a>00302       real_data = M.GetRealData();
<a name="l00303"></a>00303     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00304"></a>00304       imag_data = M.GetImagData();
<a name="l00305"></a>00305 
<a name="l00306"></a>00306     <span class="keywordflow">for</span> (i = 0; i&lt;ma; i++)
<a name="l00307"></a>00307       {
<a name="l00308"></a>00308         temp = zero;
<a name="l00309"></a>00309         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00310"></a>00310           temp += real_data[j] * X(real_ind[j]);
<a name="l00311"></a>00311         Y(i) += alpha * temp;
<a name="l00312"></a>00312       }
<a name="l00313"></a>00313     <span class="keywordflow">for</span> (i = 0; i&lt;ma-1; i++)
<a name="l00314"></a>00314       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00315"></a>00315         <span class="keywordflow">if</span> (real_ind[j] != i)
<a name="l00316"></a>00316           Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00317"></a>00317 
<a name="l00318"></a>00318     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00319"></a>00319       {
<a name="l00320"></a>00320         temp = zero;
<a name="l00321"></a>00321         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00322"></a>00322           temp += complex&lt;T1&gt;(T1(0), imag_data[j]) * X(imag_ind[j]);
<a name="l00323"></a>00323         Y(i) += alpha * temp;
<a name="l00324"></a>00324       }
<a name="l00325"></a>00325     <span class="keywordflow">for</span> (i = 0; i&lt;ma-1; i++)
<a name="l00326"></a>00326       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00327"></a>00327         <span class="keywordflow">if</span> (imag_ind[j] != i)
<a name="l00328"></a>00328           Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), imag_data[j]) * X(i);
<a name="l00329"></a>00329   }
<a name="l00330"></a>00330 
<a name="l00331"></a>00331 
<a name="l00332"></a>00332   <span class="comment">/*** Sparse matrices, *Trans ***/</span>
<a name="l00333"></a>00333 
<a name="l00334"></a>00334 
<a name="l00335"></a>00335   <span class="comment">// NoTrans.</span>
<a name="l00336"></a>00336   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00337"></a>00337             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00338"></a>00338             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00339"></a>00339             <span class="keyword">class </span>T3,
<a name="l00340"></a>00340             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00341"></a>00341   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00342"></a>00342               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00343"></a>00343               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00344"></a>00344               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00345"></a>00345               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00346"></a>00346   {
<a name="l00347"></a>00347     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00348"></a>00348   }
<a name="l00349"></a>00349 
<a name="l00350"></a>00350 
<a name="l00351"></a>00351   <span class="comment">// Trans.</span>
<a name="l00352"></a>00352   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00353"></a>00353             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00354"></a>00354             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00355"></a>00355             <span class="keyword">class </span>T3,
<a name="l00356"></a>00356             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00357"></a>00357   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00358"></a>00358               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00359"></a>00359               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00360"></a>00360               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00361"></a>00361               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00362"></a>00362   {
<a name="l00363"></a>00363     <span class="keywordtype">int</span> i, j;
<a name="l00364"></a>00364 
<a name="l00365"></a>00365     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00366"></a>00366 
<a name="l00367"></a>00367 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00368"></a>00368 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonTrans, M, X, beta, Y)&quot;</span>);
<a name="l00369"></a>00369 <span class="preprocessor">#endif</span>
<a name="l00370"></a>00370 <span class="preprocessor"></span>
<a name="l00371"></a>00371     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00372"></a>00372 
<a name="l00373"></a>00373     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00374"></a>00374     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00375"></a>00375     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00376"></a>00376       data = M.GetData();
<a name="l00377"></a>00377 
<a name="l00378"></a>00378     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00379"></a>00379       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00380"></a>00380         Y(ind[j]) += alpha * data[j] * X(i);
<a name="l00381"></a>00381   }
<a name="l00382"></a>00382 
<a name="l00383"></a>00383 
<a name="l00384"></a>00384   <span class="comment">// ConjTrans.</span>
<a name="l00385"></a>00385   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00386"></a>00386             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00387"></a>00387             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00388"></a>00388             <span class="keyword">class </span>T3,
<a name="l00389"></a>00389             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00390"></a>00390   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00391"></a>00391               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00392"></a>00392               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSparse, Allocator1&gt;&amp; M,
<a name="l00393"></a>00393               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00394"></a>00394               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00395"></a>00395   {
<a name="l00396"></a>00396     <span class="keywordtype">int</span> i, j;
<a name="l00397"></a>00397 
<a name="l00398"></a>00398     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00399"></a>00399 
<a name="l00400"></a>00400 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00401"></a>00401 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00402"></a>00402 <span class="preprocessor">#endif</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>
<a name="l00404"></a>00404     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00405"></a>00405 
<a name="l00406"></a>00406     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00407"></a>00407     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00408"></a>00408     <span class="keyword">typename</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSparse, Allocator1&gt;::pointer
<a name="l00409"></a>00409       data = M.GetData();
<a name="l00410"></a>00410 
<a name="l00411"></a>00411     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00412"></a>00412       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00413"></a>00413         Y(ind[j]) += alpha * conj(data[j]) * X(i);
<a name="l00414"></a>00414   }
<a name="l00415"></a>00415 
<a name="l00416"></a>00416 
<a name="l00417"></a>00417   <span class="comment">/*** Complex sparse matrices, *Trans ***/</span>
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00420"></a>00420   <span class="comment">// NoTrans.</span>
<a name="l00421"></a>00421   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00422"></a>00422             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00423"></a>00423             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00424"></a>00424             <span class="keyword">class </span>T3,
<a name="l00425"></a>00425             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00426"></a>00426   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00427"></a>00427               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00428"></a>00428               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00429"></a>00429               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00430"></a>00430               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00431"></a>00431   {
<a name="l00432"></a>00432     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00433"></a>00433   }
<a name="l00434"></a>00434 
<a name="l00435"></a>00435 
<a name="l00436"></a>00436   <span class="comment">// Trans.</span>
<a name="l00437"></a>00437   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00438"></a>00438             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00439"></a>00439             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00440"></a>00440             <span class="keyword">class </span>T3,
<a name="l00441"></a>00441             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00442"></a>00442   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00443"></a>00443               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00444"></a>00444               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00445"></a>00445               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00446"></a>00446               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00447"></a>00447   {
<a name="l00448"></a>00448     <span class="keywordtype">int</span> i, j;
<a name="l00449"></a>00449 
<a name="l00450"></a>00450     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00451"></a>00451 
<a name="l00452"></a>00452 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00453"></a>00453 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonTrans, M, X, beta, Y)&quot;</span>);
<a name="l00454"></a>00454 <span class="preprocessor">#endif</span>
<a name="l00455"></a>00455 <span class="preprocessor"></span>
<a name="l00456"></a>00456     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00457"></a>00457 
<a name="l00458"></a>00458     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00459"></a>00459     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00460"></a>00460     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00461"></a>00461     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00462"></a>00462     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00463"></a>00463       real_data = M.GetRealData();
<a name="l00464"></a>00464     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00465"></a>00465       imag_data = M.GetImagData();
<a name="l00466"></a>00466 
<a name="l00467"></a>00467     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00468"></a>00468       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00469"></a>00469         Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00470"></a>00470 
<a name="l00471"></a>00471     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00472"></a>00472       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00473"></a>00473         Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), imag_data[j]) * X(i);
<a name="l00474"></a>00474   }
<a name="l00475"></a>00475 
<a name="l00476"></a>00476 
<a name="l00477"></a>00477   <span class="comment">// ConjTrans.</span>
<a name="l00478"></a>00478   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00479"></a>00479             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00480"></a>00480             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00481"></a>00481             <span class="keyword">class </span>T3,
<a name="l00482"></a>00482             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00483"></a>00483   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00484"></a>00484               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00485"></a>00485               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;&amp; M,
<a name="l00486"></a>00486               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00487"></a>00487               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00488"></a>00488   {
<a name="l00489"></a>00489     <span class="keywordtype">int</span> i, j;
<a name="l00490"></a>00490 
<a name="l00491"></a>00491     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00492"></a>00492 
<a name="l00493"></a>00493 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00494"></a>00494 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00495"></a>00495 <span class="preprocessor">#endif</span>
<a name="l00496"></a>00496 <span class="preprocessor"></span>
<a name="l00497"></a>00497     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00498"></a>00498 
<a name="l00499"></a>00499     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00500"></a>00500     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00501"></a>00501     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00502"></a>00502     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00503"></a>00503     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00504"></a>00504       real_data = M.GetRealData();
<a name="l00505"></a>00505     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowComplexSparse, Allocator1&gt;::pointer
<a name="l00506"></a>00506       imag_data = M.GetImagData();
<a name="l00507"></a>00507 
<a name="l00508"></a>00508     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00509"></a>00509       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00510"></a>00510         Y(real_ind[j]) += alpha * real_data[j] * X(i);
<a name="l00511"></a>00511 
<a name="l00512"></a>00512     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00513"></a>00513       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00514"></a>00514         Y(imag_ind[j]) += alpha * complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(i);
<a name="l00515"></a>00515   }
<a name="l00516"></a>00516 
<a name="l00517"></a>00517 
<a name="l00518"></a>00518   <span class="comment">/*** Symmetric sparse matrices, *Trans ***/</span>
<a name="l00519"></a>00519 
<a name="l00520"></a>00520 
<a name="l00521"></a>00521   <span class="comment">// NoTrans.</span>
<a name="l00522"></a>00522   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00523"></a>00523             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00524"></a>00524             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00525"></a>00525             <span class="keyword">class </span>T3,
<a name="l00526"></a>00526             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00527"></a>00527   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00528"></a>00528               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00529"></a>00529               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00530"></a>00530               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00531"></a>00531               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00532"></a>00532   {
<a name="l00533"></a>00533     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00534"></a>00534   }
<a name="l00535"></a>00535 
<a name="l00536"></a>00536 
<a name="l00537"></a>00537   <span class="comment">// Trans.</span>
<a name="l00538"></a>00538   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00539"></a>00539             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00540"></a>00540             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00541"></a>00541             <span class="keyword">class </span>T3,
<a name="l00542"></a>00542             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00543"></a>00543   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00544"></a>00544               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00545"></a>00545               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00546"></a>00546               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00547"></a>00547               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00548"></a>00548   {
<a name="l00549"></a>00549     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00550"></a>00550   }
<a name="l00551"></a>00551 
<a name="l00552"></a>00552 
<a name="l00553"></a>00553   <span class="comment">// ConjTrans.</span>
<a name="l00554"></a>00554   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00555"></a>00555             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00556"></a>00556             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00557"></a>00557             <span class="keyword">class </span>T3,
<a name="l00558"></a>00558             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00559"></a>00559   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00560"></a>00560               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00561"></a>00561               <span class="keyword">const</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSymSparse, Allocator1&gt;&amp; M,
<a name="l00562"></a>00562               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00563"></a>00563               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00564"></a>00564   {
<a name="l00565"></a>00565     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00566"></a>00566 
<a name="l00567"></a>00567 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00568"></a>00568 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00569"></a>00569 <span class="preprocessor">#endif</span>
<a name="l00570"></a>00570 <span class="preprocessor"></span>
<a name="l00571"></a>00571     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00572"></a>00572 
<a name="l00573"></a>00573     <span class="keywordtype">int</span> i, j;
<a name="l00574"></a>00574     complex&lt;T1&gt; zero(0);
<a name="l00575"></a>00575     complex&lt;T1&gt; temp;
<a name="l00576"></a>00576 
<a name="l00577"></a>00577     <span class="keywordtype">int</span>* ptr = M.GetPtr();
<a name="l00578"></a>00578     <span class="keywordtype">int</span>* ind = M.GetInd();
<a name="l00579"></a>00579     <span class="keyword">typename</span> Matrix&lt;complex&lt;T1&gt;, Prop1, RowSymSparse, Allocator1&gt;::pointer
<a name="l00580"></a>00580       data = M.GetData();
<a name="l00581"></a>00581 
<a name="l00582"></a>00582     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00583"></a>00583       {
<a name="l00584"></a>00584         temp = zero;
<a name="l00585"></a>00585         <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00586"></a>00586           temp += conj(data[j]) * X(ind[j]);
<a name="l00587"></a>00587         Y(i) += temp;
<a name="l00588"></a>00588       }
<a name="l00589"></a>00589     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l00590"></a>00590       <span class="keywordflow">for</span> (j = ptr[i]; j &lt; ptr[i + 1]; j++)
<a name="l00591"></a>00591         <span class="keywordflow">if</span> (ind[j] != i)
<a name="l00592"></a>00592           Y(ind[j]) += conj(data[j]) * X(i);
<a name="l00593"></a>00593   }
<a name="l00594"></a>00594 
<a name="l00595"></a>00595 
<a name="l00596"></a>00596   <span class="comment">/*** Symmetric complex sparse matrices, *Trans ***/</span>
<a name="l00597"></a>00597 
<a name="l00598"></a>00598 
<a name="l00599"></a>00599   <span class="comment">// NoTrans.</span>
<a name="l00600"></a>00600   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00601"></a>00601             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00602"></a>00602             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00603"></a>00603             <span class="keyword">class </span>T3,
<a name="l00604"></a>00604             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00605"></a>00605   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00606"></a>00606               <span class="keyword">const</span> class_SeldonNoTrans&amp; Trans,
<a name="l00607"></a>00607               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00608"></a>00608               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00609"></a>00609               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00610"></a>00610   {
<a name="l00611"></a>00611     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00612"></a>00612   }
<a name="l00613"></a>00613 
<a name="l00614"></a>00614 
<a name="l00615"></a>00615   <span class="comment">// Trans.</span>
<a name="l00616"></a>00616   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00617"></a>00617             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00618"></a>00618             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00619"></a>00619             <span class="keyword">class </span>T3,
<a name="l00620"></a>00620             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00621"></a>00621   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00622"></a>00622               <span class="keyword">const</span> class_SeldonTrans&amp; Trans,
<a name="l00623"></a>00623               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00624"></a>00624               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00625"></a>00625               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00626"></a>00626   {
<a name="l00627"></a>00627     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00631"></a>00631   <span class="comment">// ConjTrans.</span>
<a name="l00632"></a>00632   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00633"></a>00633             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00634"></a>00634             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00635"></a>00635             <span class="keyword">class </span>T3,
<a name="l00636"></a>00636             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00637"></a>00637   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00638"></a>00638               <span class="keyword">const</span> class_SeldonConjTrans&amp; Trans,
<a name="l00639"></a>00639               <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;&amp; M,
<a name="l00640"></a>00640               <span class="keyword">const</span> Vector&lt;T2, Storage2, Allocator2&gt;&amp; X,
<a name="l00641"></a>00641               <span class="keyword">const</span> T3 beta, Vector&lt;T4, Storage4, Allocator4&gt;&amp; Y)
<a name="l00642"></a>00642   {
<a name="l00643"></a>00643     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00644"></a>00644 
<a name="l00645"></a>00645 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00646"></a>00646 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, SeldonConjTrans, M, X, beta, Y)&quot;</span>);
<a name="l00647"></a>00647 <span class="preprocessor">#endif</span>
<a name="l00648"></a>00648 <span class="preprocessor"></span>
<a name="l00649"></a>00649     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00650"></a>00650 
<a name="l00651"></a>00651     <span class="keywordtype">int</span> i, j;
<a name="l00652"></a>00652     complex&lt;T1&gt; zero(0);
<a name="l00653"></a>00653     complex&lt;T1&gt; temp;
<a name="l00654"></a>00654 
<a name="l00655"></a>00655     <span class="keywordtype">int</span>* real_ptr = M.GetRealPtr();
<a name="l00656"></a>00656     <span class="keywordtype">int</span>* imag_ptr = M.GetImagPtr();
<a name="l00657"></a>00657     <span class="keywordtype">int</span>* real_ind = M.GetRealInd();
<a name="l00658"></a>00658     <span class="keywordtype">int</span>* imag_ind = M.GetImagInd();
<a name="l00659"></a>00659     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00660"></a>00660       real_data = M.GetRealData();
<a name="l00661"></a>00661     <span class="keyword">typename</span> Matrix&lt;T1, Prop1, RowSymComplexSparse, Allocator1&gt;::pointer
<a name="l00662"></a>00662       imag_data = M.GetImagData();
<a name="l00663"></a>00663 
<a name="l00664"></a>00664     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00665"></a>00665       {
<a name="l00666"></a>00666         temp = zero;
<a name="l00667"></a>00667         <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00668"></a>00668           temp += real_data[j] * X(real_ind[j]);
<a name="l00669"></a>00669         Y(i) += temp;
<a name="l00670"></a>00670       }
<a name="l00671"></a>00671     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l00672"></a>00672       <span class="keywordflow">for</span> (j = real_ptr[i]; j &lt; real_ptr[i + 1]; j++)
<a name="l00673"></a>00673         <span class="keywordflow">if</span> (real_ind[j] != i)
<a name="l00674"></a>00674           Y(real_ind[j]) += real_data[j] * X(i);
<a name="l00675"></a>00675 
<a name="l00676"></a>00676     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l00677"></a>00677       {
<a name="l00678"></a>00678         temp = zero;
<a name="l00679"></a>00679         <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00680"></a>00680           temp += complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(imag_ind[j]);
<a name="l00681"></a>00681         Y(i) += temp;
<a name="l00682"></a>00682       }
<a name="l00683"></a>00683     <span class="keywordflow">for</span> (i = 0; i &lt; ma - 1; i++)
<a name="l00684"></a>00684       <span class="keywordflow">for</span> (j = imag_ptr[i]; j &lt; imag_ptr[i + 1]; j++)
<a name="l00685"></a>00685         <span class="keywordflow">if</span> (imag_ind[j] != i)
<a name="l00686"></a>00686           Y(imag_ind[j]) += complex&lt;T1&gt;(T1(0), - imag_data[j]) * X(i);
<a name="l00687"></a>00687   }
<a name="l00688"></a>00688 
<a name="l00689"></a>00689 
<a name="l00690"></a>00690   <span class="comment">// MltAdd //</span>
<a name="l00692"></a>00692 <span class="comment"></span>
<a name="l00693"></a>00693 
<a name="l00694"></a>00694 
<a name="l00696"></a>00696   <span class="comment">// MltAdd //</span>
<a name="l00697"></a>00697 
<a name="l00698"></a>00698 
<a name="l00713"></a>00713   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00714"></a>00714             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00715"></a>00715             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00716"></a>00716             <span class="keyword">class </span>T3,
<a name="l00717"></a>00717             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00718"></a><a class="code" href="namespace_seldon.php#ac3739907f79a615c21ec43e8762d17e4">00718</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00719"></a>00719               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; M,
<a name="l00720"></a>00720               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00721"></a>00721               <span class="keyword">const</span> T3 beta,
<a name="l00722"></a>00722               <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T4, Storage4, Allocator4&gt;</a>&amp; Y)
<a name="l00723"></a>00723   {
<a name="l00724"></a>00724     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00725"></a>00725     <span class="keywordtype">int</span> na = M.GetN();
<a name="l00726"></a>00726 
<a name="l00727"></a>00727 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00728"></a>00728 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00729"></a>00729 <span class="preprocessor">#endif</span>
<a name="l00730"></a>00730 <span class="preprocessor"></span>
<a name="l00731"></a>00731     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00732"></a>00732 
<a name="l00733"></a>00733     T4 zero(0);
<a name="l00734"></a>00734     T4 temp;
<a name="l00735"></a>00735     T4 alpha_(alpha);
<a name="l00736"></a>00736 
<a name="l00737"></a>00737     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00738"></a>00738       {
<a name="l00739"></a>00739         temp = zero;
<a name="l00740"></a>00740         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l00741"></a>00741           temp += M(i, j) * X(j);
<a name="l00742"></a>00742         Y(i) += alpha_ * temp;
<a name="l00743"></a>00743       }
<a name="l00744"></a>00744   }
<a name="l00745"></a>00745 
<a name="l00746"></a>00746 
<a name="l00761"></a>00761   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00762"></a>00762             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00763"></a>00763             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00764"></a>00764             <span class="keyword">class </span>T3,
<a name="l00765"></a>00765             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00766"></a><a class="code" href="namespace_seldon.php#af1640643ef20e07131d969393f0f56db">00766</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00767"></a>00767               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;</a>&amp; M,
<a name="l00768"></a>00768               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Collection, Allocator2&gt;</a>&amp; X,
<a name="l00769"></a>00769               <span class="keyword">const</span> T3 beta,
<a name="l00770"></a>00770               <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T4, Collection, Allocator4&gt;</a>&amp; Y)
<a name="l00771"></a>00771   {
<a name="l00772"></a>00772     <span class="keywordtype">int</span> ma = M.GetMmatrix();
<a name="l00773"></a>00773     <span class="keywordtype">int</span> na = M.GetNmatrix();
<a name="l00774"></a>00774 
<a name="l00775"></a>00775 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00776"></a>00776 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00777"></a>00777 <span class="preprocessor">#endif</span>
<a name="l00778"></a>00778 <span class="preprocessor"></span>    <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l00779"></a>00779 
<a name="l00780"></a>00780     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(value_type(beta), Y);
<a name="l00781"></a>00781 
<a name="l00782"></a>00782     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00783"></a>00783       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l00784"></a>00784         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M.GetMatrix(i, j), X.GetVector(j), value_type(1.),
<a name="l00785"></a>00785                Y.GetVector(i));
<a name="l00786"></a>00786   }
<a name="l00787"></a>00787 
<a name="l00788"></a>00788 
<a name="l00803"></a>00803   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00804"></a>00804             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00805"></a>00805             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00806"></a>00806             <span class="keyword">class </span>T3,
<a name="l00807"></a>00807             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00808"></a><a class="code" href="namespace_seldon.php#a4bb9b9f473daa5e2ef73032d9fade883">00808</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00809"></a>00809               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;</a>&amp; M,
<a name="l00810"></a>00810               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Collection, Allocator2&gt;</a>&amp; X,
<a name="l00811"></a>00811               <span class="keyword">const</span> T3 beta,
<a name="l00812"></a>00812               <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T4, Collection, Allocator4&gt;</a>&amp; Y)
<a name="l00813"></a>00813   {
<a name="l00814"></a>00814     <span class="keywordtype">int</span> ma = M.GetMmatrix();
<a name="l00815"></a>00815     <span class="keywordtype">int</span> na = M.GetNmatrix();
<a name="l00816"></a>00816 
<a name="l00817"></a>00817 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00818"></a>00818 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, M, X, beta, Y)&quot;</span>);
<a name="l00819"></a>00819 <span class="preprocessor">#endif</span>
<a name="l00820"></a>00820 <span class="preprocessor"></span>    <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l00821"></a>00821 
<a name="l00822"></a>00822     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(value_type(beta), Y);
<a name="l00823"></a>00823 
<a name="l00824"></a>00824     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++)
<a name="l00825"></a>00825       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l00826"></a>00826         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M.GetMatrix(i, j), X.GetVector(j), value_type(1.),
<a name="l00827"></a>00827                Y.GetVector(i));
<a name="l00828"></a>00828   }
<a name="l00829"></a>00829 
<a name="l00830"></a>00830 
<a name="l00850"></a>00850   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00851"></a>00851             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00852"></a>00852             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00853"></a>00853             <span class="keyword">class </span>T3,
<a name="l00854"></a>00854             <span class="keyword">class </span>T4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00855"></a><a class="code" href="namespace_seldon.php#ac9fbb323d131b8e1e8f8fc99f92b16e8">00855</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00856"></a>00856               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; Trans,
<a name="l00857"></a>00857               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; M,
<a name="l00858"></a>00858               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; X,
<a name="l00859"></a>00859               <span class="keyword">const</span> T3 beta,
<a name="l00860"></a>00860               <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T4, Storage4, Allocator4&gt;</a>&amp; Y)
<a name="l00861"></a>00861   {
<a name="l00862"></a>00862     <span class="keywordflow">if</span> (Trans.NoTrans())
<a name="l00863"></a>00863       {
<a name="l00864"></a>00864         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, M, X, beta, Y);
<a name="l00865"></a>00865         <span class="keywordflow">return</span>;
<a name="l00866"></a>00866       }
<a name="l00867"></a>00867     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (Trans.ConjTrans())
<a name="l00868"></a>00868       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;MltAdd(alpha, trans, M, X, beta, Y)&quot;</span>,
<a name="l00869"></a>00869                           <span class="stringliteral">&quot;Complex conjugation not supported.&quot;</span>);
<a name="l00870"></a>00870 
<a name="l00871"></a>00871     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00872"></a>00872     <span class="keywordtype">int</span> na = M.GetN();
<a name="l00873"></a>00873 
<a name="l00874"></a>00874 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00875"></a>00875 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(Trans, M, X, Y, <span class="stringliteral">&quot;MltAdd(alpha, trans, M, X, beta, Y)&quot;</span>);
<a name="l00876"></a>00876 <span class="preprocessor">#endif</span>
<a name="l00877"></a>00877 <span class="preprocessor"></span>
<a name="l00878"></a>00878     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00879"></a>00879       Y.Fill(T4(0));
<a name="l00880"></a>00880     <span class="keywordflow">else</span>
<a name="l00881"></a>00881       <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, Y);
<a name="l00882"></a>00882 
<a name="l00883"></a>00883     T4 zero(0);
<a name="l00884"></a>00884     T4 temp;
<a name="l00885"></a>00885     T4 alpha_(alpha);
<a name="l00886"></a>00886 
<a name="l00887"></a>00887     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; na; i++)
<a name="l00888"></a>00888       {
<a name="l00889"></a>00889         temp = zero;
<a name="l00890"></a>00890         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; ma; j++)
<a name="l00891"></a>00891           temp += M(j, i) * X(j);
<a name="l00892"></a>00892         Y(i) += alpha_ * temp;
<a name="l00893"></a>00893       }
<a name="l00894"></a>00894   }
<a name="l00895"></a>00895 
<a name="l00896"></a>00896 
<a name="l00897"></a>00897   <span class="comment">// MltAdd //</span>
<a name="l00899"></a>00899 <span class="comment"></span>
<a name="l00900"></a>00900 
<a name="l00902"></a>00902   <span class="comment">// Gauss //</span>
<a name="l00903"></a>00903 
<a name="l00904"></a>00904 
<a name="l00905"></a>00905   <span class="comment">// Solve X = M*Y with Gauss method.</span>
<a name="l00906"></a>00906   <span class="comment">// Warning: M is modified. The results are stored in X.</span>
<a name="l00907"></a>00907   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00908"></a>00908             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00909"></a>00909   <span class="keyword">inline</span> <span class="keywordtype">void</span> Gauss(Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00910"></a>00910                     Vector&lt;T1, Storage1, Allocator1&gt;&amp; X)
<a name="l00911"></a>00911   {
<a name="l00912"></a>00912     <span class="keywordtype">int</span> i, j, k;
<a name="l00913"></a>00913     T1 r, S;
<a name="l00914"></a>00914 
<a name="l00915"></a>00915     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00916"></a>00916     <span class="keywordtype">int</span> na = M.GetN();
<a name="l00917"></a>00917 
<a name="l00918"></a>00918 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00919"></a>00919 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l00920"></a>00920       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Gauss(M, X)&quot;</span>,
<a name="l00921"></a>00921                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l00922"></a>00922 
<a name="l00923"></a>00923     <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, <span class="stringliteral">&quot;Gauss(M, X)&quot;</span>);
<a name="l00924"></a>00924 <span class="preprocessor">#endif</span>
<a name="l00925"></a>00925 <span class="preprocessor"></span>
<a name="l00926"></a>00926     <span class="keywordflow">for</span> (k = 0; k &lt; ma - 1; k++)
<a name="l00927"></a>00927       <span class="keywordflow">for</span> (i = k + 1; i &lt; ma; i++)
<a name="l00928"></a>00928         {
<a name="l00929"></a>00929           r = M(i, k) / M(k, k);
<a name="l00930"></a>00930           <span class="keywordflow">for</span> (j = k + 1; j &lt; ma; j++)
<a name="l00931"></a>00931             M(i, j) -= r * M(k, j);
<a name="l00932"></a>00932           X(i) -= r *= X(k);
<a name="l00933"></a>00933         }
<a name="l00934"></a>00934 
<a name="l00935"></a>00935     X(ma - 1) = X(ma - 1) / M(ma - 1, ma - 1);
<a name="l00936"></a>00936     <span class="keywordflow">for</span> (k = ma - 2; k &gt; -1; k--)
<a name="l00937"></a>00937       {
<a name="l00938"></a>00938         S = X(k);
<a name="l00939"></a>00939         <span class="keywordflow">for</span> (j = k + 1; j &lt; ma; j++)
<a name="l00940"></a>00940           S -= M(k, j) * X(j);
<a name="l00941"></a>00941         X(k) = S / M(k, k);
<a name="l00942"></a>00942       }
<a name="l00943"></a>00943   }
<a name="l00944"></a>00944 
<a name="l00945"></a>00945 
<a name="l00946"></a>00946   <span class="comment">// Gauss //</span>
<a name="l00948"></a>00948 <span class="comment"></span>
<a name="l00949"></a>00949 
<a name="l00950"></a>00950 
<a name="l00952"></a>00952   <span class="comment">// Gauss - Seidel //</span>
<a name="l00953"></a>00953 
<a name="l00954"></a>00954 
<a name="l00955"></a>00955   <span class="comment">// Solve X = M*Y with Gauss-Seidel method.</span>
<a name="l00956"></a>00956   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00957"></a>00957             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00958"></a>00958             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00959"></a>00959   <span class="keyword">inline</span> <span class="keywordtype">void</span> GaussSeidel(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l00960"></a>00960                           <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l00961"></a>00961                           Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l00962"></a>00962                           <span class="keywordtype">int</span> iter)
<a name="l00963"></a>00963   {
<a name="l00964"></a>00964     <span class="keywordtype">int</span> i, j, k;
<a name="l00965"></a>00965     T1 temp;
<a name="l00966"></a>00966 
<a name="l00967"></a>00967     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l00968"></a>00968     <span class="keywordtype">int</span> na = M.GetN();
<a name="l00969"></a>00969 
<a name="l00970"></a>00970 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00971"></a>00971 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l00972"></a>00972       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;GaussSeidel(M, X, Y, iter)&quot;</span>,
<a name="l00973"></a>00973                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l00974"></a>00974 
<a name="l00975"></a>00975     <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;GaussSeidel(M, X, Y, iter)&quot;</span>);
<a name="l00976"></a>00976 <span class="preprocessor">#endif</span>
<a name="l00977"></a>00977 <span class="preprocessor"></span>
<a name="l00978"></a>00978     <span class="keywordflow">for</span> (i = 0; i &lt; iter; i++)
<a name="l00979"></a>00979       <span class="keywordflow">for</span> (j = 0; j &lt; na; j++)
<a name="l00980"></a>00980         {
<a name="l00981"></a>00981           temp = 0;
<a name="l00982"></a>00982           <span class="keywordflow">for</span> (k = 0; k &lt; j; k++)
<a name="l00983"></a>00983             temp -= M(j, k) * Y(k);
<a name="l00984"></a>00984           <span class="keywordflow">for</span> (k = j + 1; k &lt; na; k++)
<a name="l00985"></a>00985             temp -= M(j, k) * Y(k);
<a name="l00986"></a>00986           Y(j) = (X(j) + temp) / M(j, j);
<a name="l00987"></a>00987         }
<a name="l00988"></a>00988   }
<a name="l00989"></a>00989 
<a name="l00990"></a>00990 
<a name="l00991"></a>00991   <span class="comment">// Gauss-Seidel //</span>
<a name="l00993"></a>00993 <span class="comment"></span>
<a name="l00994"></a>00994 
<a name="l00995"></a>00995 
<a name="l00997"></a>00997   <span class="comment">// S.O.R. method //</span>
<a name="l00998"></a>00998 
<a name="l00999"></a>00999 
<a name="l01000"></a>01000   <span class="comment">// Solve X = M*Y with S.O.R. method.</span>
<a name="l01001"></a>01001   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01002"></a>01002             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01003"></a>01003             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l01004"></a>01004             <span class="keyword">class </span>T3&gt;
<a name="l01005"></a>01005   <span class="keyword">inline</span> <span class="keywordtype">void</span> SOR(<span class="keyword">const</span> Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;&amp; M,
<a name="l01006"></a>01006                   <span class="keyword">const</span> Vector&lt;T1, Storage1, Allocator1&gt;&amp; X,
<a name="l01007"></a>01007                   Vector&lt;T2, Storage2, Allocator2&gt;&amp; Y,
<a name="l01008"></a>01008                   T3 omega,
<a name="l01009"></a>01009                   <span class="keywordtype">int</span> iter)
<a name="l01010"></a>01010   {
<a name="l01011"></a>01011     <span class="keywordtype">int</span> i, j, k;
<a name="l01012"></a>01012     T1 temp;
<a name="l01013"></a>01013 
<a name="l01014"></a>01014     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01015"></a>01015     <span class="keywordtype">int</span> na = M.GetN();
<a name="l01016"></a>01016 
<a name="l01017"></a>01017 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01018"></a>01018 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (na != ma)
<a name="l01019"></a>01019       <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;SOR(M, X, Y, omega, iter)&quot;</span>,
<a name="l01020"></a>01020                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01021"></a>01021 
<a name="l01022"></a>01022     <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, X, Y, <span class="stringliteral">&quot;SOR(M, X, Y, omega, iter)&quot;</span>);
<a name="l01023"></a>01023 <span class="preprocessor">#endif</span>
<a name="l01024"></a>01024 <span class="preprocessor"></span>
<a name="l01025"></a>01025     <span class="keywordflow">for</span> (i = 0; i &lt; iter; i++)
<a name="l01026"></a>01026       <span class="keywordflow">for</span> (j = 0; j &lt; na; j++)
<a name="l01027"></a>01027         {
<a name="l01028"></a>01028           temp = 0;
<a name="l01029"></a>01029           <span class="keywordflow">for</span> (k = 0; k &lt; j; k++)
<a name="l01030"></a>01030             temp -= M(j, k) * Y(k);
<a name="l01031"></a>01031           <span class="keywordflow">for</span> (k = j + 1; k &lt; na; k++)
<a name="l01032"></a>01032             temp -= M(j, k) * Y(k);
<a name="l01033"></a>01033           Y(j) = (T3(1) - omega) * Y(j) + omega * (X(j) + temp) / M(j, j);
<a name="l01034"></a>01034         }
<a name="l01035"></a>01035   }
<a name="l01036"></a>01036 
<a name="l01037"></a>01037 
<a name="l01038"></a>01038   <span class="comment">// S.O.R. method //</span>
<a name="l01040"></a>01040 <span class="comment"></span>
<a name="l01041"></a>01041 
<a name="l01042"></a>01042 
<a name="l01044"></a>01044   <span class="comment">// SolveLU //</span>
<a name="l01045"></a>01045 
<a name="l01046"></a>01046 
<a name="l01048"></a>01048 
<a name="l01060"></a>01060   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01061"></a>01061             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01062"></a><a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c">01062</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01063"></a>01063                <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; Y)
<a name="l01064"></a>01064   {
<a name="l01065"></a>01065     <span class="keywordtype">int</span> i, k;
<a name="l01066"></a>01066     T1 temp;
<a name="l01067"></a>01067 
<a name="l01068"></a>01068     <span class="keywordtype">int</span> ma = M.GetM();
<a name="l01069"></a>01069 
<a name="l01070"></a>01070 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l01071"></a>01071 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = M.GetN();
<a name="l01072"></a>01072     <span class="keywordflow">if</span> (na != ma)
<a name="l01073"></a>01073       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;SolveLU(M, Y)&quot;</span>,
<a name="l01074"></a>01074                      <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01075"></a>01075 
<a name="l01076"></a>01076     <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(M, Y, <span class="stringliteral">&quot;SolveLU(M, Y)&quot;</span>);
<a name="l01077"></a>01077 <span class="preprocessor">#endif</span>
<a name="l01078"></a>01078 <span class="preprocessor"></span>
<a name="l01079"></a>01079     <span class="comment">// Forward substitution.</span>
<a name="l01080"></a>01080     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l01081"></a>01081       {
<a name="l01082"></a>01082         temp = 0;
<a name="l01083"></a>01083         <span class="keywordflow">for</span> (k = 0; k &lt; i; k++)
<a name="l01084"></a>01084           temp += M(i, k) * Y(k);
<a name="l01085"></a>01085         Y(i) = (Y(i) - temp) / M(i, i);
<a name="l01086"></a>01086       }
<a name="l01087"></a>01087     <span class="comment">// Back substitution.</span>
<a name="l01088"></a>01088     <span class="keywordflow">for</span> (i = ma - 2; i &gt; -1; i--)
<a name="l01089"></a>01089       {
<a name="l01090"></a>01090         temp = 0;
<a name="l01091"></a>01091         <span class="keywordflow">for</span> (k = i + 1; k &lt; ma; k++)
<a name="l01092"></a>01092           temp += M(i, k) * Y(k);
<a name="l01093"></a>01093         Y(i) -= temp;
<a name="l01094"></a>01094       }
<a name="l01095"></a>01095   }
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01098"></a>01098   <span class="comment">// SolveLU //</span>
<a name="l01100"></a>01100 <span class="comment"></span>
<a name="l01101"></a>01101 
<a name="l01103"></a>01103   <span class="comment">// SOLVE //</span>
<a name="l01104"></a>01104 
<a name="l01105"></a>01105 
<a name="l01107"></a>01107 
<a name="l01114"></a>01114   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01115"></a>01115             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01116"></a><a class="code" href="namespace_seldon.php#ac173624f2f8a8e737e4f9c7b29ad95af">01116</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ac173624f2f8a8e737e4f9c7b29ad95af" title="Solves a linear system using LU factorization.">GetAndSolveLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01117"></a>01117                      <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; Y)
<a name="l01118"></a>01118   {
<a name="l01119"></a>01119 <span class="preprocessor">#ifdef SELDON_WITH_LAPACK</span>
<a name="l01120"></a>01120 <span class="preprocessor"></span>    <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> P;
<a name="l01121"></a>01121     <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(M, P);
<a name="l01122"></a>01122     <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(M, P, Y);
<a name="l01123"></a>01123 <span class="preprocessor">#else</span>
<a name="l01124"></a>01124 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(M);
<a name="l01125"></a>01125     <a class="code" href="namespace_seldon.php#ab5ba779ff733af5fb53362198084d54c" title="Solves a linear system whose matrix has been LU-factorized.">SolveLU</a>(M, Y);
<a name="l01126"></a>01126 <span class="preprocessor">#endif</span>
<a name="l01127"></a>01127 <span class="preprocessor"></span>  }
<a name="l01128"></a>01128 
<a name="l01129"></a>01129 
<a name="l01130"></a>01130   <span class="comment">// SOLVE //</span>
<a name="l01132"></a>01132 <span class="comment"></span>
<a name="l01133"></a>01133 
<a name="l01134"></a>01134 
<a name="l01136"></a>01136   <span class="comment">// CHECKDIM //</span>
<a name="l01137"></a>01137 
<a name="l01138"></a>01138 
<a name="l01140"></a>01140 
<a name="l01149"></a>01149   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01150"></a>01150             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01151"></a>01151             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01152"></a><a class="code" href="namespace_seldon.php#af241a517d55fd5c8acbd017d7fed1b78">01152</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01153"></a>01153                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l01154"></a>01154                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y,
<a name="l01155"></a>01155                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01156"></a>01156   {
<a name="l01157"></a>01157     <span class="keywordflow">if</span> (X.GetLength() != M.GetN() || Y.GetLength() != M.GetM())
<a name="l01158"></a>01158       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01159"></a>01159                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01160"></a>01160                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01161"></a>01161                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01162"></a>01162                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01163"></a>01163                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01164"></a>01164                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01165"></a>01165                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01166"></a>01166   }
<a name="l01167"></a>01167 
<a name="l01168"></a>01168 
<a name="l01170"></a>01170 
<a name="l01179"></a>01179   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l01180"></a>01180             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01181"></a>01181             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01182"></a><a class="code" href="namespace_seldon.php#a1fbed792656c2b24cb5bf05da21743d9">01182</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowMajorCollection, Allocator0&gt;</a>&amp; M,
<a name="l01183"></a>01183                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; X,
<a name="l01184"></a>01184                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Collection, Allocator2&gt;</a>&amp; Y,
<a name="l01185"></a>01185                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01186"></a>01186   {
<a name="l01187"></a>01187     <span class="keywordflow">if</span> (X.GetNvector() != M.GetNmatrix() || Y.GetNvector() != M.GetMmatrix())
<a name="l01188"></a>01188       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01189"></a>01189                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01190"></a>01190                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01191"></a>01191                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01192"></a>01192                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01193"></a>01193                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01194"></a>01194                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01195"></a>01195                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01196"></a>01196   }
<a name="l01197"></a>01197 
<a name="l01198"></a>01198 
<a name="l01200"></a>01200 
<a name="l01209"></a>01209   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l01210"></a>01210             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01211"></a>01211             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01212"></a><a class="code" href="namespace_seldon.php#ab331d66d3471101f87c309fef8e9269d">01212</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, ColMajorCollection, Allocator0&gt;</a>&amp; M,
<a name="l01213"></a>01213                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; X,
<a name="l01214"></a>01214                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Collection, Allocator2&gt;</a>&amp; Y,
<a name="l01215"></a>01215                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01216"></a>01216   {
<a name="l01217"></a>01217     <span class="keywordflow">if</span> (X.GetNvector() != M.GetNmatrix() || Y.GetNvector() != M.GetMmatrix())
<a name="l01218"></a>01218       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01219"></a>01219                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01220"></a>01220                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01221"></a>01221                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01222"></a>01222                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01223"></a>01223                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01224"></a>01224                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01225"></a>01225                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetNvector()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01226"></a>01226   }
<a name="l01227"></a>01227 
<a name="l01228"></a>01228 
<a name="l01230"></a>01230 
<a name="l01239"></a>01239   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01240"></a>01240             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l01241"></a>01241             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01242"></a><a class="code" href="namespace_seldon.php#a0fc37866ecf481c0319d2a8f38bfeec6">01242</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01243"></a>01243                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Collection, Allocator1&gt;</a>&amp; X,
<a name="l01244"></a>01244                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y,
<a name="l01245"></a>01245                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01246"></a>01246   {
<a name="l01247"></a>01247     <span class="keywordflow">if</span> (X.GetLength() != M.GetN() || Y.GetLength() != M.GetM())
<a name="l01248"></a>01248       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>)
<a name="l01249"></a>01249                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01250"></a>01250                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01251"></a>01251                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01252"></a>01252                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01253"></a>01253                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01254"></a>01254                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01255"></a>01255                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01256"></a>01256   }
<a name="l01257"></a>01257 
<a name="l01258"></a>01258 
<a name="l01260"></a>01260 
<a name="l01272"></a>01272   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01273"></a>01273             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01274"></a>01274             <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01275"></a><a class="code" href="namespace_seldon.php#add3736fe94390ef4f00e107eb33362f7">01275</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; trans,
<a name="l01276"></a>01276                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01277"></a>01277                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l01278"></a>01278                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, Storage2, Allocator2&gt;</a>&amp; Y,
<a name="l01279"></a>01279                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;M X + Y -&gt; Y&quot;</span>)
<a name="l01280"></a>01280   {
<a name="l01281"></a>01281     <span class="keywordflow">if</span> (op == <span class="stringliteral">&quot;M X + Y -&gt; Y&quot;</span>)
<a name="l01282"></a>01282       <span class="keywordflow">if</span> (trans.Trans())
<a name="l01283"></a>01283         op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M&#39; X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01284"></a>01284       <span class="keywordflow">else</span> <span class="keywordflow">if</span> (trans.ConjTrans())
<a name="l01285"></a>01285         op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation M* X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01286"></a>01286       <span class="keywordflow">else</span>
<a name="l01287"></a>01287         op = string(<span class="stringliteral">&quot;Operation M X + Y -&gt; Y not permitted:&quot;</span>);
<a name="l01288"></a>01288     <span class="keywordflow">else</span>
<a name="l01289"></a>01289       op = string(<span class="stringliteral">&quot;Operation &quot;</span>) + op + string(<span class="stringliteral">&quot; not permitted:&quot;</span>);
<a name="l01290"></a>01290     <span class="keywordflow">if</span> (X.GetLength() != M.GetN(trans) || Y.GetLength() != M.GetM(trans))
<a name="l01291"></a>01291       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, op + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M)
<a name="l01292"></a>01292                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01293"></a>01293                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>)
<a name="l01294"></a>01294                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01295"></a>01295                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;;\n     Y (&quot;</span>)
<a name="l01296"></a>01296                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;Y) + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01297"></a>01297                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Y.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01298"></a>01298   }
<a name="l01299"></a>01299 
<a name="l01300"></a>01300 
<a name="l01302"></a>01302 
<a name="l01311"></a>01311   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01312"></a>01312             <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01313"></a><a class="code" href="namespace_seldon.php#ae0a0bc26ae17483851c00efeddcfa325">01313</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; M,
<a name="l01314"></a>01314                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T1, Storage1, Allocator1&gt;</a>&amp; X,
<a name="l01315"></a>01315                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>, <span class="keywordtype">string</span> op = <span class="stringliteral">&quot;M X&quot;</span>)
<a name="l01316"></a>01316   {
<a name="l01317"></a>01317     <span class="keywordflow">if</span> (X.GetLength() != M.GetN())
<a name="l01318"></a>01318       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation &quot;</span>) + op + <span class="stringliteral">&quot; not permitted:&quot;</span>
<a name="l01319"></a>01319                      + string(<span class="stringliteral">&quot;\n     M (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;M) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01320"></a>01320                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(M.GetN())
<a name="l01321"></a>01321                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     X (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;X)
<a name="l01322"></a>01322                      + string(<span class="stringliteral">&quot;) is vector of length &quot;</span>)
<a name="l01323"></a>01323                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(X.GetLength()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;.&quot;</span>));
<a name="l01324"></a>01324   }
<a name="l01325"></a>01325 
<a name="l01326"></a>01326 
<a name="l01327"></a>01327   <span class="comment">// CHECKDIM //</span>
<a name="l01329"></a>01329 <span class="comment"></span>
<a name="l01330"></a>01330 
<a name="l01331"></a>01331 }  <span class="comment">// namespace Seldon.</span>
<a name="l01332"></a>01332 
<a name="l01333"></a>01333 <span class="preprocessor">#define SELDON_FUNCTIONS_MATVECT_CXX</span>
<a name="l01334"></a>01334 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
