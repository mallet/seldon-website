<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/interfaces/cblas.h</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Headers from CBLAS, downloaded at http://www.netlib.org/blas/.</span>
<a name="l00002"></a>00002 
<a name="l00003"></a>00003 <span class="preprocessor">#ifndef CBLAS_H</span>
<a name="l00004"></a>00004 <span class="preprocessor"></span><span class="preprocessor">#define CBLAS_H</span>
<a name="l00005"></a>00005 <span class="preprocessor"></span><span class="preprocessor">#include &lt;stddef.h&gt;</span>
<a name="l00006"></a>00006 
<a name="l00007"></a>00007 <span class="comment">/*</span>
<a name="l00008"></a>00008 <span class="comment"> * Enumerated and derived types</span>
<a name="l00009"></a>00009 <span class="comment"> */</span>
<a name="l00010"></a>00010 <span class="preprocessor">#define CBLAS_INDEX size_t  </span><span class="comment">/* this may vary between platforms */</span>
<a name="l00011"></a>00011 
<a name="l00012"></a>00012 <span class="keyword">enum</span> CBLAS_ORDER {CblasRowMajor=101, CblasColMajor=102};
<a name="l00013"></a>00013 <span class="keyword">enum</span> CBLAS_TRANSPOSE {CblasNoTrans=111, CblasTrans=112, CblasConjTrans=113};
<a name="l00014"></a>00014 <span class="keyword">enum</span> CBLAS_UPLO {CblasUpper=121, CblasLower=122};
<a name="l00015"></a>00015 <span class="keyword">enum</span> CBLAS_DIAG {CblasNonUnit=131, CblasUnit=132};
<a name="l00016"></a>00016 <span class="keyword">enum</span> CBLAS_SIDE {CblasLeft=141, CblasRight=142};
<a name="l00017"></a>00017 
<a name="l00018"></a>00018 <span class="comment">/*</span>
<a name="l00019"></a>00019 <span class="comment"> * ===========================================================================</span>
<a name="l00020"></a>00020 <span class="comment"> * Prototypes for level 1 BLAS functions (complex are recast as routines)</span>
<a name="l00021"></a>00021 <span class="comment"> * ===========================================================================</span>
<a name="l00022"></a>00022 <span class="comment"> */</span>
<a name="l00023"></a>00023 <span class="keywordtype">float</span>  cblas_sdsdot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00024"></a>00024                     <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00025"></a>00025 <span class="keywordtype">double</span> cblas_dsdot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> *Y,
<a name="l00026"></a>00026                    <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00027"></a>00027 <span class="keywordtype">float</span>  cblas_sdot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span>  *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00028"></a>00028                   <span class="keyword">const</span> <span class="keywordtype">float</span>  *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00029"></a>00029 <span class="keywordtype">double</span> cblas_ddot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00030"></a>00030                   <span class="keyword">const</span> <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 <span class="comment">/*</span>
<a name="l00033"></a>00033 <span class="comment"> * Functions having prefixes Z and C only</span>
<a name="l00034"></a>00034 <span class="comment"> */</span>
<a name="l00035"></a>00035 <span class="keywordtype">void</span>   cblas_cdotu_sub(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00036"></a>00036                        <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *dotu);
<a name="l00037"></a>00037 <span class="keywordtype">void</span>   cblas_cdotc_sub(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00038"></a>00038                        <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *dotc);
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 <span class="keywordtype">void</span>   cblas_zdotu_sub(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00041"></a>00041                        <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *dotu);
<a name="l00042"></a>00042 <span class="keywordtype">void</span>   cblas_zdotc_sub(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00043"></a>00043                        <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *dotc);
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00046"></a>00046 <span class="comment">/*</span>
<a name="l00047"></a>00047 <span class="comment"> * Functions having prefixes S D SC DZ</span>
<a name="l00048"></a>00048 <span class="comment"> */</span>
<a name="l00049"></a>00049 <span class="keywordtype">float</span>  cblas_snrm2(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00050"></a>00050 <span class="keywordtype">float</span>  cblas_sasum(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 <span class="keywordtype">double</span> cblas_dnrm2(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00053"></a>00053 <span class="keywordtype">double</span> cblas_dasum(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00054"></a>00054 
<a name="l00055"></a>00055 <span class="keywordtype">float</span>  cblas_scnrm2(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00056"></a>00056 <span class="keywordtype">float</span>  cblas_scasum(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 <span class="keywordtype">double</span> cblas_dznrm2(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00059"></a>00059 <span class="keywordtype">double</span> cblas_dzasum(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 <span class="comment">/*</span>
<a name="l00063"></a>00063 <span class="comment"> * Functions having standard 4 prefixes (S D C Z)</span>
<a name="l00064"></a>00064 <span class="comment"> */</span>
<a name="l00065"></a>00065 CBLAS_INDEX cblas_isamax(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span>  *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00066"></a>00066 CBLAS_INDEX cblas_idamax(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00067"></a>00067 CBLAS_INDEX cblas_icamax(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span>   *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00068"></a>00068 CBLAS_INDEX cblas_izamax(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span>   *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00069"></a>00069 
<a name="l00070"></a>00070 <span class="comment">/*</span>
<a name="l00071"></a>00071 <span class="comment"> * ===========================================================================</span>
<a name="l00072"></a>00072 <span class="comment"> * Prototypes for level 1 BLAS routines</span>
<a name="l00073"></a>00073 <span class="comment"> * ===========================================================================</span>
<a name="l00074"></a>00074 <span class="comment"> */</span>
<a name="l00075"></a>00075 
<a name="l00076"></a>00076 <span class="comment">/* </span>
<a name="l00077"></a>00077 <span class="comment"> * Routines with standard 4 prefixes (s, d, c, z)</span>
<a name="l00078"></a>00078 <span class="comment"> */</span>
<a name="l00079"></a>00079 <span class="keywordtype">void</span> cblas_sswap(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00080"></a>00080                  <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00081"></a>00081 <span class="keywordtype">void</span> cblas_scopy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00082"></a>00082                  <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00083"></a>00083 <span class="keywordtype">void</span> cblas_saxpy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00084"></a>00084                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00085"></a>00085 
<a name="l00086"></a>00086 <span class="keywordtype">void</span> cblas_dswap(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00087"></a>00087                  <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00088"></a>00088 <span class="keywordtype">void</span> cblas_dcopy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00089"></a>00089                  <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00090"></a>00090 <span class="keywordtype">void</span> cblas_daxpy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00091"></a>00091                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00092"></a>00092 
<a name="l00093"></a>00093 <span class="keywordtype">void</span> cblas_cswap(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00094"></a>00094                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00095"></a>00095 <span class="keywordtype">void</span> cblas_ccopy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00096"></a>00096                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00097"></a>00097 <span class="keywordtype">void</span> cblas_caxpy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00098"></a>00098                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00099"></a>00099 
<a name="l00100"></a>00100 <span class="keywordtype">void</span> cblas_zswap(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00101"></a>00101                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00102"></a>00102 <span class="keywordtype">void</span> cblas_zcopy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, 
<a name="l00103"></a>00103                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00104"></a>00104 <span class="keywordtype">void</span> cblas_zaxpy(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00105"></a>00105                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00106"></a>00106 
<a name="l00107"></a>00107 
<a name="l00108"></a>00108 <span class="comment">/* </span>
<a name="l00109"></a>00109 <span class="comment"> * Routines with S and D prefix only</span>
<a name="l00110"></a>00110 <span class="comment"> */</span>
<a name="l00111"></a>00111 <span class="keywordtype">void</span> cblas_srotg(<span class="keywordtype">float</span> *a, <span class="keywordtype">float</span> *b, <span class="keywordtype">float</span> *c, <span class="keywordtype">float</span> *s);
<a name="l00112"></a>00112 <span class="keywordtype">void</span> cblas_srotmg(<span class="keywordtype">float</span> *d1, <span class="keywordtype">float</span> *d2, <span class="keywordtype">float</span> *b1, <span class="keyword">const</span> <span class="keywordtype">float</span> b2, <span class="keywordtype">float</span> *P);
<a name="l00113"></a>00113 <span class="keywordtype">void</span> cblas_srot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00114"></a>00114                 <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keyword">const</span> <span class="keywordtype">float</span> c, <span class="keyword">const</span> <span class="keywordtype">float</span> s);
<a name="l00115"></a>00115 <span class="keywordtype">void</span> cblas_srotm(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00116"></a>00116                 <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keyword">const</span> <span class="keywordtype">float</span> *P);
<a name="l00117"></a>00117 
<a name="l00118"></a>00118 <span class="keywordtype">void</span> cblas_drotg(<span class="keywordtype">double</span> *a, <span class="keywordtype">double</span> *b, <span class="keywordtype">double</span> *c, <span class="keywordtype">double</span> *s);
<a name="l00119"></a>00119 <span class="keywordtype">void</span> cblas_drotmg(<span class="keywordtype">double</span> *d1, <span class="keywordtype">double</span> *d2, <span class="keywordtype">double</span> *b1, <span class="keyword">const</span> <span class="keywordtype">double</span> b2, <span class="keywordtype">double</span> *P);
<a name="l00120"></a>00120 <span class="keywordtype">void</span> cblas_drot(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00121"></a>00121                 <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keyword">const</span> <span class="keywordtype">double</span> c, <span class="keyword">const</span> <span class="keywordtype">double</span>  s);
<a name="l00122"></a>00122 <span class="keywordtype">void</span> cblas_drotm(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00123"></a>00123                 <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keyword">const</span> <span class="keywordtype">double</span> *P);
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 
<a name="l00126"></a>00126 <span class="comment">/* </span>
<a name="l00127"></a>00127 <span class="comment"> * Routines with S D C Z CS and ZD prefixes</span>
<a name="l00128"></a>00128 <span class="comment"> */</span>
<a name="l00129"></a>00129 <span class="keywordtype">void</span> cblas_sscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00130"></a>00130 <span class="keywordtype">void</span> cblas_dscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00131"></a>00131 <span class="keywordtype">void</span> cblas_cscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00132"></a>00132 <span class="keywordtype">void</span> cblas_zscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00133"></a>00133 <span class="keywordtype">void</span> cblas_csscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00134"></a>00134 <span class="keywordtype">void</span> cblas_zdscal(<span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00135"></a>00135 
<a name="l00136"></a>00136 <span class="comment">/*</span>
<a name="l00137"></a>00137 <span class="comment"> * ===========================================================================</span>
<a name="l00138"></a>00138 <span class="comment"> * Prototypes for level 2 BLAS</span>
<a name="l00139"></a>00139 <span class="comment"> * ===========================================================================</span>
<a name="l00140"></a>00140 <span class="comment"> */</span>
<a name="l00141"></a>00141 
<a name="l00142"></a>00142 <span class="comment">/* </span>
<a name="l00143"></a>00143 <span class="comment"> * Routines with standard 4 prefixes (S, D, C, Z)</span>
<a name="l00144"></a>00144 <span class="comment"> */</span>
<a name="l00145"></a>00145 <span class="keywordtype">void</span> cblas_sgemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00146"></a>00146                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00147"></a>00147                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00148"></a>00148                  <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> beta,
<a name="l00149"></a>00149                  <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00150"></a>00150 <span class="keywordtype">void</span> cblas_sgbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00151"></a>00151                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00152"></a>00152                  <span class="keyword">const</span> <span class="keywordtype">int</span> KL, <span class="keyword">const</span> <span class="keywordtype">int</span> KU, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha,
<a name="l00153"></a>00153                  <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00154"></a>00154                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00155"></a>00155 <span class="keywordtype">void</span> cblas_strmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00156"></a>00156                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00157"></a>00157                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00158"></a>00158                  <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00159"></a>00159 <span class="keywordtype">void</span> cblas_stbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00160"></a>00160                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00161"></a>00161                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00162"></a>00162                  <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00163"></a>00163 <span class="keywordtype">void</span> cblas_stpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00164"></a>00164                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00165"></a>00165                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *Ap, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00166"></a>00166 <span class="keywordtype">void</span> cblas_strsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00167"></a>00167                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00168"></a>00168                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keywordtype">float</span> *X,
<a name="l00169"></a>00169                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00170"></a>00170 <span class="keywordtype">void</span> cblas_stbsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00171"></a>00171                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00172"></a>00172                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00173"></a>00173                  <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00174"></a>00174 <span class="keywordtype">void</span> cblas_stpsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00175"></a>00175                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00176"></a>00176                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> *Ap, <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00177"></a>00177 
<a name="l00178"></a>00178 <span class="keywordtype">void</span> cblas_dgemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00179"></a>00179                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00180"></a>00180                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00181"></a>00181                  <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">double</span> beta,
<a name="l00182"></a>00182                  <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00183"></a>00183 <span class="keywordtype">void</span> cblas_dgbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00184"></a>00184                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00185"></a>00185                  <span class="keyword">const</span> <span class="keywordtype">int</span> KL, <span class="keyword">const</span> <span class="keywordtype">int</span> KU, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha,
<a name="l00186"></a>00186                  <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00187"></a>00187                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00188"></a>00188 <span class="keywordtype">void</span> cblas_dtrmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00189"></a>00189                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00190"></a>00190                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00191"></a>00191                  <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00192"></a>00192 <span class="keywordtype">void</span> cblas_dtbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00193"></a>00193                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00194"></a>00194                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00195"></a>00195                  <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00196"></a>00196 <span class="keywordtype">void</span> cblas_dtpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00197"></a>00197                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00198"></a>00198                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *Ap, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00199"></a>00199 <span class="keywordtype">void</span> cblas_dtrsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00200"></a>00200                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00201"></a>00201                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keywordtype">double</span> *X,
<a name="l00202"></a>00202                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00203"></a>00203 <span class="keywordtype">void</span> cblas_dtbsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00204"></a>00204                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00205"></a>00205                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00206"></a>00206                  <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00207"></a>00207 <span class="keywordtype">void</span> cblas_dtpsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00208"></a>00208                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00209"></a>00209                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> *Ap, <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00210"></a>00210 
<a name="l00211"></a>00211 <span class="keywordtype">void</span> cblas_cgemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00212"></a>00212                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00213"></a>00213                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00214"></a>00214                  <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00215"></a>00215                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00216"></a>00216 <span class="keywordtype">void</span> cblas_cgbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00217"></a>00217                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00218"></a>00218                  <span class="keyword">const</span> <span class="keywordtype">int</span> KL, <span class="keyword">const</span> <span class="keywordtype">int</span> KU, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha,
<a name="l00219"></a>00219                  <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00220"></a>00220                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00221"></a>00221 <span class="keywordtype">void</span> cblas_ctrmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00222"></a>00222                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00223"></a>00223                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00224"></a>00224                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00225"></a>00225 <span class="keywordtype">void</span> cblas_ctbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00226"></a>00226                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00227"></a>00227                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00228"></a>00228                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00229"></a>00229 <span class="keywordtype">void</span> cblas_ctpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00230"></a>00230                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00231"></a>00231                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00232"></a>00232 <span class="keywordtype">void</span> cblas_ctrsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00233"></a>00233                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00234"></a>00234                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keywordtype">void</span> *X,
<a name="l00235"></a>00235                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00236"></a>00236 <span class="keywordtype">void</span> cblas_ctbsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00237"></a>00237                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00238"></a>00238                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00239"></a>00239                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00240"></a>00240 <span class="keywordtype">void</span> cblas_ctpsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00241"></a>00241                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00242"></a>00242                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00243"></a>00243 
<a name="l00244"></a>00244 <span class="keywordtype">void</span> cblas_zgemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00245"></a>00245                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00246"></a>00246                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00247"></a>00247                  <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00248"></a>00248                  <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00249"></a>00249 <span class="keywordtype">void</span> cblas_zgbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order,
<a name="l00250"></a>00250                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00251"></a>00251                  <span class="keyword">const</span> <span class="keywordtype">int</span> KL, <span class="keyword">const</span> <span class="keywordtype">int</span> KU, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha,
<a name="l00252"></a>00252                  <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00253"></a>00253                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00254"></a>00254 <span class="keywordtype">void</span> cblas_ztrmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00255"></a>00255                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00256"></a>00256                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00257"></a>00257                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00258"></a>00258 <span class="keywordtype">void</span> cblas_ztbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00259"></a>00259                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00260"></a>00260                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, 
<a name="l00261"></a>00261                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00262"></a>00262 <span class="keywordtype">void</span> cblas_ztpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00263"></a>00263                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00264"></a>00264                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00265"></a>00265 <span class="keywordtype">void</span> cblas_ztrsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00266"></a>00266                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00267"></a>00267                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keywordtype">void</span> *X,
<a name="l00268"></a>00268                  <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00269"></a>00269 <span class="keywordtype">void</span> cblas_ztbsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00270"></a>00270                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00271"></a>00271                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00272"></a>00272                  <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00273"></a>00273 <span class="keywordtype">void</span> cblas_ztpsv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00274"></a>00274                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag,
<a name="l00275"></a>00275                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap, <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX);
<a name="l00276"></a>00276 
<a name="l00277"></a>00277 
<a name="l00278"></a>00278 <span class="comment">/* </span>
<a name="l00279"></a>00279 <span class="comment"> * Routines with S and D prefixes only</span>
<a name="l00280"></a>00280 <span class="comment"> */</span>
<a name="l00281"></a>00281 <span class="keywordtype">void</span> cblas_ssymv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00282"></a>00282                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A,
<a name="l00283"></a>00283                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00284"></a>00284                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00285"></a>00285 <span class="keywordtype">void</span> cblas_ssbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00286"></a>00286                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A,
<a name="l00287"></a>00287                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00288"></a>00288                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00289"></a>00289 <span class="keywordtype">void</span> cblas_sspmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00290"></a>00290                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *Ap,
<a name="l00291"></a>00291                  <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00292"></a>00292                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00293"></a>00293 <span class="keywordtype">void</span> cblas_sger(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00294"></a>00294                 <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00295"></a>00295                 <span class="keyword">const</span> <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00296"></a>00296 <span class="keywordtype">void</span> cblas_ssyr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00297"></a>00297                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00298"></a>00298                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00299"></a>00299 <span class="keywordtype">void</span> cblas_sspr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00300"></a>00300                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00301"></a>00301                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">float</span> *Ap);
<a name="l00302"></a>00302 <span class="keywordtype">void</span> cblas_ssyr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00303"></a>00303                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00304"></a>00304                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">float</span> *A,
<a name="l00305"></a>00305                 <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00306"></a>00306 <span class="keywordtype">void</span> cblas_sspr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00307"></a>00307                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *X,
<a name="l00308"></a>00308                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">float</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">float</span> *A);
<a name="l00309"></a>00309 
<a name="l00310"></a>00310 <span class="keywordtype">void</span> cblas_dsymv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00311"></a>00311                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A,
<a name="l00312"></a>00312                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00313"></a>00313                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00314"></a>00314 <span class="keywordtype">void</span> cblas_dsbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00315"></a>00315                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A,
<a name="l00316"></a>00316                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00317"></a>00317                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00318"></a>00318 <span class="keywordtype">void</span> cblas_dspmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00319"></a>00319                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *Ap,
<a name="l00320"></a>00320                  <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00321"></a>00321                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00322"></a>00322 <span class="keywordtype">void</span> cblas_dger(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00323"></a>00323                 <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00324"></a>00324                 <span class="keyword">const</span> <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00325"></a>00325 <span class="keywordtype">void</span> cblas_dsyr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00326"></a>00326                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00327"></a>00327                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00328"></a>00328 <span class="keywordtype">void</span> cblas_dspr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00329"></a>00329                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00330"></a>00330                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">double</span> *Ap);
<a name="l00331"></a>00331 <span class="keywordtype">void</span> cblas_dsyr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00332"></a>00332                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00333"></a>00333                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">double</span> *A,
<a name="l00334"></a>00334                 <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00335"></a>00335 <span class="keywordtype">void</span> cblas_dspr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00336"></a>00336                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *X,
<a name="l00337"></a>00337                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keyword">const</span> <span class="keywordtype">double</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">double</span> *A);
<a name="l00338"></a>00338 
<a name="l00339"></a>00339 
<a name="l00340"></a>00340 <span class="comment">/* </span>
<a name="l00341"></a>00341 <span class="comment"> * Routines with C and Z prefixes only</span>
<a name="l00342"></a>00342 <span class="comment"> */</span>
<a name="l00343"></a>00343 <span class="keywordtype">void</span> cblas_chemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00344"></a>00344                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00345"></a>00345                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00346"></a>00346                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00347"></a>00347 <span class="keywordtype">void</span> cblas_chbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00348"></a>00348                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00349"></a>00349                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00350"></a>00350                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00351"></a>00351 <span class="keywordtype">void</span> cblas_chpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00352"></a>00352                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap,
<a name="l00353"></a>00353                  <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00354"></a>00354                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00355"></a>00355 <span class="keywordtype">void</span> cblas_cgeru(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00356"></a>00356                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00357"></a>00357                  <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00358"></a>00358 <span class="keywordtype">void</span> cblas_cgerc(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00359"></a>00359                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00360"></a>00360                  <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00361"></a>00361 <span class="keywordtype">void</span> cblas_cher(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00362"></a>00362                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00363"></a>00363                 <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00364"></a>00364 <span class="keywordtype">void</span> cblas_chpr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00365"></a>00365                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00366"></a>00366                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">void</span> *A);
<a name="l00367"></a>00367 <span class="keywordtype">void</span> cblas_cher2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00368"></a>00368                 <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00369"></a>00369                 <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00370"></a>00370 <span class="keywordtype">void</span> cblas_chpr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00371"></a>00371                 <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00372"></a>00372                 <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *Ap);
<a name="l00373"></a>00373 
<a name="l00374"></a>00374 <span class="keywordtype">void</span> cblas_zhemv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00375"></a>00375                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00376"></a>00376                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00377"></a>00377                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00378"></a>00378 <span class="keywordtype">void</span> cblas_zhbmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00379"></a>00379                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00380"></a>00380                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00381"></a>00381                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00382"></a>00382 <span class="keywordtype">void</span> cblas_zhpmv(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00383"></a>00383                  <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *Ap,
<a name="l00384"></a>00384                  <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00385"></a>00385                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY);
<a name="l00386"></a>00386 <span class="keywordtype">void</span> cblas_zgeru(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00387"></a>00387                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00388"></a>00388                  <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00389"></a>00389 <span class="keywordtype">void</span> cblas_zgerc(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00390"></a>00390                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00391"></a>00391                  <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00392"></a>00392 <span class="keywordtype">void</span> cblas_zher(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00393"></a>00393                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00394"></a>00394                 <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00395"></a>00395 <span class="keywordtype">void</span> cblas_zhpr(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00396"></a>00396                 <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X,
<a name="l00397"></a>00397                 <span class="keyword">const</span> <span class="keywordtype">int</span> incX, <span class="keywordtype">void</span> *A);
<a name="l00398"></a>00398 <span class="keywordtype">void</span> cblas_zher2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00399"></a>00399                 <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00400"></a>00400                 <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda);
<a name="l00401"></a>00401 <span class="keywordtype">void</span> cblas_zhpr2(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00402"></a>00402                 <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *X, <span class="keyword">const</span> <span class="keywordtype">int</span> incX,
<a name="l00403"></a>00403                 <span class="keyword">const</span> <span class="keywordtype">void</span> *Y, <span class="keyword">const</span> <span class="keywordtype">int</span> incY, <span class="keywordtype">void</span> *Ap);
<a name="l00404"></a>00404 
<a name="l00405"></a>00405 <span class="comment">/*</span>
<a name="l00406"></a>00406 <span class="comment"> * ===========================================================================</span>
<a name="l00407"></a>00407 <span class="comment"> * Prototypes for level 3 BLAS</span>
<a name="l00408"></a>00408 <span class="comment"> * ===========================================================================</span>
<a name="l00409"></a>00409 <span class="comment"> */</span>
<a name="l00410"></a>00410 
<a name="l00411"></a>00411 <span class="comment">/* </span>
<a name="l00412"></a>00412 <span class="comment"> * Routines with standard 4 prefixes (S, D, C, Z)</span>
<a name="l00413"></a>00413 <span class="comment"> */</span>
<a name="l00414"></a>00414 <span class="keywordtype">void</span> cblas_sgemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00415"></a>00415                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransB, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00416"></a>00416                  <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A,
<a name="l00417"></a>00417                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">float</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb,
<a name="l00418"></a>00418                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00419"></a>00419 <span class="keywordtype">void</span> cblas_ssymm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00420"></a>00420                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00421"></a>00421                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00422"></a>00422                  <span class="keyword">const</span> <span class="keywordtype">float</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">float</span> beta,
<a name="l00423"></a>00423                  <span class="keywordtype">float</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00424"></a>00424 <span class="keywordtype">void</span> cblas_ssyrk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00425"></a>00425                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00426"></a>00426                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00427"></a>00427                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">float</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00428"></a>00428 <span class="keywordtype">void</span> cblas_ssyr2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00429"></a>00429                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00430"></a>00430                   <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00431"></a>00431                   <span class="keyword">const</span> <span class="keywordtype">float</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">float</span> beta,
<a name="l00432"></a>00432                   <span class="keywordtype">float</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00433"></a>00433 <span class="keywordtype">void</span> cblas_strmm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00434"></a>00434                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00435"></a>00435                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00436"></a>00436                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00437"></a>00437                  <span class="keywordtype">float</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00438"></a>00438 <span class="keywordtype">void</span> cblas_strsm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00439"></a>00439                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00440"></a>00440                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00441"></a>00441                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">float</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00442"></a>00442                  <span class="keywordtype">float</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00443"></a>00443 
<a name="l00444"></a>00444 <span class="keywordtype">void</span> cblas_dgemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00445"></a>00445                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransB, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00446"></a>00446                  <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A,
<a name="l00447"></a>00447                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">double</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb,
<a name="l00448"></a>00448                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00449"></a>00449 <span class="keywordtype">void</span> cblas_dsymm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00450"></a>00450                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00451"></a>00451                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00452"></a>00452                  <span class="keyword">const</span> <span class="keywordtype">double</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">double</span> beta,
<a name="l00453"></a>00453                  <span class="keywordtype">double</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00454"></a>00454 <span class="keywordtype">void</span> cblas_dsyrk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00455"></a>00455                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00456"></a>00456                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00457"></a>00457                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">double</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00458"></a>00458 <span class="keywordtype">void</span> cblas_dsyr2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00459"></a>00459                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00460"></a>00460                   <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00461"></a>00461                   <span class="keyword">const</span> <span class="keywordtype">double</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">double</span> beta,
<a name="l00462"></a>00462                   <span class="keywordtype">double</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00463"></a>00463 <span class="keywordtype">void</span> cblas_dtrmm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00464"></a>00464                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00465"></a>00465                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00466"></a>00466                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00467"></a>00467                  <span class="keywordtype">double</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00468"></a>00468 <span class="keywordtype">void</span> cblas_dtrsm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00469"></a>00469                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00470"></a>00470                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00471"></a>00471                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">double</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00472"></a>00472                  <span class="keywordtype">double</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00473"></a>00473 
<a name="l00474"></a>00474 <span class="keywordtype">void</span> cblas_cgemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00475"></a>00475                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransB, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00476"></a>00476                  <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00477"></a>00477                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb,
<a name="l00478"></a>00478                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00479"></a>00479 <span class="keywordtype">void</span> cblas_csymm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00480"></a>00480                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00481"></a>00481                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00482"></a>00482                  <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00483"></a>00483                  <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00484"></a>00484 <span class="keywordtype">void</span> cblas_csyrk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00485"></a>00485                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00486"></a>00486                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00487"></a>00487                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00488"></a>00488 <span class="keywordtype">void</span> cblas_csyr2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00489"></a>00489                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00490"></a>00490                   <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00491"></a>00491                   <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00492"></a>00492                   <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00493"></a>00493 <span class="keywordtype">void</span> cblas_ctrmm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00494"></a>00494                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00495"></a>00495                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00496"></a>00496                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00497"></a>00497                  <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00498"></a>00498 <span class="keywordtype">void</span> cblas_ctrsm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00499"></a>00499                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00500"></a>00500                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00501"></a>00501                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00502"></a>00502                  <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00503"></a>00503 
<a name="l00504"></a>00504 <span class="keywordtype">void</span> cblas_zgemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00505"></a>00505                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransB, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00506"></a>00506                  <span class="keyword">const</span> <span class="keywordtype">int</span> K, <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A,
<a name="l00507"></a>00507                  <span class="keyword">const</span> <span class="keywordtype">int</span> lda, <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb,
<a name="l00508"></a>00508                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00509"></a>00509 <span class="keywordtype">void</span> cblas_zsymm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00510"></a>00510                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00511"></a>00511                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00512"></a>00512                  <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00513"></a>00513                  <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00514"></a>00514 <span class="keywordtype">void</span> cblas_zsyrk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00515"></a>00515                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00516"></a>00516                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00517"></a>00517                  <span class="keyword">const</span> <span class="keywordtype">void</span> *beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00518"></a>00518 <span class="keywordtype">void</span> cblas_zsyr2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00519"></a>00519                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00520"></a>00520                   <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00521"></a>00521                   <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00522"></a>00522                   <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00523"></a>00523 <span class="keywordtype">void</span> cblas_ztrmm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00524"></a>00524                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00525"></a>00525                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00526"></a>00526                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00527"></a>00527                  <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00528"></a>00528 <span class="keywordtype">void</span> cblas_ztrsm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00529"></a>00529                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE TransA,
<a name="l00530"></a>00530                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_DIAG Diag, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00531"></a>00531                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00532"></a>00532                  <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb);
<a name="l00533"></a>00533 
<a name="l00534"></a>00534 
<a name="l00535"></a>00535 <span class="comment">/* </span>
<a name="l00536"></a>00536 <span class="comment"> * Routines with prefixes C and Z only</span>
<a name="l00537"></a>00537 <span class="comment"> */</span>
<a name="l00538"></a>00538 <span class="keywordtype">void</span> cblas_chemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00539"></a>00539                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00540"></a>00540                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00541"></a>00541                  <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00542"></a>00542                  <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00543"></a>00543 <span class="keywordtype">void</span> cblas_cherk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00544"></a>00544                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00545"></a>00545                  <span class="keyword">const</span> <span class="keywordtype">float</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00546"></a>00546                  <span class="keyword">const</span> <span class="keywordtype">float</span> beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00547"></a>00547 <span class="keywordtype">void</span> cblas_cher2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00548"></a>00548                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00549"></a>00549                   <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00550"></a>00550                   <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">float</span> beta,
<a name="l00551"></a>00551                   <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00552"></a>00552 
<a name="l00553"></a>00553 <span class="keywordtype">void</span> cblas_zhemm(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_SIDE Side,
<a name="l00554"></a>00554                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo, <span class="keyword">const</span> <span class="keywordtype">int</span> M, <span class="keyword">const</span> <span class="keywordtype">int</span> N,
<a name="l00555"></a>00555                  <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00556"></a>00556                  <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">void</span> *beta,
<a name="l00557"></a>00557                  <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00558"></a>00558 <span class="keywordtype">void</span> cblas_zherk(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00559"></a>00559                  <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00560"></a>00560                  <span class="keyword">const</span> <span class="keywordtype">double</span> alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00561"></a>00561                  <span class="keyword">const</span> <span class="keywordtype">double</span> beta, <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00562"></a>00562 <span class="keywordtype">void</span> cblas_zher2k(<span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_ORDER Order, <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_UPLO Uplo,
<a name="l00563"></a>00563                   <span class="keyword">const</span> <span class="keyword">enum</span> CBLAS_TRANSPOSE Trans, <span class="keyword">const</span> <span class="keywordtype">int</span> N, <span class="keyword">const</span> <span class="keywordtype">int</span> K,
<a name="l00564"></a>00564                   <span class="keyword">const</span> <span class="keywordtype">void</span> *alpha, <span class="keyword">const</span> <span class="keywordtype">void</span> *A, <span class="keyword">const</span> <span class="keywordtype">int</span> lda,
<a name="l00565"></a>00565                   <span class="keyword">const</span> <span class="keywordtype">void</span> *B, <span class="keyword">const</span> <span class="keywordtype">int</span> ldb, <span class="keyword">const</span> <span class="keywordtype">double</span> beta,
<a name="l00566"></a>00566                   <span class="keywordtype">void</span> *C, <span class="keyword">const</span> <span class="keywordtype">int</span> ldc);
<a name="l00567"></a>00567 
<a name="l00568"></a>00568 <span class="keywordtype">void</span> cblas_xerbla(<span class="keywordtype">int</span> p, <span class="keyword">const</span> <span class="keywordtype">char</span> *rout, <span class="keyword">const</span> <span class="keywordtype">char</span> *form, ...);
<a name="l00569"></a>00569 <span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
