<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Matrix_ArraySparse</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-types">Public Types</a> &#124;
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::Matrix_ArraySparse" -->
<p>Sparse Array-matrix class.  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a>&gt;</code></p>

<p><a href="class_seldon_1_1_matrix___array_sparse-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-types"></a>
Public Types</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7cb3f37fb440f4b5d662bfc8fcb110f3"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::value_type" ref="a7cb3f37fb440f4b5d662bfc8fcb110f3" args="" -->
typedef Allocator::value_type&nbsp;</td><td class="memItemRight" valign="bottom"><b>value_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad942446ee432becc1e9e0b55223d60b1"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::pointer" ref="ad942446ee432becc1e9e0b55223d60b1" args="" -->
typedef Allocator::pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa51e4113603906cb6ccb1770886f9777"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::const_pointer" ref="aa51e4113603906cb6ccb1770886f9777" args="" -->
typedef Allocator::const_pointer&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_pointer</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a78e6a6b658209682bb428916b5cb4eab"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::reference" ref="a78e6a6b658209682bb428916b5cb4eab" args="" -->
typedef Allocator::reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a416fcb271e492f2c28ca01ea54488814"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::const_reference" ref="a416fcb271e492f2c28ca01ea54488814" args="" -->
typedef Allocator::const_reference&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_reference</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a96f809f4258c19a7786058627a25d9ee"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::entry_type" ref="a96f809f4258c19a7786058627a25d9ee" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>entry_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab491c58b73fe652ffdd7faa975be18dc"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::access_type" ref="ab491c58b73fe652ffdd7faa975be18dc" args="" -->
typedef T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><b>access_type</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a42c803f09cef5e281e8061cceb9098b2"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::const_access_type" ref="a42c803f09cef5e281e8061cceb9098b2" args="" -->
typedef T&nbsp;</td><td class="memItemRight" valign="bottom"><b>const_access_type</b></td></tr>
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d">Matrix_ArraySparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Default constructor.  <a href="#a6d460ce1bd8388adce95c8ad99b24f1d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ad3363505ad6df902486f4496ac0e7ea7">Matrix_ArraySparse</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Constructor.  <a href="#ad3363505ad6df902486f4496ac0e7ea7"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="adf298ef8550ee9681a384a539802736f"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::~Matrix_ArraySparse" ref="adf298ef8550ee9681a384a539802736f" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f">~Matrix_ArraySparse</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Destructor. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix.  <a href="#a61227beea7612dd22bf1f46546bb5cde"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568">Reallocate</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates memory to resize the matrix.  <a href="#afcd0d4612b325249c24407627b3dd568"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa">Resize</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reallocates additional memory to resize the matrix.  <a href="#a2c6f9586424529c8b06b6110c5a01efa"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f">GetM</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows.  <a href="#a156ebb8be6f9fbf5d2e7ef01822a341f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753">GetN</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns.  <a href="#a3ae65d56c548233051a15054643f6753"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#aeb55b9de8946ee89fb40c18612f9f700">GetM</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of rows of the matrix possibly transposed.  <a href="#aeb55b9de8946ee89fb40c18612f9f700"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a537eebc1c692724958852a83df609036">GetN</a> (const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;status) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of columns of the matrix possibly transposed.  <a href="#a537eebc1c692724958852a83df609036"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d">GetNonZeros</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of non-zero entries.  <a href="#ac58573fd27b4f0345ae196b1a6c69d0d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de">GetDataSize</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns the number of elements stored in memory.  <a href="#a01787b806fc996c2ff3c1dcf9b2d54de"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b">GetIndex</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns (row or column) indices of non-zero entries in row.  <a href="#ac3d6a23829eb932f41de04c13de22f6b"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0c18c6428cc11d175b58205727a4c67d">GetData</a> (int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns values of non-zero entries of a row/column.  <a href="#a0c18c6428cc11d175b58205727a4c67d"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt; *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9">GetData</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns values of non-zero entries.  <a href="#a550784008495b254998193d94f75bff9"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">operator()</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a41e1ef18aa87fce8062d36be5e634dfd"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5aaa013260fab95db133652459b0bb2f">operator()</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access operator.  <a href="#a5aaa013260fab95db133652459b0bb2f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659">Val</a> (int i, int j)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#ad114c8c6e002a7a0def70f2413a32659"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a130c7307de87672e3878362386481775">Val</a> (int i, int j) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Access method.  <a href="#a130c7307de87672e3878362386481775"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">const T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6">Value</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row/column i.  <a href="#a0073048e11b166e4152be498e9c497f6"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">T &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afce6920acaa6e6ca7ce340c2db8991ea">Value</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns j-th non-zero value of row/column i.  <a href="#afce6920acaa6e6ca7ce340c2db8991ea"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2">Index</a> (int num_row, int i) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column/row number of j-th non-zero value of row/column i.  <a href="#a3534fb2c57f2640c9b8cd5a59e9da0e2"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">int &amp;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a34c0904b139171db42c46d46413f414f">Index</a> (int num_row, int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Returns column/row number of j-th non-zero value of row/column i.  <a href="#a34c0904b139171db42c46d46413f414f"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46">SetData</a> (int, int, <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines the matrix.  <a href="#a9fb98c93d5bee7a4be62e1a3e1bb5a46"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6e429aff71786b06033b047e6c277fab">SetData</a> (int, int, T *, int *)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Redefines a row/column of the matrix.  <a href="#a6e429aff71786b06033b047e6c277fab"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a1931ba4ea2000131dbc4bd4577fd9f00">Nullify</a> (int i)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears a row without releasing memory.  <a href="#a1931ba4ea2000131dbc4bd4577fd9f00"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462">Nullify</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Clears the matrix without releasing memory.  <a href="#a0cceba81597165b0e23ec0c20daed462"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118">Print</a> () const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Displays the matrix on the standard output.  <a href="#a61ff8d09ea73a05eff4fb21e16fb1118"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac469920c36d5a57516a3e53c0c4e3126">Assemble</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Assembles the matrix.  <a href="#ac469920c36d5a57516a3e53c0c4e3126"></a><br/></td></tr>
<tr><td class="memTemplParams" colspan="2">template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5">RemoveSmallEntry</a> (const T0 &amp;epsilon)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Removes small coefficients from entries.  <a href="#a4b87eb77f9690ec605e9a1e7e1d155c5"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af92baadfc393d70b6e3f3ecaa25d16c7"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::SetIdentity" ref="af92baadfc393d70b6e3f3ecaa25d16c7" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7">SetIdentity</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight"><a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> is initialized to the identity matrix. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a06fc483908e9219787dfc9331b07d828"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Zero" ref="a06fc483908e9219787dfc9331b07d828" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a06fc483908e9219787dfc9331b07d828">Zero</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to 0 (but not removed). <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a806a12c725733434b08b4276de7fbcee"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Fill" ref="a806a12c725733434b08b4276de7fbcee" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee">Fill</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are filled with values 0, 1, 2, 3 ... <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="ae89973954b1f1372112ebb14045cf5d5"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Fill" ref="ae89973954b1f1372112ebb14045cf5d5" args="(const T0 &amp;x)" -->
template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ae89973954b1f1372112ebb14045cf5d5">Fill</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to a given value x. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a28d0b06c82e4bb29f36c872f71fe7b48"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::operator=" ref="a28d0b06c82e4bb29f36c872f71fe7b48" args="(const T0 &amp;x)" -->
template&lt;class T0 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Matrix_ArraySparse</a>&lt; T, Prop, <br class="typebreak"/>
Storage, Allocator &gt; &amp;&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48">operator=</a> (const T0 &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries are set to a given value x. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ae7161fac73344a98f937abbf957fdbb3"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::FillRand" ref="ae7161fac73344a98f937abbf957fdbb3" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ae7161fac73344a98f937abbf957fdbb3">FillRand</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Non-zero entries take a random value. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363">Write</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#a014e4dca632c4955660ad849db2cb363"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afaaf09b0ad951e8b82603e2e6f4d79c4">Write</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#afaaf09b0ad951e8b82603e2e6f4d79c4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412">WriteText</a> (string FileName) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix in a file.  <a href="#adf0d3ec9d5ffeb1b3d46f523051d1412"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6037e1907164259fa78d2abaa1380577">WriteText</a> (ostream &amp;FileStream) const </td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Writes the matrix to an output stream.  <a href="#a6037e1907164259fa78d2abaa1380577"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86">Read</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a2e644c704eba4e3c531c403952189d86"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#abeb6e31b268538d3c43fc8f795e994d4">Read</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#abeb6e31b268538d3c43fc8f795e994d4"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c">ReadText</a> (string FileName)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from a file.  <a href="#a22e85b66114b00ba89b1615bc1839b0c"></a><br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top">void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a4bac922eaed55ed72c4fcfac89b405ca">ReadText</a> (istream &amp;FileStream)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Reads the matrix from an input stream.  <a href="#a4bac922eaed55ed72c4fcfac89b405ca"></a><br/></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="acc9d4e1d9bdb13e8449c7b2cc92dc3db"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::m_" ref="acc9d4e1d9bdb13e8449c7b2cc92dc3db" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">m_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of rows. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a03fe364e494d6f0dd31186462681b3a4"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::n_" ref="a03fe364e494d6f0dd31186462681b3a4" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4">n_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Number of columns. <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a5d9a9091c665cea7f54ac8ba70d11fb6"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::val_" ref="a5d9a9091c665cea7f54ac8ba70d11fb6" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, <br class="typebreak"/>
Allocator &gt;, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_new_alloc.php">NewAlloc</a>&lt; <a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <br class="typebreak"/>
<a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; &gt; &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">val_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">rows or columns <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;class T, class Prop, class Storage, class Allocator = SELDON_DEFAULT_ALLOCATOR&lt;T&gt;&gt;<br/>
 class Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</h3>

<p>Sparse Array-matrix class. </p>
<p>Sparse matrices are defined by: (1) the number of rows and columns; (2) the number of non-zero entries; (3) an array of vectors ind ind(i) is a vector, which contains indices of columns of the row i (4) an array of vectors val : val(i) is a vector, which contains values of the row i </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8hxx_source.php#l00038">38</a> of file <a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a>.</p>
<hr/><h2>Constructor &amp; Destructor Documentation</h2>
<a class="anchor" id="a6d460ce1bd8388adce95c8ad99b24f1d"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Matrix_ArraySparse" ref="a6d460ce1bd8388adce95c8ad99b24f1d" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Matrix_ArraySparse</a> </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Default constructor. </p>
<p>Builds an empty matrix. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00030">30</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad3363505ad6df902486f4496ac0e7ea7"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Matrix_ArraySparse" ref="ad3363505ad6df902486f4496ac0e7ea7" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::<a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Matrix_ArraySparse</a> </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Constructor. </p>
<p>Builds a i by j sparse matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00046">46</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<hr/><h2>Member Function Documentation</h2>
<a class="anchor" id="ac469920c36d5a57516a3e53c0c4e3126"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Assemble" ref="ac469920c36d5a57516a3e53c0c4e3126" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Assemble </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Assembles the matrix. </p>
<p>All the column/row numbers are sorted. If same column/row numbers exist, values are added. </p>
<dl class="warning"><dt><b>Warning:</b></dt><dd>If you are using the methods AddInteraction, you don't need to call that method. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00583">583</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a61227beea7612dd22bf1f46546bb5cde"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Clear" ref="a61227beea7612dd22bf1f46546bb5cde" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Clear </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix. </p>
<p>This methods is equivalent to the destructor. On exit, the matrix is empty (0 by 0). </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00068">68</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0c18c6428cc11d175b58205727a4c67d"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetData" ref="a0c18c6428cc11d175b58205727a4c67d" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T * <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns values of non-zero entries of a row/column. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row (or column) number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of values of non-zero entries of row/column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00243">243</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a550784008495b254998193d94f75bff9"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetData" ref="a550784008495b254998193d94f75bff9" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocat &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocat &gt; * <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocat &gt;::GetData </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns values of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>Array of sparse rows There is a different array for each row/column. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00256">256</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a01787b806fc996c2ff3c1dcf9b2d54de"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetDataSize" ref="a01787b806fc996c2ff3c1dcf9b2d54de" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetDataSize </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of elements stored in memory. </p>
<p>Returns the number of elements stored in memory, i.e. the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of elements stored in memory. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00215">215</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac3d6a23829eb932f41de04c13de22f6b"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetIndex" ref="ac3d6a23829eb932f41de04c13de22f6b" args="(int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int * <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetIndex </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns (row or column) indices of non-zero entries in row. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row (or column) number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The array of column (or row) indices of non-zero entries of row (or column) i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00229">229</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a156ebb8be6f9fbf5d2e7ef01822a341f"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetM" ref="a156ebb8be6f9fbf5d2e7ef01822a341f" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the number of rows. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00143">143</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="aeb55b9de8946ee89fb40c18612f9f700"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetM" ref="aeb55b9de8946ee89fb40c18612f9f700" args="(const SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetM </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of rows of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of rows of the possibly-transposed matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00167">167</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3ae65d56c548233051a15054643f6753"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetN" ref="a3ae65d56c548233051a15054643f6753" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>the number of columns. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00154">154</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a537eebc1c692724958852a83df609036"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetN" ref="a537eebc1c692724958852a83df609036" args="(const SeldonTranspose &amp;status) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetN </td>
          <td>(</td>
          <td class="paramtype">const <a class="el" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> &amp;&nbsp;</td>
          <td class="paramname"> <em>status</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of columns of the matrix possibly transposed. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>status</em>&nbsp;</td><td>assumed status about the transposition of the matrix. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of columns of the possibly-transposed matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00183">183</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ac58573fd27b4f0345ae196b1a6c69d0d"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::GetNonZeros" ref="ac58573fd27b4f0345ae196b1a6c69d0d" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::GetNonZeros </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns the number of non-zero entries. </p>
<dl class="return"><dt><b>Returns:</b></dt><dd>The number of non-zero entries. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00197">197</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a3534fb2c57f2640c9b8cd5a59e9da0e2"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Index" ref="a3534fb2c57f2640c9b8cd5a59e9da0e2" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Index </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column/row number of j-th non-zero value of row/column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Column/row number of j-th non-zero value of row/column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00441">441</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a34c0904b139171db42c46d46413f414f"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Index" ref="a34c0904b139171db42c46d46413f414f" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">int &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Index </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns column/row number of j-th non-zero value of row/column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Column/row number of j-th non-zero value of row/column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00468">468</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a1931ba4ea2000131dbc4bd4577fd9f00"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Nullify" ref="a1931ba4ea2000131dbc4bd4577fd9f00" args="(int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears a row without releasing memory. </p>
<p>On exit, the row is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00508">508</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0cceba81597165b0e23ec0c20daed462"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Nullify" ref="a0cceba81597165b0e23ec0c20daed462" args="()" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Nullify </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Clears the matrix without releasing memory. </p>
<p>On exit, the matrix is empty and the memory has not been released. It is useful for low level manipulations on a <a class="el" href="class_seldon_1_1_matrix.php">Matrix</a> instance. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00536">536</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a41e1ef18aa87fce8062d36be5e634dfd"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::operator()" ref="a41e1ef18aa87fce8062d36be5e634dfd" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a1437cb081d496b576b36ae061cd9c51c">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8d872a0e035a2afcc8e40736e8556c44">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00276">276</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a5aaa013260fab95db133652459b0bb2f"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::operator()" ref="a5aaa013260fab95db133652459b0bb2f" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::operator() </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access operator. </p>
<p>Returns the value of element (i, j). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (i, j) of the matrix. </dd></dl>

<p>Reimplemented in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aaba3e074be42ad7e68fa22407fe485fb">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>, and <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a45ec07c98e6e55fdae57dda5b0c1cda7">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>.</p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00305">305</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a61ff8d09ea73a05eff4fb21e16fb1118"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Print" ref="a61ff8d09ea73a05eff4fb21e16fb1118" args="() const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Print </td>
          <td>(</td>
          <td class="paramname"></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Displays the matrix on the standard output. </p>
<p>Displays elements on the standard output, in text format. Each row is displayed on a single line and elements of a row are delimited by tabulations. </p>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00556">556</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2e644c704eba4e3c531c403952189d86"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Read" ref="a2e644c704eba4e3c531c403952189d86" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads a matrix stored in binary format in a file. The number of rows (integer) and the number of columns (integer) are read and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00790">790</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="abeb6e31b268538d3c43fc8f795e994d4"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Read" ref="abeb6e31b268538d3c43fc8f795e994d4" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Read </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix in binary format from an input stream. The number of rows (integer) and the number of columns (integer) are read and matrix elements are then read in the same order as it should be in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00818">818</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4bac922eaed55ed72c4fcfac89b405ca"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::ReadText" ref="a4bac922eaed55ed72c4fcfac89b405ca" args="(istream &amp;FileStream)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">istream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from an input stream. </p>
<p>Reads a matrix from a stream in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>input stream. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00881">881</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a22e85b66114b00ba89b1615bc1839b0c"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::ReadText" ref="a22e85b66114b00ba89b1615bc1839b0c" args="(string FileName)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::ReadText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reads the matrix from a file. </p>
<p>Reads the matrix from a file in text format. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00856">856</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afcd0d4612b325249c24407627b3dd568"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Reallocate" ref="afcd0d4612b325249c24407627b3dd568" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Reallocate </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates memory to resize the matrix. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. </td></tr>
  </table>
  </dd>
</dl>
<dl class="warning"><dt><b>Warning:</b></dt><dd>Data is lost. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00088">88</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a4b87eb77f9690ec605e9a1e7e1d155c5"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::RemoveSmallEntry" ref="a4b87eb77f9690ec605e9a1e7e1d155c5" args="(const T0 &amp;epsilon)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
<div class="memtemplate">
template&lt;class T0 &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::RemoveSmallEntry </td>
          <td>(</td>
          <td class="paramtype">const T0 &amp;&nbsp;</td>
          <td class="paramname"> <em>epsilon</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Removes small coefficients from entries. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>epsilon</em>&nbsp;</td><td>entries whose values are below epsilon are removed. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00597">597</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a2c6f9586424529c8b06b6110c5a01efa"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Resize" ref="a2c6f9586424529c8b06b6110c5a01efa" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Resize </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Reallocates additional memory to resize the matrix. </p>
<p>On exit, the matrix is a i x j matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>i</em>&nbsp;</td><td>number of rows. </td></tr>
    <tr><td valign="top"></td><td valign="top"><em>j</em>&nbsp;</td><td>number of columns. Data is kept </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00109">109</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6e429aff71786b06033b047e6c277fab"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::SetData" ref="a6e429aff71786b06033b047e6c277fab" args="(int, int, T *, int *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">T *&nbsp;</td>
          <td class="paramname"> <em>val</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int *&nbsp;</td>
          <td class="paramname"> <em>ind</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines a row/column of the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/col number </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>number of non-zero entries in the row </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>values </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>ind</em>&nbsp;</td><td>column numbers </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00496">496</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a9fb98c93d5bee7a4be62e1a3e1bb5a46"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::SetData" ref="a9fb98c93d5bee7a4be62e1a3e1bb5a46" args="(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T, class Prop , class Storage , class Allocator&gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::SetData </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>m</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>n</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype"><a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; T, <a class="el" href="class_seldon_1_1_vect_sparse.php">VectSparse</a>, Allocator &gt; *&nbsp;</td>
          <td class="paramname"> <em>val</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Redefines the matrix. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>m</em>&nbsp;</td><td>new number of rows. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>n</em>&nbsp;</td><td>new number of columns. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>val</em>&nbsp;</td><td>array of sparse rows/columns. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00522">522</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="ad114c8c6e002a7a0def70f2413a32659"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Val" ref="ad114c8c6e002a7a0def70f2413a32659" args="(int i, int j)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element (<em>i</em>, <em>j</em>). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00332">332</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a130c7307de87672e3878362386481775"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Val" ref="a130c7307de87672e3878362386481775" args="(int i, int j) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Val </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Access method. </p>
<p>Returns the value of element (<em>i</em>, <em>j</em>). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row index. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>column index. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>Element (<em>i</em>, <em>j</em>) of the matrix. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00360">360</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a0073048e11b166e4152be498e9c497f6"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Value" ref="a0073048e11b166e4152be498e9c497f6" args="(int num_row, int i) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">const T &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Value </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td> const<code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row/column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row/column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00388">388</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afce6920acaa6e6ca7ce340c2db8991ea"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Value" ref="afce6920acaa6e6ca7ce340c2db8991ea" args="(int num_row, int i)" -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">T &amp; <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Value </td>
          <td>(</td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>i</em>, </td>
        </tr>
        <tr>
          <td class="paramkey"></td>
          <td></td>
          <td class="paramtype">int&nbsp;</td>
          <td class="paramname"> <em>j</em></td><td>&nbsp;</td>
        </tr>
        <tr>
          <td></td>
          <td>)</td>
          <td></td><td></td><td><code> [inline]</code></td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Returns j-th non-zero value of row/column i. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>i</em>&nbsp;</td><td>row/column number. </td></tr>
    <tr><td valign="top"><tt>[in]</tt>&nbsp;</td><td valign="top"><em>j</em>&nbsp;</td><td>local number. </td></tr>
  </table>
  </dd>
</dl>
<dl class="return"><dt><b>Returns:</b></dt><dd>j-th non-zero entry of row/column i. </dd></dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00415">415</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="afaaf09b0ad951e8b82603e2e6f4d79c4"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Write" ref="afaaf09b0ad951e8b82603e2e6f4d79c4" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00704">704</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a014e4dca632c4955660ad849db2cb363"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::Write" ref="a014e4dca632c4955660ad849db2cb363" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::Write </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in binary format. The number of rows (integer) and the number of columns (integer) are written and matrix elements are then written in the same order as in memory (e.g. row-major storage). </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00676">676</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="a6037e1907164259fa78d2abaa1380577"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::WriteText" ref="a6037e1907164259fa78d2abaa1380577" args="(ostream &amp;FileStream) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">ostream &amp;&nbsp;</td>
          <td class="paramname"> <em>FileStream</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix to an output stream. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value). 1-index convention is used. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileStream</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00756">756</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<a class="anchor" id="adf0d3ec9d5ffeb1b3d46f523051d1412"></a><!-- doxytag: member="Seldon::Matrix_ArraySparse::WriteText" ref="adf0d3ec9d5ffeb1b3d46f523051d1412" args="(string FileName) const " -->
<div class="memitem">
<div class="memproto">
<div class="memtemplate">
template&lt;class T , class Prop , class Storage , class Allocator &gt; </div>
      <table class="memname">
        <tr>
          <td class="memname">void <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse</a>&lt; T, Prop, Storage, Allocator &gt;::WriteText </td>
          <td>(</td>
          <td class="paramtype">string&nbsp;</td>
          <td class="paramname"> <em>FileName</em></td>
          <td>&nbsp;)&nbsp;</td>
          <td> const</td>
        </tr>
      </table>
</div>
<div class="memdoc">

<p>Writes the matrix in a file. </p>
<p>Stores the matrix in a file in ascii format. The entries are written in coordinate format (row column value). 1-index convention is used. </p>
<dl><dt><b>Parameters:</b></dt><dd>
  <table border="0" cellspacing="2" cellpadding="0">
    <tr><td valign="top"></td><td valign="top"><em>FileName</em>&nbsp;</td><td>output file name. </td></tr>
  </table>
  </dd>
</dl>

<p>Definition at line <a class="el" href="_matrix___array_sparse_8cxx_source.php#l00731">731</a> of file <a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a>.</p>

</div>
</div>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>matrix_sparse/<a class="el" href="_matrix___array_sparse_8hxx_source.php">Matrix_ArraySparse.hxx</a></li>
<li>matrix_sparse/<a class="el" href="_matrix___array_sparse_8cxx_source.php">Matrix_ArraySparse.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
