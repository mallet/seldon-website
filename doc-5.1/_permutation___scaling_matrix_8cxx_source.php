<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Permutation_ScalingMatrix.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_PERMUTATION_SCALING_MATRIX_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="comment">/*</span>
<a name="l00024"></a>00024 <span class="comment">  Functions defined in this file:</span>
<a name="l00025"></a>00025 <span class="comment"></span>
<a name="l00026"></a>00026 <span class="comment">  ApplyInversePermutation(A, I, J)</span>
<a name="l00027"></a>00027 <span class="comment"></span>
<a name="l00028"></a>00028 <span class="comment">  ScaleMatrix(A, Drow, Dcol)</span>
<a name="l00029"></a>00029 <span class="comment"></span>
<a name="l00030"></a>00030 <span class="comment">  ScaleLeftMatrix(A, Drow)</span>
<a name="l00031"></a>00031 <span class="comment">*/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 <span class="keyword">namespace </span>Seldon
<a name="l00034"></a>00034 {
<a name="l00035"></a>00035 
<a name="l00036"></a>00036 
<a name="l00038"></a>00038 
<a name="l00042"></a>00042   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00043"></a><a class="code" href="namespace_seldon.php#a45020c96946631c41dd651659adfab20">00043</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php" title="Row-major sparse-matrix class.">Matrix&lt;T, Prop, RowSparse, Allocator&gt;</a>&amp; A,
<a name="l00044"></a>00044                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; row_perm,
<a name="l00045"></a>00045                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; col_perm)
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047     <span class="keywordtype">int</span> i, j, k, l, nnz;
<a name="l00048"></a>00048     <span class="keywordtype">int</span> m = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>();
<a name="l00049"></a>00049     <span class="keywordtype">int</span> n = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>();
<a name="l00050"></a>00050     <span class="keywordtype">int</span>* ind = A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa" title="Returns (row or column) indices of non-zero entries.">GetInd</a>();
<a name="l00051"></a>00051     <span class="keywordtype">int</span>* ptr = A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342" title="Returns (row or column) start indices.">GetPtr</a>();
<a name="l00052"></a>00052     T* data = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>();
<a name="l00053"></a>00053 
<a name="l00054"></a>00054     <span class="comment">/*** Permutation of the columns ***/</span>
<a name="l00055"></a>00055 
<a name="l00056"></a>00056     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> row_data;
<a name="l00057"></a>00057     <span class="comment">// Column indexes of a given row.</span>
<a name="l00058"></a>00058     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_index;
<a name="l00059"></a>00059     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00060"></a>00060       <span class="keywordflow">if</span> ((nnz = ptr[i + 1] - ptr[i]) != 0)
<a name="l00061"></a>00061         {
<a name="l00062"></a>00062           row_index.Reallocate(nnz);
<a name="l00063"></a>00063           row_data.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79" title="Vector reallocation.">Reallocate</a>(nnz);
<a name="l00064"></a>00064           <span class="keywordflow">for</span> (k = 0, l = ptr[i]; k &lt; nnz; k++, l++)
<a name="l00065"></a>00065             {
<a name="l00066"></a>00066               <span class="comment">// Applies the permutation on the columns.</span>
<a name="l00067"></a>00067               row_index(k) = col_perm(ind[l]);
<a name="l00068"></a>00068               row_data(k) = data[l];
<a name="l00069"></a>00069             }
<a name="l00070"></a>00070 
<a name="l00071"></a>00071           <span class="comment">// The columns must remain in increasing order.</span>
<a name="l00072"></a>00072           <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(row_index, row_data);
<a name="l00073"></a>00073 
<a name="l00074"></a>00074           <span class="comment">// Putting back the data into the array.</span>
<a name="l00075"></a>00075           <span class="keywordflow">for</span> (k = 0, l = ptr[i]; k &lt; nnz; k++, l++)
<a name="l00076"></a>00076             {
<a name="l00077"></a>00077               ind[l] = row_index(k);
<a name="l00078"></a>00078               data[l] = row_data(k);
<a name="l00079"></a>00079             }
<a name="l00080"></a>00080         }
<a name="l00081"></a>00081     row_index.Clear();
<a name="l00082"></a>00082     row_data.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1" title="Clears the vector.">Clear</a>();
<a name="l00083"></a>00083 
<a name="l00084"></a>00084     <span class="comment">/*** Perturbation of the rows ***/</span>
<a name="l00085"></a>00085 
<a name="l00086"></a>00086     <span class="comment">// Total number of non-zero elements.</span>
<a name="l00087"></a>00087     nnz = ptr[m];
<a name="l00088"></a>00088 
<a name="l00089"></a>00089     <span class="comment">// &#39;row_perm&#39; is const, so it must be copied.</span>
<a name="l00090"></a>00090     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> row_permutation(row_perm);
<a name="l00091"></a>00091     <span class="comment">// Row indexes in the origin matrix: prev_row_index(i) should be the</span>
<a name="l00092"></a>00092     <span class="comment">// location of the i-th row (from the permuted matrix) in the matrix</span>
<a name="l00093"></a>00093     <span class="comment">// before permutation.</span>
<a name="l00094"></a>00094     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> prev_row_index(m);
<a name="l00095"></a>00095     prev_row_index.Fill();
<a name="l00096"></a>00096 
<a name="l00097"></a>00097     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(row_permutation, prev_row_index);
<a name="l00098"></a>00098     row_permutation.Clear();
<a name="l00099"></a>00099 
<a name="l00100"></a>00100     <span class="comment">// Description of the matrix after permutations.</span>
<a name="l00101"></a>00101     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; new_ptr(m + 1);
<a name="l00102"></a>00102     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; new_ind(nnz);
<a name="l00103"></a>00103     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> new_data(nnz);
<a name="l00104"></a>00104 
<a name="l00105"></a>00105     <span class="keywordtype">int</span> ptr_count = 0, length, source_position;
<a name="l00106"></a>00106     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00107"></a>00107       {
<a name="l00108"></a>00108         length = ptr[prev_row_index(i) + 1] - ptr[prev_row_index(i)];
<a name="l00109"></a>00109         source_position = ptr[prev_row_index(i)];
<a name="l00110"></a>00110         <span class="keywordflow">for</span> (j = 0; j &lt; length; j++)
<a name="l00111"></a>00111           {
<a name="l00112"></a>00112             new_data(ptr_count + j) = data[ptr[prev_row_index(i)] + j];
<a name="l00113"></a>00113             new_ind(ptr_count + j) = ind[ptr[prev_row_index(i)] + j];
<a name="l00114"></a>00114           }
<a name="l00115"></a>00115         new_ptr(i) = ptr_count;
<a name="l00116"></a>00116         ptr_count += length;
<a name="l00117"></a>00117       }
<a name="l00118"></a>00118     new_ptr(m) = ptr_count;
<a name="l00119"></a>00119 
<a name="l00120"></a>00120     A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">SetData</a>(m, n, new_data, new_ptr, new_ind);
<a name="l00121"></a>00121   }
<a name="l00122"></a>00122 
<a name="l00123"></a>00123 
<a name="l00125"></a>00125 
<a name="l00129"></a>00129   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00130"></a><a class="code" href="namespace_seldon.php#a00d2c1d25faa339c755af4faa023d310">00130</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php" title="Column-major sparse-matrix class.">Matrix&lt;T, Prop, ColSparse, Allocator&gt;</a>&amp; A,
<a name="l00131"></a>00131                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; row_perm,
<a name="l00132"></a>00132                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; col_perm)
<a name="l00133"></a>00133   {
<a name="l00134"></a>00134     <span class="keywordtype">int</span> i, j, k, l, nnz;
<a name="l00135"></a>00135     <span class="keywordtype">int</span> m = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>();
<a name="l00136"></a>00136     <span class="keywordtype">int</span> n = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>();
<a name="l00137"></a>00137     <span class="keywordtype">int</span>* ind = A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa" title="Returns (row or column) indices of non-zero entries.">GetInd</a>();
<a name="l00138"></a>00138     <span class="keywordtype">int</span>* ptr = A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342" title="Returns (row or column) start indices.">GetPtr</a>();
<a name="l00139"></a>00139     T* data = A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>();
<a name="l00140"></a>00140 
<a name="l00141"></a>00141     <span class="comment">/*** Permutation of the rows ***/</span>
<a name="l00142"></a>00142 
<a name="l00143"></a>00143     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> col_data;
<a name="l00144"></a>00144     <span class="comment">// Row indexes of a given column.</span>
<a name="l00145"></a>00145     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> col_index;
<a name="l00146"></a>00146     <span class="keywordflow">for</span> (i = 0; i &lt; n; i++)
<a name="l00147"></a>00147       <span class="keywordflow">if</span> ((nnz = ptr[i + 1] - ptr[i]) != 0)
<a name="l00148"></a>00148         {
<a name="l00149"></a>00149           col_index.Reallocate(nnz);
<a name="l00150"></a>00150           col_data.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79" title="Vector reallocation.">Reallocate</a>(nnz);
<a name="l00151"></a>00151           <span class="keywordflow">for</span> (k = 0, l = ptr[i]; k &lt; nnz; k++, l++)
<a name="l00152"></a>00152             {
<a name="l00153"></a>00153               <span class="comment">// Applies the permutation on the rows.</span>
<a name="l00154"></a>00154               col_index(k) = row_perm(ind[l]);
<a name="l00155"></a>00155               col_data(k) = data[l];
<a name="l00156"></a>00156             }
<a name="l00157"></a>00157 
<a name="l00158"></a>00158           <span class="comment">// The rows must remain in increasing order.</span>
<a name="l00159"></a>00159           <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(col_index, col_data);
<a name="l00160"></a>00160 
<a name="l00161"></a>00161           <span class="comment">// Putting back the data into the array.</span>
<a name="l00162"></a>00162           <span class="keywordflow">for</span> (k = 0, l = ptr[i]; k &lt; nnz; k++, l++)
<a name="l00163"></a>00163             {
<a name="l00164"></a>00164               ind[l] = col_index(k);
<a name="l00165"></a>00165               data[l] = col_data(k);
<a name="l00166"></a>00166             }
<a name="l00167"></a>00167         }
<a name="l00168"></a>00168     col_index.Clear();
<a name="l00169"></a>00169     col_data.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a5120b4ecff05d8cf54bbd57fb280fbe1" title="Clears the vector.">Clear</a>();
<a name="l00170"></a>00170 
<a name="l00171"></a>00171     <span class="comment">/*** Perturbation of the columns ***/</span>
<a name="l00172"></a>00172 
<a name="l00173"></a>00173     <span class="comment">// Total number of non-zero elements.</span>
<a name="l00174"></a>00174     nnz = ptr[n];
<a name="l00175"></a>00175 
<a name="l00176"></a>00176     <span class="comment">// &#39;col_perm&#39; is const, so it must be copied.</span>
<a name="l00177"></a>00177     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> col_permutation(col_perm);
<a name="l00178"></a>00178     <span class="comment">// Column indexes in the origin matrix: prev_col_index(i) should be the</span>
<a name="l00179"></a>00179     <span class="comment">// location of the i-th column (from the permuted matrix) in the matrix</span>
<a name="l00180"></a>00180     <span class="comment">// before permutation.</span>
<a name="l00181"></a>00181     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> prev_col_index(n);
<a name="l00182"></a>00182     prev_col_index.Fill();
<a name="l00183"></a>00183 
<a name="l00184"></a>00184     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(col_permutation, prev_col_index);
<a name="l00185"></a>00185     col_permutation.Clear();
<a name="l00186"></a>00186 
<a name="l00187"></a>00187     <span class="comment">// Description of the matrix after permutations.</span>
<a name="l00188"></a>00188     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; new_ptr(n + 1);
<a name="l00189"></a>00189     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; new_ind(nnz);
<a name="l00190"></a>00190     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> new_data(nnz);
<a name="l00191"></a>00191 
<a name="l00192"></a>00192     <span class="keywordtype">int</span> ptr_count = 0, length, source_position;
<a name="l00193"></a>00193     <span class="keywordflow">for</span> (i = 0; i &lt; n; i++)
<a name="l00194"></a>00194       {
<a name="l00195"></a>00195         length = ptr[prev_col_index(i) + 1] - ptr[prev_col_index(i)];
<a name="l00196"></a>00196         source_position = ptr[prev_col_index(i)];
<a name="l00197"></a>00197         <span class="keywordflow">for</span> (j = 0; j &lt; length; j++)
<a name="l00198"></a>00198           {
<a name="l00199"></a>00199             new_data(ptr_count + j) = data[ptr[prev_col_index(i)] + j];
<a name="l00200"></a>00200             new_ind(ptr_count + j) = ind[ptr[prev_col_index(i)] + j];
<a name="l00201"></a>00201           }
<a name="l00202"></a>00202         new_ptr(i) = ptr_count;
<a name="l00203"></a>00203         ptr_count += length;
<a name="l00204"></a>00204       }
<a name="l00205"></a>00205     new_ptr(n) = ptr_count;
<a name="l00206"></a>00206 
<a name="l00207"></a>00207     A.<a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">SetData</a>(m, n, new_data, new_ptr, new_ind);
<a name="l00208"></a>00208   }
<a name="l00209"></a>00209 
<a name="l00210"></a>00210 
<a name="l00212"></a>00212 
<a name="l00216"></a>00216   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00217"></a><a class="code" href="namespace_seldon.php#a192ec8b6c0468487921b98f2cbe855c2">00217</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php" title="Row-major sparse-matrix class.">Matrix&lt;T, Prop, ArrayRowSparse, Allocator&gt;</a>&amp; A,
<a name="l00218"></a>00218                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row_perm, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col_perm)
<a name="l00219"></a>00219   {
<a name="l00220"></a>00220     <span class="keywordtype">int</span> m = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>(), n, i, i_, j, i2;
<a name="l00221"></a>00221     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> ind_tmp, iperm(m), rperm(m);
<a name="l00222"></a>00222     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00223"></a>00223       {
<a name="l00224"></a>00224         iperm(i) = i;
<a name="l00225"></a>00225         rperm(i) = i;
<a name="l00226"></a>00226       }
<a name="l00227"></a>00227     <span class="comment">// A(rperm(i),:) will be the place where is the initial row i.</span>
<a name="l00228"></a>00228 
<a name="l00229"></a>00229     <span class="comment">// Algorithm avoiding the allocation of another matrix.</span>
<a name="l00230"></a>00230     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00231"></a>00231       {
<a name="l00232"></a>00232         <span class="comment">// We get the index of row where the row initially placed on row i is.</span>
<a name="l00233"></a>00233         i2 = rperm(i);
<a name="l00234"></a>00234         <span class="comment">// We get the new index of this row.</span>
<a name="l00235"></a>00235         i_ = row_perm(i);
<a name="l00236"></a>00236 
<a name="l00237"></a>00237         <span class="comment">// We fill ind_tmp of the permuted indices of columns of row i.</span>
<a name="l00238"></a>00238         n = A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1db215224f5b1ac332a82b55da858445" title="Returns the number of non-zero entries of a row.">GetRowSize</a>(i2);
<a name="l00239"></a>00239         ind_tmp.Reallocate(n);
<a name="l00240"></a>00240         <span class="keywordflow">for</span> (j = 0; j &lt; n; j++)
<a name="l00241"></a>00241           ind_tmp(j) = col_perm(A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i2,j));
<a name="l00242"></a>00242 
<a name="l00243"></a>00243         <span class="comment">// We swap the two rows i and its destination row_perm(i).</span>
<a name="l00244"></a>00244         A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6a476c1d342322cb6b0d16d19de837dc" title="Swaps two rows.">SwapRow</a>(i2, i_);
<a name="l00245"></a>00245         A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a29c3376f6702a15782a46c240c7ce8bd" title="Sets column numbers of non-zero entries of a row.">ReplaceIndexRow</a>(i_, ind_tmp);
<a name="l00246"></a>00246 
<a name="l00247"></a>00247         <span class="comment">// We update the indices iperm and rperm in order to keep in memory</span>
<a name="l00248"></a>00248         <span class="comment">// the place where the row row_perm(i) is.</span>
<a name="l00249"></a>00249         <span class="keywordtype">int</span> i_tmp = iperm(i_);
<a name="l00250"></a>00250         iperm(i_) = iperm(i2);
<a name="l00251"></a>00251         iperm(i2) = i_tmp;
<a name="l00252"></a>00252         rperm(iperm(i_)) = i_;
<a name="l00253"></a>00253         rperm(iperm(i2)) = i2;
<a name="l00254"></a>00254 
<a name="l00255"></a>00255         <span class="comment">// We assemble the row i.</span>
<a name="l00256"></a>00256         A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a73b4fcf4fa577bbe4a88cf0a2f9a0bbc" title="Assembles a row.">AssembleRow</a>(i_);
<a name="l00257"></a>00257       }
<a name="l00258"></a>00258   }
<a name="l00259"></a>00259 
<a name="l00260"></a>00260 
<a name="l00262"></a>00262 
<a name="l00266"></a>00266   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00267"></a>00267   <span class="keywordtype">void</span>
<a name="l00268"></a><a class="code" href="namespace_seldon.php#a30e64c8a9e0bf36091099f727530a963">00268</a>   <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php" title="Column-major symmetric sparse-matrix class.">Matrix&lt;T, Prop, ArrayColSymSparse, Allocator&gt;</a>&amp; A,
<a name="l00269"></a>00269                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row_perm, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col_perm)
<a name="l00270"></a>00270   {
<a name="l00271"></a>00271     <span class="comment">// It is assumed that the permuted matrix is still symmetric! For example,</span>
<a name="l00272"></a>00272     <span class="comment">// the user can provide row_perm = col_perm.</span>
<a name="l00273"></a>00273     <span class="keywordtype">int</span> m = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>();
<a name="l00274"></a>00274     <span class="keywordtype">int</span> nnz = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de" title="Returns the number of elements stored in memory.">GetDataSize</a>();
<a name="l00275"></a>00275     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow(nnz), IndCol(nnz);
<a name="l00276"></a>00276     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> Val(nnz);
<a name="l00277"></a>00277 
<a name="l00278"></a>00278     <span class="comment">// First we convert the matrix in coordinate format and we permute the</span>
<a name="l00279"></a>00279     <span class="comment">// indices.</span>
<a name="l00280"></a>00280     <span class="comment">// IndRow -&gt; indices of the permuted rows</span>
<a name="l00281"></a>00281     <span class="comment">// IndCol -&gt; indices of the permuted columns</span>
<a name="l00282"></a>00282     <span class="keywordtype">int</span> k = 0;
<a name="l00283"></a>00283     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00284"></a>00284       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#accfeb4e792ac6f354c2f9c40827e8c3a" title="Returns the number of non-zero entries of a column.">GetColumnSize</a>(i); j++)
<a name="l00285"></a>00285         {
<a name="l00286"></a>00286           IndCol(k) = col_perm(i);
<a name="l00287"></a>00287           Val(k) = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i,j);
<a name="l00288"></a>00288           IndRow(k) = row_perm(A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i, j));
<a name="l00289"></a>00289           <span class="keywordflow">if</span> (IndCol(k) &lt;= IndRow(k))
<a name="l00290"></a>00290             {
<a name="l00291"></a>00291               <span class="comment">// We store only the superior part of the symmetric matrix.</span>
<a name="l00292"></a>00292               <span class="keywordtype">int</span> ind_tmp = IndRow(k);
<a name="l00293"></a>00293               IndRow(k) = IndCol(k);
<a name="l00294"></a>00294               IndCol(k) = ind_tmp;
<a name="l00295"></a>00295             }
<a name="l00296"></a>00296           k++;
<a name="l00297"></a>00297         }
<a name="l00298"></a>00298 
<a name="l00299"></a>00299     <span class="comment">// We sort with respect to column numbers.</span>
<a name="l00300"></a>00300     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(nnz, IndCol, IndRow, Val);
<a name="l00301"></a>00301 
<a name="l00302"></a>00302     <span class="comment">// A is filled.</span>
<a name="l00303"></a>00303     k = 0;
<a name="l00304"></a>00304     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00305"></a>00305       {
<a name="l00306"></a>00306         <span class="keywordtype">int</span> first_index = k;
<a name="l00307"></a>00307         <span class="comment">// We get the size of the column i.</span>
<a name="l00308"></a>00308         <span class="keywordflow">while</span> (k &lt; nnz &amp;&amp; IndCol(k) &lt;= i)
<a name="l00309"></a>00309           k++;
<a name="l00310"></a>00310 
<a name="l00311"></a>00311         <span class="keywordtype">int</span> size_column = k - first_index;
<a name="l00312"></a>00312         <span class="comment">// If column not empty.</span>
<a name="l00313"></a>00313         <span class="keywordflow">if</span> (size_column &gt; 0)
<a name="l00314"></a>00314           {
<a name="l00315"></a>00315             A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a2f50f94ea485c1c81aecdedd76e09116" title="Reallocates column i.">ReallocateColumn</a>(i, size_column);
<a name="l00316"></a>00316             k = first_index;
<a name="l00317"></a>00317             <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(k, k+size_column-1, IndRow, Val);
<a name="l00318"></a>00318             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_column; j++)
<a name="l00319"></a>00319               {
<a name="l00320"></a>00320                 A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i,j) = IndRow(k);
<a name="l00321"></a>00321                 A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i,j) = Val(k);
<a name="l00322"></a>00322                 k++;
<a name="l00323"></a>00323               }
<a name="l00324"></a>00324           }
<a name="l00325"></a>00325         <span class="keywordflow">else</span>
<a name="l00326"></a>00326           A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#abee36512d3f1fe6dd19693e9585395c4" title="Clears a column.">ClearColumn</a>(i);
<a name="l00327"></a>00327       }
<a name="l00328"></a>00328   }
<a name="l00329"></a>00329 
<a name="l00330"></a>00330 
<a name="l00332"></a>00332 
<a name="l00336"></a>00336   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00337"></a><a class="code" href="namespace_seldon.php#a3d830e5198f28af6321804a95f360bce">00337</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;T, Prop,
<a name="l00338"></a>00338                                <a class="code" href="class_seldon_1_1_array_row_sym_complex_sparse.php">ArrayRowSymComplexSparse</a>, Allocator&gt;&amp; A,
<a name="l00339"></a>00339                                <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row_perm,<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col_perm)
<a name="l00340"></a>00340   {
<a name="l00341"></a>00341     <span class="comment">// It is assumed that the permuted matrix is still symmetric! For example,</span>
<a name="l00342"></a>00342     <span class="comment">// the user can provide row_perm = col_perm.</span>
<a name="l00343"></a>00343     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00344"></a>00344     <span class="keywordtype">int</span> nnz_real = A.GetRealDataSize(), nnz_imag = A.GetImagDataSize();
<a name="l00345"></a>00345     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow(nnz_real), IndCol(nnz_real);
<a name="l00346"></a>00346     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> Val(nnz_real);
<a name="l00347"></a>00347 
<a name="l00348"></a>00348     <span class="comment">// First we convert the matrix in coordinate format and we permute the</span>
<a name="l00349"></a>00349     <span class="comment">// indices.</span>
<a name="l00350"></a>00350     <span class="comment">// IndRow -&gt; indices of the permuted rows</span>
<a name="l00351"></a>00351     <span class="comment">// IndCol -&gt; indices of the permuted columns</span>
<a name="l00352"></a>00352     <span class="keywordtype">int</span> k = 0;
<a name="l00353"></a>00353     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00354"></a>00354       {
<a name="l00355"></a>00355         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++)
<a name="l00356"></a>00356           {
<a name="l00357"></a>00357             IndRow(k) = row_perm(i);
<a name="l00358"></a>00358             Val(k) = A.ValueReal(i,j);
<a name="l00359"></a>00359             IndCol(k) = col_perm(A.IndexReal(i, j));
<a name="l00360"></a>00360             <span class="keywordflow">if</span> (IndCol(k) &lt;= IndRow(k))
<a name="l00361"></a>00361               {
<a name="l00362"></a>00362                 <span class="comment">// We store only the superior part of the symmetric matrix.</span>
<a name="l00363"></a>00363                 <span class="keywordtype">int</span> ind_tmp = IndRow(k);
<a name="l00364"></a>00364                 IndRow(k) = IndCol(k);
<a name="l00365"></a>00365                 IndCol(k) = ind_tmp;
<a name="l00366"></a>00366               }
<a name="l00367"></a>00367             k++;
<a name="l00368"></a>00368           }
<a name="l00369"></a>00369       }
<a name="l00370"></a>00370 
<a name="l00371"></a>00371     <span class="comment">// We sort by row number.</span>
<a name="l00372"></a>00372     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(nnz_real, IndRow, IndCol, Val);
<a name="l00373"></a>00373 
<a name="l00374"></a>00374     <span class="comment">// A is filled.</span>
<a name="l00375"></a>00375     k = 0;
<a name="l00376"></a>00376     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00377"></a>00377       {
<a name="l00378"></a>00378         <span class="keywordtype">int</span> first_index = k;
<a name="l00379"></a>00379         <span class="comment">// We get the size of the row i.</span>
<a name="l00380"></a>00380         <span class="keywordflow">while</span> (k &lt; nnz_real &amp;&amp; IndRow(k) &lt;= i)
<a name="l00381"></a>00381           k++;
<a name="l00382"></a>00382 
<a name="l00383"></a>00383         <span class="keywordtype">int</span> size_row = k - first_index;
<a name="l00384"></a>00384         <span class="comment">// If row not empty.</span>
<a name="l00385"></a>00385         <span class="keywordflow">if</span> (size_row &gt; 0)
<a name="l00386"></a>00386           {
<a name="l00387"></a>00387             A.ReallocateRealRow(i, size_row);
<a name="l00388"></a>00388             k = first_index;
<a name="l00389"></a>00389             <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(k, k+size_row-1, IndCol, Val);
<a name="l00390"></a>00390             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_row; j++)
<a name="l00391"></a>00391               {
<a name="l00392"></a>00392                 A.IndexReal(i,j) = IndCol(k);
<a name="l00393"></a>00393                 A.ValueReal(i,j) = Val(k);
<a name="l00394"></a>00394                 k++;
<a name="l00395"></a>00395               }
<a name="l00396"></a>00396           }
<a name="l00397"></a>00397         <span class="keywordflow">else</span>
<a name="l00398"></a>00398           A.ClearRealRow(i);
<a name="l00399"></a>00399       }
<a name="l00400"></a>00400 
<a name="l00401"></a>00401     <span class="comment">// Same procedure for imaginary part.</span>
<a name="l00402"></a>00402 
<a name="l00403"></a>00403     IndRow.Reallocate(nnz_imag);
<a name="l00404"></a>00404     IndCol.Reallocate(nnz_imag);
<a name="l00405"></a>00405     Val.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a6c1667ecf1265c0870ba2828b33a2d79" title="Vector reallocation.">Reallocate</a>(nnz_imag);
<a name="l00406"></a>00406 
<a name="l00407"></a>00407     <span class="comment">// First we convert the matrix in coordinate format and we permute the</span>
<a name="l00408"></a>00408     <span class="comment">// indices.</span>
<a name="l00409"></a>00409     <span class="comment">// IndRow -&gt; indices of the permuted rows</span>
<a name="l00410"></a>00410     <span class="comment">// IndCol -&gt; indices of the permuted columns</span>
<a name="l00411"></a>00411     k = 0;
<a name="l00412"></a>00412     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00413"></a>00413       {
<a name="l00414"></a>00414         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++)
<a name="l00415"></a>00415           {
<a name="l00416"></a>00416             IndRow(k) = row_perm(i);
<a name="l00417"></a>00417             Val(k) = A.ValueImag(i,j);
<a name="l00418"></a>00418             IndCol(k) = col_perm(A.IndexImag(i,j));
<a name="l00419"></a>00419             <span class="keywordflow">if</span> (IndCol(k) &lt;= IndRow(k))
<a name="l00420"></a>00420               {
<a name="l00421"></a>00421                 <span class="comment">// We store only the superior part of the symmetric matrix.</span>
<a name="l00422"></a>00422                 <span class="keywordtype">int</span> ind_tmp = IndRow(k);
<a name="l00423"></a>00423                 IndRow(k) = IndCol(k);
<a name="l00424"></a>00424                 IndCol(k) = ind_tmp;
<a name="l00425"></a>00425               }
<a name="l00426"></a>00426             k++;
<a name="l00427"></a>00427           }
<a name="l00428"></a>00428       }
<a name="l00429"></a>00429     <span class="comment">// We sort by row number.</span>
<a name="l00430"></a>00430     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(nnz_imag, IndRow, IndCol, Val);
<a name="l00431"></a>00431 
<a name="l00432"></a>00432     <span class="comment">// A is filled</span>
<a name="l00433"></a>00433     k = 0;
<a name="l00434"></a>00434     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00435"></a>00435       {
<a name="l00436"></a>00436         <span class="keywordtype">int</span> first_index = k;
<a name="l00437"></a>00437         <span class="comment">// We get the size of the row i.</span>
<a name="l00438"></a>00438         <span class="keywordflow">while</span> (k &lt; nnz_imag &amp;&amp; IndRow(k) &lt;= i)
<a name="l00439"></a>00439           k++;
<a name="l00440"></a>00440         <span class="keywordtype">int</span> size_row = k - first_index;
<a name="l00441"></a>00441         <span class="comment">// If row not empty.</span>
<a name="l00442"></a>00442         <span class="keywordflow">if</span> (size_row &gt; 0)
<a name="l00443"></a>00443           {
<a name="l00444"></a>00444             A.ReallocateImagRow(i, size_row);
<a name="l00445"></a>00445             k = first_index;
<a name="l00446"></a>00446             <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(k, k+size_row-1, IndCol, Val);
<a name="l00447"></a>00447             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_row; j++)
<a name="l00448"></a>00448               {
<a name="l00449"></a>00449                 A.IndexImag(i,j) = IndCol(k);
<a name="l00450"></a>00450                 A.ValueImag(i,j) = Val(k);
<a name="l00451"></a>00451                 k++;
<a name="l00452"></a>00452               }
<a name="l00453"></a>00453           }
<a name="l00454"></a>00454         <span class="keywordflow">else</span>
<a name="l00455"></a>00455           A.ClearImagRow(i);
<a name="l00456"></a>00456       }
<a name="l00457"></a>00457   }
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 
<a name="l00461"></a>00461 
<a name="l00465"></a>00465   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l00466"></a>00466   <span class="keywordtype">void</span>
<a name="l00467"></a><a class="code" href="namespace_seldon.php#ab5f87ebca19677ce2d5a4eacbb3765f3">00467</a>   <a class="code" href="namespace_seldon.php#a48edc9483ed12de114cfde40b9bed96e" title="Inverse permutation of a general matrix stored by rows.">ApplyInversePermutation</a>(<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php" title="Row-major symmetric sparse-matrix class.">Matrix&lt;T, Prop, ArrayRowSymSparse, Allocator&gt;</a>&amp; A,
<a name="l00468"></a>00468                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; row_perm, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">IVect</a>&amp; col_perm)
<a name="l00469"></a>00469   {
<a name="l00470"></a>00470     <span class="comment">// It is assumed that the permuted matrix is still symmetric! For example,</span>
<a name="l00471"></a>00471     <span class="comment">// the user can provide row_perm = col_perm.</span>
<a name="l00472"></a>00472     <span class="keywordtype">int</span> m = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f" title="Returns the number of rows.">GetM</a>();
<a name="l00473"></a>00473     <span class="keywordtype">int</span> nnz = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de" title="Returns the number of elements stored in memory.">GetDataSize</a>();
<a name="l00474"></a>00474     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> IndRow(nnz), IndCol(nnz);
<a name="l00475"></a>00475     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T,VectFull,Allocator&gt;</a> Val(nnz);
<a name="l00476"></a>00476 
<a name="l00477"></a>00477     <span class="comment">// First we convert the matrix in coordinate format and we permute the</span>
<a name="l00478"></a>00478     <span class="comment">// indices.</span>
<a name="l00479"></a>00479     <span class="comment">// IndRow -&gt; indices of the permuted rows</span>
<a name="l00480"></a>00480     <span class="comment">// IndCol -&gt; indices of the permuted columns</span>
<a name="l00481"></a>00481     <span class="keywordtype">int</span> k = 0;
<a name="l00482"></a>00482     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00483"></a>00483       {
<a name="l00484"></a>00484         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#af202c14cefd0cb751e4cf906ca5804e7" title="Returns the number of non-zero entries of a row.">GetRowSize</a>(i); j++)
<a name="l00485"></a>00485           {
<a name="l00486"></a>00486             IndRow(k) = row_perm(i);
<a name="l00487"></a>00487             Val(k) = A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i, j);
<a name="l00488"></a>00488             IndCol(k) = col_perm(A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i, j));
<a name="l00489"></a>00489             <span class="keywordflow">if</span> (IndCol(k) &lt;= IndRow(k))
<a name="l00490"></a>00490               {
<a name="l00491"></a>00491                 <span class="comment">// We store only the superior part of the symmetric matrix.</span>
<a name="l00492"></a>00492                 <span class="keywordtype">int</span> ind_tmp = IndRow(k);
<a name="l00493"></a>00493                 IndRow(k) = IndCol(k);
<a name="l00494"></a>00494                 IndCol(k) = ind_tmp;
<a name="l00495"></a>00495               }
<a name="l00496"></a>00496             k++;
<a name="l00497"></a>00497           }
<a name="l00498"></a>00498       }
<a name="l00499"></a>00499     <span class="comment">// We sort with respect to row numbers.</span>
<a name="l00500"></a>00500     <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(nnz, IndRow, IndCol, Val);
<a name="l00501"></a>00501 
<a name="l00502"></a>00502     <span class="comment">// A is filled.</span>
<a name="l00503"></a>00503     k = 0;
<a name="l00504"></a>00504     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00505"></a>00505       {
<a name="l00506"></a>00506         <span class="keywordtype">int</span> first_index = k;
<a name="l00507"></a>00507         <span class="comment">// We get the size of the row i.</span>
<a name="l00508"></a>00508         <span class="keywordflow">while</span> (k &lt; nnz &amp;&amp; IndRow(k) &lt;= i)
<a name="l00509"></a>00509           k++;
<a name="l00510"></a>00510         <span class="keywordtype">int</span> size_row = k - first_index;
<a name="l00511"></a>00511         <span class="comment">// If row not empty.</span>
<a name="l00512"></a>00512         <span class="keywordflow">if</span> (size_row &gt; 0)
<a name="l00513"></a>00513           {
<a name="l00514"></a>00514             A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a02d5086adf71d37c259c1a94a375bd57" title="Reallocates row i.">ReallocateRow</a>(i, size_row);
<a name="l00515"></a>00515             k = first_index;
<a name="l00516"></a>00516             <a class="code" href="namespace_seldon.php#a129c1ed34afc2ceb66cfc3a934d9cdc5" title="Sorts vector V between a start position and an end position.">Sort</a>(k, k+size_row-1, IndCol, Val);
<a name="l00517"></a>00517             <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; size_row; j++)
<a name="l00518"></a>00518               {
<a name="l00519"></a>00519                 A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2" title="Returns column/row number of j-th non-zero value of row/column i.">Index</a>(i,j) = IndCol(k);
<a name="l00520"></a>00520                 A.<a class="code" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6" title="Returns j-th non-zero value of row/column i.">Value</a>(i,j) = Val(k);
<a name="l00521"></a>00521                 k++;
<a name="l00522"></a>00522               }
<a name="l00523"></a>00523           }
<a name="l00524"></a>00524         <span class="keywordflow">else</span>
<a name="l00525"></a>00525           A.<a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a1221b2abf3eb604ab208e9f839ccfa5c" title="Clears a row.">ClearRow</a>(i);
<a name="l00526"></a>00526       }
<a name="l00527"></a>00527   }
<a name="l00528"></a>00528 
<a name="l00529"></a>00529 
<a name="l00531"></a>00531 
<a name="l00534"></a>00534   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00535"></a>00535            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00536"></a><a class="code" href="namespace_seldon.php#a39f32d2bd0f5e16bb08da578a59e645c">00536</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a39f32d2bd0f5e16bb08da578a59e645c" title="Each row and column are scaled.">ScaleMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSparse, Allocator1&gt;</a>&amp; A,
<a name="l00537"></a>00537                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale_left,
<a name="l00538"></a>00538                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, VectFull, Allocator3&gt;</a>&amp; scale_right)
<a name="l00539"></a>00539   {
<a name="l00540"></a>00540     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00541"></a>00541     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00542"></a>00542       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++ )
<a name="l00543"></a>00543         A.Value(i,j) *= scale_left(i) * scale_right(A.Index(i, j));
<a name="l00544"></a>00544 
<a name="l00545"></a>00545   }
<a name="l00546"></a>00546 
<a name="l00547"></a>00547 
<a name="l00549"></a>00549 
<a name="l00552"></a>00552   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00553"></a>00553            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00554"></a><a class="code" href="namespace_seldon.php#a3bb6bb5805b17467a34a1febbd0c36f2">00554</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a39f32d2bd0f5e16bb08da578a59e645c" title="Each row and column are scaled.">ScaleMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSymSparse, Allocator1&gt;</a>&amp; A,
<a name="l00555"></a>00555                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale_left,
<a name="l00556"></a>00556                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, VectFull, Allocator3&gt;</a>&amp; scale_right)
<a name="l00557"></a>00557   {
<a name="l00558"></a>00558     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00559"></a>00559     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00560"></a>00560       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++ )
<a name="l00561"></a>00561         A.Value(i,j) *= scale_left(i) * scale_right(A.Index(i, j));
<a name="l00562"></a>00562 
<a name="l00563"></a>00563   }
<a name="l00564"></a>00564 
<a name="l00565"></a>00565 
<a name="l00567"></a>00567 
<a name="l00570"></a>00570   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00571"></a>00571            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00572"></a><a class="code" href="namespace_seldon.php#a7fde4dd40bb82c4740ce57765629df62">00572</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a39f32d2bd0f5e16bb08da578a59e645c" title="Each row and column are scaled.">ScaleMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSymComplexSparse, Allocator1&gt;</a>&amp; A,
<a name="l00573"></a>00573                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale_left,
<a name="l00574"></a>00574                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, VectFull, Allocator3&gt;</a>&amp; scale_right)
<a name="l00575"></a>00575   {
<a name="l00576"></a>00576     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00577"></a>00577     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00578"></a>00578       {
<a name="l00579"></a>00579         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++ )
<a name="l00580"></a>00580           A.ValueReal(i,j) *= scale_left(i) * scale_right(A.IndexReal(i, j));
<a name="l00581"></a>00581 
<a name="l00582"></a>00582         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++ )
<a name="l00583"></a>00583           A.ValueImag(i,j) *= scale_left(i) * scale_right(A.IndexImag(i, j));
<a name="l00584"></a>00584       }
<a name="l00585"></a>00585   }
<a name="l00586"></a>00586 
<a name="l00587"></a>00587 
<a name="l00589"></a>00589 
<a name="l00592"></a>00592   <span class="keyword">template</span>&lt;<span class="keyword">class </span>Prop, <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00593"></a>00593            <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00594"></a><a class="code" href="namespace_seldon.php#a5735705cb264df8ef1790ac3197191eb">00594</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a39f32d2bd0f5e16bb08da578a59e645c" title="Each row and column are scaled.">ScaleMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowComplexSparse, Allocator1&gt;</a>&amp; A,
<a name="l00595"></a>00595                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale_left,
<a name="l00596"></a>00596                    <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T3, VectFull, Allocator3&gt;</a>&amp; scale_right)
<a name="l00597"></a>00597   {
<a name="l00598"></a>00598     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00599"></a>00599     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00600"></a>00600       {
<a name="l00601"></a>00601         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++ )
<a name="l00602"></a>00602           A.ValueReal(i,j) *= scale_left(i) * scale_right(A.IndexReal(i, j));
<a name="l00603"></a>00603 
<a name="l00604"></a>00604         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++ )
<a name="l00605"></a>00605           A.ValueImag(i,j) *= scale_left(i) * scale_right(A.IndexImag(i, j));
<a name="l00606"></a>00606       }
<a name="l00607"></a>00607   }
<a name="l00608"></a>00608 
<a name="l00609"></a>00609 
<a name="l00611"></a>00611 
<a name="l00614"></a>00614   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00615"></a>00615            <span class="keyword">class </span>Prop, <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00616"></a><a class="code" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff">00616</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff" title="Each row is scaled.">ScaleLeftMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSparse, Allocator1&gt;</a>&amp; A,
<a name="l00617"></a>00617                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale)
<a name="l00618"></a>00618   {
<a name="l00619"></a>00619     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00620"></a>00620     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00621"></a>00621       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++ )
<a name="l00622"></a>00622         A.Value(i,j) *= scale(i);
<a name="l00623"></a>00623   }
<a name="l00624"></a>00624 
<a name="l00625"></a>00625 
<a name="l00627"></a>00627 
<a name="l00632"></a>00632   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00633"></a>00633            <span class="keyword">class </span>Prop, <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00634"></a><a class="code" href="namespace_seldon.php#a5a58bb05fbab41205c035adc88a30643">00634</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff" title="Each row is scaled.">ScaleLeftMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSymSparse, Allocator1&gt;</a>&amp; A,
<a name="l00635"></a>00635                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale)
<a name="l00636"></a>00636   {
<a name="l00637"></a>00637     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00638"></a>00638     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00639"></a>00639       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRowSize(i); j++ )
<a name="l00640"></a>00640         A.Value(i,j) *= scale(i);
<a name="l00641"></a>00641   }
<a name="l00642"></a>00642 
<a name="l00643"></a>00643 
<a name="l00645"></a>00645 
<a name="l00650"></a>00650   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00651"></a>00651            <span class="keyword">class </span>Prop, <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00652"></a><a class="code" href="namespace_seldon.php#a44516d3a3860128df71910cf704c795a">00652</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff" title="Each row is scaled.">ScaleLeftMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowSymComplexSparse, Allocator1&gt;</a>&amp; A,
<a name="l00653"></a>00653                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale)
<a name="l00654"></a>00654   {
<a name="l00655"></a>00655     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00656"></a>00656     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00657"></a>00657       {
<a name="l00658"></a>00658         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++ )
<a name="l00659"></a>00659           A.ValueReal(i,j) *= scale(i);
<a name="l00660"></a>00660 
<a name="l00661"></a>00661         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++ )
<a name="l00662"></a>00662           A.ValueImag(i,j) *= scale(i);
<a name="l00663"></a>00663       }
<a name="l00664"></a>00664   }
<a name="l00665"></a>00665 
<a name="l00666"></a>00666 
<a name="l00668"></a>00668 
<a name="l00671"></a>00671   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00672"></a>00672            <span class="keyword">class </span>Prop, <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00673"></a><a class="code" href="namespace_seldon.php#a203e00e8b7f1a6bb7530522f58f9f9e7">00673</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aac4a905258ca48097ab48b9bc6318dff" title="Each row is scaled.">ScaleLeftMatrix</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop, ArrayRowComplexSparse, Allocator1&gt;</a>&amp; A,
<a name="l00674"></a>00674                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, Allocator2&gt;</a>&amp; scale)
<a name="l00675"></a>00675   {
<a name="l00676"></a>00676     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00677"></a>00677     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++ )
<a name="l00678"></a>00678       {
<a name="l00679"></a>00679         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetRealRowSize(i); j++ )
<a name="l00680"></a>00680           A.ValueReal(i,j) *= scale(i);
<a name="l00681"></a>00681 
<a name="l00682"></a>00682         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetImagRowSize(i); j++ )
<a name="l00683"></a>00683           A.ValueImag(i,j) *= scale(i);
<a name="l00684"></a>00684       }
<a name="l00685"></a>00685 
<a name="l00686"></a>00686   }
<a name="l00687"></a>00687 
<a name="l00688"></a>00688 
<a name="l00689"></a>00689 } <span class="comment">// end namespace</span>
<a name="l00690"></a>00690 
<a name="l00691"></a>00691 <span class="preprocessor">#define SELDON_FILE_PERMUTATION_SCALING_MATRIX_CXX</span>
<a name="l00692"></a>00692 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
