<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected, static]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a98226901bde3cf19ae74571c38280792">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a43f929eaa7b183e23c49d9ad31e5699e">Copy</a>(const Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>data_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#acb848b52cbdb82504a1e79fd5e75a87a">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a498b54d93b35a42bfb524cc697bb8d5b">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a24f99cb9628f8259310d685a55ecbb86">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af88748a55208349367d6860ad76fd691">GetAllocator</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a">GetData</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0f2796430deb08e565df8ced656097bf">GetDataConst</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a5979450cd8801810229f4a24c36e6639">GetDataConstVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a8bd2747b6c90c4114cd2f54620d1f230">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a505cdfee34741c463e0dc1943337bedc">GetDataVoid</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927">GetM</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ab6f6c5bba065ca82dad7c3abdbc8ea79">GetM</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984">GetN</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a7d27412bc9384a5ce0c0db1ee310d31c">GetN</a>(const Seldon::SeldonTranspose &amp;status) const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a0fbc3f7030583174eaa69429f655a1b0">GetSize</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>m_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a45762f0c3a34c78f8d6e99b74f269e1f">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a11105f396c82d8936279b6ae815b5505">Matrix</a>(int i, int j=0)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a47988d7972247286332e853c13bbc00b">Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#ad2ea11b0880dc32ba9472da9310c332b">Matrix_Base</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline, explicit]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#a127e0229931486cea3e6007988b446a0">Matrix_Base</a>(const Matrix_Base&lt; T, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ad6584a1ca7199865d2a4d5dc6af246fe">Matrix_HermPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ad5435d242af5fabbac0f1941972045fe">Matrix_HermPacked</a>(int i, int j=0)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a0c7b7a12f90ac04b5c8d2c76089ebee1">Matrix_HermPacked</a>(const Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>n_</b> (defined in <a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a4837520f92011b63c5115a19c8c49ad3">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a53e8c9f819dad5bb1d40a9b6edd24005">operator*=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#ad8f1511c8abd5cf290c32c1ec43dad01">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85">Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;::operator=</a>(const Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt; &amp;A)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a">operator[]</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a77a7f1c5d26dc807a7fe5c7cebabd94b">operator[]</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a149556a4c7e8b8e9c952a7f3bc2f5942">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a379079777210c4dfd9377f5fbc843c85">Print</a>(int a, int b, int m, int n) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a51b162e562a758741735fa1a6875ec9e">Print</a>(int l) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a19e44f4967fe55af83ef524f0f9be125">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a973fdab38a4f413ceec28d4c6b696d0c">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#aacf187c2102385c003476b6674d27fa1">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a2c77fe462fbafce6965c88e55ebec79d">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#aa2584f5a75b2ae0a2beb12fa45de3c11">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a46426f0740af4a12d2873b9b692f6278">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>SetData</b>(int i, int j, pointer data) (defined in <a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a5e62a678cb672277c536d1ad9aad96f1">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ae7f67ca8d3876a5fbdbce8ac3c914f36">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a7eda051ec34190ad13528f89d233acdb">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a3a8a9450a83c1e5aaa301a8f33cc3eb1">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a57a7e0eb6fa3a34edfe3334ff460faec">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a33aa63ce5334152d99d56de03a103f45">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a15b7c7126f1e630b644a29d0c1d38ecd">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a6222342fc8d2e7fccf530c892e259782">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___base.php#af34a03c0cc56f757a83cd56f97c74ed8">~Matrix_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___base.php">Seldon::Matrix_Base&lt; T, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a2d9134dbc8606170ccaaf6aed472df26">~Matrix_HermPacked</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___herm_packed.php">Seldon::Matrix_HermPacked&lt; T, Prop, ColHermPacked, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
