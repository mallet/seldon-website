<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li class="current"><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="#index_a"><span>a</span></a></li>
      <li><a href="#index_c"><span>c</span></a></li>
      <li><a href="#index_d"><span>d</span></a></li>
      <li><a href="#index_e"><span>e</span></a></li>
      <li><a href="#index_f"><span>f</span></a></li>
      <li><a href="#index_i"><span>i</span></a></li>
      <li><a href="#index_l"><span>l</span></a></li>
      <li><a href="#index_m"><span>m</span></a></li>
      <li><a href="#index_n"><span>n</span></a></li>
      <li><a href="#index_o"><span>o</span></a></li>
      <li><a href="#index_p"><span>p</span></a></li>
      <li><a href="#index_r"><span>r</span></a></li>
      <li><a href="#index_s"><span>s</span></a></li>
      <li><a href="#index_t"><span>t</span></a></li>
      <li><a href="#index_u"><span>u</span></a></li>
      <li><a href="#index_v"><span>v</span></a></li>
      <li><a href="#index_x"><span>x</span></a></li>
    </ul>
  </div>
<div class="contents">
&nbsp;

<h3><a class="anchor" id="index_a"></a>- a -</h3><ul>
<li>A
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>additional_fill
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a6330d5dc074f658ce57e9516c47e62c4">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>alpha
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#adf9a5b3757a33be59c6406c1515c92da">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_c"></a>- c -</h3><ul>
<li>col_num
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#aeca666d886a9ab8e8e8e907963a6ed64">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>collection_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a068b8212aa98d09c129bb1952aeee722">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5ad322e3511725821e82e14d632b6be1">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>column_list_
: <a class="el" href="class_seldon_1_1_sub_matrix___base.php#aa0699422389286f5dab0b65a83ca0c2c">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
</li>
<li>comm_facto
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a92ec4775edc88ab697268cc96b7c5848">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>comment_
: <a class="el" href="class_seldon_1_1_error.php#a00e98cbd016cc12b92ee48e59a492275">Seldon::Error</a>
</li>
</ul>


<h3><a class="anchor" id="index_d"></a>- d -</h3><ul>
<li>data_
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#ad43800f106a0be99442759b9c63ae487">Seldon::MatrixUmfPack&lt; double &gt;</a>
</li>
<li>data_real_
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a7cfdb169fedc7f0b9a6233645011c637">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
</li>
<li>description_
: <a class="el" href="class_seldon_1_1_error.php#a5442a8a1dffc72f0e04dd9f746e1a3f0">Seldon::Error</a>
</li>
<li>display_info
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a00b2252a6082727f89fcba9ca33fbf7d">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>distributed
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0637a34811226bdedf63b3e71c7cef6">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>double_dense_c_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e75af5dfcebe3aa872890ee596d2120">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#aedc9e7d9e1d59170fce9cf501899b0f6">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>double_sparse_c_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1270cfa610abeaa07dd55863a33cff5c">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a8743b791c26f3513a2e911812083e482">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>dparm
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#ab0e31b30663fb336f638fc847663c1eb">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>droptol
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a1a9cb311683ab176d463234521dd7cad">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_e"></a>- e -</h3><ul>
<li>error_code
: <a class="el" href="class_seldon_1_1_iteration.php#a493984d8abaec3ac0d4f73c29d2a0e60">Seldon::Iteration&lt; Titer &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_f"></a>- f -</h3><ul>
<li>facteur_reste
: <a class="el" href="class_seldon_1_1_iteration.php#adf29aa77b9dd1ecdc57c132712b02811">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>fail_convergence
: <a class="el" href="class_seldon_1_1_iteration.php#afea45227a5d730ac1464b3c33dabfc9e">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>fill_level
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#aec89b77ae63f96dba008a092da78af17">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>float_dense_c_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a9ecb39a2c309d9cec57f7e97b448771d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#afc3abf3a8ecbed59428fe26bc70c094a">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>float_sparse_c_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a66e2ad59a6007f1dd4be614a8ade4b85">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a565fcd42d62a4540995fbc6df9938e37">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>function_
: <a class="el" href="class_seldon_1_1_error.php#a45bc7e61221de461fb4b92964b7b9058">Seldon::Error</a>
</li>
</ul>


<h3><a class="anchor" id="index_i"></a>- i -</h3><ul>
<li>ind_
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a80c2fab0cdd04087069ed6aee40e5ac6">Seldon::MatrixUmfPack&lt; double &gt;</a>
</li>
<li>Info
: <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>init_guess_null
: <a class="el" href="class_seldon_1_1_iteration.php#a363306d1242c5a2b78737ace82ea465b">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>iparm
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a3e0e4d255058e66c82419232b28fe178">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_l"></a>- l -</h3><ul>
<li>label_map_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#ad9ae13022169e5ecb85874a9d0655b37">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a03d0c667f3a6eb4034ab75a1dd133a58">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
</li>
<li>label_vector_
: <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ad0185be3cff90de10f820787aafe58ba">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5cf22412ddb84b63dae86c18efd1d46e">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>length_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a55b6c0b9a6ed0958ccb7414a95b40327">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>length_sum_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a1589bd7ba73b1d9e32fa11b96b57d36a">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>Lstore
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3639abafe150759f173dc6d6a654c534">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_m"></a>- m -</h3><ul>
<li>m_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a2032848b78edab7ef6721eda9e2784de">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>mat_sym
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a450694545524ae93de1df9810b135b91">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>mat_unsym
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a8e30b497ecb4698bb22d1c0c3ea19f7e">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>matrix_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a66297d4139bb2b5b07979ef929358399">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a39e77d13e783db34a7ea38c9ba6fc812">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
</li>
<li>max_iter
: <a class="el" href="class_seldon_1_1_iteration.php#a75359d82e80a14fa10907b203fc5a2ff">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>mbloc
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#accd555595ce35fcf2e2a2288c4585494">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>Mlocal_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a48d1d3d9da13f7923a6356e49815fe66">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a234a716260de98d93fc40fc2a3fd44fe">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Mlocal_sum_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#aa16e80513c77e58a2c1b3974e360f220">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#af934beebf115742be0a29cb452017a7b">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>Mmatrix_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a094131b6edf39309cf51ec6aea9c271b">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a67cb0eb87ea633caabecdc58714723ec">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_n"></a>- n -</h3><ul>
<li>n
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#ad07a8774a76b9fdc5a71210922d2924e">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>n_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#acfa7a2c6c7f77dc8ebcee2dd411ebf98">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>nb_iter
: <a class="el" href="class_seldon_1_1_iteration.php#a4ff83b0b358c1574c6c0ababbbe04745">Seldon::Iteration&lt; Titer &gt;</a>
, <a class="el" href="class_seldon_1_1_sor_preconditioner.php#a3b3bd355ce1b08110da7483a0b4944fe">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
<li>Nlocal_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a6946bf5a2c2a2dbaf307ce2ed1feea09">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#afdcab0ec07a154e7ed23ee20b2f4f574">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>Nlocal_sum_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#a2cd7c7804f76b6e8a6966e6597e05244">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a0d31be2a9c37790f5e6ec86ef556ac82">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>Nmatrix_
: <a class="el" href="class_seldon_1_1_matrix_collection.php#abf9ee2f169c7992c2d540d07dd85b104">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a37c185f5d163c0f8a32828828fde7252">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
</li>
<li>Numeric
: <a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea">Seldon::MatrixUmfPack_Base&lt; T &gt;</a>
</li>
<li>Nvector_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#addbb1b1aa4add4cfc19d53304ea60fb8">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>nz_
: <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a3137de2c1fe161a746dbde1f10c9128e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a960417c35add0c178e31e8578bd5eb9b">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_o"></a>- o -</h3><ul>
<li>omega
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#a38fb6b7c7b21677ee0042e1c3b5f4d6f">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
<li>options
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_p"></a>- p -</h3><ul>
<li>parameter_restart
: <a class="el" href="class_seldon_1_1_iteration.php#a5c4d36f6bff1c44d6e7b31bb3c70b603">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>pastix_data
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a1d728cf85201e0e4b35fd66c985ca95a">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>perm
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a0e40bdc9ac7c0ae1bd80e05d9942afee">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>permc_spec
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a8d4acdd57e0a5e8a8a0651e1de0a8547">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>permtol
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#af02ed9cb6916f9cb848ad774b7686d94">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>permutation_row
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a81f338ca2bcf311a3fb70d4086512632">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>print_level
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a138482f32cb3046a3faf597aff405fb3">Seldon::MatrixPastix&lt; T &gt;</a>
, <a class="el" href="class_seldon_1_1_iteration.php#a82adaef364a9a20a7525ab9299b6bfd5">Seldon::Iteration&lt; Titer &gt;</a>
, <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a11647ecc317646a9560944199040df92">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>ptr_
: <a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01complex_3_01double_01_4_01_4.php#a9a9a32db0c5d5edb1f6c7bc5e7560876">Seldon::MatrixUmfPack&lt; complex&lt; double &gt; &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_r"></a>- r -</h3><ul>
<li>rank
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>refine_solution
: <a class="el" href="class_seldon_1_1_matrix_pastix.php#a8c22604c5e88bb75086bde3f344cd4d5">Seldon::MatrixPastix&lt; T &gt;</a>
</li>
<li>row_list_
: <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a7af6e9c2c869742e856f10bc83f43ece">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_s"></a>- s -</h3><ul>
<li>stat
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
<li>subvector_
: <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a0e24041339f74050c878a52404373649">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
</li>
<li>symmetric_algorithm
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a87e3208fe6fed093679b21210726be46">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>symmetric_precond
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#a4e58db67067834701f1080f81fb2442d">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_t"></a>- t -</h3><ul>
<li>tolerance
: <a class="el" href="class_seldon_1_1_iteration.php#acf9550c0ec96e51fc5d395446342c4e8">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>type_ilu
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#a91394b185bd5974beb1f929ecf716e94">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
<li>type_ordering
: <a class="el" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c">Seldon::MatrixMumps&lt; T &gt;</a>
</li>
<li>type_preconditioning
: <a class="el" href="class_seldon_1_1_iteration.php#a19f60dd0946c1a5a212b0e28b5be55a9">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>type_solver
: <a class="el" href="class_seldon_1_1_iteration.php#aced354c0bf1e3868ae5b77283982b8fa">Seldon::Iteration&lt; Titer &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_u"></a>- u -</h3><ul>
<li>Ustore
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3c0030723c06e7bffc470c94b344c00e">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_v"></a>- v -</h3><ul>
<li>val_
: <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>val_imag_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#afa77c763d3b3a280dfef2a4410ed0bd8">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>val_real_
: <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#af1766a47a3d45c0b904b07870204e4a6">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
</ul>


<h3><a class="anchor" id="index_x"></a>- x -</h3><ul>
<li>xtmp
: <a class="el" href="class_seldon_1_1_ilut_preconditioning.php#ad57d0e7c6dc5468f6c8fbf032e0a9327">Seldon::IlutPreconditioning&lt; real, cplx, Allocator &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
