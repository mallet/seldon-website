<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix_sparse/Matrix_Sparse.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_SPARSE_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Sparse.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../vector/Functions_Arrays.cxx&quot;</span>
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="preprocessor">#include &lt;set&gt;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 <span class="keyword">namespace </span>Seldon
<a name="l00031"></a>00031 {
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00034"></a>00034   <span class="comment">/****************</span>
<a name="l00035"></a>00035 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00036"></a>00036 <span class="comment">   ****************/</span>
<a name="l00037"></a>00037 
<a name="l00038"></a>00038 
<a name="l00040"></a>00040 
<a name="l00043"></a>00043   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00044"></a>00044   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_Sparse</a>():
<a name="l00045"></a>00045     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00046"></a>00046   {
<a name="l00047"></a>00047     nz_ = 0;
<a name="l00048"></a>00048     ptr_ = NULL;
<a name="l00049"></a>00049     ind_ = NULL;
<a name="l00050"></a>00050   }
<a name="l00051"></a>00051 
<a name="l00052"></a>00052 
<a name="l00054"></a>00054 
<a name="l00059"></a>00059   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00060"></a>00060   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Matrix_Sparse</a>(<span class="keywordtype">int</span> i,
<a name="l00061"></a>00061                                                                    <span class="keywordtype">int</span> j):
<a name="l00062"></a>00062     Matrix_Base&lt;T, Allocator&gt;(i, j)
<a name="l00063"></a>00063   {
<a name="l00064"></a>00064     nz_ = 0;
<a name="l00065"></a>00065     ptr_ = NULL;
<a name="l00066"></a>00066     ind_ = NULL;
<a name="l00067"></a>00067   }
<a name="l00068"></a>00068 
<a name="l00069"></a>00069 
<a name="l00071"></a>00071 
<a name="l00078"></a>00078   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00079"></a>00079   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00080"></a>00080 <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">  Matrix_Sparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l00081"></a>00081     Matrix_Base&lt;T, Allocator&gt;(i, j)
<a name="l00082"></a>00082   {
<a name="l00083"></a>00083 
<a name="l00084"></a>00084     this-&gt;nz_ = nz;
<a name="l00085"></a>00085 
<a name="l00086"></a>00086 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00087"></a>00087 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (nz_ &lt; 0)
<a name="l00088"></a>00088       {
<a name="l00089"></a>00089         this-&gt;m_ = 0;
<a name="l00090"></a>00090         this-&gt;n_ = 0;
<a name="l00091"></a>00091         nz_ = 0;
<a name="l00092"></a>00092         ptr_ = NULL;
<a name="l00093"></a>00093         ind_ = NULL;
<a name="l00094"></a>00094         this-&gt;data_ = NULL;
<a name="l00095"></a>00095         <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00096"></a>00096                        <span class="stringliteral">&quot;Invalid number of non-zero elements: &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00097"></a>00097                        + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00098"></a>00098       }
<a name="l00099"></a>00099     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00100"></a>00100         &amp;&amp; (j == 0
<a name="l00101"></a>00101             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00102"></a>00102             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00103"></a>00103       {
<a name="l00104"></a>00104         this-&gt;m_ = 0;
<a name="l00105"></a>00105         this-&gt;n_ = 0;
<a name="l00106"></a>00106         nz_ = 0;
<a name="l00107"></a>00107         ptr_ = NULL;
<a name="l00108"></a>00108         ind_ = NULL;
<a name="l00109"></a>00109         this-&gt;data_ = NULL;
<a name="l00110"></a>00110         <span class="keywordflow">throw</span> WrongDim(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00111"></a>00111                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00112"></a>00112                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00113"></a>00113                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00114"></a>00114       }
<a name="l00115"></a>00115 <span class="preprocessor">#endif</span>
<a name="l00116"></a>00116 <span class="preprocessor"></span>
<a name="l00117"></a>00117 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00118"></a>00118 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00119"></a>00119       {
<a name="l00120"></a>00120 <span class="preprocessor">#endif</span>
<a name="l00121"></a>00121 <span class="preprocessor"></span>
<a name="l00122"></a>00122         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00123"></a>00123                                               <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00124"></a>00124 
<a name="l00125"></a>00125 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00126"></a>00126 <span class="preprocessor"></span>      }
<a name="l00127"></a>00127     <span class="keywordflow">catch</span> (...)
<a name="l00128"></a>00128       {
<a name="l00129"></a>00129         this-&gt;m_ = 0;
<a name="l00130"></a>00130         this-&gt;n_ = 0;
<a name="l00131"></a>00131         nz_ = 0;
<a name="l00132"></a>00132         ptr_ = NULL;
<a name="l00133"></a>00133         ind_ = NULL;
<a name="l00134"></a>00134         this-&gt;data_ = NULL;
<a name="l00135"></a>00135       }
<a name="l00136"></a>00136     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00137"></a>00137       {
<a name="l00138"></a>00138         this-&gt;m_ = 0;
<a name="l00139"></a>00139         this-&gt;n_ = 0;
<a name="l00140"></a>00140         nz_ = 0;
<a name="l00141"></a>00141         ind_ = NULL;
<a name="l00142"></a>00142         this-&gt;data_ = NULL;
<a name="l00143"></a>00143       }
<a name="l00144"></a>00144     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00145"></a>00145       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00146"></a>00146                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00147"></a>00147                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00148"></a>00148                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00149"></a>00149                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00150"></a>00150                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00151"></a>00151 <span class="preprocessor">#endif</span>
<a name="l00152"></a>00152 <span class="preprocessor"></span>
<a name="l00153"></a>00153 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00154"></a>00154 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00155"></a>00155       {
<a name="l00156"></a>00156 <span class="preprocessor">#endif</span>
<a name="l00157"></a>00157 <span class="preprocessor"></span>
<a name="l00158"></a>00158         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00159"></a>00159 
<a name="l00160"></a>00160 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00161"></a>00161 <span class="preprocessor"></span>      }
<a name="l00162"></a>00162     <span class="keywordflow">catch</span> (...)
<a name="l00163"></a>00163       {
<a name="l00164"></a>00164         this-&gt;m_ = 0;
<a name="l00165"></a>00165         this-&gt;n_ = 0;
<a name="l00166"></a>00166         nz_ = 0;
<a name="l00167"></a>00167         free(ptr_);
<a name="l00168"></a>00168         ptr_ = NULL;
<a name="l00169"></a>00169         ind_ = NULL;
<a name="l00170"></a>00170         this-&gt;data_ = NULL;
<a name="l00171"></a>00171       }
<a name="l00172"></a>00172     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00173"></a>00173       {
<a name="l00174"></a>00174         this-&gt;m_ = 0;
<a name="l00175"></a>00175         this-&gt;n_ = 0;
<a name="l00176"></a>00176         nz_ = 0;
<a name="l00177"></a>00177         free(ptr_);
<a name="l00178"></a>00178         ptr_ = NULL;
<a name="l00179"></a>00179         this-&gt;data_ = NULL;
<a name="l00180"></a>00180       }
<a name="l00181"></a>00181     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00182"></a>00182       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00183"></a>00183                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00184"></a>00184                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00185"></a>00185                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00186"></a>00186                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00187"></a>00187 <span class="preprocessor">#endif</span>
<a name="l00188"></a>00188 <span class="preprocessor"></span>
<a name="l00189"></a>00189 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00190"></a>00190 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00191"></a>00191       {
<a name="l00192"></a>00192 <span class="preprocessor">#endif</span>
<a name="l00193"></a>00193 <span class="preprocessor"></span>
<a name="l00194"></a>00194         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00195"></a>00195 
<a name="l00196"></a>00196 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00197"></a>00197 <span class="preprocessor"></span>      }
<a name="l00198"></a>00198     <span class="keywordflow">catch</span> (...)
<a name="l00199"></a>00199       {
<a name="l00200"></a>00200         this-&gt;m_ = 0;
<a name="l00201"></a>00201         this-&gt;n_ = 0;
<a name="l00202"></a>00202         free(ptr_);
<a name="l00203"></a>00203         ptr_ = NULL;
<a name="l00204"></a>00204         free(ind_);
<a name="l00205"></a>00205         ind_ = NULL;
<a name="l00206"></a>00206         this-&gt;data_ = NULL;
<a name="l00207"></a>00207       }
<a name="l00208"></a>00208     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00209"></a>00209       {
<a name="l00210"></a>00210         this-&gt;m_ = 0;
<a name="l00211"></a>00211         this-&gt;n_ = 0;
<a name="l00212"></a>00212         free(ptr_);
<a name="l00213"></a>00213         ptr_ = NULL;
<a name="l00214"></a>00214         free(ind_);
<a name="l00215"></a>00215         ind_ = NULL;
<a name="l00216"></a>00216       }
<a name="l00217"></a>00217     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00218"></a>00218       <span class="keywordflow">throw</span> NoMemory(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00219"></a>00219                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00220"></a>00220                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00221"></a>00221                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00222"></a>00222 <span class="preprocessor">#endif</span>
<a name="l00223"></a>00223 <span class="preprocessor"></span>
<a name="l00224"></a>00224   }
<a name="l00225"></a>00225 
<a name="l00226"></a>00226 
<a name="l00228"></a>00228 
<a name="l00239"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a2ff242fb46f84f041e4709a7eff70cd3">00239</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00240"></a>00240   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00241"></a>00241             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00242"></a>00242             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00243"></a>00243   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00244"></a>00244 <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">  Matrix_Sparse</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00245"></a>00245                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00246"></a>00246                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00247"></a>00247                 <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l00248"></a>00248     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00249"></a>00249   {
<a name="l00250"></a>00250     nz_ = values.GetLength();
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00253"></a>00253 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00254"></a>00254 
<a name="l00255"></a>00255     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00256"></a>00256       {
<a name="l00257"></a>00257         this-&gt;m_ = 0;
<a name="l00258"></a>00258         this-&gt;n_ = 0;
<a name="l00259"></a>00259         nz_ = 0;
<a name="l00260"></a>00260         ptr_ = NULL;
<a name="l00261"></a>00261         ind_ = NULL;
<a name="l00262"></a>00262         this-&gt;data_ = NULL;
<a name="l00263"></a>00263         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00264"></a>00264                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00265"></a>00265                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00266"></a>00266                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00267"></a>00267       }
<a name="l00268"></a>00268 
<a name="l00269"></a>00269     <span class="keywordflow">if</span> (ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00270"></a>00270       {
<a name="l00271"></a>00271         this-&gt;m_ = 0;
<a name="l00272"></a>00272         this-&gt;n_ = 0;
<a name="l00273"></a>00273         nz_ = 0;
<a name="l00274"></a>00274         ptr_ = NULL;
<a name="l00275"></a>00275         ind_ = NULL;
<a name="l00276"></a>00276         this-&gt;data_ = NULL;
<a name="l00277"></a>00277         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00278"></a>00278                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00279"></a>00279                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00280"></a>00280                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00281"></a>00281                        + string(<span class="stringliteral">&quot; start indices (plus the number&quot;</span>)
<a name="l00282"></a>00282                        + <span class="stringliteral">&quot; of non-zero entries) but there are &quot;</span>
<a name="l00283"></a>00283                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00284"></a>00284                        + <span class="stringliteral">&quot; rows or columns (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00285"></a>00285                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00286"></a>00286       }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00289"></a>00289         &amp;&amp; (j == 0
<a name="l00290"></a>00290             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00291"></a>00291             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00292"></a>00292       {
<a name="l00293"></a>00293         this-&gt;m_ = 0;
<a name="l00294"></a>00294         this-&gt;n_ = 0;
<a name="l00295"></a>00295         nz_ = 0;
<a name="l00296"></a>00296         ptr_ = NULL;
<a name="l00297"></a>00297         ind_ = NULL;
<a name="l00298"></a>00298         this-&gt;data_ = NULL;
<a name="l00299"></a>00299         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, &quot;</span>)
<a name="l00300"></a>00300                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00301"></a>00301                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00302"></a>00302                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00303"></a>00303                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00304"></a>00304                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00305"></a>00305       }
<a name="l00306"></a>00306 <span class="preprocessor">#endif</span>
<a name="l00307"></a>00307 <span class="preprocessor"></span>
<a name="l00308"></a>00308     this-&gt;ptr_ = ptr.GetData();
<a name="l00309"></a>00309     this-&gt;ind_ = ind.GetData();
<a name="l00310"></a>00310     this-&gt;data_ = values.GetData();
<a name="l00311"></a>00311 
<a name="l00312"></a>00312     ptr.Nullify();
<a name="l00313"></a>00313     ind.Nullify();
<a name="l00314"></a>00314     values.Nullify();
<a name="l00315"></a>00315   }
<a name="l00316"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a49f2012fc254ff9e960b1dfee766ce0f">00316</a> 
<a name="l00317"></a>00317 
<a name="l00319"></a>00319   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00320"></a>00320   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00321"></a>00321 <a class="code" href="class_seldon_1_1_matrix___sparse.php#af4624e0ed00838ec34b030103f9dd383" title="Default constructor.">  Matrix_Sparse</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00322"></a>00322   {
<a name="l00323"></a>00323     this-&gt;m_ = 0;
<a name="l00324"></a>00324     this-&gt;n_ = 0;
<a name="l00325"></a>00325     this-&gt;nz_ = 0;
<a name="l00326"></a>00326     ptr_ = NULL;
<a name="l00327"></a>00327     ind_ = NULL;
<a name="l00328"></a>00328     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">Copy</a>(A);
<a name="l00329"></a>00329   }
<a name="l00330"></a>00330 
<a name="l00331"></a>00331 
<a name="l00332"></a>00332   <span class="comment">/**************</span>
<a name="l00333"></a>00333 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00334"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28">00334</a> <span class="comment">   **************/</span>
<a name="l00335"></a>00335 
<a name="l00336"></a>00336 
<a name="l00338"></a>00338   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00339"></a>00339   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28" title="Destructor.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Sparse</a>()
<a name="l00340"></a>00340   {
<a name="l00341"></a>00341     this-&gt;m_ = 0;
<a name="l00342"></a>00342     this-&gt;n_ = 0;
<a name="l00343"></a>00343 
<a name="l00344"></a>00344 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00345"></a>00345 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00346"></a>00346       {
<a name="l00347"></a>00347 <span class="preprocessor">#endif</span>
<a name="l00348"></a>00348 <span class="preprocessor"></span>
<a name="l00349"></a>00349         <span class="keywordflow">if</span> (ptr_ != NULL)
<a name="l00350"></a>00350           {
<a name="l00351"></a>00351             free(ptr_);
<a name="l00352"></a>00352             ptr_ = NULL;
<a name="l00353"></a>00353           }
<a name="l00354"></a>00354 
<a name="l00355"></a>00355 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00356"></a>00356 <span class="preprocessor"></span>      }
<a name="l00357"></a>00357     <span class="keywordflow">catch</span> (...)
<a name="l00358"></a>00358       {
<a name="l00359"></a>00359         ptr_ = NULL;
<a name="l00360"></a>00360       }
<a name="l00361"></a>00361 <span class="preprocessor">#endif</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>
<a name="l00363"></a>00363 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00364"></a>00364 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00365"></a>00365       {
<a name="l00366"></a>00366 <span class="preprocessor">#endif</span>
<a name="l00367"></a>00367 <span class="preprocessor"></span>
<a name="l00368"></a>00368         <span class="keywordflow">if</span> (ind_ != NULL)
<a name="l00369"></a>00369           {
<a name="l00370"></a>00370             free(ind_);
<a name="l00371"></a>00371             ind_ = NULL;
<a name="l00372"></a>00372           }
<a name="l00373"></a>00373 
<a name="l00374"></a>00374 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00375"></a>00375 <span class="preprocessor"></span>      }
<a name="l00376"></a>00376     <span class="keywordflow">catch</span> (...)
<a name="l00377"></a>00377       {
<a name="l00378"></a>00378         ind_ = NULL;
<a name="l00379"></a>00379       }
<a name="l00380"></a>00380 <span class="preprocessor">#endif</span>
<a name="l00381"></a>00381 <span class="preprocessor"></span>
<a name="l00382"></a>00382 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00383"></a>00383 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00384"></a>00384       {
<a name="l00385"></a>00385 <span class="preprocessor">#endif</span>
<a name="l00386"></a>00386 <span class="preprocessor"></span>
<a name="l00387"></a>00387         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00388"></a>00388           {
<a name="l00389"></a>00389             this-&gt;allocator_.deallocate(this-&gt;data_, nz_);
<a name="l00390"></a>00390             this-&gt;data_ = NULL;
<a name="l00391"></a>00391           }
<a name="l00392"></a>00392 
<a name="l00393"></a>00393 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00394"></a>00394 <span class="preprocessor"></span>      }
<a name="l00395"></a>00395     <span class="keywordflow">catch</span> (...)
<a name="l00396"></a>00396       {
<a name="l00397"></a>00397         this-&gt;nz_ = 0;
<a name="l00398"></a>00398         this-&gt;data_ = NULL;
<a name="l00399"></a>00399       }
<a name="l00400"></a>00400 <span class="preprocessor">#endif</span>
<a name="l00401"></a>00401 <span class="preprocessor"></span>
<a name="l00402"></a>00402     this-&gt;nz_ = 0;
<a name="l00403"></a>00403   }
<a name="l00404"></a>00404 
<a name="l00405"></a>00405 
<a name="l00407"></a>00407 
<a name="l00410"></a>00410   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00411"></a>00411   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00412"></a>00412   {
<a name="l00413"></a>00413     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a805ce704fffb3c509d908ec3ac45db28" title="Destructor.">~Matrix_Sparse</a>();
<a name="l00414"></a>00414   }
<a name="l00415"></a>00415 
<a name="l00416"></a>00416 
<a name="l00417"></a>00417   <span class="comment">/*********************</span>
<a name="l00418"></a>00418 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00419"></a>00419 <span class="comment">   *********************/</span>
<a name="l00420"></a>00420 
<a name="l00421"></a>00421 
<a name="l00423"></a>00423 
<a name="l00433"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09">00433</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00434"></a>00434   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00435"></a>00435             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00436"></a>00436             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00437"></a>00437   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00438"></a>00438 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">  SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00439"></a>00439           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l00440"></a>00440           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l00441"></a>00441           <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind)
<a name="l00442"></a>00442   {
<a name="l00443"></a>00443     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00444"></a>00444     this-&gt;m_ = i;
<a name="l00445"></a>00445     this-&gt;n_ = j;
<a name="l00446"></a>00446     this-&gt;nz_ = values.GetLength();
<a name="l00447"></a>00447 
<a name="l00448"></a>00448 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00449"></a>00449 <span class="preprocessor"></span>    <span class="comment">// Checks whether vector sizes are acceptable.</span>
<a name="l00450"></a>00450 
<a name="l00451"></a>00451     <span class="keywordflow">if</span> (ind.GetLength() != nz_)
<a name="l00452"></a>00452       {
<a name="l00453"></a>00453         this-&gt;m_ = 0;
<a name="l00454"></a>00454         this-&gt;n_ = 0;
<a name="l00455"></a>00455         nz_ = 0;
<a name="l00456"></a>00456         ptr_ = NULL;
<a name="l00457"></a>00457         ind_ = NULL;
<a name="l00458"></a>00458         this-&gt;data_ = NULL;
<a name="l00459"></a>00459         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00460"></a>00460                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00461"></a>00461                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz_) + <span class="stringliteral">&quot; values but &quot;</span>
<a name="l00462"></a>00462                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ind.GetLength()) + <span class="stringliteral">&quot; row or column indices.&quot;</span>);
<a name="l00463"></a>00463       }
<a name="l00464"></a>00464 
<a name="l00465"></a>00465     <span class="keywordflow">if</span> (ptr.GetLength()-1 != Storage::GetFirst(i, j))
<a name="l00466"></a>00466       {
<a name="l00467"></a>00467         this-&gt;m_ = 0;
<a name="l00468"></a>00468         this-&gt;n_ = 0;
<a name="l00469"></a>00469         nz_ = 0;
<a name="l00470"></a>00470         ptr_ = NULL;
<a name="l00471"></a>00471         ind_ = NULL;
<a name="l00472"></a>00472         this-&gt;data_ = NULL;
<a name="l00473"></a>00473         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00474"></a>00474                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00475"></a>00475                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;The vector of start indices contains &quot;</span>)
<a name="l00476"></a>00476                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(ptr.GetLength()-1) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; row or column&quot;</span>)
<a name="l00477"></a>00477                        + string(<span class="stringliteral">&quot; start indices (plus the number&quot;</span>)
<a name="l00478"></a>00478                        + <span class="stringliteral">&quot; of non-zero entries) but there are &quot;</span>
<a name="l00479"></a>00479                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j))
<a name="l00480"></a>00480                        + <span class="stringliteral">&quot; rows or columns (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span>
<a name="l00481"></a>00481                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix).&quot;</span>);
<a name="l00482"></a>00482       }
<a name="l00483"></a>00483 
<a name="l00484"></a>00484     <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00485"></a>00485         &amp;&amp; (j == 0
<a name="l00486"></a>00486             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00487"></a>00487             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00488"></a>00488       {
<a name="l00489"></a>00489         this-&gt;m_ = 0;
<a name="l00490"></a>00490         this-&gt;n_ = 0;
<a name="l00491"></a>00491         nz_ = 0;
<a name="l00492"></a>00492         ptr_ = NULL;
<a name="l00493"></a>00493         ind_ = NULL;
<a name="l00494"></a>00494         this-&gt;data_ = NULL;
<a name="l00495"></a>00495         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="keywordtype">string</span>(<span class="stringliteral">&quot;Matrix_Sparse::SetData(int, int, &quot;</span>)
<a name="l00496"></a>00496                        + <span class="stringliteral">&quot;const Vector&amp;, const Vector&amp;, const Vector&amp;)&quot;</span>,
<a name="l00497"></a>00497                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>)
<a name="l00498"></a>00498                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(values.GetLength())
<a name="l00499"></a>00499                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00500"></a>00500                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00501"></a>00501       }
<a name="l00502"></a>00502 <span class="preprocessor">#endif</span>
<a name="l00503"></a>00503 <span class="preprocessor"></span>
<a name="l00504"></a>00504     this-&gt;ptr_ = ptr.GetData();
<a name="l00505"></a>00505     this-&gt;ind_ = ind.GetData();
<a name="l00506"></a>00506     this-&gt;data_ = values.GetData();
<a name="l00507"></a>00507 
<a name="l00508"></a>00508     ptr.Nullify();
<a name="l00509"></a>00509     ind.Nullify();
<a name="l00510"></a>00510     values.Nullify();
<a name="l00511"></a>00511   }
<a name="l00512"></a>00512 
<a name="l00513"></a>00513 
<a name="l00515"></a>00515 
<a name="l00528"></a>00528   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00529"></a>00529   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00530"></a>00530 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz,
<a name="l00531"></a>00531             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00532"></a>00532             ::pointer values,
<a name="l00533"></a>00533             <span class="keywordtype">int</span>* ptr, <span class="keywordtype">int</span>* ind)
<a name="l00534"></a>00534   {
<a name="l00535"></a>00535     this-&gt;Clear();
<a name="l00536"></a>00536 
<a name="l00537"></a>00537     this-&gt;m_ = i;
<a name="l00538"></a>00538     this-&gt;n_ = j;
<a name="l00539"></a>00539 
<a name="l00540"></a>00540     this-&gt;nz_ = nz;
<a name="l00541"></a>00541 
<a name="l00542"></a>00542     this-&gt;data_ = values;
<a name="l00543"></a>00543     ind_ = ind;
<a name="l00544"></a>00544     ptr_ = ptr;
<a name="l00545"></a>00545   }
<a name="l00546"></a>00546 
<a name="l00547"></a>00547 
<a name="l00549"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9">00549</a> 
<a name="l00553"></a>00553   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00554"></a>00554   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a805231f8ec04efc5a081b21c9f65d5c9" title="Clears the matrix without releasing memory.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00555"></a>00555   {
<a name="l00556"></a>00556     this-&gt;data_ = NULL;
<a name="l00557"></a>00557     this-&gt;m_ = 0;
<a name="l00558"></a>00558     this-&gt;n_ = 0;
<a name="l00559"></a>00559     nz_ = 0;
<a name="l00560"></a>00560     ptr_ = NULL;
<a name="l00561"></a>00561     ind_ = NULL;
<a name="l00562"></a>00562   }
<a name="l00563"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77">00563</a> 
<a name="l00564"></a>00564 
<a name="l00566"></a>00566   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00567"></a>00567   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l00568"></a>00568 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a54604e7e696124d278a4aa198edcab77" title="Copies a matrix.">  Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00569"></a>00569   {
<a name="l00570"></a>00570     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l00571"></a>00571     <span class="keywordtype">int</span> nz = A.nz_;
<a name="l00572"></a>00572     <span class="keywordtype">int</span> i = A.m_;
<a name="l00573"></a>00573     <span class="keywordtype">int</span> j = A.n_;
<a name="l00574"></a>00574     this-&gt;nz_ = nz;
<a name="l00575"></a>00575     this-&gt;m_ = i;
<a name="l00576"></a>00576     this-&gt;n_ = j;
<a name="l00577"></a>00577     <span class="keywordflow">if</span> ((i == 0)||(j == 0))
<a name="l00578"></a>00578       {
<a name="l00579"></a>00579         this-&gt;m_ = 0;
<a name="l00580"></a>00580         this-&gt;n_ = 0;
<a name="l00581"></a>00581         this-&gt;nz_ = 0;
<a name="l00582"></a>00582         <span class="keywordflow">return</span>;
<a name="l00583"></a>00583       }
<a name="l00584"></a>00584 
<a name="l00585"></a>00585 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00586"></a>00586 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (nz_ &gt; 0
<a name="l00587"></a>00587         &amp;&amp; (j == 0
<a name="l00588"></a>00588             || static_cast&lt;long int&gt;(nz_-1) / static_cast&lt;long int&gt;(j)
<a name="l00589"></a>00589             &gt;= static_cast&lt;long int&gt;(i)))
<a name="l00590"></a>00590       {
<a name="l00591"></a>00591         this-&gt;m_ = 0;
<a name="l00592"></a>00592         this-&gt;n_ = 0;
<a name="l00593"></a>00593         nz_ = 0;
<a name="l00594"></a>00594         ptr_ = NULL;
<a name="l00595"></a>00595         ind_ = NULL;
<a name="l00596"></a>00596         this-&gt;data_ = NULL;
<a name="l00597"></a>00597         <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00598"></a>00598                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;There are more values (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00599"></a>00599                        + <span class="stringliteral">&quot; values) than elements in the matrix (&quot;</span>
<a name="l00600"></a>00600                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;).&quot;</span>);
<a name="l00601"></a>00601       }
<a name="l00602"></a>00602 <span class="preprocessor">#endif</span>
<a name="l00603"></a>00603 <span class="preprocessor"></span>
<a name="l00604"></a>00604 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00605"></a>00605 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00606"></a>00606       {
<a name="l00607"></a>00607 <span class="preprocessor">#endif</span>
<a name="l00608"></a>00608 <span class="preprocessor"></span>
<a name="l00609"></a>00609         ptr_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j)+1,
<a name="l00610"></a>00610                                               <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00611"></a>00611         memcpy(this-&gt;ptr_, A.ptr_,
<a name="l00612"></a>00612                (Storage::GetFirst(i, j) + 1) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00613"></a>00613 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00614"></a>00614 <span class="preprocessor"></span>      }
<a name="l00615"></a>00615     <span class="keywordflow">catch</span> (...)
<a name="l00616"></a>00616       {
<a name="l00617"></a>00617         this-&gt;m_ = 0;
<a name="l00618"></a>00618         this-&gt;n_ = 0;
<a name="l00619"></a>00619         nz_ = 0;
<a name="l00620"></a>00620         ptr_ = NULL;
<a name="l00621"></a>00621         ind_ = NULL;
<a name="l00622"></a>00622         this-&gt;data_ = NULL;
<a name="l00623"></a>00623       }
<a name="l00624"></a>00624     <span class="keywordflow">if</span> (ptr_ == NULL)
<a name="l00625"></a>00625       {
<a name="l00626"></a>00626         this-&gt;m_ = 0;
<a name="l00627"></a>00627         this-&gt;n_ = 0;
<a name="l00628"></a>00628         nz_ = 0;
<a name="l00629"></a>00629         ind_ = NULL;
<a name="l00630"></a>00630         this-&gt;data_ = NULL;
<a name="l00631"></a>00631       }
<a name="l00632"></a>00632     <span class="keywordflow">if</span> (ptr_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00633"></a>00633       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00634"></a>00634                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>)
<a name="l00635"></a>00635                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * (Storage::GetFirst(i, j)+1) )
<a name="l00636"></a>00636                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Storage::GetFirst(i, j)+1)
<a name="l00637"></a>00637                      + <span class="stringliteral">&quot; row or column start indices, for a &quot;</span>
<a name="l00638"></a>00638                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00639"></a>00639 <span class="preprocessor">#endif</span>
<a name="l00640"></a>00640 <span class="preprocessor"></span>
<a name="l00641"></a>00641 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00642"></a>00642 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00643"></a>00643       {
<a name="l00644"></a>00644 <span class="preprocessor">#endif</span>
<a name="l00645"></a>00645 <span class="preprocessor"></span>
<a name="l00646"></a>00646         ind_ = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>( calloc(nz_, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)) );
<a name="l00647"></a>00647         memcpy(this-&gt;ind_, A.ind_, nz_ * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00648"></a>00648 
<a name="l00649"></a>00649 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00650"></a>00650 <span class="preprocessor"></span>      }
<a name="l00651"></a>00651     <span class="keywordflow">catch</span> (...)
<a name="l00652"></a>00652       {
<a name="l00653"></a>00653         this-&gt;m_ = 0;
<a name="l00654"></a>00654         this-&gt;n_ = 0;
<a name="l00655"></a>00655         nz_ = 0;
<a name="l00656"></a>00656         free(ptr_);
<a name="l00657"></a>00657         ptr_ = NULL;
<a name="l00658"></a>00658         ind_ = NULL;
<a name="l00659"></a>00659         this-&gt;data_ = NULL;
<a name="l00660"></a>00660       }
<a name="l00661"></a>00661     <span class="keywordflow">if</span> (ind_ == NULL)
<a name="l00662"></a>00662       {
<a name="l00663"></a>00663         this-&gt;m_ = 0;
<a name="l00664"></a>00664         this-&gt;n_ = 0;
<a name="l00665"></a>00665         nz_ = 0;
<a name="l00666"></a>00666         free(ptr_);
<a name="l00667"></a>00667         ptr_ = NULL;
<a name="l00668"></a>00668         this-&gt;data_ = NULL;
<a name="l00669"></a>00669       }
<a name="l00670"></a>00670     <span class="keywordflow">if</span> (ind_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00671"></a>00671       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00672"></a>00672                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00673"></a>00673                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz)
<a name="l00674"></a>00674                      + <span class="stringliteral">&quot; row or column indices, for a &quot;</span>
<a name="l00675"></a>00675                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00676"></a>00676 <span class="preprocessor">#endif</span>
<a name="l00677"></a>00677 <span class="preprocessor"></span>
<a name="l00678"></a>00678 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00679"></a>00679 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00680"></a>00680       {
<a name="l00681"></a>00681 <span class="preprocessor">#endif</span>
<a name="l00682"></a>00682 <span class="preprocessor"></span>
<a name="l00683"></a>00683         this-&gt;data_ = this-&gt;allocator_.allocate(nz_, <span class="keyword">this</span>);
<a name="l00684"></a>00684         this-&gt;allocator_.memorycpy(this-&gt;data_, A.data_, nz_);
<a name="l00685"></a>00685 
<a name="l00686"></a>00686 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00687"></a>00687 <span class="preprocessor"></span>      }
<a name="l00688"></a>00688     <span class="keywordflow">catch</span> (...)
<a name="l00689"></a>00689       {
<a name="l00690"></a>00690         this-&gt;m_ = 0;
<a name="l00691"></a>00691         this-&gt;n_ = 0;
<a name="l00692"></a>00692         free(ptr_);
<a name="l00693"></a>00693         ptr_ = NULL;
<a name="l00694"></a>00694         free(ind_);
<a name="l00695"></a>00695         ind_ = NULL;
<a name="l00696"></a>00696         this-&gt;data_ = NULL;
<a name="l00697"></a>00697       }
<a name="l00698"></a>00698     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00699"></a>00699       {
<a name="l00700"></a>00700         this-&gt;m_ = 0;
<a name="l00701"></a>00701         this-&gt;n_ = 0;
<a name="l00702"></a>00702         free(ptr_);
<a name="l00703"></a>00703         ptr_ = NULL;
<a name="l00704"></a>00704         free(ind_);
<a name="l00705"></a>00705         ind_ = NULL;
<a name="l00706"></a>00706       }
<a name="l00707"></a>00707     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00708"></a>00708       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Sparse::Matrix_Sparse(int, int, int)&quot;</span>,
<a name="l00709"></a>00709                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(<span class="keywordtype">int</span>) * nz)
<a name="l00710"></a>00710                      + <span class="stringliteral">&quot; bytes to store &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(nz) + <span class="stringliteral">&quot; values, for a &quot;</span>
<a name="l00711"></a>00711                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; by &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l00712"></a>00712 <span class="preprocessor">#endif</span>
<a name="l00713"></a>00713 <span class="preprocessor"></span>
<a name="l00714"></a>00714   }
<a name="l00715"></a>00715 
<a name="l00716"></a>00716 
<a name="l00717"></a>00717   <span class="comment">/*******************</span>
<a name="l00718"></a>00718 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00719"></a>00719 <span class="comment">   *******************/</span>
<a name="l00720"></a>00720 
<a name="l00721"></a>00721 
<a name="l00723"></a>00723 
<a name="l00726"></a>00726   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00727"></a>00727   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a0a0f412562d73bb7c9361da17565e7fb" title="Returns the number of non-zero elements.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetNonZeros</a>()<span class="keyword"> const</span>
<a name="l00728"></a>00728 <span class="keyword">  </span>{
<a name="l00729"></a>00729     <span class="keywordflow">return</span> nz_;
<a name="l00730"></a>00730   }
<a name="l00731"></a>00731 
<a name="l00732"></a>00732 
<a name="l00734"></a>00734 
<a name="l00739"></a>00739   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00740"></a>00740   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00741"></a>00741 <span class="keyword">  </span>{
<a name="l00742"></a>00742     <span class="keywordflow">return</span> nz_;
<a name="l00743"></a>00743   }
<a name="l00744"></a>00744 
<a name="l00745"></a>00745 
<a name="l00747"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342">00747</a> 
<a name="l00751"></a>00751   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00752"></a>00752   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sparse.php#a9126980f0a760e8a73c2d0f480c69342" title="Returns (row or column) start indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetPtr</a>()<span class="keyword"> const</span>
<a name="l00753"></a>00753 <span class="keyword">  </span>{
<a name="l00754"></a>00754     <span class="keywordflow">return</span> ptr_;
<a name="l00755"></a>00755   }
<a name="l00756"></a>00756 
<a name="l00757"></a>00757 
<a name="l00759"></a>00759 
<a name="l00766"></a>00766   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00767"></a>00767   <span class="keywordtype">int</span>* <a class="code" href="class_seldon_1_1_matrix___sparse.php#a1378a5e93b30065896f0d84b07b18caa" title="Returns (row or column) indices of non-zero entries.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetInd</a>()<span class="keyword"> const</span>
<a name="l00768"></a>00768 <span class="keyword">  </span>{
<a name="l00769"></a>00769     <span class="keywordflow">return</span> ind_;
<a name="l00770"></a>00770   }
<a name="l00771"></a>00771 
<a name="l00772"></a>00772 
<a name="l00774"></a>00774 
<a name="l00777"></a>00777   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00778"></a>00778   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a0c3409f0fec69fb357c493154e5d763b" title="Returns the length of the array of start indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetPtrSize</a>()<span class="keyword"> const</span>
<a name="l00779"></a>00779 <span class="keyword">  </span>{
<a name="l00780"></a>00780     <span class="keywordflow">return</span> (Storage::GetFirst(this-&gt;m_, this-&gt;n_) + 1);
<a name="l00781"></a>00781   }
<a name="l00782"></a>00782 
<a name="l00783"></a>00783 
<a name="l00785"></a>00785 
<a name="l00793"></a>00793   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00794"></a>00794   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7b136934dfbe364dd016ac33e4a5dcc6" title="Returns the length of the array of (column or row) indices.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::GetIndSize</a>()<span class="keyword"> const</span>
<a name="l00795"></a>00795 <span class="keyword">  </span>{
<a name="l00796"></a>00796     <span class="keywordflow">return</span> nz_;
<a name="l00797"></a>00797   }
<a name="l00798"></a>00798 
<a name="l00799"></a>00799 
<a name="l00800"></a>00800   <span class="comment">/**********************************</span>
<a name="l00801"></a>00801 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00802"></a>00802 <span class="comment">   **********************************/</span>
<a name="l00803"></a>00803 
<a name="l00804"></a>00804 
<a name="l00806"></a>00806 
<a name="l00812"></a>00812   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00813"></a>00813   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type
<a name="l00814"></a>00814   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a019d20b720889f75c4c2314b40e86954" title="Access operator.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00815"></a>00815 <span class="keyword">  </span>{
<a name="l00816"></a>00816 
<a name="l00817"></a>00817 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00818"></a>00818 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00819"></a>00819       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::operator()&quot;</span>,
<a name="l00820"></a>00820                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00821"></a>00821                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00822"></a>00822     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00823"></a>00823       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::operator()&quot;</span>,
<a name="l00824"></a>00824                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00825"></a>00825                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00826"></a>00826 <span class="preprocessor">#endif</span>
<a name="l00827"></a>00827 <span class="preprocessor"></span>
<a name="l00828"></a>00828     <span class="keywordtype">int</span> k,l;
<a name="l00829"></a>00829     <span class="keywordtype">int</span> a,b;
<a name="l00830"></a>00830 
<a name="l00831"></a>00831     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00832"></a>00832     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00833"></a>00833 
<a name="l00834"></a>00834     <span class="keywordflow">if</span> (a == b)
<a name="l00835"></a>00835       <span class="keywordflow">return</span> T(0);
<a name="l00836"></a>00836 
<a name="l00837"></a>00837     l = Storage::GetSecond(i, j);
<a name="l00838"></a>00838 
<a name="l00839"></a>00839     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l00840"></a>00840 
<a name="l00841"></a>00841     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00842"></a>00842       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00843"></a>00843     <span class="keywordflow">else</span>
<a name="l00844"></a>00844       <span class="keywordflow">return</span> T(0);
<a name="l00845"></a>00845   }
<a name="l00846"></a>00846 
<a name="l00847"></a>00847 
<a name="l00849"></a>00849 
<a name="l00857"></a>00857   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00858"></a>00858   <span class="keyword">inline</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l00859"></a>00859   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00860"></a>00860   {
<a name="l00861"></a>00861 
<a name="l00862"></a>00862 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00863"></a>00863 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00864"></a>00864       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00865"></a>00865                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00866"></a>00866                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00867"></a>00867     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00868"></a>00868       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00869"></a>00869                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00870"></a>00870                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00871"></a>00871 <span class="preprocessor">#endif</span>
<a name="l00872"></a>00872 <span class="preprocessor"></span>
<a name="l00873"></a>00873     <span class="keywordtype">int</span> k, l;
<a name="l00874"></a>00874     <span class="keywordtype">int</span> a, b;
<a name="l00875"></a>00875 
<a name="l00876"></a>00876     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00877"></a>00877     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00878"></a>00878 
<a name="l00879"></a>00879     <span class="keywordflow">if</span> (a == b)
<a name="l00880"></a>00880       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00881"></a>00881                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00882"></a>00882                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00883"></a>00883                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00884"></a>00884 
<a name="l00885"></a>00885     l = Storage::GetSecond(i, j);
<a name="l00886"></a>00886 
<a name="l00887"></a>00887     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l00888"></a>00888 
<a name="l00889"></a>00889     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00890"></a>00890       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00891"></a>00891     <span class="keywordflow">else</span>
<a name="l00892"></a>00892       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00893"></a>00893                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00894"></a>00894                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00895"></a>00895                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00896"></a>00896   }
<a name="l00897"></a>00897 
<a name="l00898"></a>00898 
<a name="l00900"></a>00900 
<a name="l00908"></a>00908   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00909"></a>00909   <span class="keyword">inline</span>
<a name="l00910"></a>00910   <span class="keyword">const</span> <span class="keyword">typename</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::value_type&amp;
<a name="l00911"></a>00911   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a7f416147b6680860cd489dfa6cb08b58" title="Access method.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00912"></a>00912 <span class="keyword">  </span>{
<a name="l00913"></a>00913 
<a name="l00914"></a>00914 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00915"></a>00915 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00916"></a>00916       <span class="keywordflow">throw</span> WrongRow(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00917"></a>00917                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00918"></a>00918                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00919"></a>00919     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00920"></a>00920       <span class="keywordflow">throw</span> WrongCol(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00921"></a>00921                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00922"></a>00922                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00923"></a>00923 <span class="preprocessor">#endif</span>
<a name="l00924"></a>00924 <span class="preprocessor"></span>
<a name="l00925"></a>00925     <span class="keywordtype">int</span> k, l;
<a name="l00926"></a>00926     <span class="keywordtype">int</span> a, b;
<a name="l00927"></a>00927 
<a name="l00928"></a>00928     a = ptr_[Storage::GetFirst(i, j)];
<a name="l00929"></a>00929     b = ptr_[Storage::GetFirst(i, j) + 1];
<a name="l00930"></a>00930 
<a name="l00931"></a>00931     <span class="keywordflow">if</span> (a == b)
<a name="l00932"></a>00932       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00933"></a>00933                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00934"></a>00934                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00935"></a>00935                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00936"></a>00936 
<a name="l00937"></a>00937     l = Storage::GetSecond(i, j);
<a name="l00938"></a>00938 
<a name="l00939"></a>00939     <span class="keywordflow">for</span> (k = a; (k &lt; b-1) &amp;&amp; (ind_[k] &lt; l); k++);
<a name="l00940"></a>00940 
<a name="l00941"></a>00941     <span class="keywordflow">if</span> (ind_[k] == l)
<a name="l00942"></a>00942       <span class="keywordflow">return</span> this-&gt;data_[k];
<a name="l00943"></a>00943     <span class="keywordflow">else</span>
<a name="l00944"></a>00944       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix_Sparse::Val(int, int)&quot;</span>,
<a name="l00945"></a>00945                           <span class="stringliteral">&quot;No reference to element (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;, &quot;</span>
<a name="l00946"></a>00946                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00947"></a>00947                           + <span class="stringliteral">&quot;) can be returned: it is a zero entry.&quot;</span>);
<a name="l00948"></a>00948   }
<a name="l00949"></a>00949 
<a name="l00950"></a>00950 
<a name="l00952"></a>00952 
<a name="l00958"></a>00958   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00959"></a>00959   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0" title="Add a value to a non-zero entry.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00960"></a>00960 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a6ede24fc389f58952986463da449bfb0" title="Add a value to a non-zero entry.">  ::AddInteraction</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; val)
<a name="l00961"></a>00961   {
<a name="l00962"></a>00962     Val(i, j) += val;
<a name="l00963"></a>00963   }
<a name="l00964"></a>00964 
<a name="l00965"></a>00965 
<a name="l00967"></a>00967 
<a name="l00972"></a>00972   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00973"></a>00973   <span class="keyword">inline</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;&amp;
<a name="l00974"></a>00974   <a class="code" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f" title="Duplicates a matrix (assignment operator).">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00975"></a>00975 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;&amp; A)
<a name="l00976"></a>00976   {
<a name="l00977"></a>00977     this-&gt;Copy(A);
<a name="l00978"></a>00978 
<a name="l00979"></a>00979     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00980"></a>00980   }
<a name="l00981"></a>00981 
<a name="l00982"></a>00982 
<a name="l00983"></a>00983   <span class="comment">/************************</span>
<a name="l00984"></a>00984 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00985"></a>00985 <span class="comment">   ************************/</span>
<a name="l00986"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a">00986</a> 
<a name="l00987"></a>00987 
<a name="l00989"></a>00989 
<a name="l00990"></a>00990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00991"></a>00991   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a812985d3c66421787a6dbe687620ea1a" title="Resets all non-zero entries to 0-value.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00992"></a>00992   {
<a name="l00993"></a>00993     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00994"></a>00994                                this-&gt;nz_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00995"></a>00995   }
<a name="l00996"></a>00996 
<a name="l00997"></a>00997 
<a name="l00999"></a>00999 
<a name="l01002"></a>01002   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01003"></a>01003   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a657c33784aee4b940c6a635c33f2eb5e" title="Sets the matrix to identity.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l01004"></a>01004   {
<a name="l01005"></a>01005     <span class="keywordtype">int</span> m = this-&gt;m_;
<a name="l01006"></a>01006     <span class="keywordtype">int</span> n = this-&gt;n_;
<a name="l01007"></a>01007     <span class="keywordtype">int</span> nz = min(m, n);
<a name="l01008"></a>01008 
<a name="l01009"></a>01009     <span class="keywordflow">if</span> (nz == 0)
<a name="l01010"></a>01010       <span class="keywordflow">return</span>;
<a name="l01011"></a>01011 
<a name="l01012"></a>01012     <a class="code" href="class_seldon_1_1_matrix___sparse.php#ab63b8d9aee1a38054cd525d7fa708f1a" title="Clears the matrix.">Clear</a>();
<a name="l01013"></a>01013 
<a name="l01014"></a>01014     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> values(nz);
<a name="l01015"></a>01015     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ptr(Storage::GetFirst(m, n) + 1);
<a name="l01016"></a>01016     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ind(nz);
<a name="l01017"></a>01017 
<a name="l01018"></a>01018     values.Fill(T(1));
<a name="l01019"></a>01019     ind.Fill();
<a name="l01020"></a>01020     <span class="keywordtype">int</span> i;
<a name="l01021"></a>01021     <span class="keywordflow">for</span> (i = 0; i &lt; nz + 1; i++)
<a name="l01022"></a>01022       ptr(i) = i;
<a name="l01023"></a>01023     <span class="keywordflow">for</span> (i = nz + 1; i &lt; ptr.GetLength(); i++)
<a name="l01024"></a>01024       ptr(i) = nz;
<a name="l01025"></a>01025 
<a name="l01026"></a>01026     <a class="code" href="class_seldon_1_1_matrix___sparse.php#a5b411ce16d320a1442588af33adefe09" title="Redefines the matrix.">SetData</a>(m, n, values, ptr, ind);
<a name="l01027"></a>01027   }
<a name="l01028"></a>01028 
<a name="l01029"></a>01029 
<a name="l01031"></a>01031 
<a name="l01034"></a>01034   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01035"></a>01035   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l01036"></a>01036   {
<a name="l01037"></a>01037     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01038"></a>01038       this-&gt;data_[i] = i;
<a name="l01039"></a>01039   }
<a name="l01040"></a>01040 
<a name="l01041"></a>01041 
<a name="l01043"></a><a class="code" href="class_seldon_1_1_matrix___sparse.php#aa2721edc471c0603919afa9365bee419">01043</a> 
<a name="l01046"></a>01046   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01047"></a>01047   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01048"></a>01048   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#abe41180630623f201ea254e009931abd" title="Fills the non-zero entries with 0, 1, 2, ...">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01049"></a>01049   {
<a name="l01050"></a>01050     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01051"></a>01051       this-&gt;data_[i] = x;
<a name="l01052"></a>01052   }
<a name="l01053"></a>01053 
<a name="l01054"></a>01054 
<a name="l01056"></a>01056 
<a name="l01059"></a>01059   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01060"></a>01060   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f" title="Fills the non-zero entries randomly.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l01061"></a>01061   {
<a name="l01062"></a>01062     srand(time(NULL));
<a name="l01063"></a>01063     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#aac6b06ab623602d24a80d12adf495cf2" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l01064"></a>01064       this-&gt;data_[i] = rand();
<a name="l01065"></a>01065   }
<a name="l01066"></a>01066 
<a name="l01067"></a>01067 
<a name="l01069"></a>01069 
<a name="l01074"></a>01074   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01075"></a>01075   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a8d6d48b6f0277f1df2d52b798975407e" title="Displays the matrix on the standard output.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l01076"></a>01076 <span class="keyword">  </span>{
<a name="l01077"></a>01077     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l01078"></a>01078       {
<a name="l01079"></a>01079         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l01080"></a>01080           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l01081"></a>01081         cout &lt;&lt; endl;
<a name="l01082"></a>01082       }
<a name="l01083"></a>01083   }
<a name="l01084"></a>01084 
<a name="l01085"></a>01085 
<a name="l01087"></a>01087 
<a name="l01093"></a>01093   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01094"></a>01094   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01095"></a>01095 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">  WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l01096"></a>01096 <span class="keyword">  </span>{
<a name="l01097"></a>01097     ofstream FileStream; FileStream.precision(14);
<a name="l01098"></a>01098     FileStream.open(FileName.c_str());
<a name="l01099"></a>01099 
<a name="l01100"></a>01100 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01101"></a>01101 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01102"></a>01102     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01103"></a>01103       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(string FileName)&quot;</span>,
<a name="l01104"></a>01104                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01105"></a>01105 <span class="preprocessor">#endif</span>
<a name="l01106"></a>01106 <span class="preprocessor"></span>
<a name="l01107"></a>01107     this-&gt;<a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">WriteText</a>(FileStream);
<a name="l01108"></a>01108 
<a name="l01109"></a>01109     FileStream.close();
<a name="l01110"></a>01110   }
<a name="l01111"></a>01111 
<a name="l01112"></a>01112 
<a name="l01114"></a>01114 
<a name="l01120"></a>01120   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01121"></a>01121   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">Matrix_Sparse&lt;T, Prop, Storage, Allocator&gt;::</a>
<a name="l01122"></a>01122 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a45ac6acb158f9b49af3d50a2da34b708" title="Writes the matrix in a file.">  WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l01123"></a>01123 <span class="keyword">  </span>{
<a name="l01124"></a>01124 
<a name="l01125"></a>01125 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01126"></a>01126 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01127"></a>01127     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01128"></a>01128       <span class="keywordflow">throw</span> IOError(<span class="stringliteral">&quot;Matrix_ArraySparse::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l01129"></a>01129                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01130"></a>01130 <span class="preprocessor">#endif</span>
<a name="l01131"></a>01131 <span class="preprocessor"></span>
<a name="l01132"></a>01132     <span class="comment">// conversion in coordinate format (1-index convention)</span>
<a name="l01133"></a>01133     IVect IndRow, IndCol; Vector&lt;T&gt; Value;
<a name="l01134"></a>01134     <span class="keyword">const</span> Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; leaf_class =
<a name="l01135"></a>01135       <span class="keyword">static_cast&lt;</span><span class="keyword">const </span>Matrix&lt;T, Prop, Storage, Allocator&gt;&amp; <span class="keyword">&gt;</span>(*this);
<a name="l01136"></a>01136 
<a name="l01137"></a>01137     <a class="code" href="namespace_seldon.php#ac9eb5523f90447b8a1faaa656d56e9d2" title="Conversion from RowSparse to coordinate format.">ConvertMatrix_to_Coordinates</a>(leaf_class, IndRow, IndCol,
<a name="l01138"></a>01138                                  Value, 1, <span class="keyword">true</span>);
<a name="l01139"></a>01139 
<a name="l01140"></a>01140     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; IndRow.GetM(); i++)
<a name="l01141"></a>01141       FileStream &lt;&lt; IndRow(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; IndCol(i) &lt;&lt; <span class="stringliteral">&quot; &quot;</span> &lt;&lt; Value(i) &lt;&lt; <span class="charliteral">&#39;\n&#39;</span>;
<a name="l01142"></a>01142 
<a name="l01143"></a>01143   }
<a name="l01144"></a>01144 
<a name="l01145"></a>01145 
<a name="l01147"></a>01147   <span class="comment">// MATRIX&lt;COLSPARSE&gt; //</span>
<a name="l01149"></a>01149 <span class="comment"></span>
<a name="l01150"></a>01150 
<a name="l01151"></a>01151   <span class="comment">/****************</span>
<a name="l01152"></a>01152 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01153"></a>01153 <span class="comment">   ****************/</span>
<a name="l01154"></a>01154 
<a name="l01156"></a>01156 
<a name="l01159"></a>01159   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01160"></a>01160   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a02ccefd04db099e355bf432daf2b8d0b" title="Default constructor.">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01161"></a>01161     Matrix_Sparse&lt;T, Prop, ColSparse, Allocator&gt;()
<a name="l01162"></a>01162   {
<a name="l01163"></a>01163   }
<a name="l01164"></a>01164 
<a name="l01165"></a>01165 
<a name="l01167"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#a6b27ce89da3078a901e6b3123c78b9a0">01167</a> 
<a name="l01171"></a>01171   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01172"></a>01172   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01173"></a>01173     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, 0)
<a name="l01174"></a>01174   {
<a name="l01175"></a>01175   }
<a name="l01176"></a>01176 
<a name="l01177"></a>01177 
<a name="l01179"></a>01179 
<a name="l01185"></a>01185   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01186"></a>01186   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01187"></a>01187     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, nz)
<a name="l01188"></a>01188   {
<a name="l01189"></a>01189   }
<a name="l01190"></a>01190 
<a name="l01191"></a>01191 
<a name="l01193"></a>01193 
<a name="l01204"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sparse_00_01_allocator_01_4.php#aeb00bb87b12cfa5a7982da11100ef81f">01204</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01205"></a>01205   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01206"></a>01206             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01207"></a>01207             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01208"></a>01208   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColSparse, Allocator&gt;::</a>
<a name="l01209"></a>01209 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01210"></a>01210          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01211"></a>01211          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01212"></a>01212          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01213"></a>01213     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_sparse.php">ColSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01214"></a>01214   {
<a name="l01215"></a>01215   }
<a name="l01216"></a>01216 
<a name="l01217"></a>01217 
<a name="l01218"></a>01218 
<a name="l01220"></a>01220   <span class="comment">// MATRIX&lt;ROWSPARSE&gt; //</span>
<a name="l01222"></a>01222 <span class="comment"></span>
<a name="l01223"></a>01223 
<a name="l01224"></a>01224   <span class="comment">/****************</span>
<a name="l01225"></a>01225 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01226"></a>01226 <span class="comment">   ****************/</span>
<a name="l01227"></a>01227 
<a name="l01229"></a>01229 
<a name="l01232"></a>01232   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01233"></a>01233   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>()  throw():
<a name="l01234"></a>01234     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;()
<a name="l01235"></a>01235   {
<a name="l01236"></a>01236   }
<a name="l01237"></a>01237 
<a name="l01238"></a>01238 
<a name="l01240"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#a710d1a0338e3a1f80b9d4d75664abe38">01240</a> 
<a name="l01244"></a>01244   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01245"></a>01245   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01246"></a>01246     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, 0)
<a name="l01247"></a>01247   {
<a name="l01248"></a>01248   }
<a name="l01249"></a>01249 
<a name="l01250"></a>01250 
<a name="l01252"></a>01252 
<a name="l01258"></a>01258   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01259"></a>01259   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> nz):
<a name="l01260"></a>01260     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, nz)
<a name="l01261"></a>01261   {
<a name="l01262"></a>01262   }
<a name="l01263"></a>01263 
<a name="l01264"></a>01264 
<a name="l01266"></a>01266 
<a name="l01277"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sparse_00_01_allocator_01_4.php#a4d328f6bb80941304ae3f1948155ff6e">01277</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01278"></a>01278   <span class="keyword">template</span> &lt;<span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01279"></a>01279             <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01280"></a>01280             <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01281"></a>01281   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::</a>
<a name="l01282"></a>01282 <a class="code" href="class_seldon_1_1_matrix.php">  Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l01283"></a>01283          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Storage0, Allocator0&gt;</a>&amp; values,
<a name="l01284"></a>01284          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage1, Allocator1&gt;</a>&amp; ptr,
<a name="l01285"></a>01285          <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, Storage2, Allocator2&gt;</a>&amp; ind):
<a name="l01286"></a>01286     <a class="code" href="class_seldon_1_1_matrix___sparse.php" title="Sparse-matrix class.">Matrix_Sparse</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator&gt;(i, j, values, ptr, ind)
<a name="l01287"></a>01287   {
<a name="l01288"></a>01288   }
<a name="l01289"></a>01289 
<a name="l01290"></a>01290 
<a name="l01292"></a>01292 
<a name="l01300"></a>01300   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01301"></a>01301   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowSparse, Allocator&gt;::FillRand</a>(<span class="keywordtype">int</span> Nelement)
<a name="l01302"></a>01302   {
<a name="l01303"></a>01303     <span class="keywordflow">if</span> (this-&gt;m_ == 0 || this-&gt;n_ == 0)
<a name="l01304"></a>01304       <span class="keywordflow">return</span>;
<a name="l01305"></a>01305 
<a name="l01306"></a>01306     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a> i(Nelement), j(Nelement);
<a name="l01307"></a>01307     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> value(Nelement);
<a name="l01308"></a>01308 
<a name="l01309"></a>01309     set&lt;pair&lt;int, int&gt; &gt; skeleton;
<a name="l01310"></a>01310     set&lt;pair&lt;int, int&gt; &gt;::iterator it;
<a name="l01311"></a>01311 
<a name="l01312"></a>01312     srand(time(NULL));
<a name="l01313"></a>01313 
<a name="l01314"></a>01314     <span class="keywordflow">while</span> (static_cast&lt;int&gt;(skeleton.size()) != Nelement)
<a name="l01315"></a>01315       skeleton.insert(make_pair(rand() % this-&gt;m_, rand() % this-&gt;n_));
<a name="l01316"></a>01316 
<a name="l01317"></a>01317     <span class="keywordtype">int</span> l = 0;
<a name="l01318"></a>01318     <span class="keywordflow">for</span> (it = skeleton.begin(); it != skeleton.end(); it++)
<a name="l01319"></a>01319       {
<a name="l01320"></a>01320         i(l) = it-&gt;first;
<a name="l01321"></a>01321         j(l) = it-&gt;second;
<a name="l01322"></a>01322         value(l) = double(rand());
<a name="l01323"></a>01323         l++;
<a name="l01324"></a>01324       }
<a name="l01325"></a>01325 
<a name="l01326"></a>01326     <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(i, j, value, *<span class="keyword">this</span>);
<a name="l01327"></a>01327   }
<a name="l01328"></a>01328 
<a name="l01329"></a>01329 
<a name="l01331"></a>01331 
<a name="l01341"></a>01341   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01342"></a>01342   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f">Matrix&lt;T, Prop, RowSparse, Allocator&gt;</a>
<a name="l01343"></a>01343 <a class="code" href="class_seldon_1_1_matrix___sparse.php#a27b33a9915ce2922d2f3f0a0270b467f">  ::FillRand</a>(<span class="keywordtype">int</span> Nelement, <span class="keyword">const</span> T&amp; x)
<a name="l01344"></a>01344   {
<a name="l01345"></a>01345     <span class="keywordflow">if</span> (this-&gt;m_ == 0 || this-&gt;n_ == 0)
<a name="l01346"></a>01346       <span class="keywordflow">return</span>;
<a name="l01347"></a>01347 
<a name="l01348"></a>01348     Vector&lt;int&gt; i(Nelement), j(Nelement);
<a name="l01349"></a>01349     Vector&lt;T&gt; value(Nelement);
<a name="l01350"></a>01350     value.Fill(x);
<a name="l01351"></a>01351 
<a name="l01352"></a>01352     srand(time(NULL));
<a name="l01353"></a>01353     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; Nelement; l++)
<a name="l01354"></a>01354       {
<a name="l01355"></a>01355         i(l) = rand() % this-&gt;m_;
<a name="l01356"></a>01356         j(l) = rand() % this-&gt;n_;
<a name="l01357"></a>01357       }
<a name="l01358"></a>01358 
<a name="l01359"></a>01359     <a class="code" href="namespace_seldon.php#ad4977fcff081edc62cc482b1aedaf0d9" title="Conversion from coordinate format to RowSparse.">ConvertMatrix_from_Coordinates</a>(i, j, value, *<span class="keyword">this</span>);
<a name="l01360"></a>01360   }
<a name="l01361"></a>01361 
<a name="l01362"></a>01362 
<a name="l01363"></a>01363 } <span class="comment">// namespace Seldon.</span>
<a name="l01364"></a>01364 
<a name="l01365"></a>01365 <span class="preprocessor">#define SELDON_FILE_MATRIX_SPARSE_CXX</span>
<a name="l01366"></a>01366 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
