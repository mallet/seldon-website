<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector3.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_VECTOR_3_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#include &quot;Vector3.hxx&quot;</span>
<a name="l00025"></a>00025 
<a name="l00026"></a>00026 
<a name="l00027"></a>00027 <span class="keyword">namespace </span>Seldon
<a name="l00028"></a>00028 {
<a name="l00029"></a>00029 
<a name="l00030"></a>00030 
<a name="l00032"></a>00032   <span class="comment">// VECTOR3 //</span>
<a name="l00034"></a>00034 <span class="comment"></span>
<a name="l00035"></a>00035 
<a name="l00036"></a>00036   <span class="comment">/***************</span>
<a name="l00037"></a>00037 <span class="comment">   * CONSTRUCTOR *</span>
<a name="l00038"></a>00038 <span class="comment">   ***************/</span>
<a name="l00039"></a>00039 
<a name="l00040"></a>00040 
<a name="l00042"></a>00042 
<a name="l00045"></a>00045   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00046"></a>00046   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Vector3</a>()
<a name="l00047"></a>00047   {
<a name="l00048"></a>00048   }
<a name="l00049"></a>00049 
<a name="l00050"></a>00050 
<a name="l00052"></a>00052 
<a name="l00056"></a>00056   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00057"></a>00057   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Vector3</a>(<span class="keywordtype">int</span> length)
<a name="l00058"></a>00058   {
<a name="l00059"></a>00059     data_.Reallocate(length);
<a name="l00060"></a>00060   }
<a name="l00061"></a>00061 
<a name="l00062"></a>00062 
<a name="l00064"></a>00064 
<a name="l00070"></a>00070   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00071"></a>00071   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00072"></a>00072 <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">  ::Vector3</a>(Vector&lt;int&gt;&amp; length)
<a name="l00073"></a>00073   {
<a name="l00074"></a>00074     data_.Clear();
<a name="l00075"></a>00075     <span class="keywordtype">int</span> n, m = length.GetSize();
<a name="l00076"></a>00076     data_.Reallocate(m);
<a name="l00077"></a>00077     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00078"></a>00078       {
<a name="l00079"></a>00079         n = length(i);
<a name="l00080"></a>00080         data_(i).Reallocate(n);
<a name="l00081"></a>00081       }
<a name="l00082"></a>00082   }
<a name="l00083"></a>00083 
<a name="l00084"></a>00084 
<a name="l00086"></a>00086 
<a name="l00092"></a><a class="code" href="class_seldon_1_1_vector3.php#a9fff1c8ce72eb5dc5c002b6f1c0e7b86">00092</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00093"></a>00093   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator&gt;
<a name="l00094"></a>00094   <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00095"></a>00095 <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">  ::Vector3</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>, Vect_Full, Allocator&gt;&amp; length)
<a name="l00096"></a>00096   {
<a name="l00097"></a>00097     data_.Clear();
<a name="l00098"></a>00098     <span class="keywordtype">int</span> n, m = length.GetSize();
<a name="l00099"></a>00099     data_.Reallocate(m);
<a name="l00100"></a>00100     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l00101"></a>00101       {
<a name="l00102"></a>00102         n = length(i).GetSize();
<a name="l00103"></a>00103         data_(i).Reallocate(n);
<a name="l00104"></a>00104         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l00105"></a>00105           data_(i)(j).Reallocate(length(i)(j));
<a name="l00106"></a>00106       }
<a name="l00107"></a>00107   }
<a name="l00108"></a>00108 
<a name="l00109"></a>00109 
<a name="l00110"></a>00110   <span class="comment">/**************</span>
<a name="l00111"></a>00111 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00112"></a>00112 <span class="comment">   **************/</span>
<a name="l00113"></a>00113 
<a name="l00114"></a>00114 
<a name="l00116"></a>00116 
<a name="l00119"></a>00119   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00120"></a>00120   <a class="code" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80" title="Destructor.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::~Vector3</a>()
<a name="l00121"></a>00121   {
<a name="l00122"></a>00122   }
<a name="l00123"></a>00123 
<a name="l00124"></a>00124 
<a name="l00125"></a>00125   <span class="comment">/**********************</span>
<a name="l00126"></a>00126 <span class="comment">   * VECTORS MANAGEMENT *</span>
<a name="l00127"></a>00127 <span class="comment">   **********************/</span>
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 
<a name="l00131"></a>00131 
<a name="l00134"></a>00134   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00135"></a>00135   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetLength</a>()<span class="keyword"> const</span>
<a name="l00136"></a>00136 <span class="keyword">  </span>{
<a name="l00137"></a>00137     <span class="keywordflow">return</span> data_.GetLength();
<a name="l00138"></a>00138   }
<a name="l00139"></a>00139 
<a name="l00140"></a>00140 
<a name="l00142"></a>00142 
<a name="l00145"></a>00145   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00146"></a>00146   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetSize</a>()<span class="keyword"> const</span>
<a name="l00147"></a>00147 <span class="keyword">  </span>{
<a name="l00148"></a>00148     <span class="keywordflow">return</span> data_.GetSize();
<a name="l00149"></a>00149   }
<a name="l00150"></a>00150 
<a name="l00151"></a>00151 
<a name="l00153"></a>00153 
<a name="l00157"></a>00157   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00158"></a>00158   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetLength</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00159"></a>00159 <span class="keyword">  </span>{
<a name="l00160"></a>00160     <span class="keywordflow">return</span> data_(i).GetLength();
<a name="l00161"></a>00161   }
<a name="l00162"></a>00162 
<a name="l00163"></a>00163 
<a name="l00165"></a>00165 
<a name="l00169"></a>00169   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00170"></a>00170   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetSize</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00171"></a>00171 <span class="keyword">  </span>{
<a name="l00172"></a>00172     <span class="keywordflow">return</span> data_(i).GetSize();
<a name="l00173"></a>00173   }
<a name="l00174"></a>00174 
<a name="l00175"></a>00175 
<a name="l00177"></a>00177 
<a name="l00182"></a>00182   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00183"></a>00183   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00184"></a>00184 <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">  ::GetLength</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00185"></a>00185 <span class="keyword">  </span>{
<a name="l00186"></a>00186     <span class="keywordflow">return</span> data_(i)(j).GetLength();
<a name="l00187"></a>00187   }
<a name="l00188"></a>00188 
<a name="l00189"></a>00189 
<a name="l00191"></a>00191 
<a name="l00196"></a>00196   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00197"></a>00197   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00198"></a>00198 <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">  ::GetSize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00199"></a>00199 <span class="keyword">  </span>{
<a name="l00200"></a>00200     <span class="keywordflow">return</span> data_(i)(j).GetSize();
<a name="l00201"></a>00201   }
<a name="l00202"></a>00202 
<a name="l00203"></a>00203 
<a name="l00205"></a>00205 
<a name="l00208"></a>00208   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00209"></a>00209   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetNelement</a>()<span class="keyword"> const</span>
<a name="l00210"></a>00210 <span class="keyword">  </span>{
<a name="l00211"></a>00211     <span class="keywordtype">int</span> total = 0;
<a name="l00212"></a>00212     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(); i++)
<a name="l00213"></a>00213       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(i); j++ )
<a name="l00214"></a>00214         total += <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(i, j);
<a name="l00215"></a>00215     <span class="keywordflow">return</span> total;
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00220"></a>00220 
<a name="l00227"></a>00227   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00228"></a>00228   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00229"></a>00229 <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">  ::GetNelement</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end)<span class="keyword"> const</span>
<a name="l00230"></a>00230 <span class="keyword">  </span>{
<a name="l00231"></a>00231     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00232"></a>00232       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3::GetNelement(int beg, int end)&quot;</span>,
<a name="l00233"></a>00233                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00234"></a>00234                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00235"></a>00235                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00236"></a>00236     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; GetLength())
<a name="l00237"></a>00237       <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Vector3::GetNelement(int beg, int end)&quot;</span>,
<a name="l00238"></a>00238                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00239"></a>00239                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00240"></a>00240                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00241"></a>00241                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00242"></a>00242 
<a name="l00243"></a>00243     <span class="keywordtype">int</span> total = 0;
<a name="l00244"></a>00244     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = beg; i &lt; end; i++)
<a name="l00245"></a>00245       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; GetLength(i); j++ )
<a name="l00246"></a>00246         total += GetLength(i, j);
<a name="l00247"></a>00247     <span class="keywordflow">return</span> total;
<a name="l00248"></a>00248   }
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 
<a name="l00252"></a>00252 
<a name="l00255"></a>00255   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00256"></a>00256   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Reallocate</a>(<span class="keywordtype">int</span> N)
<a name="l00257"></a>00257   {
<a name="l00258"></a>00258     data_.Reallocate(N);
<a name="l00259"></a>00259   }
<a name="l00260"></a>00260 
<a name="l00261"></a>00261 
<a name="l00263"></a>00263 
<a name="l00267"></a>00267   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00268"></a>00268   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00269"></a>00269 <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> N)
<a name="l00270"></a>00270   {
<a name="l00271"></a>00271     data_(i).Reallocate(N);
<a name="l00272"></a>00272   }
<a name="l00273"></a>00273 
<a name="l00274"></a>00274 
<a name="l00276"></a>00276 
<a name="l00281"></a>00281   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00282"></a>00282   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00283"></a>00283 <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> N)
<a name="l00284"></a>00284   {
<a name="l00285"></a>00285     data_(i)(j).Reallocate(N);
<a name="l00286"></a>00286   }
<a name="l00287"></a>00287 
<a name="l00288"></a>00288 
<a name="l00290"></a>00290 
<a name="l00294"></a><a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e">00294</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00295"></a>00295   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00296"></a>00296   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00297"></a>00297 <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">  ::Flatten</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00298"></a>00298 <span class="keyword">  </span>{
<a name="l00299"></a>00299     data.Reallocate(GetNelement());
<a name="l00300"></a>00300     <span class="keywordtype">int</span> i, j, k, n(0);
<a name="l00301"></a>00301     <span class="keywordflow">for</span> (i = 0; i &lt; GetLength(); i++)
<a name="l00302"></a>00302       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00303"></a>00303         <span class="keywordflow">for</span> (k = 0; k &lt; GetLength(i, j); k++)
<a name="l00304"></a>00304           data(n++) = data_(i)(j)(k);
<a name="l00305"></a>00305   }
<a name="l00306"></a>00306 
<a name="l00307"></a>00307 
<a name="l00309"></a>00309 
<a name="l00317"></a><a class="code" href="class_seldon_1_1_vector3.php#a84d542b134e8f98e944932b154c1d48d">00317</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00318"></a>00318   <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00319"></a>00319   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00320"></a>00320 <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">  ::Flatten</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)<span class="keyword"> const</span>
<a name="l00321"></a>00321 <span class="keyword">  </span>{
<a name="l00322"></a>00322     <span class="keywordflow">if</span> (beg &gt; end)
<a name="l00323"></a>00323       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00324"></a>00324                           <span class="stringliteral">&quot;The lower bound of the range of inner vectors &quot;</span>
<a name="l00325"></a>00325                           <span class="stringliteral">&quot;of vectors, [&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg) + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end)
<a name="l00326"></a>00326                           + <span class="stringliteral">&quot;[, is strictly greater than its upper bound.&quot;</span>);
<a name="l00327"></a>00327     <span class="keywordflow">if</span> (beg &lt; 0 || end &gt; GetLength())
<a name="l00328"></a>00328       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Vector3:::Flatten(int beg, int end, Vector&amp; data)&quot;</span>,
<a name="l00329"></a>00329                           <span class="stringliteral">&quot;The inner-vector of vectors indexes should be in &quot;</span>
<a name="l00330"></a>00330                           <span class="stringliteral">&quot;[0,&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(GetLength()) + <span class="stringliteral">&quot;] but [&quot;</span>
<a name="l00331"></a>00331                           + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(beg)
<a name="l00332"></a>00332                           + <span class="stringliteral">&quot;, &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(end) + <span class="stringliteral">&quot;[ was provided.&quot;</span>);
<a name="l00333"></a>00333 
<a name="l00334"></a>00334     data.Reallocate(GetNelement());
<a name="l00335"></a>00335     <span class="keywordtype">int</span> i, j, k, n(0);
<a name="l00336"></a>00336     <span class="keywordflow">for</span> (i = beg; i &lt; end; i++)
<a name="l00337"></a>00337       <span class="keywordflow">for</span> (j = 0; j &lt; GetLength(i); j++)
<a name="l00338"></a>00338         <span class="keywordflow">for</span> (k = 0; k &lt; GetLength(i, j); k++)
<a name="l00339"></a>00339           data(n++) = data_(i)(j)(k);
<a name="l00340"></a>00340   }
<a name="l00341"></a>00341 
<a name="l00342"></a>00342 
<a name="l00344"></a>00344 
<a name="l00349"></a>00349   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00350"></a>00350   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00351"></a>00351                Allocator1, Allocator2&gt;::PushBack(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x)
<a name="l00352"></a>00352   {
<a name="l00353"></a>00353     data_(i)(j).PushBack(x);
<a name="l00354"></a>00354   }
<a name="l00355"></a>00355 
<a name="l00356"></a>00356 
<a name="l00358"></a>00358 
<a name="l00362"></a>00362   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00363"></a>00363   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::</a>
<a name="l00364"></a>00364 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  PushBack</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> Vector&lt;T, Vect_Full, Allocator0&gt;&amp; X)
<a name="l00365"></a>00365   {
<a name="l00366"></a>00366     data_(i).PushBack(X);
<a name="l00367"></a>00367   }
<a name="l00368"></a>00368 
<a name="l00369"></a>00369 
<a name="l00371"></a>00371 
<a name="l00374"></a>00374   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00375"></a>00375   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00376"></a>00376 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,
<a name="l00377"></a>00377              Vect_Full, Allocator1&gt;&amp; X)
<a name="l00378"></a>00378   {
<a name="l00379"></a>00379     data_.PushBack(X);
<a name="l00380"></a>00380   }
<a name="l00381"></a>00381 
<a name="l00382"></a>00382 
<a name="l00384"></a>00384 
<a name="l00388"></a>00388   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00389"></a>00389   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00390"></a>00390 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,
<a name="l00391"></a>00391              Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp; X)
<a name="l00392"></a>00392   {
<a name="l00393"></a>00393     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetLength(); i++)
<a name="l00394"></a>00394       data_.PushBack(X(i));
<a name="l00395"></a>00395   }
<a name="l00396"></a>00396 
<a name="l00397"></a>00397 
<a name="l00399"></a>00399 
<a name="l00403"></a>00403   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00404"></a>00404   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>
<a name="l00405"></a>00405 <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">  ::PushBack</a>(<span class="keyword">const</span> Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;&amp; X)
<a name="l00406"></a>00406   {
<a name="l00407"></a>00407     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; X.GetLength(); i++)
<a name="l00408"></a>00408       data_.PushBack(X.GetVector());
<a name="l00409"></a>00409   }
<a name="l00410"></a>00410 
<a name="l00411"></a><a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604">00411</a> 
<a name="l00413"></a>00413   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00414"></a>00414   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Clear</a>()
<a name="l00415"></a>00415   {
<a name="l00416"></a>00416     data_.Clear();
<a name="l00417"></a>00417   }
<a name="l00418"></a>00418 
<a name="l00419"></a>00419 
<a name="l00421"></a>00421 
<a name="l00424"></a>00424   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00425"></a>00425   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00426"></a>00426                Allocator1, Allocator2&gt;::Clear(<span class="keywordtype">int</span> i)
<a name="l00427"></a>00427   {
<a name="l00428"></a>00428     data_(i).Clear();
<a name="l00429"></a>00429   }
<a name="l00430"></a>00430 
<a name="l00431"></a>00431 
<a name="l00433"></a>00433 
<a name="l00437"></a>00437   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00438"></a>00438   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Clear</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00439"></a>00439   {
<a name="l00440"></a>00440     data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Clear</a>();
<a name="l00441"></a>00441   }
<a name="l00442"></a>00442 
<a name="l00443"></a>00443 
<a name="l00445"></a>00445 
<a name="l00448"></a>00448   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00449"></a>00449   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f" title="Fills the vector with a given value.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Fill</a>(<span class="keyword">const</span> T&amp; x)
<a name="l00450"></a>00450   {
<a name="l00451"></a>00451     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetSize(); i++)
<a name="l00452"></a>00452       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; data_(i).GetSize(); j++)
<a name="l00453"></a>00453         data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f" title="Fills the vector with a given value.">Fill</a>(x);
<a name="l00454"></a>00454   }
<a name="l00455"></a>00455 
<a name="l00456"></a>00456 
<a name="l00458"></a>00458 
<a name="l00461"></a><a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17">00461</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00462"></a>00462   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, Vect_Full, Allocator1&gt;,
<a name="l00463"></a>00463          Vect_Full, Allocator2&gt;&amp;
<a name="l00464"></a>00464   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>()
<a name="l00465"></a>00465   {
<a name="l00466"></a>00466     <span class="keywordflow">return</span> data_;
<a name="l00467"></a>00467   }
<a name="l00468"></a>00468 
<a name="l00469"></a>00469 
<a name="l00471"></a>00471 
<a name="l00474"></a><a class="code" href="class_seldon_1_1_vector3.php#acc38f6e8f4660dc1c98999b08a467454">00474</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00475"></a>00475   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>,
<a name="l00476"></a>00476                       Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp;
<a name="l00477"></a>00477   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>()<span class="keyword"> const</span>
<a name="l00478"></a>00478 <span class="keyword">  </span>{
<a name="l00479"></a>00479     <span class="keywordflow">return</span> data_;
<a name="l00480"></a>00480   }
<a name="l00481"></a>00481 
<a name="l00482"></a>00482 
<a name="l00484"></a>00484 
<a name="l00488"></a>00488   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00489"></a>00489   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00490"></a>00490   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i)
<a name="l00491"></a>00491   {
<a name="l00492"></a>00492     <span class="keywordflow">return</span> data_(i);
<a name="l00493"></a>00493   }
<a name="l00494"></a>00494 
<a name="l00495"></a>00495 
<a name="l00497"></a>00497 
<a name="l00501"></a>00501   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00502"></a>00502   <span class="keyword">const</span> Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;, VectFull, Allocator1&gt;&amp;
<a name="l00503"></a>00503   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00504"></a>00504 <span class="keyword">  </span>{
<a name="l00505"></a>00505     <span class="keywordflow">return</span> data_(i);
<a name="l00506"></a>00506   }
<a name="l00507"></a>00507 
<a name="l00508"></a>00508 
<a name="l00510"></a>00510 
<a name="l00515"></a>00515   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00516"></a>00516   Vector&lt;T, Vect_Full, Allocator0&gt;&amp;
<a name="l00517"></a>00517   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00518"></a>00518   {
<a name="l00519"></a>00519     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00520"></a>00520   }
<a name="l00521"></a>00521 
<a name="l00522"></a>00522 
<a name="l00524"></a>00524 
<a name="l00529"></a>00529   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00530"></a>00530   <span class="keyword">const</span> Vector&lt;T, Vect_Full, Allocator0&gt;&amp;
<a name="l00531"></a>00531   <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"></span>
<a name="l00532"></a>00532 <span class="keyword">    const</span>
<a name="l00533"></a>00533 <span class="keyword">  </span>{
<a name="l00534"></a>00534     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00535"></a>00535   }
<a name="l00536"></a>00536 
<a name="l00537"></a>00537 
<a name="l00538"></a>00538   <span class="comment">/*********************************</span>
<a name="l00539"></a>00539 <span class="comment">   * ELEMENT ACCESS AND ASSIGNMENT *</span>
<a name="l00540"></a>00540 <span class="comment">   *********************************/</span>
<a name="l00541"></a>00541 
<a name="l00542"></a>00542 
<a name="l00544"></a>00544 
<a name="l00548"></a><a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b">00548</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00549"></a>00549   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00550"></a>00550   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00551"></a>00551           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00552"></a>00552 <span class="keyword">  </span>{
<a name="l00553"></a>00553     <span class="keywordflow">return</span> data_(i);
<a name="l00554"></a>00554   }
<a name="l00555"></a>00555 
<a name="l00556"></a>00556 
<a name="l00558"></a>00558 
<a name="l00562"></a>00562   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00563"></a>00563   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00564"></a>00564   <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator() </a>(<span class="keywordtype">int</span> i)
<a name="l00565"></a>00565   {
<a name="l00566"></a>00566     <span class="keywordflow">return</span> data_(i);
<a name="l00567"></a>00567   }
<a name="l00568"></a>00568 
<a name="l00569"></a>00569 
<a name="l00571"></a>00571 
<a name="l00576"></a><a class="code" href="class_seldon_1_1_vector3.php#a1298a6ec16e6a70ce7de2e456a0c56aa">00576</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00577"></a>00577   <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp;
<a name="l00578"></a>00578   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00579"></a>00579           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00580"></a>00580 <span class="keyword">  </span>{
<a name="l00581"></a>00581     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00582"></a>00582   }
<a name="l00583"></a>00583 
<a name="l00584"></a>00584 
<a name="l00586"></a>00586 
<a name="l00591"></a>00591   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00592"></a>00592   <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp;
<a name="l00593"></a>00593   <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00594"></a>00594   {
<a name="l00595"></a>00595     <span class="keywordflow">return</span> data_(i)(j);
<a name="l00596"></a>00596   }
<a name="l00597"></a>00597 
<a name="l00598"></a>00598 
<a name="l00600"></a>00600 
<a name="l00606"></a><a class="code" href="class_seldon_1_1_vector3.php#afc9bb303c83fd61af66cb9f2b7ea9842">00606</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00607"></a>00607   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::const_reference</a>
<a name="l00608"></a>00608   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>&lt;T, Allocator0,
<a name="l00609"></a>00609           Allocator1, Allocator2&gt;::operator() (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k)<span class="keyword"> const</span>
<a name="l00610"></a>00610 <span class="keyword">  </span>{
<a name="l00611"></a>00611     <span class="keywordflow">return</span> data_(i)(j)(k);
<a name="l00612"></a>00612   }
<a name="l00613"></a>00613 
<a name="l00614"></a>00614 
<a name="l00616"></a>00616 
<a name="l00622"></a><a class="code" href="class_seldon_1_1_vector3.php#ad67a55c6828c310590dae6b671ff2d07">00622</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00623"></a>00623   <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::reference</a>
<a name="l00624"></a>00624   <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::operator</a>()
<a name="l00625"></a>00625     (<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k)
<a name="l00626"></a>00626   {
<a name="l00627"></a>00627     <span class="keywordflow">return</span> data_(i)(j)(k);
<a name="l00628"></a>00628   }
<a name="l00629"></a>00629 
<a name="l00630"></a>00630 
<a name="l00631"></a>00631   <span class="comment">/**********************</span>
<a name="l00632"></a>00632 <span class="comment">   * CONVENIENT METHODS *</span>
<a name="l00633"></a>00633 <span class="comment">   *********************/</span>
<a name="l00634"></a>00634 
<a name="l00635"></a><a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531">00635</a> 
<a name="l00637"></a>00637   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l00638"></a>00638   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531" title="Displays the vector.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00639"></a>00639 <span class="keyword">  </span>{
<a name="l00640"></a>00640     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; data_.GetSize(); i++)
<a name="l00641"></a>00641       <span class="keywordflow">for</span>(<span class="keywordtype">int</span> j = 0; j &lt; data_(i).GetSize(); j++)
<a name="l00642"></a>00642         {
<a name="l00643"></a>00643           cout &lt;&lt; <span class="stringliteral">&quot;Vector &quot;</span> &lt;&lt; i &lt;&lt; <span class="stringliteral">&quot;, &quot;</span> &lt;&lt; j &lt;&lt; <span class="stringliteral">&quot;: &quot;</span>;
<a name="l00644"></a>00644           data_(i)(j).<a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531" title="Displays the vector.">Print</a>();
<a name="l00645"></a>00645         }
<a name="l00646"></a>00646   }
<a name="l00647"></a>00647 
<a name="l00648"></a>00648 
<a name="l00649"></a>00649 } <span class="comment">// namespace Seldon.</span>
<a name="l00650"></a>00650 
<a name="l00651"></a>00651 
<a name="l00652"></a>00652 <span class="preprocessor">#define SELDON_FILE_VECTOR_VECTOR_3_CXX</span>
<a name="l00653"></a>00653 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
