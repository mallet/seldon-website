<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Triangular.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_TRIANGULAR_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Triangular.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14" title="Default constructor.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Matrix_Triangular</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     me_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00052"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a3ecbb30f1d5aee888ef396bbfde1f61b">00052</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00053"></a>00053   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14" title="Default constructor.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00054"></a>00054 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14" title="Default constructor.">  ::Matrix_Triangular</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, i)
<a name="l00055"></a>00055   {
<a name="l00056"></a>00056 
<a name="l00057"></a>00057 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00058"></a>00058 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00059"></a>00059       {
<a name="l00060"></a>00060 <span class="preprocessor">#endif</span>
<a name="l00061"></a>00061 <span class="preprocessor"></span>
<a name="l00062"></a>00062         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(i, <span class="keyword">sizeof</span>(pointer)) );
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         me_ = NULL;
<a name="l00071"></a>00071         this-&gt;data_ = NULL;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00074"></a>00074       {
<a name="l00075"></a>00075         this-&gt;m_ = 0;
<a name="l00076"></a>00076         this-&gt;n_ = 0;
<a name="l00077"></a>00077         this-&gt;data_ = NULL;
<a name="l00078"></a>00078       }
<a name="l00079"></a>00079     <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0)
<a name="l00080"></a>00080       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Triangular::Matrix_Triangular(int, int)&quot;</span>,
<a name="l00081"></a>00081                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00082"></a>00082                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00083"></a>00083                               * static_cast&lt;long int&gt;(i)
<a name="l00084"></a>00084                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00085"></a>00085                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00086"></a>00086                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00087"></a>00087 <span class="preprocessor">#endif</span>
<a name="l00088"></a>00088 <span class="preprocessor"></span>
<a name="l00089"></a>00089 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00090"></a>00090 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00091"></a>00091       {
<a name="l00092"></a>00092 <span class="preprocessor">#endif</span>
<a name="l00093"></a>00093 <span class="preprocessor"></span>
<a name="l00094"></a>00094         this-&gt;data_ = this-&gt;allocator_.allocate(i * i, <span class="keyword">this</span>);
<a name="l00095"></a>00095 
<a name="l00096"></a>00096 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00097"></a>00097 <span class="preprocessor"></span>      }
<a name="l00098"></a>00098     <span class="keywordflow">catch</span> (...)
<a name="l00099"></a>00099       {
<a name="l00100"></a>00100         this-&gt;m_ = 0;
<a name="l00101"></a>00101         this-&gt;n_ = 0;
<a name="l00102"></a>00102         free(me_);
<a name="l00103"></a>00103         me_ = NULL;
<a name="l00104"></a>00104         this-&gt;data_ = NULL;
<a name="l00105"></a>00105       }
<a name="l00106"></a>00106     <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00107"></a>00107       {
<a name="l00108"></a>00108         this-&gt;m_ = 0;
<a name="l00109"></a>00109         this-&gt;n_ = 0;
<a name="l00110"></a>00110         free(me_);
<a name="l00111"></a>00111         me_ = NULL;
<a name="l00112"></a>00112       }
<a name="l00113"></a>00113     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00114"></a>00114       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Triangular::Matrix_Triangular(int, int)&quot;</span>,
<a name="l00115"></a>00115                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00116"></a>00116                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00117"></a>00117                               * static_cast&lt;long int&gt;(i)
<a name="l00118"></a>00118                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00119"></a>00119                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00120"></a>00120                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00121"></a>00121 <span class="preprocessor">#endif</span>
<a name="l00122"></a>00122 <span class="preprocessor"></span>
<a name="l00123"></a>00123     pointer ptr = this-&gt;data_;
<a name="l00124"></a>00124     <span class="keywordtype">int</span> lgth = i;
<a name="l00125"></a>00125     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00126"></a>00126       me_[k] = ptr;
<a name="l00127"></a>00127   }
<a name="l00128"></a>00128 
<a name="l00129"></a>00129 
<a name="l00131"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#ae2727885c2d249d8f386e55403133a7d">00131</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00132"></a>00132   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14" title="Default constructor.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00133"></a>00133 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a60a394e341c7e0425b5c51f89e93dd14" title="Default constructor.">  ::Matrix_Triangular</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular</a>&lt;T, Prop,
<a name="l00134"></a>00134                       Storage, Allocator&gt;&amp; A)
<a name="l00135"></a>00135     : <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;()
<a name="l00136"></a>00136   {
<a name="l00137"></a>00137     this-&gt;m_ = 0;
<a name="l00138"></a>00138     this-&gt;n_ = 0;
<a name="l00139"></a>00139     this-&gt;data_ = NULL;
<a name="l00140"></a>00140     this-&gt;me_ = NULL;
<a name="l00141"></a>00141 
<a name="l00142"></a>00142     this-&gt;Copy(A);
<a name="l00143"></a>00143   }
<a name="l00144"></a>00144 
<a name="l00145"></a>00145 
<a name="l00146"></a>00146   <span class="comment">/**************</span>
<a name="l00147"></a>00147 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00148"></a>00148 <span class="comment">   **************/</span>
<a name="l00149"></a>00149 
<a name="l00150"></a>00150 
<a name="l00152"></a>00152   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00153"></a>00153   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a1bf20e38fe0a64a4db93991819fba679" title="Destructor.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Triangular</a>()
<a name="l00154"></a>00154   {
<a name="l00155"></a>00155 
<a name="l00156"></a>00156 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00157"></a>00157 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00158"></a>00158       {
<a name="l00159"></a>00159 <span class="preprocessor">#endif</span>
<a name="l00160"></a>00160 <span class="preprocessor"></span>
<a name="l00161"></a>00161         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00162"></a>00162           {
<a name="l00163"></a>00163             this-&gt;allocator_.deallocate(this-&gt;data_, this-&gt;m_ * this-&gt;n_);
<a name="l00164"></a>00164             this-&gt;data_ = NULL;
<a name="l00165"></a>00165           }
<a name="l00166"></a>00166 
<a name="l00167"></a>00167 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00168"></a>00168 <span class="preprocessor"></span>      }
<a name="l00169"></a>00169     <span class="keywordflow">catch</span> (...)
<a name="l00170"></a>00170       {
<a name="l00171"></a>00171         this-&gt;data_ = NULL;
<a name="l00172"></a>00172       }
<a name="l00173"></a>00173 <span class="preprocessor">#endif</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>
<a name="l00175"></a>00175 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00176"></a>00176 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00177"></a>00177       {
<a name="l00178"></a>00178 <span class="preprocessor">#endif</span>
<a name="l00179"></a>00179 <span class="preprocessor"></span>
<a name="l00180"></a>00180         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00181"></a>00181           {
<a name="l00182"></a>00182             free(me_);
<a name="l00183"></a>00183             me_ = NULL;
<a name="l00184"></a>00184           }
<a name="l00185"></a>00185 
<a name="l00186"></a>00186 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00187"></a>00187 <span class="preprocessor"></span>      }
<a name="l00188"></a>00188     <span class="keywordflow">catch</span> (...)
<a name="l00189"></a>00189       {
<a name="l00190"></a>00190         this-&gt;m_ = 0;
<a name="l00191"></a>00191         this-&gt;n_ = 0;
<a name="l00192"></a>00192         me_ = NULL;
<a name="l00193"></a>00193       }
<a name="l00194"></a>00194 <span class="preprocessor">#endif</span>
<a name="l00195"></a>00195 <span class="preprocessor"></span>
<a name="l00196"></a>00196   }
<a name="l00197"></a>00197 
<a name="l00198"></a>00198 
<a name="l00200"></a>00200 
<a name="l00204"></a>00204   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00205"></a>00205   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a01cf3952c69288e68376db61cf322c77" title="Clears the matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00206"></a>00206   {
<a name="l00207"></a>00207     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a1bf20e38fe0a64a4db93991819fba679" title="Destructor.">~Matrix_Triangular</a>();
<a name="l00208"></a>00208     this-&gt;m_ = 0;
<a name="l00209"></a>00209     this-&gt;n_ = 0;
<a name="l00210"></a>00210   }
<a name="l00211"></a>00211 
<a name="l00212"></a>00212 
<a name="l00213"></a>00213   <span class="comment">/*******************</span>
<a name="l00214"></a>00214 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00215"></a>00215 <span class="comment">   *******************/</span>
<a name="l00216"></a>00216 
<a name="l00217"></a>00217 
<a name="l00219"></a>00219 
<a name="l00225"></a>00225   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00226"></a>00226   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00227"></a>00227 <span class="keyword">  </span>{
<a name="l00228"></a>00228     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00229"></a>00229   }
<a name="l00230"></a>00230 
<a name="l00231"></a>00231 
<a name="l00232"></a>00232   <span class="comment">/*********************</span>
<a name="l00233"></a>00233 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00234"></a>00234 <span class="comment">   *********************/</span>
<a name="l00235"></a>00235 
<a name="l00236"></a>00236 
<a name="l00238"></a>00238 
<a name="l00244"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3">00244</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00245"></a>00245   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3" title="Reallocates memory to resize the matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00246"></a>00246 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a472fa39bd3faee779cae713a5bd1aeb3" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00247"></a>00247   {
<a name="l00248"></a>00248 
<a name="l00249"></a>00249     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00250"></a>00250       {
<a name="l00251"></a>00251         this-&gt;m_ = i;
<a name="l00252"></a>00252         this-&gt;n_ = i;
<a name="l00253"></a>00253 
<a name="l00254"></a>00254 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00255"></a>00255 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00256"></a>00256           {
<a name="l00257"></a>00257 <span class="preprocessor">#endif</span>
<a name="l00258"></a>00258 <span class="preprocessor"></span>
<a name="l00259"></a>00259             me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( realloc(me_,
<a name="l00260"></a>00260                                                       i * <span class="keyword">sizeof</span>(pointer)) );
<a name="l00261"></a>00261 
<a name="l00262"></a>00262 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00263"></a>00263 <span class="preprocessor"></span>          }
<a name="l00264"></a>00264         <span class="keywordflow">catch</span> (...)
<a name="l00265"></a>00265           {
<a name="l00266"></a>00266             this-&gt;m_ = 0;
<a name="l00267"></a>00267             this-&gt;n_ = 0;
<a name="l00268"></a>00268             me_ = NULL;
<a name="l00269"></a>00269             this-&gt;data_ = NULL;
<a name="l00270"></a>00270           }
<a name="l00271"></a>00271         <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00272"></a>00272           {
<a name="l00273"></a>00273             this-&gt;m_ = 0;
<a name="l00274"></a>00274             this-&gt;n_ = 0;
<a name="l00275"></a>00275             this-&gt;data_ = NULL;
<a name="l00276"></a>00276           }
<a name="l00277"></a>00277         <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0)
<a name="l00278"></a>00278           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Triangular::Reallocate(int, int)&quot;</span>,
<a name="l00279"></a>00279                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00280"></a>00280                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00281"></a>00281                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00282"></a>00282                                   * static_cast&lt;long int&gt;(i)
<a name="l00283"></a>00283                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00284"></a>00284                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00285"></a>00285                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00286"></a>00286 <span class="preprocessor">#endif</span>
<a name="l00287"></a>00287 <span class="preprocessor"></span>
<a name="l00288"></a>00288 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00289"></a>00289 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00290"></a>00290           {
<a name="l00291"></a>00291 <span class="preprocessor">#endif</span>
<a name="l00292"></a>00292 <span class="preprocessor"></span>
<a name="l00293"></a>00293             this-&gt;data_ =
<a name="l00294"></a>00294               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_.reallocate(this-&gt;data_,
<a name="l00295"></a>00295                                                                     i * i,
<a name="l00296"></a>00296                                                                     <span class="keyword">this</span>) );
<a name="l00297"></a>00297 
<a name="l00298"></a>00298 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00299"></a>00299 <span class="preprocessor"></span>          }
<a name="l00300"></a>00300         <span class="keywordflow">catch</span> (...)
<a name="l00301"></a>00301           {
<a name="l00302"></a>00302             this-&gt;m_ = 0;
<a name="l00303"></a>00303             this-&gt;n_ = 0;
<a name="l00304"></a>00304             free(me_);
<a name="l00305"></a>00305             me_ = NULL;
<a name="l00306"></a>00306             this-&gt;data_ = NULL;
<a name="l00307"></a>00307           }
<a name="l00308"></a>00308         <span class="keywordflow">if</span> (this-&gt;data_ == NULL)
<a name="l00309"></a>00309           {
<a name="l00310"></a>00310             this-&gt;m_ = 0;
<a name="l00311"></a>00311             this-&gt;n_ = 0;
<a name="l00312"></a>00312             free(me_);
<a name="l00313"></a>00313             me_ = NULL;
<a name="l00314"></a>00314           }
<a name="l00315"></a>00315         <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0)
<a name="l00316"></a>00316           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Triangular::Reallocate(int, int)&quot;</span>,
<a name="l00317"></a>00317                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00318"></a>00318                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00319"></a>00319                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00320"></a>00320                                   * static_cast&lt;long int&gt;(i)
<a name="l00321"></a>00321                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00322"></a>00322                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i)
<a name="l00323"></a>00323                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00324"></a>00324 <span class="preprocessor">#endif</span>
<a name="l00325"></a>00325 <span class="preprocessor"></span>
<a name="l00326"></a>00326         pointer ptr = this-&gt;data_;
<a name="l00327"></a>00327         <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, i);
<a name="l00328"></a>00328         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, i); k++, ptr += lgth)
<a name="l00329"></a>00329           me_[k] = ptr;
<a name="l00330"></a>00330       }
<a name="l00331"></a>00331   }
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 
<a name="l00335"></a>00335 
<a name="l00342"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915">00342</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00343"></a>00343   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00344"></a>00344 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a30b10bb11499845df9d647c3e0ef8915" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00345"></a>00345   {
<a name="l00346"></a>00346 
<a name="l00347"></a>00347     <span class="keywordflow">if</span> (i != this-&gt;m_)
<a name="l00348"></a>00348       {
<a name="l00349"></a>00349         <span class="comment">// Storing the previous values of the matrix.</span>
<a name="l00350"></a>00350         <span class="keywordtype">int</span> iold = this-&gt;m_;
<a name="l00351"></a>00351         <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;value_type, VectFull, Allocator&gt;</a> xold(this-&gt;GetDataSize());
<a name="l00352"></a>00352         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; this-&gt;GetDataSize(); k++)
<a name="l00353"></a>00353           xold(k) = this-&gt;data_[k];
<a name="l00354"></a>00354 
<a name="l00355"></a>00355         <span class="comment">// Reallocation.</span>
<a name="l00356"></a>00356         this-&gt;Reallocate(i, i);
<a name="l00357"></a>00357 
<a name="l00358"></a>00358         <span class="comment">// Filling the matrix with its previous values.</span>
<a name="l00359"></a>00359         <span class="keywordtype">int</span> imin = min(iold, i);
<a name="l00360"></a>00360         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l00361"></a>00361           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; imin; l++)
<a name="l00362"></a>00362             this-&gt;data_[k*i + l] = xold(k*iold + l);
<a name="l00363"></a>00363       }
<a name="l00364"></a>00364   }
<a name="l00365"></a>00365 
<a name="l00366"></a>00366 
<a name="l00369"></a>00369 
<a name="l00383"></a>00383   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00384"></a>00384   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00385"></a>00385 <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00386"></a>00386             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00387"></a>00387             ::pointer data)
<a name="l00388"></a>00388   {
<a name="l00389"></a>00389 
<a name="l00390"></a>00390     this-&gt;Clear();
<a name="l00391"></a>00391 
<a name="l00392"></a>00392     this-&gt;m_ = i;
<a name="l00393"></a>00393     this-&gt;n_ = i;
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00396"></a>00396 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00397"></a>00397       {
<a name="l00398"></a>00398 <span class="preprocessor">#endif</span>
<a name="l00399"></a>00399 <span class="preprocessor"></span>
<a name="l00400"></a>00400         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(i, <span class="keyword">sizeof</span>(pointer)) );
<a name="l00401"></a>00401 
<a name="l00402"></a>00402 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00403"></a>00403 <span class="preprocessor"></span>      }
<a name="l00404"></a>00404     <span class="keywordflow">catch</span> (...)
<a name="l00405"></a>00405       {
<a name="l00406"></a>00406         this-&gt;m_ = 0;
<a name="l00407"></a>00407         this-&gt;n_ = 0;
<a name="l00408"></a>00408         me_ = NULL;
<a name="l00409"></a>00409         this-&gt;data_ = NULL;
<a name="l00410"></a>00410         <span class="keywordflow">return</span>;
<a name="l00411"></a>00411       }
<a name="l00412"></a>00412     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00413"></a>00413       {
<a name="l00414"></a>00414         this-&gt;m_ = 0;
<a name="l00415"></a>00415         this-&gt;n_ = 0;
<a name="l00416"></a>00416         this-&gt;data_ = NULL;
<a name="l00417"></a>00417         <span class="keywordflow">return</span>;
<a name="l00418"></a>00418       }
<a name="l00419"></a>00419 <span class="preprocessor">#endif</span>
<a name="l00420"></a>00420 <span class="preprocessor"></span>
<a name="l00421"></a>00421     this-&gt;data_ = data;
<a name="l00422"></a>00422 
<a name="l00423"></a>00423     pointer ptr = this-&gt;data_;
<a name="l00424"></a>00424     <span class="keywordtype">int</span> lgth = i;
<a name="l00425"></a>00425     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; i; k++, ptr += lgth)
<a name="l00426"></a>00426       me_[k] = ptr;
<a name="l00427"></a>00427   }
<a name="l00428"></a>00428 
<a name="l00429"></a>00429 
<a name="l00431"></a>00431 
<a name="l00436"></a>00436   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00437"></a>00437   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a5380ef67c34e203bc186a8ca351abbbb" title="Clears the matrix without releasing memory.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00438"></a>00438   {
<a name="l00439"></a>00439     this-&gt;m_ = 0;
<a name="l00440"></a>00440     this-&gt;n_ = 0;
<a name="l00441"></a>00441 
<a name="l00442"></a>00442 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00443"></a>00443 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00444"></a>00444       {
<a name="l00445"></a>00445 <span class="preprocessor">#endif</span>
<a name="l00446"></a>00446 <span class="preprocessor"></span>
<a name="l00447"></a>00447         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00448"></a>00448           {
<a name="l00449"></a>00449             free(me_);
<a name="l00450"></a>00450             me_ = NULL;
<a name="l00451"></a>00451           }
<a name="l00452"></a>00452 
<a name="l00453"></a>00453 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00454"></a>00454 <span class="preprocessor"></span>      }
<a name="l00455"></a>00455     <span class="keywordflow">catch</span> (...)
<a name="l00456"></a>00456       {
<a name="l00457"></a>00457         this-&gt;m_ = 0;
<a name="l00458"></a>00458         this-&gt;n_ = 0;
<a name="l00459"></a>00459         me_ = NULL;
<a name="l00460"></a>00460       }
<a name="l00461"></a>00461 <span class="preprocessor">#endif</span>
<a name="l00462"></a>00462 <span class="preprocessor"></span>
<a name="l00463"></a>00463     this-&gt;data_ = NULL;
<a name="l00464"></a>00464   }
<a name="l00465"></a>00465 
<a name="l00466"></a>00466 
<a name="l00467"></a>00467   <span class="comment">/**********************************</span>
<a name="l00468"></a>00468 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00469"></a>00469 <span class="comment">   **********************************/</span>
<a name="l00470"></a>00470 
<a name="l00471"></a>00471 
<a name="l00473"></a>00473 
<a name="l00479"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f">00479</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00480"></a>00480   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00481"></a>00481   <a class="code" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f" title="Access operator.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00482"></a>00482   {
<a name="l00483"></a>00483 
<a name="l00484"></a>00484 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00485"></a>00485 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00486"></a>00486       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator()&quot;</span>,
<a name="l00487"></a>00487                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00488"></a>00488                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00489"></a>00489     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00490"></a>00490       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator()&quot;</span>,
<a name="l00491"></a>00491                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00492"></a>00492                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00493"></a>00493 <span class="preprocessor">#endif</span>
<a name="l00494"></a>00494 <span class="preprocessor"></span>
<a name="l00495"></a>00495     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00496"></a>00496       {
<a name="l00497"></a>00497         <span class="keywordflow">if</span> (i &gt; j)
<a name="l00498"></a>00498           <span class="keywordflow">return</span> T(0);
<a name="l00499"></a>00499         <span class="keywordflow">else</span>
<a name="l00500"></a>00500           <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00501"></a>00501       }
<a name="l00502"></a>00502     <span class="keywordflow">else</span>
<a name="l00503"></a>00503       {
<a name="l00504"></a>00504         <span class="keywordflow">if</span> (i &lt; j)
<a name="l00505"></a>00505           <span class="keywordflow">return</span> T(0);
<a name="l00506"></a>00506         <span class="keywordflow">else</span>
<a name="l00507"></a>00507           <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00508"></a>00508       }
<a name="l00509"></a>00509   }
<a name="l00510"></a>00510 
<a name="l00511"></a>00511 
<a name="l00513"></a>00513 
<a name="l00519"></a>00519   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00520"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a6cc7b33b27d2a8d51293614d176ebfc0">00520</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::value_type</a>
<a name="l00521"></a>00521   <a class="code" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f" title="Access operator.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00522"></a>00522 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00523"></a>00523 <span class="keyword">  </span>{
<a name="l00524"></a>00524 
<a name="l00525"></a>00525 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00526"></a>00526 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00527"></a>00527       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator() const&quot;</span>,
<a name="l00528"></a>00528                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00529"></a>00529                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00530"></a>00530     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00531"></a>00531       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator() const&quot;</span>,
<a name="l00532"></a>00532                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00533"></a>00533                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00534"></a>00534 <span class="preprocessor">#endif</span>
<a name="l00535"></a>00535 <span class="preprocessor"></span>
<a name="l00536"></a>00536     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l00537"></a>00537       {
<a name="l00538"></a>00538         <span class="keywordflow">if</span> (i &gt; j)
<a name="l00539"></a>00539           <span class="keywordflow">return</span> T(0);
<a name="l00540"></a>00540         <span class="keywordflow">else</span>
<a name="l00541"></a>00541           <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00542"></a>00542       }
<a name="l00543"></a>00543     <span class="keywordflow">else</span>
<a name="l00544"></a>00544       {
<a name="l00545"></a>00545         <span class="keywordflow">if</span> (i &lt; j)
<a name="l00546"></a>00546           <span class="keywordflow">return</span> T(0);
<a name="l00547"></a>00547         <span class="keywordflow">else</span>
<a name="l00548"></a>00548           <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00549"></a>00549       }
<a name="l00550"></a>00550   }
<a name="l00551"></a>00551 
<a name="l00552"></a>00552 
<a name="l00554"></a>00554 
<a name="l00563"></a>00563   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00564"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f">00564</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00565"></a>00565   ::const_reference
<a name="l00566"></a>00566   <a class="code" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f" title="Access operator.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00567"></a>00567 <span class="keyword">  </span>{
<a name="l00568"></a>00568 
<a name="l00569"></a>00569 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00570"></a>00570 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00571"></a>00571       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Triangular::Val(int, int) const&quot;</span>,
<a name="l00572"></a>00572                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00573"></a>00573                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00574"></a>00574     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00575"></a>00575       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Triangular::Val(int, int) const&quot;</span>,
<a name="l00576"></a>00576                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00577"></a>00577                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00578"></a>00578 <span class="preprocessor">#endif</span>
<a name="l00579"></a>00579 <span class="preprocessor"></span>
<a name="l00580"></a>00580     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00581"></a>00581   }
<a name="l00582"></a>00582 
<a name="l00583"></a>00583 
<a name="l00585"></a>00585 
<a name="l00594"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#ab2e953becbaab6664f72752932e14f5c">00594</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00595"></a>00595   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00596"></a>00596   <a class="code" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f" title="Access operator.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00597"></a>00597   {
<a name="l00598"></a>00598 
<a name="l00599"></a>00599 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00600"></a>00600 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00601"></a>00601       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Triangular::Val(int, int)&quot;</span>,
<a name="l00602"></a>00602                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00603"></a>00603                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00604"></a>00604     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00605"></a>00605       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Triangular::Val(int, int)&quot;</span>,
<a name="l00606"></a>00606                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00607"></a>00607                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00608"></a>00608 <span class="preprocessor">#endif</span>
<a name="l00609"></a>00609 <span class="preprocessor"></span>
<a name="l00610"></a>00610     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00611"></a>00611   }
<a name="l00612"></a>00612 
<a name="l00613"></a>00613 
<a name="l00615"></a>00615 
<a name="l00620"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c">00620</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00621"></a>00621   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00622"></a>00622   <a class="code" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c" title="Access to elements of the data array.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00623"></a>00623   {
<a name="l00624"></a>00624 
<a name="l00625"></a>00625 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00626"></a>00626 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00627"></a>00627       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator[] (int)&quot;</span>,
<a name="l00628"></a>00628                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00629"></a>00629                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00630"></a>00630                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00631"></a>00631 <span class="preprocessor">#endif</span>
<a name="l00632"></a>00632 <span class="preprocessor"></span>
<a name="l00633"></a>00633     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00634"></a>00634   }
<a name="l00635"></a>00635 
<a name="l00636"></a>00636 
<a name="l00638"></a>00638 
<a name="l00643"></a>00643   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00644"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#ab4f150251c1afd88a7a59a382ac4f623">00644</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00645"></a>00645   ::const_reference
<a name="l00646"></a>00646   <a class="code" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c" title="Access to elements of the data array.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00647"></a>00647 <span class="keyword">  </span>{
<a name="l00648"></a>00648 
<a name="l00649"></a>00649 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00650"></a>00650 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00651"></a>00651       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Triangular::operator[] (int) const&quot;</span>,
<a name="l00652"></a>00652                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00653"></a>00653                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00654"></a>00654                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00655"></a>00655 <span class="preprocessor">#endif</span>
<a name="l00656"></a>00656 <span class="preprocessor"></span>
<a name="l00657"></a>00657     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00658"></a>00658   }
<a name="l00659"></a>00659 
<a name="l00660"></a>00660 
<a name="l00662"></a>00662 
<a name="l00667"></a>00667   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00668"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4">00668</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00669"></a>00669   <a class="code" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4" title="Duplicates a matrix (assignment operator).">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00670"></a>00670 <a class="code" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00671"></a>00671   {
<a name="l00672"></a>00672     this-&gt;Copy(A);
<a name="l00673"></a>00673 
<a name="l00674"></a>00674     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00675"></a>00675   }
<a name="l00676"></a>00676 
<a name="l00677"></a>00677 
<a name="l00679"></a>00679 
<a name="l00684"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d">00684</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00685"></a>00685   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d" title="Duplicates a matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00686"></a>00686 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a22a6a81034ad25df2123e33d1fb9052d" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00687"></a>00687   {
<a name="l00688"></a>00688     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00689"></a>00689 
<a name="l00690"></a>00690     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00691"></a>00691   }
<a name="l00692"></a>00692 
<a name="l00693"></a>00693 
<a name="l00694"></a>00694   <span class="comment">/************************</span>
<a name="l00695"></a>00695 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00696"></a>00696 <span class="comment">   ************************/</span>
<a name="l00697"></a>00697 
<a name="l00698"></a>00698 
<a name="l00700"></a>00700 
<a name="l00704"></a>00704   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00705"></a>00705   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#af668e8676a93c1a99ebee4794994cc6e" title="Sets all elements to zero.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00706"></a>00706   {
<a name="l00707"></a>00707     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00708"></a>00708                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00709"></a>00709   }
<a name="l00710"></a>00710 
<a name="l00711"></a>00711 
<a name="l00713"></a>00713   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00714"></a>00714   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a17b39f20590f545d3dce529aeaab9d5f" title="Sets the matrix to the identity.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00715"></a>00715   {
<a name="l00716"></a>00716     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00717"></a>00717 
<a name="l00718"></a>00718     T one(1);
<a name="l00719"></a>00719     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00720"></a>00720       this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#aca8b82d5764a1943b2ec9a36d502816f" title="Access operator.">Val</a>(i, i) = one;
<a name="l00721"></a>00721   }
<a name="l00722"></a>00722 
<a name="l00723"></a>00723 
<a name="l00725"></a>00725 
<a name="l00729"></a>00729   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00730"></a>00730   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178" title="Fills the matrix with 0, 1, 2, ...">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00731"></a>00731   {
<a name="l00732"></a>00732     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00733"></a>00733       this-&gt;data_[i] = i;
<a name="l00734"></a>00734   }
<a name="l00735"></a>00735 
<a name="l00736"></a>00736 
<a name="l00738"></a>00738 
<a name="l00742"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#abddc5c5f65873a308e5f6919f5dc4023">00742</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00743"></a>00743   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00744"></a>00744   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178" title="Fills the matrix with 0, 1, 2, ...">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00745"></a>00745   {
<a name="l00746"></a>00746     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00747"></a>00747       this-&gt;data_[i] = x;
<a name="l00748"></a>00748   }
<a name="l00749"></a>00749 
<a name="l00750"></a>00750 
<a name="l00752"></a>00752 
<a name="l00756"></a>00756   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00757"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a2f25eaae12f9e30ddad62426278dee62">00757</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00758"></a>00758   <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00759"></a>00759   <a class="code" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4" title="Duplicates a matrix (assignment operator).">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00760"></a>00760   {
<a name="l00761"></a>00761     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a12f60cdce0a9a1dc924cfab291af4178" title="Fills the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00762"></a>00762 
<a name="l00763"></a>00763     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00764"></a>00764   }
<a name="l00765"></a>00765 
<a name="l00766"></a>00766 
<a name="l00768"></a>00768 
<a name="l00771"></a>00771   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00772"></a>00772   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad9f5e4d6ffc7dff60d8946e26102f71b" title="Fills the matrix randomly.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00773"></a>00773   {
<a name="l00774"></a>00774     srand(time(NULL));
<a name="l00775"></a>00775     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#a812d8809554264f3b8f64429231b4e39" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00776"></a>00776       this-&gt;data_[i] = rand();
<a name="l00777"></a>00777   }
<a name="l00778"></a>00778 
<a name="l00779"></a>00779 
<a name="l00781"></a>00781 
<a name="l00786"></a>00786   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00787"></a>00787   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73" title="Displays the matrix on the standard output.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00788"></a>00788 <span class="keyword">  </span>{
<a name="l00789"></a>00789     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00790"></a>00790       {
<a name="l00791"></a>00791         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00792"></a>00792           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00793"></a>00793         cout &lt;&lt; endl;
<a name="l00794"></a>00794       }
<a name="l00795"></a>00795   }
<a name="l00796"></a>00796 
<a name="l00797"></a>00797 
<a name="l00799"></a>00799 
<a name="l00810"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#aae8a9e344c29a49dac68392d5bb076be">00810</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00811"></a>00811   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73" title="Displays the matrix on the standard output.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00812"></a>00812 <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73" title="Displays the matrix on the standard output.">  ::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b, <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00813"></a>00813 <span class="keyword">  </span>{
<a name="l00814"></a>00814     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a + m); i++)
<a name="l00815"></a>00815       {
<a name="l00816"></a>00816         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b + n); j++)
<a name="l00817"></a>00817           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00818"></a>00818         cout &lt;&lt; endl;
<a name="l00819"></a>00819       }
<a name="l00820"></a>00820   }
<a name="l00821"></a>00821 
<a name="l00822"></a>00822 
<a name="l00824"></a>00824 
<a name="l00832"></a>00832   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00833"></a>00833   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73" title="Displays the matrix on the standard output.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00834"></a>00834 <span class="keyword">  </span>{
<a name="l00835"></a>00835     <a class="code" href="class_seldon_1_1_matrix___triangular.php#ad1b8f26eb7bfda6d3eee2a70707dea73" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00836"></a>00836   }
<a name="l00837"></a>00837 
<a name="l00838"></a>00838 
<a name="l00839"></a>00839   <span class="comment">/**************************</span>
<a name="l00840"></a>00840 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00841"></a>00841 <span class="comment">   **************************/</span>
<a name="l00842"></a>00842 
<a name="l00843"></a>00843 
<a name="l00845"></a>00845 
<a name="l00852"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5">00852</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00853"></a>00853   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5" title="Writes the matrix in a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00854"></a>00854 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00855"></a>00855 <span class="keyword">  </span>{
<a name="l00856"></a>00856     ofstream FileStream;
<a name="l00857"></a>00857     FileStream.open(FileName.c_str());
<a name="l00858"></a>00858 
<a name="l00859"></a>00859 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00860"></a>00860 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00861"></a>00861     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00862"></a>00862       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Write(string FileName)&quot;</span>,
<a name="l00863"></a>00863                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00864"></a>00864 <span class="preprocessor">#endif</span>
<a name="l00865"></a>00865 <span class="preprocessor"></span>
<a name="l00866"></a>00866     this-&gt;Write(FileStream);
<a name="l00867"></a>00867 
<a name="l00868"></a>00868     FileStream.close();
<a name="l00869"></a>00869   }
<a name="l00870"></a>00870 
<a name="l00871"></a>00871 
<a name="l00873"></a>00873 
<a name="l00880"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a5415533e02cb7b5445e3005ce1f54bda">00880</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00881"></a>00881   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5" title="Writes the matrix in a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00882"></a>00882 <a class="code" href="class_seldon_1_1_matrix___triangular.php#a9696365a66c66029580049b9f282a4c5" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00883"></a>00883 <span class="keyword">  </span>{
<a name="l00884"></a>00884 
<a name="l00885"></a>00885 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00886"></a>00886 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00887"></a>00887     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00888"></a>00888       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00889"></a>00889                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00890"></a>00890 <span class="preprocessor">#endif</span>
<a name="l00891"></a>00891 <span class="preprocessor"></span>
<a name="l00892"></a>00892     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00893"></a>00893                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00894"></a>00894     FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00895"></a>00895                      <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00896"></a>00896 
<a name="l00897"></a>00897     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00898"></a>00898                      this-&gt;m_ * this-&gt;n_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00899"></a>00899 
<a name="l00900"></a>00900 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00901"></a>00901 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00902"></a>00902     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00903"></a>00903       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Write(ofstream&amp; FileStream)&quot;</span>,
<a name="l00904"></a>00904                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00905"></a>00905                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00906"></a>00906                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00907"></a>00907 <span class="preprocessor">#endif</span>
<a name="l00908"></a>00908 <span class="preprocessor"></span>
<a name="l00909"></a>00909   }
<a name="l00910"></a>00910 
<a name="l00911"></a>00911 
<a name="l00913"></a>00913 
<a name="l00920"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076">00920</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00921"></a>00921   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076" title="Writes the matrix in a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00922"></a>00922 <a class="code" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00923"></a>00923 <span class="keyword">  </span>{
<a name="l00924"></a>00924     ofstream FileStream;
<a name="l00925"></a>00925     FileStream.precision(cout.precision());
<a name="l00926"></a>00926     FileStream.flags(cout.flags());
<a name="l00927"></a>00927     FileStream.open(FileName.c_str());
<a name="l00928"></a>00928 
<a name="l00929"></a>00929 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00930"></a>00930 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00931"></a>00931     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00932"></a>00932       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::WriteText(string FileName)&quot;</span>,
<a name="l00933"></a>00933                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00934"></a>00934 <span class="preprocessor">#endif</span>
<a name="l00935"></a>00935 <span class="preprocessor"></span>
<a name="l00936"></a>00936     this-&gt;WriteText(FileStream);
<a name="l00937"></a>00937 
<a name="l00938"></a>00938     FileStream.close();
<a name="l00939"></a>00939   }
<a name="l00940"></a>00940 
<a name="l00941"></a>00941 
<a name="l00943"></a>00943 
<a name="l00950"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#abc80799bd9b45574598b3350187715da">00950</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00951"></a>00951   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076" title="Writes the matrix in a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00952"></a>00952 <a class="code" href="class_seldon_1_1_matrix___triangular.php#ac192355d2d110149854ca8bb9406b076" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00953"></a>00953 <span class="keyword">  </span>{
<a name="l00954"></a>00954 
<a name="l00955"></a>00955 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00956"></a>00956 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00957"></a>00957     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00958"></a>00958       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00959"></a>00959                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l00960"></a>00960 <span class="preprocessor">#endif</span>
<a name="l00961"></a>00961 <span class="preprocessor"></span>
<a name="l00962"></a>00962     <span class="keywordtype">int</span> i, j;
<a name="l00963"></a>00963     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00964"></a>00964       {
<a name="l00965"></a>00965         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00966"></a>00966           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00967"></a>00967         FileStream &lt;&lt; endl;
<a name="l00968"></a>00968       }
<a name="l00969"></a>00969 
<a name="l00970"></a>00970 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00971"></a>00971 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00972"></a>00972     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00973"></a>00973       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::WriteText(ofstream&amp; FileStream)&quot;</span>,
<a name="l00974"></a>00974                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l00975"></a>00975                     + string(<span class="stringliteral">&quot; The output file may have been removed&quot;</span>)
<a name="l00976"></a>00976                     + <span class="stringliteral">&quot; or there is no space left on device.&quot;</span>);
<a name="l00977"></a>00977 <span class="preprocessor">#endif</span>
<a name="l00978"></a>00978 <span class="preprocessor"></span>
<a name="l00979"></a>00979   }
<a name="l00980"></a>00980 
<a name="l00981"></a>00981 
<a name="l00983"></a>00983 
<a name="l00990"></a>00990   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00991"></a>00991   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a" title="Reads the matrix from a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::Read</a>(<span class="keywordtype">string</span> FileName)
<a name="l00992"></a>00992   {
<a name="l00993"></a>00993     ifstream FileStream;
<a name="l00994"></a>00994     FileStream.open(FileName.c_str());
<a name="l00995"></a>00995 
<a name="l00996"></a>00996 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00997"></a>00997 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00998"></a>00998     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00999"></a>00999       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Read(string FileName)&quot;</span>,
<a name="l01000"></a>01000                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01001"></a>01001 <span class="preprocessor">#endif</span>
<a name="l01002"></a>01002 <span class="preprocessor"></span>
<a name="l01003"></a>01003     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a" title="Reads the matrix from a file.">Read</a>(FileStream);
<a name="l01004"></a>01004 
<a name="l01005"></a>01005     FileStream.close();
<a name="l01006"></a>01006   }
<a name="l01007"></a>01007 
<a name="l01008"></a>01008 
<a name="l01010"></a>01010 
<a name="l01017"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a1fad405d3c4a66ef2f1aab8ad0d2c054">01017</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01018"></a>01018   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a" title="Reads the matrix from a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01019"></a>01019 <a class="code" href="class_seldon_1_1_matrix___triangular.php#afd699e58126e4a75b734c72a7b07652a" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream)
<a name="l01020"></a>01020   {
<a name="l01021"></a>01021 
<a name="l01022"></a>01022 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01023"></a>01023 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01024"></a>01024     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01025"></a>01025       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01026"></a>01026                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01027"></a>01027 <span class="preprocessor">#endif</span>
<a name="l01028"></a>01028 <span class="preprocessor"></span>
<a name="l01029"></a>01029     <span class="keywordtype">int</span> new_m, new_n;
<a name="l01030"></a>01030     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01031"></a>01031     FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01032"></a>01032     this-&gt;Reallocate(new_m, new_n);
<a name="l01033"></a>01033 
<a name="l01034"></a>01034     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01035"></a>01035                     new_m * new_n * <span class="keyword">sizeof</span>(value_type));
<a name="l01036"></a>01036 
<a name="l01037"></a>01037 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01038"></a>01038 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01039"></a>01039     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01040"></a>01040       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Triangular::Read(ifstream&amp; FileStream)&quot;</span>,
<a name="l01041"></a>01041                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Output operation failed.&quot;</span>)
<a name="l01042"></a>01042                     + string(<span class="stringliteral">&quot; The intput file may have been removed&quot;</span>)
<a name="l01043"></a>01043                     + <span class="stringliteral">&quot; or may not contain enough data.&quot;</span>);
<a name="l01044"></a>01044 <span class="preprocessor">#endif</span>
<a name="l01045"></a>01045 <span class="preprocessor"></span>
<a name="l01046"></a>01046   }
<a name="l01047"></a>01047 
<a name="l01048"></a>01048 
<a name="l01050"></a>01050 
<a name="l01054"></a>01054   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01055"></a>01055   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb" title="Reads the matrix from a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01056"></a>01056   {
<a name="l01057"></a>01057     ifstream FileStream;
<a name="l01058"></a>01058     FileStream.open(FileName.c_str());
<a name="l01059"></a>01059 
<a name="l01060"></a>01060 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01061"></a>01061 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01062"></a>01062     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01063"></a>01063       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l01064"></a>01064                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01065"></a>01065 <span class="preprocessor">#endif</span>
<a name="l01066"></a>01066 <span class="preprocessor"></span>
<a name="l01067"></a>01067     this-&gt;<a class="code" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01068"></a>01068 
<a name="l01069"></a>01069     FileStream.close();
<a name="l01070"></a>01070   }
<a name="l01071"></a>01071 
<a name="l01072"></a>01072 
<a name="l01074"></a>01074 
<a name="l01078"></a><a class="code" href="class_seldon_1_1_matrix___triangular.php#a16abbe61e55e1a6a1f0a173f32457eed">01078</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01079"></a>01079   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb" title="Reads the matrix from a file.">Matrix_Triangular&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01080"></a>01080 <a class="code" href="class_seldon_1_1_matrix___triangular.php#aa1064e91f58256717c26641aff9f8dbb" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01081"></a>01081   {
<a name="l01082"></a>01082     <span class="comment">// clears previous matrix</span>
<a name="l01083"></a>01083     Clear();
<a name="l01084"></a>01084 
<a name="l01085"></a>01085 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01086"></a>01086 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01087"></a>01087     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01088"></a>01088       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01089"></a>01089                     <span class="stringliteral">&quot;Stream is not ready.&quot;</span>);
<a name="l01090"></a>01090 <span class="preprocessor">#endif</span>
<a name="l01091"></a>01091 <span class="preprocessor"></span>
<a name="l01092"></a>01092     <span class="comment">// we read first line</span>
<a name="l01093"></a>01093     <span class="keywordtype">string</span> line;
<a name="l01094"></a>01094     getline(FileStream, line);
<a name="l01095"></a>01095 
<a name="l01096"></a>01096     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01097"></a>01097       {
<a name="l01098"></a>01098         <span class="comment">// empty file ?</span>
<a name="l01099"></a>01099         <span class="keywordflow">return</span>;
<a name="l01100"></a>01100       }
<a name="l01101"></a>01101 
<a name="l01102"></a>01102     <span class="comment">// converting first line into a vector</span>
<a name="l01103"></a>01103     istringstream line_stream(line);
<a name="l01104"></a>01104     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l01105"></a>01105     first_row.ReadText(line_stream);
<a name="l01106"></a>01106 
<a name="l01107"></a>01107     <span class="comment">// and now the other rows</span>
<a name="l01108"></a>01108     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_rows;
<a name="l01109"></a>01109     other_rows.ReadText(FileStream);
<a name="l01110"></a>01110 
<a name="l01111"></a>01111     <span class="comment">// number of rows and columns</span>
<a name="l01112"></a>01112     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l01113"></a>01113     <span class="keywordtype">int</span> m = 1 + other_rows.GetM()/n;
<a name="l01114"></a>01114 
<a name="l01115"></a>01115 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01116"></a>01116 <span class="preprocessor"></span>    <span class="comment">// Checking number of elements</span>
<a name="l01117"></a>01117     <span class="keywordflow">if</span> (other_rows.GetM() != (m-1)*n)
<a name="l01118"></a>01118       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(ifstream&amp; FileStream)&quot;</span>,
<a name="l01119"></a>01119                     <span class="stringliteral">&quot;The file should contain same number of columns.&quot;</span>);
<a name="l01120"></a>01120 <span class="preprocessor">#endif</span>
<a name="l01121"></a>01121 <span class="preprocessor"></span>
<a name="l01122"></a>01122     this-&gt;Reallocate(m,n);
<a name="l01123"></a>01123     <span class="comment">// filling matrix</span>
<a name="l01124"></a>01124     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01125"></a>01125       this-&gt;Val(0, j) = first_row(j);
<a name="l01126"></a>01126 
<a name="l01127"></a>01127     <span class="keywordtype">int</span> nb = 0;
<a name="l01128"></a>01128     <span class="keywordflow">if</span> (Storage::UpLo())
<a name="l01129"></a>01129       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01130"></a>01130         {
<a name="l01131"></a>01131           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l01132"></a>01132             nb++;
<a name="l01133"></a>01133 
<a name="l01134"></a>01134           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i; j &lt; n; j++)
<a name="l01135"></a>01135             this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01136"></a>01136         }
<a name="l01137"></a>01137     <span class="keywordflow">else</span>
<a name="l01138"></a>01138       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01139"></a>01139         {
<a name="l01140"></a>01140           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt;= i; j++)
<a name="l01141"></a>01141             this-&gt;Val(i, j) = other_rows(nb++);
<a name="l01142"></a>01142 
<a name="l01143"></a>01143           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = i+1; j &lt; n; j++)
<a name="l01144"></a>01144             nb++;
<a name="l01145"></a>01145         }
<a name="l01146"></a>01146   }
<a name="l01147"></a>01147 
<a name="l01148"></a>01148 
<a name="l01149"></a>01149 
<a name="l01151"></a>01151   <span class="comment">// MATRIX&lt;COLUPTRIANG&gt; //</span>
<a name="l01153"></a>01153 <span class="comment"></span>
<a name="l01154"></a>01154 
<a name="l01155"></a>01155   <span class="comment">/****************</span>
<a name="l01156"></a>01156 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01157"></a>01157 <span class="comment">   ****************/</span>
<a name="l01158"></a>01158 
<a name="l01159"></a>01159 
<a name="l01161"></a>01161 
<a name="l01164"></a>01164   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01165"></a>01165   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;::Matrix</a>()  throw():
<a name="l01166"></a>01166     <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_up_triang.php">ColUpTriang</a>, Allocator&gt;()
<a name="l01167"></a>01167   {
<a name="l01168"></a>01168   }
<a name="l01169"></a>01169 
<a name="l01170"></a>01170 
<a name="l01172"></a>01172 
<a name="l01176"></a>01176   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01177"></a>01177   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#a4666563d07bce1c82424eb8371145a60" title="Default constructor.">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01178"></a>01178     Matrix_Triangular&lt;T, Prop, ColUpTriang, Allocator&gt;(i, j)
<a name="l01179"></a>01179   {
<a name="l01180"></a>01180   }
<a name="l01181"></a>01181 
<a name="l01182"></a>01182 
<a name="l01183"></a>01183   <span class="comment">/*****************</span>
<a name="l01184"></a>01184 <span class="comment">   * OTHER METHODS *</span>
<a name="l01185"></a>01185 <span class="comment">   *****************/</span>
<a name="l01186"></a>01186 
<a name="l01187"></a>01187 
<a name="l01189"></a>01189 
<a name="l01193"></a>01193   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01194"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#ac7f70aeb83a8db1f3b8e157f731d76f9">01194</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01195"></a>01195   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php" title="Column-major upper-triangular full-matrix class.">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;</a>&amp;
<a name="l01196"></a>01196   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01197"></a>01197   {
<a name="l01198"></a>01198     this-&gt;Fill(x);
<a name="l01199"></a>01199 
<a name="l01200"></a>01200     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01201"></a>01201   }
<a name="l01202"></a>01202 
<a name="l01203"></a>01203 
<a name="l01205"></a>01205 
<a name="l01208"></a>01208   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01209"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#aa7129e3653db9f71709cc835d05e1876">01209</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01210"></a>01210   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php" title="Column-major upper-triangular full-matrix class.">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;</a>&amp;
<a name="l01211"></a>01211   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColUpTriang, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01212"></a>01212   {
<a name="l01213"></a>01213     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01214"></a>01214       this-&gt;data_[i] *= x;
<a name="l01215"></a>01215 
<a name="l01216"></a>01216     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01217"></a>01217   }
<a name="l01218"></a>01218 
<a name="l01219"></a>01219 
<a name="l01220"></a>01220 
<a name="l01222"></a>01222   <span class="comment">// MATRIX&lt;COLLOTRIANG&gt; //</span>
<a name="l01224"></a>01224 <span class="comment"></span>
<a name="l01225"></a>01225 
<a name="l01226"></a>01226   <span class="comment">/****************</span>
<a name="l01227"></a>01227 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01228"></a>01228 <span class="comment">   ****************/</span>
<a name="l01229"></a>01229 
<a name="l01230"></a>01230 
<a name="l01232"></a>01232 
<a name="l01235"></a>01235   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01236"></a>01236   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;::Matrix</a>()  throw():
<a name="l01237"></a>01237     <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_lo_triang.php">ColLoTriang</a>, Allocator&gt;()
<a name="l01238"></a>01238   {
<a name="l01239"></a>01239   }
<a name="l01240"></a>01240 
<a name="l01241"></a>01241 
<a name="l01243"></a>01243 
<a name="l01247"></a>01247   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01248"></a>01248   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#a223ddc59cbc385192a72028c63f91ca5" title="Default constructor.">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01249"></a>01249     Matrix_Triangular&lt;T, Prop, ColLoTriang, Allocator&gt;(i, j)
<a name="l01250"></a>01250   {
<a name="l01251"></a>01251   }
<a name="l01252"></a>01252 
<a name="l01253"></a>01253 
<a name="l01254"></a>01254   <span class="comment">/*****************</span>
<a name="l01255"></a>01255 <span class="comment">   * OTHER METHODS *</span>
<a name="l01256"></a>01256 <span class="comment">   *****************/</span>
<a name="l01257"></a>01257 
<a name="l01258"></a>01258 
<a name="l01260"></a>01260 
<a name="l01264"></a>01264   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01265"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#ac04f5c5815fbc6085da6b7f0c62a0281">01265</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01266"></a>01266   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php" title="Column-major lower-triangular full-matrix class.">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;</a>&amp;
<a name="l01267"></a>01267   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01268"></a>01268   {
<a name="l01269"></a>01269     this-&gt;Fill(x);
<a name="l01270"></a>01270 
<a name="l01271"></a>01271     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01272"></a>01272   }
<a name="l01273"></a>01273 
<a name="l01274"></a>01274 
<a name="l01276"></a>01276 
<a name="l01279"></a>01279   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01280"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#a034cf7dc7c8ba68a6a94e45db6789c5d">01280</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01281"></a>01281   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php" title="Column-major lower-triangular full-matrix class.">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;</a>&amp;
<a name="l01282"></a>01282   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColLoTriang, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01283"></a>01283   {
<a name="l01284"></a>01284     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01285"></a>01285       this-&gt;data_[i] *= x;
<a name="l01286"></a>01286 
<a name="l01287"></a>01287     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01288"></a>01288   }
<a name="l01289"></a>01289 
<a name="l01290"></a>01290 
<a name="l01291"></a>01291 
<a name="l01293"></a>01293   <span class="comment">// MATRIX&lt;ROWUPTRIANG&gt; //</span>
<a name="l01295"></a>01295 <span class="comment"></span>
<a name="l01296"></a>01296 
<a name="l01297"></a>01297   <span class="comment">/****************</span>
<a name="l01298"></a>01298 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01299"></a>01299 <span class="comment">   ****************/</span>
<a name="l01300"></a>01300 
<a name="l01301"></a>01301 
<a name="l01303"></a>01303 
<a name="l01306"></a>01306   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01307"></a>01307   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;::Matrix</a>()  throw():
<a name="l01308"></a>01308     <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_up_triang.php">RowUpTriang</a>, Allocator&gt;()
<a name="l01309"></a>01309   {
<a name="l01310"></a>01310   }
<a name="l01311"></a>01311 
<a name="l01312"></a>01312 
<a name="l01314"></a>01314 
<a name="l01318"></a>01318   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01319"></a>01319   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a5d54235702d1a8347394a5b63846568a" title="Default constructor.">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01320"></a>01320     Matrix_Triangular&lt;T, Prop, RowUpTriang, Allocator&gt;(i, j)
<a name="l01321"></a>01321   {
<a name="l01322"></a>01322   }
<a name="l01323"></a>01323 
<a name="l01324"></a>01324 
<a name="l01325"></a>01325   <span class="comment">/*****************</span>
<a name="l01326"></a>01326 <span class="comment">   * OTHER METHODS *</span>
<a name="l01327"></a>01327 <span class="comment">   *****************/</span>
<a name="l01328"></a>01328 
<a name="l01329"></a>01329 
<a name="l01331"></a>01331 
<a name="l01335"></a>01335   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01336"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1c1975aa196d966318a70af7ccfa177b">01336</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01337"></a>01337   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php" title="Row-major upper-triangular full-matrix class.">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;</a>&amp;
<a name="l01338"></a>01338   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01339"></a>01339   {
<a name="l01340"></a>01340     this-&gt;Fill(x);
<a name="l01341"></a>01341 
<a name="l01342"></a>01342     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01343"></a>01343   }
<a name="l01344"></a>01344 
<a name="l01345"></a>01345 
<a name="l01347"></a>01347 
<a name="l01350"></a>01350   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01351"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a19b9f7624485f210146c6255c6835c1a">01351</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01352"></a>01352   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php" title="Row-major upper-triangular full-matrix class.">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;</a>&amp;
<a name="l01353"></a>01353   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowUpTriang, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01354"></a>01354   {
<a name="l01355"></a>01355     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01356"></a>01356       this-&gt;data_[i] *= x;
<a name="l01357"></a>01357 
<a name="l01358"></a>01358     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01359"></a>01359   }
<a name="l01360"></a>01360 
<a name="l01361"></a>01361 
<a name="l01362"></a>01362 
<a name="l01364"></a>01364   <span class="comment">// MATRIX&lt;ROWLOTRIANG&gt; //</span>
<a name="l01366"></a>01366 <span class="comment"></span>
<a name="l01367"></a>01367 
<a name="l01368"></a>01368   <span class="comment">/****************</span>
<a name="l01369"></a>01369 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01370"></a>01370 <span class="comment">   ****************/</span>
<a name="l01371"></a>01371 
<a name="l01372"></a>01372 
<a name="l01374"></a>01374 
<a name="l01377"></a>01377   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01378"></a>01378   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;::Matrix</a>()  throw():
<a name="l01379"></a>01379     <a class="code" href="class_seldon_1_1_matrix___triangular.php" title="Triangular matrix stored in a full matrix.">Matrix_Triangular</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_lo_triang.php">RowLoTriang</a>, Allocator&gt;()
<a name="l01380"></a>01380   {
<a name="l01381"></a>01381   }
<a name="l01382"></a>01382 
<a name="l01383"></a>01383 
<a name="l01385"></a>01385 
<a name="l01389"></a>01389   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01390"></a>01390   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#ac212f7adb2afae45023db30083b8a5c0" title="Default constructor.">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01391"></a>01391     Matrix_Triangular&lt;T, Prop, RowLoTriang, Allocator&gt;(i, j)
<a name="l01392"></a>01392   {
<a name="l01393"></a>01393   }
<a name="l01394"></a>01394 
<a name="l01395"></a>01395 
<a name="l01396"></a>01396   <span class="comment">/*****************</span>
<a name="l01397"></a>01397 <span class="comment">   * OTHER METHODS *</span>
<a name="l01398"></a>01398 <span class="comment">   *****************/</span>
<a name="l01399"></a>01399 
<a name="l01400"></a>01400 
<a name="l01402"></a>01402 
<a name="l01406"></a>01406   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01407"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#ae36017d3afd1be1b01bfb6a63c86f4c9">01407</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01408"></a>01408   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php" title="Row-major lower-triangular full-matrix class.">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;</a>&amp;
<a name="l01409"></a>01409   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01410"></a>01410   {
<a name="l01411"></a>01411     this-&gt;Fill(x);
<a name="l01412"></a>01412 
<a name="l01413"></a>01413     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01414"></a>01414   }
<a name="l01415"></a>01415 
<a name="l01416"></a>01416 
<a name="l01418"></a>01418 
<a name="l01421"></a>01421   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01422"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#a172f58cf130f1566b88db419dabbb02a">01422</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01423"></a>01423   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php" title="Row-major lower-triangular full-matrix class.">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;</a>&amp;
<a name="l01424"></a>01424   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowLoTriang, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01425"></a>01425   {
<a name="l01426"></a>01426     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;GetDataSize();i++)
<a name="l01427"></a>01427       this-&gt;data_[i] *= x;
<a name="l01428"></a>01428 
<a name="l01429"></a>01429     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01430"></a>01430   }
<a name="l01431"></a>01431 
<a name="l01432"></a>01432 } <span class="comment">// namespace Seldon.</span>
<a name="l01433"></a>01433 
<a name="l01434"></a>01434 <span class="preprocessor">#define SELDON_FILE_MATRIX_TRIANGULAR_CXX</span>
<a name="l01435"></a>01435 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
