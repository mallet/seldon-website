<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::MatrixMumps&lt; T &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>CallMumps</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>CallMumps</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline, protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>CallMumps</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a8d3c03fd1f5e00dbbbb39e8f9e1cc4ee">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>DisableOutOfCore</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>EnableOutOfCore</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a712e9edf500c0a63ddded960762707c5">FactorizeMatrix</a>(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a3b7b913e2e23e1d02890df3c332c6637">FindOrdering</a>(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat, IVect &amp;numbers, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#aab6219d8e5473f80dae2957a3d003ab0">GetInfoFactorization</a>() const </td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>GetSchurMatrix</b>(Matrix&lt; T, Prop1, Storage1, Allocator1 &gt; &amp;mat, const IVect &amp;num, Matrix&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix=false) (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#adb5993784b010142a470da631025ce0a">GetSchurMatrix</a>(Matrix&lt; T, Prop1, Storage1, Allocator &gt; &amp;mat, const IVect &amp;num, Matrix&lt; T, Prop2, Storage2, Allocator2 &gt; &amp;mat_schur, bool keep_matrix)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a693fc679e4e3703162cd7e8e976b30d0">HideMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a90879b937c4794ebc45fa3858c6e97a2">InitMatrix</a>(const MatrixSparse &amp;, bool dist=false)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline, protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#af1dbd7261f5a31bdc48ba7149b952b32">MatrixMumps</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>new_communicator</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>num_col_glob</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>num_row_glob</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>out_of_core</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a9fd9e06d1da9a27e63cd2fb266f628c0">PerformAnalysis</a>(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#ab59b1cd5d9c51196ea91dea968ae2c00">PerformFactorization</a>(Matrix&lt; T, Prop, Storage, Allocator &gt; &amp;mat)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a7d695b24c99275e0911cb817757f2737">pointer</a> typedef</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>print_level</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a06ba655df528009ca61f4dd767850f68">rank</a></td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a752caecc1023fe5da2a71fc83ae8cbf9">SelectOrdering</a>(int num_ordering)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a9375b89f5cbe3908db7120d22f033beb">ShowMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>Solve</b>(Vector&lt; T, VectFull, Allocator2 &gt; &amp;x) (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#ac2ed47d3a9f1f5c99e9a01a8c6f58083">Solve</a>(const Transpose_status &amp;TransA, Vector&lt; T, VectFull, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#a5863d614bd5110fda7cc3d1dcc7c15f4">Solve</a>(const Transpose_status &amp;TransA, Matrix&lt; T, Prop, ColMajor, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>struct_mumps</b> (defined in <a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#ad33cdf6539eea328102bfa3918adef6c">type_ordering</a></td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_mumps.php#af9f7189c4f700303976d3f2ffd13945a">~MatrixMumps</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_mumps.php">Seldon::MatrixMumps&lt; T &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
