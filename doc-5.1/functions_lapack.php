<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Lapack Functions </h1>  </div>
</div>
<div class="contents">
<p>Those functions are available mainly for dense matrices. When it is available for sparse matrices, it will be specified and the use detailed. In the case of dense matrices, Lapack subroutines are called and you will need to define <code>SELDON_WITH_BLAS</code> and <code> SELDON_WITH_LAPACK</code>. </p>
<table  class="category-table">
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getlu">GetLU </a> </td><td class="category-table-td">performs a LU (or LDL^t) factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getlu">SolveLU </a> </td><td class="category-table-td">solve linear system by using LU factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#refinesolutionlu">RefineSolutionLU</a> </td><td class="category-table-td">improves solution computed by SolveLU  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#reciprocalconditionnumber">ReciprocalConditionNumber </a> </td><td class="category-table-td">computes the inverse of matrix condition number  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getscalingfactors">GetScalingFactors </a> </td><td class="category-table-td">computes row and column scalings to equilibrate a matrix  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getinverse">GetInverse </a> </td><td class="category-table-td">computes the matrix inverse   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getqr">GetQR </a> </td><td class="category-table-td">QR factorization of matrix   </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getlq">GetLQ </a> </td><td class="category-table-td">LQ factorization of matrix   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getq_fromqr">GetQ_FromQR </a> </td><td class="category-table-td">Forms explicitely Q from QR factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#mltq_fromqr">MltQ_FromQR </a> </td><td class="category-table-td">multiplies vector by Q   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getqr">SolveQR </a> </td><td class="category-table-td">solves least-square problems by using QR factorization  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#getlq">SolveLQ </a> </td><td class="category-table-td">solves least-square problems by using LQ factorization   </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#geteigenvalues">GetEigenvalues </a> </td><td class="category-table-td">computes eigenvalues  </td></tr>
<tr class="category-table-tr-2">
<td class="category-table-td"><a href="#geteigenvalueseigenvec">GetEigenvaluesEigenvectors </a> </td><td class="category-table-td">computes eigenvalues and eigenvectors  </td></tr>
<tr class="category-table-tr-1">
<td class="category-table-td"><a href="#getsvd">GetSVD </a> </td><td class="category-table-td">performs singular value decomposition (SVD)   </td></tr>
</table>
<div class="separator"><a class="anchor" id="getlu"></a></div><h3>GetLU, SolveLU</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetLU(Matrix&amp;, Vector&lt;int&gt;&amp; pivot);
  void SolveLU(const Matrix&amp;, const Vector&lt;int&gt;&amp; pivot, Vector&amp;);</pre><pre class="syntax-box">  void GetLU(Matrix&amp;, MatrixMumps&amp;);
  void SolveLU(MatrixMumps&amp;, Vector&amp;);
  void GetLU(Matrix&amp;, MatrixSuperLU&amp;);
  void SolveLU(MatrixSuperLU&amp;, Vector&amp;);
  void GetLU(Matrix&amp;, MatrixUmfPack&amp;);
  void SolveLU(MatrixUmfPack&amp;, Vector&amp;);
</pre><p><code>SolveLU</code> performs a LU factorization or LDL^t factorization (for symmetric matrices) of the provided matrix. This function is implemented both for dense and sparse matrices. In the case of sparse matrices, Seldon is interfaced with external librairies, i.e. <a href="http://mumps.enseeiht.fr/">MUMPS</a>, <a href="http://www.cise.ufl.edu/research/sparse/umfpack/">UMFPACK</a> or <a href="http://crd.lbl.gov/~xiaoye/SuperLU/">SUPERLU</a>. You need to define SELDON_WITH_MUMPS, SELDON_WITH_SUPERLU and/or SELDON_WITH_UMFPACK if you want to factorize a sparse matrix. After a call to GetLU, you can call SolveLU to solve a linear system by using the computed factorization.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// lapack for dense matrices
#define SELDON_WITH_LAPACK

// external libraries for sparse matrices
#define SELDON_WITH_MUMPS
#define SELDON_WITH_SUPERLU
#define SELDON_WITH_UMFPACK

#include "Seldon.hxx"
#include "Solver.hxx"

int main()
{
  // dense matrices
  Matrix&lt;double&gt; A(3, 3);
  Vector&lt;int&gt; pivot;

  // factorization
  GetLU(A, pivot);

  // now you can solve the linear system A x = b
  Vector&lt;double&gt; X(3), B(3);
  B.Fill();
  X = B;
  SolveLU(A, pivot, X);
  
  // sparse matrices, use of Mumps for example
  MatrixMumps&lt;double&gt; mat_lu;
  Matrix&lt;double, General, ArrayRowSparse&gt; Asp(n, n);
  // you add all non-zero entries to matrix Asp
  // then you call GetLU to factorize the matrix
  GetLU(Asp, mat_lu);
  // Asp is empty after GetLU
  // you can solve Asp x = b 
  X = B;
  SolveLU(mat_lu, X);

  return 0;
}
</pre></div>  </pre><h4>Related topics :</h4>
<p><a href="direct.php">Direct solvers for sparse linear systems</a></p>
<h4>Location :</h4>
<p><a class="el" href="_lapack___linear_equations_8cxx_source.php">Lapack_LinearEquations.cxx</a><br/>
 <a class="el" href="_mumps_8cxx_source.php">Mumps.cxx</a><br/>
 <a class="el" href="_super_l_u_8cxx_source.php">SuperLU.cxx</a><br/>
 <a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a></p>
<div class="separator"><a class="anchor" id="reciprocalconditionnumber"></a></div><h3>ReciprocalConditionNumber</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  T ReciprocalConditionNumber(const Matrix&amp;, const Vector&lt;int&gt;&amp;,
                              SeldonNorm1, T&amp;);
  T ReciprocalConditionNumber(const Matrix&amp;, const Vector&lt;int&gt;&amp;,
                              SeldonNormInf, T&amp;);
</pre><p>This function estimates the reciprocal of the condition number of a matrix A, in either the 1-norm or the infinity-norm, using the LU factorization computed by GetLU.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 3);
// initialization of A
A.Fill();
// computation of 1-norm and infinity-norm of matrix
double anorm_one = Norm1(A);
double anorm_inf = NormInf(A);
// factorization of A
Vector&lt;int&gt; pivot;
GetLU(mat_lu, pivot);

// computation of reciprocal of condition number in 1-norm
double rcond = ReciprocalConditionNumber(A, pivot, SeldonNorm1, anorm_one);
// or infinity norm
rcond = ReciprocalConditionNumber(A, pivot, SeldonNormInf, anorm_inf);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#getlu">GetLU</a></p>
<h4>Location :</h4>
<p><a class="el" href="_lapack___linear_equations_8cxx_source.php">Lapack_LinearEquations.cxx</a> </p>
<div class="separator"><a class="anchor" id="refinesolutionlu"></a></div><h3>RefineSolutionLU</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void RefineSolutionLU(const Matrix&amp;, const Matrix&amp;, Vector&lt;int&gt;&amp;,
                        Vector&amp;, const Vector&amp;, T&amp;, T&amp;);
</pre><p><code>RefineSolutionLU</code> improves the computed solution to a system of linear equations and provides error bounds and backward error estimates for the solution. This function should be called after GetLU and SolveLU.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 3);
Matrix&lt;double&gt; mat_lu(3, 3);
// initialization of A
A.Fill();
// factorization of A
mat_lu = A;
Vector&lt;int&gt; pivot;
GetLU(mat_lu, pivot);

// solution of linear system
Vector&lt;double&gt; X(3), B(3);
B.Fill();
X = B;
GetLU(mat_lu, pivot, X);

// now we can call RefineSolutionLU
// backward error
double berr;
// forward error
double ferr;
RefineSolutionLU(A, mat_lu, pivot, X, B, ferr, berr);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#getlu">GetLU</a></p>
<h4>Location :</h4>
<p><br/>
 </p>
<p><a class="el" href="_lapack___linear_equations_8cxx_source.php">Lapack_LinearEquations.cxx</a> </p>
<div class="separator"><a class="anchor" id="getinverse"></a></div><h3>GetInverse</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetInverse(Matrix&amp;);
</pre><p>This function overwrites a dense matrix with its inverse.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 3);
// initialization of A
A.Fill();

// computation of the inverse
GetInverse(A);
</pre></div>  </pre><h4>Related topics : </h4>
<p><a href="#getlu">GetLU</a></p>
<h4>Location :</h4>
<p><a class="el" href="_lapack___linear_equations_8cxx_source.php">Lapack_LinearEquations.cxx</a> </p>
<div class="separator"><a class="anchor" id="getscalingfactors"></a></div><h3>GetScalingFactors</h3>
<h4>Syntax : </h4>
<pre class="syntax-box">
  void GetScalingFactors(const Matrix&amp;, Vector&amp;, Vector&amp;, T&amp;, T&amp;, T&amp;);
</pre><p>This function computes a row and column scaling that reduce the condition number of the matrix. This function is only defined for storages RowMajor and ColMajor (unsymmetric dense matrices).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 3);
// initialization of A
A.Fill();

// computation of scaling
int m = A.GetM(), n = A.GetN();
Vector&lt;double&gt; row_scale(m), col_scale(n);
double row_condition_number, col_condition_number;

GetScalingFactors(A, row_scale, col_scale, row_condition_number, col_condition_number, amax);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___linear_equations_8cxx_source.php">Lapack_LinearEquations.cxx</a> </p>
<div class="separator"><a class="anchor" id="getqr"></a></div><h3>GetQR</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetQR(Matrix&amp;, Vector&amp;);
  void SolveQR(Matrix&amp;, Vector&amp;, Vector&amp;);
</pre><p><code>GetQR</code> computes the QR factorization of a rectangular matrix, while <code>SolveQR</code> exploits this factorization to solve a least-squares problem. This is only defined for storages RowMajor and ColMajor (unsymmetric dense matrices).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 3);
// initialization of A
A.Fill();

// QR Factorization
int m = A.GetM(), n = A.GetN();
Vector&lt;double&gt; tau;
GetQR(A, tau);

// Solving Least squares problem QR X = B  (m &amp;ge; n)
Vector&lt;double&gt; X, B(m);
B.Fill();
SolveQR(A, X);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___least_squares_8cxx_source.php">Lapack_LeastSquares.cxx</a> </p>
<div class="separator"><a class="anchor" id="getlq"></a></div><h3>GetLQ</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetLQ(Matrix&lt;T&gt;&amp;, Vector&lt;T&gt;&amp;);
  void SolveLQ(Matrix&lt;T&gt;&amp;, Vector&lt;T&gt;&amp;, Vector&lt;T&gt;&amp;);
</pre><p><code>GetLQ</code> computes the LQ factorization of a rectangular matrix, while <code>SolveLQ</code> exploits this factorization to solve a least-squares problem. This is only defined for storages RowMajor and ColMajor (unsymmetric dense matrices).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(3, 5);
// initialization of A
A.Fill();

// LQ Factorization
int m = A.GetM(), n = A.GetN();
Vector&lt;double&gt; tau;
GetLQ(A, tau);

// Solving Least squares problem LQ X = B  (m &amp;le; n)
Vector&lt;double&gt; X, B(m);
B.Fill();
SolveLQ(A, X);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___least_squares_8cxx_source.php">Lapack_LeastSquares.cxx</a> </p>
<div class="separator"><a class="anchor" id="mltq_fromqr"></a></div><h3>MltQ_FromQR</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void MltQ_FromQR(const Side&amp;, const Trans&amp;, const Matrix&amp;,
                   const Vector&amp;, Matrix&amp;);
</pre><p>This function multiplies a matrix by Q, where Q is the orthogonal matrix computed during QR factorization. This is only defined for storages RowMajor and ColMajor (unsymmetric dense matrices).</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 3);
// initialization of A
A.Fill();

// QR Factorization
int m = A.GetM(), n = A.GetN();
Vector&lt;double&gt; tau;
GetQR(A, tau);

// computation of Q*C
Matrix&lt;double&gt; C(m, m);
MltQ_FromQR(SeldonLeft, SeldonNoTrans, A, tau, C);

// you can compute C*transpose(Q)
MltQ_FromQR(SeldonRight, SeldonTrans, A, tau, C);

// for complex numbers, you have SeldonConjTrans

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___least_squares_8cxx_source.php">Lapack_LeastSquares.cxx</a> </p>
<div class="separator"><a class="anchor" id="getq_fromqr"></a></div><h3>GetQ_FromQR</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetQ_FromQR(Matrix&amp;, Vector&amp;);
</pre><p>This functions overwrites the QR factorization by matrix Q. This is only defined for storages RowMajor and ColMajor (unsymmetric dense matrices). </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 3);
// initialization of A
A.Fill();

// QR Factorization
int m = A.GetM(), n = A.GetN();
Vector&lt;double&gt; tau;
GetQR(A, tau);

Matrix&lt;double&gt; Q = A;
GetQ_FromQR(Q, tau);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___least_squares_8cxx_source.php">Lapack_LeastSquares.cxx</a> </p>
<div class="separator"><a class="anchor" id="geteigenvalues"></a></div><h3>GetEigenvalues</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetEigenvalues(Matrix&amp;, Vector&amp;);
  void GetEigenvalues(Matrix&amp;, Vector&amp;, Vector&amp;);
  void GetEigenvalues(Matrix&amp;, Matrix&amp;, Vector&amp;);
  void GetEigenvalues(Matrix&amp;, Matrix&amp;, Vector&amp;, Vector&amp;);
  void GetEigenvalues(Matrix&amp;, Matrix&amp;, Vector&amp;, Vector&amp;, Vector&amp;);
</pre><p>This function computes the eigenvalues of a matrix. The matrix is modified after the call to this function. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
Vector&lt;double&gt; lambda_real, lambda_imag;
// initialization of A
A.Fill();

// computing eigenvalues (real part and imaginary part)
GetEigenvalues(A, lambda_real, lambda_imag);

// for symmetric matrices, eigenvalues are real
Matrix&lt;double, Symmetric, RowSymPacked&gt; B(5, 5);
Vector&lt;double&gt; lambda;
// initialization of B
B.Fill();
GetEigenvalues(B, lambda);

// for hermitian matrices too
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; C(5, 5);
// initialization of C
C.Fill();
GetEigenvalues(C, lambda);

// other complex matrices -&gt; complex eigenvalues
Matrix&lt;complex&lt;double&gt;, Symmetric, RowSymPacked&gt; D(5, 5);
Vector&lt;complex&lt;double&gt; &gt; lambda_cpx;
// initialization of D
D.Fill();
GetEigenvalues(D, lambda_cpx);
</pre></div>  </pre><p>The function can also solve a generalized eigenvalue problem, as detailed in the following example.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// symmetric matrices A, B
Matrix&lt;double, Symmetric, RowSymPacked&gt; A(5, 5), B(5, 5);
Vector&lt;double&gt; lambda;
// initialization of A and B as you want
// B has to be positive definite
A.FillRand(); B.SetIdentity();

// we solve generalized eigenvalue problem
// i.e. seeking lambda so that A x = lambda B x
// the function assumes that B is POSITIVE DEFINITE
GetEigenvalues(A, B, lambda);

// same use for hermitian matrices
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; Ah(5, 5), Bh(5,5);
// initialize Ah and Bh as you want
// Bh has to be positive definite
// as a result, eigenvalues are real and you compute them 
GetEigenvalues(Ah, Bh, lambda);

// other complex matrices
// provide complex eigenvalues, potentially infinite if B is indefinite
Matrix&lt;complex&lt;double&gt; &gt; C(5, 5), D(5, 5);
Vector&lt;complex&lt;double&gt; &gt; alphac, betac;

// eigenvalues are written in the form lambda = alphac/betac
GetEigenvalues(C, D, alphac, betac);

// for unsymmetric real matrices, real part and imaginary are stored
// in different vectors
Matrix&lt;double&gt; Ar(5, 5), Br(5, 5);
Vector&lt;double&gt; alpha_real, alpha_imag, beta;

// lambda are written in the form lambda = (alpha_real,alpha_imag)/beta
GetEigenvalues(Ar, Br, alpha_real, alpha_imag, beta);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___eigenvalues_8cxx_source.php">Lapack_Eigenvalues.cxx</a> </p>
<div class="separator"><a class="anchor" id="geteigenvalueseigenvec"></a></div><h3>GetEigenvaluesEigenvectors</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetEigenvaluesEigenvectors(Matrix&amp;, Vector&amp;, Matrix&amp;);
  void GetEigenvaluesEigenvectors(Matrix&amp;, Vector&amp;, Vector&amp;, Matrix&amp;);
  void GetEigenvaluesEigenvectors(Matrix&amp;, Matrix&amp;, Vector&amp;, Matrix&amp;);
  void GetEigenvaluesEigenvectors(Matrix&amp;, Matrix&amp;, Vector&amp;, Vector&amp;,
                                  Matrix&amp;);
</pre><p>This function computes the eigenvalues and eigenvectors of a matrix. The matrix is modified after the call to this function. Each eigenvector is stored in a column. When real unsymmetric matrices are selected, you need to compute real and imaginary part of eigenvalues, then if the j-th eigenvalue is real, the j-th column is the eigenvector associated. If j-th and j+1-th are complex conjugate eigenvalues, the j-th and j+1-the associated columns are vectors A et B such that A+iB and A-iB are the complex conjugate eigenvectors of the initial matrix. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(5, 5);
Vector&lt;double&gt; lambda_real, lambda_imag;
Matrix&lt;double&gt; eigen_vectors;
// initialization of A
A.Fill();

// computing eigenvalues and eigenvectors
GetEigenvalues(A, lambda_real, lambda_imag, eigen_vectors);

// for symmetric matrices, eigenvalues are real
Matrix&lt;double, Symmetric, RowSymPacked&gt; B(5, 5);
Vector&lt;double&gt; lambda;
// initialization of B
B.Fill();
GetEigenvalues(B, lambda);

// for hermitian matrices too
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; C(5, 5);
// initialization of C
C.Fill();
GetEigenvalues(C, lambda);

// other complex matrices -&gt; complex eigenvalues
Matrix&lt;complex&lt;double&gt;, Symmetric, RowSymPacked&gt; D(5, 5);
Vector&lt;complex&lt;double&gt; &gt; lambda_cpx;
// initialization of D
D.Fill();
GetEigenvalues(D, lambda_cpx);


</pre></div>  </pre><p>As for <a href="#geteigenvalues">GetEigenvalues</a>, this function can be used to solve generalized eigenvalues problems as detailed in the following example.</p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
// symmetric matrices A, B
Matrix&lt;double, Symmetric, RowSymPacked&gt; A(5, 5), B(5, 5);
Vector&lt;double&gt; lambda;
Matrix&lt;double&gt; eigen_vectors;
// initialization of A and B as you want
// B has to be positive definite
A.FillRand(); B.SetIdentity();

// we solve generalized eigenvalue problem
// i.e. seeking lambda so that A x = lambda B x
// the function assumes that B is POSITIVE DEFINITE
GetEigenvalues(A, B, lambda, eigen_vectors);

// same use for hermitian matrices
Matrix&lt;complex&lt;double&gt;, General, RowHermPacked&gt; Ah(5, 5), Bh(5,5);
// initialize Ah and Bh as you want
// Bh has to be positive definite
// as a result, eigenvalues are real and you compute them 
Matrix&lt;complex&lt;double&gt; &gt; eigen_vec_cpx;
GetEigenvalues(Ah, Bh, lambda, eigen_vec_cpx);

// other complex matrices
// provide complex eigenvalues, potentially infinite if B is indefinite
Matrix&lt;complex&lt;double&gt; &gt; C(5, 5), D(5, 5);
Vector&lt;complex&lt;double&gt; &gt; alphac, betac;

// eigenvalues are written in the form lambda = alphac/betac
GetEigenvalues(C, D, alphac, betac, eigen_vec_cpx);

// for unsymmetric real matrices, real part and imaginary are stored
// in different vectors
Matrix&lt;double&gt; Ar(5, 5), Br(5, 5);
Vector&lt;double&gt; alpha_real, alpha_imag, beta;

// lambda are written in the form lambda = (alpha_real,alpha_imag)/beta
GetEigenvalues(Ar, Br, alpha_real, alpha_imag, beta, eigen_vector);

</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___eigenvalues_8cxx_source.php">Lapack_Eigenvalues.cxx</a> </p>
<div class="separator"><a class="anchor" id="getsvd"></a></div><h3>GetSVD</h3>
<h4>Syntax :</h4>
<pre class="syntax-box">
  void GetSVD(Matrix&amp;, Vector&amp;, Matrix&amp;, Matrix&amp;);
</pre><p>This function computes the singular value decomposition of a rectangular matrix. As a result, this function is defined only for storages RowMajor and ColMajor. </p>
<h4>Example : </h4>
 <pre class="prettyprint">  <div class="fragment"><pre class="fragment">
Matrix&lt;double&gt; A(10, 5);
Vector&lt;double&gt; lambda;
Matrix&lt;double&gt; U, V;
// initialization of A
A.Fill();

// computing singular value decomposition
// A = U diag(lambda) V
GetSVD(A, lambda, U, V);
</pre></div>  </pre><h4>Location :</h4>
<p><a class="el" href="_lapack___eigenvalues_8cxx_source.php">Lapack_Eigenvalues.cxx</a>  </p>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
