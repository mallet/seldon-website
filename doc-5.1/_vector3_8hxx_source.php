<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>vector/Vector3.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2010, INRIA</span>
<a name="l00002"></a>00002 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_VECTOR_VECTOR_3_HXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 
<a name="l00024"></a>00024 <span class="preprocessor">#ifndef SELDON_VECTOR3_DEFAULT_ALLOCATOR_0</span>
<a name="l00025"></a>00025 <span class="preprocessor"></span>
<a name="l00028"></a>00028 <span class="preprocessor">#define SELDON_VECTOR3_DEFAULT_ALLOCATOR_0 SELDON_DEFAULT_ALLOCATOR</span>
<a name="l00029"></a>00029 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00030"></a>00030 <span class="preprocessor"></span>
<a name="l00031"></a>00031 <span class="preprocessor">#ifndef SELDON_VECTOR3_DEFAULT_ALLOCATOR_1</span>
<a name="l00032"></a>00032 <span class="preprocessor"></span>
<a name="l00035"></a>00035 <span class="preprocessor">#define SELDON_VECTOR3_DEFAULT_ALLOCATOR_1 MallocObject</span>
<a name="l00036"></a>00036 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
<a name="l00037"></a>00037 <span class="preprocessor"></span>
<a name="l00038"></a>00038 <span class="preprocessor">#ifndef SELDON_VECTOR3_DEFAULT_ALLOCATOR_2</span>
<a name="l00039"></a>00039 <span class="preprocessor"></span><span class="preprocessor">#define SELDON_VECTOR3_DEFAULT_ALLOCATOR_2 MallocObject</span>
<a name="l00040"></a>00040 <span class="preprocessor"></span>
<a name="l00043"></a>00043 <span class="preprocessor">#endif</span>
<a name="l00044"></a>00044 <span class="preprocessor"></span>
<a name="l00045"></a>00045 
<a name="l00046"></a>00046 <span class="keyword">namespace </span>Seldon
<a name="l00047"></a>00047 {
<a name="l00048"></a>00048 
<a name="l00050"></a>00050 
<a name="l00064"></a>00064   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T,
<a name="l00065"></a>00065             <span class="keyword">class </span>Allocator0 = SELDON_VECTOR3_DEFAULT_ALLOCATOR_0&lt;T&gt;,
<a name="l00066"></a>00066             <span class="keyword">class </span>Allocator1 = SELDON_VECTOR3_DEFAULT_ALLOCATOR_1&lt;
<a name="l00067"></a>00067               Vector&lt;T, Vect_Full, Allocator0&gt; &gt;,
<a name="l00068"></a>00068             <span class="keyword">class </span>Allocator2 =
<a name="l00069"></a>00069             SELDON_VECTOR3_DEFAULT_ALLOCATOR_2&lt;
<a name="l00070"></a>00070               Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;,
<a name="l00071"></a>00071                      Vect_Full, Allocator1&gt; &gt; &gt;
<a name="l00072"></a><a class="code" href="class_seldon_1_1_vector3.php">00072</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3</a>
<a name="l00073"></a>00073   {
<a name="l00074"></a>00074   <span class="keyword">public</span>:
<a name="l00075"></a>00075     <span class="keyword">typedef</span> T value_type;
<a name="l00076"></a>00076     <span class="keyword">typedef</span> T* pointer;
<a name="l00077"></a>00077     <span class="keyword">typedef</span> <span class="keyword">const</span> T* const_pointer;
<a name="l00078"></a>00078     <span class="keyword">typedef</span> T&amp; reference;
<a name="l00079"></a>00079     <span class="keyword">typedef</span> <span class="keyword">const</span> T&amp; const_reference;
<a name="l00080"></a>00080 
<a name="l00081"></a>00081   <span class="keyword">protected</span>:
<a name="l00082"></a>00082     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, Vect_Full, Allocator1&gt;,
<a name="l00083"></a>00083            Vect_Full, Allocator2&gt; data_;
<a name="l00084"></a>00084 
<a name="l00085"></a>00085   <span class="keyword">public</span>:
<a name="l00086"></a>00086 
<a name="l00087"></a>00087     <span class="comment">/*** Constructors and destructor ***/</span>
<a name="l00088"></a>00088 
<a name="l00089"></a>00089     <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3</a>();
<a name="l00090"></a>00090     <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3</a>(<span class="keywordtype">int</span>);
<a name="l00091"></a>00091     <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>&amp; length);
<a name="l00092"></a>00092     <span class="keyword">template</span> &lt;<span class="keyword">class</span> Allocator&gt;
<a name="l00093"></a>00093     <a class="code" href="class_seldon_1_1_vector3.php#a3425eec2b0400057adeda33a005a188d" title="Default constructor.">Vector3</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int&gt;</a>, Vect_Full, Allocator&gt;&amp; length);
<a name="l00094"></a>00094     <a class="code" href="class_seldon_1_1_vector3.php#ae85c3b686a4100a5c05dc7d82a4b6b80" title="Destructor.">~Vector3</a>();
<a name="l00095"></a>00095 
<a name="l00096"></a>00096     <span class="comment">/*** Management of the vectors ***/</span>
<a name="l00097"></a>00097 
<a name="l00098"></a>00098     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>() <span class="keyword">const</span>;
<a name="l00099"></a>00099     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">GetSize</a>() <span class="keyword">const</span>;
<a name="l00100"></a>00100     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00101"></a>00101     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">GetSize</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00102"></a>00102     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a4a694febe8dba0a1203fd628cd1e3167" title="Returns size along dimension 1.">GetLength</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00103"></a>00103     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a66946f1764cc14abec8a51d4e310f6b5" title="Returns size along dimension 1.">GetSize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j) <span class="keyword">const</span>;
<a name="l00104"></a>00104     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">GetNelement</a>() <span class="keyword">const</span>;
<a name="l00105"></a>00105     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_vector3.php#a497379d28c87524052ee202702db9c91" title="Returns the total number of elements in the inner vectors.">GetNelement</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end) <span class="keyword">const</span>;
<a name="l00106"></a>00106     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Reallocate</a>(<span class="keywordtype">int</span> N);
<a name="l00107"></a>00107     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> N);
<a name="l00108"></a>00108     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a21e22b8d942820eb4c60e3e34c2afcd6" title="Reallocates the vector of vectors of vectors.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> N);
<a name="l00109"></a>00109 
<a name="l00110"></a>00110     <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00111"></a>00111     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Flatten</a>(<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data) <span class="keyword">const</span>;
<a name="l00112"></a>00112     <span class="keyword">template</span> &lt;<span class="keyword">class</span> Td, <span class="keyword">class</span> Allocatord&gt;
<a name="l00113"></a>00113     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a1f70de4b1edeb1a64078ad3113014a8e" title="Returns all values in a vector.">Flatten</a>(<span class="keywordtype">int</span> beg, <span class="keywordtype">int</span> end, <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Td, VectFull, Allocatord&gt;</a>&amp; data)
<a name="l00114"></a>00114       <span class="keyword">const</span>;
<a name="l00115"></a>00115 
<a name="l00116"></a>00116     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">PushBack</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keyword">const</span> T&amp; x);
<a name="l00117"></a>00117     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">PushBack</a>(<span class="keywordtype">int</span> i, <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp; X);
<a name="l00118"></a>00118     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">PushBack</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>,
<a name="l00119"></a>00119                   Vect_Full, Allocator1&gt;&amp; X);
<a name="l00120"></a>00120     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">PushBack</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector</a>&lt;<a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>,
<a name="l00121"></a>00121                   Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp; X);
<a name="l00122"></a>00122     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9bd517dbe558026e398730fd0c604edd" title="Appends an element at the end of the inner vector #i #j.">PushBack</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector3.php" title="Vector of vectors of vectors.">Vector3&lt;T, Allocator0, Allocator1, Allocator2&gt;</a>&amp; X);
<a name="l00123"></a>00123 
<a name="l00124"></a>00124 
<a name="l00125"></a>00125     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Clear</a>();
<a name="l00126"></a>00126     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Clear</a>(<span class="keywordtype">int</span> i);
<a name="l00127"></a>00127     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0e178a2f8fad92f0929f4ea1b1dd1604" title="Clears the vector.">Clear</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00128"></a>00128 
<a name="l00129"></a>00129     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a0d6388a07fda225152eef87cdab2ab5f" title="Fills the vector with a given value.">Fill</a>(<span class="keyword">const</span> T&amp; x);
<a name="l00130"></a>00130 
<a name="l00131"></a>00131     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, Vect_Full, Allocator1&gt;,
<a name="l00132"></a>00132            Vect_Full, Allocator2&gt;&amp;
<a name="l00133"></a>00133     <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>();
<a name="l00134"></a>00134     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>,
<a name="l00135"></a>00135                         Vect_Full, Allocator1&gt;, Vect_Full, Allocator2&gt;&amp;
<a name="l00136"></a>00136     <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>() <span class="keyword">const</span>;
<a name="l00137"></a>00137 
<a name="l00138"></a>00138     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00139"></a>00139     <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>(<span class="keywordtype">int</span> i);
<a name="l00140"></a>00140     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00141"></a>00141     <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00142"></a>00142 
<a name="l00143"></a>00143     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00144"></a>00144     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector3.php#aadaf82b78ca64f7278c081b5b17b6c17" title="Returns the vector of vectors of vectors.">GetVector</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00145"></a>00145       <span class="keyword">const</span>;
<a name="l00146"></a>00146 
<a name="l00147"></a>00147     <span class="comment">/*** Element access and assignment ***/</span>
<a name="l00148"></a>00148     <span class="keyword">const</span>
<a name="l00149"></a>00149     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00150"></a>00150     <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i) <span class="keyword">const</span>;
<a name="l00151"></a>00151     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;Vector&lt;T, Vect_Full, Allocator0&gt;</a>, <a class="code" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator1&gt;&amp;
<a name="l00152"></a>00152     <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i);
<a name="l00153"></a>00153 
<a name="l00154"></a>00154     <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00155"></a>00155       <span class="keyword">const</span>;
<a name="l00156"></a>00156     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T, Vect_Full, Allocator0&gt;</a>&amp; <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j);
<a name="l00157"></a>00157 
<a name="l00158"></a>00158     const_reference <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k) <span class="keyword">const</span>;
<a name="l00159"></a>00159     reference <a class="code" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b" title="Returns a given inner vector of vectors.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k);
<a name="l00160"></a>00160 
<a name="l00161"></a>00161     <span class="comment">/*** Convenient method ***/</span>
<a name="l00162"></a>00162 
<a name="l00163"></a>00163     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_vector3.php#a9b3c87591141123ac43759d6b9ca9531" title="Displays the vector.">Print</a>() <span class="keyword">const</span>;
<a name="l00164"></a>00164   };
<a name="l00165"></a>00165 
<a name="l00166"></a>00166 }
<a name="l00167"></a>00167 
<a name="l00168"></a>00168 
<a name="l00169"></a>00169 <span class="preprocessor">#define SELDON_FILE_VECTOR_VECTOR_3_HXX</span>
<a name="l00170"></a>00170 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
