<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>matrix/Matrix_Pointers.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">//</span>
<a name="l00004"></a>00004 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00005"></a>00005 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00006"></a>00006 <span class="comment">//</span>
<a name="l00007"></a>00007 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00008"></a>00008 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00009"></a>00009 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00010"></a>00010 <span class="comment">// any later version.</span>
<a name="l00011"></a>00011 <span class="comment">//</span>
<a name="l00012"></a>00012 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00013"></a>00013 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00014"></a>00014 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00015"></a>00015 <span class="comment">// more details.</span>
<a name="l00016"></a>00016 <span class="comment">//</span>
<a name="l00017"></a>00017 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00018"></a>00018 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 
<a name="l00021"></a>00021 <span class="preprocessor">#ifndef SELDON_FILE_MATRIX_POINTERS_CXX</span>
<a name="l00022"></a>00022 <span class="preprocessor"></span>
<a name="l00023"></a>00023 <span class="preprocessor">#include &quot;Matrix_Pointers.hxx&quot;</span>
<a name="l00024"></a>00024 
<a name="l00025"></a>00025 <span class="keyword">namespace </span>Seldon
<a name="l00026"></a>00026 {
<a name="l00027"></a>00027 
<a name="l00028"></a>00028 
<a name="l00029"></a>00029   <span class="comment">/****************</span>
<a name="l00030"></a>00030 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l00031"></a>00031 <span class="comment">   ****************/</span>
<a name="l00032"></a>00032 
<a name="l00033"></a>00033 
<a name="l00035"></a>00035 
<a name="l00038"></a>00038   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00039"></a>00039   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Matrix_Pointers</a>():
<a name="l00040"></a>00040     Matrix_Base&lt;T, Allocator&gt;()
<a name="l00041"></a>00041   {
<a name="l00042"></a>00042     me_ = NULL;
<a name="l00043"></a>00043   }
<a name="l00044"></a>00044 
<a name="l00045"></a>00045 
<a name="l00047"></a>00047 
<a name="l00051"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a70433b5aff5b052203136dd43783ac7d">00051</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00052"></a>00052   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00053"></a>00053 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">  ::Matrix_Pointers</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j): <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(i, j)
<a name="l00054"></a>00054   {
<a name="l00055"></a>00055 
<a name="l00056"></a>00056 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00057"></a>00057 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00058"></a>00058       {
<a name="l00059"></a>00059 <span class="preprocessor">#endif</span>
<a name="l00060"></a>00060 <span class="preprocessor"></span>
<a name="l00061"></a>00061         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j),
<a name="l00062"></a>00062                                                  <span class="keyword">sizeof</span>(pointer)) );
<a name="l00063"></a>00063 
<a name="l00064"></a>00064 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00065"></a>00065 <span class="preprocessor"></span>      }
<a name="l00066"></a>00066     <span class="keywordflow">catch</span> (...)
<a name="l00067"></a>00067       {
<a name="l00068"></a>00068         this-&gt;m_ = 0;
<a name="l00069"></a>00069         this-&gt;n_ = 0;
<a name="l00070"></a>00070         me_ = NULL;
<a name="l00071"></a>00071         this-&gt;data_ = NULL;
<a name="l00072"></a>00072       }
<a name="l00073"></a>00073     <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00074"></a>00074       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Matrix_Pointers(int, int)&quot;</span>,
<a name="l00075"></a>00075                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00076"></a>00076                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00077"></a>00077                               * static_cast&lt;long int&gt;(j)
<a name="l00078"></a>00078                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00079"></a>00079                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00080"></a>00080                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00081"></a>00081 <span class="preprocessor">#endif</span>
<a name="l00082"></a>00082 <span class="preprocessor"></span>
<a name="l00083"></a>00083 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00084"></a>00084 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00085"></a>00085       {
<a name="l00086"></a>00086 <span class="preprocessor">#endif</span>
<a name="l00087"></a>00087 <span class="preprocessor"></span>
<a name="l00088"></a>00088         this-&gt;data_ = this-&gt;allocator_.allocate(i * j, <span class="keyword">this</span>);
<a name="l00089"></a>00089 
<a name="l00090"></a>00090 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00091"></a>00091 <span class="preprocessor"></span>      }
<a name="l00092"></a>00092     <span class="keywordflow">catch</span> (...)
<a name="l00093"></a>00093       {
<a name="l00094"></a>00094         this-&gt;m_ = 0;
<a name="l00095"></a>00095         this-&gt;n_ = 0;
<a name="l00096"></a>00096         free(me_);
<a name="l00097"></a>00097         me_ = NULL;
<a name="l00098"></a>00098         this-&gt;data_ = NULL;
<a name="l00099"></a>00099       }
<a name="l00100"></a>00100     <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00101"></a>00101       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Matrix_Pointers(int, int)&quot;</span>,
<a name="l00102"></a>00102                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to allocate memory for a matrix of size &quot;</span>)
<a name="l00103"></a>00103                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00104"></a>00104                               * static_cast&lt;long int&gt;(j)
<a name="l00105"></a>00105                               * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00106"></a>00106                      + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00107"></a>00107                      + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00108"></a>00108 <span class="preprocessor">#endif</span>
<a name="l00109"></a>00109 <span class="preprocessor"></span>
<a name="l00110"></a>00110     pointer ptr = this-&gt;data_;
<a name="l00111"></a>00111     <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00112"></a>00112     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00113"></a>00113       me_[k] = ptr;
<a name="l00114"></a>00114 
<a name="l00115"></a>00115   }
<a name="l00116"></a>00116 
<a name="l00117"></a>00117 
<a name="l00119"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ad8f9783bed7ab3a8a3c781b8bab3ce62">00119</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00120"></a>00120   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00121"></a>00121 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a5f99e5a5a2f9a8f8e7873f829d82ed33" title="Default constructor.">  ::Matrix_Pointers</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A):
<a name="l00122"></a>00122     <a class="code" href="class_seldon_1_1_matrix___base.php" title="Base class for all matrices.">Matrix_Base</a>&lt;T, Allocator&gt;(A)
<a name="l00123"></a>00123   {
<a name="l00124"></a>00124     this-&gt;m_ = 0;
<a name="l00125"></a>00125     this-&gt;n_ = 0;
<a name="l00126"></a>00126     this-&gt;data_ = NULL;
<a name="l00127"></a>00127     this-&gt;me_ = NULL;
<a name="l00128"></a>00128 
<a name="l00129"></a>00129     this-&gt;Copy(A);
<a name="l00130"></a>00130   }
<a name="l00131"></a>00131 
<a name="l00132"></a>00132 
<a name="l00133"></a>00133   <span class="comment">/**************</span>
<a name="l00134"></a>00134 <span class="comment">   * DESTRUCTOR *</span>
<a name="l00135"></a>00135 <span class="comment">   **************/</span>
<a name="l00136"></a>00136 
<a name="l00137"></a>00137 
<a name="l00139"></a>00139   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00140"></a>00140   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca" title="Destructor.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::~Matrix_Pointers</a>()
<a name="l00141"></a>00141   {
<a name="l00142"></a>00142 
<a name="l00143"></a>00143 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00144"></a>00144 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00145"></a>00145       {
<a name="l00146"></a>00146 <span class="preprocessor">#endif</span>
<a name="l00147"></a>00147 <span class="preprocessor"></span>
<a name="l00148"></a>00148         <span class="keywordflow">if</span> (this-&gt;data_ != NULL)
<a name="l00149"></a>00149           {
<a name="l00150"></a>00150             this-&gt;allocator_.deallocate(this-&gt;data_, this-&gt;m_ * this-&gt;n_);
<a name="l00151"></a>00151             this-&gt;data_ = NULL;
<a name="l00152"></a>00152           }
<a name="l00153"></a>00153 
<a name="l00154"></a>00154 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00155"></a>00155 <span class="preprocessor"></span>      }
<a name="l00156"></a>00156     <span class="keywordflow">catch</span> (...)
<a name="l00157"></a>00157       {
<a name="l00158"></a>00158         this-&gt;data_ = NULL;
<a name="l00159"></a>00159       }
<a name="l00160"></a>00160 <span class="preprocessor">#endif</span>
<a name="l00161"></a>00161 <span class="preprocessor"></span>
<a name="l00162"></a>00162 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00163"></a>00163 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00164"></a>00164       {
<a name="l00165"></a>00165 <span class="preprocessor">#endif</span>
<a name="l00166"></a>00166 <span class="preprocessor"></span>
<a name="l00167"></a>00167         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00168"></a>00168           {
<a name="l00169"></a>00169             free(me_);
<a name="l00170"></a>00170             me_ = NULL;
<a name="l00171"></a>00171           }
<a name="l00172"></a>00172 
<a name="l00173"></a>00173 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00174"></a>00174 <span class="preprocessor"></span>      }
<a name="l00175"></a>00175     <span class="keywordflow">catch</span> (...)
<a name="l00176"></a>00176       {
<a name="l00177"></a>00177         this-&gt;m_ = 0;
<a name="l00178"></a>00178         this-&gt;n_ = 0;
<a name="l00179"></a>00179         me_ = NULL;
<a name="l00180"></a>00180       }
<a name="l00181"></a>00181 <span class="preprocessor">#endif</span>
<a name="l00182"></a>00182 <span class="preprocessor"></span>
<a name="l00183"></a>00183   }
<a name="l00184"></a>00184 
<a name="l00185"></a>00185 
<a name="l00187"></a>00187 
<a name="l00191"></a>00191   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00192"></a>00192   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a361729faf34aaedf14ad0b3f044e093c" title="Clears the matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Clear</a>()
<a name="l00193"></a>00193   {
<a name="l00194"></a>00194     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#adc705f0d17fc4d5676e39b1b8f2b08ca" title="Destructor.">~Matrix_Pointers</a>();
<a name="l00195"></a>00195     this-&gt;m_ = 0;
<a name="l00196"></a>00196     this-&gt;n_ = 0;
<a name="l00197"></a>00197   }
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 
<a name="l00200"></a>00200   <span class="comment">/*******************</span>
<a name="l00201"></a>00201 <span class="comment">   * BASIC FUNCTIONS *</span>
<a name="l00202"></a>00202 <span class="comment">   *******************/</span>
<a name="l00203"></a>00203 
<a name="l00204"></a>00204 
<a name="l00206"></a>00206 
<a name="l00212"></a>00212   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00213"></a>00213   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::GetDataSize</a>()<span class="keyword"> const</span>
<a name="l00214"></a>00214 <span class="keyword">  </span>{
<a name="l00215"></a>00215     <span class="keywordflow">return</span> this-&gt;m_ * this-&gt;n_;
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00219"></a>00219   <span class="comment">/*********************</span>
<a name="l00220"></a>00220 <span class="comment">   * MEMORY MANAGEMENT *</span>
<a name="l00221"></a>00221 <span class="comment">   *********************/</span>
<a name="l00222"></a>00222 
<a name="l00223"></a>00223 
<a name="l00225"></a>00225 
<a name="l00231"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793">00231</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00232"></a>00232   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793" title="Reallocates memory to resize the matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00233"></a>00233 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a782143339736b347e5a1c7ecd4d0c793" title="Reallocates memory to resize the matrix.">  ::Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00234"></a>00234   {
<a name="l00235"></a>00235 
<a name="l00236"></a>00236     <span class="keywordflow">if</span> (i != this-&gt;m_ || j != this-&gt;n_)
<a name="l00237"></a>00237       {
<a name="l00238"></a>00238         this-&gt;m_ = i;
<a name="l00239"></a>00239         this-&gt;n_ = j;
<a name="l00240"></a>00240 
<a name="l00241"></a>00241 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00242"></a>00242 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00243"></a>00243           {
<a name="l00244"></a>00244 <span class="preprocessor">#endif</span>
<a name="l00245"></a>00245 <span class="preprocessor"></span>
<a name="l00246"></a>00246             me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( realloc(me_,
<a name="l00247"></a>00247                                                       Storage::GetFirst(i, j)
<a name="l00248"></a>00248                                                       * <span class="keyword">sizeof</span>(pointer)) );
<a name="l00249"></a>00249 
<a name="l00250"></a>00250 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00251"></a>00251 <span class="preprocessor"></span>          }
<a name="l00252"></a>00252         <span class="keywordflow">catch</span> (...)
<a name="l00253"></a>00253           {
<a name="l00254"></a>00254             this-&gt;m_ = 0;
<a name="l00255"></a>00255             this-&gt;n_ = 0;
<a name="l00256"></a>00256             me_ = NULL;
<a name="l00257"></a>00257             this-&gt;data_ = NULL;
<a name="l00258"></a>00258           }
<a name="l00259"></a>00259         <span class="keywordflow">if</span> (me_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00260"></a>00260           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Reallocate(int, int)&quot;</span>,
<a name="l00261"></a>00261                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory for&quot;</span>)
<a name="l00262"></a>00262                          + <span class="stringliteral">&quot; a matrix of size &quot;</span>
<a name="l00263"></a>00263                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00264"></a>00264                                   * static_cast&lt;long int&gt;(j)
<a name="l00265"></a>00265                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00266"></a>00266                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00267"></a>00267                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00268"></a>00268 <span class="preprocessor">#endif</span>
<a name="l00269"></a>00269 <span class="preprocessor"></span>
<a name="l00270"></a>00270 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00271"></a>00271 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l00272"></a>00272           {
<a name="l00273"></a>00273 <span class="preprocessor">#endif</span>
<a name="l00274"></a>00274 <span class="preprocessor"></span>
<a name="l00275"></a>00275             this-&gt;data_ =
<a name="l00276"></a>00276               <span class="keyword">reinterpret_cast&lt;</span>pointer<span class="keyword">&gt;</span>(this-&gt;allocator_
<a name="l00277"></a>00277                                         .reallocate(this-&gt;data_, i * j,
<a name="l00278"></a>00278                                                     <span class="keyword">this</span>));
<a name="l00279"></a>00279 
<a name="l00280"></a>00280 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00281"></a>00281 <span class="preprocessor"></span>          }
<a name="l00282"></a>00282         <span class="keywordflow">catch</span> (...)
<a name="l00283"></a>00283           {
<a name="l00284"></a>00284             this-&gt;m_ = 0;
<a name="l00285"></a>00285             this-&gt;n_ = 0;
<a name="l00286"></a>00286             free(me_);
<a name="l00287"></a>00287             me_ = NULL;
<a name="l00288"></a>00288             this-&gt;data_ = NULL;
<a name="l00289"></a>00289           }
<a name="l00290"></a>00290         <span class="keywordflow">if</span> (this-&gt;data_ == NULL &amp;&amp; i != 0 &amp;&amp; j != 0)
<a name="l00291"></a>00291           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Matrix_Pointers::Reallocate(int, int)&quot;</span>,
<a name="l00292"></a>00292                          <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to reallocate memory&quot;</span>)
<a name="l00293"></a>00293                          + <span class="stringliteral">&quot; for a matrix of size &quot;</span>
<a name="l00294"></a>00294                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(static_cast&lt;long int&gt;(i)
<a name="l00295"></a>00295                                   * static_cast&lt;long int&gt;(j)
<a name="l00296"></a>00296                                   * static_cast&lt;long int&gt;(<span class="keyword">sizeof</span>(T)))
<a name="l00297"></a>00297                          + <span class="stringliteral">&quot; bytes (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j)
<a name="l00298"></a>00298                          + <span class="stringliteral">&quot; elements).&quot;</span>);
<a name="l00299"></a>00299 <span class="preprocessor">#endif</span>
<a name="l00300"></a>00300 <span class="preprocessor"></span>
<a name="l00301"></a>00301         pointer ptr = this-&gt;data_;
<a name="l00302"></a>00302         <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00303"></a>00303         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00304"></a>00304           me_[k] = ptr;
<a name="l00305"></a>00305       }
<a name="l00306"></a>00306   }
<a name="l00307"></a>00307 
<a name="l00308"></a>00308 
<a name="l00311"></a>00311 
<a name="l00325"></a>00325   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00326"></a>00326   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00327"></a>00327 <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">  ::SetData</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j,
<a name="l00328"></a>00328             <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00329"></a>00329             ::pointer data)
<a name="l00330"></a>00330   {
<a name="l00331"></a>00331     this-&gt;Clear();
<a name="l00332"></a>00332 
<a name="l00333"></a>00333     this-&gt;m_ = i;
<a name="l00334"></a>00334     this-&gt;n_ = j;
<a name="l00335"></a>00335 
<a name="l00336"></a>00336 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00337"></a>00337 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00338"></a>00338       {
<a name="l00339"></a>00339 <span class="preprocessor">#endif</span>
<a name="l00340"></a>00340 <span class="preprocessor"></span>
<a name="l00341"></a>00341         me_ = <span class="keyword">reinterpret_cast&lt;</span>pointer*<span class="keyword">&gt;</span>( calloc(Storage::GetFirst(i, j),
<a name="l00342"></a>00342                                                  <span class="keyword">sizeof</span>(pointer)) );
<a name="l00343"></a>00343 
<a name="l00344"></a>00344 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00345"></a>00345 <span class="preprocessor"></span>      }
<a name="l00346"></a>00346     <span class="keywordflow">catch</span> (...)
<a name="l00347"></a>00347       {
<a name="l00348"></a>00348         this-&gt;m_ = 0;
<a name="l00349"></a>00349         this-&gt;n_ = 0;
<a name="l00350"></a>00350         me_ = NULL;
<a name="l00351"></a>00351         this-&gt;data_ = NULL;
<a name="l00352"></a>00352         <span class="keywordflow">return</span>;
<a name="l00353"></a>00353       }
<a name="l00354"></a>00354     <span class="keywordflow">if</span> (me_ == NULL)
<a name="l00355"></a>00355       {
<a name="l00356"></a>00356         this-&gt;m_ = 0;
<a name="l00357"></a>00357         this-&gt;n_ = 0;
<a name="l00358"></a>00358         this-&gt;data_ = NULL;
<a name="l00359"></a>00359         <span class="keywordflow">return</span>;
<a name="l00360"></a>00360       }
<a name="l00361"></a>00361 <span class="preprocessor">#endif</span>
<a name="l00362"></a>00362 <span class="preprocessor"></span>
<a name="l00363"></a>00363     this-&gt;data_ = data;
<a name="l00364"></a>00364 
<a name="l00365"></a>00365     pointer ptr = this-&gt;data_;
<a name="l00366"></a>00366     <span class="keywordtype">int</span> lgth = Storage::GetSecond(i, j);
<a name="l00367"></a>00367     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; Storage::GetFirst(i, j); k++, ptr += lgth)
<a name="l00368"></a>00368       me_[k] = ptr;
<a name="l00369"></a>00369   }
<a name="l00370"></a>00370 
<a name="l00371"></a>00371 
<a name="l00373"></a>00373 
<a name="l00378"></a>00378   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00379"></a>00379   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2898e791b8d8e9b282bc36a59da5a0ec" title="Clears the matrix without releasing memory.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Nullify</a>()
<a name="l00380"></a>00380   {
<a name="l00381"></a>00381     this-&gt;m_ = 0;
<a name="l00382"></a>00382     this-&gt;n_ = 0;
<a name="l00383"></a>00383 
<a name="l00384"></a>00384 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00385"></a>00385 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00386"></a>00386       {
<a name="l00387"></a>00387 <span class="preprocessor">#endif</span>
<a name="l00388"></a>00388 <span class="preprocessor"></span>
<a name="l00389"></a>00389         <span class="keywordflow">if</span> (me_ != NULL)
<a name="l00390"></a>00390           {
<a name="l00391"></a>00391             free(me_);
<a name="l00392"></a>00392             me_ = NULL;
<a name="l00393"></a>00393           }
<a name="l00394"></a>00394 
<a name="l00395"></a>00395 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00396"></a>00396 <span class="preprocessor"></span>      }
<a name="l00397"></a>00397     <span class="keywordflow">catch</span> (...)
<a name="l00398"></a>00398       {
<a name="l00399"></a>00399         this-&gt;m_ = 0;
<a name="l00400"></a>00400         this-&gt;n_ = 0;
<a name="l00401"></a>00401         me_ = NULL;
<a name="l00402"></a>00402       }
<a name="l00403"></a>00403 <span class="preprocessor">#endif</span>
<a name="l00404"></a>00404 <span class="preprocessor"></span>
<a name="l00405"></a>00405     this-&gt;data_ = NULL;
<a name="l00406"></a>00406   }
<a name="l00407"></a>00407 
<a name="l00408"></a>00408 
<a name="l00410"></a>00410 
<a name="l00417"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec">00417</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00418"></a>00418   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec" title="Reallocates memory to resize the matrix and keeps previous entries.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00419"></a>00419 <a class="code" href="class_seldon_1_1_matrix___pointers.php#ae5cee7b46956ee19dbbb3308613378ec" title="Reallocates memory to resize the matrix and keeps previous entries.">  ::Resize</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00420"></a>00420   {
<a name="l00421"></a>00421 
<a name="l00422"></a>00422     <span class="comment">// Storing the old values of the matrix.</span>
<a name="l00423"></a>00423     <span class="keywordtype">int</span> iold = Storage::GetFirst(this-&gt;m_, this-&gt;n_);
<a name="l00424"></a>00424     <span class="keywordtype">int</span> jold = Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00425"></a>00425     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;value_type, VectFull, Allocator&gt;</a> xold(this-&gt;GetDataSize());
<a name="l00426"></a>00426     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; this-&gt;GetDataSize(); k++)
<a name="l00427"></a>00427       xold(k) = this-&gt;data_[k];
<a name="l00428"></a>00428 
<a name="l00429"></a>00429     <span class="comment">// Reallocation.</span>
<a name="l00430"></a>00430     <span class="keywordtype">int</span> inew = Storage::GetFirst(i, j);
<a name="l00431"></a>00431     <span class="keywordtype">int</span> jnew = Storage::GetSecond(i, j);
<a name="l00432"></a>00432     this-&gt;Reallocate(i,j);
<a name="l00433"></a>00433 
<a name="l00434"></a>00434     <span class="comment">// Filling the matrix with its old values.</span>
<a name="l00435"></a>00435     <span class="keywordtype">int</span> imin = min(iold, inew), jmin = min(jold, jnew);
<a name="l00436"></a>00436     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; imin; k++)
<a name="l00437"></a>00437       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> l = 0; l &lt; jmin; l++)
<a name="l00438"></a>00438         this-&gt;data_[k*jnew+l] = xold(l+jold*k);
<a name="l00439"></a>00439   }
<a name="l00440"></a>00440 
<a name="l00441"></a>00441 
<a name="l00442"></a>00442   <span class="comment">/**********************************</span>
<a name="l00443"></a>00443 <span class="comment">   * ELEMENT ACCESS AND AFFECTATION *</span>
<a name="l00444"></a>00444 <span class="comment">   **********************************/</span>
<a name="l00445"></a>00445 
<a name="l00446"></a>00446 
<a name="l00448"></a>00448 
<a name="l00454"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac">00454</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00455"></a>00455   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00456"></a>00456   <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00457"></a>00457   {
<a name="l00458"></a>00458 
<a name="l00459"></a>00459 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00460"></a>00460 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00461"></a>00461       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00462"></a>00462                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00463"></a>00463                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00464"></a>00464                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00465"></a>00465     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00466"></a>00466       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00467"></a>00467                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00468"></a>00468                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00469"></a>00469                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00470"></a>00470 <span class="preprocessor">#endif</span>
<a name="l00471"></a>00471 <span class="preprocessor"></span>
<a name="l00472"></a>00472     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00473"></a>00473   }
<a name="l00474"></a>00474 
<a name="l00475"></a>00475 
<a name="l00477"></a>00477 
<a name="l00483"></a>00483   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00484"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a1152a779c807df6022715b8635be7bf7">00484</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00485"></a>00485   ::const_reference <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00486"></a>00486 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac" title="Access operator.">  ::operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00487"></a>00487 <span class="keyword">  </span>{
<a name="l00488"></a>00488 
<a name="l00489"></a>00489 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00490"></a>00490 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00491"></a>00491       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00492"></a>00492                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00493"></a>00493                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00494"></a>00494                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00495"></a>00495     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00496"></a>00496       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator()&quot;</span>,
<a name="l00497"></a>00497                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00498"></a>00498                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00499"></a>00499                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00500"></a>00500 <span class="preprocessor">#endif</span>
<a name="l00501"></a>00501 <span class="preprocessor"></span>
<a name="l00502"></a>00502     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00503"></a>00503   }
<a name="l00504"></a>00504 
<a name="l00505"></a>00505 
<a name="l00507"></a>00507 
<a name="l00513"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e">00513</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00514"></a>00514   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00515"></a>00515   <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00516"></a>00516   {
<a name="l00517"></a>00517 
<a name="l00518"></a>00518 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00519"></a>00519 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00520"></a>00520       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int)&quot;</span>,
<a name="l00521"></a>00521                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00522"></a>00522                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00523"></a>00523     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00524"></a>00524       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int)&quot;</span>,
<a name="l00525"></a>00525                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00526"></a>00526                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00527"></a>00527 <span class="preprocessor">#endif</span>
<a name="l00528"></a>00528 <span class="preprocessor"></span>
<a name="l00529"></a>00529     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00530"></a>00530   }
<a name="l00531"></a>00531 
<a name="l00532"></a>00532 
<a name="l00534"></a>00534 
<a name="l00540"></a>00540   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00541"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a3d83977c9b496a6c1745a8e41f79af5c">00541</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00542"></a>00542   ::const_reference
<a name="l00543"></a>00543   <a class="code" href="class_seldon_1_1_matrix___pointers.php#afca09e0690510a4a40fe022e62677b6e" title="Access operator.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Val</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)<span class="keyword"> const</span>
<a name="l00544"></a>00544 <span class="keyword">  </span>{
<a name="l00545"></a>00545 
<a name="l00546"></a>00546 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00547"></a>00547 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;m_)
<a name="l00548"></a>00548       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_row.php">WrongRow</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int) const&quot;</span>,
<a name="l00549"></a>00549                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;m_-1)
<a name="l00550"></a>00550                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00551"></a>00551     <span class="keywordflow">if</span> (j &lt; 0 || j &gt;= this-&gt;n_)
<a name="l00552"></a>00552       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_col.php">WrongCol</a>(<span class="stringliteral">&quot;Matrix_Pointers::Val(int, int) const&quot;</span>,
<a name="l00553"></a>00553                      <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;n_-1)
<a name="l00554"></a>00554                      + <span class="stringliteral">&quot;], but is equal to &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00555"></a>00555 <span class="preprocessor">#endif</span>
<a name="l00556"></a>00556 <span class="preprocessor"></span>
<a name="l00557"></a>00557     <span class="keywordflow">return</span> me_[Storage::GetFirst(i, j)][Storage::GetSecond(i, j)];
<a name="l00558"></a>00558   }
<a name="l00559"></a>00559 
<a name="l00560"></a>00560 
<a name="l00562"></a>00562 
<a name="l00567"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255">00567</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00568"></a>00568   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::reference</a>
<a name="l00569"></a>00569   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255" title="Access to elements of the data array.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)
<a name="l00570"></a>00570   {
<a name="l00571"></a>00571 
<a name="l00572"></a>00572 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00573"></a>00573 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00574"></a>00574       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator[] (int)&quot;</span>,
<a name="l00575"></a>00575                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00576"></a>00576                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00577"></a>00577                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00578"></a>00578 <span class="preprocessor">#endif</span>
<a name="l00579"></a>00579 <span class="preprocessor"></span>
<a name="l00580"></a>00580     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00581"></a>00581   }
<a name="l00582"></a>00582 
<a name="l00583"></a>00583 
<a name="l00585"></a>00585 
<a name="l00590"></a>00590   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00591"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#af98cb1a88c5cd8675ce73ccff9f37db9">00591</a>   <span class="keyword">inline</span> <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00592"></a>00592   ::const_reference
<a name="l00593"></a>00593   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255" title="Access to elements of the data array.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator[] </a>(<span class="keywordtype">int</span> i)<span class="keyword"> const</span>
<a name="l00594"></a>00594 <span class="keyword">  </span>{
<a name="l00595"></a>00595 
<a name="l00596"></a>00596 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l00597"></a>00597 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (i &lt; 0 || i &gt;= this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>())
<a name="l00598"></a>00598       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_index.php">WrongIndex</a>(<span class="stringliteral">&quot;Matrix_Pointers::operator[] (int) const&quot;</span>,
<a name="l00599"></a>00599                        <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Index should be in [0, &quot;</span>)
<a name="l00600"></a>00600                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>()-1) + <span class="stringliteral">&quot;], but is equal to &quot;</span>
<a name="l00601"></a>00601                        + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot;.&quot;</span>);
<a name="l00602"></a>00602 <span class="preprocessor">#endif</span>
<a name="l00603"></a>00603 <span class="preprocessor"></span>
<a name="l00604"></a>00604     <span class="keywordflow">return</span> this-&gt;data_[i];
<a name="l00605"></a>00605   }
<a name="l00606"></a>00606 
<a name="l00607"></a>00607 
<a name="l00609"></a>00609 
<a name="l00614"></a>00614   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00615"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3">00615</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00616"></a>00616   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00617"></a>00617 <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00618"></a>00618   {
<a name="l00619"></a>00619     this-&gt;Copy(A);
<a name="l00620"></a>00620 
<a name="l00621"></a>00621     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00622"></a>00622   }
<a name="l00623"></a>00623 
<a name="l00624"></a>00624 
<a name="l00626"></a>00626 
<a name="l00631"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c">00631</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00632"></a>00632   <span class="keyword">inline</span> <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c" title="Duplicates a matrix.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00633"></a>00633 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a043929510eae4908750348b4862e190c" title="Duplicates a matrix.">  ::Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l00634"></a>00634   {
<a name="l00635"></a>00635     this-&gt;Reallocate(A.<a class="code" href="class_seldon_1_1_matrix___base.php#a65e9c3f0db9c7c8c3fa10ead777dd927" title="Returns the number of rows.">GetM</a>(), A.<a class="code" href="class_seldon_1_1_matrix___base.php#a6b134070d1b890ba6b7eb72fee170984" title="Returns the number of columns.">GetN</a>());
<a name="l00636"></a>00636 
<a name="l00637"></a>00637     this-&gt;allocator_.memorycpy(this-&gt;data_, A.<a class="code" href="class_seldon_1_1_matrix___base.php#a453a269dfe7fadba249064363d5ab92a" title="Returns a pointer to the data array.">GetData</a>(), this-&gt;GetDataSize());
<a name="l00638"></a>00638   }
<a name="l00639"></a>00639 
<a name="l00640"></a>00640 
<a name="l00641"></a>00641   <span class="comment">/************************</span>
<a name="l00642"></a>00642 <span class="comment">   * CONVENIENT FUNCTIONS *</span>
<a name="l00643"></a>00643 <span class="comment">   ************************/</span>
<a name="l00644"></a>00644 
<a name="l00645"></a>00645 
<a name="l00647"></a>00647 
<a name="l00650"></a>00650   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00651"></a>00651   <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#af93c5e73fc737a2f6e4f0b15164dd46d" title="Returns the leading dimension.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::GetLD</a>()<span class="keyword"> const</span>
<a name="l00652"></a>00652 <span class="keyword">  </span>{
<a name="l00653"></a>00653     <span class="keywordflow">return</span> Storage::GetSecond(this-&gt;m_, this-&gt;n_);
<a name="l00654"></a>00654   }
<a name="l00655"></a>00655 
<a name="l00656"></a>00656 
<a name="l00658"></a>00658 
<a name="l00662"></a>00662   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00663"></a>00663   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#add248e51552e9cec9a6214a8c1328ed0" title="Sets all elements to zero.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Zero</a>()
<a name="l00664"></a>00664   {
<a name="l00665"></a>00665     this-&gt;allocator_.memoryset(this-&gt;data_, <span class="keywordtype">char</span>(0),
<a name="l00666"></a>00666                                this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>() * <span class="keyword">sizeof</span>(value_type));
<a name="l00667"></a>00667   }
<a name="l00668"></a>00668 
<a name="l00669"></a>00669 
<a name="l00671"></a>00671   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00672"></a>00672   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a35fc7bad6fe6cf252737916dfafae0ff" title="Sets the current matrix to the identity.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::SetIdentity</a>()
<a name="l00673"></a>00673   {
<a name="l00674"></a>00674     <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Fill</a>(T(0));
<a name="l00675"></a>00675 
<a name="l00676"></a>00676     T one(1);
<a name="l00677"></a>00677     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; min(this-&gt;m_, this-&gt;n_); i++)
<a name="l00678"></a>00678       (*<span class="keyword">this</span>)(i,i) = one;
<a name="l00679"></a>00679   }
<a name="l00680"></a>00680 
<a name="l00681"></a>00681 
<a name="l00683"></a>00683 
<a name="l00687"></a>00687   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00688"></a>00688   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Fill</a>()
<a name="l00689"></a>00689   {
<a name="l00690"></a>00690     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00691"></a>00691       this-&gt;data_[i] = i;
<a name="l00692"></a>00692   }
<a name="l00693"></a>00693 
<a name="l00694"></a>00694 
<a name="l00696"></a>00696 
<a name="l00699"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#aff7bbc1161aeb1ffcf7894e4b21a270f">00699</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00700"></a>00700   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00701"></a>00701   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Fill</a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00702"></a>00702   {
<a name="l00703"></a>00703     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00704"></a>00704       this-&gt;data_[i] = x;
<a name="l00705"></a>00705   }
<a name="l00706"></a>00706 
<a name="l00707"></a>00707 
<a name="l00709"></a>00709 
<a name="l00712"></a>00712   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00713"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">00713</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00714"></a>00714   <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>&amp;
<a name="l00715"></a>00715   <a class="code" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3" title="Duplicates a matrix (assignment operator).">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l00716"></a>00716   {
<a name="l00717"></a>00717     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a2d3aeda0fa734518dd700f8568aaccde" title="Fills the matrix the matrix with 0, 1, 2, ...">Fill</a>(x);
<a name="l00718"></a>00718 
<a name="l00719"></a>00719     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l00720"></a>00720   }
<a name="l00721"></a>00721 
<a name="l00722"></a>00722 
<a name="l00724"></a>00724 
<a name="l00727"></a>00727   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00728"></a>00728   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a656390ed0505de78a077a6533586428e" title="Fills the matrix randomly.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::FillRand</a>()
<a name="l00729"></a>00729   {
<a name="l00730"></a>00730     srand(time(NULL));
<a name="l00731"></a>00731     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a36ac8e7e9bee92ae266201dfb60bf32e" title="Returns the number of elements stored in memory.">GetDataSize</a>(); i++)
<a name="l00732"></a>00732       this-&gt;data_[i] = rand();
<a name="l00733"></a>00733   }
<a name="l00734"></a>00734 
<a name="l00735"></a>00735 
<a name="l00737"></a>00737 
<a name="l00742"></a>00742   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00743"></a>00743   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>()<span class="keyword"> const</span>
<a name="l00744"></a>00744 <span class="keyword">  </span>{
<a name="l00745"></a>00745     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_; i++)
<a name="l00746"></a>00746       {
<a name="l00747"></a>00747         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; this-&gt;n_; j++)
<a name="l00748"></a>00748           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00749"></a>00749         cout &lt;&lt; endl;
<a name="l00750"></a>00750       }
<a name="l00751"></a>00751   }
<a name="l00752"></a>00752 
<a name="l00753"></a>00753 
<a name="l00755"></a>00755 
<a name="l00766"></a>00766   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00767"></a>00767   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> a, <span class="keywordtype">int</span> b,
<a name="l00768"></a>00768                                                            <span class="keywordtype">int</span> m, <span class="keywordtype">int</span> n)<span class="keyword"> const</span>
<a name="l00769"></a>00769 <span class="keyword">  </span>{
<a name="l00770"></a>00770     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = a; i &lt; min(this-&gt;m_, a+m); i++)
<a name="l00771"></a>00771       {
<a name="l00772"></a>00772         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = b; j &lt; min(this-&gt;n_, b+n); j++)
<a name="l00773"></a>00773           cout &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="stringliteral">&quot;\t&quot;</span>;
<a name="l00774"></a>00774         cout &lt;&lt; endl;
<a name="l00775"></a>00775       }
<a name="l00776"></a>00776   }
<a name="l00777"></a>00777 
<a name="l00778"></a>00778 
<a name="l00780"></a>00780 
<a name="l00788"></a>00788   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00789"></a>00789   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::Print</a>(<span class="keywordtype">int</span> l)<span class="keyword"> const</span>
<a name="l00790"></a>00790 <span class="keyword">  </span>{
<a name="l00791"></a>00791     <a class="code" href="class_seldon_1_1_matrix___pointers.php#a648346709f078652cb0a76f4001c901e" title="Displays the matrix on the standard output.">Print</a>(0, 0, l, l);
<a name="l00792"></a>00792   }
<a name="l00793"></a>00793 
<a name="l00794"></a>00794 
<a name="l00795"></a>00795   <span class="comment">/**************************</span>
<a name="l00796"></a>00796 <span class="comment">   * INPUT/OUTPUT FUNCTIONS *</span>
<a name="l00797"></a>00797 <span class="comment">   **************************/</span>
<a name="l00798"></a>00798 
<a name="l00799"></a>00799 
<a name="l00801"></a>00801 
<a name="l00810"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44">00810</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00811"></a>00811   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00812"></a>00812 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">  ::Write</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00813"></a>00813 <span class="keyword">  </span>{
<a name="l00814"></a>00814     ofstream FileStream;
<a name="l00815"></a>00815     FileStream.open(FileName.c_str());
<a name="l00816"></a>00816 
<a name="l00817"></a>00817 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00818"></a>00818 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00819"></a>00819     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00820"></a>00820       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(string FileName)&quot;</span>,
<a name="l00821"></a>00821                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00822"></a>00822 <span class="preprocessor">#endif</span>
<a name="l00823"></a>00823 <span class="preprocessor"></span>
<a name="l00824"></a>00824     this-&gt;Write(FileStream, with_size);
<a name="l00825"></a>00825 
<a name="l00826"></a>00826     FileStream.close();
<a name="l00827"></a>00827   }
<a name="l00828"></a>00828 
<a name="l00829"></a>00829 
<a name="l00831"></a>00831 
<a name="l00840"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a150457071742a4d3a4bfd503a0e10c29">00840</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00841"></a>00841   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00842"></a>00842 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a05252b9ce76816127365574721a97e44" title="Writes the matrix in a file.">  ::Write</a>(ostream&amp; FileStream, <span class="keywordtype">bool</span> with_size)<span class="keyword"> const</span>
<a name="l00843"></a>00843 <span class="keyword">  </span>{
<a name="l00844"></a>00844 
<a name="l00845"></a>00845 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00846"></a>00846 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00847"></a>00847     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00848"></a>00848       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00849"></a>00849                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00850"></a>00850 <span class="preprocessor">#endif</span>
<a name="l00851"></a>00851 <span class="preprocessor"></span>
<a name="l00852"></a>00852     <span class="keywordflow">if</span> (with_size)
<a name="l00853"></a>00853       {
<a name="l00854"></a>00854         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;m_)),
<a name="l00855"></a>00855                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00856"></a>00856         FileStream.write(reinterpret_cast&lt;char*&gt;(const_cast&lt;int*&gt;(&amp;this-&gt;n_)),
<a name="l00857"></a>00857                          <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00858"></a>00858       }
<a name="l00859"></a>00859 
<a name="l00860"></a>00860     FileStream.write(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l00861"></a>00861                      this-&gt;m_ * this-&gt;n_ * <span class="keyword">sizeof</span>(value_type));
<a name="l00862"></a>00862 
<a name="l00863"></a>00863 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00864"></a>00864 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00865"></a>00865     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00866"></a>00866       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Write(ostream&amp; FileStream)&quot;</span>,
<a name="l00867"></a>00867                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00868"></a>00868 <span class="preprocessor">#endif</span>
<a name="l00869"></a>00869 <span class="preprocessor"></span>
<a name="l00870"></a>00870   }
<a name="l00871"></a>00871 
<a name="l00872"></a>00872 
<a name="l00874"></a>00874 
<a name="l00881"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e">00881</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00882"></a>00882   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00883"></a>00883 <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">  ::WriteText</a>(<span class="keywordtype">string</span> FileName)<span class="keyword"> const</span>
<a name="l00884"></a>00884 <span class="keyword">  </span>{
<a name="l00885"></a>00885     ofstream FileStream;
<a name="l00886"></a>00886     FileStream.precision(cout.precision());
<a name="l00887"></a>00887     FileStream.flags(cout.flags());
<a name="l00888"></a>00888     FileStream.open(FileName.c_str());
<a name="l00889"></a>00889 
<a name="l00890"></a>00890 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00891"></a>00891 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00892"></a>00892     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00893"></a>00893       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(string FileName)&quot;</span>,
<a name="l00894"></a>00894                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00895"></a>00895 <span class="preprocessor">#endif</span>
<a name="l00896"></a>00896 <span class="preprocessor"></span>
<a name="l00897"></a>00897     this-&gt;WriteText(FileStream);
<a name="l00898"></a>00898 
<a name="l00899"></a>00899     FileStream.close();
<a name="l00900"></a>00900   }
<a name="l00901"></a>00901 
<a name="l00902"></a>00902 
<a name="l00904"></a>00904 
<a name="l00911"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#af1aa0dd65f43e9ab7fc47dddc38d2cd6">00911</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00912"></a>00912   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00913"></a>00913 <a class="code" href="class_seldon_1_1_matrix___pointers.php#aa1b41e8fe89480269acdc9b8cd36c21e" title="Writes the matrix in a file.">  ::WriteText</a>(ostream&amp; FileStream)<span class="keyword"> const</span>
<a name="l00914"></a>00914 <span class="keyword">  </span>{
<a name="l00915"></a>00915 
<a name="l00916"></a>00916 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00917"></a>00917 <span class="preprocessor"></span>    <span class="comment">// Checks if the file is ready.</span>
<a name="l00918"></a>00918     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00919"></a>00919       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00920"></a>00920                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00921"></a>00921 <span class="preprocessor">#endif</span>
<a name="l00922"></a>00922 <span class="preprocessor"></span>
<a name="l00923"></a>00923     <span class="keywordtype">int</span> i, j;
<a name="l00924"></a>00924     <span class="keywordflow">for</span> (i = 0; i &lt; this-&gt;GetM(); i++)
<a name="l00925"></a>00925       {
<a name="l00926"></a>00926         <span class="keywordflow">for</span> (j = 0; j &lt; this-&gt;GetN(); j++)
<a name="l00927"></a>00927           FileStream &lt;&lt; (*<span class="keyword">this</span>)(i, j) &lt;&lt; <span class="charliteral">&#39;\t&#39;</span>;
<a name="l00928"></a>00928         FileStream &lt;&lt; endl;
<a name="l00929"></a>00929       }
<a name="l00930"></a>00930 
<a name="l00931"></a>00931 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00932"></a>00932 <span class="preprocessor"></span>    <span class="comment">// Checks if data was written.</span>
<a name="l00933"></a>00933     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00934"></a>00934       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::WriteText(ostream&amp; FileStream)&quot;</span>,
<a name="l00935"></a>00935                     <span class="stringliteral">&quot;Output operation failed.&quot;</span>);
<a name="l00936"></a>00936 <span class="preprocessor">#endif</span>
<a name="l00937"></a>00937 <span class="preprocessor"></span>
<a name="l00938"></a>00938   }
<a name="l00939"></a>00939 
<a name="l00940"></a>00940 
<a name="l00942"></a>00942 
<a name="l00952"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6">00952</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00953"></a>00953   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00954"></a>00954 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">  ::Read</a>(<span class="keywordtype">string</span> FileName, <span class="keywordtype">bool</span> with_size)
<a name="l00955"></a>00955   {
<a name="l00956"></a>00956     ifstream FileStream;
<a name="l00957"></a>00957     FileStream.open(FileName.c_str());
<a name="l00958"></a>00958 
<a name="l00959"></a>00959 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00960"></a>00960 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l00961"></a>00961     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l00962"></a>00962       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(string FileName)&quot;</span>,
<a name="l00963"></a>00963                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l00964"></a>00964 <span class="preprocessor">#endif</span>
<a name="l00965"></a>00965 <span class="preprocessor"></span>
<a name="l00966"></a>00966     this-&gt;Read(FileStream, with_size);
<a name="l00967"></a>00967 
<a name="l00968"></a>00968     FileStream.close();
<a name="l00969"></a>00969   }
<a name="l00970"></a>00970 
<a name="l00971"></a>00971 
<a name="l00973"></a>00973 
<a name="l00983"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a697168859e965ebfe3e0c722544217de">00983</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l00984"></a>00984   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l00985"></a>00985 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a45b2cbff13b8da2a247def9e87e910a6" title="Reads the matrix from a file.">  ::Read</a>(istream&amp; FileStream, <span class="keywordtype">bool</span> with_size)
<a name="l00986"></a>00986   {
<a name="l00987"></a>00987 
<a name="l00988"></a>00988 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l00989"></a>00989 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l00990"></a>00990     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l00991"></a>00991       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(istream&amp; FileStream)&quot;</span>,
<a name="l00992"></a>00992                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l00993"></a>00993 <span class="preprocessor">#endif</span>
<a name="l00994"></a>00994 <span class="preprocessor"></span>
<a name="l00995"></a>00995     <span class="keywordflow">if</span> (with_size)
<a name="l00996"></a>00996       {
<a name="l00997"></a>00997         <span class="keywordtype">int</span> new_m, new_n;
<a name="l00998"></a>00998         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_m), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l00999"></a>00999         FileStream.read(reinterpret_cast&lt;char*&gt;(&amp;new_n), <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>));
<a name="l01000"></a>01000         this-&gt;Reallocate(new_m, new_n);
<a name="l01001"></a>01001       }
<a name="l01002"></a>01002 
<a name="l01003"></a>01003     FileStream.read(reinterpret_cast&lt;char*&gt;(this-&gt;data_),
<a name="l01004"></a>01004                     this-&gt;GetM() * this-&gt;GetN() * <span class="keyword">sizeof</span>(value_type));
<a name="l01005"></a>01005 
<a name="l01006"></a>01006 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01007"></a>01007 <span class="preprocessor"></span>    <span class="comment">// Checks if data was read.</span>
<a name="l01008"></a>01008     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01009"></a>01009       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::Read(istream&amp; FileStream)&quot;</span>,
<a name="l01010"></a>01010                     <span class="stringliteral">&quot;Input operation failed.&quot;</span>);
<a name="l01011"></a>01011 <span class="preprocessor">#endif</span>
<a name="l01012"></a>01012 <span class="preprocessor"></span>
<a name="l01013"></a>01013   }
<a name="l01014"></a>01014 
<a name="l01015"></a>01015 
<a name="l01017"></a>01017 
<a name="l01021"></a>01021   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01022"></a>01022   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;::ReadText</a>(<span class="keywordtype">string</span> FileName)
<a name="l01023"></a>01023   {
<a name="l01024"></a>01024     ifstream FileStream;
<a name="l01025"></a>01025     FileStream.open(FileName.c_str());
<a name="l01026"></a>01026 
<a name="l01027"></a>01027 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01028"></a>01028 <span class="preprocessor"></span>    <span class="comment">// Checks if the file was opened.</span>
<a name="l01029"></a>01029     <span class="keywordflow">if</span> (!FileStream.is_open())
<a name="l01030"></a>01030       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(string FileName)&quot;</span>,
<a name="l01031"></a>01031                     <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Unable to open file \&quot;&quot;</span>) + FileName + <span class="stringliteral">&quot;\&quot;.&quot;</span>);
<a name="l01032"></a>01032 <span class="preprocessor">#endif</span>
<a name="l01033"></a>01033 <span class="preprocessor"></span>
<a name="l01034"></a>01034     this-&gt;<a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">ReadText</a>(FileStream);
<a name="l01035"></a>01035 
<a name="l01036"></a>01036     FileStream.close();
<a name="l01037"></a>01037   }
<a name="l01038"></a>01038 
<a name="l01039"></a>01039 
<a name="l01041"></a>01041 
<a name="l01045"></a><a class="code" href="class_seldon_1_1_matrix___pointers.php#a96dc6a908187241ae7fea78ed2f12e93">01045</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01046"></a>01046   <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">Matrix_Pointers&lt;T, Prop, Storage, Allocator&gt;</a>
<a name="l01047"></a>01047 <a class="code" href="class_seldon_1_1_matrix___pointers.php#a2ec5770914f507080dac5452b8ddaee5" title="Reads the matrix from a file.">  ::ReadText</a>(istream&amp; FileStream)
<a name="l01048"></a>01048   {
<a name="l01049"></a>01049     <span class="comment">// Clears the matrix.</span>
<a name="l01050"></a>01050     Clear();
<a name="l01051"></a>01051 
<a name="l01052"></a>01052 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01053"></a>01053 <span class="preprocessor"></span>    <span class="comment">// Checks if the stream is ready.</span>
<a name="l01054"></a>01054     <span class="keywordflow">if</span> (!FileStream.good())
<a name="l01055"></a>01055       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l01056"></a>01056                     <span class="stringliteral">&quot;The stream is not ready.&quot;</span>);
<a name="l01057"></a>01057 <span class="preprocessor">#endif</span>
<a name="l01058"></a>01058 <span class="preprocessor"></span>
<a name="l01059"></a>01059     <span class="comment">// Reads the first line.</span>
<a name="l01060"></a>01060     <span class="keywordtype">string</span> line;
<a name="l01061"></a>01061     getline(FileStream, line);
<a name="l01062"></a>01062     <span class="keywordflow">if</span> (FileStream.fail())
<a name="l01063"></a>01063       <span class="comment">// Is the file empty?</span>
<a name="l01064"></a>01064       <span class="keywordflow">return</span>;
<a name="l01065"></a>01065 
<a name="l01066"></a>01066     <span class="comment">// Converts the first line into a vector.</span>
<a name="l01067"></a>01067     istringstream line_stream(line);
<a name="l01068"></a>01068     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> first_row;
<a name="l01069"></a>01069     first_row.ReadText(line_stream);
<a name="l01070"></a>01070 
<a name="l01071"></a>01071     <span class="comment">// Now reads all other rows, and puts them in a single vector.</span>
<a name="l01072"></a>01072     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T&gt;</a> other_row;
<a name="l01073"></a>01073     other_row.ReadText(FileStream);
<a name="l01074"></a>01074 
<a name="l01075"></a>01075     <span class="comment">// Number of rows and columns.</span>
<a name="l01076"></a>01076     <span class="keywordtype">int</span> n = first_row.GetM();
<a name="l01077"></a>01077     <span class="keywordtype">int</span> m = 1 + other_row.GetM() / n;
<a name="l01078"></a>01078 
<a name="l01079"></a>01079 <span class="preprocessor">#ifdef SELDON_CHECK_IO</span>
<a name="l01080"></a>01080 <span class="preprocessor"></span>    <span class="comment">// Checks that enough elements were read.</span>
<a name="l01081"></a>01081     <span class="keywordflow">if</span> (other_row.GetM() != (m - 1) * n)
<a name="l01082"></a>01082       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_i_o_error.php">IOError</a>(<span class="stringliteral">&quot;Matrix_Pointers::ReadText(istream&amp; FileStream)&quot;</span>,
<a name="l01083"></a>01083                     <span class="stringliteral">&quot;Not all rows have the same number of columns.&quot;</span>);
<a name="l01084"></a>01084 <span class="preprocessor">#endif</span>
<a name="l01085"></a>01085 <span class="preprocessor"></span>
<a name="l01086"></a>01086     this-&gt;Reallocate(m, n);
<a name="l01087"></a>01087     <span class="comment">// Fills the matrix.</span>
<a name="l01088"></a>01088     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01089"></a>01089       this-&gt;Val(0, j) = first_row(j);
<a name="l01090"></a>01090 
<a name="l01091"></a>01091     <span class="keywordtype">int</span> k = 0;
<a name="l01092"></a>01092     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 1; i &lt; m; i++)
<a name="l01093"></a>01093       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01094"></a>01094         this-&gt;Val(i, j) = other_row(k++);
<a name="l01095"></a>01095   }
<a name="l01096"></a>01096 
<a name="l01097"></a>01097 
<a name="l01099"></a>01099   <span class="comment">// MATRIX&lt;COLMAJOR&gt; //</span>
<a name="l01101"></a>01101 <span class="comment"></span>
<a name="l01102"></a>01102 
<a name="l01103"></a>01103   <span class="comment">/****************</span>
<a name="l01104"></a>01104 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01105"></a>01105 <span class="comment">   ****************/</span>
<a name="l01106"></a>01106 
<a name="l01107"></a>01107 
<a name="l01109"></a>01109 
<a name="l01112"></a>01112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01113"></a>01113   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::Matrix</a>()  throw():
<a name="l01114"></a>01114     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator&gt;()
<a name="l01115"></a>01115   {
<a name="l01116"></a>01116   }
<a name="l01117"></a>01117 
<a name="l01118"></a>01118 
<a name="l01120"></a>01120 
<a name="l01124"></a>01124   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01125"></a>01125   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#aa20ef1ed118ce455350561b4e7446bbb" title="Default constructor.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01126"></a>01126     Matrix_Pointers&lt;T, Prop, ColMajor, Allocator&gt;(i, j)
<a name="l01127"></a>01127   {
<a name="l01128"></a>01128   }
<a name="l01129"></a>01129 
<a name="l01130"></a>01130 
<a name="l01132"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a20c877edd580054313c623edfdaf8837">01132</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01133"></a>01133   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>
<a name="l01134"></a>01134 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp; A):
<a name="l01135"></a>01135     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_col_major.php">ColMajor</a>, Allocator&gt;(A)
<a name="l01136"></a>01136   {
<a name="l01137"></a>01137   }
<a name="l01138"></a>01138 
<a name="l01139"></a>01139 
<a name="l01140"></a>01140   <span class="comment">/*****************</span>
<a name="l01141"></a>01141 <span class="comment">   * OTHER METHODS *</span>
<a name="l01142"></a>01142 <span class="comment">   *****************/</span>
<a name="l01143"></a>01143 
<a name="l01144"></a>01144 
<a name="l01146"></a>01146 
<a name="l01149"></a>01149   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01150"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a5f16442782b3155c49889aee1a6df7e1">01150</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01151"></a>01151   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01152"></a>01152   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01153"></a>01153   {
<a name="l01154"></a>01154     this-&gt;Fill(x);
<a name="l01155"></a>01155 
<a name="l01156"></a>01156     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01157"></a>01157   }
<a name="l01158"></a>01158 
<a name="l01159"></a>01159 
<a name="l01161"></a>01161 
<a name="l01166"></a>01166   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01167"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a71b5c6a90688dd19e45d2b6f59369f14">01167</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01168"></a>01168   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>
<a name="l01169"></a>01169 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp; A)
<a name="l01170"></a>01170   {
<a name="l01171"></a>01171     this-&gt;Copy(A);
<a name="l01172"></a>01172 
<a name="l01173"></a>01173     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01174"></a>01174   }
<a name="l01175"></a>01175 
<a name="l01176"></a>01176 
<a name="l01178"></a>01178 
<a name="l01181"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#ac3a6b15a4c8e247460385cb9b924e9e9">01181</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l01182"></a>01182   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php" title="Column-major full-matrix class.">Matrix&lt;T, Prop, ColMajor, Allocator&gt;</a>&amp;
<a name="l01183"></a>01183   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, ColMajor, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l01184"></a>01184   {
<a name="l01185"></a>01185     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_ * this-&gt;n_; i++)
<a name="l01186"></a>01186       this-&gt;data_[i] *= alpha;
<a name="l01187"></a>01187 
<a name="l01188"></a>01188     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01189"></a>01189   }
<a name="l01190"></a>01190 
<a name="l01191"></a>01191 
<a name="l01193"></a>01193   <span class="comment">// MATRIX&lt;ROWMAJOR&gt; //</span>
<a name="l01195"></a>01195 <span class="comment"></span>
<a name="l01196"></a>01196 
<a name="l01197"></a>01197   <span class="comment">/****************</span>
<a name="l01198"></a>01198 <span class="comment">   * CONSTRUCTORS *</span>
<a name="l01199"></a>01199 <span class="comment">   ****************/</span>
<a name="l01200"></a>01200 
<a name="l01201"></a>01201 
<a name="l01203"></a>01203 
<a name="l01206"></a>01206   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01207"></a>01207   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::Matrix</a>()  throw():
<a name="l01208"></a>01208     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator&gt;()
<a name="l01209"></a>01209   {
<a name="l01210"></a>01210   }
<a name="l01211"></a>01211 
<a name="l01212"></a>01212 
<a name="l01214"></a>01214 
<a name="l01218"></a>01218   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01219"></a>01219   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#ad6540bf8d5f0d58e12915162e1d6b852" title="Default constructor.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::Matrix</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j):
<a name="l01220"></a>01220     Matrix_Pointers&lt;T, Prop, RowMajor, Allocator&gt;(i, j)
<a name="l01221"></a>01221   {
<a name="l01222"></a>01222   }
<a name="l01223"></a>01223 
<a name="l01224"></a>01224 
<a name="l01226"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a4951a370ce9457e8625cc5257a349f96">01226</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01227"></a>01227   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>
<a name="l01228"></a>01228 <a class="code" href="class_seldon_1_1_matrix.php">  ::Matrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp; A):
<a name="l01229"></a>01229     <a class="code" href="class_seldon_1_1_matrix___pointers.php" title="Full matrix class.">Matrix_Pointers</a>&lt;T, Prop, <a class="code" href="class_seldon_1_1_row_major.php">RowMajor</a>, Allocator&gt;(A)
<a name="l01230"></a>01230   {
<a name="l01231"></a>01231   }
<a name="l01232"></a>01232 
<a name="l01233"></a>01233 
<a name="l01234"></a>01234   <span class="comment">/*****************</span>
<a name="l01235"></a>01235 <span class="comment">   * OTHER METHODS *</span>
<a name="l01236"></a>01236 <span class="comment">   *****************/</span>
<a name="l01237"></a>01237 
<a name="l01238"></a>01238 
<a name="l01240"></a>01240 
<a name="l01243"></a>01243   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01244"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a3727350f7283b6e8e08ccb2f6733362b">01244</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l01245"></a>01245   <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01246"></a>01246   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::operator= </a>(<span class="keyword">const</span> T0&amp; x)
<a name="l01247"></a>01247   {
<a name="l01248"></a>01248     this-&gt;Fill(x);
<a name="l01249"></a>01249 
<a name="l01250"></a>01250     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01251"></a>01251   }
<a name="l01252"></a>01252 
<a name="l01253"></a>01253 
<a name="l01255"></a>01255 
<a name="l01260"></a>01260   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt;
<a name="l01261"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a11d3b562dc6263cad3299de5cbff269a">01261</a>   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01262"></a>01262   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>
<a name="l01263"></a>01263 <a class="code" href="class_seldon_1_1_matrix.php">  ::operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp; A)
<a name="l01264"></a>01264   {
<a name="l01265"></a>01265     this-&gt;Copy(A);
<a name="l01266"></a>01266 
<a name="l01267"></a>01267     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01268"></a>01268   }
<a name="l01269"></a>01269 
<a name="l01270"></a>01270 
<a name="l01272"></a>01272 
<a name="l01275"></a><a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a1f8c93e3abfe109f258ab5b31e6382e4">01275</a>   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Allocator&gt; <span class="keyword">template</span>&lt;<span class="keyword">class</span> T0&gt;
<a name="l01276"></a>01276   <span class="keyword">inline</span> <a class="code" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php" title="Row-major full-matrix class.">Matrix&lt;T, Prop, RowMajor, Allocator&gt;</a>&amp;
<a name="l01277"></a>01277   <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, RowMajor, Allocator&gt;::operator*= </a>(<span class="keyword">const</span> T0&amp; alpha)
<a name="l01278"></a>01278   {
<a name="l01279"></a>01279     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; this-&gt;m_*this-&gt;n_; i++)
<a name="l01280"></a>01280       this-&gt;data_[i] *= alpha;
<a name="l01281"></a>01281 
<a name="l01282"></a>01282     <span class="keywordflow">return</span> *<span class="keyword">this</span>;
<a name="l01283"></a>01283   }
<a name="l01284"></a>01284 
<a name="l01285"></a>01285 
<a name="l01286"></a>01286 } <span class="comment">// namespace Seldon.</span>
<a name="l01287"></a>01287 
<a name="l01288"></a>01288 <span class="preprocessor">#define SELDON_FILE_MATRIX_POINTERS_CXX</span>
<a name="l01289"></a>01289 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
