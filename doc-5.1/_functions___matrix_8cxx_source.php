<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>computation/basic_functions/Functions_Matrix.cxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">// Copyright (C) 2003-2009 Marc Duruflé</span>
<a name="l00003"></a>00003 <span class="comment">// Copyright (C) 2010 INRIA</span>
<a name="l00004"></a>00004 <span class="comment">// Author(s): Marc Fragu</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00007"></a>00007 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00008"></a>00008 <span class="comment">//</span>
<a name="l00009"></a>00009 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00010"></a>00010 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00011"></a>00011 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00012"></a>00012 <span class="comment">// any later version.</span>
<a name="l00013"></a>00013 <span class="comment">//</span>
<a name="l00014"></a>00014 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00015"></a>00015 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00016"></a>00016 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00017"></a>00017 <span class="comment">// more details.</span>
<a name="l00018"></a>00018 <span class="comment">//</span>
<a name="l00019"></a>00019 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00020"></a>00020 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_FUNCTIONS_MATRIX_CXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="comment">/*</span>
<a name="l00026"></a>00026 <span class="comment">  Function defined in this file:</span>
<a name="l00027"></a>00027 <span class="comment"></span>
<a name="l00028"></a>00028 <span class="comment">  alpha A -&gt; A</span>
<a name="l00029"></a>00029 <span class="comment">  Mlt(alpha, A)</span>
<a name="l00030"></a>00030 <span class="comment"></span>
<a name="l00031"></a>00031 <span class="comment">  A B -&gt; C</span>
<a name="l00032"></a>00032 <span class="comment">  Mlt(A, B, C)</span>
<a name="l00033"></a>00033 <span class="comment"></span>
<a name="l00034"></a>00034 <span class="comment">  alpha A B -&gt; C</span>
<a name="l00035"></a>00035 <span class="comment">  Mlt(alpha, A, B, C)</span>
<a name="l00036"></a>00036 <span class="comment"></span>
<a name="l00037"></a>00037 <span class="comment">  alpha A B + beta C -&gt; C</span>
<a name="l00038"></a>00038 <span class="comment">  MltAdd(alpha, A, B, beta, C)</span>
<a name="l00039"></a>00039 <span class="comment"></span>
<a name="l00040"></a>00040 <span class="comment">  alpha A + B -&gt; B</span>
<a name="l00041"></a>00041 <span class="comment">  Add(alpha, A, B)</span>
<a name="l00042"></a>00042 <span class="comment"></span>
<a name="l00043"></a>00043 <span class="comment">  LU factorization of matrix A without pivoting.</span>
<a name="l00044"></a>00044 <span class="comment">  GetLU(A)</span>
<a name="l00045"></a>00045 <span class="comment"></span>
<a name="l00046"></a>00046 <span class="comment">  Highest absolute value of A.</span>
<a name="l00047"></a>00047 <span class="comment">  MaxAbs(A)</span>
<a name="l00048"></a>00048 <span class="comment"></span>
<a name="l00049"></a>00049 <span class="comment">  1-norm of matrix A.</span>
<a name="l00050"></a>00050 <span class="comment">  Norm1(A)</span>
<a name="l00051"></a>00051 <span class="comment"></span>
<a name="l00052"></a>00052 <span class="comment">  infinity norm of matrix A.</span>
<a name="l00053"></a>00053 <span class="comment">  NormInf(A)</span>
<a name="l00054"></a>00054 <span class="comment"></span>
<a name="l00055"></a>00055 <span class="comment">  Transpose(A)</span>
<a name="l00056"></a>00056 <span class="comment">*/</span>
<a name="l00057"></a>00057 
<a name="l00058"></a>00058 <span class="keyword">namespace </span>Seldon
<a name="l00059"></a>00059 {
<a name="l00060"></a>00060 
<a name="l00061"></a>00061 
<a name="l00063"></a>00063   <span class="comment">// MLT //</span>
<a name="l00064"></a>00064 
<a name="l00065"></a>00065 
<a name="l00067"></a>00067 
<a name="l00071"></a>00071   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00072"></a>00072             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00073"></a><a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab">00073</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00074"></a>00074            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; A)  <span class="keywordflow">throw</span>()
<a name="l00075"></a>00075   {
<a name="l00076"></a>00076     T1 alpha_ = alpha;
<a name="l00077"></a>00077 
<a name="l00078"></a>00078     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;::pointer</a>
<a name="l00079"></a>00079       data = A.GetData();
<a name="l00080"></a>00080 
<a name="l00081"></a>00081     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetDataSize(); i++)
<a name="l00082"></a>00082       data[i] = alpha_ * data[i];
<a name="l00083"></a>00083   }
<a name="l00084"></a>00084 
<a name="l00085"></a>00085 
<a name="l00087"></a>00087 
<a name="l00091"></a>00091   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00092"></a>00092             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00093"></a><a class="code" href="namespace_seldon.php#a51fe021c252546e3e297e57276964ee2">00093</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00094"></a>00094            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;</a>&amp; A)
<a name="l00095"></a>00095   {
<a name="l00096"></a>00096     <span class="keyword">typename</span> T1::value_type alpha_ = alpha;
<a name="l00097"></a>00097     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetMmatrix(); i++)
<a name="l00098"></a>00098       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetNmatrix(); j++)
<a name="l00099"></a>00099         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, A.GetMatrix(i, j));
<a name="l00100"></a>00100   }
<a name="l00101"></a>00101 
<a name="l00102"></a>00102 
<a name="l00104"></a>00104 
<a name="l00108"></a>00108   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00109"></a>00109             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1&gt;
<a name="l00110"></a><a class="code" href="namespace_seldon.php#a401b2e81b73b44771dd985953a2899fc">00110</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00111"></a>00111            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;</a>&amp; A)
<a name="l00112"></a>00112   {
<a name="l00113"></a>00113     <span class="keyword">typename</span> T1::value_type alpha_ = alpha;
<a name="l00114"></a>00114     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetMmatrix(); i++)
<a name="l00115"></a>00115       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetNmatrix(); j++)
<a name="l00116"></a>00116         <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha_, A.GetMatrix(i, j));
<a name="l00117"></a>00117   }
<a name="l00118"></a>00118 
<a name="l00119"></a>00119 
<a name="l00121"></a>00121 
<a name="l00125"></a>00125   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator&gt;
<a name="l00126"></a><a class="code" href="namespace_seldon.php#a25de211299ecbb37d186d3d33a5cc133">00126</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00127"></a>00127            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&gt;</a>&amp; A)
<a name="l00128"></a>00128   {
<a name="l00129"></a>00129     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&gt;</a>
<a name="l00130"></a>00130       ::float_dense_m m0;
<a name="l00131"></a>00131     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&gt;</a>
<a name="l00132"></a>00132       ::float_sparse_m m1;
<a name="l00133"></a>00133     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&gt;</a>
<a name="l00134"></a>00134       ::double_dense_m m2;
<a name="l00135"></a>00135     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator&gt;</a>
<a name="l00136"></a>00136       ::double_sparse_m m3;
<a name="l00137"></a>00137 
<a name="l00138"></a>00138     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetMmatrix(); i++)
<a name="l00139"></a>00139       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetNmatrix(); j++)
<a name="l00140"></a>00140         {
<a name="l00141"></a>00141           <span class="keywordflow">switch</span> (A.GetType(i, j))
<a name="l00142"></a>00142             {
<a name="l00143"></a>00143             <span class="keywordflow">case</span> 0:
<a name="l00144"></a>00144               A.GetMatrix(i, j, m0);
<a name="l00145"></a>00145               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keywordtype">float</span>(alpha), m0);
<a name="l00146"></a>00146               A.SetMatrix(i, j, m0);
<a name="l00147"></a>00147               m0.Nullify();
<a name="l00148"></a>00148               <span class="keywordflow">break</span>;
<a name="l00149"></a>00149             <span class="keywordflow">case</span> 1:
<a name="l00150"></a>00150               A.GetMatrix(i, j, m1);
<a name="l00151"></a>00151               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keywordtype">float</span>(alpha), m1);
<a name="l00152"></a>00152               A.SetMatrix(i, j, m1);
<a name="l00153"></a>00153               m1.Nullify();
<a name="l00154"></a>00154               <span class="keywordflow">break</span>;
<a name="l00155"></a>00155             <span class="keywordflow">case</span> 2:
<a name="l00156"></a>00156               A.GetMatrix(i, j, m2);
<a name="l00157"></a>00157               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keywordtype">double</span>(alpha), m2);
<a name="l00158"></a>00158               A.SetMatrix(i, j, m2);
<a name="l00159"></a>00159               m2.Nullify();
<a name="l00160"></a>00160               <span class="keywordflow">break</span>;
<a name="l00161"></a>00161             <span class="keywordflow">case</span> 3:
<a name="l00162"></a>00162               A.GetMatrix(i, j, m3);
<a name="l00163"></a>00163               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keywordtype">double</span>(alpha), m3);
<a name="l00164"></a>00164               A.SetMatrix(i, j, m3);
<a name="l00165"></a>00165               m3.Nullify();
<a name="l00166"></a>00166               <span class="keywordflow">break</span>;
<a name="l00167"></a>00167             <span class="keywordflow">default</span>:
<a name="l00168"></a>00168               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Mlt(alpha, Matrix&lt;FloatDouble, &quot;</span>
<a name="l00169"></a>00169                                   <span class="stringliteral">&quot;DenseSparseCollection)&quot;</span>,
<a name="l00170"></a>00170                                   <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00171"></a>00171                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00172"></a>00172             }
<a name="l00173"></a>00173         }
<a name="l00174"></a>00174   }
<a name="l00175"></a>00175 
<a name="l00176"></a>00176 
<a name="l00178"></a>00178 
<a name="l00186"></a>00186   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00187"></a>00187             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00188"></a>00188             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00189"></a>00189             <span class="keyword">class </span>T3, <span class="keyword">class </span>Prop3, <span class="keyword">class </span>Storage3, <span class="keyword">class </span>Allocator3&gt;
<a name="l00190"></a><a class="code" href="namespace_seldon.php#abc3136bfd007c68e1ff005a9efac7953">00190</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00191"></a>00191            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; A,
<a name="l00192"></a>00192            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; B,
<a name="l00193"></a>00193            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T3, Prop3, Storage3, Allocator3&gt;</a>&amp; C)
<a name="l00194"></a>00194   {
<a name="l00195"></a>00195     C.Fill(T3(0));
<a name="l00196"></a>00196     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, T3(0), C);
<a name="l00197"></a>00197   }
<a name="l00198"></a>00198 
<a name="l00199"></a>00199 
<a name="l00201"></a>00201 
<a name="l00207"></a>00207   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l00208"></a>00208             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00209"></a>00209             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00210"></a><a class="code" href="namespace_seldon.php#a1fab5465a02b7912de8298553598a545">00210</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l00211"></a>00211            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l00212"></a>00212            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; C)
<a name="l00213"></a>00213   {
<a name="l00214"></a>00214     C.Fill(T2(0));
<a name="l00215"></a>00215     <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(T0(1), A, B, T2(0), C);
<a name="l00216"></a>00216   }
<a name="l00217"></a>00217 
<a name="l00218"></a>00218 
<a name="l00220"></a>00220 
<a name="l00228"></a>00228   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00229"></a>00229             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00230"></a>00230             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00231"></a><a class="code" href="namespace_seldon.php#a5ea3b74707d2aff1234f7a486f34e871">00231</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSparse, Allocator0&gt;</a>&amp; A,
<a name="l00232"></a>00232            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; B,
<a name="l00233"></a>00233            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; C)
<a name="l00234"></a>00234   {
<a name="l00235"></a>00235 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00236"></a>00236 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(A, B, <span class="stringliteral">&quot;Mlt(const Matrix&lt;RowSparse&gt;&amp; A, const &quot;</span>
<a name="l00237"></a>00237              <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B, Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>);
<a name="l00238"></a>00238 <span class="preprocessor">#endif</span>
<a name="l00239"></a>00239 <span class="preprocessor"></span>
<a name="l00240"></a>00240     <span class="keywordtype">int</span> h, i, k, l, col;
<a name="l00241"></a>00241     <span class="keywordtype">int</span> Nnonzero, Nnonzero_row, Nnonzero_row_max;
<a name="l00242"></a>00242     <a class="code" href="class_seldon_1_1_vector.php">IVect</a> column_index;
<a name="l00243"></a>00243     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2&gt;</a> row_value;
<a name="l00244"></a>00244     T1 value;
<a name="l00245"></a>00245     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00246"></a>00246 
<a name="l00247"></a>00247     <span class="keywordtype">int</span>* c_ptr = NULL;
<a name="l00248"></a>00248     <span class="keywordtype">int</span>* c_ind = NULL;
<a name="l00249"></a>00249     T2* c_data = NULL;
<a name="l00250"></a>00250     C.Clear();
<a name="l00251"></a>00251 
<a name="l00252"></a>00252 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00253"></a>00253 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00254"></a>00254       {
<a name="l00255"></a>00255 <span class="preprocessor">#endif</span>
<a name="l00256"></a>00256 <span class="preprocessor"></span>
<a name="l00257"></a>00257         c_ptr = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(calloc(m + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00258"></a>00258 
<a name="l00259"></a>00259 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00260"></a>00260 <span class="preprocessor"></span>      }
<a name="l00261"></a>00261     <span class="keywordflow">catch</span> (...)
<a name="l00262"></a>00262       {
<a name="l00263"></a>00263         c_ptr = NULL;
<a name="l00264"></a>00264       }
<a name="l00265"></a>00265 
<a name="l00266"></a>00266     <span class="keywordflow">if</span> (c_ptr == NULL)
<a name="l00267"></a>00267       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Mlt(const Matrix&lt;RowSparse&gt;&amp; A, const &quot;</span>
<a name="l00268"></a>00268                      <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B, Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l00269"></a>00269                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00270"></a>00270                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(m + 1) + <span class="stringliteral">&quot; integers.&quot;</span>);
<a name="l00271"></a>00271 <span class="preprocessor">#endif</span>
<a name="l00272"></a>00272 <span class="preprocessor"></span>
<a name="l00273"></a>00273     c_ptr[0] = 0;
<a name="l00274"></a>00274 
<a name="l00275"></a>00275     <span class="comment">// Number of non-zero elements in C.</span>
<a name="l00276"></a>00276     Nnonzero = 0;
<a name="l00277"></a>00277     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00278"></a>00278       {
<a name="l00279"></a>00279         c_ptr[i + 1] = c_ptr[i];
<a name="l00280"></a>00280 
<a name="l00281"></a>00281         <span class="keywordflow">if</span> (A.GetPtr()[i + 1] != A.GetPtr()[i])
<a name="l00282"></a>00282           <span class="comment">// There are elements in the i-th row of A, so there can be non-zero</span>
<a name="l00283"></a>00283           <span class="comment">// entries in C as well. Checks whether any column in B has an</span>
<a name="l00284"></a>00284           <span class="comment">// element whose row index matches a column index of a non-zero in</span>
<a name="l00285"></a>00285           <span class="comment">// the i-th row of A.</span>
<a name="l00286"></a>00286           {
<a name="l00287"></a>00287             <span class="comment">// Maximum number of non-zero entry on the i-th row of C.</span>
<a name="l00288"></a>00288             Nnonzero_row_max = 0;
<a name="l00289"></a>00289             <span class="comment">// For every element in the i-th row.</span>
<a name="l00290"></a>00290             <span class="keywordflow">for</span> (k = A.GetPtr()[i]; k &lt; A.GetPtr()[i + 1]; k++)
<a name="l00291"></a>00291               {
<a name="l00292"></a>00292                 col = A.GetInd()[k];
<a name="l00293"></a>00293                 Nnonzero_row_max += B.GetPtr()[col + 1] - B.GetPtr()[col];
<a name="l00294"></a>00294               }
<a name="l00295"></a>00295             <span class="comment">// Now gets the column indexes.</span>
<a name="l00296"></a>00296             column_index.Reallocate(Nnonzero_row_max);
<a name="l00297"></a>00297             row_value.Reallocate(Nnonzero_row_max);
<a name="l00298"></a>00298             h = 0;
<a name="l00299"></a>00299             <span class="comment">// For every element in the i-th row.</span>
<a name="l00300"></a>00300             <span class="keywordflow">for</span> (k = A.GetPtr()[i]; k &lt; A.GetPtr()[i + 1]; k++)
<a name="l00301"></a>00301               {
<a name="l00302"></a>00302                 <span class="comment">// The k-th column index (among the nonzero entries) on the</span>
<a name="l00303"></a>00303                 <span class="comment">// i-th row, and the corresponding value.</span>
<a name="l00304"></a>00304                 col = A.GetInd()[k];
<a name="l00305"></a>00305                 value = A.GetData()[k];
<a name="l00306"></a>00306                 <span class="comment">// Loop on all elements in the col-th row in B. These elements</span>
<a name="l00307"></a>00307                 <span class="comment">// are multiplied with the element (i, col) of A.</span>
<a name="l00308"></a>00308                 <span class="keywordflow">for</span> (l = B.GetPtr()[col]; l &lt; B.GetPtr()[col + 1]; l++)
<a name="l00309"></a>00309                   {
<a name="l00310"></a>00310                     column_index(h) = B.GetInd()[l];
<a name="l00311"></a>00311                     row_value(h) = value * B.GetData()[l];
<a name="l00312"></a>00312                     h++;
<a name="l00313"></a>00313                   }
<a name="l00314"></a>00314               }
<a name="l00315"></a>00315             <span class="comment">// Now gathers and sorts all elements on the i-th row of C.</span>
<a name="l00316"></a>00316             Nnonzero_row = column_index.GetLength();
<a name="l00317"></a>00317             <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(Nnonzero_row, column_index, row_value);
<a name="l00318"></a>00318 
<a name="l00319"></a>00319 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00320"></a>00320 <span class="preprocessor"></span>            <span class="keywordflow">try</span>
<a name="l00321"></a>00321               {
<a name="l00322"></a>00322 <span class="preprocessor">#endif</span>
<a name="l00323"></a>00323 <span class="preprocessor"></span>
<a name="l00324"></a>00324                 <span class="comment">// Reallocates &#39;c_ind&#39; and &#39;c_data&#39; in order to append the</span>
<a name="l00325"></a>00325                 <span class="comment">// elements of the i-th row of C.</span>
<a name="l00326"></a>00326                 c_ind = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>
<a name="l00327"></a>00327                   (realloc(reinterpret_cast&lt;void*&gt;(c_ind),
<a name="l00328"></a>00328                            (Nnonzero + Nnonzero_row) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00329"></a>00329                 c_data = <span class="keyword">reinterpret_cast&lt;</span>T2*<span class="keyword">&gt;</span>
<a name="l00330"></a>00330                   (C.GetAllocator().reallocate(c_data,
<a name="l00331"></a>00331                                                Nnonzero + Nnonzero_row));
<a name="l00332"></a>00332 
<a name="l00333"></a>00333 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00334"></a>00334 <span class="preprocessor"></span>              }
<a name="l00335"></a>00335             <span class="keywordflow">catch</span> (...)
<a name="l00336"></a>00336               {
<a name="l00337"></a>00337                 c_ind = NULL;
<a name="l00338"></a>00338                 c_data = NULL;
<a name="l00339"></a>00339               }
<a name="l00340"></a>00340 
<a name="l00341"></a>00341             <span class="keywordflow">if</span> ((c_ind == NULL || c_data == NULL)
<a name="l00342"></a>00342                 &amp;&amp; Nnonzero + Nnonzero_row != 0)
<a name="l00343"></a>00343               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Mlt(const Matrix&lt;RowSparse&gt;&amp; A, const &quot;</span>
<a name="l00344"></a>00344                              <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B, Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l00345"></a>00345                              <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00346"></a>00346                              + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nnonzero + Nnonzero_row) + <span class="stringliteral">&quot; integers &quot;</span>
<a name="l00347"></a>00347                              <span class="stringliteral">&quot;and for an array of &quot;</span>
<a name="l00348"></a>00348                              + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(T2) * (Nnonzero + Nnonzero_row))
<a name="l00349"></a>00349                              + <span class="stringliteral">&quot; bytes.&quot;</span>);
<a name="l00350"></a>00350 <span class="preprocessor">#endif</span>
<a name="l00351"></a>00351 <span class="preprocessor"></span>
<a name="l00352"></a>00352             c_ptr[i + 1] += Nnonzero_row;
<a name="l00353"></a>00353             <span class="keywordflow">for</span> (h = 0; h &lt; Nnonzero_row; h++)
<a name="l00354"></a>00354               {
<a name="l00355"></a>00355                 c_ind[Nnonzero + h] = column_index(h);
<a name="l00356"></a>00356                 c_data[Nnonzero + h] = row_value(h);
<a name="l00357"></a>00357               }
<a name="l00358"></a>00358             Nnonzero += Nnonzero_row;
<a name="l00359"></a>00359           }
<a name="l00360"></a>00360       }
<a name="l00361"></a>00361 
<a name="l00362"></a>00362     C.SetData(A.GetM(), B.GetN(), Nnonzero, c_data, c_ptr, c_ind);
<a name="l00363"></a>00363   }
<a name="l00364"></a>00364 
<a name="l00365"></a>00365 
<a name="l00367"></a>00367 
<a name="l00375"></a>00375   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Allocator0,
<a name="l00376"></a>00376             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00377"></a>00377             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l00378"></a><a class="code" href="namespace_seldon.php#a8083bad063691c1d96129f0180f8dbdb">00378</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a8083bad063691c1d96129f0180f8dbdb" title="Multiplies two row-major sparse matrices in Harwell-Boeing format.">MltNoTransTrans</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, RowSparse, Allocator0&gt;</a>&amp; A,
<a name="l00379"></a>00379                        <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; B,
<a name="l00380"></a>00380                        <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; C)
<a name="l00381"></a>00381   {
<a name="l00382"></a>00382 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00383"></a>00383 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(SeldonNoTrans, A, SeldonTrans, B,
<a name="l00384"></a>00384              <span class="stringliteral">&quot;MltNoTransTrans(const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l00385"></a>00385              <span class="stringliteral">&quot;const Matrix&lt;RowSparse&gt;&amp; B, Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>);
<a name="l00386"></a>00386 <span class="preprocessor">#endif</span>
<a name="l00387"></a>00387 <span class="preprocessor"></span>
<a name="l00388"></a>00388     <span class="keywordtype">int</span> h, i, k, col;
<a name="l00389"></a>00389     <span class="keywordtype">int</span> ib, kb;
<a name="l00390"></a>00390     <span class="keywordtype">int</span> Nnonzero_row;
<a name="l00391"></a>00391     <span class="keywordtype">int</span> Nnonzero;
<a name="l00392"></a>00392 
<a name="l00393"></a>00393     <span class="comment">// &#39;MallocAlloc&#39; is specified so that reallocations may be efficient.</span>
<a name="l00394"></a>00394     <span class="comment">// There will be no need for &#39;Resize&#39;: &#39;Reallocate&#39; will do the job.</span>
<a name="l00395"></a>00395     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, MallocAlloc&lt;int&gt;</a> &gt; column_index;
<a name="l00396"></a>00396     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;T2, VectFull, MallocAlloc&lt;T2&gt;</a> &gt; row_value;
<a name="l00397"></a>00397     T2 value = 0;
<a name="l00398"></a>00398 
<a name="l00399"></a>00399     <span class="keywordtype">int</span> m = A.GetM();
<a name="l00400"></a>00400     <span class="keywordtype">int</span> n = B.GetM();
<a name="l00401"></a>00401 
<a name="l00402"></a>00402     <span class="keywordtype">int</span>* c_ptr = NULL;
<a name="l00403"></a>00403     <span class="keywordtype">int</span>* c_ind = NULL;
<a name="l00404"></a>00404     T2* c_data = NULL;
<a name="l00405"></a>00405 
<a name="l00406"></a>00406 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00407"></a>00407 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l00408"></a>00408       {
<a name="l00409"></a>00409 <span class="preprocessor">#endif</span>
<a name="l00410"></a>00410 <span class="preprocessor"></span>
<a name="l00411"></a>00411         c_ptr = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(calloc(m + 1, <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00412"></a>00412 
<a name="l00413"></a>00413 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00414"></a>00414 <span class="preprocessor"></span>      }
<a name="l00415"></a>00415     <span class="keywordflow">catch</span> (...)
<a name="l00416"></a>00416       {
<a name="l00417"></a>00417         c_ptr = NULL;
<a name="l00418"></a>00418       }
<a name="l00419"></a>00419 
<a name="l00420"></a>00420     <span class="keywordflow">if</span> (c_ptr == NULL)
<a name="l00421"></a>00421       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;MltNoTransTrans(const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l00422"></a>00422                      <span class="stringliteral">&quot;const Matrix&lt;RowSparse&gt;&amp; B, Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l00423"></a>00423                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00424"></a>00424                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(m + 1) + <span class="stringliteral">&quot; integers.&quot;</span>);
<a name="l00425"></a>00425 <span class="preprocessor">#endif</span>
<a name="l00426"></a>00426 <span class="preprocessor"></span>
<a name="l00427"></a>00427     c_ptr[0] = 0;
<a name="l00428"></a>00428 
<a name="l00429"></a>00429     <span class="comment">// Number of non-zero elements in C.</span>
<a name="l00430"></a>00430     Nnonzero = 0;
<a name="l00431"></a>00431 
<a name="l00432"></a>00432     <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l00433"></a>00433       {
<a name="l00434"></a>00434         c_ptr[i + 1] = c_ptr[i];
<a name="l00435"></a>00435 
<a name="l00436"></a>00436         <span class="keywordflow">if</span> (A.GetPtr()[i + 1] != A.GetPtr()[i])
<a name="l00437"></a>00437           <span class="comment">// There are elements in the i-th row of A, so there can be non-zero</span>
<a name="l00438"></a>00438           <span class="comment">// entries in C as well. It is checked below whether any row in B</span>
<a name="l00439"></a>00439           <span class="comment">// has an element whose row index matches a column index of a</span>
<a name="l00440"></a>00440           <span class="comment">// non-zero in the i-th row of A.</span>
<a name="l00441"></a>00441           {
<a name="l00442"></a>00442             <span class="comment">// For every element in the i-th row.</span>
<a name="l00443"></a>00443             <span class="keywordflow">for</span> (k = A.GetPtr()[i]; k &lt; A.GetPtr()[i + 1]; k++)
<a name="l00444"></a>00444               {
<a name="l00445"></a>00445                 col = A.GetInd()[k];
<a name="l00446"></a>00446                 <span class="comment">// For every row in B.</span>
<a name="l00447"></a>00447                 <span class="keywordflow">for</span> (ib = 0; ib &lt; n; ib++)
<a name="l00448"></a>00448                   {
<a name="l00449"></a>00449                     <span class="keywordflow">for</span> (kb = B.GetPtr()[ib]; kb &lt; B.GetPtr()[ib + 1]; kb++)
<a name="l00450"></a>00450                       <span class="keywordflow">if</span> (col == B.GetInd()[kb])
<a name="l00451"></a>00451                         value += A.GetData()[k] * B.GetData()[kb];
<a name="l00452"></a>00452                     <span class="keywordflow">if</span> (value != T2(0))
<a name="l00453"></a>00453                       {
<a name="l00454"></a>00454                         row_value.Append(value);
<a name="l00455"></a>00455                         column_index.Append(ib);
<a name="l00456"></a>00456                         value = T2(0);
<a name="l00457"></a>00457                       }
<a name="l00458"></a>00458                   }
<a name="l00459"></a>00459               }
<a name="l00460"></a>00460 
<a name="l00461"></a>00461             Nnonzero_row = column_index.GetLength();
<a name="l00462"></a>00462             <a class="code" href="namespace_seldon.php#a3e269a2dbcd9a3d99011cee07da8c230" title="Assembles a sparse vector.">Assemble</a>(Nnonzero_row, column_index, row_value);
<a name="l00463"></a>00463 
<a name="l00464"></a>00464 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00465"></a>00465 <span class="preprocessor"></span>            <span class="keywordflow">try</span>
<a name="l00466"></a>00466               {
<a name="l00467"></a>00467 <span class="preprocessor">#endif</span>
<a name="l00468"></a>00468 <span class="preprocessor"></span>
<a name="l00469"></a>00469                 <span class="comment">// Reallocates &#39;c_ind&#39; and &#39;c_data&#39; in order to append the</span>
<a name="l00470"></a>00470                 <span class="comment">// elements of the i-th row of C.</span>
<a name="l00471"></a>00471                 c_ind = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>
<a name="l00472"></a>00472                   (realloc(reinterpret_cast&lt;void*&gt;(c_ind),
<a name="l00473"></a>00473                            (Nnonzero + Nnonzero_row) * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l00474"></a>00474                 c_data = <span class="keyword">reinterpret_cast&lt;</span>T2*<span class="keyword">&gt;</span>
<a name="l00475"></a>00475                   (C.GetAllocator().reallocate(c_data,
<a name="l00476"></a>00476                                                Nnonzero + Nnonzero_row));
<a name="l00477"></a>00477 
<a name="l00478"></a>00478 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l00479"></a>00479 <span class="preprocessor"></span>              }
<a name="l00480"></a>00480             <span class="keywordflow">catch</span> (...)
<a name="l00481"></a>00481               {
<a name="l00482"></a>00482                 c_ind = NULL;
<a name="l00483"></a>00483                 c_data = NULL;
<a name="l00484"></a>00484               }
<a name="l00485"></a>00485 
<a name="l00486"></a>00486             <span class="keywordflow">if</span> ((c_ind == NULL || c_data == NULL)
<a name="l00487"></a>00487                 &amp;&amp; Nnonzero_row != 0)
<a name="l00488"></a>00488               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;MltNoTransTrans(const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l00489"></a>00489                              <span class="stringliteral">&quot;const Matrix&lt;RowSparse&gt;&amp; B, &quot;</span>
<a name="l00490"></a>00490                              <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l00491"></a>00491                              <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l00492"></a>00492                              + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nnonzero + Nnonzero_row) + <span class="stringliteral">&quot; integers &quot;</span>
<a name="l00493"></a>00493                              <span class="stringliteral">&quot;and for an array of &quot;</span>
<a name="l00494"></a>00494                              + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(T2) * (Nnonzero + Nnonzero_row))
<a name="l00495"></a>00495                              + <span class="stringliteral">&quot; bytes.&quot;</span>);
<a name="l00496"></a>00496 <span class="preprocessor">#endif</span>
<a name="l00497"></a>00497 <span class="preprocessor"></span>
<a name="l00498"></a>00498             c_ptr[i + 1] += Nnonzero_row;
<a name="l00499"></a>00499             <span class="keywordflow">for</span> (h = 0; h &lt; Nnonzero_row; h++)
<a name="l00500"></a>00500               {
<a name="l00501"></a>00501                 c_ind[Nnonzero + h] = column_index(h);
<a name="l00502"></a>00502                 c_data[Nnonzero + h] = row_value(h);
<a name="l00503"></a>00503               }
<a name="l00504"></a>00504             Nnonzero += Nnonzero_row;
<a name="l00505"></a>00505           }
<a name="l00506"></a>00506 
<a name="l00507"></a>00507         column_index.Clear();
<a name="l00508"></a>00508         row_value.Clear();
<a name="l00509"></a>00509       }
<a name="l00510"></a>00510 
<a name="l00511"></a>00511     C.SetData(A.GetM(), B.GetM(), Nnonzero, c_data, c_ptr, c_ind);
<a name="l00512"></a>00512   }
<a name="l00513"></a>00513 
<a name="l00514"></a>00514 
<a name="l00515"></a>00515   <span class="comment">// MLT //</span>
<a name="l00517"></a>00517 <span class="comment"></span>
<a name="l00518"></a>00518 
<a name="l00519"></a>00519 
<a name="l00521"></a>00521   <span class="comment">// MLTADD //</span>
<a name="l00522"></a>00522 
<a name="l00523"></a>00523 
<a name="l00525"></a>00525 
<a name="l00535"></a>00535   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00536"></a>00536             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00537"></a>00537             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2,
<a name="l00538"></a>00538             <span class="keyword">class </span>T3,
<a name="l00539"></a>00539             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00540"></a><a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3">00540</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00541"></a>00541               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; A,
<a name="l00542"></a>00542               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; B,
<a name="l00543"></a>00543               <span class="keyword">const</span> T3 beta,
<a name="l00544"></a>00544               <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, Storage4, Allocator4&gt;</a>&amp; C)
<a name="l00545"></a>00545   {
<a name="l00546"></a>00546     <span class="keywordtype">int</span> na = A.GetN();
<a name="l00547"></a>00547     <span class="keywordtype">int</span> mc = C.GetM();
<a name="l00548"></a>00548     <span class="keywordtype">int</span> nc = C.GetN();
<a name="l00549"></a>00549 
<a name="l00550"></a>00550 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00551"></a>00551 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(A, B, C, <span class="stringliteral">&quot;MltAdd(alpha, A, B, beta, C)&quot;</span>);
<a name="l00552"></a>00552 <span class="preprocessor">#endif</span>
<a name="l00553"></a>00553 <span class="preprocessor"></span>
<a name="l00554"></a>00554     T4 temp;
<a name="l00555"></a>00555     T4 alpha_(alpha);
<a name="l00556"></a>00556     T4 beta_(beta);
<a name="l00557"></a>00557 
<a name="l00558"></a>00558     <span class="keywordflow">if</span> (beta_ != T4(0))
<a name="l00559"></a>00559       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage4::GetFirst(mc, nc); i++)
<a name="l00560"></a>00560         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Storage4::GetSecond(mc, nc); j++)
<a name="l00561"></a>00561           C(Storage4::GetFirst(i, j), Storage4::GetSecond(i, j))
<a name="l00562"></a>00562             *= beta_;
<a name="l00563"></a>00563     <span class="keywordflow">else</span>
<a name="l00564"></a>00564       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage4::GetFirst(mc, nc); i++)
<a name="l00565"></a>00565         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Storage4::GetSecond(mc, nc); j++)
<a name="l00566"></a>00566           C(Storage4::GetFirst(i, j), Storage4::GetSecond(i, j)) = T4(0);
<a name="l00567"></a>00567 
<a name="l00568"></a>00568     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; Storage4::GetFirst(mc, nc); i++)
<a name="l00569"></a>00569       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; Storage4::GetSecond(mc, nc); j++)
<a name="l00570"></a>00570         {
<a name="l00571"></a>00571           temp = T4(0);
<a name="l00572"></a>00572           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; na; k++)
<a name="l00573"></a>00573             temp += A(Storage4::GetFirst(i, j), k)
<a name="l00574"></a>00574               * B(k, Storage4::GetSecond(i, j));
<a name="l00575"></a>00575           C(Storage4::GetFirst(i, j), Storage4::GetSecond(i, j))
<a name="l00576"></a>00576             += alpha_ * temp;
<a name="l00577"></a>00577         }
<a name="l00578"></a>00578   }
<a name="l00579"></a>00579 
<a name="l00580"></a>00580 
<a name="l00582"></a>00582 
<a name="l00592"></a>00592   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00593"></a>00593             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00594"></a>00594             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2,
<a name="l00595"></a>00595             <span class="keyword">class </span>T3,
<a name="l00596"></a>00596             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00597"></a><a class="code" href="namespace_seldon.php#af704da4762c969a71e9b23c99937acab">00597</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00598"></a>00598               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;</a>&amp; A,
<a name="l00599"></a>00599               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowMajorCollection, Allocator2&gt;</a>&amp; B,
<a name="l00600"></a>00600               <span class="keyword">const</span> T3 beta,
<a name="l00601"></a>00601               <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowMajorCollection, Allocator4&gt;</a>&amp; C)
<a name="l00602"></a>00602   {
<a name="l00603"></a>00603     <span class="keywordtype">int</span> na = A.GetNmatrix();
<a name="l00604"></a>00604     <span class="keywordtype">int</span> mc = C.GetMmatrix();
<a name="l00605"></a>00605     <span class="keywordtype">int</span> nc = C.GetNmatrix();
<a name="l00606"></a>00606 
<a name="l00607"></a>00607 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00608"></a>00608 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(A, B, C, <span class="stringliteral">&quot;MltAdd(alpha, A, B, beta, C)&quot;</span>);
<a name="l00609"></a>00609 <span class="preprocessor">#endif</span>
<a name="l00610"></a>00610 <span class="preprocessor"></span>
<a name="l00611"></a>00611     <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l00612"></a>00612 
<a name="l00613"></a>00613     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(value_type(beta), C);
<a name="l00614"></a>00614 
<a name="l00615"></a>00615     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; mc; i++ )
<a name="l00616"></a>00616       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nc; j++)
<a name="l00617"></a>00617         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; na; k++)
<a name="l00618"></a>00618           <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(value_type(alpha), A.GetMatrix(i, k), B.GetMatrix(k, j),
<a name="l00619"></a>00619                  value_type(1), C.GetMatrix(i, j));
<a name="l00620"></a>00620   }
<a name="l00621"></a>00621 
<a name="l00622"></a>00622 
<a name="l00624"></a>00624 
<a name="l00634"></a>00634   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00635"></a>00635             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00636"></a>00636             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2,
<a name="l00637"></a>00637             <span class="keyword">class </span>T3,
<a name="l00638"></a>00638             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00639"></a><a class="code" href="namespace_seldon.php#a4672f7639a9fde7bf46120791dff7479">00639</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00640"></a>00640               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;</a>&amp; A,
<a name="l00641"></a>00641               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, ColMajorCollection, Allocator2&gt;</a>&amp; B,
<a name="l00642"></a>00642               <span class="keyword">const</span> T3 beta,
<a name="l00643"></a>00643               <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, ColMajorCollection, Allocator4&gt;</a>&amp; C)
<a name="l00644"></a>00644   {
<a name="l00645"></a>00645     <span class="keywordtype">int</span> na = A.GetNmatrix();
<a name="l00646"></a>00646     <span class="keywordtype">int</span> mc = C.GetMmatrix();
<a name="l00647"></a>00647     <span class="keywordtype">int</span> nc = C.GetNmatrix();
<a name="l00648"></a>00648 
<a name="l00649"></a>00649 <span class="preprocessor">#ifdef SELDON_CHECK_DIMENSIONS</span>
<a name="l00650"></a>00650 <span class="preprocessor"></span>    <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(A, B, C, <span class="stringliteral">&quot;MltAdd(alpha, A, B, beta, C)&quot;</span>);
<a name="l00651"></a>00651 <span class="preprocessor">#endif</span>
<a name="l00652"></a>00652 <span class="preprocessor"></span>
<a name="l00653"></a>00653     <span class="keyword">typedef</span> <span class="keyword">typename</span> T4::value_type value_type;
<a name="l00654"></a>00654 
<a name="l00655"></a>00655     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(value_type(beta), C);
<a name="l00656"></a>00656 
<a name="l00657"></a>00657     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; mc; i++ )
<a name="l00658"></a>00658       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nc; j++)
<a name="l00659"></a>00659         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; na; k++)
<a name="l00660"></a>00660           <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(value_type(alpha), A.GetMatrix(i, k), B.GetMatrix(k, j),
<a name="l00661"></a>00661                  value_type(1), C.GetMatrix(i, j));
<a name="l00662"></a>00662   }
<a name="l00663"></a>00663 
<a name="l00664"></a>00664 
<a name="l00665"></a>00665   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00666"></a>00666             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00667"></a>00667             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00668"></a>00668             <span class="keyword">class </span>T3,
<a name="l00669"></a>00669             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00670"></a>00670   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00671"></a>00671               <span class="keyword">const</span> Matrix&lt;T1, General, RowMajor, Allocator1&gt;&amp; A,
<a name="l00672"></a>00672               <span class="keyword">const</span> Matrix&lt;T2, General, RowMajor, Allocator2&gt;&amp; B,
<a name="l00673"></a>00673               <span class="keyword">const</span> T3 beta,
<a name="l00674"></a>00674               Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)
<a name="l00675"></a>00675   {
<a name="l00676"></a>00676     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void MltAdd(const T0 alpha,&quot;</span>
<a name="l00677"></a>00677                     <span class="stringliteral">&quot;const Matrix&lt;T1, General, RowMajor, Allocator1&gt;&amp; A,&quot;</span>
<a name="l00678"></a>00678                     <span class="stringliteral">&quot;const Matrix&lt;T2, General, RowMajor, Allocator2&gt;&amp; B,&quot;</span>
<a name="l00679"></a>00679                     <span class="stringliteral">&quot;const T3 beta,&quot;</span>
<a name="l00680"></a>00680                     <span class="stringliteral">&quot;Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)&quot;</span>);
<a name="l00681"></a>00681   }
<a name="l00682"></a>00682 
<a name="l00683"></a>00683 
<a name="l00684"></a>00684   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00685"></a>00685             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00686"></a>00686             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00687"></a>00687             <span class="keyword">class </span>T3,
<a name="l00688"></a>00688             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00689"></a>00689   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00690"></a>00690               <span class="keyword">const</span> Matrix&lt;T1, General, RowMajor, Allocator1&gt;&amp; A,
<a name="l00691"></a>00691               <span class="keyword">const</span> Matrix&lt;T2, General, RowSparse, Allocator2&gt;&amp; B,
<a name="l00692"></a>00692               <span class="keyword">const</span> T3 beta,
<a name="l00693"></a>00693               Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)
<a name="l00694"></a>00694   {
<a name="l00695"></a>00695     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void MltAdd(const T0 alpha,&quot;</span>
<a name="l00696"></a>00696                     <span class="stringliteral">&quot;const Matrix&lt;T1, General, RowMajor, Allocator1&gt;&amp; A,&quot;</span>
<a name="l00697"></a>00697                     <span class="stringliteral">&quot;const Matrix&lt;T2, General, RowSparse, Allocator2&gt;&amp; B,&quot;</span>
<a name="l00698"></a>00698                     <span class="stringliteral">&quot;const T3 beta,&quot;</span>
<a name="l00699"></a>00699                     <span class="stringliteral">&quot;Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)&quot;</span>);
<a name="l00700"></a>00700   }
<a name="l00701"></a>00701 
<a name="l00702"></a>00702 
<a name="l00703"></a>00703   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00704"></a>00704             <span class="keyword">class </span>T1, <span class="keyword">class </span>Allocator1,
<a name="l00705"></a>00705             <span class="keyword">class </span>T2, <span class="keyword">class </span>Allocator2,
<a name="l00706"></a>00706             <span class="keyword">class </span>T3,
<a name="l00707"></a>00707             <span class="keyword">class </span>T4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00708"></a>00708   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00709"></a>00709               <span class="keyword">const</span> Matrix&lt;T1, General, RowSparse, Allocator1&gt;&amp; A,
<a name="l00710"></a>00710               <span class="keyword">const</span> Matrix&lt;T2, General, RowMajor, Allocator2&gt;&amp; B,
<a name="l00711"></a>00711               <span class="keyword">const</span> T3 beta,
<a name="l00712"></a>00712               Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)
<a name="l00713"></a>00713   {
<a name="l00714"></a>00714     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void MltAdd(const T0 alpha,&quot;</span>
<a name="l00715"></a>00715                     <span class="stringliteral">&quot;const Matrix&lt;T1, General, RowSparse, Allocator1&gt;&amp; A,&quot;</span>
<a name="l00716"></a>00716                     <span class="stringliteral">&quot;const Matrix&lt;T2, General, RowMajor, Allocator2&gt;&amp; B,&quot;</span>
<a name="l00717"></a>00717                     <span class="stringliteral">&quot;const T3 beta,&quot;</span>
<a name="l00718"></a>00718                     <span class="stringliteral">&quot;Matrix&lt;T4, General, RowSparse, Allocator4&gt;&amp; C)&quot;</span>);
<a name="l00719"></a>00719   }
<a name="l00720"></a>00720 
<a name="l00721"></a>00721 
<a name="l00722"></a>00722   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00723"></a>00723            <span class="keyword">class </span>Allocator1,
<a name="l00724"></a>00724            <span class="keyword">class </span>Allocator2,
<a name="l00725"></a>00725            <span class="keyword">class </span>Allocator3,
<a name="l00726"></a>00726            <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00727"></a>00727   <span class="keywordtype">void</span> MltAdd_heterogeneous(<span class="keyword">const</span> T0 alpha,
<a name="l00728"></a>00728                             <span class="keyword">const</span> Matrix&lt;FloatDouble, General,
<a name="l00729"></a>00729                             DenseSparseCollection, Allocator1&gt;&amp; A,
<a name="l00730"></a>00730                             <span class="keyword">const</span> Matrix&lt;FloatDouble, General,
<a name="l00731"></a>00731                             DenseSparseCollection, Allocator2&gt;&amp; B,
<a name="l00732"></a>00732                             Matrix&lt;FloatDouble, General,
<a name="l00733"></a>00733                             DenseSparseCollection, Allocator3&gt;&amp; C,
<a name="l00734"></a>00734                             Matrix&lt;T4, Prop4, Storage4, Allocator4&gt;&amp; mc,
<a name="l00735"></a>00735                             <span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l00736"></a>00736   {
<a name="l00737"></a>00737     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator1&gt;
<a name="l00738"></a>00738       ::float_dense_m m0a;
<a name="l00739"></a>00739     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator1&gt;
<a name="l00740"></a>00740       ::float_sparse_m m1a;
<a name="l00741"></a>00741     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator1&gt;
<a name="l00742"></a>00742       ::double_dense_m m2a;
<a name="l00743"></a>00743     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator1&gt;
<a name="l00744"></a>00744       ::double_sparse_m m3a;
<a name="l00745"></a>00745 
<a name="l00746"></a>00746     <span class="keywordtype">int</span> na = A.GetNmatrix();
<a name="l00747"></a>00747     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> k = 0; k &lt; na; k++)
<a name="l00748"></a>00748       {
<a name="l00749"></a>00749         <span class="keywordflow">switch</span> (A.GetType(i, k))
<a name="l00750"></a>00750           {
<a name="l00751"></a>00751           <span class="keywordflow">case</span> 0:
<a name="l00752"></a>00752             A.GetMatrix(i, k, m0a);
<a name="l00753"></a>00753             MltAdd_heterogeneous2(alpha, m0a, B, C, mc, j, k);
<a name="l00754"></a>00754             m0a.Nullify();
<a name="l00755"></a>00755             <span class="keywordflow">break</span>;
<a name="l00756"></a>00756           <span class="keywordflow">case</span> 1:
<a name="l00757"></a>00757             A.GetMatrix(i, k, m1a);
<a name="l00758"></a>00758             MltAdd_heterogeneous2(alpha, m1a, B, C, mc, j, k);
<a name="l00759"></a>00759             m1a.Nullify();
<a name="l00760"></a>00760             <span class="keywordflow">break</span>;
<a name="l00761"></a>00761           <span class="keywordflow">case</span> 2:
<a name="l00762"></a>00762             A.GetMatrix(i, k, m2a);
<a name="l00763"></a>00763             MltAdd_heterogeneous2(alpha, m2a, B, C, mc, j, k);
<a name="l00764"></a>00764             m2a.Nullify();
<a name="l00765"></a>00765             <span class="keywordflow">break</span>;
<a name="l00766"></a>00766           <span class="keywordflow">case</span> 3:
<a name="l00767"></a>00767             A.GetMatrix(i, k, m3a);
<a name="l00768"></a>00768             MltAdd_heterogeneous2(alpha, m3a, B, C, mc, j, k);
<a name="l00769"></a>00769             m3a.Nullify();
<a name="l00770"></a>00770             <span class="keywordflow">break</span>;
<a name="l00771"></a>00771           <span class="keywordflow">default</span>:
<a name="l00772"></a>00772             <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;::&quot;</span>
<a name="l00773"></a>00773                                 <span class="stringliteral">&quot;MltAdd_heterogeneous(alpha, A, B, beta, C) &quot;</span>,
<a name="l00774"></a>00774                                 <span class="stringliteral">&quot;Underlying matrix  A (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00775"></a>00775                                 + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(k) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00776"></a>00776           }
<a name="l00777"></a>00777       }
<a name="l00778"></a>00778   }
<a name="l00779"></a>00779 
<a name="l00780"></a>00780 
<a name="l00781"></a>00781   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0,
<a name="l00782"></a>00782            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l00783"></a>00783            <span class="keyword">class </span>Allocator2,
<a name="l00784"></a>00784            <span class="keyword">class </span>Allocator3,
<a name="l00785"></a>00785            <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Storage4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00786"></a>00786   <span class="keywordtype">void</span> MltAdd_heterogeneous2(<span class="keyword">const</span> T0 alpha,
<a name="l00787"></a>00787                              <span class="keyword">const</span> Matrix&lt;T1, Prop1,
<a name="l00788"></a>00788                              Storage1, Allocator1&gt;&amp; ma,
<a name="l00789"></a>00789                              <span class="keyword">const</span> Matrix&lt;FloatDouble, General,
<a name="l00790"></a>00790                              DenseSparseCollection, Allocator2&gt;&amp; B,
<a name="l00791"></a>00791                              Matrix&lt;FloatDouble, General,
<a name="l00792"></a>00792                              DenseSparseCollection, Allocator3&gt;&amp; C,
<a name="l00793"></a>00793                              Matrix&lt;T4, Prop4, Storage4, Allocator4&gt;&amp; mc,
<a name="l00794"></a>00794                              <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k)
<a name="l00795"></a>00795   {
<a name="l00796"></a>00796     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l00797"></a>00797       ::float_dense_m m0b;
<a name="l00798"></a>00798     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l00799"></a>00799       ::float_sparse_m m1b;
<a name="l00800"></a>00800     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l00801"></a>00801       ::double_dense_m m2b;
<a name="l00802"></a>00802     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l00803"></a>00803       ::double_sparse_m m3b;
<a name="l00804"></a>00804 
<a name="l00805"></a>00805     <span class="keywordflow">switch</span> (B.GetType(k, j))
<a name="l00806"></a>00806       {
<a name="l00807"></a>00807       <span class="keywordflow">case</span> 0:
<a name="l00808"></a>00808         B.GetMatrix(k, j, m0b);
<a name="l00809"></a>00809         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, ma, m0b, 1., mc);
<a name="l00810"></a>00810         m0b.Nullify();
<a name="l00811"></a>00811         <span class="keywordflow">break</span>;
<a name="l00812"></a>00812       <span class="keywordflow">case</span> 1:
<a name="l00813"></a>00813         B.GetMatrix(k, j, m1b);
<a name="l00814"></a>00814         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, ma, m1b, 1., mc);
<a name="l00815"></a>00815         m1b.Nullify();
<a name="l00816"></a>00816         <span class="keywordflow">break</span>;
<a name="l00817"></a>00817       <span class="keywordflow">case</span> 2:
<a name="l00818"></a>00818         B.GetMatrix(k, j, m2b);
<a name="l00819"></a>00819         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, ma, m2b, 1., mc);
<a name="l00820"></a>00820         m2b.Nullify();
<a name="l00821"></a>00821         <span class="keywordflow">break</span>;
<a name="l00822"></a>00822       <span class="keywordflow">case</span> 3:
<a name="l00823"></a>00823         B.GetMatrix(k, j, m3b);
<a name="l00824"></a>00824         <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, ma, m3b, 1., mc);
<a name="l00825"></a>00825         m3b.Nullify();
<a name="l00826"></a>00826         <span class="keywordflow">break</span>;
<a name="l00827"></a>00827       <span class="keywordflow">default</span>:
<a name="l00828"></a>00828         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00829"></a>00829                             <span class="stringliteral">&quot;::MltAdd_heterogeneous2(alpha, A, B, beta, C)&quot;</span>,
<a name="l00830"></a>00830                             <span class="stringliteral">&quot;Underlying matrix  B (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(k) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00831"></a>00831                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00832"></a>00832       }
<a name="l00833"></a>00833   }
<a name="l00834"></a>00834 
<a name="l00835"></a>00835 
<a name="l00836"></a>00836 
<a name="l00838"></a>00838 
<a name="l00842"></a>00842   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Allocator1, <span class="keyword">class </span>Allocator2, <span class="keyword">class </span>T3,
<a name="l00843"></a>00843             <span class="keyword">class </span>Allocator4&gt;
<a name="l00844"></a><a class="code" href="namespace_seldon.php#af98be62d4455489d97a565037993f6a3">00844</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00845"></a>00845               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>,
<a name="l00846"></a>00846               Allocator1&gt;&amp; A,
<a name="l00847"></a>00847               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>,
<a name="l00848"></a>00848               Allocator2&gt;&amp; B,
<a name="l00849"></a>00849               <span class="keyword">const</span> T3 beta,
<a name="l00850"></a>00850               <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>, <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>,
<a name="l00851"></a>00851               Allocator4&gt;&amp; C)
<a name="l00852"></a>00852   {
<a name="l00853"></a>00853     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator4&gt;</a>
<a name="l00854"></a>00854       ::float_dense_m m0c;
<a name="l00855"></a>00855     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator4&gt;</a>
<a name="l00856"></a>00856       ::float_sparse_m m1c;
<a name="l00857"></a>00857     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator4&gt;</a>
<a name="l00858"></a>00858       ::double_dense_m m2c;
<a name="l00859"></a>00859     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator4&gt;</a>
<a name="l00860"></a>00860       ::double_sparse_m m3c;
<a name="l00861"></a>00861 
<a name="l00862"></a>00862     <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00863"></a>00863 
<a name="l00864"></a>00864     <span class="keywordtype">int</span> mc = C.GetMmatrix();
<a name="l00865"></a>00865     <span class="keywordtype">int</span> nc = C.GetNmatrix();
<a name="l00866"></a>00866     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; mc; i++ )
<a name="l00867"></a>00867       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; nc; j++)
<a name="l00868"></a>00868         {
<a name="l00869"></a>00869           <span class="keywordflow">switch</span> (C.GetType(i, j))
<a name="l00870"></a>00870             {
<a name="l00871"></a>00871             <span class="keywordflow">case</span> 0:
<a name="l00872"></a>00872               C.GetMatrix(i, j, m0c);
<a name="l00873"></a>00873               MltAdd_heterogeneous(<span class="keywordtype">float</span>(alpha), A, B, C, m0c, i, j);
<a name="l00874"></a>00874               C.SetMatrix(i, j, m0c);
<a name="l00875"></a>00875               m0c.Nullify();
<a name="l00876"></a>00876               <span class="keywordflow">break</span>;
<a name="l00877"></a>00877             <span class="keywordflow">case</span> 1:
<a name="l00878"></a>00878               C.GetMatrix(i, j, m1c);
<a name="l00879"></a>00879               MltAdd_heterogeneous(<span class="keywordtype">float</span>(alpha), A, B, C, m1c, i, j);
<a name="l00880"></a>00880               C.SetMatrix(i, j, m1c);
<a name="l00881"></a>00881               m1c.Nullify();
<a name="l00882"></a>00882               <span class="keywordflow">break</span>;
<a name="l00883"></a>00883             <span class="keywordflow">case</span> 2:
<a name="l00884"></a>00884               C.GetMatrix(i, j, m2c);
<a name="l00885"></a>00885               MltAdd_heterogeneous(<span class="keywordtype">double</span>(alpha), A, B, C, m2c, i, j);
<a name="l00886"></a>00886               C.SetMatrix(i, j, m2c);
<a name="l00887"></a>00887               m2c.Nullify();
<a name="l00888"></a>00888               <span class="keywordflow">break</span>;
<a name="l00889"></a>00889             <span class="keywordflow">case</span> 3:
<a name="l00890"></a>00890               C.GetMatrix(i, j, m3c);
<a name="l00891"></a>00891               MltAdd_heterogeneous(<span class="keywordtype">double</span>(alpha), A, B, C, m3c, i, j);
<a name="l00892"></a>00892               C.SetMatrix(i, j, m3c);
<a name="l00893"></a>00893               m3c.Nullify();
<a name="l00894"></a>00894               <span class="keywordflow">break</span>;
<a name="l00895"></a>00895             <span class="keywordflow">default</span>:
<a name="l00896"></a>00896               <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Matrix&lt;FloatDouble, DenseSparseCollection&gt;&quot;</span>
<a name="l00897"></a>00897                                   <span class="stringliteral">&quot;::MltAdd(alpha, A, B, beta, C) &quot;</span>,
<a name="l00898"></a>00898                                   <span class="stringliteral">&quot;Underlying matrix  C (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l00899"></a>00899                                   + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l00900"></a>00900             }
<a name="l00901"></a>00901         }
<a name="l00902"></a>00902   }
<a name="l00903"></a>00903 
<a name="l00904"></a>00904 
<a name="l00906"></a>00906 
<a name="l00916"></a>00916   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00917"></a>00917             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00918"></a>00918             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2,
<a name="l00919"></a>00919             <span class="keyword">class </span>T3,
<a name="l00920"></a>00920             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00921"></a><a class="code" href="namespace_seldon.php#abe3a2e2e627dcf8fcdc512be8d9207a2">00921</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00922"></a>00922               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; A,
<a name="l00923"></a>00923               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; B,
<a name="l00924"></a>00924               <span class="keyword">const</span> T3 beta,
<a name="l00925"></a>00925               <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowSparse, Allocator4&gt;</a>&amp; C)
<a name="l00926"></a>00926   {
<a name="l00927"></a>00927     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00928"></a>00928       {
<a name="l00929"></a>00929         <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00930"></a>00930           <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, C);
<a name="l00931"></a>00931         <span class="keywordflow">else</span>
<a name="l00932"></a>00932           {
<a name="l00933"></a>00933             <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, B, C);
<a name="l00934"></a>00934             <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00935"></a>00935               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, C);
<a name="l00936"></a>00936           }
<a name="l00937"></a>00937       }
<a name="l00938"></a>00938     <span class="keywordflow">else</span>
<a name="l00939"></a>00939       {
<a name="l00940"></a>00940         <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00941"></a>00941           <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00942"></a>00942         <span class="keywordflow">else</span>
<a name="l00943"></a>00943           {
<a name="l00944"></a>00944             <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowSparse, Allocator4&gt;</a> tmp;
<a name="l00945"></a>00945             <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(A, B, tmp);
<a name="l00946"></a>00946             <span class="keywordflow">if</span> (beta != T0(1))
<a name="l00947"></a>00947               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00948"></a>00948             <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, tmp, C);
<a name="l00949"></a>00949           }
<a name="l00950"></a>00950       }
<a name="l00951"></a>00951   }
<a name="l00952"></a>00952 
<a name="l00953"></a>00953 
<a name="l00955"></a>00955 
<a name="l00967"></a>00967   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l00968"></a>00968             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l00969"></a>00969             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2,
<a name="l00970"></a>00970             <span class="keyword">class </span>T3,
<a name="l00971"></a>00971             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Allocator4&gt;
<a name="l00972"></a><a class="code" href="namespace_seldon.php#a0bf7b11986806e6b41606916d4bbe695">00972</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a0bf7b11986806e6b41606916d4bbe695" title="Multiplies two row-major sparse matrices and adds the result to a third.">MltNoTransTransAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l00973"></a>00973                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; A,
<a name="l00974"></a>00974                           <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; B,
<a name="l00975"></a>00975                           <span class="keyword">const</span> T3 beta,
<a name="l00976"></a>00976                           <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowSparse, Allocator4&gt;</a>&amp; C)
<a name="l00977"></a>00977   {
<a name="l00978"></a>00978     <span class="keywordflow">if</span> (beta == T3(0))
<a name="l00979"></a>00979       {
<a name="l00980"></a>00980         <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00981"></a>00981           <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, C);
<a name="l00982"></a>00982         <span class="keywordflow">else</span>
<a name="l00983"></a>00983           {
<a name="l00984"></a>00984             <a class="code" href="namespace_seldon.php#a8083bad063691c1d96129f0180f8dbdb" title="Multiplies two row-major sparse matrices in Harwell-Boeing format.">MltNoTransTrans</a>(A, B, C);
<a name="l00985"></a>00985             <span class="keywordflow">if</span> (alpha != T0(1))
<a name="l00986"></a>00986               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(alpha, C);
<a name="l00987"></a>00987           }
<a name="l00988"></a>00988       }
<a name="l00989"></a>00989     <span class="keywordflow">else</span>
<a name="l00990"></a>00990       {
<a name="l00991"></a>00991         <span class="keywordflow">if</span> (alpha == T0(0))
<a name="l00992"></a>00992           <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00993"></a>00993         <span class="keywordflow">else</span>
<a name="l00994"></a>00994           {
<a name="l00995"></a>00995             <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowSparse, Allocator4&gt;</a> tmp;
<a name="l00996"></a>00996             <a class="code" href="namespace_seldon.php#a8083bad063691c1d96129f0180f8dbdb" title="Multiplies two row-major sparse matrices in Harwell-Boeing format.">MltNoTransTrans</a>(A, B, tmp);
<a name="l00997"></a>00997             <span class="keywordflow">if</span> (beta != T0(1))
<a name="l00998"></a>00998               <a class="code" href="namespace_seldon.php#a31902c0b2350a5f330ec7896e3a1c5ab" title="Multiplies a matrix by a scalar.">Mlt</a>(beta, C);
<a name="l00999"></a>00999             <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(alpha, tmp, C);
<a name="l01000"></a>01000           }
<a name="l01001"></a>01001       }
<a name="l01002"></a>01002   }
<a name="l01003"></a>01003 
<a name="l01004"></a>01004 
<a name="l01006"></a>01006 
<a name="l01022"></a>01022   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01023"></a>01023             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01024"></a>01024             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2,
<a name="l01025"></a>01025             <span class="keyword">class </span>T3,
<a name="l01026"></a>01026             <span class="keyword">class </span>T4, <span class="keyword">class </span>Prop4, <span class="keyword">class </span>Allocator4&gt;
<a name="l01027"></a><a class="code" href="namespace_seldon.php#a4b44b3c01294b847fa1d184841643a84">01027</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01028"></a>01028               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l01029"></a>01029               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; A,
<a name="l01030"></a>01030               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransB,
<a name="l01031"></a>01031               <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; B,
<a name="l01032"></a>01032               <span class="keyword">const</span> T3 beta,
<a name="l01033"></a>01033               <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T4, Prop4, RowSparse, Allocator4&gt;</a>&amp; C)
<a name="l01034"></a>01034   {
<a name="l01035"></a>01035     <span class="keywordflow">if</span> (!TransA.NoTrans())
<a name="l01036"></a>01036       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;MltAdd(T0 alpha, SeldonTranspose TransA, &quot;</span>
<a name="l01037"></a>01037                           <span class="stringliteral">&quot;const Matrix&lt;RowSparse&gt;&amp; A, SeldonTranspose &quot;</span>
<a name="l01038"></a>01038                           <span class="stringliteral">&quot;TransB, const Matrix&lt;RowSparse&gt;&amp; B, T3 beta, &quot;</span>
<a name="l01039"></a>01039                           <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l01040"></a>01040                           <span class="stringliteral">&quot;&#39;TransA&#39; must be equal to &#39;SeldonNoTrans&#39;.&quot;</span>);
<a name="l01041"></a>01041     <span class="keywordflow">if</span> (!TransB.NoTrans() &amp;&amp; !TransB.Trans())
<a name="l01042"></a>01042       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;MltAdd(T0 alpha, SeldonTranspose TransA, &quot;</span>
<a name="l01043"></a>01043                           <span class="stringliteral">&quot;const Matrix&lt;RowSparse&gt;&amp; A, SeldonTranspose &quot;</span>
<a name="l01044"></a>01044                           <span class="stringliteral">&quot;TransB, const Matrix&lt;RowSparse&gt;&amp; B, T3 beta, &quot;</span>
<a name="l01045"></a>01045                           <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; C)&quot;</span>,
<a name="l01046"></a>01046                           <span class="stringliteral">&quot;&#39;TransB&#39; must be equal to &#39;SeldonNoTrans&#39; or &quot;</span>
<a name="l01047"></a>01047                           <span class="stringliteral">&quot;&#39;SeldonTrans&#39;.&quot;</span>);
<a name="l01048"></a>01048 
<a name="l01049"></a>01049     <span class="keywordflow">if</span> (TransB.Trans())
<a name="l01050"></a>01050       <a class="code" href="namespace_seldon.php#a0bf7b11986806e6b41606916d4bbe695" title="Multiplies two row-major sparse matrices and adds the result to a third.">MltNoTransTransAdd</a>(alpha, A, B, beta, C);
<a name="l01051"></a>01051     <span class="keywordflow">else</span>
<a name="l01052"></a>01052       <a class="code" href="namespace_seldon.php#a6ff145c5ab283c371ab6b881e1c3e7d3" title="Multiplies two matrices, and adds the result to a third matrix.">MltAdd</a>(alpha, A, B, beta, C);
<a name="l01053"></a>01053   }
<a name="l01054"></a>01054 
<a name="l01055"></a>01055 
<a name="l01056"></a>01056   <span class="comment">// MLTADD //</span>
<a name="l01058"></a>01058 <span class="comment"></span>
<a name="l01059"></a>01059 
<a name="l01060"></a>01060 
<a name="l01062"></a>01062   <span class="comment">// ADD //</span>
<a name="l01063"></a>01063 
<a name="l01064"></a>01064 
<a name="l01066"></a>01066 
<a name="l01073"></a>01073   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01074"></a>01074            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01075"></a><a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3">01075</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01076"></a>01076            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; A,
<a name="l01077"></a>01077            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; B)
<a name="l01078"></a>01078   {
<a name="l01079"></a>01079     <span class="keywordtype">int</span> i, j;
<a name="l01080"></a>01080     <span class="keywordflow">for</span> (i = 0; i &lt; A.GetM(); i++)
<a name="l01081"></a>01081       <span class="keywordflow">for</span> (j = 0; j &lt; A.GetN(); j++)
<a name="l01082"></a>01082         B(i, j) += alpha * A(i, j);
<a name="l01083"></a>01083   }
<a name="l01084"></a>01084 
<a name="l01085"></a>01085 
<a name="l01087"></a>01087 
<a name="l01094"></a>01094   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0,
<a name="l01095"></a>01095            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01096"></a>01096            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01097"></a><a class="code" href="namespace_seldon.php#a5b3620db5d196cc4d22201882a07f4ff">01097</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01098"></a>01098            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowMajorCollection, Allocator1&gt;</a>&amp; A,
<a name="l01099"></a>01099            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowMajorCollection, Allocator2&gt;</a>&amp; B)
<a name="l01100"></a>01100   {
<a name="l01101"></a>01101     <span class="keywordtype">int</span> na = A.GetNmatrix();
<a name="l01102"></a>01102     <span class="keywordtype">int</span> ma = A.GetMmatrix();
<a name="l01103"></a>01103 
<a name="l01104"></a>01104     <span class="keyword">typedef</span> <span class="keyword">typename</span> T2::value_type value_type;
<a name="l01105"></a>01105 
<a name="l01106"></a>01106     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++ )
<a name="l01107"></a>01107       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01108"></a>01108         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(value_type(alpha), A.GetMatrix(i, j), B.GetMatrix(i, j));
<a name="l01109"></a>01109   }
<a name="l01110"></a>01110 
<a name="l01111"></a>01111 
<a name="l01113"></a>01113 
<a name="l01120"></a>01120   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0,
<a name="l01121"></a>01121            <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01122"></a>01122            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01123"></a><a class="code" href="namespace_seldon.php#a640ab1b444096b5571e13715b427d450">01123</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01124"></a>01124            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, ColMajorCollection, Allocator1&gt;</a>&amp; A,
<a name="l01125"></a>01125            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, ColMajorCollection, Allocator2&gt;</a>&amp; B)
<a name="l01126"></a>01126   {
<a name="l01127"></a>01127     <span class="keywordtype">int</span> na = A.GetNmatrix();
<a name="l01128"></a>01128     <span class="keywordtype">int</span> ma = A.GetMmatrix();
<a name="l01129"></a>01129 
<a name="l01130"></a>01130     <span class="keyword">typedef</span> <span class="keyword">typename</span> T2::value_type value_type;
<a name="l01131"></a>01131 
<a name="l01132"></a>01132     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; ma; i++ )
<a name="l01133"></a>01133       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; na; j++)
<a name="l01134"></a>01134         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(value_type(alpha), A.GetMatrix(i, j), B.GetMatrix(i, j));
<a name="l01135"></a>01135   }
<a name="l01136"></a>01136 
<a name="l01137"></a>01137 
<a name="l01138"></a>01138   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0,
<a name="l01139"></a>01139             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01140"></a>01140             <span class="keyword">class </span>Allocator2&gt;
<a name="l01141"></a>01141   <span class="keywordtype">void</span> Add_heterogeneous(<span class="keyword">const</span> T0 alpha,
<a name="l01142"></a>01142                          <span class="keyword">const</span>  Matrix&lt;T1, Prop1, Storage1, Allocator1 &gt;&amp; ma,
<a name="l01143"></a>01143                          Matrix&lt;FloatDouble, General,
<a name="l01144"></a>01144                          DenseSparseCollection, Allocator2&gt;&amp; B,
<a name="l01145"></a>01145                          <span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j)
<a name="l01146"></a>01146   {
<a name="l01147"></a>01147     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l01148"></a>01148       ::float_dense_m m0b;
<a name="l01149"></a>01149     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l01150"></a>01150       ::float_sparse_m m1b;
<a name="l01151"></a>01151     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l01152"></a>01152       ::double_dense_m m2b;
<a name="l01153"></a>01153     <span class="keyword">typename</span> Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;
<a name="l01154"></a>01154       ::double_sparse_m m3b;
<a name="l01155"></a>01155 
<a name="l01156"></a>01156     T0 alpha_ = alpha;
<a name="l01157"></a>01157 
<a name="l01158"></a>01158     <span class="keywordflow">switch</span> (B.GetType(i, j))
<a name="l01159"></a>01159       {
<a name="l01160"></a>01160       <span class="keywordflow">case</span> 0:
<a name="l01161"></a>01161         B.GetMatrix(i, j, m0b);
<a name="l01162"></a>01162         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keywordtype">float</span>(alpha_), ma, m0b);
<a name="l01163"></a>01163         B.SetMatrix(i, j, m0b);
<a name="l01164"></a>01164         m0b.Nullify();
<a name="l01165"></a>01165         <span class="keywordflow">break</span>;
<a name="l01166"></a>01166       <span class="keywordflow">case</span> 1:
<a name="l01167"></a>01167         B.GetMatrix(i, j, m1b);
<a name="l01168"></a>01168         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keywordtype">float</span>(alpha_), ma, m1b);
<a name="l01169"></a>01169         B.SetMatrix(i, j, m1b);
<a name="l01170"></a>01170         m1b.Nullify();
<a name="l01171"></a>01171         <span class="keywordflow">break</span>;
<a name="l01172"></a>01172       <span class="keywordflow">case</span> 2:
<a name="l01173"></a>01173         B.GetMatrix(i, j, m2b);
<a name="l01174"></a>01174         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keywordtype">double</span>(alpha_), ma, m2b);
<a name="l01175"></a>01175         B.SetMatrix(i, j, m2b);
<a name="l01176"></a>01176         m2b.Nullify();
<a name="l01177"></a>01177         <span class="keywordflow">break</span>;
<a name="l01178"></a>01178       <span class="keywordflow">case</span> 3:
<a name="l01179"></a>01179         B.GetMatrix(i, j, m3b);
<a name="l01180"></a>01180         <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keywordtype">double</span>(alpha_), ma, m3b);
<a name="l01181"></a>01181         B.SetMatrix(i, j, m3b);
<a name="l01182"></a>01182         m3b.Nullify();
<a name="l01183"></a>01183         <span class="keywordflow">break</span>;
<a name="l01184"></a>01184       <span class="keywordflow">default</span>:
<a name="l01185"></a>01185         <span class="keywordflow">throw</span> WrongArgument(<span class="stringliteral">&quot;Add_heterogeneous(alpha, Matrix&lt;FloatDouble, &quot;</span>
<a name="l01186"></a>01186                             <span class="stringliteral">&quot;DenseSparseCollection&gt;, Matrix&lt;FloatDouble,&quot;</span>
<a name="l01187"></a>01187                             <span class="stringliteral">&quot;DenseSparseCollection&gt; )&quot;</span>,
<a name="l01188"></a>01188                             <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01189"></a>01189                             + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01190"></a>01190       }
<a name="l01191"></a>01191   }
<a name="l01192"></a>01192 
<a name="l01193"></a>01193 
<a name="l01195"></a>01195 
<a name="l01199"></a>01199   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Allocator1, <span class="keyword">class</span> Allocator2&gt;
<a name="l01200"></a><a class="code" href="namespace_seldon.php#a3f9ca32de7662c4ace3fc7b8d4eea0d9">01200</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0 alpha,
<a name="l01201"></a>01201            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;<a class="code" href="class_seldon_1_1_float_double.php">FloatDouble</a>, <a class="code" href="class_seldon_1_1_general.php">General</a>,
<a name="l01202"></a>01202            <a class="code" href="class_seldon_1_1_dense_sparse_collection.php">DenseSparseCollection</a>, Allocator1&gt;&amp; A,
<a name="l01203"></a>01203            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;</a>&amp; B)
<a name="l01204"></a>01204   {
<a name="l01205"></a>01205     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;</a>
<a name="l01206"></a>01206       ::float_dense_m m0a;
<a name="l01207"></a>01207     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;</a>
<a name="l01208"></a>01208       ::float_sparse_m m1a;
<a name="l01209"></a>01209     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;</a>
<a name="l01210"></a>01210       ::double_dense_m m2a;
<a name="l01211"></a>01211     <span class="keyword">typename</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;FloatDouble, General, DenseSparseCollection, Allocator2&gt;</a>
<a name="l01212"></a>01212       ::double_sparse_m m3a;
<a name="l01213"></a>01213 
<a name="l01214"></a>01214     T0 alpha_ = alpha;
<a name="l01215"></a>01215 
<a name="l01216"></a>01216     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; B.GetMmatrix(); i++)
<a name="l01217"></a>01217       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; B.GetNmatrix(); j++)
<a name="l01218"></a>01218         {
<a name="l01219"></a>01219           <span class="keywordflow">switch</span> (B.GetType(i, j))
<a name="l01220"></a>01220             {
<a name="l01221"></a>01221             <span class="keywordflow">case</span> 0:
<a name="l01222"></a>01222               A.GetMatrix(i, j, m0a);
<a name="l01223"></a>01223               Add_heterogeneous(<span class="keywordtype">float</span>(alpha_), m0a, B, i, j);
<a name="l01224"></a>01224               m0a.Nullify();
<a name="l01225"></a>01225               <span class="keywordflow">break</span>;
<a name="l01226"></a>01226             <span class="keywordflow">case</span> 1:
<a name="l01227"></a>01227               A.GetMatrix(i, j, m1a);
<a name="l01228"></a>01228               Add_heterogeneous(<span class="keywordtype">float</span>(alpha_), m1a, B, i, j);
<a name="l01229"></a>01229               m1a.Nullify();
<a name="l01230"></a>01230               <span class="keywordflow">break</span>;
<a name="l01231"></a>01231             <span class="keywordflow">case</span> 2:
<a name="l01232"></a>01232               A.GetMatrix(i, j, m2a);
<a name="l01233"></a>01233               Add_heterogeneous(<span class="keywordtype">double</span>(alpha_), m2a, B, i, j);
<a name="l01234"></a>01234               m2a.Nullify();
<a name="l01235"></a>01235               <span class="keywordflow">break</span>;
<a name="l01236"></a>01236             <span class="keywordflow">case</span> 3:
<a name="l01237"></a>01237               A.GetMatrix(i, j, m3a);
<a name="l01238"></a>01238               Add_heterogeneous(<span class="keywordtype">double</span>(alpha_), m3a, B, i, j);
<a name="l01239"></a>01239               m3a.Nullify();
<a name="l01240"></a>01240               <span class="keywordflow">break</span>;
<a name="l01241"></a>01241             <span class="keywordflow">default</span>:
<a name="l01242"></a>01242               <span class="keywordflow">throw</span>
<a name="l01243"></a>01243                 <a class="code" href="class_seldon_1_1_wrong_argument.php">WrongArgument</a>(<span class="stringliteral">&quot;Add(alpha, Matrix&lt;FloatDouble, &quot;</span>
<a name="l01244"></a>01244                               <span class="stringliteral">&quot;DenseSparseCollection&gt;, Matrix&lt;FloatDouble,&quot;</span>
<a name="l01245"></a>01245                               <span class="stringliteral">&quot;DenseSparseCollection&gt; )&quot;</span>,
<a name="l01246"></a>01246                               <span class="stringliteral">&quot;Underlying matrix (&quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(i) + <span class="stringliteral">&quot; ,&quot;</span>
<a name="l01247"></a>01247                               + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(j) + <span class="stringliteral">&quot; ) not defined.&quot;</span>);
<a name="l01248"></a>01248             }
<a name="l01249"></a>01249         }
<a name="l01250"></a>01250   }
<a name="l01251"></a>01251 
<a name="l01252"></a>01252 
<a name="l01253"></a>01253   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01254"></a>01254            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01255"></a>01255   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01256"></a>01256            <span class="keyword">const</span> Matrix&lt;T1, Prop1, RowMajor, Allocator1&gt;&amp; A,
<a name="l01257"></a>01257            Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;&amp; B) <span class="keywordflow">throw</span>()
<a name="l01258"></a>01258   {
<a name="l01259"></a>01259     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void Add(const T0&amp; alpha,&quot;</span>
<a name="l01260"></a>01260                     <span class="stringliteral">&quot;const Matrix&lt;T1, Prop1, RowMajor, Allocator1&gt;&amp; A,&quot;</span>
<a name="l01261"></a>01261                     <span class="stringliteral">&quot;Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;&amp; B)&quot;</span>);
<a name="l01262"></a>01262   }
<a name="l01263"></a>01263 
<a name="l01264"></a>01264 
<a name="l01265"></a>01265   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01266"></a>01266            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01267"></a>01267   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01268"></a>01268            <span class="keyword">const</span> Matrix&lt;T1, Prop1, ColMajor, Allocator1&gt;&amp; A,
<a name="l01269"></a>01269            Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;&amp; B) <span class="keywordflow">throw</span>()
<a name="l01270"></a>01270   {
<a name="l01271"></a>01271     <span class="keywordflow">throw</span> Undefined(<span class="stringliteral">&quot;void Add(const T0&amp; alpha,&quot;</span>
<a name="l01272"></a>01272                     <span class="stringliteral">&quot;const Matrix&lt;T1, Prop1, RowMajor, Allocator1&gt;&amp; A,&quot;</span>
<a name="l01273"></a>01273                     <span class="stringliteral">&quot;Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;&amp; B)&quot;</span>);
<a name="l01274"></a>01274   }
<a name="l01275"></a>01275 
<a name="l01276"></a>01276 
<a name="l01277"></a>01277   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01278"></a>01278            <span class="keyword">class </span>T2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01279"></a>01279   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01280"></a>01280            <span class="keyword">const</span> Matrix&lt;T1, Symmetric, Storage1, Allocator1&gt;&amp; A,
<a name="l01281"></a>01281            Matrix&lt;T2, Symmetric, Storage2, Allocator2&gt;&amp; B)
<a name="l01282"></a>01282   {
<a name="l01283"></a>01283     <span class="keywordtype">int</span> i, j;
<a name="l01284"></a>01284     <span class="keywordflow">for</span> (i = 0; i &lt; A.GetM(); i++)
<a name="l01285"></a>01285       <span class="keywordflow">for</span> (j = i; j &lt; A.GetN(); j++)
<a name="l01286"></a>01286         B(i, j) += alpha * A(i, j);
<a name="l01287"></a>01287   }
<a name="l01288"></a>01288 
<a name="l01289"></a>01289 
<a name="l01291"></a>01291 
<a name="l01298"></a>01298   <span class="keyword">template</span>&lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Allocator1,
<a name="l01299"></a>01299            <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01300"></a><a class="code" href="namespace_seldon.php#ae4c4fbf36e7c70892d309b5f96de7ba3">01300</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a06bbccf90395f0ab02d6b5c00c392fb3" title="Adds two matrices.">Add</a>(<span class="keyword">const</span> T0&amp; alpha,
<a name="l01301"></a>01301            <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, RowSparse, Allocator1&gt;</a>&amp; A,
<a name="l01302"></a>01302            <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, RowSparse, Allocator2&gt;</a>&amp; B)
<a name="l01303"></a>01303   {
<a name="l01304"></a>01304 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01305"></a>01305 <span class="preprocessor"></span>    <span class="keywordflow">if</span> (A.GetM() != B.GetM() || A.GetN() != B.GetN())
<a name="l01306"></a>01306       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;Add(alpha, const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l01307"></a>01307                      <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B)&quot;</span>,
<a name="l01308"></a>01308                      <span class="stringliteral">&quot;Unable to add a &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="stringliteral">&quot; x &quot;</span>
<a name="l01309"></a>01309                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN()) + <span class="stringliteral">&quot; matrix with a &quot;</span>
<a name="l01310"></a>01310                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM()) + <span class="stringliteral">&quot; x &quot;</span> + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN())
<a name="l01311"></a>01311                      + <span class="stringliteral">&quot; matrix.&quot;</span>);
<a name="l01312"></a>01312 <span class="preprocessor">#endif</span>
<a name="l01313"></a>01313 <span class="preprocessor"></span>
<a name="l01314"></a>01314     <span class="keywordtype">int</span> i = 0;
<a name="l01315"></a>01315     <span class="keywordtype">int</span> j = 0;
<a name="l01316"></a>01316     <span class="keywordtype">int</span> k;
<a name="l01317"></a>01317 
<a name="l01318"></a>01318     <span class="keywordflow">if</span> (A.GetNonZeros() == B.GetNonZeros())
<a name="l01319"></a>01319       <span class="comment">// A and B might have the same structure.</span>
<a name="l01320"></a>01320       {
<a name="l01321"></a>01321         <span class="comment">// Loop over all non-zeros. If the structures of A and B differ at any</span>
<a name="l01322"></a>01322         <span class="comment">// time, the loop is broken and a different strategy is undertaken.</span>
<a name="l01323"></a>01323         <span class="keywordflow">for</span> (i = 0; i &lt; A.GetM(); i++)
<a name="l01324"></a>01324           <span class="keywordflow">if</span> (A.GetPtr()[i + 1] == B.GetPtr()[i + 1])
<a name="l01325"></a>01325             {
<a name="l01326"></a>01326               <span class="keywordflow">for</span> (j = A.GetPtr()[i]; j &lt; A.GetPtr()[i + 1]; j++)
<a name="l01327"></a>01327                 <span class="keywordflow">if</span> (A.GetInd()[j] == B.GetInd()[j])
<a name="l01328"></a>01328                   B.GetData()[j] += alpha * A.GetData()[j];
<a name="l01329"></a>01329                 <span class="keywordflow">else</span>
<a name="l01330"></a>01330                   <span class="keywordflow">break</span>;
<a name="l01331"></a>01331               <span class="keywordflow">if</span> (j != A.GetPtr()[i + 1])
<a name="l01332"></a>01332                 <span class="keywordflow">break</span>;
<a name="l01333"></a>01333             }
<a name="l01334"></a>01334           <span class="keywordflow">else</span>
<a name="l01335"></a>01335             <span class="keywordflow">break</span>;
<a name="l01336"></a>01336         <span class="comment">// Success: A and B have the same structure.</span>
<a name="l01337"></a>01337         <span class="keywordflow">if</span> (i == A.GetM())
<a name="l01338"></a>01338           <span class="keywordflow">return</span>;
<a name="l01339"></a>01339       }
<a name="l01340"></a>01340 
<a name="l01341"></a>01341     <span class="comment">// The addition is performed row by row in the following lines. Thus the</span>
<a name="l01342"></a>01342     <span class="comment">// additions already performed in the current line, if started, should be</span>
<a name="l01343"></a>01343     <span class="comment">// canceled.</span>
<a name="l01344"></a>01344     <span class="keywordflow">for</span> (k = A.GetPtr()[i]; k &lt; j; k++)
<a name="l01345"></a>01345       <span class="keywordflow">if</span> (A.GetInd()[k] == B.GetInd()[k])
<a name="l01346"></a>01346         B.GetData()[k] -= alpha * A.GetData()[k];
<a name="l01347"></a>01347 
<a name="l01348"></a>01348     <span class="comment">// Number of non zero entries currently found.</span>
<a name="l01349"></a>01349     <span class="keywordtype">int</span> Nnonzero = A.GetPtr()[i];
<a name="l01350"></a>01350 
<a name="l01351"></a>01351     <span class="comment">// A and B do not have the same structure. An intermediate matrix will be</span>
<a name="l01352"></a>01352     <span class="comment">// needed. The first i rows have already been added. These computations</span>
<a name="l01353"></a>01353     <span class="comment">// should be preserved.</span>
<a name="l01354"></a>01354     <span class="keywordtype">int</span>* c_ptr = NULL;
<a name="l01355"></a>01355     <span class="keywordtype">int</span>* c_ind = NULL;
<a name="l01356"></a>01356     T2* c_data = NULL;
<a name="l01357"></a>01357 
<a name="l01358"></a>01358 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01359"></a>01359 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01360"></a>01360       {
<a name="l01361"></a>01361 <span class="preprocessor">#endif</span>
<a name="l01362"></a>01362 <span class="preprocessor"></span>
<a name="l01363"></a>01363         c_ptr = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>(calloc(A.GetM() + 1, <span class="keyword">sizeof</span>(int)));
<a name="l01364"></a>01364 
<a name="l01365"></a>01365 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01366"></a>01366 <span class="preprocessor"></span>      }
<a name="l01367"></a>01367     <span class="keywordflow">catch</span> (...)
<a name="l01368"></a>01368       {
<a name="l01369"></a>01369         c_ptr = NULL;
<a name="l01370"></a>01370       }
<a name="l01371"></a>01371 
<a name="l01372"></a>01372     <span class="keywordflow">if</span> (c_ptr == NULL)
<a name="l01373"></a>01373       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Add(alpha, const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l01374"></a>01374                      <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B)&quot;</span>,
<a name="l01375"></a>01375                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l01376"></a>01376                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM() + 1) + <span class="stringliteral">&quot; integers.&quot;</span>);
<a name="l01377"></a>01377 <span class="preprocessor">#endif</span>
<a name="l01378"></a>01378 <span class="preprocessor"></span>
<a name="l01379"></a>01379 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01380"></a>01380 <span class="preprocessor"></span>    <span class="keywordflow">try</span>
<a name="l01381"></a>01381       {
<a name="l01382"></a>01382 <span class="preprocessor">#endif</span>
<a name="l01383"></a>01383 <span class="preprocessor"></span>
<a name="l01384"></a>01384         <span class="comment">// Reallocates &#39;c_ind&#39; and &#39;c_data&#39; for the first i rows.</span>
<a name="l01385"></a>01385         c_ind = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>
<a name="l01386"></a>01386           (realloc(reinterpret_cast&lt;void*&gt;(c_ind), Nnonzero * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l01387"></a>01387         c_data = <span class="keyword">reinterpret_cast&lt;</span>T2*<span class="keyword">&gt;</span>
<a name="l01388"></a>01388           (B.GetAllocator().reallocate(c_data, Nnonzero));
<a name="l01389"></a>01389 
<a name="l01390"></a>01390 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01391"></a>01391 <span class="preprocessor"></span>      }
<a name="l01392"></a>01392     <span class="keywordflow">catch</span> (...)
<a name="l01393"></a>01393       {
<a name="l01394"></a>01394         c_ind = NULL;
<a name="l01395"></a>01395         c_data = NULL;
<a name="l01396"></a>01396       }
<a name="l01397"></a>01397 
<a name="l01398"></a>01398     <span class="keywordflow">if</span> ((c_ind == NULL || c_data == NULL)
<a name="l01399"></a>01399         &amp;&amp; Nnonzero != 0)
<a name="l01400"></a>01400       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Add(alpha, const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l01401"></a>01401                      <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B)&quot;</span>,
<a name="l01402"></a>01402                      <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l01403"></a>01403                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nnonzero) + <span class="stringliteral">&quot; integers and for an array of &quot;</span>
<a name="l01404"></a>01404                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(T2) * Nnonzero) + <span class="stringliteral">&quot; bytes.&quot;</span>);
<a name="l01405"></a>01405 <span class="preprocessor">#endif</span>
<a name="l01406"></a>01406 <span class="preprocessor"></span>
<a name="l01407"></a>01407     <span class="comment">// The pointers of the first i rows are correct.</span>
<a name="l01408"></a>01408     <span class="keywordflow">for</span> (k = 0; k &lt; i + 1; k++)
<a name="l01409"></a>01409       c_ptr[k] = B.GetPtr()[k];
<a name="l01410"></a>01410 
<a name="l01411"></a>01411     <span class="comment">// Copies the elements from the first i rows, as they were already added.</span>
<a name="l01412"></a>01412     <span class="keywordflow">for</span> (k = 0; k &lt; Nnonzero; k++)
<a name="l01413"></a>01413       {
<a name="l01414"></a>01414         c_ind[k] = B.GetInd()[k];
<a name="l01415"></a>01415         c_data[k] = B.GetData()[k];
<a name="l01416"></a>01416       }
<a name="l01417"></a>01417 
<a name="l01418"></a>01418     <span class="keywordtype">int</span> Nnonzero_row_max;
<a name="l01419"></a>01419     <span class="keywordtype">int</span> Nnonzero_max;
<a name="l01420"></a>01420     <span class="keywordtype">int</span> ja, jb(0), ka, kb;
<a name="l01421"></a>01421     <span class="comment">// Now deals with the remaining lines.</span>
<a name="l01422"></a>01422     <span class="keywordflow">for</span> (; i &lt; A.GetM(); i++)
<a name="l01423"></a>01423       {
<a name="l01424"></a>01424         Nnonzero_row_max = A.GetPtr()[i + 1] - A.GetPtr()[i]
<a name="l01425"></a>01425           + B.GetPtr()[i + 1] - B.GetPtr()[i];
<a name="l01426"></a>01426         <span class="comment">// Maximum number of non zero entries up to row i.</span>
<a name="l01427"></a>01427         Nnonzero_max = Nnonzero + Nnonzero_row_max;
<a name="l01428"></a>01428 
<a name="l01429"></a>01429 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01430"></a>01430 <span class="preprocessor"></span>        <span class="keywordflow">try</span>
<a name="l01431"></a>01431           {
<a name="l01432"></a>01432 <span class="preprocessor">#endif</span>
<a name="l01433"></a>01433 <span class="preprocessor"></span>
<a name="l01434"></a>01434             c_ind = <span class="keyword">reinterpret_cast&lt;</span><span class="keywordtype">int</span>*<span class="keyword">&gt;</span>
<a name="l01435"></a>01435               (realloc(reinterpret_cast&lt;void*&gt;(c_ind),
<a name="l01436"></a>01436                        Nnonzero_max * <span class="keyword">sizeof</span>(<span class="keywordtype">int</span>)));
<a name="l01437"></a>01437             c_data = <span class="keyword">reinterpret_cast&lt;</span>T2*<span class="keyword">&gt;</span>
<a name="l01438"></a>01438               (B.GetAllocator().reallocate(c_data, Nnonzero_max));
<a name="l01439"></a>01439 
<a name="l01440"></a>01440 <span class="preprocessor">#ifdef SELDON_CHECK_MEMORY</span>
<a name="l01441"></a>01441 <span class="preprocessor"></span>          }
<a name="l01442"></a>01442         <span class="keywordflow">catch</span> (...)
<a name="l01443"></a>01443           {
<a name="l01444"></a>01444             c_ind = NULL;
<a name="l01445"></a>01445             c_data = NULL;
<a name="l01446"></a>01446           }
<a name="l01447"></a>01447 
<a name="l01448"></a>01448         <span class="keywordflow">if</span> ((c_ind == NULL || c_data == NULL)
<a name="l01449"></a>01449             &amp;&amp; Nnonzero_max != 0)
<a name="l01450"></a>01450           <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_no_memory.php">NoMemory</a>(<span class="stringliteral">&quot;Add(alpha, const Matrix&lt;RowSparse&gt;&amp; A, &quot;</span>
<a name="l01451"></a>01451                          <span class="stringliteral">&quot;Matrix&lt;RowSparse&gt;&amp; B)&quot;</span>,
<a name="l01452"></a>01452                          <span class="stringliteral">&quot;Unable to allocate memory for an array of &quot;</span>
<a name="l01453"></a>01453                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(Nnonzero_max) + <span class="stringliteral">&quot; integers and for an &quot;</span>
<a name="l01454"></a>01454                          <span class="stringliteral">&quot;array of &quot;</span>
<a name="l01455"></a>01455                          + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(<span class="keyword">sizeof</span>(T2) * Nnonzero_max) + <span class="stringliteral">&quot; bytes.&quot;</span>);
<a name="l01456"></a>01456 <span class="preprocessor">#endif</span>
<a name="l01457"></a>01457 <span class="preprocessor"></span>
<a name="l01458"></a>01458         kb = B.GetPtr()[i];
<a name="l01459"></a>01459         <span class="keywordflow">if</span> (kb &lt; B.GetPtr()[i + 1])
<a name="l01460"></a>01460           jb = B.GetInd()[kb];
<a name="l01461"></a>01461         <span class="keywordflow">for</span> (ka = A.GetPtr()[i]; ka &lt; A.GetPtr()[i + 1]; ka++)
<a name="l01462"></a>01462           {
<a name="l01463"></a>01463             ja = A.GetInd()[ka];
<a name="l01464"></a>01464             <span class="keywordflow">while</span> (kb &lt; B.GetPtr()[i + 1] &amp;&amp; jb &lt; ja)
<a name="l01465"></a>01465               <span class="comment">// For all elements in B that are before the ka-th element of A.</span>
<a name="l01466"></a>01466               {
<a name="l01467"></a>01467                 c_ind[Nnonzero] = jb;
<a name="l01468"></a>01468                 c_data[Nnonzero] = B.GetData()[kb];
<a name="l01469"></a>01469                 kb++;
<a name="l01470"></a>01470                 <span class="keywordflow">if</span> (kb &lt; B.GetPtr()[i + 1])
<a name="l01471"></a>01471                   jb = B.GetInd()[kb];
<a name="l01472"></a>01472                 Nnonzero++;
<a name="l01473"></a>01473               }
<a name="l01474"></a>01474 
<a name="l01475"></a>01475             <span class="keywordflow">if</span> (kb &lt; B.GetPtr()[i + 1] &amp;&amp; ja == jb)
<a name="l01476"></a>01476               <span class="comment">// The element in A is also in B.</span>
<a name="l01477"></a>01477               {
<a name="l01478"></a>01478                 c_ind[Nnonzero] = jb;
<a name="l01479"></a>01479                 c_data[Nnonzero] = B.GetData()[kb] + alpha * A.GetData()[ka];
<a name="l01480"></a>01480                 kb++;
<a name="l01481"></a>01481                 <span class="keywordflow">if</span> (kb &lt; B.GetPtr()[i + 1])
<a name="l01482"></a>01482                   jb = B.GetInd()[kb];
<a name="l01483"></a>01483               }
<a name="l01484"></a>01484             <span class="keywordflow">else</span>
<a name="l01485"></a>01485               {
<a name="l01486"></a>01486                 c_ind[Nnonzero] = ja;
<a name="l01487"></a>01487                 c_data[Nnonzero] = alpha * A.GetData()[ka];
<a name="l01488"></a>01488               }
<a name="l01489"></a>01489             Nnonzero++;
<a name="l01490"></a>01490           }
<a name="l01491"></a>01491 
<a name="l01492"></a>01492         <span class="comment">// The remaining elements from B.</span>
<a name="l01493"></a>01493         <span class="keywordflow">while</span> (kb &lt; B.GetPtr()[i + 1])
<a name="l01494"></a>01494           {
<a name="l01495"></a>01495             c_ind[Nnonzero] = jb;
<a name="l01496"></a>01496             c_data[Nnonzero] = B.GetData()[kb];
<a name="l01497"></a>01497             kb++;
<a name="l01498"></a>01498             <span class="keywordflow">if</span> (kb &lt; B.GetPtr()[i + 1])
<a name="l01499"></a>01499               jb = B.GetInd()[kb];
<a name="l01500"></a>01500             Nnonzero++;
<a name="l01501"></a>01501           }
<a name="l01502"></a>01502 
<a name="l01503"></a>01503         c_ptr[i + 1] = Nnonzero;
<a name="l01504"></a>01504       }
<a name="l01505"></a>01505 
<a name="l01506"></a>01506     B.SetData(B.GetM(), B.GetN(), Nnonzero, c_data, c_ptr, c_ind);
<a name="l01507"></a>01507   }
<a name="l01508"></a>01508 
<a name="l01509"></a>01509 
<a name="l01510"></a>01510   <span class="comment">// ADD //</span>
<a name="l01512"></a>01512 <span class="comment"></span>
<a name="l01513"></a>01513 
<a name="l01514"></a>01514 
<a name="l01516"></a>01516   <span class="comment">// GETLU //</span>
<a name="l01517"></a>01517 
<a name="l01518"></a>01518 
<a name="l01520"></a>01520 
<a name="l01530"></a>01530   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0, <span class="keyword">class</span> Prop0, <span class="keyword">class</span> Storage0, <span class="keyword">class</span> Allocator0&gt;
<a name="l01531"></a><a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00">01531</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a76d3f187a877c6c58245b0716e1adc00" title="Returns the LU factorization of a matrix.">GetLU</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A)
<a name="l01532"></a>01532   {
<a name="l01533"></a>01533     <span class="keywordtype">int</span> i, p, q, k;
<a name="l01534"></a>01534     T0 temp;
<a name="l01535"></a>01535 
<a name="l01536"></a>01536     <span class="keywordtype">int</span> ma = A.GetM();
<a name="l01537"></a>01537 
<a name="l01538"></a>01538 <span class="preprocessor">#ifdef SELDON_CHECK_BOUNDS</span>
<a name="l01539"></a>01539 <span class="preprocessor"></span>    <span class="keywordtype">int</span> na = A.GetN();
<a name="l01540"></a>01540     <span class="keywordflow">if</span> (na != ma)
<a name="l01541"></a>01541       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(<span class="stringliteral">&quot;GetLU(A)&quot;</span>, <span class="stringliteral">&quot;The matrix must be squared.&quot;</span>);
<a name="l01542"></a>01542 <span class="preprocessor">#endif</span>
<a name="l01543"></a>01543 <span class="preprocessor"></span>
<a name="l01544"></a>01544     <span class="keywordflow">for</span> (i = 0; i &lt; ma; i++)
<a name="l01545"></a>01545       {
<a name="l01546"></a>01546         <span class="keywordflow">for</span> (p = i; p &lt; ma; p++)
<a name="l01547"></a>01547           {
<a name="l01548"></a>01548             temp = 0;
<a name="l01549"></a>01549             <span class="keywordflow">for</span> (k = 0; k &lt; i; k++)
<a name="l01550"></a>01550               temp += A(p, k) * A(k, i);
<a name="l01551"></a>01551             A(p, i) -= temp;
<a name="l01552"></a>01552           }
<a name="l01553"></a>01553         <span class="keywordflow">for</span> (q = i+1; q &lt; ma; q++)
<a name="l01554"></a>01554           {
<a name="l01555"></a>01555             temp = 0;
<a name="l01556"></a>01556             <span class="keywordflow">for</span> (k = 0; k &lt; i; k++)
<a name="l01557"></a>01557               temp += A(i, k) * A(k, q);
<a name="l01558"></a>01558             A(i, q) = (A(i,q ) - temp) / A(i, i);
<a name="l01559"></a>01559           }
<a name="l01560"></a>01560       }
<a name="l01561"></a>01561   }
<a name="l01562"></a>01562 
<a name="l01563"></a>01563 
<a name="l01564"></a>01564   <span class="comment">// GETLU //</span>
<a name="l01566"></a>01566 <span class="comment"></span>
<a name="l01567"></a>01567 
<a name="l01568"></a>01568 
<a name="l01570"></a>01570   <span class="comment">// CHECKDIM //</span>
<a name="l01571"></a>01571 
<a name="l01572"></a>01572 
<a name="l01574"></a>01574 
<a name="l01583"></a>01583   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01584"></a>01584             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01585"></a>01585             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01586"></a><a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45">01586</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01587"></a>01587                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01588"></a>01588                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; C,
<a name="l01589"></a>01589                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01590"></a>01590   {
<a name="l01591"></a>01591     <span class="keywordflow">if</span> (B.GetM() != A.GetN() || C.GetM() != A.GetM() || B.GetN() != C.GetN())
<a name="l01592"></a>01592       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation A B + C -&gt; C not permitted:&quot;</span>)
<a name="l01593"></a>01593                      + string(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01594"></a>01594                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01595"></a>01595                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01596"></a>01596                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01597"></a>01597                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     C (&quot;</span>)
<a name="l01598"></a>01598                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;C) + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetM())
<a name="l01599"></a>01599                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01600"></a>01600   }
<a name="l01601"></a>01601 
<a name="l01602"></a>01602 
<a name="l01604"></a>01604 
<a name="l01614"></a>01614   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01615"></a>01615             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01616"></a>01616             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01617"></a><a class="code" href="namespace_seldon.php#a3a9fe4d23ebbb768bbc1c9496d87c978">01617</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>&amp; side,
<a name="l01618"></a>01618                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01619"></a>01619                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01620"></a>01620                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; C,
<a name="l01621"></a>01621                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01622"></a>01622   {
<a name="l01623"></a>01623     <span class="keywordflow">if</span> ( <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>(side).Left() &amp;&amp;
<a name="l01624"></a>01624          (B.GetM() != A.GetN() || C.GetM() != A.GetM()
<a name="l01625"></a>01625           || B.GetN() != C.GetN()) )
<a name="l01626"></a>01626       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation A B + C -&gt; C not permitted:&quot;</span>)
<a name="l01627"></a>01627                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01628"></a>01628                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01629"></a>01629                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01630"></a>01630                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01631"></a>01631                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     C (&quot;</span>)
<a name="l01632"></a>01632                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;C) + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetM())
<a name="l01633"></a>01633                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01634"></a>01634     <span class="keywordflow">else</span> <span class="keywordflow">if</span> ( <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>(side).Right() &amp;&amp;
<a name="l01635"></a>01635               (B.GetN() != A.GetM() || C.GetM() != B.GetM()
<a name="l01636"></a>01636                || A.GetN() != C.GetN()) )
<a name="l01637"></a>01637       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation B A + C -&gt; C not permitted:&quot;</span>)
<a name="l01638"></a>01638                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01639"></a>01639                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01640"></a>01640                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01641"></a>01641                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01642"></a>01642                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     C (&quot;</span>)
<a name="l01643"></a>01643                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;C) + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetM())
<a name="l01644"></a>01644                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01645"></a>01645   }
<a name="l01646"></a>01646 
<a name="l01647"></a>01647 
<a name="l01649"></a>01649 
<a name="l01659"></a>01659   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01660"></a>01660             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01661"></a><a class="code" href="namespace_seldon.php#a1dd90de7928e2d9c20a5d337556bafef">01661</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l01662"></a>01662                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01663"></a>01663                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransB,
<a name="l01664"></a>01664                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01665"></a>01665                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01666"></a>01666   {
<a name="l01667"></a>01667     <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> status_A(TransA);
<a name="l01668"></a>01668     <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a> status_B(TransB);
<a name="l01669"></a>01669     <span class="keywordtype">string</span> op;
<a name="l01670"></a>01670     <span class="keywordflow">if</span> (status_A.Trans())
<a name="l01671"></a>01671       op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;A&#39;&quot;</span>);
<a name="l01672"></a>01672     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (status_A.ConjTrans())
<a name="l01673"></a>01673       op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;A*&quot;</span>);
<a name="l01674"></a>01674     <span class="keywordflow">else</span>
<a name="l01675"></a>01675       op = string(<span class="stringliteral">&quot;A&quot;</span>);
<a name="l01676"></a>01676     <span class="keywordflow">if</span> (status_B.Trans())
<a name="l01677"></a>01677       op += <span class="keywordtype">string</span>(<span class="stringliteral">&quot; B&#39;&quot;</span>);
<a name="l01678"></a>01678     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (status_B.ConjTrans())
<a name="l01679"></a>01679       op += <span class="keywordtype">string</span>(<span class="stringliteral">&quot; B*&quot;</span>);
<a name="l01680"></a>01680     <span class="keywordflow">else</span>
<a name="l01681"></a>01681       op += string(<span class="stringliteral">&quot; B&quot;</span>);
<a name="l01682"></a>01682     op = string(<span class="stringliteral">&quot;Operation &quot;</span>) + op + string(<span class="stringliteral">&quot; not permitted:&quot;</span>);
<a name="l01683"></a>01683     <span class="keywordflow">if</span> (B.GetM(status_B) != A.GetN(status_A))
<a name="l01684"></a>01684       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, op
<a name="l01685"></a>01685                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01686"></a>01686                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01687"></a>01687                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01688"></a>01688                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01689"></a>01689                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01690"></a>01690   }
<a name="l01691"></a>01691 
<a name="l01692"></a>01692 
<a name="l01694"></a>01694 
<a name="l01705"></a>01705   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01706"></a>01706             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1,
<a name="l01707"></a>01707             <span class="keyword">class </span>T2, <span class="keyword">class </span>Prop2, <span class="keyword">class </span>Storage2, <span class="keyword">class </span>Allocator2&gt;
<a name="l01708"></a><a class="code" href="namespace_seldon.php#a5a73b3f0b4857b317d7e48e3f921dafb">01708</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransA,
<a name="l01709"></a>01709                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01710"></a>01710                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_transpose.php">SeldonTranspose</a>&amp; TransB,
<a name="l01711"></a>01711                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01712"></a>01712                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T2, Prop2, Storage2, Allocator2&gt;</a>&amp; C,
<a name="l01713"></a>01713                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01714"></a>01714   {
<a name="l01715"></a>01715     <span class="keywordtype">string</span> op;
<a name="l01716"></a>01716     <span class="keywordflow">if</span> (TransA.Trans())
<a name="l01717"></a>01717       op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;A&#39;&quot;</span>);
<a name="l01718"></a>01718     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (TransA.ConjTrans())
<a name="l01719"></a>01719       op = <span class="keywordtype">string</span>(<span class="stringliteral">&quot;A*&quot;</span>);
<a name="l01720"></a>01720     <span class="keywordflow">else</span>
<a name="l01721"></a>01721       op = string(<span class="stringliteral">&quot;A&quot;</span>);
<a name="l01722"></a>01722     <span class="keywordflow">if</span> (TransB.Trans())
<a name="l01723"></a>01723       op += <span class="keywordtype">string</span>(<span class="stringliteral">&quot; B&#39; + C&quot;</span>);
<a name="l01724"></a>01724     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (TransB.ConjTrans())
<a name="l01725"></a>01725       op += <span class="keywordtype">string</span>(<span class="stringliteral">&quot; B* + C&quot;</span>);
<a name="l01726"></a>01726     <span class="keywordflow">else</span>
<a name="l01727"></a>01727       op += string(<span class="stringliteral">&quot; B + C&quot;</span>);
<a name="l01728"></a>01728     op = string(<span class="stringliteral">&quot;Operation &quot;</span>) + op + string(<span class="stringliteral">&quot; not permitted:&quot;</span>);
<a name="l01729"></a>01729     <span class="keywordflow">if</span> (B.GetM(TransB) != A.GetN(TransA) || C.GetM() != A.GetM(TransA)
<a name="l01730"></a>01730         || B.GetN(TransB) != C.GetN())
<a name="l01731"></a>01731       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, op
<a name="l01732"></a>01732                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01733"></a>01733                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01734"></a>01734                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01735"></a>01735                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01736"></a>01736                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     C (&quot;</span>)
<a name="l01737"></a>01737                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;C) + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetM())
<a name="l01738"></a>01738                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(C.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01739"></a>01739   }
<a name="l01740"></a>01740 
<a name="l01741"></a>01741 
<a name="l01743"></a>01743 
<a name="l01751"></a>01751   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01752"></a>01752             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01753"></a><a class="code" href="namespace_seldon.php#adad23d6286f3c1a7b916f9b825f9ec95">01753</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01754"></a>01754                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01755"></a>01755                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01756"></a>01756   {
<a name="l01757"></a>01757     <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(SeldonLeft, A, B, function);
<a name="l01758"></a>01758   }
<a name="l01759"></a>01759 
<a name="l01760"></a>01760 
<a name="l01762"></a>01762 
<a name="l01771"></a>01771   <span class="keyword">template</span> &lt;<span class="keyword">class </span>T0, <span class="keyword">class </span>Prop0, <span class="keyword">class </span>Storage0, <span class="keyword">class </span>Allocator0,
<a name="l01772"></a>01772             <span class="keyword">class </span>T1, <span class="keyword">class </span>Prop1, <span class="keyword">class </span>Storage1, <span class="keyword">class </span>Allocator1&gt;
<a name="l01773"></a><a class="code" href="namespace_seldon.php#abda6910aeb4b903e350cf592c48bd303">01773</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#aedf2b5db8c95a4849983a633d795ad45" title="Checks the compatibility of the dimensions.">CheckDim</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_seldon_side.php">SeldonSide</a>&amp; side,
<a name="l01774"></a>01774                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T0, Prop0, Storage0, Allocator0&gt;</a>&amp; A,
<a name="l01775"></a>01775                 <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T1, Prop1, Storage1, Allocator1&gt;</a>&amp; B,
<a name="l01776"></a>01776                 <span class="keywordtype">string</span> function = <span class="stringliteral">&quot;&quot;</span>)
<a name="l01777"></a>01777   {
<a name="l01778"></a>01778     <span class="keywordflow">if</span> (side.Left() &amp;&amp; B.GetM() != A.GetN())
<a name="l01779"></a>01779       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation A B not permitted:&quot;</span>)
<a name="l01780"></a>01780                      + string(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01781"></a>01781                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01782"></a>01782                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01783"></a>01783                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01784"></a>01784                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01785"></a>01785     <span class="keywordflow">else</span> <span class="keywordflow">if</span> (side.Right() &amp;&amp; B.GetN() != A.GetM())
<a name="l01786"></a>01786       <span class="keywordflow">throw</span> <a class="code" href="class_seldon_1_1_wrong_dim.php">WrongDim</a>(function, <span class="keywordtype">string</span>(<span class="stringliteral">&quot;Operation B A not permitted:&quot;</span>)
<a name="l01787"></a>01787                      + string(<span class="stringliteral">&quot;\n     A (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;A) + string(<span class="stringliteral">&quot;) is a &quot;</span>)
<a name="l01788"></a>01788                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetM()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(A.GetN())
<a name="l01789"></a>01789                      + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix;\n     B (&quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(&amp;B)
<a name="l01790"></a>01790                      + string(<span class="stringliteral">&quot;) is a &quot;</span>) + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetM())  + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; x &quot;</span>)
<a name="l01791"></a>01791                      + <a class="code" href="namespace_seldon.php#a945e873b0804ec87d790bfac758923e3" title="Converts most types to string.">to_str</a>(B.GetN()) + <span class="keywordtype">string</span>(<span class="stringliteral">&quot; matrix.&quot;</span>));
<a name="l01792"></a>01792   }
<a name="l01793"></a>01793 
<a name="l01794"></a>01794 
<a name="l01795"></a>01795   <span class="comment">// CHECKDIM //</span>
<a name="l01797"></a>01797 <span class="comment"></span>
<a name="l01798"></a>01798 
<a name="l01800"></a>01800   <span class="comment">// NORMS //</span>
<a name="l01801"></a>01801 
<a name="l01802"></a>01802 
<a name="l01804"></a>01804 
<a name="l01808"></a>01808   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01809"></a><a class="code" href="namespace_seldon.php#ab03e0763091dcf33a7f2d59a2cfdefdc">01809</a>   T <a class="code" href="namespace_seldon.php#ab03e0763091dcf33a7f2d59a2cfdefdc" title="Returns the maximum (in absolute value) of a matrix.">MaxAbs</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01810"></a>01810   {
<a name="l01811"></a>01811     T res(0);
<a name="l01812"></a>01812     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01813"></a>01813       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01814"></a>01814         res = max(res, abs(A(i, j)) );
<a name="l01815"></a>01815 
<a name="l01816"></a>01816     <span class="keywordflow">return</span> res;
<a name="l01817"></a>01817   }
<a name="l01818"></a>01818 
<a name="l01819"></a>01819 
<a name="l01821"></a>01821 
<a name="l01825"></a>01825   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01826"></a><a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c">01826</a>   T <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01827"></a>01827   {
<a name="l01828"></a>01828     T res(0), sum;
<a name="l01829"></a>01829     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01830"></a>01830       {
<a name="l01831"></a>01831         sum = T(0);
<a name="l01832"></a>01832         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01833"></a>01833           sum += abs( A(i, j) );
<a name="l01834"></a>01834 
<a name="l01835"></a>01835         res = max(res, sum);
<a name="l01836"></a>01836       }
<a name="l01837"></a>01837 
<a name="l01838"></a>01838     <span class="keywordflow">return</span> res;
<a name="l01839"></a>01839   }
<a name="l01840"></a>01840 
<a name="l01841"></a>01841 
<a name="l01843"></a>01843 
<a name="l01847"></a>01847   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01848"></a><a class="code" href="namespace_seldon.php#ad345433f1f078dbb4cdc2829b1811e50">01848</a>   T <a class="code" href="namespace_seldon.php#ad345433f1f078dbb4cdc2829b1811e50" title="Returns the infinity-norm of a matrix.">NormInf</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01849"></a>01849   {
<a name="l01850"></a>01850     T res(0), sum;
<a name="l01851"></a>01851     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01852"></a>01852       {
<a name="l01853"></a>01853         sum = T(0);
<a name="l01854"></a>01854         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01855"></a>01855           sum += abs( A(i, j) );
<a name="l01856"></a>01856 
<a name="l01857"></a>01857         res = max(res, sum);
<a name="l01858"></a>01858       }
<a name="l01859"></a>01859 
<a name="l01860"></a>01860     <span class="keywordflow">return</span> res;
<a name="l01861"></a>01861   }
<a name="l01862"></a>01862 
<a name="l01863"></a>01863 
<a name="l01865"></a>01865 
<a name="l01869"></a>01869   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01870"></a><a class="code" href="namespace_seldon.php#a48ee8348f12f27355d7b73c804574535">01870</a>   T <a class="code" href="namespace_seldon.php#ab03e0763091dcf33a7f2d59a2cfdefdc" title="Returns the maximum (in absolute value) of a matrix.">MaxAbs</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;complex&lt;T&gt;, Prop, Storage, Allocator&gt;&amp; A)
<a name="l01871"></a>01871   {
<a name="l01872"></a>01872     T res(0);
<a name="l01873"></a>01873     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01874"></a>01874       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01875"></a>01875         {
<a name="l01876"></a>01876           res = max(res, abs(A(i, j)) );
<a name="l01877"></a>01877         }
<a name="l01878"></a>01878 
<a name="l01879"></a>01879     <span class="keywordflow">return</span> res;
<a name="l01880"></a>01880   }
<a name="l01881"></a>01881 
<a name="l01882"></a>01882 
<a name="l01884"></a>01884 
<a name="l01888"></a>01888   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01889"></a><a class="code" href="namespace_seldon.php#a6f3f5f1a3a77dec2409166e2bd2bc715">01889</a>   T <a class="code" href="namespace_seldon.php#afefce2ae4cd3147f863955efddc4184c" title="Returns the 1-norm of a matrix.">Norm1</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;complex&lt;T&gt;, Prop, Storage, Allocator&gt;&amp; A)
<a name="l01890"></a>01890   {
<a name="l01891"></a>01891     T res(0), sum;
<a name="l01892"></a>01892     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01893"></a>01893       {
<a name="l01894"></a>01894         sum = T(0);
<a name="l01895"></a>01895         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01896"></a>01896           sum += abs( A(i, j) );
<a name="l01897"></a>01897 
<a name="l01898"></a>01898         res = max(res, sum);
<a name="l01899"></a>01899       }
<a name="l01900"></a>01900 
<a name="l01901"></a>01901     <span class="keywordflow">return</span> res;
<a name="l01902"></a>01902   }
<a name="l01903"></a>01903 
<a name="l01904"></a>01904 
<a name="l01906"></a>01906 
<a name="l01910"></a>01910   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01911"></a><a class="code" href="namespace_seldon.php#a18ce437500998e72b0753097b8fd0c81">01911</a>   T <a class="code" href="namespace_seldon.php#ad345433f1f078dbb4cdc2829b1811e50" title="Returns the infinity-norm of a matrix.">NormInf</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix</a>&lt;complex&lt;T&gt;, Prop, Storage, Allocator&gt;&amp; A)
<a name="l01912"></a>01912   {
<a name="l01913"></a>01913     T res(0), sum;
<a name="l01914"></a>01914     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; A.GetM(); i++)
<a name="l01915"></a>01915       {
<a name="l01916"></a>01916         sum = T(0);
<a name="l01917"></a>01917         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; A.GetN(); j++)
<a name="l01918"></a>01918           sum += abs( A(i, j) );
<a name="l01919"></a>01919 
<a name="l01920"></a>01920         res = max(res, sum);
<a name="l01921"></a>01921       }
<a name="l01922"></a>01922 
<a name="l01923"></a>01923     <span class="keywordflow">return</span> res;
<a name="l01924"></a>01924   }
<a name="l01925"></a>01925 
<a name="l01926"></a>01926 
<a name="l01927"></a>01927   <span class="comment">// NORMS //</span>
<a name="l01929"></a>01929 <span class="comment"></span>
<a name="l01930"></a>01930 
<a name="l01931"></a>01931 
<a name="l01933"></a>01933   <span class="comment">// TRANSPOSE //</span>
<a name="l01934"></a>01934 
<a name="l01935"></a>01935 
<a name="l01937"></a>01937   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l01938"></a><a class="code" href="namespace_seldon.php#ad685d5c5489bff59c0db91807e80938d">01938</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad685d5c5489bff59c0db91807e80938d" title="Matrix transposition.">Transpose</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l01939"></a>01939   {
<a name="l01940"></a>01940     <span class="keywordtype">int</span> m = A.GetM();
<a name="l01941"></a>01941     <span class="keywordtype">int</span> n = A.GetN();
<a name="l01942"></a>01942 
<a name="l01943"></a>01943     <span class="keywordflow">if</span> (m == n)
<a name="l01944"></a>01944       {
<a name="l01945"></a>01945         T tmp;
<a name="l01946"></a>01946         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l01947"></a>01947           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; i; j++)
<a name="l01948"></a>01948             {
<a name="l01949"></a>01949               tmp = A(i,j);
<a name="l01950"></a>01950               A(i,j) = A(j,i);
<a name="l01951"></a>01951               A(j,i) = tmp;
<a name="l01952"></a>01952             }
<a name="l01953"></a>01953       }
<a name="l01954"></a>01954     <span class="keywordflow">else</span>
<a name="l01955"></a>01955       {
<a name="l01956"></a>01956         <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> B;
<a name="l01957"></a>01957         B = A;
<a name="l01958"></a>01958         A.Reallocate(n,m);
<a name="l01959"></a>01959         <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l01960"></a>01960           <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 0; j &lt; n; j++)
<a name="l01961"></a>01961             A(j,i) = B(i,j);
<a name="l01962"></a>01962       }
<a name="l01963"></a>01963   }
<a name="l01964"></a>01964 
<a name="l01965"></a>01965 
<a name="l01967"></a>01967   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l01968"></a><a class="code" href="namespace_seldon.php#a6e5eb7fdae4e88b684cd30fc46149b3b">01968</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#ad685d5c5489bff59c0db91807e80938d" title="Matrix transposition.">Transpose</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, General, RowSparse, Allocator&gt;</a>&amp; A)
<a name="l01969"></a>01969   {
<a name="l01970"></a>01970     <span class="keywordtype">int</span> m = A.GetM();
<a name="l01971"></a>01971     <span class="keywordtype">int</span> n = A.GetN();
<a name="l01972"></a>01972     <span class="keywordtype">int</span> nnz = A.GetDataSize();
<a name="l01973"></a>01973     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ptr_T(n+1), ptr;
<a name="l01974"></a>01974     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; ind_T(nnz), ind;
<a name="l01975"></a>01975     <a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php" title="Full vector class.">Vector&lt;T, VectFull, Allocator&gt;</a> data_T(nnz), data;
<a name="l01976"></a>01976 
<a name="l01977"></a>01977     ptr.SetData(m+1, A.GetPtr());
<a name="l01978"></a>01978     ind.SetData(nnz,  A.GetInd());
<a name="l01979"></a>01979     data.SetData(nnz, A.GetData());
<a name="l01980"></a>01980 
<a name="l01981"></a>01981     ptr_T.Zero();
<a name="l01982"></a>01982     ind_T.Zero();
<a name="l01983"></a>01983     data_T.Zero();
<a name="l01984"></a>01984 
<a name="l01985"></a>01985     <span class="comment">// For each column j, computes number of its non-zeroes and stores it in</span>
<a name="l01986"></a>01986     <span class="comment">// ptr_T[j].</span>
<a name="l01987"></a>01987     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; nnz; i++)
<a name="l01988"></a>01988       ptr_T(ind(i) + 1)++;
<a name="l01989"></a>01989 
<a name="l01990"></a>01990     <span class="comment">// Computes the required number of non-zeroes ptr_T(j).</span>
<a name="l01991"></a>01991     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> j = 1; j &lt; n + 1; j++)
<a name="l01992"></a>01992       ptr_T(j) += ptr_T(j - 1);
<a name="l01993"></a>01993 
<a name="l01994"></a>01994     <a class="code" href="class_seldon_1_1_vector.php">Vector&lt;int, VectFull, CallocAlloc&lt;int&gt;</a> &gt; row_ind(n+1);
<a name="l01995"></a>01995     row_ind.Fill(0);
<a name="l01996"></a>01996     <span class="keywordflow">for</span> (<span class="keywordtype">int</span> i = 0; i &lt; m; i++)
<a name="l01997"></a>01997       <span class="keywordflow">for</span> (<span class="keywordtype">int</span> jp = ptr(i); jp &lt; ptr(i+1); jp++)
<a name="l01998"></a>01998         {
<a name="l01999"></a>01999           <span class="keywordtype">int</span> j = ind(jp);
<a name="l02000"></a>02000           <span class="keywordtype">int</span> k = ptr_T(j) + row_ind(j);
<a name="l02001"></a>02001           ++row_ind(j);
<a name="l02002"></a>02002           data_T(k) = data(jp);
<a name="l02003"></a>02003           ind_T(k) = i;
<a name="l02004"></a>02004         }
<a name="l02005"></a>02005 
<a name="l02006"></a>02006     A.SetData(n, m, data_T, ptr_T, ind_T);
<a name="l02007"></a>02007 
<a name="l02008"></a>02008     data.<a class="code" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a151dc47113d07ab24775733d0c46dc90" title="Clears the vector without releasing memory.">Nullify</a>();
<a name="l02009"></a>02009     ptr.Nullify();
<a name="l02010"></a>02010     ind.Nullify();
<a name="l02011"></a>02011   }
<a name="l02012"></a>02012 
<a name="l02013"></a>02013 
<a name="l02015"></a>02015   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02016"></a><a class="code" href="namespace_seldon.php#a389009bc578445ea109829807e97bfdd">02016</a>   <span class="keywordtype">void</span> <a class="code" href="namespace_seldon.php#a389009bc578445ea109829807e97bfdd" title="Matrix transposition and conjugation.">TransposeConj</a>(<a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l02017"></a>02017   {
<a name="l02018"></a>02018     <span class="keywordtype">int</span> i, j;
<a name="l02019"></a>02019 
<a name="l02020"></a>02020     <span class="keywordtype">int</span> m = A.GetM();
<a name="l02021"></a>02021     <span class="keywordtype">int</span> n = A.GetN();
<a name="l02022"></a>02022 
<a name="l02023"></a>02023     <span class="keywordflow">if</span> (m == n)
<a name="l02024"></a>02024       {
<a name="l02025"></a>02025         T tmp;
<a name="l02026"></a>02026         <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l02027"></a>02027           {
<a name="l02028"></a>02028             <span class="comment">// Extra-diagonal part.</span>
<a name="l02029"></a>02029             <span class="keywordflow">for</span> (j = 0; j &lt; i; j++)
<a name="l02030"></a>02030               {
<a name="l02031"></a>02031                 tmp = A(i, j);
<a name="l02032"></a>02032                 A(i, j) = conj(A(j, i));
<a name="l02033"></a>02033                 A(j, i) = conj(tmp);
<a name="l02034"></a>02034               }
<a name="l02035"></a>02035 
<a name="l02036"></a>02036             <span class="comment">// Diagonal part.</span>
<a name="l02037"></a>02037             A(i, i) = conj(A(i, i));
<a name="l02038"></a>02038           }
<a name="l02039"></a>02039       }
<a name="l02040"></a>02040     <span class="keywordflow">else</span>
<a name="l02041"></a>02041       {
<a name="l02042"></a>02042         <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a> B;
<a name="l02043"></a>02043         B = A;
<a name="l02044"></a>02044         A.Reallocate(n, m);
<a name="l02045"></a>02045         <span class="keywordflow">for</span> (i = 0; i &lt; m; i++)
<a name="l02046"></a>02046           <span class="keywordflow">for</span> (j = 0; j &lt; n; j++)
<a name="l02047"></a>02047             A(j, i) = conj(B(i, j));
<a name="l02048"></a>02048       }
<a name="l02049"></a>02049   }
<a name="l02050"></a>02050 
<a name="l02051"></a>02051 
<a name="l02052"></a>02052   <span class="comment">// TRANSPOSE //</span>
<a name="l02054"></a>02054 <span class="comment"></span>
<a name="l02055"></a>02055 
<a name="l02057"></a>02057   <span class="comment">// ISSYMMETRICMATRIX //</span>
<a name="l02058"></a>02058 
<a name="l02059"></a>02059 
<a name="l02061"></a>02061   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Prop, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02062"></a><a class="code" href="namespace_seldon.php#aa92697611346050d5157af5e936def3d">02062</a>   <span class="keywordtype">bool</span> <a class="code" href="namespace_seldon.php#aa92697611346050d5157af5e936def3d" title="returns true if the matrix is symmetric">IsSymmetricMatrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Prop, Storage, Allocator&gt;</a>&amp; A)
<a name="l02063"></a>02063   {
<a name="l02064"></a>02064     <span class="keywordflow">return</span> <span class="keyword">false</span>;
<a name="l02065"></a>02065   }
<a name="l02066"></a>02066 
<a name="l02067"></a>02067 
<a name="l02069"></a>02069   <span class="keyword">template</span>&lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Storage, <span class="keyword">class</span> Allocator&gt;
<a name="l02070"></a><a class="code" href="namespace_seldon.php#a696435a89b231c7b6a382705dfcf21fa">02070</a>   <span class="keywordtype">bool</span> <a class="code" href="namespace_seldon.php#aa92697611346050d5157af5e936def3d" title="returns true if the matrix is symmetric">IsSymmetricMatrix</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_matrix.php">Matrix&lt;T, Symmetric, Storage, Allocator&gt;</a>&amp; A)
<a name="l02071"></a>02071   {
<a name="l02072"></a>02072     <span class="keywordflow">return</span> <span class="keyword">true</span>;
<a name="l02073"></a>02073   }
<a name="l02074"></a>02074 
<a name="l02075"></a>02075 
<a name="l02076"></a>02076   <span class="comment">// ISSYMMETRICMATRIX //</span>
<a name="l02078"></a>02078 <span class="comment"></span>
<a name="l02079"></a>02079 } <span class="comment">// namespace Seldon.</span>
<a name="l02080"></a>02080 
<a name="l02081"></a>02081 <span class="preprocessor">#define SELDON_FILE_FUNCTIONS_MATRIX_CXX</span>
<a name="l02082"></a>02082 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
