<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>, including all inherited members.<table>
  <tr bgcolor="#f0f0f0"><td><b>access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a4bed47faffc46ab4a08c0869e5f7cdad">AddInteraction</a>(int i, int j, const T &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aba0481a22dc304dc647d1ff3efdf9916">AddInteractionColumn</a>(int, int, int *, T *)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1f7b4b524875b008fc0aa212d022ac37">AddInteractionColumn</a>(int i, int nb, const IVect &amp;row, const Vector&lt; T, VectFull, Alloc1 &gt; &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6428579a6d7521fb171b28c12f48a030">AddInteractionRow</a>(int, int, int *, T *)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a0563d3c1193b5095374765262957006f">AddInteractionRow</a>(int i, int nb, const IVect &amp;col, const Vector&lt; T, VectFull, Alloc1 &gt; &amp;val)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>allocator</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac469920c36d5a57516a3e53c0c4e3126">Assemble</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a73b4fcf4fa577bbe4a88cf0a2f9a0bbc">AssembleRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61227beea7612dd22bf1f46546bb5cde">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#ad7e78da914d3e0d3818b8c21ec8e7bbf">ClearRow</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_access_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>const_reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>entry_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a806a12c725733434b08b4276de7fbcee">Fill</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ae89973954b1f1372112ebb14045cf5d5">Fill</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ae7161fac73344a98f937abbf957fdbb3">FillRand</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0c18c6428cc11d175b58205727a4c67d">GetData</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a550784008495b254998193d94f75bff9">GetData</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a01787b806fc996c2ff3c1dcf9b2d54de">GetDataSize</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac3d6a23829eb932f41de04c13de22f6b">GetIndex</a>(int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a156ebb8be6f9fbf5d2e7ef01822a341f">GetM</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#aeb55b9de8946ee89fb40c18612f9f700">GetM</a>(const SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a3ae65d56c548233051a15054643f6753">GetN</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a537eebc1c692724958852a83df609036">GetN</a>(const SeldonTranspose &amp;status) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ac58573fd27b4f0345ae196b1a6c69d0d">GetNonZeros</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a1db215224f5b1ac332a82b55da858445">GetRowSize</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a3534fb2c57f2640c9b8cd5a59e9da0e2">Index</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a34c0904b139171db42c46d46413f414f">Index</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#acc9d4e1d9bdb13e8449c7b2cc92dc3db">m_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a45d914bbc71d7f861330e804296906c2">Matrix</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a0c716a84da879bffc8d6e0ee9553e39f">Matrix</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6d460ce1bd8388adce95c8ad99b24f1d">Matrix_ArraySparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ad3363505ad6df902486f4496ac0e7ea7">Matrix_ArraySparse</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a03fe364e494d6f0dd31186462681b3a4">n_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a1931ba4ea2000131dbc4bd4577fd9f00">Nullify</a>(int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0cceba81597165b0e23ec0c20daed462">Nullify</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">operator()</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5aaa013260fab95db133652459b0bb2f">operator()</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48">operator=</a>(const T0 &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>pointer</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a61ff8d09ea73a05eff4fb21e16fb1118">Print</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#aedf0c511638d6bcc7f5b8e4cfddf6e93">PrintRow</a>(int i) const </td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>property</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2e644c704eba4e3c531c403952189d86">Read</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#abeb6e31b268538d3c43fc8f795e994d4">Read</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a22e85b66114b00ba89b1615bc1839b0c">ReadText</a>(string FileName)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a4bac922eaed55ed72c4fcfac89b405ca">ReadText</a>(istream &amp;FileStream)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afcd0d4612b325249c24407627b3dd568">Reallocate</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a141cfc9e4bacfcb106564df3154a104b">ReallocateRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>reference</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a4b87eb77f9690ec605e9a1e7e1d155c5">RemoveSmallEntry</a>(const T0 &amp;epsilon)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a29c3376f6702a15782a46c240c7ce8bd">ReplaceIndexRow</a>(int i, IVect &amp;new_index)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a2c6f9586424529c8b06b6110c5a01efa">Resize</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6d7b0590ee4833ecc7a38c787cea92c3">ResizeRow</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a9fb98c93d5bee7a4be62e1a3e1bb5a46">SetData</a>(int, int, Vector&lt; T, VectSparse, Allocator &gt; *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6e429aff71786b06033b047e6c277fab">SetData</a>(int, int, T *, int *)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#af92baadfc393d70b6e3f3ecaa25d16c7">SetIdentity</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>storage</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php#a6a476c1d342322cb6b0d16d19de837dc">SwapRow</a>(int i, int i_)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#ad114c8c6e002a7a0def70f2413a32659">Val</a>(int i, int j)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a130c7307de87672e3878362386481775">Val</a>(int i, int j) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5d9a9091c665cea7f54ac8ba70d11fb6">val_</a></td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a0073048e11b166e4152be498e9c497f6">Value</a>(int num_row, int i) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afce6920acaa6e6ca7ce340c2db8991ea">Value</a>(int num_row, int i)</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>value_type</b> typedef (defined in <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sparse_00_01_allocator_01_4.php">Seldon::Matrix&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a014e4dca632c4955660ad849db2cb363">Write</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#afaaf09b0ad951e8b82603e2e6f4d79c4">Write</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#adf0d3ec9d5ffeb1b3d46f523051d1412">WriteText</a>(string FileName) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a6037e1907164259fa78d2abaa1380577">WriteText</a>(ostream &amp;FileStream) const</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a06fc483908e9219787dfc9331b07d828">Zero</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php#adf298ef8550ee9681a384a539802736f">~Matrix_ArraySparse</a>()</td><td><a class="el" href="class_seldon_1_1_matrix___array_sparse.php">Seldon::Matrix_ArraySparse&lt; T, Prop, ArrayRowSparse, Allocator &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
