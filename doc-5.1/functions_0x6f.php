<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="tabs2">
    <ul class="tablist">
      <li class="current"><a href="functions.php"><span>All</span></a></li>
      <li><a href="functions_func.php"><span>Functions</span></a></li>
      <li><a href="functions_vars.php"><span>Variables</span></a></li>
      <li><a href="functions_type.php"><span>Typedefs</span></a></li>
    </ul>
  </div>
  <div class="tabs3">
    <ul class="tablist">
      <li><a href="functions.php#index_a"><span>a</span></a></li>
      <li><a href="functions_0x63.php#index_c"><span>c</span></a></li>
      <li><a href="functions_0x64.php#index_d"><span>d</span></a></li>
      <li><a href="functions_0x65.php#index_e"><span>e</span></a></li>
      <li><a href="functions_0x66.php#index_f"><span>f</span></a></li>
      <li><a href="functions_0x67.php#index_g"><span>g</span></a></li>
      <li><a href="functions_0x68.php#index_h"><span>h</span></a></li>
      <li><a href="functions_0x69.php#index_i"><span>i</span></a></li>
      <li><a href="functions_0x6c.php#index_l"><span>l</span></a></li>
      <li><a href="functions_0x6d.php#index_m"><span>m</span></a></li>
      <li><a href="functions_0x6e.php#index_n"><span>n</span></a></li>
      <li class="current"><a href="functions_0x6f.php#index_o"><span>o</span></a></li>
      <li><a href="functions_0x70.php#index_p"><span>p</span></a></li>
      <li><a href="functions_0x72.php#index_r"><span>r</span></a></li>
      <li><a href="functions_0x73.php#index_s"><span>s</span></a></li>
      <li><a href="functions_0x74.php#index_t"><span>t</span></a></li>
      <li><a href="functions_0x75.php#index_u"><span>u</span></a></li>
      <li><a href="functions_0x76.php#index_v"><span>v</span></a></li>
      <li><a href="functions_0x77.php#index_w"><span>w</span></a></li>
      <li><a href="functions_0x78.php#index_x"><span>x</span></a></li>
      <li><a href="functions_0x7a.php#index_z"><span>z</span></a></li>
      <li><a href="functions_0x7e.php#index_~"><span>~</span></a></li>
    </ul>
  </div>
<div class="contents">
Here is a list of all documented class members with links to the class documentation for each member:

<h3><a class="anchor" id="index_o"></a>- o -</h3><ul>
<li>omega
: <a class="el" href="class_seldon_1_1_sor_preconditioner.php#a38fb6b7c7b21677ee0042e1c3b5f4d6f">Seldon::SorPreconditioner&lt; T &gt;</a>
</li>
<li>operator()()
: <a class="el" href="class_seldon_1_1_array3_d.php#aa29212ec08a08bbda48cf91cce043dd2">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#accc5a22477245a42cd0e017157f7d17d">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a165b523d2483eaf716a43f2a89363d73">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#aa72fab293552e87ee528d11b04d361bf">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a019d20b720889f75c4c2314b40e86954">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#ad441174c77a9c101560e77794aae6369">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a5f07919d9f3d4d8ac483247e4d9d752f">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#aa899e8ece6a4210b5199a37e3e60b401">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a2bed95a4398789fdbdc0abd96b233c5e">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a859b5a76ca8ff32246c610145151ebac">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a6cc7b33b27d2a8d51293614d176ebfc0">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#afd992c6ba8e53a2c556459388fc42293">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a774399178dd07c6e598e6051e8f89009">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abb6f9d844071aaa8c8bc1ac62b1dba07">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a17491e8cc9822f3ff66fb23e8a42a7e4">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#a1152a779c807df6022715b8635be7bf7">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a5784a509a63b4a0e4f0758e342218e2a">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#ae163011cc1d13382280abd54c1f4af93">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_sub_matrix___base.php#a5444261ef158cbcf0c65ed0fe93ef0e4">Seldon::SubMatrix_Base&lt; T, Prop, M, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector2.php#a90309917cc1fe070d182ed32bf97a0ed">Seldon::Vector2&lt; T, Allocator0, Allocator1 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#ab5c20ffc81e74baf90d2752e0c1ea136">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a30be6306dbdd79b0a282ef99289b0c2b">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#aaacba77cabdd3d80b2809778ee76206b">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_complex_sparse_00_01_allocator_01_4.php#ace196e6c26320571bf77a782833d7232">Seldon::Matrix&lt; T, Prop, ArrayColSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#a1298a6ec16e6a70ce7de2e456a0c56aa">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#ab596d570ea5b72f77f189b8ebbd7796e">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a520c43fa72c7b7e550dbb3bdac184f59">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a88e1be079725c42ffcd7591e9cc8debc">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_complex_sparse_00_01_allocator_01_4.php#a5fb8630efa21241d2517927eeb80de5e">Seldon::Matrix&lt; T, Prop, ArrayRowSymComplexSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector3.php#afc9bb303c83fd61af66cb9f2b7ea9842">Seldon::Vector3&lt; T, Allocator0, Allocator1, Allocator2 &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a41e1ef18aa87fce8062d36be5e634dfd">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ae10c68e40c5383c9d822e8cfd2647e8f">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a8ea7c92005432ab73b7b8fbbe37e4868">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a5aaa013260fab95db133652459b0bb2f">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#a1437cb081d496b576b36ae061cd9c51c">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a6123f522a052278ebc1853848056c9e3">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a2323bcd2dcd381a1b02e5e6c308bb4f0">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_col_sym_sparse_00_01_allocator_01_4.php#aaba3e074be42ad7e68fa22407fe485fb">Seldon::Matrix&lt; T, Prop, ArrayColSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a8d872a0e035a2afcc8e40736e8556c44">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#a45ce5b8ba17733bffde87e4558431077">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_array_row_sym_sparse_00_01_allocator_01_4.php#a45ec07c98e6e55fdae57dda5b0c1cda7">Seldon::Matrix&lt; T, Prop, ArrayRowSymSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#ad9e481e5ea6de45867d8952dd9676bea">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>operator*=()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a22d1b5d7b3c8f7dbbce29d36a7124d47">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a6d3dfe89ad65b9daf47875ac529f7aa2">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#aa7129e3653db9f71709cc835d05e1876">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#a034cf7dc7c8ba68a6a94e45db6789c5d">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a19b9f7624485f210146c6255c6835c1a">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#a172f58cf130f1566b88db419dabbb02a">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a429e20e3d4e359ccbdca47fd89e87d4b">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a236fd63d659f7d92f3b12da557380cb3">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#a2279bc596ff24069515312c1332f2d8e">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a7ae16201a85d4196867631346db84c85">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#adc2b4012e48ecdcbce64604a7319f5ce">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#a53e8c9f819dad5bb1d40a9b6edd24005">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a9079c6a12adc5c1b0ef054bc03bdf5b6">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#ac3a6b15a4c8e247460385cb9b924e9e9">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a1f8c93e3abfe109f258ab5b31e6382e4">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#aec3d6da476f91a8d3b6033e835a3ffe9">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a44d2b394ff94b327b2349ea2cc32e17c">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#aa5f0317f8db1d83626497941bf83e9e1">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a59af6d333e0929fb0d556d42c7d8b791">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#a5e92bfa8836941382f706138d3dd91e7">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#a078e3eb8345ce3294d5d3a40dab61e1d">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a>
</li>
<li>operator++()
: <a class="el" href="class_seldon_1_1_iteration.php#a105cecfc4c5d1c1c27278eaa5807e32d">Seldon::Iteration&lt; Titer &gt;</a>
</li>
<li>operator=()
: <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_00_01_allocator_01_4.php#aa8571c2671559a14854607d20ff9008d">Seldon::Matrix&lt; T, Prop, RowHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_packed_00_01_allocator_01_4.php#ad8f1511c8abd5cf290c32c1ec43dad01">Seldon::Matrix&lt; T, Prop, ColHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#ac18163af8e9002a336f71ad38fd69230">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a71b5c6a90688dd19e45d2b6f59369f14">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_00_01_allocator_01_4.php#ac04f5c5815fbc6085da6b7f0c62a0281">Seldon::Matrix&lt; T, Prop, ColLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_herm_packed_00_01_allocator_01_4.php#a4edcaa25fa32c27fa993e46fcc3472a0">Seldon::Matrix&lt; T, Prop, RowHermPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_heterogeneous_matrix_collection.php#a1f383b28bd0289fc1f00c9e06e597c9e">Seldon::HeterogeneousMatrixCollection&lt; Prop0, Storage0, Prop1, Storage1, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_major_00_01_allocator_01_4.php#a5f16442782b3155c49889aee1a6df7e1">Seldon::Matrix&lt; T, Prop, ColMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#ab52557ba17dd1288794a2e6d472ebdc3">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#ad57f72a50f4f8bdb421c9f02251bde85">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a3727350f7283b6e8e08ccb2f6733362b">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a892540112f14801e1b48f3cf18b112ab">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_packed_00_01_allocator_01_4.php#a847d2334cad1a45a4d2555eb5f15e95c">Seldon::Matrix&lt; T, Prop, RowSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_array3_d.php#a0cb6b0016cddb1a1e4c25c8e538e69be">Seldon::Array3D&lt; T, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_packed_00_01_allocator_01_4.php#a683f609a9b3b4ab34521d1e9d74b5974">Seldon::Matrix&lt; T, Prop, RowUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_packed_00_01_allocator_01_4.php#a17e0f0ea723e14c87abfbbecb99abfc9">Seldon::Matrix&lt; T, Prop, RowLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#acea882121d471497d730d032054e8c14">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_complex_sparse.php#a95e0c06209ddfeea67740ecb9c0c1451">Seldon::Matrix_ArrayComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___array_sparse.php#a28d0b06c82e4bb29f36c872f71fe7b48">Seldon::Matrix_ArraySparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sparse.php#a58f047d2d62a3e5e7f7feaae98cd7d7f">Seldon::Matrix_Sparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_complex_sparse.php#aba07f35ba600bf49197e1b1802f95c67">Seldon::Matrix_SymComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_sparse_00_01_allocator_01_4.php#a87ef82c6607c68b77a5ec18dbcb15eb0">Seldon::Vector&lt; T, VectSparse, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#a0bf814aab7883056808671f5b6866ba6">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_collection_00_01_allocator_01_4.php#ab33349edcc4ad9275540eaebc0279cc5">Seldon::Vector&lt; T, Collection, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_herm_00_01_allocator_01_4.php#a9ce3f192a24e1d1845a9377435bbb99c">Seldon::Matrix&lt; T, Prop, ColHerm, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_up_triang_00_01_allocator_01_4.php#a1c1975aa196d966318a70af7ccfa177b">Seldon::Matrix&lt; T, Prop, RowUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_sparse.php#ad9e277dce0daf0cfb76e81e6087bcb75">Seldon::Matrix_SymSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_t_00_01_vect_full_00_01_allocator_01_4.php#abd12db9e14f5ba1c417d4c587e6d359a">Seldon::Vector&lt; T, VectFull, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_vector_3_01_float_double_00_01_dense_sparse_collection_00_01_allocator_3_01_t_01_4_01_4.php#a5a45815f1db11248f8e963611f41d0b1">Seldon::Vector&lt; FloatDouble, DenseSparseCollection, Allocator&lt; T &gt; &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_lo_triang_00_01_allocator_01_4.php#ae36017d3afd1be1b01bfb6a63c86f4c9">Seldon::Matrix&lt; T, Prop, RowLoTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___complex_sparse.php#a3e54cf340623d3c2c03f4926d9846383">Seldon::Matrix_ComplexSparse&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_collection.php#a9e3b633e9622c3a86561f8b08e9ff8e8">Seldon::MatrixCollection&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a13726b53c0a2db160c719e7d99b15274">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a9a70002e76a6930a24aa0b724c6976a1">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_sym_00_01_allocator_01_4.php#a206f2265db8f733fbf11d574717d1e01">Seldon::Matrix&lt; T, Prop, RowSym, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ac8f8519e62ace1acd0e0c514d080471a">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_lo_triang_packed_00_01_allocator_01_4.php#aa2ac6a90200f30d3f7efe7c4592bb95e">Seldon::Matrix&lt; T, Prop, ColLoTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#a2f25eaae12f9e30ddad62426278dee62">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_packed_00_01_allocator_01_4.php#abf68ccb2ad56edf5e5332a88677635e9">Seldon::Matrix&lt; T, Prop, ColUpTriangPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#ab379631da575bbb4b2404f1fc48c37a4">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_up_triang_00_01_allocator_01_4.php#ac7f70aeb83a8db1f3b8e157f731d76f9">Seldon::Matrix&lt; T, Prop, ColUpTriang, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_packed_00_01_allocator_01_4.php#a2be59cf2b48ebed2b1972cdf7791b209">Seldon::Matrix&lt; T, Prop, ColSymPacked, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_row_major_00_01_allocator_01_4.php#a11d3b562dc6263cad3299de5cbff269a">Seldon::Matrix&lt; T, Prop, RowMajor, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a63be5be398e6a3be1f028f6c6966766c">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix_3_01_t_00_01_prop_00_01_col_sym_00_01_allocator_01_4.php#a02437899b3e29faaf714a28429b66de2">Seldon::Matrix&lt; T, Prop, ColSym, Allocator &gt;</a>
</li>
<li>operator[]()
: <a class="el" href="class_seldon_1_1_matrix___triangular.php#a30661e664fd84702ea900809e4c7764c">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___herm_packed.php#a804fa785b8d17fd2d56d01226bce844a">Seldon::Matrix_HermPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a92663437cbb2ca113e78f461a4689879">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a57a7db421db0df658c32ed661d935089">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#af98cb1a88c5cd8675ce73ccff9f37db9">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#aead099f326217ddb05c0782e58b0b1a5">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___hermitian.php#a23f10cdfae40711e79c2be1e0c682f59">Seldon::Matrix_Hermitian&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___pointers.php#ac3be1736d3b8f78244483b879b72b255">Seldon::Matrix_Pointers&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triang_packed.php#ade6cf6ee8f39e79ae4419b5a4851b32e">Seldon::Matrix_TriangPacked&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___triangular.php#ab4f150251c1afd88a7a59a382ac4f623">Seldon::Matrix_Triangular&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___symmetric.php#a674815a3c75a714b790ee7dec3a46b4b">Seldon::Matrix_Symmetric&lt; T, Prop, Storage, Allocator &gt;</a>
, <a class="el" href="class_seldon_1_1_matrix___sym_packed.php#a7628441645b1367f95b15d1f77a208af">Seldon::Matrix_SymPacked&lt; T, Prop, Storage, Allocator &gt;</a>
</li>
<li>options
: <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1">Seldon::MatrixSuperLU_Base&lt; T &gt;</a>
</li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
