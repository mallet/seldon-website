<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
  <div class="navpath">
    <ul>
      <li><a class="el" href="namespace_seldon.php">Seldon</a>      </li>
      <li><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php">MatrixUmfPack&lt; double &gt;</a>      </li>
    </ul>
  </div>
<div class="header">
  <div class="summary">
<a href="#pub-methods">Public Member Functions</a> &#124;
<a href="#pub-attribs">Public Attributes</a> &#124;
<a href="#pro-attribs">Protected Attributes</a>  </div>
  <div class="headertitle">
<h1>Seldon::MatrixUmfPack&lt; double &gt; Class Template Reference</h1>  </div>
</div>
<div class="contents">
<!-- doxytag: class="Seldon::MatrixUmfPack&lt; double &gt;" --><!-- doxytag: inherits="MatrixUmfPack_Base&lt; double &gt;" -->
<p>class to solve linear system in double precision with UmfPack  
<a href="#_details">More...</a></p>

<p><code>#include &lt;<a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a>&gt;</code></p>
<div class="dynheader">
Inheritance diagram for Seldon::MatrixUmfPack&lt; double &gt;:</div>
<div class="dyncontent">
 <div class="center">
  <img src="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.png" usemap="#Seldon::MatrixUmfPack&lt; double &gt;_map" alt=""/>
  <map id="Seldon::MatrixUmfPack&lt; double &gt;_map" name="Seldon::MatrixUmfPack&lt; double &gt;_map">
<area href="class_seldon_1_1_matrix_umf_pack___base.php" alt="Seldon::MatrixUmfPack_Base&lt; double &gt;" shape="rect" coords="0,0,238,24"/>
</map>
</div>

<p><a href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4-members.php">List of all members.</a></p>
<table class="memberdecls">
<tr><td colspan="2"><h2><a name="pub-methods"></a>
Public Member Functions</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a70dd6831eed0355caedc69e8d3c99920"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::MatrixUmfPack" ref="a70dd6831eed0355caedc69e8d3c99920" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a70dd6831eed0355caedc69e8d3c99920">MatrixUmfPack</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">constructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa660d4563cf769365f1121761f7db67f"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::~MatrixUmfPack" ref="aa660d4563cf769365f1121761f7db67f" args="()" -->
&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#aa660d4563cf769365f1121761f7db67f">~MatrixUmfPack</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">destructor <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a24db3569fa50301554e9ca73ebd167e3"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Clear" ref="a24db3569fa50301554e9ca73ebd167e3" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a24db3569fa50301554e9ca73ebd167e3">Clear</a> ()</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">we clear present factorization if any <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="aa2dd343b5e23a0d46704531f201e1f24"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::FactorizeMatrix" ref="aa2dd343b5e23a0d46704531f201e1f24" args="(Matrix&lt; double, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)" -->
template&lt;class Prop , class Storage , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#aa2dd343b5e23a0d46704531f201e1f24">FactorizeMatrix</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">factorization of a real matrix in double precision <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a182a7c745c3d05a8fbe6733aa837087d"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::PerformAnalysis" ref="a182a7c745c3d05a8fbe6733aa837087d" args="(Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;mat)" -->
template&lt;class Prop , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a182a7c745c3d05a8fbe6733aa837087d">PerformAnalysis</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;mat)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Symbolic factorization. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="affb8a911794ad355a183469ed2e42480"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::PerformFactorization" ref="affb8a911794ad355a183469ed2e42480" args="(Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;mat)" -->
template&lt;class Prop , class Allocator &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#affb8a911794ad355a183469ed2e42480">PerformFactorization</a> (<a class="el" href="class_seldon_1_1_matrix.php">Matrix</a>&lt; double, Prop, <a class="el" href="class_seldon_1_1_row_sparse.php">RowSparse</a>, Allocator &gt; &amp;mat)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">Numerical factorization. <br/></td></tr>
<tr><td class="memTemplParams" colspan="2"><a class="anchor" id="a4f4e2a42ec974a69b46a91d5f68003ad"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Solve" ref="a4f4e2a42ec974a69b46a91d5f68003ad" args="(Vector&lt; double, VectFull, Allocator2 &gt; &amp;x)" -->
template&lt;class Allocator2 &gt; </td></tr>
<tr><td class="memTemplItemLeft" align="right" valign="top">void&nbsp;</td><td class="memTemplItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a4f4e2a42ec974a69b46a91d5f68003ad">Solve</a> (<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double, <a class="el" href="class_seldon_1_1_vect_full.php">VectFull</a>, Allocator2 &gt; &amp;x)</td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">resolution of A y = x (result is overwritten in x) <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad88543986d6cf6273c9f56654820b2f1"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::HideMessages" ref="ad88543986d6cf6273c9f56654820b2f1" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>HideMessages</b> ()</td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="af5a60fc35fefccab49c6888cdb148c94"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::ShowMessages" ref="af5a60fc35fefccab49c6888cdb148c94" args="()" -->
void&nbsp;</td><td class="memItemRight" valign="bottom"><b>ShowMessages</b> ()</td></tr>
<tr><td colspan="2"><h2><a name="pub-attribs"></a>
Public Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a86ccadbb48712eec2c6f335236f70a"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Control" ref="a4a86ccadbb48712eec2c6f335236f70a" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><b>Control</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4a6b504d370b37ef4951626d2a3ce439"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Info" ref="a4a6b504d370b37ef4951626d2a3ce439" args="" -->
<a class="el" href="class_seldon_1_1_vector.php">Vector</a>&lt; double &gt;&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a4a6b504d370b37ef4951626d2a3ce439">Info</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">parameters for UmfPack <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a4657c453f55d8b8077af14d70aa1f3e0"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Symbolic" ref="a4657c453f55d8b8077af14d70aa1f3e0" args="" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><b>Symbolic</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a7a91b3ba9d23982fc5b75ebf8f4d2eea"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::Numeric" ref="a7a91b3ba9d23982fc5b75ebf8f4d2eea" args="" -->
void *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a7a91b3ba9d23982fc5b75ebf8f4d2eea">Numeric</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">pointers of UmfPack objects <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="aa442e0754359877423d73f857aa75731"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::n" ref="aa442e0754359877423d73f857aa75731" args="" -->
int&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#aa442e0754359877423d73f857aa75731">n</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">number of rows in the matrix <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a00b2252a6082727f89fcba9ca33fbf7d"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::display_info" ref="a00b2252a6082727f89fcba9ca33fbf7d" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack___base.php#a00b2252a6082727f89fcba9ca33fbf7d">display_info</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">true if display is allowed <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ac6a40597353446887c3b8c0c210aae74"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::transpose" ref="ac6a40597353446887c3b8c0c210aae74" args="" -->
bool&nbsp;</td><td class="memItemRight" valign="bottom"><b>transpose</b></td></tr>
<tr><td colspan="2"><h2><a name="pro-attribs"></a>
Protected Attributes</h2></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="a80c2fab0cdd04087069ed6aee40e5ac6"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::ind_" ref="a80c2fab0cdd04087069ed6aee40e5ac6" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#a80c2fab0cdd04087069ed6aee40e5ac6">ind_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">arrays containing matrix pattern in csc format <br/></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ab6517160f2a1f2c7532eb80d4c5f0f9f"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::ptr_" ref="ab6517160f2a1f2c7532eb80d4c5f0f9f" args="" -->
int *&nbsp;</td><td class="memItemRight" valign="bottom"><b>ptr_</b></td></tr>
<tr><td class="memItemLeft" align="right" valign="top"><a class="anchor" id="ad43800f106a0be99442759b9c63ae487"></a><!-- doxytag: member="Seldon::MatrixUmfPack&lt; double &gt;::data_" ref="ad43800f106a0be99442759b9c63ae487" args="" -->
double *&nbsp;</td><td class="memItemRight" valign="bottom"><a class="el" href="class_seldon_1_1_matrix_umf_pack_3_01double_01_4.php#ad43800f106a0be99442759b9c63ae487">data_</a></td></tr>
<tr><td class="mdescLeft">&nbsp;</td><td class="mdescRight">non-zero values <br/></td></tr>
</table>
<hr/><a name="_details"></a><h2>Detailed Description</h2>
<h3>template&lt;&gt;<br/>
 class Seldon::MatrixUmfPack&lt; double &gt;</h3>

<p>class to solve linear system in double precision with UmfPack </p>

<p>Definition at line <a class="el" href="_umf_pack_8hxx_source.php#l00056">56</a> of file <a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a>.</p>
<hr/>The documentation for this class was generated from the following files:<ul>
<li>computation/interfaces/direct/<a class="el" href="_umf_pack_8hxx_source.php">UmfPack.hxx</a></li>
<li>computation/interfaces/direct/<a class="el" href="_umf_pack_8cxx_source.php">UmfPack.cxx</a></li>
</ul>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
