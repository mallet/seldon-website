<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>array3d/Array3D.hxx</h1>  </div>
</div>
<div class="contents">
<div class="fragment"><pre class="fragment"><a name="l00001"></a>00001 <span class="comment">// Copyright (C) 2001-2009 Vivien Mallet</span>
<a name="l00002"></a>00002 <span class="comment">//</span>
<a name="l00003"></a>00003 <span class="comment">// This file is part of the linear-algebra library Seldon,</span>
<a name="l00004"></a>00004 <span class="comment">// http://seldon.sourceforge.net/.</span>
<a name="l00005"></a>00005 <span class="comment">//</span>
<a name="l00006"></a>00006 <span class="comment">// Seldon is free software; you can redistribute it and/or modify it under the</span>
<a name="l00007"></a>00007 <span class="comment">// terms of the GNU Lesser General Public License as published by the Free</span>
<a name="l00008"></a>00008 <span class="comment">// Software Foundation; either version 2.1 of the License, or (at your option)</span>
<a name="l00009"></a>00009 <span class="comment">// any later version.</span>
<a name="l00010"></a>00010 <span class="comment">//</span>
<a name="l00011"></a>00011 <span class="comment">// Seldon is distributed in the hope that it will be useful, but WITHOUT ANY</span>
<a name="l00012"></a>00012 <span class="comment">// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS</span>
<a name="l00013"></a>00013 <span class="comment">// FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for</span>
<a name="l00014"></a>00014 <span class="comment">// more details.</span>
<a name="l00015"></a>00015 <span class="comment">//</span>
<a name="l00016"></a>00016 <span class="comment">// You should have received a copy of the GNU Lesser General Public License</span>
<a name="l00017"></a>00017 <span class="comment">// along with Seldon. If not, see http://www.gnu.org/licenses/.</span>
<a name="l00018"></a>00018 
<a name="l00019"></a>00019 
<a name="l00020"></a>00020 <span class="comment">// To be included by Seldon.hxx</span>
<a name="l00021"></a>00021 
<a name="l00022"></a>00022 
<a name="l00023"></a>00023 <span class="preprocessor">#ifndef SELDON_FILE_ARRAY3D_HXX</span>
<a name="l00024"></a>00024 <span class="preprocessor"></span>
<a name="l00025"></a>00025 <span class="preprocessor">#include &quot;../share/Common.hxx&quot;</span>
<a name="l00026"></a>00026 <span class="preprocessor">#include &quot;../share/Errors.hxx&quot;</span>
<a name="l00027"></a>00027 <span class="preprocessor">#include &quot;../share/Allocator.hxx&quot;</span>
<a name="l00028"></a>00028 
<a name="l00029"></a>00029 <span class="keyword">namespace </span>Seldon
<a name="l00030"></a>00030 {
<a name="l00031"></a>00031 
<a name="l00032"></a>00032 
<a name="l00034"></a>00034 
<a name="l00037"></a>00037   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator = <span class="keywordtype">SEL</span>DON_DEFAULT_ALLOCATOR&lt;T&gt; &gt;
<a name="l00038"></a><a class="code" href="class_seldon_1_1_array3_d.php">00038</a>   <span class="keyword">class </span><a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D</a>
<a name="l00039"></a>00039   {
<a name="l00040"></a>00040     <span class="comment">// typdef declarations.</span>
<a name="l00041"></a>00041   <span class="keyword">public</span>:
<a name="l00042"></a>00042     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::value_type value_type;
<a name="l00043"></a>00043     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::pointer pointer;
<a name="l00044"></a>00044     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_pointer const_pointer;
<a name="l00045"></a>00045     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::reference reference;
<a name="l00046"></a>00046     <span class="keyword">typedef</span> <span class="keyword">typename</span> Allocator::const_reference const_reference;
<a name="l00047"></a>00047 
<a name="l00048"></a>00048     <span class="comment">// Static attributes.</span>
<a name="l00049"></a>00049   <span class="keyword">protected</span>:
<a name="l00050"></a>00050     <span class="keyword">static</span> Allocator array3D_allocator_;
<a name="l00051"></a>00051 
<a name="l00052"></a>00052     <span class="comment">// Attributes.</span>
<a name="l00053"></a>00053   <span class="keyword">protected</span>:
<a name="l00054"></a>00054     <span class="comment">// Length along dimension #1.</span>
<a name="l00055"></a>00055     <span class="keywordtype">int</span> length1_;
<a name="l00056"></a>00056     <span class="comment">// Length along dimension #2.</span>
<a name="l00057"></a>00057     <span class="keywordtype">int</span> length2_;
<a name="l00058"></a>00058     <span class="comment">// Length along dimension #3.</span>
<a name="l00059"></a>00059     <span class="keywordtype">int</span> length3_;
<a name="l00060"></a>00060 
<a name="l00061"></a>00061     <span class="comment">// Size of a slice (i.e. length1_ by length2_).</span>
<a name="l00062"></a>00062     <span class="keywordtype">int</span> length23_;
<a name="l00063"></a>00063 
<a name="l00064"></a>00064     <span class="comment">// Pointer to stored elements.</span>
<a name="l00065"></a>00065     pointer data_;
<a name="l00066"></a>00066 
<a name="l00067"></a>00067     <span class="comment">// Methods.</span>
<a name="l00068"></a>00068   <span class="keyword">public</span>:
<a name="l00069"></a>00069     <span class="comment">// Constructors.</span>
<a name="l00070"></a>00070     <a class="code" href="class_seldon_1_1_array3_d.php#af53eaf5dca82c2d4ae7762ea532789ef" title="Default constructor.">Array3D</a>();
<a name="l00071"></a>00071     <a class="code" href="class_seldon_1_1_array3_d.php#af53eaf5dca82c2d4ae7762ea532789ef" title="Default constructor.">Array3D</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k);
<a name="l00072"></a>00072     <a class="code" href="class_seldon_1_1_array3_d.php#af53eaf5dca82c2d4ae7762ea532789ef" title="Default constructor.">Array3D</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00073"></a>00073 
<a name="l00074"></a>00074     <span class="comment">// Destructor.</span>
<a name="l00075"></a>00075     <a class="code" href="class_seldon_1_1_array3_d.php#a0acf50b4c510976bdde062f03787f06b" title="Destructor.">~Array3D</a>();
<a name="l00076"></a>00076 
<a name="l00077"></a>00077     <span class="comment">// Basic methods.</span>
<a name="l00078"></a>00078     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_array3_d.php#ab792b6b72a04b557792b6d5ad3e86afc" title="Returns the length in dimension #1.">GetLength1</a>() <span class="keyword">const</span>;
<a name="l00079"></a>00079     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_array3_d.php#af8acaef0c21a50f4ab6128710d534f8b" title="Returns the length in dimension #2.">GetLength2</a>() <span class="keyword">const</span>;
<a name="l00080"></a>00080     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_array3_d.php#afae5f3ffbbcb66b09ec27d1cfc81759a" title="Returns the length in dimension #3.">GetLength3</a>() <span class="keyword">const</span>;
<a name="l00081"></a>00081     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_array3_d.php#a99dc59f2c62979e78c43d8ec31c02e68" title="Returns the number of elements in the 3D array.">GetSize</a>() <span class="keyword">const</span>;
<a name="l00082"></a>00082     <span class="keywordtype">int</span> <a class="code" href="class_seldon_1_1_array3_d.php#a64e6b9afc92a4c7fe4a99f5017959cb6" title="Returns the number of elements stored in memory.">GetDataSize</a>() <span class="keyword">const</span>;
<a name="l00083"></a>00083     pointer <a class="code" href="class_seldon_1_1_array3_d.php#a41def646fbd18d0a86b2fced6e80faf1" title="Returns a pointer to the data array.">GetData</a>() <span class="keyword">const</span>;
<a name="l00084"></a>00084 
<a name="l00085"></a>00085     <span class="comment">// Memory management.</span>
<a name="l00086"></a>00086     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#ae37a5c9ebc684062db0c1be3604641e8" title="Reallocates memory to resize the 3D array.">Reallocate</a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k);
<a name="l00087"></a>00087     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#aa9bf1047ace17bd5c2bf2d4863d46838" title="Clears the array.">Clear</a>();
<a name="l00088"></a>00088 
<a name="l00089"></a>00089     <span class="comment">// Element access and affectation.</span>
<a name="l00090"></a>00090     reference <a class="code" href="class_seldon_1_1_array3_d.php#aa29212ec08a08bbda48cf91cce043dd2" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k);
<a name="l00091"></a>00091     const_reference <a class="code" href="class_seldon_1_1_array3_d.php#aa29212ec08a08bbda48cf91cce043dd2" title="Access operator.">operator() </a>(<span class="keywordtype">int</span> i, <span class="keywordtype">int</span> j, <span class="keywordtype">int</span> k) <span class="keyword">const</span>;
<a name="l00092"></a>00092     <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;</a>&amp; <a class="code" href="class_seldon_1_1_array3_d.php#a0cb6b0016cddb1a1e4c25c8e538e69be" title="Duplicates a 3D array (assignment operator).">operator= </a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00093"></a>00093     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#a2f8287363e777b0072017405e182ccc0" title="Duplicates a 3D array.">Copy</a>(<span class="keyword">const</span> <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00094"></a>00094 
<a name="l00095"></a>00095     <span class="comment">// Convenient functions.</span>
<a name="l00096"></a>00096     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#ab965837a5d4a255c217bb33e08b45ead" title="Sets all elements to zero.">Zero</a>();
<a name="l00097"></a>00097     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#a3a5ea3c1352767c1ef1f88219660c045" title="Fills the array.">Fill</a>();
<a name="l00098"></a>00098     <span class="keyword">template</span> &lt;<span class="keyword">class</span> T0&gt;
<a name="l00099"></a>00099     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#a3a5ea3c1352767c1ef1f88219660c045" title="Fills the array.">Fill</a>(<span class="keyword">const</span> T0&amp; x);
<a name="l00100"></a>00100     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#af32714dd9d5bcad70aad958c1e6299e8" title="Fills the 3D array randomly.">FillRand</a>();
<a name="l00101"></a>00101     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#aca40f1ee2914601787fceafab5d8c654" title="Displays the array on the standard output.">Print</a>() <span class="keyword">const</span>;
<a name="l00102"></a>00102 
<a name="l00103"></a>00103     <span class="comment">// Input/output functions</span>
<a name="l00104"></a>00104     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#ac4711cf67da3bca2527438ad5b1c2eaa" title="Writes the 3D array in a file.">Write</a>(<span class="keywordtype">string</span> FileName) <span class="keyword">const</span>;
<a name="l00105"></a>00105     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#ac4711cf67da3bca2527438ad5b1c2eaa" title="Writes the 3D array in a file.">Write</a>(ofstream&amp; FileStream) <span class="keyword">const</span>;
<a name="l00106"></a>00106     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#a943c1fb9b12c7e781bf4e39887636ab6" title="Reads the 3D array from a file.">Read</a>(<span class="keywordtype">string</span> FileName);
<a name="l00107"></a>00107     <span class="keywordtype">void</span> <a class="code" href="class_seldon_1_1_array3_d.php#a943c1fb9b12c7e781bf4e39887636ab6" title="Reads the 3D array from a file.">Read</a>(ifstream&amp; FileStream);
<a name="l00108"></a>00108   };
<a name="l00109"></a>00109 
<a name="l00110"></a>00110 
<a name="l00111"></a>00111   <span class="comment">// 3D array allocator.</span>
<a name="l00112"></a>00112   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00113"></a>00113   Allocator <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;::array3D_allocator_</a>;
<a name="l00114"></a>00114 
<a name="l00115"></a>00115 
<a name="l00116"></a>00116   <span class="keyword">template</span> &lt;<span class="keyword">class</span> T, <span class="keyword">class</span> Allocator&gt;
<a name="l00117"></a>00117   ostream&amp; <a class="code" href="namespace_seldon.php#a5bd4a6d6f3c83048663d57d2fc032107" title="operator&amp;lt;&amp;lt; overloaded for a 3D array.">operator &lt;&lt; </a>(ostream&amp; out,
<a name="l00118"></a>00118                         <span class="keyword">const</span> <a class="code" href="class_seldon_1_1_array3_d.php" title="3D array.">Array3D&lt;T, Allocator&gt;</a>&amp; A);
<a name="l00119"></a>00119 
<a name="l00120"></a>00120 
<a name="l00121"></a>00121 } <span class="comment">// namespace Seldon.</span>
<a name="l00122"></a>00122 
<a name="l00123"></a>00123 
<a name="l00124"></a>00124 <span class="preprocessor">#define SELDON_FILE_ARRAY3D_HXX</span>
<a name="l00125"></a>00125 <span class="preprocessor"></span><span class="preprocessor">#endif</span>
</pre></div></div>
</div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
