<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<?php $root='..';?>

<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Seldon user's guide</title>
<link rel="stylesheet" type="text/css" href="<?php echo $root?>/content.css">
<link rel="stylesheet" href="tabs.css" type="text/css">
<link rel="stylesheet" href="guide.css" type="text/css">
<?php if (file_exists($root.'/prettify.js'))
  echo '<script type="text/javascript" src="'.$root.'/prettify.js"></script>';
else if (file_exists('prettify.js'))
  echo '<script type="text/javascript" src="prettify.js"></script>'; ?>
</head>

<body onload="prettyPrint()">

<div class="page">

<?php if (file_exists($root.'/header.php'))
      include $root.'/header.php'; ?>

<div class="doc">

<?php function HL($file_, $section_, $string_)
{
if ($file_ == $section_)
  echo '<em>'.$string_.' </em>';
else
  echo '<a href="'.$section_.'.php">'.$string_.'</a>';
}; ?>

<?php $file=basename($_SERVER['REQUEST_URI'], ".php"); $file = explode(".", $file); $file = $file[0];?>

<div class="nav">

<ul>
<li class="jelly"> <b>USER'S GUIDE</b> </li>
<li class="jelly"> <?php HL($file, "installation", "Installation");?> </li>
<li class="jelly"> <?php HL($file, "overview", "Overview");?> </li>
<li class="jelly"> <?php HL($file, "vectors", "Vectors");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "vectors"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_vector"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_vector")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_vector", "Dense Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_vector", "Sparse Vectors");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_vector", "Functions");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "matrices", "Matrices");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "matrices"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "class_sparse_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_matrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "submatrix"
or basename($_SERVER['REQUEST_URI'], ".php") == "matrix_miscellaneous")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "class_matrix", "Dense Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "class_sparse_matrix", "Sparse Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_matrix", "Functions");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "submatrix", "Sub-Matrices");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "matrix_miscellaneous", "Miscellaneous");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "other_structures", "Other Structures");?>
<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "other_structures"
or basename($_SERVER['REQUEST_URI'], ".php") == "vector2"
or basename($_SERVER['REQUEST_URI'], ".php") == "array3d")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "vector2", "Vector2");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "array3d", "3D&nbsp;Array");
  echo '</li> </ul>';
} ?>
</li>

<li class="jelly"> <?php HL($file, "allocators", "Allocators");?>  </li>
<li class="jelly"> <?php HL($file, "exceptions", "Exceptions");?>  </li>
<li class="jelly"> <?php HL($file, "computations", "Computations");?>

<?php if (basename($_SERVER['REQUEST_URI'], ".php") == "computations"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_blas"
or basename($_SERVER['REQUEST_URI'], ".php") == "functions_lapack"
or basename($_SERVER['REQUEST_URI'], ".php") == "direct"
or basename($_SERVER['REQUEST_URI'], ".php") == "iterative")
{
  echo '<ul class="navsubul"> <li class="jelly">';
  HL($file, "functions_blas", "Blas");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "functions_lapack", "Lapack");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "direct", "Direct Solvers");
  echo '</li>';
  echo '<li class="jelly">';
  HL($file, "iterative", "Iterative Solvers");
  echo '</li> </ul>';
} ?>

</li>

<li class="jelly"> <?php HL($file, "python", "Python Interface");?> </li>
<li class="jelly"> <?php HL($file, "glossary", "Index");?> </li>
<li class="jelly"> <b>API REFERENCE</b> </li>
<li class="jelly"> <?php HL($file, "annotated", "Classes");?>
<ul class="navsubul"> <li class="jelly"> <?php HL($file, "annotated", "Class List");?> </li> 
<li class="jelly"> <?php HL($file, "hierarchy", "Class Hierarchy");?> </li>
<li class="jelly"> <?php HL($file, "functions", "Class Members");?>
</li> </ul> </li>
<li class="jelly"> <?php HL($file, "namespacemembers", "Functions");?> </li>
<li class="jelly"> Search for <form action="search.php" method="get">
    <input class="search" type="text" name="query" value="" size="20" accesskey="s">
  </form>
</li>
<!-- <li class="jelly"> <?php HL($file, "faq", "F.A.Q.");?> </li>-->
<li class="jelly"> <a
href="mailto:seldon-help@lists.sourceforge.net"
style="color:black">Support</a></li>
</ul>

</div>

<div class="doxygen">
<!-- Generated by Doxygen 1.6.3-20100507 -->
<script type="text/javascript"><!--
var searchBox = new SearchBox("searchBox", "search",false,'Search');
--></script>
<div class="header">
  <div class="headertitle">
<h1>Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt; Member List</h1>  </div>
</div>
<div class="contents">
This is the complete list of members for <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a>, including all inherited members.<table>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a6dd29bcb497b1c7175d6dc9f10ac8e53">A</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>B</b> (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acac4d12b1d9e464f630b7d56b2c61a23">Clear</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a5b067c2c8ef0a0fcbfec31d399fd2bc3">display_info</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#aff33f45fbe34f32a2e8db108e406c51b">FactorizeMatrix</a>(Matrix&lt; complex&lt; double &gt;, Prop, Storage, Allocator &gt; &amp;mat, bool keep_matrix=false)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#acf6c8120e481e56c99e77bea79b30f84">GetColPermutation</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a6492cdcde57b8524db187be6f4602ef5">GetLU</a>(Matrix&lt; double, Prop, ColSparse, Allocator &gt; &amp;Lmat, Matrix&lt; double, Prop, ColSparse, Allocator &gt; &amp;Umat, bool permuted=true)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a7713db1a177053fdee0cd121411ae986">GetLU</a>(Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;Lmat, Matrix&lt; double, Prop, RowSparse, Allocator &gt; &amp;Umat, bool permuted=true)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a27aca5955b9d58ab561f652f153139ee">GetRowPermutation</a>() const</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a733cd4e38035ae701435f9f298442e4d">HideMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>L</b> (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3639abafe150759f173dc6d6a654c534">Lstore</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>MatrixSuperLU</b>() (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></td><td><code> [inline]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a526c7f0e29c86b81fc4705b437e167f9">MatrixSuperLU_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3a2831b9dac4fa42c7815015627f5f38">n</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3de025ec4fdc83b5c793d69becc051c1">options</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>perm_c</b> (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>perm_r</b> (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a8d4acdd57e0a5e8a8a0651e1de0a8547">permc_spec</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#af5322d1293240d5d3ab6aeb93183acc7">ShowMessages</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a13625fddc750bb908d481b2c789344eb">Solve</a>(Vector&lt; complex&lt; double &gt;, VectFull, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php#a81a49129375211e439ffc2e96d494d0e">Solve</a>(const SeldonTranspose &amp;TransA, Vector&lt; complex&lt; double &gt;, VectFull, Allocator2 &gt; &amp;x)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u_3_01complex_3_01double_01_4_01_4.php">Seldon::MatrixSuperLU&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a51a947858f30367b812df38f97ec3aa4">stat</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr bgcolor="#f0f0f0"><td><b>U</b> (defined in <a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a>)</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a3c0030723c06e7bffc470c94b344c00e">Ustore</a></td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td><code> [protected]</code></td></tr>
  <tr class="memlist"><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php#a8f0a2adf458ec4f187f3936fea612106">~MatrixSuperLU_Base</a>()</td><td><a class="el" href="class_seldon_1_1_matrix_super_l_u___base.php">Seldon::MatrixSuperLU_Base&lt; complex&lt; double &gt; &gt;</a></td><td></td></tr>
</table></div>
<!--- window showing the filter options -->
<div id="MSearchSelectWindow"
     onmouseover="return searchBox.OnSearchSelectShow()"
     onmouseout="return searchBox.OnSearchSelectHide()"
     onkeydown="return searchBox.OnSearchSelectKey(event)">
<a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(0)"><span class="SelectionMark">&nbsp;</span>All</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(1)"><span class="SelectionMark">&nbsp;</span>Classes</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(2)"><span class="SelectionMark">&nbsp;</span>Namespaces</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(3)"><span class="SelectionMark">&nbsp;</span>Files</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(4)"><span class="SelectionMark">&nbsp;</span>Functions</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(5)"><span class="SelectionMark">&nbsp;</span>Variables</a><a class="SelectItem" href="javascript:void(0)" onclick="searchBox.OnSelectItem(6)"><span class="SelectionMark">&nbsp;</span>Typedefs</a></div>

<!-- iframe showing the search results (closed by default) -->
<div id="MSearchResultsWindow">
<iframe src="" frameborder="0" 
        name="MSearchResults" id="MSearchResults">
</iframe>
</div>

</div> <!-- doxygen -->

</div> <!-- doc -->

<?php if (file_exists($root.'/header.php'))
      include $root.'/footer.php'; ?>

</div>

</body>

</html>
