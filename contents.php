<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php $root='.';?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<?php include $root.'/css.php';?>
<meta name="description" content="Seldon is a C++ library for linear
algebra. It provides matrices and vectors objects."/>
<meta name="keywords" content="matrix, vector, Seldon, C++, library,
storage, sparse"/>
<meta name="author" content="Vivien Mallet"/>
<title>Seldon C++ library - linear algebra</title>
</head>

<body>

<div class="page">

<?php include $root.'/header.php';?>

<div class="content">

<p>This page introduces the main structures and the functions available in the
  interface to Blas and Lapack. Other features are described in the
  documentation</a>.</p>

<h1>Available Structures</h1>

<p>Vector structures:</p>
<ol class="close"> 
  <li>Full vectors: <code>Vector&lt;T&gt;</code></li>
  <li>Sparse vectors: <code>Vector&lt;T, VectSparse&gt;</code></li>
</ol>

<p>Matrix structures:</p>
<ol class="close"> 
  <li>Full matrices: <code>Matrix&lt;T&gt;</code></li>
  <li>Sparse matrices: <code>Matrix&lt;T, P, RowSparse&gt;</code></li>
  <li>Alternative sparse matrices: <code>Matrix&lt;T, P, ArrayRowSparse&gt;</code></li>
  <li>Complex sparse matrices: <code>Matrix&lt;T, P, RowComplexSparse&gt;</code></li>
  <li>Alternative complex sparse matrices: <code>Matrix&lt;T, P, ArrayRowComplexSparse&gt;</code></li>
  <li>Symmetric sparse matrices: <code>Matrix&lt;T, P, RowSymSparse&gt;</code></li>
  <li>Symmetric complex sparse matrices: <code>Matrix&lt;T, P, RowSymComplexSparse&gt;</code></li>
  <li>Symmetric matrices: <code>Matrix&lt;T, P, RowSym&gt;</code></li>
  <li>Symmetric packed matrices: <code>Matrix&lt;T, P, RowSymPacked&gt;</code></li>
  <li>Hermitian matrices: <code>Matrix&lt;T, P, RowHerm&gt;</code></li>
  <li>Hermitian packed matrices: <code>Matrix&lt;T, P, RowHermPacked&gt;</code></li>
  <li>Lower triangular matrices: <code>Matrix&lt;T, P, RowLoTriang&gt;</code></li>
  <li>Upper triangular matrices: <code>Matrix&lt;T, P, RowUpTriang&gt;</code></li>
  <li>Lower triangular packed matrices: <code>Matrix&lt;T, P, RowLoTriangPacked&gt;</code></li>
  <li>Upper triangular packed matrices: <code>Matrix&lt;T, P, RowUpTriangPacked&gt;</code></li>
</ol>

<p><code>T</code> can be any
  type: <code>double</code>, <code>float</code>, <code>int</code>,
  <code>bool</code>, <code>Vector&lt;double&gt;</code>, ...
</p>
<p><code>P</code> is a property, usually <code>General</code> (i.e., no
  special property) or <code>Symmetric</code>.</p>
<p>One can always switch to column-oriented storage: simply
  replace <code>Row</code> with <code>Col</code>; e.g. <code>ColSparse</code>
  or <code>ColLoTriangPacked</code>.</p>

<p>3D array structure:</p>
<ol class="close"> 
  <li>Full 3D arrays (provided for
  convenience): <code>Array3D&lt;T&gt;</code></li>
</ol>

<p>The way the vectors, matrices and arrays are allocated can be controlled
  through a class called "allocator". This allocator is the last template
  argument of every structure. For instance, a full matrix may be declared
  as <code>Matrix&lt;double, General, RowMajor&gt;</code> (same
  as <code>Matrix&lt;double&gt;</code>) or as <code>Matrix&lt;double, General,
  RowMajor, NaNAlloc&lt;double&gt; &gt;</code> to use an allocator that sets
  to NaN (not a number) every allocated element.</p>


<h1>Interface to Blas and Lapack</h1>

<p>Available Blas functions (Seldon functions):</p>
<ul class="close"> 
<li> [Blas 1] xROTG (GenRot), xROTMG (GenModifRot), xROT (ApplyRot), xROTM
  (ApplyModifRot), xSWAP (Swap), xSCAL (Mlt), xCOPY (Copy), xAXPY (Add), xDOT
  (DotProd), xDOTU (DotProd), SDSDOT (ScaledDotProd), xDOTC (DotProdConj),
  xASUM (Norm1), xNRM2 (Norm2), IxAMAX (GetMaxAbsIndex);</li>
<li> [Blas 2] xTRMV (Mlt), xTPMV (Mlt), xGEMV (MltAdd), xHEMV (MltAdd), xHPMV
  (MltAdd), xSYMV (MltAdd), xSPMV (MltAdd), xGER (Rank1Update), xGERU
  (Rank1Update), xGERC (Rank1Update), xSPR (Rank1Update), xHPR (Rank1Update),
  xSPR2 (Rank2Update), xHPR2 (Rank2Update), xTRSV (Solve), xTPSV
  (Solve); </li>
<li>[Blas 3] xGEMM (MltAdd), xSYMM (MltAdd), xHEMM (MltAdd), xTRMM (Mlt),
  xTRSM (Solve).</li>
</ul>
</p>

<p>Available Lapack functions (Seldon functions):</p>
<ul class="close"> 
<li> [Linear algebra] xGETRF (GetLU), xSYTRF (GetLU), xSPTRF (GetLU), xHETRF
  (GetLU), xHPTRF (GetLU), xGETRS (SolveLU), xSYTRS (SolveLU), xSPTRS
  (SolveLU), xHETRS (SolveLU), xHPTRS (SolveLU), xTRTRS (SolveLU), xTPTRS
  (SolveLU), xGECON (ReciprocalConditionNumber), xSYCON
  (ReciprocalConditionNumber), xSPCON (ReciprocalConditionNumber), xHECON
  (ReciprocalConditionNumber), xHPCON (ReciprocalConditionNumber), xTRCON
  (ReciprocalConditionNumber), xTPCON (ReciprocalConditionNumber), xGERFS
  (RefineSolutionLU), xSYRFS (RefineSolutionLU), xSPRFS (RefineSolutionLU),
  xHERFS (RefineSolutionLU), xHPRFS (RefineSolutionLU), xTRRFS
  (RefineSolutionLU), xTPRFS (RefineSolutionLU), xGETRI (GetInverse), xSYTRI
  (RefineSolutionLU), xSPTRI (RefineSolutionLU), xHETRI (RefineSolutionLU),
  xHPTRI (RefineSolutionLU), xTRTRI (RefineSolutionLU), xTPTRI
  (RefineSolutionLU), xGEEQU (GetScalingFactors);</li>
<li> [Eigen values] xGEEV (GetEigenvalues, GetEigenvaluesEigenvectors), xSYEV
  (GetEigenvalues, GetEigenvaluesEigenvectors), xHEEV (GetEigenvalues,
  GetEigenvaluesEigenvectors), xSPEV (GetEigenvalues, GetEigenvaluesEigenvectors),
  xHPEV (GetEigenvalues, GetEigenvaluesEigenvectors), xSYGV (GetEigenvalues,
  GetEigenvaluesEigenvectors), xGGEV (GetEigenvalues, GetEigenvaluesEigenvectors),
  xHEGV (GetEigenvalues, GetEigenvaluesEigenvectors), xSPGV (GetEigenvalues,
  GetEigenvaluesEigenvectors), xHPGV (GetEigenvalues, GetEigenvaluesEigenvectors),
  xGESVD (GetSVD), xGEQRF (GetHessian), ZGEQRF + ZUNGQR + ZUNMQR + ZGGHRD
  (GetHessian), ZGEQRF + ZUNGQR + ZUNMQR + ZGGHRD + ZHGEQZ (GetQZ),
  (SolveSylvester); </li>
<li>[Least squares] xGEQRF (GetQR, GetLQ), xGELQF (GetQR, GetLQ), xGEQP3
  (GetQR_Pivot), xORGQR (GetQ_FromQR), xUNGQR (GetQ_FromQR), xUNMQR
  (MltQ_FromQR), xORMQR (MltQ_FromQR), xORMQR + xTRSM (SolveQR), ZUNMQR +
  ZTRSM (SolveQR), xORMLQ + xTRSM (SolveQR), ZUNMLQ + ZTRSM (SolveQR), xTRSM +
  xORMLQ (SolveLQ), ZTRSM + ZUNMLQ (SolveLQ), xTRSM + xORMQR (SolveLQ), ZTRSM
  + ZUNMQR (SolveLQ).</li>
</ul>
</p>

</div>

<?php include $root.'/footer.php'?>

</div>

</body>

</html>
