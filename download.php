<?php echo '<?xml version="1.0"  encoding="iso-8859-1"?'.'>' ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<?php $root='.';?>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
<?php include $root.'/css.php';?>
<meta name="description" content="Seldon is a C++ library for linear
algebra. It provides matrices and vectors objects."/>
<meta name="keywords" content="matrix, vector, Seldon, C++, library,
storage, sparse"/>
<meta name="author" content="Vivien Mallet"/>
<title>Seldon C++ library - linear algebra</title>
</head>

<body>

<div class="page">

<?php include $root.'/header.php';?>

<div class="content">

<h1>Download Source Code</h1>

<p> <b><i>The latest release</i></b>
is: <a href="http://sourceforge.net/projects/seldon/files/seldon-5.2.tar.bz2">Seldon
5.2</a> (2013-02-24). Seldon is provided under the GNU LGPL (Lesser General
Public License). </p>

<p><b><i>Support and requests</b></i>: if you have any question or comment,
you may
<a href="mailto:seldon-help@lists.sourceforge.net">contact
us</a>.</p>

<p>Previous versions are available
on <a href="http://sourceforge.net/project/showfiles.php?group_id=125201"
target="_blank">SourceForge servers</a>.</p>

<p>The latest revision is available in
the <a href="http://gitorious.org/seldon/">Gitorious Git repository</a>, which
is the reference repository:
<br/><code>git clone git://gitorious.org/seldon/src.git seldon</code></p>

<p>On SourceForge, one may find
a <a href="https://sourceforge.net/scm/?type=git&group_id=125201">mirror Git
repository</a>:<br/>
<code>git clone git://seldon.git.sourceforge.net/gitroot/seldon/seldon</code></p>

<p>Note that the SourceForge SVN repository is now closed.</p>

</div>

<?php include $root.'/footer.php'?>

</div>

</body>

</html>
