<?php function HLheader($root_, $headerdoc_, $headerfile_, $section_, $string_)
{
  if ($string_ == "DOCUMENTATION")
    if (substr($headerdoc_, 0, 3) == 'doc')
      echo '<li><a href="'.$root_.'/'.$section_.'.php" style="font-style: italic;">'.$string_.'</a></li>';
    else
      echo '<li><a href="'.$root_.'/'.$section_.'.php">'.$string_.'</a></li>';
  elseif ($headerfile_ == $section_ and substr($headerdoc_, 0, 3) != 'doc')
    echo '<li><a href="'.$root_.'/'.$section_.'.php" style="font-style: italic;">'.$string_.'</a></li>';
  else
    echo '<li><a href="'.$root_.'/'.$section_.'.php">'.$string_.'</a></li>';
}; ?>

<?php $headerfile=basename($_SERVER['SCRIPT_NAME'], ".php"); $headerfile = explode(".", $headerfile); $headerfile = $headerfile[0];?>
<?php $headerdoc=basename(dirname($_SERVER['SCRIPT_NAME']));?>

<div class="header">
  <img src="<?php echo $root?>/image/header.png" alt="" />
</div>
<div class="topmenu">
  <img class="ltopmenu" src="<?php echo $root?>/image/topmenu-left.png" alt="" />
  <img class="rtopmenu" src="<?php echo $root?>/image/topmenu-right.png" alt="" />
  <div class="ttopmenu">
    <ul>
   <?php HLheader($root, $headerdoc, $headerfile, "index", "INTRODUCTION")?>
   <?php HLheader($root, $headerdoc, $headerfile, "contents", "CONTENTS")?>
   <?php HLheader($root, $headerdoc, $headerfile, "doc-5.2/index", "DOCUMENTATION")?>
   <?php HLheader($root, $headerdoc, $headerfile, "download", "DOWNLOAD")?>
    </ul>
  </div>
</div>

<?php if (substr(basename(dirname($_SERVER['SCRIPT_NAME'])), 0, 3) == 'doc') {?>
<div class="submenu">
  <img class="lsubmenu" src="<?php echo $root?>/image/submenu-left.png" alt="" />
  <img class="rsubmenu" src="<?php echo $root?>/image/submenu-right.png" alt="" />
  <div class="tsubmenu">
    <ul>
      <li>
          <?php if (basename(dirname($_SERVER['SCRIPT_NAME'])) == 'doc-5.0')
                echo '<a href="'.$root.'/doc-5.0/index.php">VERSIONS&nbsp;&nbsp;5.0.x</a>';
                else
                echo '<a href="'.$root.'/doc-5.0/index.php" style="color: black;">VERSIONS&nbsp;&nbsp;5.0.x</a>'; ?>
      </li>
      <li>
          <?php if (basename(dirname($_SERVER['SCRIPT_NAME'])) == 'doc-5.1')
                echo '<a href="'.$root.'/doc-5.1/index.php">VERSIONS&nbsp;&nbsp;5.1.x</a>';
                else
                echo '<a href="'.$root.'/doc-5.1/index.php" style="color: black;">VERSIONS&nbsp;&nbsp;5.1.x</a>'; ?>
      </li>
      <li>
          <?php if (basename(dirname($_SERVER['SCRIPT_NAME'])) == 'doc-5.2')
                echo '<a href="'.$root.'/doc-5.2/index.php">VERSION&nbsp;&nbsp;5.2</a>';
                else
                echo '<a href="'.$root.'/doc-5.2/index.php" style="color: black;">VERSION&nbsp;&nbsp;5.2</a>'; ?>
      </li>
      <li>
          <?php if (basename(dirname($_SERVER['SCRIPT_NAME'])) == 'doc-dev')
                echo '<a href="'.$root.'/doc-dev/index.php">DEV.&nbsp;&nbsp;VERSION</a>';
                else
                echo '<a href="'.$root.'/doc-dev/index.php" style="color: black;">DEV.&nbsp;&nbsp;VERSION</a>'; ?>
      </li>
    </ul>
  </div>
</div>
<?php } ?>

<?php if (basename(dirname($_SERVER['SCRIPT_NAME'])) == 'doc-dev') {?>
<p style="color: #448888; font-size: 75%; font-style: italic; margin: 5px 0px
          5px 15px;">Warning: this documentation for the development version is under construction.</p>
<?php } ?>
